//
//  ProfileVisitorsTableViewCell.swift
//  WestIndianMatch
//
//  Created by brst on 23/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class ProfileVisitorsTableViewCell: UITableViewCell
{
    @IBOutlet weak var MessageVisitorBtn: UIButton!
    @IBOutlet weak var blockVisitorBtn: UIButton!
    @IBOutlet weak var deleteVisitorBtn: UIButton!
    @IBOutlet weak var daysAgo_lbl: UILabel!
    @IBOutlet weak var location_lbl: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var userName_lbl: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var cellWrapperView: UIView!
    @IBOutlet weak var onlineUserIcon: UIImageView!
    
    @IBOutlet weak var acitivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var visitorsMeetUp_Lbl: UILabel!
    
    
    //Unblock Blocked Users
    @IBOutlet weak var unblockBtnAction: UIButton!
    @IBOutlet weak var unblockUserLocation: UILabel!
    @IBOutlet weak var unblockUserName: UILabel!
    @IBOutlet weak var unblockProfileImage: UIImageView!
    
    @IBOutlet weak var unblockUsergender: UILabel!
    
    func clearData()
    {
        profileImage.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
