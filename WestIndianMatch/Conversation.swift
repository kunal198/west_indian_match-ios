//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import UIKit
import Firebase

class Conversation {
    //MARK: Properties
    let user: User
    var lastMessage: Message
    var opponentIDWithVisibility: String
    var myIDWithVisibility: String
    var getOpponentRevealStatus: String
    var locationOfThread: String
    
    
    //MARK: Methods
    class func showConversations(completion: @escaping ([Conversation]) -> Swift.Void)
    {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.threadCount = 0
        
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            var conversations = [Conversation]()
        /*FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").observe(.value, with: { (snapshot) in
                
                if snapshot.exists()
                {
                    print("snapshot.value = \(String(describing: snapshot.value))")
                    
                    for childSnap in snapshot.children.allObjects
                    {
                        let snap1 = childSnap as! FIRDataSnapshot
                        
                        print("snap1.key = \(snap1.key)")
                        print("snap1.value = \(String(describing: snap1.value))")
                        
                        let fromID = snap1.key
                        
                        let values = snap1.value as! [String: String]
                        print("values = \(values)")
                        
                        let location = values["location"]!
                        var userType = String()
                        var getRevealStr = String()
                        if values["userType"] != nil
                        {
                            userType  = values["userType"]!
                        }
                        else{
                            userType = "WithProfileUser"
                        }
                        
                        if values["checkRevealProfile"] != nil
                        {
                            getRevealStr  = values["checkRevealProfile"]!
                        }
                        else
                        {
                            getRevealStr = ""
                        }
                        
                        var oppoIDVisibStr = String()
                        var myIDVisibStr = String()
                        
                        if values["myProfileVisibilityStatus"] != nil
                        {
                            myIDVisibStr  = values["myProfileVisibilityStatus"]!
                        }
                        else
                        {
                            myIDVisibStr = currentUserID + "On"
                        }
                        
                        if values["opponentProfileVisibilityStatus"] != nil
                        {
                            oppoIDVisibStr  = values["opponentProfileVisibilityStatus"]!
                        }
                        else
                        {
                            oppoIDVisibStr = fromID //+ "On"
                        }
                        
                        let name = UserDefaults.standard.value(forKey: "userName") as! String
                        let picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
                        var userID = String()
                        if let myID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                        {
                            userID = myID
                        }
                        else
                        {
                            userID = currentUserID
                        }
                        
                        var auth_Token = String()
                        if values["auth_Token"] != nil
                        {
                            auth_Token = values["auth_Token"] as! String
                        }
                        else{
                            auth_Token = ""
                        }
                        
                        User.info(forUserID: fromID, completion: { (user) in
                            
                            print("++++++++++++++++++++++++++++ user informatin for messages ++++++++++++++++++++++++++++ = \(user)")
                            
                            if (user.id == "")
                            {
                                print("user id is blank for mesaagews")
                            }
                            else
                            {
                                let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, profilePicture: picture, userName: name, userID: userID, typeOfUser: userType, checkRevealProfile: getRevealStr, postID: "", postedText: "", postedImageType: "", postedConfessionCheck: "", myProfileInvisibilitySavedStr: "", myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "", opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                
                                let conversation = Conversation.init(user: user, lastMessage: emptyMessage, oppoIDWithVisibility: oppoIDVisibStr, myIDWithVisibilityStr: myIDVisibStr, getOpponentRevealStatusStr: getRevealStr, threadlocation: location)
                                
                                conversations.append(conversation)
                                
                                print("conversations in all messages = \(conversations)")
                                
                            conversation.lastMessage.downloadLastMessage(forLocation: location, opponentIDWithVisibility: fromID,getUserType: userType, completion: { (_) in
                                    completion(conversations)
                                })
                            }
                        })
                    }
                }
            })*/
       
            
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").observe(.childAdded, with: { (snapshot) in
                
                if snapshot.exists()
                {

                    let fromID = snapshot.key
                    
                    let values = snapshot.value as! [String: String]
                    print("values = \(values)")
                    
                    let location = values["location"]!
                    var userType = String()
                    var getRevealStr = String()
                    if values["userType"] != nil
                    {
                        userType  = values["userType"]!
                    }
                    else{
                        userType = "WithProfileUser"
                    }
                    
                    if values["checkRevealProfile"] != nil
                    {
                        getRevealStr  = values["checkRevealProfile"]!
                    }
                    else
                    {
                        getRevealStr = ""
                    }
                    
                    var oppoIDVisibStr = String()
                    var myIDVisibStr = String()
                    
                    if values["myProfileVisibilityStatus"] != nil
                    {
                        myIDVisibStr  = values["myProfileVisibilityStatus"]!
                    }
                    else
                    {
                        myIDVisibStr = currentUserID + "On"
                    }
                    
                    
                    if values["opponentProfileVisibilityStatus"] != nil
                    {
                        oppoIDVisibStr  = values["opponentProfileVisibilityStatus"]!
                    }
                    else
                    {
                        oppoIDVisibStr = fromID //+ "On"
                    }
                    
                    let name = UserDefaults.standard.value(forKey: "userName") as! String
                    let picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
                    var userID = String()
                    if let myID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                    {
                        userID = myID
                    }
                    else
                    {
                        userID = currentUserID
                    }
                    
                    var auth_Token = String()
                    if values["auth_Token"] != nil
                    {
                        auth_Token = values["auth_Token"] as! String
                    }
                    else{
                        auth_Token = ""
                    }
                    
                    User.info(forUserID: fromID, completion: { (user) in
                        
                        print("++++++++++++++++++++++++++++ user informatin for messages ++++++++++++++++++++++++++++ = \(user)")
                        
                        appDel.threadCount = appDel.threadCount + 1

                       if (user.id == "")
                       {
                         print("user id is blank for mesaagews")
                       }
                       else
                       {
                         let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, profilePicture: picture, userName: name, userID: userID, typeOfUser: userType, checkRevealProfile: getRevealStr, postID: "", postedText: "", postedImageType: "", postedConfessionCheck: "", myProfileInvisibilitySavedStr: "", myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "", opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                        
                          let conversation = Conversation.init(user: user, lastMessage: emptyMessage, oppoIDWithVisibility: oppoIDVisibStr, myIDWithVisibilityStr: myIDVisibStr, getOpponentRevealStatusStr: getRevealStr, threadlocation: location)
                        
                         conversations.append(conversation)

                        print("conversations = \(conversations)")

                conversation.lastMessage.downloadLastMessage(forLocation: location, opponentIDWithVisibility: fromID,getUserType: userType, completion: { (_) in
                            completion(conversations)
                        })
                      }
                    })
                }
                else
                {
                  //  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
                }
            })
            
            
            /*FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").observe(.value, with: { (snapshot) in
                
                if snapshot.exists()
                {
                    print("snapshot.value = \(String(describing: snapshot.value))")
                    
                    for childSnap in snapshot.children.allObjects
                    {
                        let snap1 = childSnap as! FIRDataSnapshot
                        
                        print("snap1.key = \(snap1.key)")
                        print("snap1.value = \(String(describing: snap1.value))")
                        
                        let fromID = snap1.key
                        let values = snap1.value as! [String: String]
                        
                        let location = values["location"]!
                        var userType = String()
                        var getRevealStr = String()
                        if values["userType"] != nil
                        {
                            userType  = values["userType"]!
                        }
                        else{
                            userType = "WithProfileUser"
                        }
                        
                        if values["checkRevealProfile"] != nil
                        {
                            getRevealStr  = values["checkRevealProfile"]!
                        }
                        else
                        {
                            getRevealStr = "doesNotExists"
                        }
                        
                        let name = UserDefaults.standard.value(forKey: "userName") as! String
                        let picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
                        var userID = String()
                        if let myID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                        {
                            userID = myID
                        }
                        else{
                            userID = currentUserID
                        }
                        
                        var auth_Token = String()
                        if values["auth_Token"] != nil
                        {
                            auth_Token = values["auth_Token"] as! String
                        }
                        else{
                            auth_Token = ""
                        }
                        
                        User.info(forUserID: fromID, completion: { (user) in
                            
                            if (user.id == "")
                            {
                                print("user id is blank for mesaages")
                            }
                            else
                            {
                                let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, profilePicture: picture, userName: name,userID: userID,typeOfUser: userType,checkRevealProfile: getRevealStr,postID: "", postedText: "", postedImageType: "", postedConfessionCheck: "", myProfileInvisibilitySavedStr: "", myConvoversationTypeStr: "withAnonymousChat", myRevealProfileStatusStr: "", opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                
                                let conversation = Conversation.init(user: user, lastMessage: emptyMessage, oppoIDWithVisibility: "", myIDWithVisibilityStr: "", getOpponentRevealStatusStr: getRevealStr, threadlocation: location)
                                
                                conversations.append(conversation)
                                
                                conversation.lastMessage.downloadLastMessage_AnonymousUser(forLocation: location, getUserType: userType, getUserProfileRevealed: getRevealStr , completion:  { (_) in
                                    completion(conversations)
                                })
                            }
                        })
                    }
                }
             })*/
            
    FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").observe(.childAdded, with: { (snapshot) in
                if snapshot.exists() {
                    
                    let fromID = snapshot.key
                    let values = snapshot.value as! [String: String]
                    
                    let location = values["location"]!
                    var userType = String()
                    var getRevealStr = String()
                    if values["userType"] != nil
                    {
                        userType  = values["userType"]!
                    }
                    else{
                        userType = "WithProfileUser"
                    }
                    
                    if values["checkRevealProfile"] != nil
                    {
                        getRevealStr  = values["checkRevealProfile"]!
                    }
                    else
                    {
                        getRevealStr = "doesNotExists"
                    }
                    
                    let name = UserDefaults.standard.value(forKey: "userName") as! String
                    let picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
                    var userID = String()
                    if let myID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                    {
                        userID = myID
                    }
                    else{
                        userID = currentUserID
                    }
                    
                    var auth_Token = String()
                    if values["auth_Token"] != nil
                    {
                        auth_Token = values["auth_Token"] as! String
                    }
                    else{
                        auth_Token = ""
                    }
                    
                    User.info(forUserID: fromID, completion: { (user) in
                        
                        appDel.threadCount = appDel.threadCount + 1

                        if (user.id == "")
                        {
                            print("user id is blank for mesaages")
                        }
                        else
                        {
                            let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, profilePicture: picture, userName: name,userID: userID,typeOfUser: userType,checkRevealProfile: getRevealStr,postID: "", postedText: "", postedImageType: "", postedConfessionCheck: "", myProfileInvisibilitySavedStr: "", myConvoversationTypeStr: "withAnonymousChat", myRevealProfileStatusStr: "", opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                            
                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage, oppoIDWithVisibility: "", myIDWithVisibilityStr: "", getOpponentRevealStatusStr: getRevealStr, threadlocation: location)
                            
                            conversations.append(conversation)
                            
                    conversation.lastMessage.downloadLastMessage_AnonymousUser(forLocation: location, getUserType: userType, getUserProfileRevealed: getRevealStr , completion:  { (_) in
                                completion(conversations)
                            })
                        }
                    })
                }
                else
                {
                    
                }
            })
        }
    }

    
    //MARK: Inits
    init(user: User, lastMessage: Message, oppoIDWithVisibility: String, myIDWithVisibilityStr: String, getOpponentRevealStatusStr: String, threadlocation: String) {
        self.user = user
        self.lastMessage = lastMessage
        self.opponentIDWithVisibility = oppoIDWithVisibility
        self.myIDWithVisibility = myIDWithVisibilityStr
        self.getOpponentRevealStatus = getOpponentRevealStatusStr
        self.locationOfThread = threadlocation
    }
}
