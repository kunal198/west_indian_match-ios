//
//  WiConfessionsViewController.swift
//  Wi Match
//
//  Created by brst on 03/11/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import SDWebImage
import FLAnimatedImage
import CoreLocation


class compareTimestampForConfessions
{
    var name: String
    var location: String
    var age: String
    var profilePic: String
    var timestamp:String
    var userIDStr: String
    var postedImage: String
    var gender: String
    var reportCount: String
    var likesCount: Int
    var replyCount: String
    var timeFrame: String
    var statusAdded: String
    var postIDStr: String
    var hexColor: String
    var moodTypeStr: String
    var photoAlbumTypeStr: String
    var likedUsersArray: NSMutableDictionary
    var width: Int
    var height: Int
    var removeConfessionStatusStr: String
    var removeConfessionDict: NSMutableDictionary
    
    init(name: String, location: String, age: String, pic: String,timestamp: String,u_id: String, postedImage: String,gender: String, reportCount: String, likesCount: Int, replyCount: String, timeFrame: String, status: String,postID: String, selectedHexColor: String, photoType: String, moodType: String, likedByUsersArray: NSMutableDictionary, width: Int, height: Int, postremoveStr: String, removeConfessionDict: NSMutableDictionary)
    {
        self.name = name
        self.location = location
        self.age = age
        self.profilePic = pic
        self.timestamp = timestamp
        self.userIDStr = u_id
        self.postedImage = postedImage
        self.gender = gender
        self.reportCount = reportCount
        self.likesCount = likesCount
        self.replyCount = replyCount
        self.timeFrame = timeFrame
        self.statusAdded = status
        self.postIDStr = postID
        self.hexColor = selectedHexColor
        self.moodTypeStr = moodType
        self.photoAlbumTypeStr = photoType
        self.likedUsersArray = likedByUsersArray
        self.width = width
        self.height = height
        self.removeConfessionStatusStr = postremoveStr
        self.removeConfessionDict = removeConfessionDict
    }
}


class ConfessionDetails
{
    var location: String
    var age: String
    var createdAt:String
    var userIDStr: String
    var postedImage: String
    var gender: String
    var likesCount: String
    var replyCount: String
    var totalTime: String
    var statusAdded: String
    var postIDStr: String
    var moodTypeStr: String
    var photoAlbumTypeStr: String
    var width: Int
    var height: Int
    var postHours: String
    var likedByMeStatus: String
    var name: String
    var profilePic: String
    var firebase_user_id: String
    var typeOfConfession: String
    
    init(location: String, age: String, createdAt: String, userID: String, postedImage: String, gender: String, likesCount: String, replyCount: String, totalTime: String, Status: String,postID: String, moodType: String, photoTypeAlbum: String, width: Int, height: Int, postHoursStr: String, likedByMeStr: String, username: String, profileStr: String, firebase_user_id: String,typeOfConfession: String)
    {
        self.location = location
        self.age = age
        self.createdAt = createdAt
        self.userIDStr = userID
        self.postedImage = postedImage
        self.gender = gender
        self.likesCount = likesCount
        self.replyCount = replyCount
        self.totalTime = totalTime
        self.statusAdded = Status
        self.postIDStr = postID
        self.moodTypeStr = moodType
        self.photoAlbumTypeStr = photoTypeAlbum
        self.width = width
        self.height = height
        self.postHours = postHoursStr
        self.likedByMeStatus = likedByMeStr
        self.name = username
        self.profilePic = profileStr
        self.firebase_user_id = firebase_user_id
        self.typeOfConfession = typeOfConfession
    }
}

class WiConfessionsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,CLLocationManagerDelegate, UITextViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate {
    
    //MARK:- ***** VARIABLES DECLARATION *****
     var indexPathSelected_NearBy = IndexPath()
     var indexPathSelected_popular = IndexPath()
     var indexPathSelected_Latest = IndexPath()
    
    var reportedUsersByMeArray = NSMutableArray()
    var nearByIntArr = NSMutableArray()
    var popularIntArr = NSMutableArray()
    var latestIntArr = NSMutableArray()
    var getFullDataStr = String()
    
    var latestArrayBool = Bool()
    var postIdLikeStr = String()
    var totalCountArray = NSMutableArray()
    var getButtonClickStr = String()
    var likePostBool = Bool()
    var getUserStatus = String()
    var postIDOwner = String()
    var badgeCount = Int()
    var postIDLikesStr = String()
    var postLikedByArray = NSMutableArray()
    var reportBool = Bool()
    var getLikesCountStr = String()
    var getReportTotalCount = String()
    var userIDToReportStr = String()
    var myStateStr = String()
    var defaultImagesArray = [String]()
    var statusArray = [String]()
    var userID = String()
    var nearByFeedsArray = [ConfessionDetails]()
    var latestFeedsArray = [ConfessionDetails]()
    var latestFeedsCopyArray = [ConfessionDetails]()
    var popularFeedsArray = [ConfessionDetails]()
    var myLocationStr = String()
    var appDelegatePush = String()
    var postReportedByArray = NSMutableArray()
    var getReplyCountStr = String()
    var decreaseCountBool = Bool()
    
    var getLastKeyValue = String()
    
    var indexToClick = Int()
    
    var blockedUsersByMeArray = NSMutableArray()
    var blockedUsersByMeStr = String()
    var blockedUsersArray = NSMutableArray()
    var blockedByUserArray = NSMutableArray()
    
    var ownerDeviceId = String()
    var ownerProfileVisibility_postLikes = String()
    var likedUserDict = NSMutableDictionary()
    
    var latestImagesLoadedArray = [Bool]()
    var getReloadIndexPath = Int()
    var likedBtnBool = Bool()
    
    var tabsBtnStr = String()
    
    var likeBool = Bool()
    var popularBool = Bool()
    var latestBool = Bool()
    
    var checkLikeBtnBool = Bool()
    
    var lastLocation:CGPoint = CGPoint()
    
    var searchArray = NSMutableArray()
  
    var ownerDetailsDict = NSMutableDictionary()
    
    var currentLatitude = Double()
    var currentLongitude = Double()
    
    var currentLatitudeFromHomePage = Double()
    var currentLongitudeFromHomePage = Double()
    var currentLocationFromHomePage = String()
    
    var locationManager: CLLocationManager!
    
    var currentPageNearBy = Int()
    var currentPagePopular = Int()
    var currentPageLatest = Int()
    var totalPageNearby = Int()
    var totalPageLatest = Int()
    var TotalPagePopular = Int()
    
    let appDel = UIApplication.shared.delegate as! AppDelegate
    
    //MARK:- ***** OUTLETS DECLARATION *****
    
    @IBOutlet weak var wrapperViewForHeading_latest: UIView!
    @IBOutlet weak var wrapperViewForHeading_Popular: UIView!
    @IBOutlet weak var wrapperViewForHeadingButtons: UIView!
    @IBOutlet weak var confessionReplyIcon_latest: UIImageView!
    @IBOutlet weak var confessionLikeIcon_latest: UIImageView!
    @IBOutlet weak var confessionReplyIcon_popular: UIImageView!
    @IBOutlet weak var confessionLikeIcon_popular: UIImageView!
    @IBOutlet weak var confessionReplyIcon_nearBy: UIImageView!
    @IBOutlet weak var confessionLikeIcon_nearBy: UIImageView!
    @IBOutlet weak var latestAnimatedGifImage: FLAnimatedImageView!
    @IBOutlet weak var popularAnimatedGifImage: FLAnimatedImageView!
    @IBOutlet weak var nearByAnimatedGifImage: FLAnimatedImageView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var search_txtField: UITextField!
    @IBOutlet weak var confessionLikeLbl: UILabel!
    @IBOutlet weak var confessionReplyLbl: UILabel!
    @IBOutlet weak var nearByConfessions_tableView: UITableView!
    @IBOutlet weak var fullSizeImgae: UIImageView!
    @IBOutlet weak var showFullImageView: UIView!
    @IBOutlet weak var popularConfessions_tableView: UITableView!
    @IBOutlet weak var latestConfessions_tableView: UITableView!
    @IBOutlet weak var noPopularView: UIView!
    @IBOutlet weak var noNearByView: UIView!
    @IBOutlet weak var noLatest_lbl: UILabel!
    @IBOutlet weak var noLatestView: UIView!
    @IBOutlet weak var noPopular_lbl: UILabel!
    @IBOutlet weak var noNearBy_Lbl: UILabel!
    @IBOutlet weak var latestView: UIView!
    @IBOutlet weak var statusMainScrollView: UIScrollView!
    @IBOutlet weak var popularView: UIView!
    @IBOutlet weak var nearByView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var latestUnderline_lbl: UILabel!
    @IBOutlet weak var latest_lbl: UILabel!
    @IBOutlet weak var popularUnderline_lbl: UILabel!
    @IBOutlet weak var popular_lbl: UILabel!
    @IBOutlet weak var nearBy_lbl: UILabel!
    @IBOutlet weak var nearByUnderline_lbl: UILabel!
    @IBOutlet weak var loadingImage: UIImageView!
    @IBOutlet weak var showFullView_Latest: UIView!
    @IBOutlet weak var showFullView_Popular: UIView!
    @IBOutlet weak var popularButtonsWrapperView: UIView!
    @IBOutlet weak var nearByButtonsWrapperView: UIView!
    @IBOutlet weak var latestButtonsWrapperView: UIView!
    @IBOutlet weak var nearByGifWrapperView: UIView!
    @IBOutlet weak var latestReplyCountLbl: UILabel!
    @IBOutlet weak var latestLikeCountLbl: UILabel!
    @IBOutlet weak var latestFullImageView: UIImageView!
    @IBOutlet weak var popularReplyCountLbl: UILabel!
    @IBOutlet weak var popularLikeCountLbl: UILabel!
    @IBOutlet weak var popularFullImage: UIImageView!
    @IBOutlet weak var latestGifWrapperView: UIView!
    @IBOutlet weak var popularGifWrapperView: UIView!
    
    
    
    var refreshControl: UIRefreshControl!
    var refreshControl_popularTableView: UIRefreshControl!
    var refreshControl_latestTableView: UIRefreshControl!
    
    
    // MARK: - ****** viewDidLoad ******
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDel.confessionLocationPoint = 0
        
        self.addingGesturesOnImage()
        
        self.nearByConfessions_tableView.tableHeaderView = wrapperViewForHeadingButtons
        self.popularConfessions_tableView.tableHeaderView = wrapperViewForHeading_Popular
        self.latestConfessions_tableView.tableHeaderView = wrapperViewForHeading_latest
        
        refreshControl = UIRefreshControl()
        
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.nearByConfessions_tableView.addSubview(refreshControl)
        
        self.refreshControl_popularTableView = UIRefreshControl()
      
        self.refreshControl_popularTableView.backgroundColor = UIColor.clear
        self.refreshControl_popularTableView.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.refreshControl_popularTableView.addTarget(self, action: #selector(refresh_popularTableView), for: .valueChanged)
        self.popularConfessions_tableView.addSubview(self.refreshControl_popularTableView)
        
        self.refreshControl_latestTableView = UIRefreshControl()
       
        self.refreshControl_latestTableView.backgroundColor = UIColor.clear
        self.refreshControl_latestTableView.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.refreshControl_latestTableView.addTarget(self, action: #selector(refresh_latestTableView), for: .valueChanged)
       self.latestConfessions_tableView.addSubview(self.refreshControl_latestTableView)
      
        self.setDoneOnKeyboard()
        self.search_txtField.text = ""
        self.tabsBtnStr = "nearBy"
        self.checkLikeBtnBool = false
        self.latestArrayBool = true
        
        self.likeBool = true
        self.latestBool = false
        self.popularBool = false
        
        self.popularConfessions_tableView.rowHeight = UITableViewAutomaticDimension
        self.popularConfessions_tableView.estimatedRowHeight = 109.0
        
        self.latestConfessions_tableView.rowHeight = UITableViewAutomaticDimension
        self.latestConfessions_tableView.estimatedRowHeight = 109.0
        
        self.nearByConfessions_tableView.rowHeight = UITableViewAutomaticDimension
        self.nearByConfessions_tableView.estimatedRowHeight = 109.0
        
        self.decreaseCountBool = false
        
        self.getButtonClickStr = ""
        self.likePostBool = false
        self.getUserStatus = "offline"
        self.postIDLikesStr = ""
        self.ownerProfileVisibility_postLikes = "On"
        self.ownerDeviceId = ""
        self.reportBool = false
        self.getReportTotalCount = ""
        self.userIDToReportStr = ""
      
        self.statusMainScrollView.delegate = self
        self.showFullImageView.isHidden = true
        
        UIApplication.shared.isStatusBarHidden=false
        self.statusMainScrollView.delegate = self
        self.statusMainScrollView.contentSize = CGSize.init(width: self.view.frame.size.width*3, height: statusMainScrollView.contentSize.height)
        
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        self.searchView.layer.cornerRadius = 15
        self.searchView.layer.masksToBounds = false
        
        self.statusArray = ["withoutPicture","withPicture","withoutPicture","withPicture","withPicture","withoutPicture","withPicture","withPicture","withoutPicture","withPicture"]
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(WiRepliesViewController.notifyOnUpdate), name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(WiConfessionsViewController.notifyOnCreatePost), name: NSNotification.Name(rawValue: "notifyOnCreatePost"), object: nil)

        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.getFullDataStr = "fetchAllData"
            
            print("appDel.confessionLocationPoint in viewwillappear = \(appDel.confessionLocationPoint)")
            
            if (self.tabsBtnStr == "nearBy")
            {
                self.currentPageNearBy = 1
                self.nearByFeedsArray = []
                self.getNearByConfessionListing(pageValue: self.currentPageNearBy,searchText: self.search_txtField.text!, completion: { (responseBool) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    self.tabsBtnStr = "nearBy"
                    self.indexPathSelected_NearBy = IndexPath()
                    self.nearBy_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.nearByUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.popular_lbl.textColor =  UIColor.lightGray
                    self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
                    self.latest_lbl.textColor =  UIColor.lightGray
                    self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
                    var frame: CGPoint = self.statusMainScrollView.contentOffset
                    frame.x = self.statusMainScrollView.frame.size.width * CGFloat(100-100);
                    self.statusMainScrollView.setContentOffset(frame, animated: false)
                    
                    if (responseBool == true)
                    {
                        DispatchQueue.main.async {
                            self.noNearByView.isHidden = true
                            self.nearByConfessions_tableView.isHidden = false
                            self.nearByConfessions_tableView.reloadData()
                            self.nearByConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                            
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else
                    {
                        self.nearByFeedsArray = []
                        self.noNearBy_Lbl.text = "No nearby confessions found"
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.nearByConfessions_tableView.isHidden = false
                        self.noNearByView.isHidden = false
                        DispatchQueue.main.async {
                            self.nearByConfessions_tableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                })
            }
            else if (self.tabsBtnStr == "popular")
            {
                self.currentPagePopular = 1
                self.popularFeedsArray = []
                self.getPopularConfessionListing(pageValue: self.currentPagePopular,searchText: self.search_txtField.text!, completion: { (response) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    self.tabsBtnStr = "popular"
                    self.indexPathSelected_popular = IndexPath()
                    self.popular_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.popularUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.nearBy_lbl.textColor =  UIColor.lightGray
                    self.nearByUnderline_lbl.backgroundColor =  UIColor.lightGray
                    self.latest_lbl.textColor =  UIColor.lightGray
                    self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
                    var frame: CGPoint = self.statusMainScrollView.contentOffset
                    frame.x = self.statusMainScrollView.frame.size.width * CGFloat(101-100);
                    self.statusMainScrollView.setContentOffset(frame, animated: false)
                    
                    if (response == true)
                    {
                        DispatchQueue.main.async {
                            self.noPopularView.isHidden = true
                            self.popularConfessions_tableView.isHidden = false
                            self.popularConfessions_tableView.reloadData()
                            self.popularConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                            self.refreshControl_popularTableView.endRefreshing()
                        }
                    }
                    else
                    {
                        self.popularFeedsArray = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.popularConfessions_tableView.isHidden = false
                        self.noPopularView.isHidden = false
                        DispatchQueue.main.async {
                            self.popularConfessions_tableView.reloadData()
                            self.refreshControl_popularTableView.endRefreshing()
                        }
                    }
                })
            }
            else if (self.tabsBtnStr == "latest")
            {
                self.currentPageLatest = 1
                self.latestFeedsArray = []
                self.latestFeedsCopyArray = []
                self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.tabsBtnStr = "latest"
                    self.indexPathSelected_Latest = IndexPath()
                    self.latest_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.latestUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.nearBy_lbl.textColor =  UIColor.lightGray
                    self.nearByUnderline_lbl.backgroundColor =  UIColor.lightGray
                    self.popular_lbl.textColor =  UIColor.lightGray
                    self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
                    
                    var frame: CGPoint = self.statusMainScrollView.contentOffset
                    frame.x = self.statusMainScrollView.frame.size.width * CGFloat(102-100);
                    self.statusMainScrollView.setContentOffset(frame, animated: false)
                    
                    if (response == true)
                    {
                        DispatchQueue.main.async {
                            
                            self.noLatestView.isHidden = true
                            self.latestConfessions_tableView.isHidden = false
                            self.latestConfessions_tableView.reloadData()
                            self.latestConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                            
                            
                            /*  if (self.appDel.confessionLocationPoint != nil)
                             {
                             if (self.appDel.confessionLocationPoint > 0)
                             {
                             self.latestConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                             }
                             else{
                             self.latestConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                             }
                             }
                             else{
                             self.latestConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                             }*/
                            
                            self.refreshControl_latestTableView.endRefreshing()
                        }
                    }
                    else
                    {
                        self.latestFeedsArray = []
                        self.latestFeedsCopyArray = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.latestConfessions_tableView.isHidden = false
                        self.noLatestView.isHidden = false
                        DispatchQueue.main.async {
                            self.latestConfessions_tableView.reloadData()
                            self.refreshControl_latestTableView.endRefreshing()
                        }
                    }
                })
            }
        }
    }
    
    //MARK: - ***** userBlocked *****
    func notifyOnCreatePost()
    {
        print("notifyOnCreatePost")
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.getFullDataStr = "fetchAllData"
            
            print("appDel.confessionLocationPoint in viewwillappear = \(appDel.confessionLocationPoint)")
            
            if (self.tabsBtnStr == "nearBy")
            {
                self.currentPageNearBy = 1
                self.nearByFeedsArray = []
                self.getNearByConfessionListing(pageValue: self.currentPageNearBy,searchText: self.search_txtField.text!, completion: { (responseBool) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    self.tabsBtnStr = "nearBy"
                    self.indexPathSelected_NearBy = IndexPath()
                    self.nearBy_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.nearByUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.popular_lbl.textColor =  UIColor.lightGray
                    self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
                    self.latest_lbl.textColor =  UIColor.lightGray
                    self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
                    var frame: CGPoint = self.statusMainScrollView.contentOffset
                    frame.x = self.statusMainScrollView.frame.size.width * CGFloat(100-100);
                    self.statusMainScrollView.setContentOffset(frame, animated: false)
                    
                    if (responseBool == true)
                    {
                        DispatchQueue.main.async {
                            self.noNearByView.isHidden = true
                            self.nearByConfessions_tableView.isHidden = false
                            self.nearByConfessions_tableView.reloadData()
                            self.nearByConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                            
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else
                    {
                        self.nearByFeedsArray = []
                        self.noNearBy_Lbl.text = "No nearby confessions found"
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.nearByConfessions_tableView.isHidden = false
                        self.noNearByView.isHidden = false
                        DispatchQueue.main.async {
                            self.nearByConfessions_tableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                })
            }
            else if (self.tabsBtnStr == "popular")
            {
                self.currentPagePopular = 1
                self.popularFeedsArray = []
                self.getPopularConfessionListing(pageValue: self.currentPagePopular,searchText: self.search_txtField.text!, completion: { (response) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    self.tabsBtnStr = "popular"
                    self.indexPathSelected_popular = IndexPath()
                    self.popular_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.popularUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.nearBy_lbl.textColor =  UIColor.lightGray
                    self.nearByUnderline_lbl.backgroundColor =  UIColor.lightGray
                    self.latest_lbl.textColor =  UIColor.lightGray
                    self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
                    var frame: CGPoint = self.statusMainScrollView.contentOffset
                    frame.x = self.statusMainScrollView.frame.size.width * CGFloat(101-100);
                    self.statusMainScrollView.setContentOffset(frame, animated: false)
                    
                    if (response == true)
                    {
                        DispatchQueue.main.async {
                            self.noPopularView.isHidden = true
                            self.popularConfessions_tableView.isHidden = false
                            self.popularConfessions_tableView.reloadData()
                            self.popularConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                            self.refreshControl_popularTableView.endRefreshing()
                        }
                    }
                    else
                    {
                        self.popularFeedsArray = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.popularConfessions_tableView.isHidden = false
                        self.noPopularView.isHidden = false
                        DispatchQueue.main.async {
                            self.popularConfessions_tableView.reloadData()
                            self.refreshControl_popularTableView.endRefreshing()
                        }
                    }
                })
            }
            else if (self.tabsBtnStr == "latest")
            {
                self.currentPageLatest = 1
                self.latestFeedsArray = []
                self.latestFeedsCopyArray = []
                self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.tabsBtnStr = "latest"
                    self.indexPathSelected_Latest = IndexPath()
                    self.latest_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.latestUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    self.nearBy_lbl.textColor =  UIColor.lightGray
                    self.nearByUnderline_lbl.backgroundColor =  UIColor.lightGray
                    self.popular_lbl.textColor =  UIColor.lightGray
                    self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
                    
                    var frame: CGPoint = self.statusMainScrollView.contentOffset
                    frame.x = self.statusMainScrollView.frame.size.width * CGFloat(102-100);
                    self.statusMainScrollView.setContentOffset(frame, animated: false)
                    
                    if (response == true)
                    {
                        DispatchQueue.main.async {
                            
                            self.noLatestView.isHidden = true
                            self.latestConfessions_tableView.isHidden = false
                            self.latestConfessions_tableView.reloadData()
                            self.latestConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                            
                            
                            /*  if (self.appDel.confessionLocationPoint != nil)
                             {
                             if (self.appDel.confessionLocationPoint > 0)
                             {
                             self.latestConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                             }
                             else{
                             self.latestConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                             }
                             }
                             else{
                             self.latestConfessions_tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                             }*/
                            
                            self.refreshControl_latestTableView.endRefreshing()
                        }
                    }
                    else
                    {
                        self.latestFeedsArray = []
                        self.latestFeedsCopyArray = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.latestConfessions_tableView.isHidden = false
                        self.noLatestView.isHidden = false
                        DispatchQueue.main.async {
                            self.latestConfessions_tableView.reloadData()
                            self.refreshControl_latestTableView.endRefreshing()
                        }
                    }
                })
            }
        }
    }
    
    func addingGesturesOnImage()
    {
        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGestureDetected(_:)))
        pinchGestureRecognizer.delegate = self
        self.fullSizeImgae.addGestureRecognizer(pinchGestureRecognizer)
      nearByAnimatedGifImage.addGestureRecognizer(pinchGestureRecognizer)
        
        let rotationGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(self.rotationGestureDetected(_:)))
        rotationGestureRecognizer.delegate = self
//        self.fullSizeImgae.addGestureRecognizer(rotationGestureRecognizer)
        
        // create and configure the pan gesture
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.panGestureDetected(_:)))
        panGestureRecognizer.delegate = self
//        self.fullSizeImgae.addGestureRecognizer(panGestureRecognizer)
        
        self.fullSizeImgae.isUserInteractionEnabled = true
    }
    
    func pinchGestureDetected(_ recognizer: UIPinchGestureRecognizer?) {
        let state: UIGestureRecognizerState? = recognizer?.state
        if state == .began || state == .changed {
            let scale: CGFloat? = recognizer?.scale
            recognizer?.view?.transform = (recognizer?.view?.transform.scaledBy(x: scale!, y: scale!))!
            recognizer?.scale = 1.0
        }
        else if state == .ended
        {
            if (self.tabsBtnStr == "nearBy")
            {
                self.nearByConfessions_tableView.isUserInteractionEnabled = true
                self.statusMainScrollView.isUserInteractionEnabled = true
            }
            else if (self.tabsBtnStr == "popular")
            {
                
            }
            else
            {
                
            }
        }
    }
    
    func rotationGestureDetected(_ recognizer: UIRotationGestureRecognizer?) {
        let state: UIGestureRecognizerState? = recognizer?.state
        if state == .began || state == .changed {
            let rotation: CGFloat? = recognizer?.rotation
            recognizer?.view?.transform = (recognizer?.view?.transform.rotated(by: rotation!))!
            recognizer?.rotation = 0
        }
    }
    
    func panGestureDetected(_ recognizer: UIPanGestureRecognizer?) {
        let state: UIGestureRecognizerState? = recognizer?.state
        if state == .began || state == .changed {
            let translation: CGPoint? = recognizer?.translation(in: recognizer?.view)
            recognizer?.view?.transform = (recognizer?.view?.transform.translatedBy(x: (translation?.x)!, y: (translation?.y)!))!
            recognizer?.setTranslation(CGPoint.zero, in: recognizer?.view)
        }
    }

    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        if (self.tabsBtnStr == "nearBy")
        {
           self.nearByConfessions_tableView.isUserInteractionEnabled = false
            self.statusMainScrollView.isUserInteractionEnabled = false
        }
        else if (self.tabsBtnStr == "popular")
        {
            
        }
        else
        {
            
        }
        
        return true
    }
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool{
        
        // check for the url string for performing your own custom actions here
        let urlString = URL.absoluteString
        
        // Return NO if you don't want iOS to open the link
        return true
    }
  
    
    func getNearByConfessionListing(pageValue: Int,searchText: String,completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getNearByConfessionsListing(userID: userID, userToken: authToken, searchText: searchText, currentLatitude: self.currentLatitudeFromHomePage, currentLongitude: self.currentLongitudeFromHomePage,paginationValue: pageValue, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            
                            if (dataDict!["meta"] != nil)
                            {
                                let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                                self.totalPageNearby = (metaDict?.value(forKey: "totalpages") as? Int)!
                            }
                            
                            if (dataDict!["list"] != nil)
                            {
                                let nearByListingArray = dataDict?.value(forKey: "list") as! NSArray
                                
                                if (nearByListingArray.count > 0)
                                {
                                    for index in 0..<nearByListingArray.count
                                    {
                                        var userData = NSDictionary()
                                        userData = nearByListingArray[index] as! NSDictionary
                                        
                                        let timestamp: String = userData.value(forKey: "created") as! String
                                        
                                        var loc = String()
                                        var age = String()
                                        var gender = String()
                                        var checkUserExists = String()
                                        var username = String()
                                        var userPic = String()
                                        var firebase_user_id = String()
                                        
                                        let userID: String = userData.value(forKey: "user_id") as! String
                                        let postedImage: String = userData.value(forKey: "image") as! String
                                        
                                        let likeCountInt: Int = userData.value(forKey: "likes_count") as! Int
                                        let likeCountStr : String = String(likeCountInt)
                                        let likedby_current: String = userData.value(forKey: "likedby_current") as! String
                                        
                                        let replyCountStr : Int = userData.value(forKey: "comments_count") as! Int
                                        let total_duration: String = userData.value(forKey: "created") as! String
                                        let statusAddedStr: String = userData.value(forKey: "text") as! String
                                        let postID: String = userData.value(forKey: "id") as! String
                                        let mood: String = userData.value(forKey: "mood") as! String
                                        let duration: String = userData.value(forKey: "duration") as! String
                                        
                                         let type: String = userData.value(forKey: "type") as! String
                                        
                                        
//                                          let location: String = userData.value(forKey: "location") as! String
                                        
                                        let image_height: String = userData.value(forKey: "image_height") as! String
                                        let image_width: String = userData.value(forKey: "image_width") as! String
                                        
                                        var image_height_int = Int()
                                        var image_width_int = Int()
                                        
                                        if (image_height == "")
                                        {
                                            image_height_int = 250
                                        }
                                        else{
                                            image_height_int = Int(image_height)!
                                        }
                                        
                                        if (image_width == "")
                                        {
                                            image_width_int = 250
                                        }
                                        else{
                                            image_width_int = Int(image_width)!
                                        }
                                       
                                        var usersDetailsDict = NSDictionary()
                                        if (userData["user"] != nil)
                                        {
                                            usersDetailsDict = (userData.value(forKey: "user") as? NSDictionary)!
                                            
                                            checkUserExists = usersDetailsDict.value(forKey: "found") as! String
                                            
                                            if (checkUserExists == "no")
                                            {
                                                return
                                            }
                                            
                                            age = String(usersDetailsDict.value(forKey: "Age") as! Int)
                                            
                                            loc = usersDetailsDict.value(forKey: "Location") as! String
                                            gender = usersDetailsDict.value(forKey: "gender") as! String
                                            username = usersDetailsDict.value(forKey: "username") as! String
                                            userPic = usersDetailsDict.value(forKey: "profile_pic") as! String
                                            firebase_user_id = usersDetailsDict.value(forKey: "firebase_user_id") as! String
                                          
                                        }
                                        else
                                        {
                                            age = ""
                                            loc = ""
                                            gender = ""
                                            username = ""
                                            userPic = ""
                                            checkUserExists = "no"
                                            firebase_user_id = ""
                                        }
                                        
                                        if (checkUserExists == "yes")
                                        {
                                            
                                            let userConfession = ConfessionDetails.init(location: loc, age: age, createdAt: timestamp, userID: userID, postedImage: postedImage, gender: gender, likesCount: likeCountStr, replyCount: String(replyCountStr), totalTime: total_duration, Status: statusAddedStr, postID: postID, moodType: mood, photoTypeAlbum: "", width: image_width_int, height: image_height_int, postHoursStr: duration,likedByMeStr: likedby_current,username: username,profileStr: userPic, firebase_user_id: firebase_user_id,typeOfConfession: type)
                                            
                                            self.nearByFeedsArray.append(userConfession)
                                        }
                                    }
                                    
                                    completion(true)
                                }
                                else
                                {
                                    completion(false)
                                }
                            }
                            else
                            {
                                completion(false)
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    
    func getPopularConfessionListing(pageValue: Int,searchText: String,completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getPopularConfessionsListing(pageValue: pageValue,userID: userID, userToken: authToken, searchText: searchText, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            if (dataDict!["meta"] != nil)
                            {
                                let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                                self.TotalPagePopular = (metaDict?.value(forKey: "totalpages") as? Int)!
                            }
                            
                            if (dataDict!["list"] != nil)
                            {
                                let nearByListingArray = dataDict?.value(forKey: "list") as! NSArray
                                if (nearByListingArray.count > 0)
                                {
                                    for index in 0..<nearByListingArray.count
                                    {
                                        var userData = NSDictionary()
                                        userData = nearByListingArray[index] as! NSDictionary
                                        
                                        let timestamp: String = userData.value(forKey: "created") as! String
                                        var loc = String()
                                        var age = String()
                                        var gender = String()
                                        var username = String()
                                        var userPic = String()
                                        var checkUserExists = String()
                                        var firebase_user_id = String()
                                        
                                        let userID: String = userData.value(forKey: "user_id") as! String
                                        let postedImage: String = userData.value(forKey: "image") as! String
                                        
                                        let likeCountInt: Int = userData.value(forKey: "likes_count") as! Int
                                        let likeCountStr : String = String(likeCountInt)
                                        let likedby_current: String = userData.value(forKey: "likedby_current") as! String
                                        let replyCountStr : Int = userData.value(forKey: "comments_count") as! Int
                                         let total_duration: String = userData.value(forKey: "created") as! String
                                        let statusAddedStr: String = userData.value(forKey: "text") as! String
                                        let postID: String = userData.value(forKey: "id") as! String
                                        let mood: String = userData.value(forKey: "mood") as! String
                                        let duration: String = userData.value(forKey: "duration") as! String
                                        
                                         let type: String = userData.value(forKey: "type") as! String
                                        
//                                        let location: String = userData.value(forKey: "location") as! String
                                        
                                        let image_height: String = userData.value(forKey: "image_height") as! String
                                        let image_width: String = userData.value(forKey: "image_width") as! String
                                        
                                        var image_height_int = Int()
                                        var image_width_int = Int()
                                        
                                        if (image_height == "")
                                        {
                                            image_height_int = 250
                                        }
                                        else{
                                            image_height_int = Int(image_height)!
                                        }
                                        
                                        if (image_width == "")
                                        {
                                            image_width_int = 250
                                        }
                                        else{
                                            image_width_int = Int(image_width)!
                                        }
                                        
                                        var usersDetailsDict = NSDictionary()
                                        if (userData["user"] != nil)
                                        {
                                            usersDetailsDict = (userData.value(forKey: "user") as? NSDictionary)!
                                            
                                            checkUserExists = usersDetailsDict.value(forKey: "found") as! String
                                            
                                            if (checkUserExists == "no")
                                            {
                                                return
                                            }
                                            
                                            age = String(usersDetailsDict.value(forKey: "Age") as! Int)
                                            loc = usersDetailsDict.value(forKey: "Location") as! String
                                            gender = usersDetailsDict.value(forKey: "gender") as! String
                                            
                                            username = usersDetailsDict.value(forKey: "username") as! String
                                            userPic = usersDetailsDict.value(forKey: "profile_pic") as! String
                                            firebase_user_id = usersDetailsDict.value(forKey: "firebase_user_id") as! String
                                        }
                                        else
                                        {
                                            age = ""
                                            loc = ""
                                            gender = ""
                                            username = ""
                                            userPic = ""
                                            firebase_user_id = ""
                                        }
                                        
                                        if (checkUserExists == "yes")
                                        {
                                            let userConfession = ConfessionDetails.init(location: loc, age: age, createdAt: timestamp, userID: userID, postedImage: postedImage, gender: gender, likesCount: likeCountStr, replyCount: String(replyCountStr), totalTime: total_duration, Status: statusAddedStr, postID: postID, moodType: mood, photoTypeAlbum: "", width: image_width_int, height: image_height_int, postHoursStr: duration, likedByMeStr: likedby_current, username: username, profileStr: userPic, firebase_user_id: firebase_user_id, typeOfConfession: type)
                                            
                                            self.popularFeedsArray.append(userConfession)
                                        }
                                    }
                                    
                                    completion(true)
                                }
                                else
                                {
                                    completion(false)
                                }
                            }
                            else
                            {
                                completion(false)
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    func getLatestConfessionListing(paginationValueCount: Int,searchText: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getLatestConfessionsListing(pageValue: paginationValueCount ,userID: userID, userToken: authToken, searchText: searchText, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
  
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            if (dataDict!["meta"] != nil)
                            {
                                let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                                self.totalPageLatest = (metaDict?.value(forKey: "totalpages") as? Int)!
                            }
                            
                            if (dataDict!["list"] != nil)
                            {
                                let nearByListingArray = dataDict?.value(forKey: "list") as! NSArray
                                if (nearByListingArray.count > 0)
                                {
                                    for index in 0..<nearByListingArray.count
                                    {
                                        var userData = NSDictionary()
                                        userData = nearByListingArray[index] as! NSDictionary
                                        
                                        let timestamp: String = userData.value(forKey: "created") as! String
                                        
                                        var username = String()
                                        var userPic = String()
                                        var loc = String()
                                        var age = String()
                                        var gender = String()
                                        var checkUserExists = String()
                                        var firebase_user_id = String()
                                        
                                        let userID: String = userData.value(forKey: "user_id") as! String
                                        let postedImage: String = userData.value(forKey: "image") as! String
                                        
                                        let likeCountInt: Int = userData.value(forKey: "likes_count") as! Int
                                        let likeCountStr : String = String(likeCountInt)
                                        let likedby_current: String = userData.value(forKey: "likedby_current") as! String
                                        let replyCountStr : Int = userData.value(forKey: "comments_count") as! Int
                                         let total_duration: String = userData.value(forKey: "created") as! String
                                        let statusAddedStr: String = userData.value(forKey: "text") as! String
                                        let postID: String = userData.value(forKey: "id") as! String
                                        let mood: String = userData.value(forKey: "mood") as! String
                                        let duration: String = userData.value(forKey: "duration") as! String
                                         let type: String = userData.value(forKey: "type") as! String
                                        
//                                        let location: String = userData.value(forKey: "location") as! String
                                        
                                        let image_height: String = userData.value(forKey: "image_height") as! String
                                        let image_width: String = userData.value(forKey: "image_width") as! String
                                        
                                        var image_height_int = Int()
                                        var image_width_int = Int()
                                        
                                        if (image_height == "")
                                        {
                                            image_height_int = 250
                                        }
                                        else{
                                            image_height_int = Int(image_height)!
                                        }
                                        
                                        if (image_width == "")
                                        {
                                            image_width_int = 250
                                        }
                                        else{
                                            image_width_int = Int(image_width)!
                                        }
                                        
                                        var usersDetailsDict = NSDictionary()
                                        if (userData["user"] != nil)
                                        {
                                            usersDetailsDict = (userData.value(forKey: "user") as? NSDictionary)!
                                            
                                            checkUserExists = usersDetailsDict.value(forKey: "found") as! String
                                            
                                            if (checkUserExists == "no")
                                            {
                                                return
                                            }
                                            
                                            age = String(usersDetailsDict.value(forKey: "Age") as! Int)
                                            loc = usersDetailsDict.value(forKey: "Location") as! String
                                            gender = usersDetailsDict.value(forKey: "gender") as! String
                                            
                                            username = usersDetailsDict.value(forKey: "username") as! String
                                            userPic = usersDetailsDict.value(forKey: "profile_pic") as! String
                                            firebase_user_id = usersDetailsDict.value(forKey: "firebase_user_id") as! String
                                        }
                                        else
                                        {
                                            age = ""
                                            loc = ""
                                            gender = ""
                                            username = ""
                                            userPic = ""
                                            firebase_user_id = ""
                                        }
                                        
                                        if (checkUserExists == "yes")
                                        {
                                            let userConfession = ConfessionDetails.init(location: loc, age: age, createdAt: timestamp, userID: userID, postedImage: postedImage, gender: gender, likesCount: likeCountStr, replyCount: String(replyCountStr), totalTime: total_duration, Status: statusAddedStr, postID: postID, moodType: mood, photoTypeAlbum: "", width: image_width_int, height: image_height_int, postHoursStr: duration,likedByMeStr: likedby_current, username: username,profileStr: userPic, firebase_user_id: firebase_user_id,typeOfConfession: type)
                                            
                                            self.latestFeedsArray.append(userConfession)
                                        }
                                    }
                                    
                                    completion(true)
                                }
                                else
                                {
                                    completion(false)
                                }
                            }
                            else
                            {
                                completion(false)
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        
                    })
                }
            }
        }
    }
    
    
    // MARK: - ****** Fetch Opponent's Users Details ******
    func fetchOwnerDetails(opponentUserID: String, completion: @escaping (NSMutableDictionary) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WWAN")
        }
    }
    
    
    func refresh_latestTableView(_ sender: Any) {
 
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            self.currentPageLatest = 1
            self.latestFeedsArray = []
            self.latestFeedsCopyArray = []
            self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                if (response == true)
                {
                    DispatchQueue.main.async {
                        self.noLatestView.isHidden = true
                        self.latestConfessions_tableView.isHidden = false
                        self.latestConfessions_tableView.reloadData()
                        self.refreshControl_latestTableView.endRefreshing()
                    }
                }
                else
                {
                    self.latestFeedsArray = []
                    self.latestFeedsCopyArray = []
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.latestConfessions_tableView.isHidden = false
                    self.noLatestView.isHidden = false
                    DispatchQueue.main.async {
                        self.latestConfessions_tableView.reloadData()
                        self.refreshControl_latestTableView.endRefreshing()
                    }
                }
            })
        }
    }
    
    func refresh_popularTableView(_ sender: Any) {
     
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            self.currentPagePopular = 1
            self.popularFeedsArray = []
            self.getPopularConfessionListing(pageValue: self.currentPagePopular, searchText: self.search_txtField.text!, completion: { (response) in
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                
                if (response == true)
                {
                    DispatchQueue.main.async {
                        self.noPopularView.isHidden = true
                        self.popularConfessions_tableView.isHidden = false
                        self.popularConfessions_tableView.reloadData()
                        self.refreshControl_popularTableView.endRefreshing()
                    }
                }
                else
                {
                    self.popularFeedsArray = []
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.popularConfessions_tableView.isHidden = false
                    self.noPopularView.isHidden = false
                    DispatchQueue.main.async {
                        self.popularConfessions_tableView.reloadData()
                        self.refreshControl_popularTableView.endRefreshing()
                    }
                }
            })
        }
    }
    
    func refresh(_ sender: Any) {

        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            self.getFullDataStr = "fetchAllData"
            self.currentPageNearBy = 1
            self.nearByFeedsArray = []
            self.getNearByConfessionListing(pageValue: self.currentPageNearBy,searchText: self.search_txtField.text!, completion: { (responseBool) in
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                if (responseBool == true)
                {
                    DispatchQueue.main.async {
                        self.noNearByView.isHidden = true
                        self.nearByConfessions_tableView.isHidden = false
                        self.nearByConfessions_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.nearByFeedsArray = []
                    
                    self.noNearBy_Lbl.text = "No nearby confessions found"
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.nearByConfessions_tableView.isHidden = false
                    self.noNearByView.isHidden = false
                    
                    DispatchQueue.main.async {
                        self.nearByConfessions_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
            })
        }
    }

    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if self.search_txtField.text != ""
            {
                if (self.tabsBtnStr == "nearBy")
                {
                    self.getFullDataStr = "fetchAllData"
                    
                    self.currentPageNearBy = 1
                    self.nearByFeedsArray = []
                    self.getNearByConfessionListing(pageValue: self.currentPageNearBy, searchText: self.search_txtField.text!, completion: { (responseBool) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        if (responseBool == true)
                        {
                            DispatchQueue.main.async {
                                self.noNearByView.isHidden = true
                                self.nearByConfessions_tableView.isHidden = false
                                self.nearByConfessions_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                        else
                        {
                            self.nearByFeedsArray = []
                            
                            self.noNearBy_Lbl.text = "No nearby confessions found"
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.nearByConfessions_tableView.isHidden = false
                            self.noNearByView.isHidden = false
                            
                            DispatchQueue.main.async {
                                self.nearByConfessions_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                    })
                }
                else if (self.tabsBtnStr == "popular")
                {
                    self.currentPagePopular = 1
                    self.popularFeedsArray = []
                    self.getPopularConfessionListing(pageValue: self.currentPagePopular,searchText: self.search_txtField.text!,completion: { (response) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (response == true)
                        {
                            DispatchQueue.main.async {
                                self.noPopularView.isHidden = true
                                self.popularConfessions_tableView.isHidden = false
                                self.popularConfessions_tableView.reloadData()
                                self.refreshControl_popularTableView.endRefreshing()
                            }
                        }
                        else
                        {
                            self.popularFeedsArray = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.popularConfessions_tableView.isHidden = false
                            self.noPopularView.isHidden = false
                            DispatchQueue.main.async {
                                self.popularConfessions_tableView.reloadData()
                                self.refreshControl_popularTableView.endRefreshing()
                            }
                        }
                    })
                }
                else if (self.tabsBtnStr == "latest")
                {
                     self.currentPageLatest = 1
                    self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!,completion: { (response) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        if (response == true)
                        {
                            DispatchQueue.main.async {
                                self.noLatestView.isHidden = true
                                self.latestConfessions_tableView.isHidden = false
                                self.latestConfessions_tableView.reloadData()
                                self.refreshControl_latestTableView.endRefreshing()
                            }
                        }
                        else
                        {
                            self.latestFeedsArray = []
                            self.latestFeedsCopyArray = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.latestConfessions_tableView.isHidden = false
                            self.noLatestView.isHidden = false
                            DispatchQueue.main.async {
                                self.latestConfessions_tableView.reloadData()
                                self.refreshControl_latestTableView.endRefreshing()
                            }
                        }
                    })
                }
            }
            else
            {
                self.search_txtField.text = ""
                self.getFullDataStr = "fetchAllData"
                if  self.tabsBtnStr == "nearBy"
                {
                    self.currentPageNearBy = 1
                    self.nearByFeedsArray = []
                    self.getNearByConfessionListing(pageValue:  self.currentPageNearBy ,searchText: self.search_txtField.text!,completion: { (responseBool) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (responseBool == true)
                        {
                            DispatchQueue.main.async {
                                self.noNearByView.isHidden = true
                                self.nearByConfessions_tableView.isHidden = false
                                self.nearByConfessions_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                        else
                        {
                            self.nearByFeedsArray = []
                            self.noNearBy_Lbl.text = "No nearby confessions found"
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.nearByConfessions_tableView.isHidden = false
                            self.noNearByView.isHidden = false
                            
                            DispatchQueue.main.async {
                                self.nearByConfessions_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                    })
                }
                else if  self.tabsBtnStr == "popular"
                {
                    self.currentPagePopular = 1
                    self.popularFeedsArray = []
                    self.getPopularConfessionListing(pageValue: self.currentPagePopular,searchText: self.search_txtField.text!, completion: { (response) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        if (response == true)
                        {
                            DispatchQueue.main.async {
                                self.noPopularView.isHidden = true
                                self.popularConfessions_tableView.isHidden = false
                                self.popularConfessions_tableView.reloadData()
                                self.refreshControl_popularTableView.endRefreshing()
                            }
                        }
                        else
                        {
                            self.popularFeedsArray = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.popularConfessions_tableView.isHidden = false
                            self.noPopularView.isHidden = false
                            DispatchQueue.main.async {
                                self.popularConfessions_tableView.reloadData()
                                self.refreshControl_popularTableView.endRefreshing()
                            }
                        }
                    })
                }
                else if  self.tabsBtnStr == "latest"
                {
                    self.currentPageLatest = 1
                    self.latestFeedsArray = []
                    self.latestFeedsCopyArray = []
                    self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        if (response == true)
                        {
                            DispatchQueue.main.async {
                                self.noLatestView.isHidden = true
                                self.latestConfessions_tableView.isHidden = false
                                self.latestConfessions_tableView.reloadData()
                                self.refreshControl_latestTableView.endRefreshing()
                            }
                        }
                        else
                        {
                            self.latestFeedsArray = []
                            self.latestFeedsCopyArray = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.latestConfessions_tableView.isHidden = false
                            self.noLatestView.isHidden = false
                            DispatchQueue.main.async {
                                self.latestConfessions_tableView.reloadData()
                                self.refreshControl_latestTableView.endRefreshing()
                            }
                        }
                    })
                }
            }
        }
        return true
    }
 
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(WiConfessionsViewController.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.search_txtField.inputAccessoryView = keyboardToolbar
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
        self.search_txtField.resignFirstResponder()
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
           
            if self.search_txtField.text != ""
            {
//                self.searchConfessions(_searchtext: self.search_txtField.text!)
            }
            else
            {
                self.search_txtField.text = ""
                self.getFullDataStr = "fetchAllData"
                if  self.tabsBtnStr == "nearBy"
                {
                    self.currentPageNearBy = 1
                    self.nearByFeedsArray = []
                    self.getNearByConfessionListing(pageValue: self.currentPageNearBy,searchText: self.search_txtField.text!, completion: { (responseBool) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        if (responseBool == true)
                        {
                            DispatchQueue.main.async {
                                self.noNearByView.isHidden = true
                                self.nearByConfessions_tableView.isHidden = false
                                self.nearByConfessions_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                        else
                        {
                            self.nearByFeedsArray = []
                            self.noNearBy_Lbl.text = "No nearby confessions found"
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.nearByConfessions_tableView.isHidden = false
                            self.noNearByView.isHidden = false
                            
                            DispatchQueue.main.async {
                                self.nearByConfessions_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                    })
                }
                else if  self.tabsBtnStr == "popular"
                {
                    self.currentPagePopular = 1
                    self.popularFeedsArray = []
                    self.getPopularConfessionListing(pageValue: self.currentPagePopular,searchText: self.search_txtField.text!, completion: { (response) in
                    
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (response == true)
                        {
                            DispatchQueue.main.async {
                                self.noPopularView.isHidden = true
                                self.popularConfessions_tableView.isHidden = false
                                self.popularConfessions_tableView.reloadData()
                                self.refreshControl_popularTableView.endRefreshing()
                            }
                        }
                        else
                        {
                            self.popularFeedsArray = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.popularConfessions_tableView.isHidden = false
                            self.noPopularView.isHidden = false
                            DispatchQueue.main.async {
                                self.popularConfessions_tableView.reloadData()
                                self.refreshControl_popularTableView.endRefreshing()
                            }
                        }
                    })
                }
                else if  self.tabsBtnStr == "latest"
                {
                    self.currentPageLatest = 1
                    self.latestFeedsArray = []
                    self.latestFeedsCopyArray = []
                    self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (response == true)
                        {
                            DispatchQueue.main.async {
                                self.noLatestView.isHidden = true
                                self.latestConfessions_tableView.isHidden = false
                                self.latestConfessions_tableView.reloadData()
                                self.refreshControl_latestTableView.endRefreshing()
                            }
                        }
                        else
                        {
                            self.latestFeedsArray = []
                            self.latestFeedsCopyArray = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.latestConfessions_tableView.isHidden = false
                            self.noLatestView.isHidden = false
                            DispatchQueue.main.async {
                                self.latestConfessions_tableView.reloadData()
                                self.refreshControl_latestTableView.endRefreshing()
                            }
                        }
                    })
                }
            }
        }
    }
    
    
    
    // MARK: - ****** viewWillAppear ******
    override func viewWillAppear(_ animated: Bool)
    {
        self.getFullDataStr = "fetchAllData"
        self.showFullView_Latest.isHidden = true
        self.showFullView_Popular.isHidden = true
        self.showFullImageView.isHidden = true
        self.indexPathSelected_NearBy = IndexPath()
        var frame: CGPoint = self.statusMainScrollView.contentOffset
        frame.x = self.statusMainScrollView.frame.size.width * CGFloat(100-100);
        self.nearBy_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.nearByUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.popular_lbl.textColor =  UIColor.lightGray
        self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
        self.latest_lbl.textColor =  UIColor.lightGray
        self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
//        self.statusMainScrollView.setContentOffset(frame, animated: true)
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
    }
    
    func notifyOnUpdate()
    {
        self.tabsBtnStr = "nearBy"
        self.showFullView_Latest.isHidden = true
        self.showFullView_Popular.isHidden = true
        self.showFullImageView.isHidden = true
        self.indexPathSelected_NearBy = IndexPath()
        var frame: CGPoint = self.statusMainScrollView.contentOffset
        frame.x = self.statusMainScrollView.frame.size.width * CGFloat(100-100);
        self.tabsBtnStr = "nearBy"
        self.nearBy_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.nearByUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.popular_lbl.textColor =  UIColor.lightGray
        self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
        self.latest_lbl.textColor =  UIColor.lightGray
        self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
        self.statusMainScrollView.setContentOffset(frame, animated: true)
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
                if (self.tabsBtnStr == "nearBy")
                {
                    self.getFullDataStr = "fetchAllData"
                     self.currentPageNearBy = 1
                     self.nearByFeedsArray = []
                    self.getNearByConfessionListing(pageValue:  self.currentPageNearBy,searchText: self.search_txtField.text!, completion: { (responseBool) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        if (responseBool == true)
                        {
                            DispatchQueue.main.async {
                                self.noNearByView.isHidden = true
                                self.nearByConfessions_tableView.isHidden = false
                                self.nearByConfessions_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                        else
                        {
                            self.nearByFeedsArray = []
                            self.noNearBy_Lbl.text = "No nearby confessions found"
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.nearByConfessions_tableView.isHidden = false
                            self.noNearByView.isHidden = false
                            
                            DispatchQueue.main.async {
                                self.nearByConfessions_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                    })
                }
                else if (self.tabsBtnStr == "popular")
                {
                    self.currentPagePopular = 1
                    self.popularFeedsArray = []
                    self.getPopularConfessionListing(pageValue: self.currentPagePopular, searchText: self.search_txtField.text!, completion: { (response) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (response == true)
                        {
                            DispatchQueue.main.async {
                                self.noPopularView.isHidden = true
                                self.popularConfessions_tableView.isHidden = false
                                self.popularConfessions_tableView.reloadData()
                                self.refreshControl_popularTableView.endRefreshing()
                            }
                        }
                        else
                        {
                            self.popularFeedsArray = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.popularConfessions_tableView.isHidden = false
                            self.noPopularView.isHidden = false
                            DispatchQueue.main.async {
                                self.popularConfessions_tableView.reloadData()
                                self.refreshControl_popularTableView.endRefreshing()
                            }
                        }
                    })
                }
                else if (self.tabsBtnStr == "latest")
                {
                    self.currentPageLatest = 1
                    self.latestFeedsArray = []
                    self.latestFeedsCopyArray = []
                    self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (response == true)
                        {
                            DispatchQueue.main.async {
                                self.noLatestView.isHidden = true
                                self.latestConfessions_tableView.isHidden = false
                                self.latestConfessions_tableView.reloadData()
                                self.refreshControl_latestTableView.endRefreshing()
                            }
                        }
                        else
                        {
                            self.latestFeedsArray = []
                            self.latestFeedsCopyArray = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.latestConfessions_tableView.isHidden = false
                            self.noLatestView.isHidden = false
                            DispatchQueue.main.async {
                                self.latestConfessions_tableView.reloadData()
                                self.refreshControl_latestTableView.endRefreshing()
                            }
                        }
                    })
                }
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    
    // MARK: - ****** ViewDidDisAppear ******
    override func viewDidDisappear(_ animated: Bool)
    {
      
    }

    
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        print("scrollView.contentOffset = \(scrollView.contentOffset.y)")
    }
    
    
    // MARK: - ****** WiSayPosts_btnAction. ******
    @IBAction func WiSayPosts_btnAction(_ sender: UIButton) {
        self.search_txtField.text = ""
        var frame: CGPoint = self.statusMainScrollView.contentOffset
        frame.x = self.statusMainScrollView.frame.size.width * CGFloat(sender.tag-100);
        if sender.tag == 100
        {
            self.tabsBtnStr = "nearBy"
            self.indexPathSelected_NearBy = IndexPath()
            self.nearBy_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            self.nearByUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            self.popular_lbl.textColor =  UIColor.lightGray
            self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
            self.latest_lbl.textColor =  UIColor.lightGray
            self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
            
        }
        else if sender.tag == 101
        {
            self.tabsBtnStr = "popular"
            self.indexPathSelected_popular = IndexPath()
            self.popular_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            self.popularUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            self.nearBy_lbl.textColor =  UIColor.lightGray
            self.nearByUnderline_lbl.backgroundColor =  UIColor.lightGray
            self.latest_lbl.textColor =  UIColor.lightGray
            self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
        }
        else if sender.tag == 102
        {
            self.tabsBtnStr = "latest"
            self.indexPathSelected_Latest = IndexPath()
            self.latest_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            self.latestUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            self.nearBy_lbl.textColor =  UIColor.lightGray
            self.nearByUnderline_lbl.backgroundColor =  UIColor.lightGray
            self.popular_lbl.textColor =  UIColor.lightGray
            self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
        }
        self.statusMainScrollView.setContentOffset(frame, animated: true)
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):

            if self.tabsBtnStr == "nearBy"
            {
                self.getFullDataStr = "fetchAllData"
                self.currentPageNearBy = 1
                 self.nearByFeedsArray = []
                self.getNearByConfessionListing(pageValue:  self.currentPageNearBy, searchText: self.search_txtField.text!, completion: { (responseBool) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    if (responseBool == true)
                    {
                        DispatchQueue.main.async {
                            self.noNearByView.isHidden = true
                            self.nearByConfessions_tableView.isHidden = false
                            self.nearByConfessions_tableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else
                    {
                        self.nearByFeedsArray = []
                        self.noNearBy_Lbl.text = "No nearby confessions found"
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.nearByConfessions_tableView.isHidden = false
                        self.noNearByView.isHidden = false
                        
                        DispatchQueue.main.async {
                            self.nearByConfessions_tableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                })
            }
            else if  self.tabsBtnStr == "popular"
            {
                self.currentPagePopular = 1
                self.popularFeedsArray = []
                self.getPopularConfessionListing(pageValue: self.currentPagePopular, searchText: self.search_txtField.text!, completion: { (response) in
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    if (response == true)
                    {
                        DispatchQueue.main.async {
                            self.noPopularView.isHidden = true
                            self.popularConfessions_tableView.isHidden = false
                            self.popularConfessions_tableView.reloadData()
                            self.refreshControl_popularTableView.endRefreshing()
                        }
                    }
                    else
                    {
                        self.popularFeedsArray = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.popularConfessions_tableView.isHidden = false
                        self.noPopularView.isHidden = false
                        DispatchQueue.main.async {
                            self.popularConfessions_tableView.reloadData()
                            self.refreshControl_popularTableView.endRefreshing()
                        }
                    }
                })
            }
            else if self.tabsBtnStr == "latest"
            {
                self.currentPageLatest = 1
                self.latestFeedsArray = []
                self.latestFeedsCopyArray = []
                self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    if (response == true)
                    {
                        DispatchQueue.main.async {
                            self.noLatestView.isHidden = true
                            self.latestConfessions_tableView.isHidden = false
                            self.latestConfessions_tableView.reloadData()
                            self.refreshControl_latestTableView.endRefreshing()
                        }
                    }
                    else
                    {
                        self.latestFeedsArray = []
                        self.latestFeedsCopyArray = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.latestConfessions_tableView.isHidden = false
                        self.noLatestView.isHidden = false
                        DispatchQueue.main.async {
                            self.latestConfessions_tableView.reloadData()
                            self.refreshControl_latestTableView.endRefreshing()
                        }
                    }
                })
            }
        }
    }
    
    // MARK: - ****** createNewConfession_btnAction. ******
        @IBAction func createNewConfession_btnAction(_ sender: Any) {
            self.search_txtField.resignFirstResponder()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiCreateConfessionViewController") as! WiCreateConfessionViewController
            homeScreen.postTypeStr = "CreateNewPost"
            homeScreen.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
            homeScreen.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
            homeScreen.currentLocationFromHomePage = self.currentLocationFromHomePage
            self.navigationController?.pushViewController(homeScreen, animated: true)
    }
    
    // MARK: - ****** UIScrollView Delegate ******
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        self.lastLocation.x = self.statusMainScrollView.contentOffset.x
        self.lastLocation.y = self.statusMainScrollView.contentOffset.y
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        //self.statusMainScrollView
        let translation = self.statusMainScrollView.panGestureRecognizer.translation(in: self.statusMainScrollView.superview)
        
        let pageWidth:CGFloat = self.statusMainScrollView.frame.width
        let currentPage:CGFloat = floor((self.statusMainScrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        
        if translation.y > 0 {
            // swipes from top to bottom of screen -> down
            print("okkk")
        } else {
            // swipes from bottom to top of screen -> up
            print("okkk 111")
            
            if currentPage == 0.0
            {
                if (self.nearByConfessions_tableView.contentOffset.y >= (self.nearByConfessions_tableView.contentSize.height - self.nearByConfessions_tableView.frame.size.height) ) {
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                        
                        if self.currentPageNearBy < self.totalPageNearby {
                            let bottomEdge: Float = Float(self.nearByConfessions_tableView.contentOffset.y + nearByConfessions_tableView.frame.size.height)
                            if bottomEdge >= Float(nearByConfessions_tableView.contentSize.height)  {
                                
                                print("we are at end of table")
                                
                                 self.currentPageNearBy = self.currentPageNearBy + 1
                                
                                self.getFullDataStr = "fetchAllData"
                                
                                self.getNearByConfessionListing(pageValue: self.currentPageNearBy,searchText: self.search_txtField.text!, completion: { (responseBool) in
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    if (responseBool == true)
                                    {
                                        DispatchQueue.main.async {
                                            self.noNearByView.isHidden = true
                                            self.nearByConfessions_tableView.isHidden = false
                                            self.nearByConfessions_tableView.reloadData()
                                            self.refreshControl.endRefreshing()
                                        }
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.nearByConfessions_tableView.isHidden = false
                                        self.noNearByView.isHidden = true
                                        
                                        DispatchQueue.main.async {
                                            self.nearByConfessions_tableView.reloadData()
                                            self.refreshControl.endRefreshing()
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            }
            else if currentPage == 1.0
            {
                if (self.popularConfessions_tableView.contentOffset.y >= (self.popularConfessions_tableView.contentSize.height - self.popularConfessions_tableView.frame.size.height) ) {
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                        
                        if self.currentPagePopular < self.TotalPagePopular {
                            
                            let bottomEdge: Float = Float(self.popularConfessions_tableView.contentOffset.y + popularConfessions_tableView.frame.size.height)
                            if bottomEdge >= Float(popularConfessions_tableView.contentSize.height)  {
                                
                                print("we are at end of table")
                                
                                self.currentPagePopular = self.currentPagePopular + 1
                                
                                self.getFullDataStr = "fetchAllData"
                                
                                self.getPopularConfessionListing(pageValue: self.currentPagePopular, searchText: self.search_txtField.text!, completion: { (response) in
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    
                                    if (response == true)
                                    {
                                        DispatchQueue.main.async {
                                            self.noPopularView.isHidden = true
                                            self.popularConfessions_tableView.isHidden = false
                                            self.popularConfessions_tableView.reloadData()
                                            self.refreshControl_popularTableView.endRefreshing()
                                        }
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.popularConfessions_tableView.isHidden = false
                                        self.noPopularView.isHidden = true
                                        DispatchQueue.main.async {
                                            self.popularConfessions_tableView.reloadData()
                                            self.refreshControl_popularTableView.endRefreshing()
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            }
            else if currentPage == 2.0
            {
                if (self.latestConfessions_tableView.contentOffset.y >= (self.latestConfessions_tableView.contentSize.height - self.latestConfessions_tableView.frame.size.height) ) {
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                        
                        if self.currentPageLatest < self.totalPageLatest {
                            let bottomEdge: Float = Float(self.latestConfessions_tableView.contentOffset.y + latestConfessions_tableView.frame.size.height)
                            if bottomEdge >= Float(latestConfessions_tableView.contentSize.height)  {
                                
                                print("we are at end of table")
                                
                                self.currentPageLatest = self.currentPageLatest + 1
                                
                                self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    if (response == true)
                                    {
                                        DispatchQueue.main.async {
                                            self.noLatestView.isHidden = true
                                            self.latestConfessions_tableView.isHidden = false
                                            self.latestConfessions_tableView.reloadData()
                                            self.refreshControl_latestTableView.endRefreshing()
                                        }
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.latestConfessions_tableView.isHidden = false
                                        self.noLatestView.isHidden = true
                                        DispatchQueue.main.async {
                                            self.latestConfessions_tableView.reloadData()
                                            self.refreshControl_latestTableView.endRefreshing()
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
       
        if currentPage == 0.0
        {
            self.tabsBtnStr = "nearBy"
            self.indexPathSelected_NearBy = IndexPath()
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.nearBy_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.nearByUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.popular_lbl.textColor =  UIColor.lightGray
                self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
                self.latest_lbl.textColor =  UIColor.lightGray
                self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
            }
        }
        else if currentPage == 1.0
        {
            self.tabsBtnStr = "popular"
            self.indexPathSelected_popular = IndexPath()
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.popular_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.popularUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.nearBy_lbl.textColor =  UIColor.lightGray
                self.nearByUnderline_lbl.backgroundColor =  UIColor.lightGray
                self.latest_lbl.textColor =  UIColor.lightGray
                self.latestUnderline_lbl.backgroundColor =  UIColor.lightGray
            }
        }
        else if currentPage == 2.0
        {
            self.tabsBtnStr = "latest"
            self.indexPathSelected_Latest = IndexPath()
            DispatchQueue.main.asyncAfter(deadline: .now() ) {
                self.latest_lbl.textColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.latestUnderline_lbl.backgroundColor =  UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.nearBy_lbl.textColor =  UIColor.lightGray
                self.nearByUnderline_lbl.backgroundColor =  UIColor.lightGray
                self.popular_lbl.textColor =  UIColor.lightGray
                self.popularUnderline_lbl.backgroundColor =  UIColor.lightGray
            }
        }
        
        if Int(self.lastLocation.x) <  Int(self.statusMainScrollView.contentOffset.x) {
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                print("Not connected")
             
            case .online(.wwan) , .online(.wiFi):
                self.search_txtField.text = ""
                self.getConfessionsDataOnScroll()
            }
        }
        else if Int(self.lastLocation.x) > Int(self.statusMainScrollView.contentOffset.x) {
            // moved left
            print("left")
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                print("Not connected")
              
            case .online(.wwan) , .online(.wiFi):
                self.search_txtField.text = ""
                self.getConfessionsDataOnScroll()
            }
        }
        else if Int(self.lastLocation.y) < Int(self.statusMainScrollView.contentOffset.y) {
            print("up")
        }
        else if Int(self.lastLocation.y) > Int(self.statusMainScrollView.contentOffset.y) {
            print("down")
        }
    }
    
    //MARK:- ****** backBtnAction ******
    @IBAction func backBtnAction(_ sender: Any) {
        self.search_txtField.resignFirstResponder()
        if appDelegatePush == "fromAppDelegate"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        else
        {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    func getConfessionsDataOnScroll()
    {
        if (self.tabsBtnStr == "nearBy")
        {
            self.getFullDataStr = "fetchAllData"
            self.currentPageNearBy = 1
            self.nearByFeedsArray = []
            self.getNearByConfessionListing(pageValue: self.currentPageNearBy,searchText: self.search_txtField.text!, completion: { (responseBool) in
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                if (responseBool == true)
                {
                    DispatchQueue.main.async {
                        self.noNearByView.isHidden = true
                        self.nearByConfessions_tableView.isHidden = false
                        self.nearByConfessions_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.nearByFeedsArray = []
                    self.noNearBy_Lbl.text = "No nearby confessions found"
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.nearByConfessions_tableView.isHidden = false
                    self.noNearByView.isHidden = false
                    
                    DispatchQueue.main.async {
                        self.nearByConfessions_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
            })
        }
        else if (self.tabsBtnStr == "popular")
        {
            self.currentPagePopular = 1
            self.popularFeedsArray = []
            self.getPopularConfessionListing(pageValue: self.currentPagePopular, searchText: self.search_txtField.text!, completion: { (response) in
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                
                if (response == true)
                {
                    DispatchQueue.main.async {
                        self.noPopularView.isHidden = true
                        self.popularConfessions_tableView.isHidden = false
                        self.popularConfessions_tableView.reloadData()
                        self.refreshControl_popularTableView.endRefreshing()
                    }
                }
                else
                {
                    self.popularFeedsArray = []
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.popularConfessions_tableView.isHidden = false
                    self.noPopularView.isHidden = false
                    DispatchQueue.main.async {
                        self.popularConfessions_tableView.reloadData()
                        self.refreshControl_popularTableView.endRefreshing()
                    }
                }
            })
        }
        else if (self.tabsBtnStr == "latest")
        {
            self.currentPageLatest = 1
            self.latestFeedsArray = []
            self.latestFeedsCopyArray = []
            self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                if (response == true)
                {
                    DispatchQueue.main.async {
                        self.noLatestView.isHidden = true
                        self.latestConfessions_tableView.isHidden = false
                        self.latestConfessions_tableView.reloadData()
                        self.refreshControl_latestTableView.endRefreshing()
                    }
                }
                else
                {
                    self.latestFeedsArray = []
                    self.latestFeedsCopyArray = []
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.latestConfessions_tableView.isHidden = false
                    self.noLatestView.isHidden = false
                    DispatchQueue.main.async {
                        self.latestConfessions_tableView.reloadData()
                        self.refreshControl_latestTableView.endRefreshing()
                    }
                }
            })
        }
    }
    
    
    
    // MARK: - ****** UITableView Delegate & Data Source Methods. ******
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       if tableView == self.nearByConfessions_tableView
       {
             return self.nearByFeedsArray.count
       }
       else if tableView == self.popularConfessions_tableView
       {
            return self.popularFeedsArray.count
        }
        else
       {
            return self.latestFeedsArray.count
       }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let wrapperView = UIView()
        return wrapperView //wrapperViewForHeadingButtons
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.nearByConfessions_tableView
        {
//            self.nearByConfessions_tableView.separatorColor = UIColor.clear
            
            if (self.nearByFeedsArray.count <= 0)
            {
                let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath) as! CreditPackagesTableViewCell
                return creditsCell
            }
           
            if (self.nearByFeedsArray[indexPath.row].postedImage != "")
            {
                let imageURL = self.nearByFeedsArray[indexPath.row].postedImage
                if (imageURL.contains("jpg"))
                {
                    let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath) as! CreditPackagesTableViewCell
                    
                    if (self.nearByFeedsArray.count <= 0)
                    {
                        return creditsCell
                    }
                    
                    creditsCell.animatedGifImage.isHidden = true
                    creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                    creditsCell.tag = indexPath.row
                    creditsCell.confessionLikeIcon.tag = indexPath.row
                    creditsCell.animatedGifImage.tag = indexPath.row
                    creditsCell.activityIndicator_nearBy.tag = indexPath.row
                    creditsCell.moodTypeIcon.tag = indexPath.row
                    
                    DispatchQueue.main.async {
                        
                        if self.getFullDataStr == "fetchAllData"
                        {
                            if (self.nearByFeedsArray.count > 0)
                            {
                                if (self.nearByFeedsArray[indexPath.row].likedByMeStatus == "yes")
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                }
                            }
                        }
                        else
                        {
                            if (self.nearByIntArr.count > 0)
                            {
                                if self.nearByIntArr.contains(indexPath.row)
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                }
                            }
                            
                        }
                   }
                    
                    var endDateForConfession = String(self.nearByFeedsArray[indexPath.row].totalTime)
                    endDateForConfession = endDateForConfession! + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    let datee = dateFormatter.date(from: endDateForConfession!)!
       
                    creditsCell.decreaseTimerLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                     creditsCell.decreaseTimerLbl.adjustsFontSizeToFitWidth = true
                     
                     let genderStr = self.nearByFeedsArray[indexPath.row].gender
                     var gender = String()
                     if genderStr == "1"
                     {
                      gender = "Male"
                     }
                     else
                     {
                       gender = "Female"
                     }
                    
                    creditsCell.moodTypeIcon.frame.size.width = 45
                    creditsCell.moodTypeIcon.frame.size.height = 45
                    creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                    creditsCell.moodTypeIcon.clipsToBounds = true
                    creditsCell.moodTypeIcon.layer.masksToBounds = true
                    creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                    creditsCell.moodTypeIcon.image = nil
                   
                    if (self.nearByFeedsArray[indexPath.row].typeOfConfession == "anonymous")
                    {
                        creditsCell.userGenderAgeLbl.text = gender + ", " + self.nearByFeedsArray[indexPath.row].age
                        
                        creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.nearByFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell.moodTypeIcon.image = image
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                            creditsCell.moodTypeIcon.addSubview(blurEffectView)
                        }
                   
                    }
                    else
                    {
                        creditsCell.userGenderAgeLbl.text = self.nearByFeedsArray[indexPath.row].name
                        creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.nearByFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell.moodTypeIcon.image = image
                            }
                            
                            for subview in creditsCell.moodTypeIcon.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                        }
                    }

                    let inputString: NSString = self.nearByFeedsArray[indexPath.row].statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                    
                    creditsCell.usersStatus_TxtView.text = modifiedString
                    
                  /*  DispatchQueue.main.async {
                        if (creditsCell.usersStatus_TxtView.text != "")
                        {
                            let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                            creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            var newFrame = creditsCell.usersStatus_TxtView.frame
                            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                            
                            creditsCell.animatedImageHeight.constant = newSize.height
                        }
                        else{
                            creditsCell.animatedImageHeight.constant = 0
                        }
                    }*/
                    
                    
                    
                    creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.nearByFeedsArray[indexPath.row].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell.activityIndicator_nearBy.isHidden = true
                        
                        creditsCell.animatedGifImage.isHidden = false
                        
//                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
//                        creditsCell.animatedGifImage.layer.masksToBounds = false
//                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                        creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell.animatedGifImage.clipsToBounds = true
                    }
                    
                     if self.nearByFeedsArray[indexPath.row].likesCount == "0"
                     {
                       creditsCell.totalLikesCountLbl.text = ""
                     }
                     else
                     {
                       creditsCell.totalLikesCountLbl.text = String(self.nearByFeedsArray[indexPath.row].likesCount)
                     }
                    
                    if (self.nearByFeedsArray[indexPath.row].location != "")
                    {
                        creditsCell.locationIcon.isHidden = false
                        creditsCell.userNameLbl_Y_Constraint.constant = 5
                    }else{
                        creditsCell.locationIcon.isHidden = true
                        creditsCell.userNameLbl_Y_Constraint.constant = (creditsCell.moodTypeIcon.frame.origin.y + creditsCell.moodTypeIcon.frame.size.height)/3
                        //creditsCell.userNameLbl_Y_Constraint.constant + 8
                    }
                    
                     creditsCell.userLocationLbl.text = self.nearByFeedsArray[indexPath.row].location
                     creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                    
                     if self.nearByFeedsArray[indexPath.row].replyCount == "0"
                     {
                       creditsCell.totalReplyCountLbl.text = ""
                     }
                     else
                     {
                        creditsCell.totalReplyCountLbl.text = self.nearByFeedsArray[indexPath.row].replyCount
                     }
                    
                    creditsCell.confessionLikesBtn.tag = indexPath.row
                    creditsCell.confessionLikesBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionLikesNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewProfileBtn.tag = indexPath.row
                    creditsCell.viewProfileBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewProfileNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionReplyBtn.tag = indexPath.row
                    creditsCell.confessionReplyBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionReplyNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionMessageBtn.tag = indexPath.row
                    creditsCell.confessionMessageBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionMessageNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.dropDownBtn.tag = indexPath.row
                    creditsCell.dropDownBtn.addTarget(self, action: #selector(WiConfessionsViewController.DropDownNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewFullImageBtn.tag = indexPath.row
                    creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewFullSizeImageACtion_nearBy(sender:)), for: UIControlEvents.touchUpInside)
                    
                    return creditsCell
                }
                else
                {
                    let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell1", for: indexPath) as! CreditPackagesTableViewCell
                    
                    creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                    creditsCell.tag = indexPath.row
                    creditsCell.confessionLikeIcon.tag = indexPath.row
                    creditsCell.animatedGifImage.tag = indexPath.row
                    creditsCell.animatedGifImage.isHidden = true
                    creditsCell.activityIndicatorGif_nearBy.tag = indexPath.row
                    creditsCell.moodTypeIcon.tag = indexPath.row
                    
                    if (self.nearByFeedsArray.count <= 0)
                    {
                        return creditsCell
                    }
                    
                    DispatchQueue.main.async {
                        
                        if self.getFullDataStr == "fetchAllData"
                        {
                            if (self.nearByFeedsArray.count > 0)
                            {
                                if (self.nearByFeedsArray[indexPath.row].likedByMeStatus == "yes")
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                }
                            }
                        }
                        else
                        {
                            if (self.nearByIntArr.count > 0)
                            {
                                if self.nearByIntArr.contains(indexPath.row)
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                }
                            }
                        }
                    }
                    
                    var endDateForConfession = String(self.nearByFeedsArray[indexPath.row].totalTime)
                    endDateForConfession = endDateForConfession! + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    let datee = dateFormatter.date(from: endDateForConfession!)!
                    
//                    let dateStr = dateFormatter.string(from: datee as Date)
                   
                    creditsCell.decreaseTimerLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                    creditsCell.decreaseTimerLbl.adjustsFontSizeToFitWidth = true
                    
                    let genderStr = self.nearByFeedsArray[indexPath.row].gender
                    var gender = String()
                    if genderStr == "1"
                    {
                        gender = "Male"
                    }
                    else
                    {
                        gender = "Female"
                    }
                    
                    creditsCell.moodTypeIcon.frame.size.width = 45
                    creditsCell.moodTypeIcon.frame.size.height = 45
                    creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                    creditsCell.moodTypeIcon.clipsToBounds = true
                    creditsCell.moodTypeIcon.layer.masksToBounds = true
                    creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                    creditsCell.moodTypeIcon.image = nil
                    
                    if (self.nearByFeedsArray[indexPath.row].typeOfConfession == "anonymous")
                    {
                        creditsCell.userGenderAgeLbl.text = gender + ", " + self.nearByFeedsArray[indexPath.row].age
                        
                        creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.nearByFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell.moodTypeIcon.image = image
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                            creditsCell.moodTypeIcon.addSubview(blurEffectView)
                        }
                    }
                    else
                    {
                        creditsCell.userGenderAgeLbl.text = self.nearByFeedsArray[indexPath.row].name
                        
                        creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.nearByFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell.moodTypeIcon.image = image
                            }
                            
                            for subview in creditsCell.moodTypeIcon.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                        }
                    }
                    
                    
                    let inputString: NSString = self.nearByFeedsArray[indexPath.row].statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                    creditsCell.usersStatus_TxtView.text = modifiedString
                    
                   /* DispatchQueue.main.async {
                        if (creditsCell.usersStatus_TxtView.text != "")
                        {
                            let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                            creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            var newFrame = creditsCell.usersStatus_TxtView.frame
                            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                            
                            creditsCell.animatedGifHeight.constant = newSize.height
                        }
                        else{
                            creditsCell.animatedGifHeight.constant = 0
                        }
                    }*/
                    
                    DispatchQueue.main.async {
                        
                        creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.nearByFeedsArray[indexPath.row].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                            
                            creditsCell.activityIndicatorGif_nearBy.isHidden = true
                            creditsCell.animatedGifImage.isHidden = false
//                            creditsCell.animatedGifImage.layer.borderWidth = 2.0
//                            creditsCell.animatedGifImage.layer.masksToBounds = false
//                            creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                            
                            creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                            creditsCell.animatedGifImage.clipsToBounds = true
                        }
                    }

                    
                    if self.nearByFeedsArray[indexPath.row].likesCount == "0"
                    {
                        creditsCell.totalLikesCountLbl.text = ""
                    }
                    else
                    {
                        creditsCell.totalLikesCountLbl.text = String(self.nearByFeedsArray[indexPath.row].likesCount)
                    }
                    
                    
                    if (self.nearByFeedsArray[indexPath.row].location != "")
                    {
                        creditsCell.locationIcon.isHidden = false
                        creditsCell.userNameLbl_Y_Constraint.constant = 5
                    }else{
                        creditsCell.locationIcon.isHidden = true
                        creditsCell.userNameLbl_Y_Constraint.constant = (creditsCell.moodTypeIcon.frame.origin.y + creditsCell.moodTypeIcon.frame.size.height)/3
                        //creditsCell.userNameLbl_Y_Constraint.constant + 8
                    }
                   
                    creditsCell.userLocationLbl.text = self.nearByFeedsArray[indexPath.row].location
                    creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                
                    if  self.nearByFeedsArray[indexPath.row].replyCount == "0"
                    {
                        creditsCell.totalReplyCountLbl.text = ""
                    }
                    else
                    {
                        creditsCell.totalReplyCountLbl.text = self.nearByFeedsArray[indexPath.row].replyCount
                    }
                    
                    creditsCell.confessionLikesBtn.tag = indexPath.row
                    creditsCell.confessionLikesBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionLikesNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionReplyBtn.tag = indexPath.row
                    creditsCell.confessionReplyBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionReplyNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionMessageBtn.tag = indexPath.row
                    creditsCell.confessionMessageBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionMessageNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.dropDownBtn.tag = indexPath.row
                    creditsCell.dropDownBtn.addTarget(self, action: #selector(WiConfessionsViewController.DropDownNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewProfileBtn.tag = indexPath.row
                    creditsCell.viewProfileBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewProfileNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewFullImageBtn.tag = indexPath.row
                    creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewFullSizeImageACtion_nearBy(sender:)), for: UIControlEvents.touchUpInside)
                    
                    return creditsCell
                }
            }
            else
            {
                let onlineUsersCell = tableView.dequeueReusableCell(withIdentifier: "statusOnlyCell", for: indexPath) as! ConfessionStatusTableViewCell
                
                onlineUsersCell.selectionStyle = UITableViewCellSelectionStyle.none
                onlineUsersCell.confessionLikeIcon.tag = indexPath.row
                onlineUsersCell.moodIcon.tag = indexPath.row
                
                if (self.nearByFeedsArray.count <= 0)
                {
                    return onlineUsersCell
                }
                
                 DispatchQueue.main.async {
                    
                    if self.getFullDataStr == "fetchAllData"
                    {
                        if (self.nearByFeedsArray.count > 0)
                        {
                            if (self.nearByFeedsArray[indexPath.row].likedByMeStatus == "yes")
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                    }
                    else
                    {
                        if (self.nearByIntArr.count > 0)
                        {
                            if self.nearByIntArr.contains(indexPath.row)
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                    }
                 }
                 
                 let genderStr = self.nearByFeedsArray[indexPath.row].gender
                 var gender = String()
                 if genderStr == "1"
                 {
                   gender = "Male"
                 }
                 else
                 {
                   gender = "Female"
                 }
                
                onlineUsersCell.moodIcon.frame.size.width = 45
                onlineUsersCell.moodIcon.frame.size.height = 45
                onlineUsersCell.moodIcon.layer.cornerRadius = onlineUsersCell.moodIcon.frame.size.height/2
                onlineUsersCell.moodIcon.clipsToBounds = true
                onlineUsersCell.moodIcon.layer.masksToBounds = true
                onlineUsersCell.moodIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.nearByFeedsArray[indexPath.row].typeOfConfession == "anonymous")
                {
                    onlineUsersCell.userDetailsLbl.text = gender + ", " + self.nearByFeedsArray[indexPath.row].age
                    
                    onlineUsersCell.moodIcon?.sd_setImage(with: URL(string: self.nearByFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            onlineUsersCell.moodIcon.image = UIImage(named: "Default Icon")
                        } else {
                            onlineUsersCell.moodIcon.image = image
                        }
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = onlineUsersCell.moodIcon.bounds
                        onlineUsersCell.moodIcon.addSubview(blurEffectView)
                    }
        
                }
                else
                {
                    onlineUsersCell.userDetailsLbl.text = self.nearByFeedsArray[indexPath.row].name
            
                    
                    onlineUsersCell.moodIcon?.sd_setImage(with: URL(string: self.nearByFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            onlineUsersCell.moodIcon.image = UIImage(named: "Default Icon")
                        } else {
                            onlineUsersCell.moodIcon.image = image
                        }
                        
                        for subview in onlineUsersCell.moodIcon.subviews
                        {
                            if subview.isKind(of: UIVisualEffectView.self)
                            {
                                subview.removeFromSuperview()
                            }
                        }
                    }
                }
                
                var endDateForConfession = String(self.nearByFeedsArray[indexPath.row].totalTime)
                endDateForConfession = endDateForConfession! + " UTC"
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone.current
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                let datee = dateFormatter.date(from: endDateForConfession!)!
                
//                let dateStr = dateFormatter.string(from: datee as Date)
                
                onlineUsersCell.decreasePostTimeLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                
                 onlineUsersCell.decreasePostTimeLbl.adjustsFontSizeToFitWidth = true
                 
                 if self.nearByFeedsArray[indexPath.row].likesCount == "0"
                 {
                   onlineUsersCell.likeCountLbl.text = ""
                 }
                 else
                 {
                    onlineUsersCell.likeCountLbl.text = String(self.nearByFeedsArray[indexPath.row].likesCount)
                 }

                 if self.nearByFeedsArray[indexPath.row].replyCount == "0"
                 {
                   onlineUsersCell.replyCountLbl.text = ""
                 }
                 else
                 {
                   onlineUsersCell.replyCountLbl.text = self.nearByFeedsArray[indexPath.row].replyCount
                 }
                
                if (self.nearByFeedsArray[indexPath.row].location != "")
                {
                    onlineUsersCell.locationIcon.isHidden = false
                    onlineUsersCell.userDetailsLbl_Y_Frame.constant = 5
                }else{
                    onlineUsersCell.locationIcon.isHidden = true
                    onlineUsersCell.userDetailsLbl_Y_Frame.constant = (onlineUsersCell.moodIcon.frame.origin.y + onlineUsersCell.moodIcon.frame.size.height)/3
                    // onlineUsersCell.userDetailsLbl_Y_Frame.constant + 8
                }
                
                 onlineUsersCell.userLocationLbl.text = self.nearByFeedsArray[indexPath.row].location
                 onlineUsersCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                
                
                let inputString: NSString = self.nearByFeedsArray[indexPath.row].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                onlineUsersCell.usersStatusTextView.text = modifiedString
                
                onlineUsersCell.postLikesBtn.tag = indexPath.row
                onlineUsersCell.postLikesBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionLikesNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postReplyBtn.tag = indexPath.row
                onlineUsersCell.postReplyBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionReplyNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.viewProfile_btn.tag = indexPath.row
                onlineUsersCell.viewProfile_btn.addTarget(self, action: #selector(WiConfessionsViewController.viewProfileNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postMessageBtn.tag = indexPath.row
                onlineUsersCell.postMessageBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionMessageNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postDropDownBtn.tag = indexPath.row
                onlineUsersCell.postDropDownBtn.addTarget(self, action: #selector(WiConfessionsViewController.DropDownNearBy_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return onlineUsersCell
            }
        }
        else if tableView == self.popularConfessions_tableView
        {
//            self.popularConfessions_tableView.separatorColor = UIColor.clear
            
            if (self.popularFeedsArray.count <= 0)
            {
                let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath) as! CreditPackagesTableViewCell
                return creditsCell
            }
            
            if (self.popularFeedsArray[indexPath.row].postedImage != "")
            {
                let imageURL = self.popularFeedsArray[indexPath.row].postedImage
                if (imageURL.contains("jpg"))
                {
                    let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath)as! CreditPackagesTableViewCell
                    
                    creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                    
                    creditsCell.animatedGifImage.isHidden = true
                    creditsCell.tag = indexPath.row
                    creditsCell.confessionLikeIcon.tag = indexPath.row
                    creditsCell.animatedGifImage.tag = indexPath.row
                    creditsCell.activityIndicator_popular.tag = indexPath.row
                    creditsCell.moodTypeIcon.tag = indexPath.row
                    
                    if (self.popularFeedsArray.count <= 0)
                    {
                        return creditsCell
                    }
                    
                    DispatchQueue.main.async {
                        
                        if self.getFullDataStr == "fetchAllData"
                        {
                            if (self.popularFeedsArray.count > 0)
                            {
                                if (self.popularFeedsArray[indexPath.row].likedByMeStatus == "yes")
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                }
                            }
                        }
                        else{
                            if (self.popularIntArr.count > 0)
                            {
                                if self.popularIntArr.contains(indexPath.row)
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                }
                            }
                        }
                    }
                    
                     let genderStr = self.popularFeedsArray[indexPath.row].gender
                     var gender = String()
                     if genderStr == "1"
                     {
                     gender = "Male"
                     }
                     else
                     {
                     gender = "Female"
                     }
                    
                    creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                    creditsCell.moodTypeIcon.clipsToBounds = true
                    creditsCell.moodTypeIcon.layer.masksToBounds = true
                    creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                    
                    if (self.popularFeedsArray[indexPath.row].typeOfConfession == "anonymous")
                    {
                       creditsCell.userGenderAgeLbl.text = gender + ", " + self.popularFeedsArray[indexPath.row].age
                        
                        creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.popularFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell.moodTypeIcon.image = image
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                            creditsCell.moodTypeIcon.addSubview(blurEffectView)
                        }
                    }
                    else
                    {
                        creditsCell.userGenderAgeLbl.text = self.popularFeedsArray[indexPath.row].name
                        
                        
                        
                        creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.popularFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell.moodTypeIcon.image = image
                            }
                            
                            for subview in creditsCell.moodTypeIcon.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                        }
                    }
                    
                    var endDateForConfession = String(self.popularFeedsArray[indexPath.row].totalTime)
                    endDateForConfession = endDateForConfession! + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    let datee = dateFormatter.date(from: endDateForConfession!)!
                    
                    creditsCell.decreaseTimerLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                     creditsCell.decreaseTimerLbl.adjustsFontSizeToFitWidth = true
                     
                    
                    let inputString: NSString = self.popularFeedsArray[indexPath.row].statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    
                    creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                    creditsCell.usersStatus_TxtView.text = modifiedString
                    
                   /* DispatchQueue.main.async {
                        if (creditsCell.usersStatus_TxtView.text != "")
                        {
                            let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                            creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            var newFrame = creditsCell.usersStatus_TxtView.frame
                            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                            
                            creditsCell.animatedImageHeight.constant = newSize.height
                        }
                        else{
                            creditsCell.animatedImageHeight.constant = 0
                        }
                    }*/
                    
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                     creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.popularFeedsArray[indexPath.row].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell.activityIndicator_popular.isHidden = true
                        
                        creditsCell.animatedGifImage.isHidden = false
//                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
//                        creditsCell.animatedGifImage.layer.masksToBounds = false
//                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                        creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell.animatedGifImage.clipsToBounds = true
                     
                      }
                    })
                    
                     if self.popularFeedsArray[indexPath.row].likesCount == "0"
                     {
                     creditsCell.totalLikesCountLbl.text = ""
                     }
                     else
                     {
                     creditsCell.totalLikesCountLbl.text = String(self.popularFeedsArray[indexPath.row].likesCount)
                     }
                    
                     
                    if  self.popularFeedsArray[indexPath.row].replyCount == "0"
                     {
                       creditsCell.totalReplyCountLbl.text = ""
                     }
                     else
                     {
                       creditsCell.totalReplyCountLbl.text = self.popularFeedsArray[indexPath.row].replyCount
                     }
                    
                    if (self.popularFeedsArray[indexPath.row].location != "")
                    {
                        creditsCell.locationIcon.isHidden = false
                        creditsCell.userNameLbl_Y_Constraint.constant = 5
                    }else{
                        creditsCell.locationIcon.isHidden = true
                        creditsCell.userNameLbl_Y_Constraint.constant = (creditsCell.moodTypeIcon.frame.origin.y + creditsCell.moodTypeIcon.frame.size.height)/3
                    }
                    
                     creditsCell.userLocationLbl.text = self.popularFeedsArray[indexPath.row].location
                     creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                    
                    creditsCell.viewProfileBtn.tag = indexPath.row
                    creditsCell.viewProfileBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewProfilePopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionLikesBtn.tag = indexPath.row
                    creditsCell.confessionLikesBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionLikesPopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionReplyBtn.tag = indexPath.row
                    creditsCell.confessionReplyBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionReplyPopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionMessageBtn.tag = indexPath.row
                    creditsCell.confessionMessageBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionMessagePopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.dropDownBtn.tag = indexPath.row
                    creditsCell.dropDownBtn.addTarget(self, action: #selector(WiConfessionsViewController.DropDownPopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewFullImageBtn.tag = indexPath.row
                    creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewFullSizeImageACtion_popular(sender:)), for: UIControlEvents.touchUpInside)
                    
                    return creditsCell
                }
                else
                {
                    let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell1", for: indexPath)as! CreditPackagesTableViewCell
                    
                    creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                    
                    creditsCell.animatedGifImage.isHidden = true
                    creditsCell.tag = indexPath.row
                    creditsCell.confessionLikeIcon.tag = indexPath.row
                    creditsCell.animatedGifImage.tag = indexPath.row
                    creditsCell.activityIndicatorGif_popular.tag = indexPath.row
                    creditsCell.moodTypeIcon.tag = indexPath.row
                    
                    if (self.popularFeedsArray.count <= 0)
                    {
                        return creditsCell
                    }
                    
                     DispatchQueue.main.async {
                        
                        if self.getFullDataStr == "fetchAllData"
                        {
                            if (self.popularFeedsArray.count > 0)
                            {
                                if (self.popularFeedsArray[indexPath.row].likedByMeStatus == "yes")
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                }
                            }
                            else
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                        else
                        {
                            if (self.popularIntArr.count > 0)
                            {
                                if self.popularIntArr.contains(indexPath.row)
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                }
                            }
                        }
                     }
                     
                     
                     let genderStr = self.popularFeedsArray[indexPath.row].gender
                     var gender = String()
                     if genderStr == "1"
                     {
                     gender = "Male"
                     }
                     else
                     {
                     gender = "Female"
                     }
                     
                    creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                    creditsCell.moodTypeIcon.clipsToBounds = true
                    creditsCell.moodTypeIcon.layer.masksToBounds = true
                    creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                    
                    if (self.popularFeedsArray[indexPath.row].typeOfConfession == "anonymous")
                    {
                        creditsCell.userGenderAgeLbl.text = gender + ", " + self.popularFeedsArray[indexPath.row].age
                        
                        creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.popularFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell.moodTypeIcon.image = image
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                            creditsCell.moodTypeIcon.addSubview(blurEffectView)
                        }
                    }
                    else
                    {
                        creditsCell.userGenderAgeLbl.text = self.popularFeedsArray[indexPath.row].name
                        
                        creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.popularFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell.moodTypeIcon.image = image
                            }
                            
                            for subview in creditsCell.moodTypeIcon.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                        }
                    }
                    
                    var endDateForConfession = String(self.popularFeedsArray[indexPath.row].totalTime)
                    endDateForConfession = endDateForConfession! + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    let datee = dateFormatter.date(from: endDateForConfession!)!
                    
                    creditsCell.decreaseTimerLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                     creditsCell.decreaseTimerLbl.adjustsFontSizeToFitWidth = true
                     
                    let inputString: NSString = self.popularFeedsArray[indexPath.row].statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    
                    creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                    creditsCell.usersStatus_TxtView.text = modifiedString
                    
                  /*  DispatchQueue.main.async {
                        if (creditsCell.usersStatus_TxtView.text != "")
                        {
                            let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                            creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            var newFrame = creditsCell.usersStatus_TxtView.frame
                            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                            
                            creditsCell.animatedGifHeight.constant = newSize.height
                        }
                        else{
                            creditsCell.animatedGifHeight.constant = 0
                        }
                    }*/
                    
                   DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                     creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.popularFeedsArray[indexPath.row].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell.activityIndicatorGif_popular.isHidden = true
                        
                        creditsCell.animatedGifImage.isHidden = false
//                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
//                        creditsCell.animatedGifImage.layer.masksToBounds = false
//                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                        creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell.animatedGifImage.clipsToBounds = true
                     }
                    })
                    
                     if self.popularFeedsArray[indexPath.row].likesCount == "0"
                     {
                     creditsCell.totalLikesCountLbl.text = ""
                     }
                     else
                     {
                     creditsCell.totalLikesCountLbl.text = String(self.popularFeedsArray[indexPath.row].likesCount)
                     }
                    
                     if self.popularFeedsArray[indexPath.row].replyCount == "0"
                     {
                     creditsCell.totalReplyCountLbl.text = ""
                     }
                     else
                     {
                     creditsCell.totalReplyCountLbl.text = self.popularFeedsArray[indexPath.row].replyCount
                     }
                    
                    if (self.popularFeedsArray[indexPath.row].location != "")
                    {
                        creditsCell.locationIcon.isHidden = false
                        creditsCell.userNameLbl_Y_Constraint.constant = 5
                    }else{
                        creditsCell.locationIcon.isHidden = true
                        creditsCell.userNameLbl_Y_Constraint.constant = (creditsCell.moodTypeIcon.frame.origin.y + creditsCell.moodTypeIcon.frame.size.height)/3
                    }
                    
                     creditsCell.userLocationLbl.text = self.popularFeedsArray[indexPath.row].location
                     creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                    
                    creditsCell.confessionLikesBtn.tag = indexPath.row
                    creditsCell.confessionLikesBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionLikesPopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewProfileBtn.tag = indexPath.row
                    creditsCell.viewProfileBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewProfilePopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionReplyBtn.tag = indexPath.row
                    creditsCell.confessionReplyBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionReplyPopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionMessageBtn.tag = indexPath.row
                    creditsCell.confessionMessageBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionMessagePopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.dropDownBtn.tag = indexPath.row
                    creditsCell.dropDownBtn.addTarget(self, action: #selector(WiConfessionsViewController.DropDownPopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewFullImageBtn.tag = indexPath.row
                    creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewFullSizeImageACtion_popular(sender:)), for: UIControlEvents.touchUpInside)
                    
                    return creditsCell
                }
            }
            else
            {
                let onlineUsersCell = tableView.dequeueReusableCell(withIdentifier: "statusOnlyCell", for: indexPath)as! ConfessionStatusTableViewCell
                
                onlineUsersCell.selectionStyle = UITableViewCellSelectionStyle.none
                onlineUsersCell.confessionLikeIcon.tag = indexPath.row
                onlineUsersCell.moodIcon.tag = indexPath.row
                
                if (self.popularFeedsArray.count <= 0)
                {
                    return onlineUsersCell
                }
                
                 DispatchQueue.main.async {
                    
                    if self.getFullDataStr == "fetchAllData"
                    {
                        if (self.popularFeedsArray.count > 0)
                        {
                            if (self.popularFeedsArray[indexPath.row].likedByMeStatus == "yes")
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                //confessionLikeIcon
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                        else
                        {
                            onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                        
                    }
                    else{
                        if (self.popularIntArr.count > 0)
                        {
                            if self.popularIntArr.contains(indexPath.row)
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                    }
                 }
                 
                 let genderStr = self.popularFeedsArray[indexPath.row].gender
                 var gender = String()
                 if genderStr == "1"
                 {
                 gender = "Male"
                 }
                 else
                 {
                 gender = "Female"
                 }
                
                onlineUsersCell.moodIcon.layer.cornerRadius = onlineUsersCell.moodIcon.frame.size.height/2
                onlineUsersCell.moodIcon.clipsToBounds = true
                onlineUsersCell.moodIcon.layer.masksToBounds = true
                onlineUsersCell.moodIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.popularFeedsArray[indexPath.row].typeOfConfession == "anonymous")
                {
                    onlineUsersCell.userDetailsLbl.text = gender + ", " + self.popularFeedsArray[indexPath.row].age
                    
                    onlineUsersCell.moodIcon?.sd_setImage(with: URL(string: self.popularFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            onlineUsersCell.moodIcon.image = UIImage(named: "Default Icon")
                        } else {
                            onlineUsersCell.moodIcon.image = image
                        }
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = onlineUsersCell.moodIcon.bounds
                        onlineUsersCell.moodIcon.addSubview(blurEffectView)
                    }
                }
                else
                {
                    onlineUsersCell.userDetailsLbl.text = self.popularFeedsArray[indexPath.row].name
                    
                   
                    
                    onlineUsersCell.moodIcon?.sd_setImage(with: URL(string: self.popularFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            onlineUsersCell.moodIcon.image = UIImage(named: "Default Icon")
                        } else {
                            onlineUsersCell.moodIcon.image = image
                        }
                        
                        for subview in onlineUsersCell.moodIcon.subviews
                        {
                            if subview.isKind(of: UIVisualEffectView.self)
                            {
                                subview.removeFromSuperview()
                            }
                        }
                    }
                }
                
                 if self.popularFeedsArray[indexPath.row].likesCount == "0"
                 {
                 onlineUsersCell.likeCountLbl.text = ""
                 }
                 else
                 {
                 onlineUsersCell.likeCountLbl.text = String(self.popularFeedsArray[indexPath.row].likesCount)
                 }
                
                 if  self.popularFeedsArray[indexPath.row].replyCount == "0"
                 {
                 onlineUsersCell.replyCountLbl.text = ""
                 }
                 else
                 {
                 onlineUsersCell.replyCountLbl.text = self.popularFeedsArray[indexPath.row].replyCount
                 }
                
                
                if (self.popularFeedsArray[indexPath.row].location != "")
                {
                    onlineUsersCell.locationIcon.isHidden = false
                    onlineUsersCell.userDetailsLbl_Y_Frame.constant = 5
                }else{
                    onlineUsersCell.locationIcon.isHidden = true
                    onlineUsersCell.userDetailsLbl_Y_Frame.constant = (onlineUsersCell.moodIcon.frame.origin.y + onlineUsersCell.moodIcon.frame.size.height)/3
                }
                 
                 onlineUsersCell.userLocationLbl.text = self.popularFeedsArray[indexPath.row].location
                 onlineUsersCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                 
                var endDateForConfession = String(self.popularFeedsArray[indexPath.row].totalTime)
                endDateForConfession = endDateForConfession! + " UTC"
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone.current
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                let datee = dateFormatter.date(from: endDateForConfession!)!
                
                 onlineUsersCell.decreasePostTimeLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                 onlineUsersCell.decreasePostTimeLbl.adjustsFontSizeToFitWidth = true
                
                
                let inputString: NSString = self.popularFeedsArray[indexPath.row].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                onlineUsersCell.usersStatusTextView.text = modifiedString //self.nearByFeedsArray[indexPath.row].statusAdded
                
                onlineUsersCell.postLikesBtn.tag = indexPath.row
                onlineUsersCell.postLikesBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionLikesPopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postReplyBtn.tag = indexPath.row
                onlineUsersCell.postReplyBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionReplyPopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postMessageBtn.tag = indexPath.row
                onlineUsersCell.postMessageBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionMessagePopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.viewProfile_btn.tag = indexPath.row
                onlineUsersCell.viewProfile_btn.addTarget(self, action: #selector(WiConfessionsViewController.viewProfilePopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postDropDownBtn.tag = indexPath.row
                onlineUsersCell.postDropDownBtn.addTarget(self, action: #selector(WiConfessionsViewController.DropDownPopular_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return onlineUsersCell
            }
        }
        else
        {
//            self.latestConfessions_tableView.separatorColor = UIColor.clear
            
            if (self.latestFeedsArray.count <= 0)
            {
                let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath) as! CreditPackagesTableViewCell
                return creditsCell
            }
            
            if (self.latestFeedsArray[indexPath.row].postedImage != "")
            {
                let imageURL = self.latestFeedsArray[indexPath.row].postedImage
                if (imageURL.contains("jpg"))
                {
                    let creditsCell_Latest = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath)as! CreditPackagesTableViewCell
                    
                    creditsCell_Latest.selectionStyle = UITableViewCellSelectionStyle.none
                    
                    creditsCell_Latest.animatedGifImage.isHidden = true
                    creditsCell_Latest.tag = indexPath.row
                    creditsCell_Latest.confessionLikeIcon.tag = indexPath.row
                    creditsCell_Latest.activityIndicator_Latest.tag = indexPath.row
                    creditsCell_Latest.moodTypeIcon.tag = indexPath.row
                    
                    if (self.latestFeedsArray.count <= 0)
                    {
                        return creditsCell_Latest
                    }
                     
                     if self.getFullDataStr == "fetchAllData"
                     {
                        creditsCell_Latest.animatedGifImage.image = nil
                     
                        if (self.latestFeedsArray.count <= 0)
                        {
                            return creditsCell_Latest
                        }
                        
                        if (self.latestFeedsArray.count > 0)
                        {
                            if (self.latestFeedsArray[indexPath.row].likedByMeStatus == "yes")
                            {
                                creditsCell_Latest.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell_Latest.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                        else
                        {
                            creditsCell_Latest.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                     }
                     else
                     {
                        if (self.latestIntArr.count > 0)
                        {
                            if self.latestIntArr.contains(indexPath.row)
                            {
                                creditsCell_Latest.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell_Latest.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                     
                     }
                     
                     let genderStr = self.latestFeedsArray[indexPath.row].gender
                     var gender = String()
                     if genderStr == "1"
                     {
                     gender = "Male"
                     }
                     else
                     {
                     gender = "Female"
                     }
                    
                    creditsCell_Latest.moodTypeIcon.layer.cornerRadius = creditsCell_Latest.moodTypeIcon.frame.size.height/2
                    creditsCell_Latest.moodTypeIcon.clipsToBounds = true
                    creditsCell_Latest.moodTypeIcon.layer.masksToBounds = true
                    creditsCell_Latest.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                    
                    if (self.latestFeedsArray[indexPath.row].typeOfConfession == "anonymous")
                    {
                        creditsCell_Latest.userGenderAgeLbl.text = gender + ", " + self.latestFeedsArray[indexPath.row].age
                        
                        creditsCell_Latest.moodTypeIcon?.sd_setImage(with: URL(string: self.latestFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell_Latest.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell_Latest.moodTypeIcon.image = image
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = creditsCell_Latest.moodTypeIcon.bounds
                            creditsCell_Latest.moodTypeIcon.addSubview(blurEffectView)
                        }
                    }
                    else
                    {
                        creditsCell_Latest.userGenderAgeLbl.text = self.latestFeedsArray[indexPath.row].name
                        
                       
                        creditsCell_Latest.moodTypeIcon?.sd_setImage(with: URL(string: self.latestFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell_Latest.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell_Latest.moodTypeIcon.image = image
                            }
                            
                            for subview in creditsCell_Latest.moodTypeIcon.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                        }
                    }
                    
                    var endDateForConfession = String(self.latestFeedsArray[indexPath.row].totalTime)
                    endDateForConfession = endDateForConfession! + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    let datee = dateFormatter.date(from: endDateForConfession!)!
                    
                    creditsCell_Latest.decreaseTimerLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                     creditsCell_Latest.decreaseTimerLbl.adjustsFontSizeToFitWidth = true
                     
                    
                    let inputString: NSString = self.latestFeedsArray[indexPath.row].statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    
                    creditsCell_Latest.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                    creditsCell_Latest.usersStatus_TxtView.text = modifiedString
                    
                  /*  DispatchQueue.main.async {
                        if (creditsCell_Latest.usersStatus_TxtView.text != "")
                        {
                            let fixedWidth = creditsCell_Latest.usersStatus_TxtView.frame.size.width
                            creditsCell_Latest.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            let newSize = creditsCell_Latest.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            var newFrame = creditsCell_Latest.usersStatus_TxtView.frame
                            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                            
                            creditsCell_Latest.animatedImageHeight.constant = newSize.height
                        }
                        else{
                            creditsCell_Latest.animatedImageHeight.constant = 0
                        }
                    }*/
                   
                    
                     let urlStr = self.latestFeedsArray[indexPath.row].postedImage
                     
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                     creditsCell_Latest.animatedGifImage.sd_setImage(with: URL(string: urlStr), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell_Latest.activityIndicator_Latest.isHidden = true
                        
                        creditsCell_Latest.animatedGifImage.isHidden = false
                        creditsCell_Latest.animatedGifImage.contentMode = UIViewContentMode.scaleAspectFit
                        creditsCell_Latest.animatedGifImage.clipsToBounds = true
                     }
                  })
                    
                     if (self.latestFeedsArray[indexPath.row].likesCount == "0")
                     {
                       creditsCell_Latest.totalLikesCountLbl.text  = ""
                     }
                     else
                     {
                       creditsCell_Latest.totalLikesCountLbl.text = String(self.latestFeedsArray[indexPath.row].likesCount)
                     }
                    
                     if self.latestFeedsArray[indexPath.row].replyCount == "0"
                     {
                       creditsCell_Latest.totalReplyCountLbl.text = ""
                     }
                     else
                     {
                       creditsCell_Latest.totalReplyCountLbl.text = self.latestFeedsArray[indexPath.row].replyCount
                     }
                    
                    if (self.latestFeedsArray[indexPath.row].location != "")
                    {
                        creditsCell_Latest.locationIcon.isHidden = false
                        creditsCell_Latest.userNameLbl_Y_Constraint.constant = 5
                    }else{
                        creditsCell_Latest.locationIcon.isHidden = true
                        creditsCell_Latest.userNameLbl_Y_Constraint.constant = (creditsCell_Latest.moodTypeIcon.frame.origin.y + creditsCell_Latest.moodTypeIcon.frame.size.height)/3
                    }
                    
                     creditsCell_Latest.userLocationLbl.text = self.latestFeedsArray[indexPath.row].location
                     creditsCell_Latest.userLocationLbl.adjustsFontSizeToFitWidth = true
                     
                    creditsCell_Latest.confessionLikesBtn.tag = indexPath.row
                    creditsCell_Latest.confessionLikesBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionLikesLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.confessionReplyBtn.tag = indexPath.row
                    creditsCell_Latest.confessionReplyBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionReplyLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.confessionMessageBtn.tag = indexPath.row
                    creditsCell_Latest.confessionMessageBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionMessageLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.dropDownBtn.tag = indexPath.row
                    creditsCell_Latest.dropDownBtn.addTarget(self, action: #selector(WiConfessionsViewController.DropDownLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.viewFullImageBtn.tag = indexPath.row
                    creditsCell_Latest.viewFullImageBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.viewProfileBtn.tag = indexPath.row
                    creditsCell_Latest.viewProfileBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewProfileLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    return creditsCell_Latest
                }
                else
                {
                    let creditsCell_Latest = tableView.dequeueReusableCell(withIdentifier: "creditsCell1", for: indexPath)as! CreditPackagesTableViewCell
                    
                    creditsCell_Latest.animatedGifImage.isHidden = true
                    creditsCell_Latest.selectionStyle = UITableViewCellSelectionStyle.none
                    creditsCell_Latest.tag = indexPath.row
                    creditsCell_Latest.confessionLikeIcon.tag = indexPath.row
                    creditsCell_Latest.activityIndicatorGif_Latest.tag = indexPath.row
                    creditsCell_Latest.moodTypeIcon.tag = indexPath.row
                    
                    if (self.latestFeedsArray.count <= 0)
                    {
                        return creditsCell_Latest
                    }
                    
                     if self.getFullDataStr == "fetchAllData"
                     {
                        creditsCell_Latest.animatedGifImage.image = nil
                        
                        if (self.latestFeedsArray.count <= 0)
                        {
                            return creditsCell_Latest
                        }
                        
                        if (self.latestFeedsArray.count > 0)
                        {
                            if (self.latestFeedsArray[indexPath.row].likedByMeStatus == "yes")
                            {
                                creditsCell_Latest.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell_Latest.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                     }
                     else
                     {
                        if (self.latestIntArr.count > 0)
                        {
                            if self.latestIntArr.contains(indexPath.row)
                            {
                                creditsCell_Latest.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell_Latest.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                      
                     }
                    
                     let genderStr = self.latestFeedsArray[indexPath.row].gender
                     var gender = String()
                     if genderStr == "1"
                     {
                     gender = "Male"
                     }
                     else
                     {
                     gender = "Female"
                     }
                     
                    creditsCell_Latest.moodTypeIcon.layer.cornerRadius = creditsCell_Latest.moodTypeIcon.frame.size.height/2
                    creditsCell_Latest.moodTypeIcon.clipsToBounds = true
                    creditsCell_Latest.moodTypeIcon.layer.masksToBounds = true
                    creditsCell_Latest.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                    
                    if (self.latestFeedsArray[indexPath.row].typeOfConfession == "anonymous")
                    {
                        creditsCell_Latest.userGenderAgeLbl.text = gender + ", " + self.latestFeedsArray[indexPath.row].age
                        
                        creditsCell_Latest.moodTypeIcon?.sd_setImage(with: URL(string: self.latestFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell_Latest.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell_Latest.moodTypeIcon.image = image
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = creditsCell_Latest.moodTypeIcon.bounds
                            creditsCell_Latest.moodTypeIcon.addSubview(blurEffectView)
                        }
                    }
                    else
                    {
                        creditsCell_Latest.userGenderAgeLbl.text = self.latestFeedsArray[indexPath.row].name
                        
                      
                        
                        creditsCell_Latest.moodTypeIcon?.sd_setImage(with: URL(string: self.latestFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                            if (error != nil) {
                                creditsCell_Latest.moodTypeIcon.image = UIImage(named: "Default Icon")
                            } else {
                                creditsCell_Latest.moodTypeIcon.image = image
                            }
                            
                            for subview in creditsCell_Latest.moodTypeIcon.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                        }
                    }
                    
                    var endDateForConfession = String(self.latestFeedsArray[indexPath.row].totalTime)
                    endDateForConfession = endDateForConfession! + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    let datee = dateFormatter.date(from: endDateForConfession!)!
                    creditsCell_Latest.decreaseTimerLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                     creditsCell_Latest.decreaseTimerLbl.adjustsFontSizeToFitWidth = true
                    
                    let inputString: NSString = self.latestFeedsArray[indexPath.row].statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    
                    creditsCell_Latest.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                    creditsCell_Latest.usersStatus_TxtView.text = modifiedString
                    
                 /*   DispatchQueue.main.async {
                        if (creditsCell_Latest.usersStatus_TxtView.text != "")
                        {
                            let fixedWidth = creditsCell_Latest.usersStatus_TxtView.frame.size.width
                            creditsCell_Latest.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            let newSize = creditsCell_Latest.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            var newFrame = creditsCell_Latest.usersStatus_TxtView.frame
                            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                            
                            creditsCell_Latest.animatedGifHeight.constant = newSize.height
                        }
                        else{
                            creditsCell_Latest.animatedGifHeight.constant = 0
                        }
                    }*/

                    
                     let urlStr = self.latestFeedsArray[indexPath.row].postedImage
                     
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                     creditsCell_Latest.animatedGifImage.sd_setImage(with: URL(string: urlStr), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell_Latest.activityIndicatorGif_Latest.isHidden = true
                        
                        creditsCell_Latest.animatedGifImage.isHidden = false
                        creditsCell_Latest.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell_Latest.animatedGifImage.clipsToBounds = true
                        
//                        creditsCell_Latest.animatedGifImage.layer.borderWidth = 2.0
//                        creditsCell_Latest.animatedGifImage.layer.masksToBounds = false
//                        creditsCell_Latest.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                     }
                })
                    
                     if  (self.latestFeedsArray[indexPath.row].likesCount == "0")
                     {
                     creditsCell_Latest.totalLikesCountLbl.text  = ""
                     }
                     else
                     {
                     creditsCell_Latest.totalLikesCountLbl.text = String(self.latestFeedsArray[indexPath.row].likesCount)
                     }
                    
                     if  self.latestFeedsArray[indexPath.row].replyCount == "0"
                     {
                     creditsCell_Latest.totalReplyCountLbl.text = ""
                     }
                     else
                     {
                     creditsCell_Latest.totalReplyCountLbl.text = self.latestFeedsArray[indexPath.row].replyCount
                     }
                    
                    if (self.latestFeedsArray[indexPath.row].location != "")
                    {
                        creditsCell_Latest.locationIcon.isHidden = false
                         creditsCell_Latest.userNameLbl_Y_Constraint.constant = 5
                    }else{
                        creditsCell_Latest.locationIcon.isHidden = true
                        creditsCell_Latest.userNameLbl_Y_Constraint.constant = (creditsCell_Latest.moodTypeIcon.frame.origin.y + creditsCell_Latest.moodTypeIcon.frame.size.height)/3
                    }
                    
                     creditsCell_Latest.userLocationLbl.text = self.latestFeedsArray[indexPath.row].location
                     creditsCell_Latest.userLocationLbl.adjustsFontSizeToFitWidth = true
                     
                    creditsCell_Latest.confessionLikesBtn.tag = indexPath.row
                    creditsCell_Latest.confessionLikesBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionLikesLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.confessionReplyBtn.tag = indexPath.row
                    creditsCell_Latest.confessionReplyBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionReplyLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.confessionMessageBtn.tag = indexPath.row
                    creditsCell_Latest.confessionMessageBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionMessageLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.dropDownBtn.tag = indexPath.row
                    creditsCell_Latest.dropDownBtn.addTarget(self, action: #selector(WiConfessionsViewController.DropDownLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.viewFullImageBtn.tag = indexPath.row
                    creditsCell_Latest.viewFullImageBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell_Latest.viewProfileBtn.tag = indexPath.row
                    creditsCell_Latest.viewProfileBtn.addTarget(self, action: #selector(WiConfessionsViewController.viewProfileLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    return creditsCell_Latest
                }
            }
            else
            {
                let onlineUsersCell = tableView.dequeueReusableCell(withIdentifier: "statusOnlyCell", for: indexPath)as! ConfessionStatusTableViewCell
                
                onlineUsersCell.selectionStyle = UITableViewCellSelectionStyle.none
                onlineUsersCell.confessionLikeIcon.tag = indexPath.row
                onlineUsersCell.moodIcon.tag = indexPath.row
                
                var endDateForConfession = String(self.latestFeedsArray[indexPath.row].totalTime)
                endDateForConfession = endDateForConfession! + " UTC"
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone.current
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                let datee = dateFormatter.date(from: endDateForConfession!)!
                
                onlineUsersCell.decreasePostTimeLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                
                if (self.latestFeedsArray.count <= 0)
                {
                    return onlineUsersCell
                }
                 
                 DispatchQueue.main.async {
                    if self.getFullDataStr == "fetchAllData"
                    {
                        if (self.latestFeedsArray.count > 0)
                        {
                            if (self.latestFeedsArray[indexPath.row].likedByMeStatus == "yes")
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                    }
                    else
                    {
                        if (self.latestIntArr.count > 0)
                        {
                            if self.latestIntArr.contains(indexPath.row)
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                    }
                 }
                
                 let genderStr = self.latestFeedsArray[indexPath.row].gender
                 var gender = String()
                 if genderStr == "1"
                 {
                 gender = "Male"
                 }
                 else
                 {
                 gender = "Female"
                 }
                
                onlineUsersCell.moodIcon.layer.cornerRadius = onlineUsersCell.moodIcon.frame.size.height/2
                onlineUsersCell.moodIcon.clipsToBounds = true
                onlineUsersCell.moodIcon.layer.masksToBounds = true
                onlineUsersCell.moodIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.latestFeedsArray[indexPath.row].typeOfConfession == "anonymous")
                {
                    onlineUsersCell.userDetailsLbl.text = gender + ", " + self.latestFeedsArray[indexPath.row].age
                    
                    onlineUsersCell.moodIcon?.sd_setImage(with: URL(string: self.latestFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            onlineUsersCell.moodIcon.image = UIImage(named: "Default Icon")
                        } else {
                            onlineUsersCell.moodIcon.image = image
                        }
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = onlineUsersCell.moodIcon.bounds
                        onlineUsersCell.moodIcon.addSubview(blurEffectView)
                    }
                    
                   
                }
                else
                {
                    onlineUsersCell.userDetailsLbl.text = self.latestFeedsArray[indexPath.row].name
                    
                    onlineUsersCell.moodIcon?.sd_setImage(with: URL(string: self.latestFeedsArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            onlineUsersCell.moodIcon.image = UIImage(named: "Default Icon")
                        } else {
                            onlineUsersCell.moodIcon.image = image
                        }
                        
                        for subview in onlineUsersCell.moodIcon.subviews
                        {
                            if subview.isKind(of: UIVisualEffectView.self)
                            {
                                subview.removeFromSuperview()
                            }
                        }
                    }
                }
                
                 if  self.latestFeedsArray[indexPath.row].likesCount == "0"
                 {
                 onlineUsersCell.likeCountLbl.text  = ""
                 }
                 else
                 {
                 onlineUsersCell.likeCountLbl.text = String(self.latestFeedsArray[indexPath.row].likesCount)
                 }
                
                 if self.latestFeedsArray[indexPath.row].replyCount == "0"
                 {
                 onlineUsersCell.replyCountLbl.text = ""
                 }
                 else
                 {
                 onlineUsersCell.replyCountLbl.text = self.latestFeedsArray[indexPath.row].replyCount
                 }
                
                if (self.latestFeedsArray[indexPath.row].location != "")
                {
                    onlineUsersCell.locationIcon.isHidden = false
                    onlineUsersCell.userDetailsLbl_Y_Frame.constant = 5
                }else{
                    onlineUsersCell.locationIcon.isHidden = true
                    onlineUsersCell.userDetailsLbl_Y_Frame.constant = (onlineUsersCell.moodIcon.frame.origin.y + onlineUsersCell.moodIcon.frame.size.height)/3
                }
                
                 onlineUsersCell.userLocationLbl.text = self.latestFeedsArray[indexPath.row].location
                 onlineUsersCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                 
//                 onlineUsersCell.userPostedStatusLbl.text = self.latestFeedsArray[indexPath.row].statusAdded
                
                let inputString: NSString = self.latestFeedsArray[indexPath.row].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                onlineUsersCell.usersStatusTextView.text = modifiedString
 
                onlineUsersCell.postLikesBtn.tag = indexPath.row
                onlineUsersCell.postLikesBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionLikesLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postReplyBtn.tag = indexPath.row
                onlineUsersCell.postReplyBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionReplyLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postMessageBtn.tag = indexPath.row
                onlineUsersCell.postMessageBtn.addTarget(self, action: #selector(WiConfessionsViewController.confessionMessageLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postDropDownBtn.tag = indexPath.row
                onlineUsersCell.postDropDownBtn.addTarget(self, action: #selector(WiConfessionsViewController.DropDownLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.viewProfile_btn.tag = indexPath.row
                onlineUsersCell.viewProfile_btn.addTarget(self, action: #selector(WiConfessionsViewController.viewProfileLatest_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return onlineUsersCell
            }
        }
    }
 
    // MARK: - ****** getRemainingTime ******
    func getRemainingTime(timestamp: Date, timeframe: String)->  String
    {
        if (timestamp != nil)
        {
            let date22 = timestamp
            
            var calendar = Calendar.current
            calendar.timeZone = TimeZone.current
            var TimeFrameAdded = String()
            
            let formatter3 = DateFormatter()
            
            if (timeframe.range(of: " ") != nil)
            {
                let fullUserID = timeframe.components(separatedBy: " ")
                let beforeAtTheRateStr: String = fullUserID[0]
                TimeFrameAdded = beforeAtTheRateStr
            }
            
            if (date22 != nil)
            {
                var dateStr = Date()
                if timeframe == "24 hrs"
                {
                    dateStr = calendar.date(byAdding: .day, value: 1, to: date22 as Date)!
                    formatter3.timeZone = TimeZone.current
                    formatter3.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            now3 = formatter.string(from: dateStr as Date)
                }
                else
                {
                    dateStr = calendar.date(byAdding: .hour, value: Int(TimeFrameAdded)!, to: date22 as Date)!
                    
                    formatter3.timeZone = TimeZone.current
                    formatter3.dateFormat = "yyyy-MM-dd HH:mm:ss"
                }
                
                let nowDate = NSDate()
                let secondsFromNowToFinish = dateStr.timeIntervalSince(nowDate as Date)
                let hours = Int(secondsFromNowToFinish / 3600)
                let minutes = Int((secondsFromNowToFinish - Double(hours) * 3600) / 60)
                let seconds = Int(secondsFromNowToFinish - Double(hours) * 3600 - Double(minutes) * 60 + 0.5)
                
//        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
                return String(format: "%02d", hours)
            }else{
                return ""
            }
        }else{
            return ""
        }
    }
    

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == self.nearByConfessions_tableView
        {
            if (self.nearByFeedsArray.count <= 0)
            {
                return UITableViewAutomaticDimension
            }
            
            if (self.nearByFeedsArray[indexPath.row].photoAlbumTypeStr != "")
            {
                return UITableViewAutomaticDimension
            }
            else{
                return UITableViewAutomaticDimension
            }
        }
        else if tableView == self.popularConfessions_tableView
        {
            if (self.popularFeedsArray.count <= 0)
            {
                return UITableViewAutomaticDimension
            }
            
            if (self.popularFeedsArray[indexPath.row].photoAlbumTypeStr != "") && ((self.popularFeedsArray[indexPath.row].photoAlbumTypeStr == "photo") || (self.popularFeedsArray[indexPath.row].photoAlbumTypeStr == "gif"))
            {
                return UITableViewAutomaticDimension
            }
            else{
                return UITableViewAutomaticDimension
            }
        }
        else
        {
            if (self.latestFeedsArray.count <= 0)
            {
                return UITableViewAutomaticDimension
            }
            
            if (self.latestFeedsArray[indexPath.row].photoAlbumTypeStr != "") && ((self.latestFeedsArray[indexPath.row].photoAlbumTypeStr == "photo") || (self.latestFeedsArray[indexPath.row].photoAlbumTypeStr == "gif"))
            {
                return UITableViewAutomaticDimension
            }
            else{
                return UITableViewAutomaticDimension
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    
    // MARK: - ****** getRemainingTime ******
    @IBAction func closeFullSizeView_Action(_ sender: Any) {
        self.showFullImageView.isHidden = true
    }
    
    
    func likeDislikeConfessions(confessionID: String, completion: @escaping (String) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.likeUnlikeConfessions(userID: userID, userToken: authToken, confession_id: confessionID, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            completion(message!)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(message!)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion("Error Occurs.")
                        }
                        
                    })
                }
            }
        }
    }
    
    func viewProfileNearBy_btnAction(sender: UIButton!) {
        
        if (self.nearByFeedsArray.count > 0)
        {
            if (self.nearByFeedsArray[sender.tag].typeOfConfession == "anonymous")
            {
                print("post is anonymous")
            }
            else
            {
                let indexPathRow = IndexPath(row: sender.tag, section: 0)
                let userOwnerIDStr = self.nearByFeedsArray[sender.tag].userIDStr
                if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if id == userOwnerIDStr
                    {
                        let profileView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                        self.navigationController?.pushViewController(profileView, animated: true)
                    }
                    else
                    {
                        let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                        viewProfileScreen.ownerID = self.nearByFeedsArray[sender.tag].userIDStr
                        self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                    }
                }
            }
        }
    }
    
    func viewProfilePopular_btnAction(sender: UIButton!) {
        
        if (self.popularFeedsArray.count > 0)
        {
            if (self.popularFeedsArray[sender.tag].typeOfConfession == "anonymous")
            {
                print("post is anonymous")
            }
            else
            {
//                let indexPathRow = IndexPath(row: sender.tag, section: 0)
                let userOwnerIDStr = self.popularFeedsArray[sender.tag].userIDStr
                if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if id == userOwnerIDStr
                    {
                        let profileView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                        self.navigationController?.pushViewController(profileView, animated: true)
                    }
                    else
                    {
                        let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                        viewProfileScreen.ownerID = self.popularFeedsArray[sender.tag].userIDStr
                        self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                    }
                }
            }
        }
    }
    
    func viewProfileLatest_btnAction(sender: UIButton!) {
        
        if (self.latestFeedsArray.count > 0)
        {
            if (self.latestFeedsArray[sender.tag].typeOfConfession == "anonymous")
            {
                print("post is anonymous")
            }
            else
            {
//                let indexPathRow = IndexPath(row: sender.tag, section: 0)
                let userOwnerIDStr = self.latestFeedsArray[sender.tag].userIDStr
                if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if id == userOwnerIDStr
                    {
                        let profileView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                        self.navigationController?.pushViewController(profileView, animated: true)
                    }
                    else
                    {
                        let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                        viewProfileScreen.ownerID = self.latestFeedsArray[sender.tag].userIDStr
                        self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                    }
                }
            }
        }
    }
    
    
    
    // MARK: - ****** confessionLikesNearBy_btnAction ******
    func confessionLikesNearBy_btnAction(sender: UIButton!) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.nearByFeedsArray.count > 0)
            {
                self.likeDislikeConfessions(confessionID: self.nearByFeedsArray[sender.tag].postIDStr, completion: { (responseBool) in
//                    print("responseBool = \(responseBool)")
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected_NearBy = IndexPath()
                        self.indexPathSelected_NearBy = IndexPath(row: sender.tag, section: 0)
                        
//                        self.nearByFeedsArray[sender.tag].likedByMeStatus = "yes"
//                        self.nearByFeedsArray[sender.tag].likesCount
                        
                        self.nearByIntArr.add(self.indexPathSelected_NearBy.row)
                        
                        var typeOfCOnfession = String()
                        if (self.nearByFeedsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.nearByFeedsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.nearByConfessions_tableView.cellForRow(at: self.indexPathSelected_NearBy) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.nearByFeedsArray[sender.tag].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text  = ""
                                    
                                    self.nearByFeedsArray[sender.tag].likedByMeStatus = "yes"
                                    self.nearByFeedsArray[sender.tag].likesCount = "1"
                                    
                                    self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikeLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.nearByFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.nearByFeedsArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.nearByFeedsArray[sender.tag].likesCount)! + 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.nearByFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.nearByFeedsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.nearByConfessions_tableView.cellForRow(at: self.indexPathSelected_NearBy) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.nearByFeedsArray[sender.tag].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.nearByFeedsArray[sender.tag].likedByMeStatus = "yes"
                                    self.nearByFeedsArray[sender.tag].likesCount = "1"
                                    
                                    self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikeLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.nearByFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.nearByFeedsArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.nearByFeedsArray[sender.tag].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.nearByFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.nearByFeedsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
//                        self.getFullDataStr = "onliked"
//                        self.nearByFeedsArray[sender.tag].likedByMeStatus = "no"

                        if (self.nearByFeedsArray.count > 0)
                        {
                            self.indexPathSelected_NearBy = IndexPath()
                            self.indexPathSelected_NearBy = IndexPath(row: sender.tag, section: 0)
                            
                            self.nearByIntArr.remove(self.indexPathSelected_NearBy.row)
                            
                            var typeOfCOnfession = String()
                            if (self.nearByFeedsArray[sender.tag].postedImage != "")
                            {
                                let imageURL = self.nearByFeedsArray[sender.tag].postedImage
                                if (imageURL.contains("jpg"))
                                {
                                    typeOfCOnfession = "photo"
                                }
                                else
                                {
                                    typeOfCOnfession = "gif"
                                }
                            }
                            else
                            {
                                typeOfCOnfession = "text"
                            }
                            
                            
                            if (typeOfCOnfession == "text")
                            {
                                let onlineUsersCell = self.nearByConfessions_tableView.cellForRow(at: self.indexPathSelected_NearBy) as! ConfessionStatusTableViewCell
                                
                                DispatchQueue.main.async {
                                    onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.nearByFeedsArray[sender.tag].likesCount
                                    //                                print("likesCountStr = \(likesCountStr)")
                                    
                                    if (likesCountStr == "0")
                                    {
                                        onlineUsersCell.likeCountLbl.text = ""//String(totalLikes)
                                        
                                        self.nearByFeedsArray[sender.tag].likedByMeStatus = "no"
                                        self.nearByFeedsArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikeLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            onlineUsersCell.likeCountLbl.text = "" //String(totalLikes)
                                            
                                            self.nearByFeedsArray[sender.tag].likedByMeStatus = "no"
                                            self.nearByFeedsArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.nearByFeedsArray[sender.tag].likesCount)! - 1
                                            if (String(totalLikes) == "0")
                                            {
                                                onlineUsersCell.likeCountLbl.text  = ""
                                                
                                                self.nearByFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.nearByFeedsArray[sender.tag].likesCount = ""
                                                
                                                self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikeLbl.text = ""
                                            }
                                            else
                                            {
                                                self.nearByFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.nearByFeedsArray[sender.tag].likesCount = String(totalLikes)
                                                
                                                onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                                
                                                self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikeLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                let creditsCell = self.nearByConfessions_tableView.cellForRow(at: self.indexPathSelected_NearBy) as! CreditPackagesTableViewCell
                                
                                DispatchQueue.main.async {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.nearByFeedsArray[sender.tag].likesCount
                                    //                                print("likesCountStr = \(likesCountStr)")
                                    
                                    if (likesCountStr == "0")
                                    {
                                        creditsCell.totalLikesCountLbl.text = ""
                                        
                                        self.nearByFeedsArray[sender.tag].likedByMeStatus = "no"
                                        self.nearByFeedsArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikeLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                            
                                            self.nearByFeedsArray[sender.tag].likedByMeStatus = "no"
                                            self.nearByFeedsArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.nearByFeedsArray[sender.tag].likesCount)! - 1
                                            
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                creditsCell.totalLikesCountLbl.text = ""
                                                
                                                self.nearByFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.nearByFeedsArray[sender.tag].likesCount = ""
                                                
                                                self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikeLbl.text = ""
                                            }
                                            else
                                            {
                                                self.nearByFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.nearByFeedsArray[sender.tag].likesCount = String(totalLikes)
                                                
                                                creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                                
                                                self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikeLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
    
    
    // MARK: - ****** confessionLikesPopular_btnAction ******
    func confessionLikesPopular_btnAction(sender: UIButton!) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.popularFeedsArray.count > 0)
            {
                self.likeDislikeConfessions(confessionID: self.popularFeedsArray[sender.tag].postIDStr, completion: { (responseBool) in
                    
//                    print("responseBool = \(responseBool)")
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected_popular = IndexPath()
                        self.indexPathSelected_popular = IndexPath(row: sender.tag, section: 0)
                        
                        self.popularIntArr.add(self.indexPathSelected_popular.row)
                        
                        var typeOfCOnfession = String()
                        if (self.popularFeedsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.popularFeedsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.popularConfessions_tableView.cellForRow(at: self.indexPathSelected_popular) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.popularFeedsArray[sender.tag].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if  (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                    
                                    self.popularFeedsArray[sender.tag].likedByMeStatus = "yes"
                                    self.popularFeedsArray[sender.tag].likesCount = String(totalLikes)
                                    
                                    self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                    self.popularLikeCountLbl.text = String(totalLikes)
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.popularFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.popularFeedsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                        self.popularLikeCountLbl.text = String(totalLikes)
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.popularFeedsArray[sender.tag].likesCount)! + 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.popularFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.popularFeedsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                        self.popularLikeCountLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.popularConfessions_tableView.cellForRow(at: self.indexPathSelected_popular) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.popularFeedsArray[sender.tag].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.popularFeedsArray[sender.tag].likedByMeStatus = "yes"
                                    self.popularFeedsArray[sender.tag].likesCount = String(totalLikes)
                                    
                                    self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                    self.popularLikeCountLbl.text = String(totalLikes)
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.popularFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.popularFeedsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                        self.popularLikeCountLbl.text = String(totalLikes)
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.popularFeedsArray[sender.tag].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.popularFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.popularFeedsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                        self.popularLikeCountLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
                        if (self.popularFeedsArray.count > 0)
                        {
                            self.indexPathSelected_popular = IndexPath()
                            self.indexPathSelected_popular = IndexPath(row: sender.tag, section: 0)
                            self.popularIntArr.remove(self.indexPathSelected_popular.row)
                            
                            var typeOfCOnfession = String()
                            if (self.popularFeedsArray[sender.tag].postedImage != "")
                            {
                                let imageURL = self.popularFeedsArray[sender.tag].postedImage
                                if (imageURL.contains("jpg"))
                                {
                                    typeOfCOnfession = "photo"
                                }
                                else
                                {
                                    typeOfCOnfession = "gif"
                                }
                            }
                            else
                            {
                                typeOfCOnfession = "text"
                            }
                            
                            if (typeOfCOnfession == "text")
                            {
                                let onlineUsersCell = self.popularConfessions_tableView.cellForRow(at: self.indexPathSelected_popular) as! ConfessionStatusTableViewCell
                                
                                DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.popularFeedsArray[sender.tag].likesCount
                                    
                                    if (likesCountStr == "0")
                                    {
                                        onlineUsersCell.likeCountLbl.text = ""
                                        self.popularFeedsArray[sender.tag].likedByMeStatus = "no"
                                        self.popularFeedsArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                        self.popularLikeCountLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            onlineUsersCell.likeCountLbl.text = "" //String(totalLikes)
                                            
                                            self.popularFeedsArray[sender.tag].likedByMeStatus = "no"
                                            self.popularFeedsArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                            self.popularLikeCountLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.popularFeedsArray[sender.tag].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                onlineUsersCell.likeCountLbl.text  = ""
                                                
                                                self.popularFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.popularFeedsArray[sender.tag].likesCount = ""
                                                
                                                self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                                self.popularLikeCountLbl.text = ""
                                            }
                                            else
                                            {
                                                self.popularFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.popularFeedsArray[sender.tag].likesCount = String(totalLikes)
                                                
                                                onlineUsersCell.likeCountLbl.text  = String(totalLikes)
                                                
                                                self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                                self.popularLikeCountLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                let creditsCell = self.popularConfessions_tableView.cellForRow(at: self.indexPathSelected_popular) as! CreditPackagesTableViewCell
                                
                                DispatchQueue.main.async {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.popularFeedsArray[sender.tag].likesCount
                                    //                                print("likesCountStr = \(likesCountStr)")
                                    
                                    if (likesCountStr == "0")
                                    {
                                        creditsCell.totalLikesCountLbl.text = ""
                                        
                                        self.popularFeedsArray[sender.tag].likedByMeStatus = "no"
                                        self.popularFeedsArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                        self.popularLikeCountLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            creditsCell.totalLikesCountLbl.text = ""
                                            self.popularFeedsArray[sender.tag].likedByMeStatus = "no"
                                            self.popularFeedsArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                            self.popularLikeCountLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.popularFeedsArray[sender.tag].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                creditsCell.totalLikesCountLbl.text  = ""
                                                
                                                self.popularFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.popularFeedsArray[sender.tag].likesCount = ""
                                                
                                                self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                                self.popularLikeCountLbl.text = ""
                                            }
                                            else
                                            {
                                                self.popularFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.popularFeedsArray[sender.tag].likesCount = String(totalLikes)
                                                
                                                creditsCell.totalLikesCountLbl.text  = String(totalLikes)
                                                
                                                self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                                self.popularLikeCountLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
  
    
    // MARK: - ****** confessionLikesLatest_btnAction ******
    func confessionLikesLatest_btnAction(sender: UIButton!) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.latestFeedsArray.count > 0)
            {
                self.likeDislikeConfessions(confessionID: self.latestFeedsArray[sender.tag].postIDStr, completion: { (responseBool) in
//                    print("responseBool = \(responseBool)")
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected_Latest = IndexPath()
                        self.indexPathSelected_Latest = IndexPath(row: sender.tag, section: 0)
                        
                        self.latestIntArr.add(self.indexPathSelected_Latest.row)
                        
                        var typeOfCOnfession = String()
                        if (self.latestFeedsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.latestFeedsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.latestConfessions_tableView.cellForRow(at: self.indexPathSelected_Latest) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.latestFeedsArray[sender.tag].likesCount
                                
                                if (likesCountStr != "") || (likesCountStr != nil)
                                {
                                    if (likesCountStr == "0")
                                    {
                                        onlineUsersCell.likeCountLbl.text  = "1"
                                        
                                        self.latestFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.latestFeedsArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                        self.latestLikeCountLbl.text = "1"
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            let totalLikes = 1
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.latestFeedsArray[sender.tag].likedByMeStatus = "yes"
                                            self.latestFeedsArray[sender.tag].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                            self.latestLikeCountLbl.text = String(totalLikes)
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.latestFeedsArray[sender.tag].likesCount)! + 1
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.latestFeedsArray[sender.tag].likedByMeStatus = "yes"
                                            self.latestFeedsArray[sender.tag].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                            self.latestLikeCountLbl.text = String(totalLikes)
                                        }
                                    }
                                }else{
                                    let totalLikes = 1
                                    onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                    
                                    self.latestFeedsArray[sender.tag].likedByMeStatus = "yes"
                                    self.latestFeedsArray[sender.tag].likesCount = String(totalLikes)
                                    
                                    self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                    self.latestLikeCountLbl.text = String(totalLikes)
                                }
                                
                               
                            }
                        }
                        else
                        {
                            let creditsCell = self.latestConfessions_tableView.cellForRow(at: self.indexPathSelected_Latest) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.latestFeedsArray[sender.tag].likesCount
                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.latestFeedsArray[sender.tag].likedByMeStatus = "yes"
                                    self.latestFeedsArray[sender.tag].likesCount = String(totalLikes)
                                    
                                    self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                    self.latestLikeCountLbl.text = String(totalLikes)
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.latestFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.latestFeedsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                        self.latestLikeCountLbl.text = String(totalLikes)
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.latestFeedsArray[sender.tag].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.latestFeedsArray[sender.tag].likedByMeStatus = "yes"
                                        self.latestFeedsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                        self.latestLikeCountLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
                        if (self.latestFeedsArray.count > 0)
                        {
                            self.indexPathSelected_Latest = IndexPath()
                            self.indexPathSelected_Latest = IndexPath(row: sender.tag, section: 0)
                            
                            self.latestIntArr.remove(self.indexPathSelected_Latest.row)
                            
                            var typeOfCOnfession = String()
                            if (self.latestFeedsArray[sender.tag].postedImage != "")
                            {
                                let imageURL = self.latestFeedsArray[sender.tag].postedImage
                                if (imageURL.contains("jpg"))
                                {
                                    typeOfCOnfession = "photo"
                                }
                                else
                                {
                                    typeOfCOnfession = "gif"
                                }
                            }
                            else
                            {
                                typeOfCOnfession = "text"
                            }
                            
                            
                            if (typeOfCOnfession == "text")
                            {
                                let onlineUsersCell = self.latestConfessions_tableView.cellForRow(at: self.indexPathSelected_Latest) as! ConfessionStatusTableViewCell
                                
                                DispatchQueue.main.async {
                                    onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.latestFeedsArray[sender.tag].likesCount
                                    //                                print("likesCountStr = \(likesCountStr)")
                                    
                                    if (likesCountStr == "0")
                                    {
                                        onlineUsersCell.likeCountLbl.text = ""
                                        
                                        self.latestFeedsArray[sender.tag].likedByMeStatus = "no"
                                        self.latestFeedsArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                        self.latestLikeCountLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            onlineUsersCell.likeCountLbl.text = ""
                                            self.latestFeedsArray[sender.tag].likedByMeStatus = "no"
                                            self.latestFeedsArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                            self.latestLikeCountLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.latestFeedsArray[sender.tag].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                onlineUsersCell.likeCountLbl.text  = ""
                                                
                                                self.latestFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.latestFeedsArray[sender.tag].likesCount = ""
                                                
                                                self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                                self.latestLikeCountLbl.text = ""
                                            }
                                            else
                                            {
                                                onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                                
                                                self.latestFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.latestFeedsArray[sender.tag].likesCount = String(totalLikes)
                                                
                                                self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                                self.latestLikeCountLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                let creditsCell = self.latestConfessions_tableView.cellForRow(at: self.indexPathSelected_Latest) as! CreditPackagesTableViewCell
                                
                                DispatchQueue.main.async {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.latestFeedsArray[sender.tag].likesCount
                                    //                                print("likesCountStr = \(likesCountStr)")
                                    
                                    if (likesCountStr == "0")
                                    {
                                        creditsCell.totalLikesCountLbl.text = ""
                                        self.latestFeedsArray[sender.tag].likedByMeStatus = "no"
                                        self.latestFeedsArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                        self.latestLikeCountLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            creditsCell.totalLikesCountLbl.text = ""
                                            self.latestFeedsArray[sender.tag].likedByMeStatus = "no"
                                            self.latestFeedsArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                            self.latestLikeCountLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.latestFeedsArray[sender.tag].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                creditsCell.totalLikesCountLbl.text  = ""
                                                
                                                self.latestFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.latestFeedsArray[sender.tag].likesCount = ""
                                                
                                                self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                                self.latestLikeCountLbl.text = ""
                                            }
                                            else
                                            {
                                                creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                                
                                                self.latestFeedsArray[sender.tag].likedByMeStatus = "no"
                                                self.latestFeedsArray[sender.tag].likesCount = String(totalLikes)
                                                
                                                self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                                self.latestLikeCountLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
  

    // MARK: - ****** viewFullSizeImageACtion_nearBy ******
    func viewFullSizeImageACtion_nearBy(sender: UIButton!)
    {
        self.indexToClick = sender.tag
        
        if (self.nearByFeedsArray.count <= 0)
        {
            return
        }
        
        if (self.nearByFeedsArray[sender.tag].postedImage != "")
        {
            let imageURL = self.nearByFeedsArray[sender.tag].postedImage
            if (imageURL.contains("jpg"))
            {
                //Default Icon
                self.fullSizeImgae.sd_setImage(with: URL(string: self.nearByFeedsArray[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                   
                    if ((image?.size.height) != nil) && ((image?.size.width) != nil)
                    {
                        self.fullSizeImgae.backgroundColor = UIColor.clear
                        self.showFullImageView.isHidden = false
                        self.fullSizeImgae.backgroundColor = UIColor.clear
                        
                        self.fullSizeImgae.isHidden = false
                        self.nearByAnimatedGifImage.isHidden = true
                        
                        if (image?.size.height)! <= (self.nearByGifWrapperView.frame.size.height)
                        {
                            var gifWidth = Int()
                            var screenRes = Int()
                            var newXPos =  Int()
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  0
                            }
                            else{
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  screenRes - gifWidth
                            }
                            
                            
                            let gifHeight = Int((image?.size.height)!/2)
                            let heightOfWrraperView = Int(self.nearByGifWrapperView.frame.height/2)
                            let newYPos: Int =  heightOfWrraperView - gifHeight
                            
                            self.nearByGifWrapperView.frame.origin.y = 0
                            self.fullSizeImgae.frame.origin.y = CGFloat(newYPos)
                            self.fullSizeImgae.frame.origin.x = CGFloat(newXPos)
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                self.fullSizeImgae.frame.size.width = self.view.frame.size.width
                            }
                            else{
                                self.fullSizeImgae.frame.size.width = (image?.size.width)!
                            }
                            
                            if (image?.size.height)! <= (self.nearByGifWrapperView.frame.size.height)
                            {
                                self.fullSizeImgae.frame.size.height = (image?.size.height)!
                            }
                            else{
                                self.fullSizeImgae.frame.size.height = self.nearByGifWrapperView.frame.size.height
                            }
                            
                            self.nearByButtonsWrapperView.frame.origin.y = self.fullSizeImgae.frame.origin.y + self.fullSizeImgae.frame.size.height + 10
                            self.nearByGifWrapperView.bringSubview(toFront: self.nearByButtonsWrapperView)
                        }
                        else{
                            self.fullSizeImgae.frame.origin.y = 21
                            self.fullSizeImgae.frame.size.height = (self.nearByGifWrapperView.frame.size.height - 21)
                            
                            self.nearByButtonsWrapperView.frame.origin.y = self.nearByGifWrapperView.frame.origin.y + self.nearByGifWrapperView.frame.size.height + 10
                            self.nearByGifWrapperView.bringSubview(toFront: self.nearByButtonsWrapperView)
                        }
                        
                        self.fullSizeImgae.layer.borderWidth = 2.0
                        self.fullSizeImgae.layer.masksToBounds = false
                        self.fullSizeImgae.layer.borderColor = UIColor.lightGray.cgColor
                        self.fullSizeImgae.contentMode = UIViewContentMode.scaleAspectFit
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Please wait.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                self.fullSizeImgae.backgroundColor = UIColor.clear
                self.showFullImageView.isHidden = false
                self.fullSizeImgae.backgroundColor = UIColor.clear
                
                self.fullSizeImgae.isHidden = true
                self.nearByAnimatedGifImage.isHidden = false
              
                DispatchQueue.main.async {
                    self.nearByAnimatedGifImage.sd_setImage(with: URL(string: self.nearByFeedsArray[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions.refreshCached)
                }
                
                self.nearByAnimatedGifImage.layer.borderWidth = 2.0
                self.nearByAnimatedGifImage.layer.masksToBounds = false
                self.nearByAnimatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                self.nearByAnimatedGifImage.contentMode = UIViewContentMode.scaleAspectFit
                
              
                var gifWidth = Int()
                var screenRes = Int()
                var newXPos =  Int()
                if (self.nearByFeedsArray[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    gifWidth = Int(self.nearByFeedsArray[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  0
                }
                else{
                    gifWidth = Int(self.nearByFeedsArray[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  screenRes - gifWidth
                }
                
                let gifHeight = Int(self.nearByFeedsArray[sender.tag].height/2)
                let heightOfWrraperView = Int(self.nearByGifWrapperView.frame.height/2)
                let newYPos: Int =  heightOfWrraperView - gifHeight
                
                self.nearByGifWrapperView.frame.origin.y = 0
                self.nearByAnimatedGifImage.frame.origin.y = CGFloat(newYPos)
                self.nearByAnimatedGifImage.frame.origin.x = CGFloat(newXPos)
                
                if (self.nearByFeedsArray[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    self.nearByAnimatedGifImage.frame.size.width = self.view.frame.size.width
                }
                else{
                    self.nearByAnimatedGifImage.frame.size.width = CGFloat(self.nearByFeedsArray[sender.tag].width)
                }
                
                self.nearByAnimatedGifImage.frame.size.height = CGFloat(self.nearByFeedsArray[sender.tag].height)
                
                self.nearByButtonsWrapperView.frame.origin.y = self.nearByAnimatedGifImage.frame.origin.y + self.nearByAnimatedGifImage.frame.size.height + 10
                self.nearByGifWrapperView.bringSubview(toFront: self.nearByButtonsWrapperView)
            }
        }
        
        DispatchQueue.main.async {
            
            if self.getFullDataStr == "fetchAllData"
            {
                if (self.nearByFeedsArray[sender.tag].likedByMeStatus == "yes")
                {
                    self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
            else
            {
                if self.nearByIntArr.contains(sender.tag)
                {
                    self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
        }
        
        if self.nearByFeedsArray[sender.tag].likesCount == "0"
        {
             self.confessionLikeLbl.text = ""
        }
        else
        {
            self.confessionLikeLbl.text = String(self.nearByFeedsArray[sender.tag].likesCount)
        }
        
        if Int(self.nearByFeedsArray[sender.tag].replyCount)! == 0
        {
            self.confessionReplyLbl.text = ""
        }
        else if Int(self.nearByFeedsArray[sender.tag].replyCount)! > 0
        {
            self.confessionReplyLbl.text = self.nearByFeedsArray[sender.tag].replyCount
        }
        else
        {
            self.confessionReplyLbl.text = ""
        }
    }
    
    
    // MARK: - ****** viewFullSizeImageACtion_popular ******
    func viewFullSizeImageACtion_popular(sender: UIButton!)
    {
        self.indexToClick = sender.tag
        
        if (self.popularFeedsArray.count <= 0)
        {
            return
        }
        
        if (self.popularFeedsArray[sender.tag].postedImage != "")
        {
            let imageURL = self.popularFeedsArray[sender.tag].postedImage
            if (imageURL.contains("jpg"))
            {
                self.popularFullImage.sd_setImage(with: URL(string: self.popularFeedsArray[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                    
                    if ((image?.size.height) != nil) && ((image?.size.width) != nil)
                    {
                        self.showFullView_Popular.backgroundColor = UIColor.clear
                        self.popularFullImage.backgroundColor = UIColor.clear
                        self.showFullView_Popular.isHidden = false
                        
                        self.popularFullImage.isHidden = false
                        self.popularAnimatedGifImage.isHidden = true
                        
                        if (image?.size.height)! <= (self.popularGifWrapperView.frame.size.height)
                        {
                            var gifWidth = Int()
                            var screenRes = Int()
                            var newXPos =  Int()
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  0
                            }
                            else{
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  screenRes - gifWidth
                            }
                            
                            let gifHeight = Int((image?.size.height)!/2)
                            let heightOfWrraperView = Int(self.popularGifWrapperView.frame.height/2)
                            let newYPos: Int =  heightOfWrraperView - gifHeight
                            
                            self.popularGifWrapperView.frame.origin.y = 0
                            self.popularFullImage.frame.origin.y = CGFloat(newYPos)
                            self.popularFullImage.frame.origin.x = CGFloat(newXPos)
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                self.popularFullImage.frame.size.width = self.view.frame.size.width
                            }
                            else{
                                self.popularFullImage.frame.size.width = (image?.size.width)!
                            }
                            
                            if (image?.size.height)! <= (self.popularGifWrapperView.frame.size.height)
                            {
                                self.popularFullImage.frame.size.height = (image?.size.height)!
                            }
                            else{
                                self.popularFullImage.frame.size.height = self.popularGifWrapperView.frame.size.height
                            }
                            
                            self.popularButtonsWrapperView.frame.origin.y = self.popularFullImage.frame.origin.y + self.popularFullImage.frame.size.height + 10
                            self.popularGifWrapperView.bringSubview(toFront: self.popularButtonsWrapperView)
                        }
                        else{
                            self.popularFullImage.frame.origin.y = 21
                            self.popularFullImage.frame.size.height = (self.popularGifWrapperView.frame.size.height - 21)
                            
                            self.popularButtonsWrapperView.frame.origin.y = self.popularGifWrapperView.frame.origin.y + self.popularGifWrapperView.frame.size.height + 10
                            self.popularGifWrapperView.bringSubview(toFront: self.popularButtonsWrapperView)
                        }
                        
                        self.popularFullImage.layer.borderWidth = 2.0
                        self.popularFullImage.layer.masksToBounds = false
                        self.popularFullImage.layer.borderColor = UIColor.lightGray.cgColor
                        self.popularFullImage.contentMode = UIViewContentMode.scaleAspectFit
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Please wait.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                self.showFullView_Popular.backgroundColor = UIColor.clear
                self.popularFullImage.backgroundColor = UIColor.clear
                self.showFullView_Popular.isHidden = false
                
                self.popularFullImage.isHidden = true
                self.popularAnimatedGifImage.isHidden = false
               
                DispatchQueue.main.async {
                    self.popularAnimatedGifImage.sd_setImage(with: URL(string: self.popularFeedsArray[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions.refreshCached)
                }
                self.popularAnimatedGifImage.layer.borderWidth = 2.0
                self.popularAnimatedGifImage.layer.masksToBounds = false
                self.popularAnimatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                
                self.popularAnimatedGifImage.contentMode = UIViewContentMode.scaleAspectFit

                var gifWidth = Int()
                var screenRes = Int()
                var newXPos =  Int()
                if (self.popularFeedsArray[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    gifWidth = Int(self.popularFeedsArray[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  0
                }
                else{
                    gifWidth = Int(self.popularFeedsArray[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  screenRes - gifWidth
                }
                
                let gifHeight = Int(self.popularFeedsArray[sender.tag].height/2)
                let heightOfWrraperView = Int(self.popularGifWrapperView.frame.height/2)
                let newYPos: Int =  heightOfWrraperView - gifHeight
                
                self.popularGifWrapperView.frame.origin.y = 0
                self.popularAnimatedGifImage.frame.origin.y = CGFloat(newYPos)
                self.popularAnimatedGifImage.frame.origin.x = CGFloat(newXPos)
                
                if (self.popularFeedsArray[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    self.popularAnimatedGifImage.frame.size.width = self.view.frame.size.width
                }
                else{
                    self.popularAnimatedGifImage.frame.size.width = CGFloat(self.popularFeedsArray[sender.tag].width)
                }
                
                self.popularAnimatedGifImage.frame.size.height = CGFloat(self.popularFeedsArray[sender.tag].height)
                
                self.popularButtonsWrapperView.frame.origin.y = self.popularAnimatedGifImage.frame.origin.y + self.popularAnimatedGifImage.frame.size.height + 10
                self.popularGifWrapperView.bringSubview(toFront: self.popularButtonsWrapperView)
            }
        }
        
        DispatchQueue.main.async {
            
            if self.getFullDataStr == "fetchAllData"
            {
                if (self.popularFeedsArray[sender.tag].likedByMeStatus == "yes")
                {
                    self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
            else
            {
                if self.popularIntArr.contains(sender.tag)
                {
                    self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
        }
        
        if self.popularFeedsArray[sender.tag].likesCount == "0"
        {
            self.popularLikeCountLbl.text = ""
        }
        else
        {
            self.popularLikeCountLbl.text = String(self.popularFeedsArray[sender.tag].likesCount)
        }
        
        if Int(self.popularFeedsArray[sender.tag].replyCount)! == 0
        {
            self.popularReplyCountLbl.text = ""
        }
        else if Int(self.popularFeedsArray[sender.tag].replyCount)! > 0
        {
            self.popularReplyCountLbl.text = self.popularFeedsArray[sender.tag].replyCount
        }
        else
        {
            self.popularReplyCountLbl.text = ""
        }
    }

    // MARK: - ****** viewFullSizeImageACtion ******
    func viewFullSizeImageACtion(sender: UIButton!)
    {
        self.indexToClick = sender.tag
        
        if (self.latestFeedsArray.count <= 0)
        {
            return
        }
        
        if (self.latestFeedsArray[sender.tag].postedImage != "")
        {
            let imageURL = self.latestFeedsArray[sender.tag].postedImage
            if (imageURL.contains("jpg"))
            {
                self.latestFullImageView.sd_setImage(with: URL(string: self.latestFeedsArray[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                  
                    
                    if ((image?.size.height) != nil) && ((image?.size.width) != nil)
                    {
                        self.showFullView_Latest.backgroundColor = UIColor.clear
                        self.showFullView_Latest.isHidden = false
                        self.latestFullImageView.backgroundColor = UIColor.clear
                        
                        self.latestFullImageView.isHidden = false
                        self.latestAnimatedGifImage.isHidden = true
                        
                        if (image?.size.height)! <= (self.latestGifWrapperView.frame.size.height)
                        {
                            var gifWidth = Int()
                            var screenRes = Int()
                            var newXPos =  Int()
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  0
                            }
                            else
                            {
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  screenRes - gifWidth
                            }
                            
                            let gifHeight = Int((image?.size.height)!/2)
                            let heightOfWrraperView = Int(self.latestGifWrapperView.frame.height/2)
                            let newYPos: Int =  heightOfWrraperView - gifHeight
                            
                            self.latestGifWrapperView.frame.origin.y = 0
                            self.latestFullImageView.frame.origin.y = CGFloat(newYPos)
                            self.latestFullImageView.frame.origin.x = CGFloat(newXPos)
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                self.latestFullImageView.frame.size.width = self.view.frame.size.width
                            }
                            else{
                                self.latestFullImageView.frame.size.width = (image?.size.width)!
                            }
                            
                            if (image?.size.height)! <= (self.latestGifWrapperView.frame.size.height)
                            {
                                self.latestFullImageView.frame.size.height = (image?.size.height)!
                            }
                            else{
                                self.latestFullImageView.frame.size.height = self.latestGifWrapperView.frame.size.height
                            }
                            
                            self.latestButtonsWrapperView.frame.origin.y = self.latestFullImageView.frame.origin.y + self.latestFullImageView.frame.size.height + 10
                            self.latestGifWrapperView.bringSubview(toFront: self.latestButtonsWrapperView)
                        }
                        else{
                            self.latestFullImageView.frame.origin.y = 21
                            self.latestFullImageView.frame.size.height = (self.latestGifWrapperView.frame.size.height - 21)
                            
                            self.latestButtonsWrapperView.frame.origin.y = self.latestGifWrapperView.frame.origin.y + self.latestGifWrapperView.frame.size.height + 10
                            self.latestGifWrapperView.bringSubview(toFront: self.latestButtonsWrapperView)
                        }
                        
                        self.latestFullImageView.layer.borderWidth = 2.0
                        self.latestFullImageView.layer.masksToBounds = false
                        self.latestFullImageView.layer.borderColor = UIColor.lightGray.cgColor
                        
                        self.latestFullImageView.contentMode = UIViewContentMode.scaleAspectFit
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Please wait.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
            else
            {
                self.showFullView_Latest.backgroundColor = UIColor.clear
                self.showFullView_Latest.isHidden = false
                self.latestFullImageView.backgroundColor = UIColor.clear
                
                self.latestFullImageView.isHidden = true
                self.latestAnimatedGifImage.isHidden = false
                
                DispatchQueue.main.async {
                    self.latestAnimatedGifImage.sd_setImage(with: URL(string: self.latestFeedsArray[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions.refreshCached)
                }
                
                self.latestAnimatedGifImage.layer.borderWidth = 2.0
                self.latestAnimatedGifImage.layer.masksToBounds = false
                self.latestAnimatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                self.latestAnimatedGifImage.contentMode = UIViewContentMode.scaleAspectFit
 
                var gifWidth = Int()
                var screenRes = Int()
                var newXPos =  Int()
                
                if (self.latestFeedsArray[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    gifWidth = Int(self.latestFeedsArray[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  0
                }
                else{
                    gifWidth = Int(self.latestFeedsArray[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  screenRes - gifWidth
                }
                
                let gifHeight = Int(self.latestFeedsArray[sender.tag].height/2)
                let heightOfWrraperView = Int(self.latestGifWrapperView.frame.height/2)
                let newYPos: Int =  heightOfWrraperView - gifHeight
                
                self.latestGifWrapperView.frame.origin.y = 0
                self.latestAnimatedGifImage.frame.origin.y = CGFloat(newYPos)
                self.latestAnimatedGifImage.frame.origin.x = CGFloat(newXPos)
                
                if (self.latestFeedsArray[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    self.latestAnimatedGifImage.frame.size.width = self.view.frame.size.width
                }
                else{
                    self.latestAnimatedGifImage.frame.size.width = CGFloat(self.latestFeedsArray[sender.tag].width)
                }
                
                self.latestAnimatedGifImage.frame.size.height = CGFloat(self.latestFeedsArray[sender.tag].height)
                
                self.latestButtonsWrapperView.frame.origin.y = self.latestAnimatedGifImage.frame.origin.y + self.latestAnimatedGifImage.frame.size.height + 10
                self.latestGifWrapperView.bringSubview(toFront: self.latestButtonsWrapperView)
            }
        }
        
        
        DispatchQueue.main.async {
            
            if self.getFullDataStr == "fetchAllData"
            {
                if (self.latestFeedsArray[sender.tag].likedByMeStatus == "yes")
                {
                    self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
            else{
                if self.latestIntArr.contains(sender.tag)
                {
                    self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
        }
        
        if self.latestFeedsArray[sender.tag].likesCount == "0"
        {
            self.latestLikeCountLbl.text = ""
        }
        else
        {
            self.latestLikeCountLbl.text = String(self.latestFeedsArray[sender.tag].likesCount)
        }
        
        if Int(self.latestFeedsArray[sender.tag].replyCount)! == 0
        {
            self.latestReplyCountLbl.text = ""
        }
        else if Int(self.latestFeedsArray[sender.tag].replyCount)! > 0
        {
            self.latestReplyCountLbl.text = self.latestFeedsArray[sender.tag].replyCount
        }
        else
        {
            self.latestReplyCountLbl.text = ""
        }
    }

    
     // MARK: - ****** confessionReplyNearBy_btnAction ******
    func confessionReplyNearBy_btnAction(sender: UIButton!) {
        
        self.search_txtField.resignFirstResponder()
        
        if (self.nearByFeedsArray.count > 0)
        {
            var typeOfCOnfession = String()
            if (self.nearByFeedsArray[sender.tag].postedImage != "")
            {
                let imageURL = self.nearByFeedsArray[sender.tag].postedImage
                if (imageURL.contains("jpg"))
                {
                    typeOfCOnfession = "photo"
                }
                else
                {
                    typeOfCOnfession = "gif"
                }
            }
            else
            {
                typeOfCOnfession = "text"
            }
            
            print("typeOfCOnfession = \(typeOfCOnfession)")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
            
            var dictForConfessions = [String: String]()
            dictForConfessions=["age": self.nearByFeedsArray[sender.tag].age,
                                "gender": self.nearByFeedsArray[sender.tag].gender,
                                "username": self.nearByFeedsArray[sender.tag].name,
                                "location": self.nearByFeedsArray[sender.tag].location,
                                "totalTime": String(self.nearByFeedsArray[sender.tag].totalTime),
                                "typeOfConfession": self.nearByFeedsArray[sender.tag].typeOfConfession,
                                "ownerProfilePic": self.nearByFeedsArray[sender.tag].profilePic,
                                "statusAdded": self.nearByFeedsArray[sender.tag].statusAdded
            ]
            homeScreen.ConfessionsDataDict = dictForConfessions
            homeScreen.checkReplyType = "replyOnPost"
            homeScreen.postIdOwnerStr = self.nearByFeedsArray[sender.tag].userIDStr
            homeScreen.postIDStr = self.nearByFeedsArray[sender.tag].postIDStr
            homeScreen.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
            homeScreen.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
            homeScreen.currentLocationFromHomePage = self.currentLocationFromHomePage
            self.navigationController?.pushViewController(homeScreen, animated: true)
            
        }
    }
    
    
    
    // MARK: - ****** confessionReplyLatest_btnAction ******
    func confessionReplyLatest_btnAction(sender: UIButton!)
    {
        self.search_txtField.resignFirstResponder()
        
        if (self.latestFeedsArray.count > 0)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
            
            appDel.confessionLocationPoint = sender.tag
            print("appDel.confessionLocationPoint = \(appDel.confessionLocationPoint)")
            
            var dictForConfessions = [String: String]()
            dictForConfessions=["age": self.latestFeedsArray[sender.tag].age,
                                "gender": self.latestFeedsArray[sender.tag].gender,
                                "username": self.latestFeedsArray[sender.tag].name,
                                "location": self.latestFeedsArray[sender.tag].location,
                                "totalTime": String(self.latestFeedsArray[sender.tag].totalTime),
                                "typeOfConfession": self.latestFeedsArray[sender.tag].typeOfConfession,
                                "ownerProfilePic": self.latestFeedsArray[sender.tag].profilePic,
                                "statusAdded": self.latestFeedsArray[sender.tag].statusAdded
            ]
            homeScreen.ConfessionsDataDict = dictForConfessions
            
            homeScreen.checkReplyType = "replyOnPost"
            homeScreen.postIdOwnerStr = self.latestFeedsArray[sender.tag].userIDStr
            homeScreen.postIDStr = self.latestFeedsArray[sender.tag].postIDStr
            homeScreen.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
            homeScreen.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
            homeScreen.currentLocationFromHomePage = self.currentLocationFromHomePage
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
    }
    
    // MARK: - ****** confessionReplyPopular_btnAction ******
    func confessionReplyPopular_btnAction(sender: UIButton!) {
        self.search_txtField.resignFirstResponder()
        
        if (self.popularFeedsArray.count > 0)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
            
            var dictForConfessions = [String: String]()
            dictForConfessions=["age": self.popularFeedsArray[sender.tag].age,
                                "gender": self.popularFeedsArray[sender.tag].gender,
                                "username": self.popularFeedsArray[sender.tag].name,
                                "location": self.popularFeedsArray[sender.tag].location,
                                "totalTime": String(self.popularFeedsArray[sender.tag].totalTime),
                                "typeOfConfession": self.popularFeedsArray[sender.tag].typeOfConfession,
                                "ownerProfilePic": self.popularFeedsArray[sender.tag].profilePic,
                                "statusAdded": self.popularFeedsArray[sender.tag].statusAdded
            ]
            homeScreen.ConfessionsDataDict = dictForConfessions
            homeScreen.checkReplyType = "replyOnPost"
            homeScreen.postIdOwnerStr = self.popularFeedsArray[sender.tag].userIDStr
            homeScreen.postIDStr = self.popularFeedsArray[sender.tag].postIDStr
            homeScreen.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
            homeScreen.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
            homeScreen.currentLocationFromHomePage = self.currentLocationFromHomePage
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        
    }
    
    
     // MARK: - ****** confessionMessagePopular_btnAction ******
    func confessionMessagePopular_btnAction(sender: UIButton!) {

        if (self.popularFeedsArray.count > 0)
        {
            let indexPathRow = IndexPath(row: sender.tag, section: 0)
            let userOwnerIDStr =  self.popularFeedsArray[sender.tag].userIDStr
            if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if id == userOwnerIDStr
                {
                    print("owner of the post and current user is same")
                }
                else
                {
                    let ownerProfileImage = UIImageView()
                    ownerProfileImage.sd_setImage(with: URL(string: self.popularFeedsArray[sender.tag].postedImage), placeholderImage: UIImage(named: "Default Icon"))
                    
                    let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                    messagesScreen.chatUserDifferentiateStr = "Messages"
                    messagesScreen.viewOpponentProfile = "fromStatusScreen"
                    
                    let userIDStr =  self.popularFeedsArray[sender.tag].userIDStr
                    
                    let firebase_user_id =  self.popularFeedsArray[sender.tag].firebase_user_id
                    
                    messagesScreen.opponentName = self.popularFeedsArray[sender.tag].name
                    messagesScreen.opponentImage = self.popularFeedsArray[sender.tag].profilePic
                    messagesScreen.opponentUserID = userIDStr
                    messagesScreen.opponentFirebaseUserID = firebase_user_id
                    messagesScreen.postID = self.popularFeedsArray[sender.tag].postIDStr
                    messagesScreen.opponentProfilePic =  ownerProfileImage.image!
                    
                    if (self.popularFeedsArray[sender.tag].typeOfConfession == "anonymous")
                    {
                        messagesScreen.typeOfUser = "AnonymousUser"
                        messagesScreen.checkVisibiltyStr = "comingFromconfession"
                        messagesScreen.confessionTypeUser = "AnonymousUser"
                    }
                    else
                    {
                        messagesScreen.typeOfUser = "AnonymousUser"
                        messagesScreen.checkVisibiltyStr = "comingFromconfession"
                        messagesScreen.confessionTypeUser = "WithProfileConfessionUser"
                    }
                    
                    if (self.popularFeedsArray[sender.tag].postedImage != "")
                    {
                        let Cell = self.popularConfessions_tableView.cellForRow(at: indexPathRow) as! CreditPackagesTableViewCell
                        
                        let imageURL = self.popularFeedsArray[sender.tag].postedImage
                        if (imageURL.contains("jpg"))
                        {
                            messagesScreen.postedConfessionImageType = "photo"
                        }
                        else{
                            messagesScreen.postedConfessionImageType = "gif"
                        }
                        
                        messagesScreen.postedConfessionStatus = Cell.usersStatus_TxtView.text!
                        messagesScreen.postedConfessionData = "textWithImage"
                        messagesScreen.userPostedConfessionImageURL = self.popularFeedsArray[sender.tag].postedImage
                        
                        messagesScreen.userPostedConfessionImage.animatedImage = Cell.animatedGifImage.animatedImage
                    }
                    else
                    {
                        let onlineUsersCell = self.popularConfessions_tableView.cellForRow(at: indexPathRow) as! ConfessionStatusTableViewCell
                        
                        messagesScreen.postedConfessionStatus = onlineUsersCell.usersStatusTextView.text!
                        messagesScreen.postedConfessionData = "onlyStatusText"
                        messagesScreen.userPostedConfessionImageURL = ""
                        messagesScreen.postedConfessionImageType = "text"
                        messagesScreen.userPostedConfessionImage.animatedImage = nil
                    }
                    
                    messagesScreen.mainConfessionCheck = "comingFromconfession"
                    
                    self.navigationController?.pushViewController(messagesScreen, animated: true)
                    
                }
            }
        }
       
    }
    
    // MARK: - ****** confessionMessageLatest_btnAction ******
    func confessionMessageLatest_btnAction(sender: UIButton!) {
        
        if (self.latestFeedsArray.count > 0)
        {
            let indexPathRow = IndexPath(row: sender.tag, section: 0)
            let userOwnerIDStr = self.latestFeedsArray[sender.tag].userIDStr
            if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if id == userOwnerIDStr
                {
                    print("owner of the post and current user is same")
                }
                else
                {
                    let ownerProfileImage = UIImageView()
                    ownerProfileImage.sd_setImage(with: URL(string: self.latestFeedsArray[sender.tag].postedImage), placeholderImage: UIImage(named: "Default Icon"))
                    
                    let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                    messagesScreen.chatUserDifferentiateStr = "Messages"
                    messagesScreen.viewOpponentProfile = "fromStatusScreen"
                    let userIDStr =  self.latestFeedsArray[sender.tag].userIDStr
                    let firebase_user_id =  self.latestFeedsArray[sender.tag].firebase_user_id
                    
                    messagesScreen.opponentName = self.latestFeedsArray[sender.tag].name
                    messagesScreen.opponentImage = self.latestFeedsArray[sender.tag].profilePic
                    messagesScreen.opponentUserID = userIDStr
                    messagesScreen.opponentFirebaseUserID = firebase_user_id
                    messagesScreen.postID = self.latestFeedsArray[sender.tag].postIDStr
                    messagesScreen.opponentProfilePic =  ownerProfileImage.image!
                    
                    if (self.latestFeedsArray[sender.tag].typeOfConfession == "anonymous")
                    {
                        messagesScreen.typeOfUser = "AnonymousUser"
                        messagesScreen.checkVisibiltyStr = "comingFromconfession"
                        messagesScreen.confessionTypeUser = "AnonymousUser"
                    }
                    else
                    {
                        messagesScreen.typeOfUser = "AnonymousUser"
                        messagesScreen.checkVisibiltyStr = "comingFromconfession"
                        messagesScreen.confessionTypeUser = "WithProfileConfessionUser"
                    }
                    
                    if (self.latestFeedsArray[sender.tag].postedImage != "")
                    {
                        let Cell = self.latestConfessions_tableView.cellForRow(at: indexPathRow) as! CreditPackagesTableViewCell
                        
                        let imageURL = self.latestFeedsArray[sender.tag].postedImage
                        if (imageURL.contains("jpg"))
                        {
                            messagesScreen.postedConfessionImageType = "photo"
                        }
                        else{
                            messagesScreen.postedConfessionImageType = "gif"
                        }
                        
                        messagesScreen.postedConfessionStatus = Cell.usersStatus_TxtView.text!
                        messagesScreen.postedConfessionData = "textWithImage"
                        messagesScreen.userPostedConfessionImageURL = self.latestFeedsArray[sender.tag].postedImage
                        messagesScreen.userPostedConfessionImage.animatedImage = Cell.animatedGifImage.animatedImage
                    }
                    else
                    {
                        let onlineUsersCell = self.latestConfessions_tableView.cellForRow(at: indexPathRow) as! ConfessionStatusTableViewCell
                        
                        messagesScreen.postedConfessionStatus = onlineUsersCell.usersStatusTextView.text!
                        messagesScreen.postedConfessionData = "onlyStatusText"
                        messagesScreen.userPostedConfessionImageURL = ""
                        messagesScreen.postedConfessionImageType = "text"
                        messagesScreen.userPostedConfessionImage.animatedImage = nil
                    }
                    
                    messagesScreen.mainConfessionCheck = "comingFromconfession"
                    
                    self.navigationController?.pushViewController(messagesScreen, animated: true)
                }
            }
        }
       
    }
    
    
    // MARK: - ****** confessionMessageNearBy_btnAction ******
    func confessionMessageNearBy_btnAction(sender: UIButton!) {
        
        if (self.nearByFeedsArray.count > 0)
        {
            let indexPathRow = IndexPath(row: sender.tag, section: 0)
            let userOwnerIDStr = self.nearByFeedsArray[sender.tag].userIDStr
            if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if id == userOwnerIDStr
                {
                    print("owner of the post and current user is same")
                }
                else
                {
                    let ownerProfileImage = UIImageView()
                    ownerProfileImage.sd_setImage(with: URL(string: self.nearByFeedsArray[sender.tag].postedImage), placeholderImage: UIImage(named: "Default Icon"))
                    
                    let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                    messagesScreen.chatUserDifferentiateStr = "Messages"
                    
                    messagesScreen.viewOpponentProfile = "fromStatusScreen" //"fromListingScreen"
                    
                    let userIDStr =  self.nearByFeedsArray[sender.tag].userIDStr
                    let firebase_user_id =  self.nearByFeedsArray[sender.tag].firebase_user_id
                    messagesScreen.opponentName = self.nearByFeedsArray[sender.tag].name
                    messagesScreen.opponentImage = self.nearByFeedsArray[sender.tag].profilePic
                    messagesScreen.opponentUserID = userIDStr
                    messagesScreen.opponentFirebaseUserID = firebase_user_id
                    messagesScreen.postID = self.nearByFeedsArray[sender.tag].postIDStr
                    messagesScreen.opponentProfilePic =  ownerProfileImage.image!
                    
                    if (self.nearByFeedsArray[sender.tag].typeOfConfession == "anonymous")
                    {
                        messagesScreen.typeOfUser = "AnonymousUser"
                        messagesScreen.checkVisibiltyStr = "comingFromconfession"
                        messagesScreen.confessionTypeUser = "AnonymousUser"
                    }
                    else
                    {
                        messagesScreen.typeOfUser = "AnonymousUser"
                        messagesScreen.checkVisibiltyStr = "comingFromconfession"
                        messagesScreen.confessionTypeUser = "WithProfileConfessionUser"
                    }
                    
                    
                    if (self.nearByFeedsArray[sender.tag].postedImage != "")
                    {
                        let Cell = self.nearByConfessions_tableView.cellForRow(at: indexPathRow) as! CreditPackagesTableViewCell
                        
                        let imageURL = self.nearByFeedsArray[sender.tag].postedImage
                        if (imageURL.contains("jpg"))
                        {
                            messagesScreen.postedConfessionImageType = "photo"
                        }
                        else{
                            messagesScreen.postedConfessionImageType = "gif"
                        }
                        
                        messagesScreen.postedConfessionStatus = Cell.usersStatus_TxtView.text!
                        messagesScreen.postedConfessionData = "textWithImage"
                        messagesScreen.userPostedConfessionImageURL = self.nearByFeedsArray[sender.tag].postedImage
                        
                        messagesScreen.userPostedConfessionImage.animatedImage = Cell.animatedGifImage.animatedImage
                    }
                    else
                    {
                        let onlineUsersCell = self.nearByConfessions_tableView.cellForRow(at: indexPathRow) as! ConfessionStatusTableViewCell
                        
                        messagesScreen.postedConfessionStatus = onlineUsersCell.usersStatusTextView.text!
                        messagesScreen.postedConfessionData = "onlyStatusText"
                        messagesScreen.userPostedConfessionImageURL = ""
                        messagesScreen.postedConfessionImageType = "text"
                        messagesScreen.userPostedConfessionImage.animatedImage = nil
                    }
                    
                    messagesScreen.mainConfessionCheck = "comingFromconfession"
                    
                    self.navigationController?.pushViewController(messagesScreen, animated: true)
                }
            }
        }
    }
    
    
     // MARK: - ****** DropDownLatest_btnAction ******
    func DropDownLatest_btnAction(sender: UIButton!) {
        
        if (self.latestFeedsArray.count > 0)
        {
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
//                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
//                {
                    if (self.latestFeedsArray[sender.tag].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.latestFeedsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.latestFeedsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        print("typeOfCOnfession = \(typeOfCOnfession)")
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: self.latestFeedsArray[sender.tag].postIDStr, status: self.latestFeedsArray[sender.tag].statusAdded, backgroudImageStr: self.latestFeedsArray[sender.tag].postedImage, moodTypeImageStr: self.latestFeedsArray[sender.tag].moodTypeStr, timeFrameStr: self.latestFeedsArray[sender.tag].postHours, moodTypeStr: self.latestFeedsArray[sender.tag].moodTypeStr, moodTypeUrl: self.latestFeedsArray[sender.tag].moodTypeStr, photoTypeStr: typeOfCOnfession, getTotalLikeCount: self.latestFeedsArray[sender.tag].likesCount, getTotalReplyCount: self.latestFeedsArray[sender.tag].replyCount, gifHeight: self.latestFeedsArray[sender.tag].width, gifWidth: self.latestFeedsArray[sender.tag].height, postStatusStr: self.latestFeedsArray[sender.tag].statusAdded)
                    }
                    else
                    {
                        self.showActionSheetForDropDownMenu_otherUser(myID: userID, postStatusStr: self.latestFeedsArray[sender.tag].statusAdded, postIDStr: self.latestFeedsArray[sender.tag].postIDStr, oppoUserID: self.latestFeedsArray[sender.tag].userIDStr)
                    }
//                }
            }
        }
    }
    
    
    // MARK: - ****** DropDownPopular_btnAction ******
    func DropDownPopular_btnAction(sender: UIButton!) {
        
        if (self.popularFeedsArray.count > 0)
        {
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
//                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
//                {
                    if (self.popularFeedsArray[sender.tag].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.popularFeedsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.popularFeedsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        print("typeOfCOnfession = \(typeOfCOnfession)")
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: self.popularFeedsArray[sender.tag].postIDStr, status: self.popularFeedsArray[sender.tag].statusAdded, backgroudImageStr: self.popularFeedsArray[sender.tag].postedImage, moodTypeImageStr: self.popularFeedsArray[sender.tag].moodTypeStr, timeFrameStr: self.popularFeedsArray[sender.tag].postHours, moodTypeStr: self.popularFeedsArray[sender.tag].moodTypeStr, moodTypeUrl: self.popularFeedsArray[sender.tag].moodTypeStr, photoTypeStr: typeOfCOnfession, getTotalLikeCount: self.popularFeedsArray[sender.tag].likesCount, getTotalReplyCount: self.popularFeedsArray[sender.tag].replyCount, gifHeight: self.popularFeedsArray[sender.tag].width, gifWidth: self.popularFeedsArray[sender.tag].height, postStatusStr: self.popularFeedsArray[sender.tag].statusAdded)
                    }
                    else
                    {
                        self.showActionSheetForDropDownMenu_otherUser(myID: userID, postStatusStr: self.popularFeedsArray[sender.tag].statusAdded, postIDStr: self.popularFeedsArray[sender.tag].postIDStr, oppoUserID: self.popularFeedsArray[sender.tag].userIDStr)
                    }
//                }
            }
        }
    }
    
    // MARK: - ****** DropDownNearBy_btnAction ******
    func DropDownNearBy_btnAction(sender: UIButton!) {
        
        if (self.nearByFeedsArray.count > 0)
        {
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
//                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
//                {
                    if (self.nearByFeedsArray[sender.tag].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.nearByFeedsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.nearByFeedsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        print("typeOfCOnfession = \(typeOfCOnfession)")
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: self.nearByFeedsArray[sender.tag].postIDStr, status: self.nearByFeedsArray[sender.tag].statusAdded, backgroudImageStr: self.nearByFeedsArray[sender.tag].postedImage, moodTypeImageStr: self.nearByFeedsArray[sender.tag].moodTypeStr, timeFrameStr: self.nearByFeedsArray[sender.tag].postHours, moodTypeStr: self.nearByFeedsArray[sender.tag].moodTypeStr, moodTypeUrl: self.nearByFeedsArray[sender.tag].moodTypeStr, photoTypeStr: typeOfCOnfession, getTotalLikeCount: self.nearByFeedsArray[sender.tag].likesCount, getTotalReplyCount: self.nearByFeedsArray[sender.tag].replyCount, gifHeight: self.nearByFeedsArray[sender.tag].width, gifWidth: self.nearByFeedsArray[sender.tag].height, postStatusStr: self.nearByFeedsArray[sender.tag].statusAdded)
                    }
                    else
                    {
                        self.showActionSheetForDropDownMenu_otherUser(myID: userID, postStatusStr: self.nearByFeedsArray[sender.tag].statusAdded, postIDStr: self.nearByFeedsArray[sender.tag].postIDStr, oppoUserID: self.nearByFeedsArray[sender.tag].userIDStr)
                    }
//                }
            }
        }
    }
    
    
    func reportParticularConfession(postIDToReport: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.view.isUserInteractionEnabled = false
                    self.loadingView.isHidden = false
                    
                    ApiHandler.reportContentOrUser(userID: userID, authToken: authToken, typeStr: "3", content_id: postIDToReport, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                   
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    func blockConfessionOwner(confesionOWnerID: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.view.isUserInteractionEnabled = false
                    self.loadingView.isHidden = false
                    
                    ApiHandler.blockUnblockUser(userID: userID, authToken: authToken, other_user_id: confesionOWnerID, typeOfBlockUser: "confessions", completion: { (responseData) in
                 
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                           
                             let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                             alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                             action in
                             
                             }))
                             self.present(alert, animated: true, completion: nil)
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    
    // MARK: - ****** Show Action Sheet From select options. ******
    func showActionSheetForDropDownMenu_otherUser(myID: String, postStatusStr: String, postIDStr: String, oppoUserID: String) {

        let actionSheet = UIAlertController(title: "Please choose your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Report", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.reportBool = true
            self.reportParticularConfession(postIDToReport: postIDStr, completion: { (responseData) in
                
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
                
                if (responseData == true)
                {
                   self.getConfessionsDataOnScroll()
                }
            })
        }))

        
        if (postStatusStr == "removedFromConfessionListing")
        {
            actionSheet.addAction(UIAlertAction(title: "Put back to Wi Confessions", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
                
                if postIDStr != ""
                {
//                    self.unHideConfessionFromUsers(postIDStr: postIdStr)
                }
            }))
        }
        else
        {
            actionSheet.addAction(UIAlertAction(title: "Hide From My Feed", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
                
                if postIDStr != ""
                {
                    self.hideConfessionFromUsers(postIDStr: postIDStr, completion: { (responseData) in
                        
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        
                        if (responseData == true)
                        {
                            self.getConfessionsDataOnScroll()
                        }
                    })
                }
            }))
        }
        
        var blockActionButton = UIAlertAction()
    
        blockActionButton = UIAlertAction(title: "Block User", style: .destructive) { action -> Void in
            
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                       
                        let alert = UIAlertController(title: "", message: "Do you really want to block?", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
                        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
             
                            self.blockConfessionOwner(confesionOWnerID: oppoUserID, completion: { (responseData) in
                                
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                
                                if (responseData == true)
                                {
                                    self.getConfessionsDataOnScroll()
                                }
                            })
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                }

        actionSheet.addAction(blockActionButton)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    //MARK:- ****** deleteConfession ******
    func hideConfessionFromUsers(postIDStr: String, completion: @escaping (Bool) -> Swift.Void)
    {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "Hide From My Feed", message: "Do you really want to hide from your feed?", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }
        
        actionSheetController.addAction(cancelActionButton)
        
        let reportActionButton = UIAlertAction(title: "Hide From My Feed", style: .destructive) { action -> Void in
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
               
                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                    {
                        self.view.isUserInteractionEnabled = false
                        self.loadingView.isHidden = false
                        
                        ApiHandler.hideConfessionFromMyPosts(userID: userID, userToken: authToken, confession_id: postIDStr, completion: { (responseData) in
                            
//                            print("responseData = \(responseData)")
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let message = responseData.value(forKey: "message") as? String
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
            }
        }
        
        actionSheetController.addAction(reportActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }

    
    // MARK: - ****** Show Action Sheet From select options. ******
    func showActionSheetForDropDownMenu_owner(postIdStr: String, status: String, backgroudImageStr : String, moodTypeImageStr: String, timeFrameStr: String, moodTypeStr: String, moodTypeUrl: String, photoTypeStr: String,getTotalLikeCount: String, getTotalReplyCount: String,gifHeight: Int, gifWidth: Int, postStatusStr: String) {
        
        let actionSheet = UIAlertController(title: "Please choose your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Edit", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let createConfession = storyboard.instantiateViewController(withIdentifier: "WiCreateConfessionViewController") as! WiCreateConfessionViewController
            createConfession.updateStr = "EditConfession"
            createConfession.updateMoodUrl = moodTypeUrl
            createConfession.updateTimeframe = timeFrameStr
            createConfession.updatePhotoTypeStr = photoTypeStr
            createConfession.updateStatusText = status
            createConfession.updateBackgroundImageUrl = backgroudImageStr
            createConfession.updateMoodTypeStr = moodTypeStr
            createConfession.updatePostID = postIdStr
            createConfession.postTypeStr = "CreateNewPost"
            createConfession.updateReplyToReplyID = ""
            createConfession.updateLikeCountStr = getTotalLikeCount
            createConfession.updateReplyCountStr = getTotalReplyCount
            createConfession.updateGifWidth = gifWidth
            createConfession.updateGifHeight = gifHeight
            createConfession.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
            createConfession.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
            createConfession.currentLocationFromHomePage = self.currentLocationFromHomePage
            self.navigationController?.pushViewController(createConfession, animated: true)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            if postIdStr != ""
            {
                self.decreaseCountBool = true
                self.deleteConfessionWithMessage(postIDStrToDelete: postIdStr)
            }
        }))
 
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** deleteConfession ******
    func deleteConfessionWithMessage(postIDStrToDelete: String)
    {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "Delete Wi Post", message: "Do you really want to delete post?", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }
        actionSheetController.addAction(cancelActionButton)
        
        let reportActionButton = UIAlertAction(title: "Delete", style: .destructive) { action -> Void in
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
             
                self.deleteParticularConfession(postIDToDelete: postIDStrToDelete, completion: { (responseBool) in
                    
                    if (responseBool == true)
                    {
                        self.getFullDataStr = "fetchAllData"
                        
                        if (self.tabsBtnStr == "nearBy")
                        {
                            self.nearByFeedsArray = []
                            self.getNearByConfessionListing(pageValue:  self.currentPageNearBy,searchText: self.search_txtField.text!, completion: { (responseBool) in
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                if (responseBool == true)
                                {
                                    DispatchQueue.main.async {
                                        self.noNearByView.isHidden = true
                                        self.nearByConfessions_tableView.isHidden = false
                                        self.nearByConfessions_tableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                    }
                                }
                                else
                                {
                                    self.nearByFeedsArray = []
                                    self.noNearBy_Lbl.text = "No nearby confessions found"
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.nearByConfessions_tableView.isHidden = false
                                    self.noNearByView.isHidden = false
                                    
                                    DispatchQueue.main.async {
                                        self.nearByConfessions_tableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                    }
                                }
                            })
                        }
                        else if (self.tabsBtnStr == "popular")
                        {
                            self.popularFeedsArray = []
                            self.getPopularConfessionListing(pageValue: self.currentPagePopular , searchText: self.search_txtField.text!, completion: { (response) in
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                
                                if (response == true)
                                {
                                    DispatchQueue.main.async {
                                        self.noPopularView.isHidden = true
                                        self.popularConfessions_tableView.isHidden = false
                                        self.popularConfessions_tableView.reloadData()
                                        self.refreshControl_popularTableView.endRefreshing()
                                    }
                                }
                                else
                                {
                                    self.popularFeedsArray = []
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.popularConfessions_tableView.isHidden = false
                                    self.noPopularView.isHidden = false
                                    DispatchQueue.main.async {
                                        self.popularConfessions_tableView.reloadData()
                                        self.refreshControl_popularTableView.endRefreshing()
                                    }
                                }
                            })
                        }
                        else if (self.tabsBtnStr == "latest")
                        {
                            self.latestFeedsArray = []
                            self.latestFeedsCopyArray = []
                            self.getLatestConfessionListing(paginationValueCount: self.currentPageLatest,searchText: self.search_txtField.text!, completion: { (response) in
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                
                                if (response == true)
                                {
                                    DispatchQueue.main.async {
                                        self.noLatestView.isHidden = true
                                        self.latestConfessions_tableView.isHidden = false
                                        self.latestConfessions_tableView.reloadData()
                                        self.refreshControl_latestTableView.endRefreshing()
                                    }
                                }
                                else
                                {
                                    self.latestFeedsArray = []
                                    self.latestFeedsCopyArray = []
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.latestConfessions_tableView.isHidden = false
                                    self.noLatestView.isHidden = false
                                    DispatchQueue.main.async {
                                        self.latestConfessions_tableView.reloadData()
                                        self.refreshControl_latestTableView.endRefreshing()
                                    }
                                }
                            })
                        }
                    }
                })
            }
        }
        
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
        actionSheetController.addAction(reportActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    func deleteParticularConfession(postIDToDelete: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    ApiHandler.deleteConfessions(userID: userID, userToken: authToken, confession_id: postIDToDelete, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                            print("dataDict = \(String(describing: dataDict))")
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
//                            print("message = \(String(describing: message))")
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    
    // MARK: - ****** deleteConfession ******
    func deleteConfession(postIDStr: String)
    {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "Delete Wi Confession", message: "Do you really want to delete confession?", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }
        actionSheetController.addAction(cancelActionButton)
        
        let reportActionButton = UIAlertAction(title: "Delete", style: .destructive) { action -> Void in
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                print("Connected via WWAN")
              
            }
        }
        
        actionSheetController.addAction(reportActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** reportConfession ******
    func reportConfession(postIDStr: String)
    {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "Report Wi Confession", message: "Do you really want to report?", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let reportActionButton = UIAlertAction(title: "Report", style: .destructive) { action -> Void in
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                print("Connected via WWAN")
            
            }
        }
        
        actionSheetController.addAction(reportActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
 
    
    //MARK:- ****** TIME DIFFERENCE FROM TIMESTAMP AND CURRENT TIME ******
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        /// GET TIMESTAMP IN UNIX
        let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        
        let nYears = timeDiff / (1000*60*60*24*365)
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        var timeMsg = ""
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            timeMsg = " \(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            timeMsg = " \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            timeMsg = " \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = " \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            timeMsg = " \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }

    
    // This function converts from HTML colors (hex strings of the form '#ffffff') to UIColors
    func hexStringToUIColor (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
        
    //MARK:- ****** popularClose_btnAction ******
    @IBAction func popularClose_btnAction(_ sender: Any) {
        self.showFullView_Popular.isHidden = true
    }
    
    //MARK:- ****** popularDropDown_btnAction ******
    @IBAction func popularDropDown_btnAction(_ sender: UIButton) {
        
        if (self.popularFeedsArray.count > 0)
        {
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    if (self.popularFeedsArray[sender.tag].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.popularFeedsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.popularFeedsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        print("typeOfCOnfession = \(typeOfCOnfession)")
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: self.popularFeedsArray[sender.tag].postIDStr, status: self.popularFeedsArray[sender.tag].statusAdded, backgroudImageStr: self.popularFeedsArray[sender.tag].postedImage, moodTypeImageStr: self.popularFeedsArray[sender.tag].moodTypeStr, timeFrameStr: self.popularFeedsArray[sender.tag].postHours, moodTypeStr: self.popularFeedsArray[sender.tag].moodTypeStr, moodTypeUrl: self.popularFeedsArray[sender.tag].moodTypeStr, photoTypeStr: typeOfCOnfession, getTotalLikeCount: self.popularFeedsArray[sender.tag].likesCount, getTotalReplyCount: self.popularFeedsArray[sender.tag].replyCount, gifHeight: self.popularFeedsArray[sender.tag].width, gifWidth: self.popularFeedsArray[sender.tag].height, postStatusStr: self.popularFeedsArray[sender.tag].statusAdded)
                    }
                    else
                    {
                        self.showActionSheetForDropDownMenu_otherUser(myID: userID, postStatusStr: self.popularFeedsArray[sender.tag].statusAdded, postIDStr: self.popularFeedsArray[sender.tag].postIDStr, oppoUserID: self.popularFeedsArray[sender.tag].userIDStr)
                    }
                }
            }
        }
    }
    
    
    //MARK:- ****** popularMessage_btnAction ******
    @IBAction func popularMessage_btnAction(_ sender: UIButton) {
        
        let indexPathRow = IndexPath(row: self.indexToClick, section: 0)
        let userOwnerIDStr =  self.popularFeedsArray[self.indexToClick].userIDStr
        if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if id == userOwnerIDStr
            {
                print("owner of the post and current user is same")
            }
            else
            {
                let ownerProfileImage = UIImageView()
                ownerProfileImage.sd_setImage(with: URL(string: self.popularFeedsArray[self.indexToClick].postedImage), placeholderImage: UIImage(named: "Default Icon"))
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromStatusScreen" //"fromListingScreen"
                
                let userIDStr =  self.popularFeedsArray[self.indexToClick].userIDStr
                
                let firebase_user_id =  self.popularFeedsArray[self.indexToClick].firebase_user_id
                
                messagesScreen.opponentName = self.popularFeedsArray[self.indexToClick].name
                messagesScreen.opponentImage = self.popularFeedsArray[self.indexToClick].profilePic
                messagesScreen.opponentUserID = userIDStr
                messagesScreen.opponentFirebaseUserID = firebase_user_id
                messagesScreen.postID = self.popularFeedsArray[self.indexToClick].postIDStr
                messagesScreen.opponentProfilePic =  ownerProfileImage.image!
                
                if (self.popularFeedsArray[self.indexToClick].typeOfConfession == "anonymous")
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                    messagesScreen.confessionTypeUser = "AnonymousUser"
                }
                else
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                    messagesScreen.confessionTypeUser = "WithProfileConfessionUser"
                }
                
                if (self.popularFeedsArray[self.indexToClick].postedImage != "")
                {
                    let Cell = self.popularConfessions_tableView.cellForRow(at: indexPathRow) as! CreditPackagesTableViewCell
                    
                    let imageURL = self.popularFeedsArray[self.indexToClick].postedImage
                    if (imageURL.contains("jpg"))
                    {
                        messagesScreen.postedConfessionImageType = "photo"
                    }
                    else{
                        messagesScreen.postedConfessionImageType = "gif"
                    }
                    
                    messagesScreen.postedConfessionStatus = Cell.usersStatus_TxtView.text!
                    messagesScreen.postedConfessionData = "textWithImage"
                    messagesScreen.userPostedConfessionImageURL = self.popularFeedsArray[self.indexToClick].postedImage
                    
                    messagesScreen.userPostedConfessionImage.animatedImage = Cell.animatedGifImage.animatedImage
                }
                else
                {
                    let onlineUsersCell = self.popularConfessions_tableView.cellForRow(at: indexPathRow) as! ConfessionStatusTableViewCell
                    
                    messagesScreen.postedConfessionStatus = onlineUsersCell.usersStatusTextView.text!
                    messagesScreen.postedConfessionData = "onlyStatusText"
                    messagesScreen.userPostedConfessionImageURL = ""
                    messagesScreen.postedConfessionImageType = "text"
                    messagesScreen.userPostedConfessionImage.animatedImage = nil
                }
           
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }
        }
    }
    
    
    //MARK:- ****** popularLike_btnAction ******
    @IBAction func popularLike_btnAction(_ sender: UIButton) {
        self.getButtonClickStr = "popularLike_btnAction"
        self.getReloadIndexPath = self.indexToClick
        self.checkLikeBtnBool = true
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.popularFeedsArray.count > 0)
            {
                self.likeDislikeConfessions(confessionID: self.popularFeedsArray[self.indexToClick].postIDStr, completion: { (responseBool) in
                    
//                    print("responseBool = \(responseBool)")
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected_popular = IndexPath()
                        self.indexPathSelected_popular = IndexPath(row: self.indexToClick, section: 0)
                        
                        self.popularIntArr.add(self.indexPathSelected_popular.row)
                        
                        var typeOfCOnfession = String()
                        if (self.popularFeedsArray[self.indexToClick].postedImage != "")
                        {
                            let imageURL = self.popularFeedsArray[self.indexToClick].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.popularConfessions_tableView.cellForRow(at: self.indexPathSelected_popular) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.popularFeedsArray[self.indexPathSelected_popular.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if  (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                    
                                    self.popularFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                    self.popularFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                    
                                    self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                    self.popularLikeCountLbl.text = String(totalLikes)
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.popularFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.popularFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                        self.popularLikeCountLbl.text = String(totalLikes)
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.popularFeedsArray[self.indexPathSelected_popular.row].likesCount)! + 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.popularFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.popularFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                        self.popularLikeCountLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.popularConfessions_tableView.cellForRow(at: self.indexPathSelected_popular) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.popularFeedsArray[self.indexPathSelected_popular.row].likesCount
                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.popularFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                    self.popularFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                    
                                    self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                    self.popularLikeCountLbl.text = String(totalLikes)
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.popularFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.popularFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                        self.popularLikeCountLbl.text = String(totalLikes)
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.popularFeedsArray[self.indexPathSelected_popular.row].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.popularFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.popularFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "LikeIconBlue")
                                        self.popularLikeCountLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
                        self.indexPathSelected_popular = IndexPath()
                        self.indexPathSelected_popular = IndexPath(row: self.indexToClick, section: 0)
                        
                        self.popularIntArr.remove(self.indexPathSelected_popular.row)
                        
                        var typeOfCOnfession = String()
                        if (self.popularFeedsArray[self.indexToClick].postedImage != "")
                        {
                            let imageURL = self.popularFeedsArray[self.indexToClick].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.popularConfessions_tableView.cellForRow(at: self.indexPathSelected_popular) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                
                                var likesCountStr = String()
                                likesCountStr = self.popularFeedsArray[self.indexPathSelected_popular.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text = ""
                                    self.popularFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                    self.popularFeedsArray[self.indexToClick].likesCount = ""
                                    
                                    self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                    self.popularLikeCountLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        onlineUsersCell.likeCountLbl.text = ""
                                        self.popularFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                        self.popularFeedsArray[self.indexToClick].likesCount = ""
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                        self.popularLikeCountLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.popularFeedsArray[self.indexPathSelected_popular.row].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            onlineUsersCell.likeCountLbl.text  = ""
                                            
                                            self.popularFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.popularFeedsArray[self.indexToClick].likesCount = ""
                                            
                                            self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                            self.popularLikeCountLbl.text = ""
                                        }
                                        else
                                        {
                                            self.popularFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.popularFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                            
                                            onlineUsersCell.likeCountLbl.text  = String(totalLikes)
                                            
                                            self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                            self.popularLikeCountLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.popularConfessions_tableView.cellForRow(at: self.indexPathSelected_popular) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.popularFeedsArray[self.indexPathSelected_popular.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    creditsCell.totalLikesCountLbl.text = ""
                                self.popularFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                    
                                   self.popularFeedsArray[self.indexToClick].likesCount = ""
                                    
                                    self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                    self.popularLikeCountLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        creditsCell.totalLikesCountLbl.text = ""
                                    self.popularFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                    self.popularFeedsArray[self.indexToClick].likesCount = ""
                                        
                                        self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                        self.popularLikeCountLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.popularFeedsArray[self.indexPathSelected_popular.row].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            creditsCell.totalLikesCountLbl.text  = ""
                                            
                                            self.popularFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.popularFeedsArray[self.indexToClick].likesCount = ""
                                            
                                            self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                            self.popularLikeCountLbl.text = ""
                                        }
                                        else
                                        {
                                        self.popularFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                        self.popularFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                            
                                            creditsCell.totalLikesCountLbl.text  = String(totalLikes)
                                            
                                            self.confessionLikeIcon_popular.image = UIImage(named: "ConfessionLikeBlue")
                                            self.popularLikeCountLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
    
    //MARK:- ****** replyBtnAction ******
    @IBAction func replyBtnAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
        
        var dictForConfessions = [String: String]()
        dictForConfessions=["age": self.nearByFeedsArray[self.indexToClick].age,
                            "gender": self.nearByFeedsArray[self.indexToClick].gender,
                            "username": self.nearByFeedsArray[self.indexToClick].name,
                            "location": self.nearByFeedsArray[self.indexToClick].location,
                            "totalTime": String(self.nearByFeedsArray[self.indexToClick].totalTime),
                            "typeOfConfession": self.nearByFeedsArray[self.indexToClick].typeOfConfession,
                            "ownerProfilePic": self.nearByFeedsArray[self.indexToClick].profilePic,
                            "statusAdded": self.nearByFeedsArray[self.indexToClick].statusAdded
        ]
        
        homeScreen.ConfessionsDataDict = dictForConfessions
        homeScreen.checkReplyType = "replyOnPost"
        homeScreen.postIdOwnerStr = self.nearByFeedsArray[self.indexToClick].userIDStr
        homeScreen.postIDStr = self.nearByFeedsArray[self.indexToClick].postIDStr
        homeScreen.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
        homeScreen.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
        homeScreen.currentLocationFromHomePage = self.currentLocationFromHomePage
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }
     
    //MARK:- ****** latestClose_btnAction ******
    @IBAction func latestClose_btnAction(_ sender: UIButton) {
        self.showFullView_Latest.isHidden = true
    }
    
    
    //MARK:- ****** popularReply_btnAction ******
    @IBAction func popularReply_btnAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
        
        var dictForConfessions = [String: String]()
        dictForConfessions=["age": self.popularFeedsArray[self.indexToClick].age,
                            "gender": self.popularFeedsArray[self.indexToClick].gender,
                            "username": self.popularFeedsArray[self.indexToClick].name,
                            "location": self.popularFeedsArray[self.indexToClick].location,
                            "totalTime": String(self.popularFeedsArray[self.indexToClick].totalTime),
                            "typeOfConfession": self.popularFeedsArray[self.indexToClick].typeOfConfession,
                            "ownerProfilePic": self.popularFeedsArray[self.indexToClick].profilePic,
                            "statusAdded": self.popularFeedsArray[self.indexToClick].statusAdded
        ]
        homeScreen.ConfessionsDataDict = dictForConfessions
        
        homeScreen.checkReplyType = "replyOnPost"
        homeScreen.postIdOwnerStr = self.popularFeedsArray[self.indexToClick].userIDStr
        homeScreen.postIDStr = self.popularFeedsArray[self.indexToClick].postIDStr
        homeScreen.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
        homeScreen.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
        homeScreen.currentLocationFromHomePage = self.currentLocationFromHomePage
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }
    
    //MARK:- ****** latestDropDown_btnAction ******
    @IBAction func latestDropDown_btnAction(_ sender: UIButton) {
        
        if (self.latestFeedsArray.count > 0)
        {
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    print("authToken = \(authToken)")
                    if (self.latestFeedsArray[sender.tag].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.latestFeedsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.latestFeedsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        print("typeOfCOnfession = \(typeOfCOnfession)")
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: self.latestFeedsArray[sender.tag].postIDStr, status: self.latestFeedsArray[sender.tag].statusAdded, backgroudImageStr: self.latestFeedsArray[sender.tag].postedImage, moodTypeImageStr: self.latestFeedsArray[sender.tag].moodTypeStr, timeFrameStr: self.latestFeedsArray[sender.tag].postHours, moodTypeStr: self.latestFeedsArray[sender.tag].moodTypeStr, moodTypeUrl: self.latestFeedsArray[sender.tag].moodTypeStr, photoTypeStr: typeOfCOnfession, getTotalLikeCount: self.latestFeedsArray[sender.tag].likesCount, getTotalReplyCount: self.latestFeedsArray[sender.tag].replyCount, gifHeight: self.latestFeedsArray[sender.tag].width, gifWidth: self.latestFeedsArray[sender.tag].height, postStatusStr: self.latestFeedsArray[sender.tag].statusAdded)
                    }
                    else
                    {
                        self.showActionSheetForDropDownMenu_otherUser(myID: userID, postStatusStr: self.latestFeedsArray[sender.tag].statusAdded, postIDStr: self.latestFeedsArray[sender.tag].postIDStr, oppoUserID: self.latestFeedsArray[sender.tag].userIDStr)
                    }
                }
            }
        }
    }
    
    
    //MARK:- ****** latestMessage_btnAction ******
    @IBAction func latestMessage_btnAction(_ sender: UIButton) {
        
        let indexPathRow = IndexPath(row: self.indexToClick, section: 0)
        let userOwnerIDStr = self.latestFeedsArray[self.indexToClick].userIDStr
        if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if id == userOwnerIDStr
            {
                print("owner of the post and current user is same")
            }
            else
            {
                let ownerProfileImage = UIImageView()
                ownerProfileImage.sd_setImage(with: URL(string: self.latestFeedsArray[self.indexToClick].postedImage), placeholderImage: UIImage(named: "Default Icon"))
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromStatusScreen"
                let userIDStr =  self.latestFeedsArray[self.indexToClick].userIDStr
                let firebase_user_id =  self.latestFeedsArray[self.indexToClick].firebase_user_id
                
                messagesScreen.opponentName = self.latestFeedsArray[self.indexToClick].name
                messagesScreen.opponentImage = self.latestFeedsArray[self.indexToClick].profilePic
                messagesScreen.opponentUserID = userIDStr
                messagesScreen.opponentFirebaseUserID = firebase_user_id
                messagesScreen.postID = self.latestFeedsArray[self.indexToClick].postIDStr
                messagesScreen.opponentProfilePic =  ownerProfileImage.image!
                
                if (self.latestFeedsArray[self.indexToClick].typeOfConfession == "anonymous")
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                    messagesScreen.confessionTypeUser = "AnonymousUser"
                }
                else
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                    messagesScreen.confessionTypeUser = "WithProfileConfessionUser"
                }
                
                if (self.latestFeedsArray[self.indexToClick].postedImage != "")
                {
                    let Cell = self.latestConfessions_tableView.cellForRow(at: indexPathRow) as! CreditPackagesTableViewCell
                    
                    let imageURL = self.latestFeedsArray[self.indexToClick].postedImage
                    if (imageURL.contains("jpg"))
                    {
                        messagesScreen.postedConfessionImageType = "photo"
                    }
                    else{
                        messagesScreen.postedConfessionImageType = "gif"
                    }
                    
                    messagesScreen.postedConfessionStatus = Cell.usersStatus_TxtView.text!
                    messagesScreen.postedConfessionData = "textWithImage"
                    messagesScreen.userPostedConfessionImageURL = self.latestFeedsArray[self.indexToClick].postedImage
                    messagesScreen.userPostedConfessionImage.animatedImage = Cell.animatedGifImage.animatedImage
                }
                else
                {
                    let onlineUsersCell = self.latestConfessions_tableView.cellForRow(at: indexPathRow) as! ConfessionStatusTableViewCell
                    
                    messagesScreen.postedConfessionStatus = onlineUsersCell.usersStatusTextView.text!
                    messagesScreen.postedConfessionData = "onlyStatusText"
                    messagesScreen.userPostedConfessionImageURL = ""
                    messagesScreen.postedConfessionImageType = "text"
                    messagesScreen.userPostedConfessionImage.animatedImage = nil
                }
                
                messagesScreen.mainConfessionCheck = "comingFromconfession"
                
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }
        }
    }
    
    
    //MARK:- ****** latestReply_btnAction ******
    @IBAction func latestReply_btnAction(_ sender: UIButton) {

    }
    
    //MARK:- ****** latestLike_btnAction ******
    @IBAction func latestLike_btnAction(_ sender: UIButton) {
        self.getButtonClickStr = "latestLike_btnAction"
        self.getReloadIndexPath = self.indexToClick
        self.checkLikeBtnBool = true
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.latestFeedsArray.count > 0)
            {
                self.likeDislikeConfessions(confessionID: self.latestFeedsArray[self.indexToClick].postIDStr, completion: { (responseBool) in
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected_Latest = IndexPath()
                        self.indexPathSelected_Latest = IndexPath(row: self.indexToClick, section: 0)
                        
                        self.latestIntArr.add(self.indexPathSelected_Latest.row)
                        
                        var typeOfCOnfession = String()
                        if (self.latestFeedsArray[self.indexToClick].postedImage != "")
                        {
                            let imageURL = self.latestFeedsArray[self.indexToClick].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.latestConfessions_tableView.cellForRow(at: self.indexPathSelected_Latest) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.latestFeedsArray[self.indexPathSelected_Latest.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if  (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text  = "1"
                                    
                                    self.latestFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                    self.latestFeedsArray[self.indexToClick].likesCount = "1"
                                    
                                    self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                    self.latestLikeCountLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.latestFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.latestFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                        self.latestLikeCountLbl.text = String(totalLikes)
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.latestFeedsArray[self.indexPathSelected_Latest.row].likesCount)! + 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.latestFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.latestFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                        self.latestLikeCountLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.latestConfessions_tableView.cellForRow(at: self.indexPathSelected_Latest) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.latestFeedsArray[self.indexPathSelected_Latest.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.latestFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                    self.latestFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                    
                                    self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                    self.latestLikeCountLbl.text = String(totalLikes)
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.latestFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.latestFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                        self.latestLikeCountLbl.text = String(totalLikes)
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.latestFeedsArray[self.indexPathSelected_Latest.row].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.latestFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.latestFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "LikeIconBlue")
                                        self.latestLikeCountLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
                        self.indexPathSelected_Latest = IndexPath()
                        self.indexPathSelected_Latest = IndexPath(row: self.indexToClick, section: 0)
                        
                        self.latestIntArr.remove(self.indexPathSelected_Latest.row)
                        
                        var typeOfCOnfession = String()
                        if (self.latestFeedsArray[self.indexToClick].postedImage != "")
                        {
                            let imageURL = self.latestFeedsArray[self.indexToClick].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.latestConfessions_tableView.cellForRow(at: self.indexPathSelected_Latest) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                
                                var likesCountStr = String()
                                likesCountStr = self.latestFeedsArray[self.indexPathSelected_Latest.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text = ""
                                    self.latestFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                    self.latestFeedsArray[self.indexToClick].likesCount = ""
                                    
                                    self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                    self.latestLikeCountLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        onlineUsersCell.likeCountLbl.text = ""
                                    self.latestFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                        self.latestFeedsArray[self.indexToClick].likesCount = ""
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                        self.latestLikeCountLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.latestFeedsArray[self.indexPathSelected_Latest.row].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            onlineUsersCell.likeCountLbl.text  = ""
                                            
                                            self.latestFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.latestFeedsArray[self.indexToClick].likesCount = ""
                                            
                                            self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                            self.latestLikeCountLbl.text = ""
                                        }
                                        else
                                        {
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.latestFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.latestFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                            self.latestLikeCountLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.latestConfessions_tableView.cellForRow(at: self.indexPathSelected_Latest) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.latestFeedsArray[self.indexPathSelected_Latest.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    creditsCell.totalLikesCountLbl.text = ""
                                    self.latestFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                    self.latestFeedsArray[self.indexToClick].likesCount = ""
                                    
                                    self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                    self.latestLikeCountLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        creditsCell.totalLikesCountLbl.text = ""
                                        self.latestFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                        self.latestFeedsArray[self.indexToClick].likesCount = ""
                                        
                                        self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                        self.latestLikeCountLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.latestFeedsArray[self.indexPathSelected_Latest.row].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            creditsCell.totalLikesCountLbl.text  = ""
                                            
                                            self.latestFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.latestFeedsArray[self.indexToClick].likesCount = ""
                                            
                                            self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                            self.latestLikeCountLbl.text = ""
                                        }
                                        else
                                        {
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.latestFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.latestFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon_latest.image = UIImage(named: "ConfessionLikeBlue")
                                            self.latestLikeCountLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
    
    //MARK:- ****** nearByDropDown_btnAction ******
    @IBAction func nearByDropDown_btnAction(_ sender: UIButton) {
        
        if (self.nearByFeedsArray.count > 0)
        {
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    print("authToken = \(authToken)")
                    
                    if (self.nearByFeedsArray[sender.tag].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.nearByFeedsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.nearByFeedsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        print("typeOfCOnfession = \(typeOfCOnfession)")
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: self.nearByFeedsArray[sender.tag].postIDStr, status: self.nearByFeedsArray[sender.tag].statusAdded, backgroudImageStr: self.nearByFeedsArray[sender.tag].postedImage, moodTypeImageStr: self.nearByFeedsArray[sender.tag].moodTypeStr, timeFrameStr: self.nearByFeedsArray[sender.tag].postHours, moodTypeStr: self.nearByFeedsArray[sender.tag].moodTypeStr, moodTypeUrl: self.nearByFeedsArray[sender.tag].moodTypeStr, photoTypeStr: typeOfCOnfession, getTotalLikeCount: self.nearByFeedsArray[sender.tag].likesCount, getTotalReplyCount: self.nearByFeedsArray[sender.tag].replyCount, gifHeight: self.nearByFeedsArray[sender.tag].width, gifWidth: self.nearByFeedsArray[sender.tag].height, postStatusStr: self.nearByFeedsArray[sender.tag].statusAdded)
                    }
                    else
                    {
                        self.showActionSheetForDropDownMenu_otherUser(myID: userID, postStatusStr: self.nearByFeedsArray[sender.tag].statusAdded, postIDStr: self.nearByFeedsArray[sender.tag].postIDStr, oppoUserID: self.nearByFeedsArray[sender.tag].userIDStr)
                    }
                }
            }
        }
    }
    
    //MARK:- ****** nearByMessage_btnAction ******
    @IBAction func nearByMessage_btnAction(_ sender: UIButton) {
     
        let indexPathRow = IndexPath(row: self.indexToClick, section: 0)
        let userOwnerIDStr = self.nearByFeedsArray[self.indexToClick].userIDStr
        if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if id == userOwnerIDStr
            {
                print("owner of the post and current user is same")
            }
            else
            {
                
                let ownerProfileImage = UIImageView()
                ownerProfileImage.sd_setImage(with: URL(string: self.nearByFeedsArray[self.indexToClick].postedImage), placeholderImage: UIImage(named: "Default Icon"))
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromStatusScreen"
                let userIDStr =  self.nearByFeedsArray[self.indexToClick].userIDStr
                let firebase_user_id =  self.nearByFeedsArray[self.indexToClick].firebase_user_id
                messagesScreen.opponentName = self.nearByFeedsArray[self.indexToClick].name
                messagesScreen.opponentImage = self.nearByFeedsArray[self.indexToClick].profilePic
                messagesScreen.opponentUserID = userIDStr
                messagesScreen.opponentFirebaseUserID = firebase_user_id
                messagesScreen.postID = self.nearByFeedsArray[self.indexToClick].postIDStr
                messagesScreen.opponentProfilePic =  ownerProfileImage.image!
                
                if (self.nearByFeedsArray[self.indexToClick].typeOfConfession == "anonymous")
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                    messagesScreen.confessionTypeUser = "AnonymousUser"
                }
                else
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                    messagesScreen.confessionTypeUser = "WithProfileConfessionUser"
                }
                
                if (self.nearByFeedsArray[self.indexToClick].postedImage != "")
                {
                    let Cell = self.nearByConfessions_tableView.cellForRow(at: indexPathRow) as! CreditPackagesTableViewCell
                    
                    let imageURL = self.nearByFeedsArray[self.indexToClick].postedImage
                    if (imageURL.contains("jpg"))
                    {
                        messagesScreen.postedConfessionImageType = "photo"
                    }
                    else{
                        messagesScreen.postedConfessionImageType = "gif"
                    }
                    
                    messagesScreen.postedConfessionStatus = Cell.usersStatus_TxtView.text!
                    messagesScreen.postedConfessionData = "textWithImage"
                    messagesScreen.userPostedConfessionImageURL = self.nearByFeedsArray[self.indexToClick].postedImage
                    
                    messagesScreen.userPostedConfessionImage.animatedImage = Cell.animatedGifImage.animatedImage
                }
                else
                {
                    let onlineUsersCell = self.nearByConfessions_tableView.cellForRow(at: indexPathRow) as! ConfessionStatusTableViewCell
                    
                    messagesScreen.postedConfessionStatus = onlineUsersCell.usersStatusTextView.text!
                    messagesScreen.postedConfessionData = "onlyStatusText"
                    messagesScreen.userPostedConfessionImageURL = ""
                    messagesScreen.postedConfessionImageType = "text"
                    messagesScreen.userPostedConfessionImage.animatedImage = nil
                }
                messagesScreen.mainConfessionCheck = "comingFromconfession"
                
                self.navigationController?.pushViewController(messagesScreen, animated: true)
                
            }
        }
        
    }
    
    //MARK:- ****** nearbyReply_btnAction ******
    @IBAction func nearbyReply_btnAction(_ sender: UIButton!) {

    }
    
    
    //MARK:- ****** nearByLike_btnAction ******
    @IBAction func nearByLike_btnAction(_ sender: UIButton) {
        
        self.getButtonClickStr = "nearByLike_btnAction"
        self.getReloadIndexPath = self.indexToClick
        self.checkLikeBtnBool = true
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.nearByFeedsArray.count > 0)
            {
                self.likeDislikeConfessions(confessionID: self.nearByFeedsArray[self.indexToClick].postIDStr, completion: { (responseBool) in
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected_NearBy = IndexPath()
                        self.indexPathSelected_NearBy = IndexPath(row: self.indexToClick, section: 0)
                        
                        self.nearByIntArr.add(self.indexPathSelected_NearBy.row)
                        
                        var typeOfCOnfession = String()
                        if (self.nearByFeedsArray[self.indexToClick].postedImage != "")
                        {
                            let imageURL = self.nearByFeedsArray[self.indexToClick].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.nearByConfessions_tableView.cellForRow(at: self.indexPathSelected_NearBy) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.nearByFeedsArray[self.indexPathSelected_NearBy.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text  = ""
                                    
                                    self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                    self.nearByFeedsArray[self.indexToClick].likesCount = "1"
                                    
                                    self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikeLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        
                                        self.nearByFeedsArray[self.indexToClick].likesCount = "1"
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.nearByFeedsArray[self.indexPathSelected_NearBy.row].likesCount)! + 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.nearByFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.nearByConfessions_tableView.cellForRow(at: self.indexPathSelected_NearBy) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.nearByFeedsArray[self.indexPathSelected_NearBy.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                    self.nearByFeedsArray[self.indexToClick].likesCount = "1"
                                    
                                    self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikeLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.nearByFeedsArray[self.indexToClick].likesCount = "1"
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.nearByFeedsArray[self.indexPathSelected_NearBy.row].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.nearByFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
   
                        self.indexPathSelected_NearBy = IndexPath()
                        self.indexPathSelected_NearBy = IndexPath(row: self.indexToClick, section: 0)
                        
                        self.nearByIntArr.remove(self.indexPathSelected_NearBy.row)
                        
                        var typeOfCOnfession = String()
                        if (self.nearByFeedsArray[self.indexToClick].postedImage != "")
                        {
                            let imageURL = self.nearByFeedsArray[self.indexToClick].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.nearByConfessions_tableView.cellForRow(at: self.indexPathSelected_NearBy) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.nearByFeedsArray[self.indexPathSelected_NearBy.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text = ""
                                    
                                    self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                    self.nearByFeedsArray[self.indexToClick].likesCount = ""
                                    
                                    self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikeLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        onlineUsersCell.likeCountLbl.text = ""
                                        
                                        self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                        self.nearByFeedsArray[self.indexToClick].likesCount = ""
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikeLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.nearByFeedsArray[self.indexPathSelected_NearBy.row].likesCount)! - 1
                                        
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            onlineUsersCell.likeCountLbl.text  = ""
                                            
                                            self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.nearByFeedsArray[self.indexToClick].likesCount = ""
                                            
                                            self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = ""
                                        }
                                        else
                                        {
                                            self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.nearByFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                            
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.nearByConfessions_tableView.cellForRow(at: self.indexPathSelected_NearBy) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                
                                var likesCountStr = String()
                                likesCountStr = self.nearByFeedsArray[self.indexPathSelected_NearBy.row].likesCount
//                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    creditsCell.totalLikesCountLbl.text = ""
                                    
                                    self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                    self.nearByFeedsArray[self.indexToClick].likesCount = ""
                                    
                                    self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikeLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        creditsCell.totalLikesCountLbl.text = ""
                                        self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                        self.nearByFeedsArray[self.indexToClick].likesCount = ""
                                        
                                        self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikeLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.nearByFeedsArray[self.indexPathSelected_NearBy.row].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            creditsCell.totalLikesCountLbl.text = ""
                                            
                                            self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.nearByFeedsArray[self.indexToClick].likesCount = ""
                                            
                                            self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = ""
                                        }
                                        else
                                        {
                                            self.nearByFeedsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.nearByFeedsArray[self.indexToClick].likesCount = String(totalLikes)
                                            
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.confessionLikeIcon_nearBy.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
   
    // MARK: - ****** didReceiveMemoryWarning ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
//        SDImageCache.shared().clearMemory()
    }

}
