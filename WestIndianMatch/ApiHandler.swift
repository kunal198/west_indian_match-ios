//
//  ApiHandler.swift
//  Wi Match
//
//  Created by brst on 25/05/18.
//  Copyright © 2018 brst. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FLAnimatedImage


//let BASEURL = "http://localhost/wimatch/api/web/index.php/v1/users/"

//let BASEURL = "http://api.wimatchapp.com/api/web/index.php/v1/users/"
//let BASEURLFORPURCHASE = "http://api.wimatchapp.com/api/web/index.php/v1/swipe/"
//let BASEURLFORCONFESSIONS = "http://api.wimatchapp.com/api/web/index.php/v1/confessions/"


let BASEURL = "http://rawapi.wimatchapp.com/api/web/index.php/v1/users/"
let BASEURLFORPURCHASE = "http://rawapi.wimatchapp.com/api/web/index.php/v1/swipe/"
let BASEURLFORCONFESSIONS = "http://rawapi.wimatchapp.com/api/web/index.php/v1/confessions/"

let REGISTRATION_URL = "register"
let LOGIN_URL = "login"
let LOGOUT_URL = "logout"
let VIEW_PROFILE_URL = "getuserprofile"
let UPDATE_PROFILE_URL = "updateuserprofile"
let ADD_MEDIA_PROFILE = "addusermedia"
let DEFAULT_OPTIONS = "defaultoptions"
let GET_USER_ALBUMS = "getusermedia"
let DELETE_ALBUM = "deleteusermedia"
let UPDATE_ALBUM = "updateusermedia"
let GET_INTERESTS = "defaultintrests"
let UPDATE_INTERESTS = "updateintrests"
let GET_ALL_USERS_SEARCH = "getalluserswithfilters"
let PURCHASE_TOKENS = "purchasetokens"
let DEFAULT_TOKENS = "defaulttokenandpackages"
let REPORT_CONTENT = "reportcontent"
let BLOCK_UNBLOCK = "blockunblockuser"
let BLOCKED_LISTING = "getblockedlist"
let VISITORS_LISTING = "getprofilevisitorlist"
let LIKE_UNLIKE_MEDIA = "likeunlikemedia"
let PURCHASE_PACKAGE = "purchasepackage"

let GET_DETAILS_BLOCKED_USERS = "getuserallstatus"
let UPDATE_REPORT_CONTENT_TO_CONTINUE_CHAT = "updatereportcontent"

let DELETE_VISTIOR = "deleteprofilevisitor"

let LIST_SWIPE_USERS = "getswipelisting"
let SWIPE_USER = "swipeuser"
let LINK_UP_LIST = "getrightswiped"
let VIBES_LIST = "getmatchedswiped"

let FORGOT_PASSWORD = "forgetpassword"
let EMAIL_VERIFICATION = "resendemail"

let CHANGE_PASSWORD = "changepassword"
let DELETE_PROFILE = "deleteprofile"

let CREATE_CONFESSION = "addconfession"
let NEARBY_CONFESSION_LISTING = "getnearbyconfessions"
let POPULAR_CONFESSION_LISTING = "getpopularconfessions"
let LATEST_CONFESSION_LISTING = "getlatestconfessions"
let LIKE_UNLIKE_CONFESSION = "likeunlikeconfession"
let DELETE_CONFESSION = "deleteconfession"
let UPDATE_CONFESSION = "updateconfession"

let LIKE_UNLIKE_COMMENTS = "likeunlikeconfessioncomments"

let CREATE_REPLY_ON_CONFESSIONS = "addconfessioncomment"
let UPDATE_REPLY_COMMENTS = "updateconfessioncomment"
let GET_REPLY_LISTING = "getconfessioncomment"
let GET_REPLY_Reply = "getconfessionchildcomment"
let DELETE_REPLY_COMMENTS = "deleteconfessioncomment"
let HIDE_CONFESSION = "hideconfession"
let ONLINE_USERS_LISTING = "getonlineusers"
let MY_CONFESSIONS_LISTING = "getownconfessions"

let redeemToken = "redeemtokens"
let GET_BADGES = "getbadges"
let UPDATE_BADGE_COUNT = "updatebadges"
let GET_SINGLE_MEDIA_DETAILS = "getsinglemediadetails"


class ApiHandler: NSObject {

    //MARK: Properties
    let userName: String
    let email: String
    let currentLocation: String
    let dob: String
    let gender: String
    let countryRepresenting: String
    let meetUpPreferences: String
    let interestedInMeetingWith: String
    let bodyType: String
    let hairColor: String
    let smoking: String
    let drinking: String
    let education: String
    let profilePic: String
    let userID: String
    let age: String
    let currentLat: String
    let currentLong: String
    let userType: String
    let relationshipStatus: String

    
    //MARK: ****** Registration Users Method ******
    class func registerUser(userName: String, emailID: String, password: String, profilePic: UIImage,currentLocation: String, dateOfBirth: String, gender: String, countryRepresenting: String, relationshipStatus: String, meetUpPreferences: String, interestedInMeetingWith: String, BodyType: String, HairColor: String, Smoking: String, Drinking: String, Education: String, typeOfUser: String, myDeviceToken: String, currentLatitude: Double, curentLongitue: Double, currentState: String, currentCity: String, currentCountry: String, socialID: String, deviceID: String ,completion: @escaping (NSDictionary) -> Swift.Void) {
     
        let parameters:  [String: Any] = [
            "email" : emailID,
            "password_hash" : password,
            "username" : userName,
            "social_id" : socialID,
            "device_id" : deviceID,
            "dob" : dateOfBirth,
            "body_type" : BodyType,
            "hair_color" : HairColor,
            "relationship_status" : relationshipStatus,
            "smoking" : Smoking,
            "drinking" : Drinking,
            "education" : Education,
            "user_type" : typeOfUser,
            "gender" : gender,
            "meet_up_preferences" : meetUpPreferences,
            "intrested_in_meeting" : interestedInMeetingWith,
            "device_token" : myDeviceToken,
            "device_type" : "ios",
            "location" : currentLocation,
            "latitude" : String(currentLatitude),
            "longitude" : String(curentLongitue),
            "country" : countryRepresenting
      ]
        
        print("parameters = \(parameters)")
        print("BASEURL + REGISTRATION_URL = \(BASEURL + REGISTRATION_URL)")
        
        let imgData = UIImageJPEGRepresentation(profilePic,1.0)!
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "profile_pic",fileName: "profilePic.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + REGISTRATION_URL)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    
     //MARK: ****** Get All Default Values for registering a user Method ******
    class func getAllDefaultValues(completion: @escaping (NSDictionary) -> Swift.Void)
    {
        print("BASEURL + DEFAULT_OPTIONS = \(BASEURL + DEFAULT_OPTIONS)")
        
        Alamofire.request(BASEURL + DEFAULT_OPTIONS).responseJSON(completionHandler: { (responseData) -> Void in
            
            print("responseData.result.value = \(String(describing: responseData.result.value))")
            
            if ((responseData.result.value) != nil) {
                if let json = responseData.result.value as? NSDictionary {
                    completion(json)
                }
                else{
                    let json: NSDictionary = ["Error":"nil"]
                    completion(json)
                }
            }
            else{
                let json: NSDictionary = ["Error":"nil"]
                completion(json)
            }
        })
    }
    
     //MARK: ****** Get All Default Interests Method ******
    class func getAllDefaultInterests(completion: @escaping (NSDictionary) -> Swift.Void)
    {
        print("BASEURL + GET_INTERESTS = \(BASEURL + GET_INTERESTS)")
        
        Alamofire.request(BASEURL + GET_INTERESTS).responseJSON(completionHandler: { (responseData) -> Void in
            
            print("responseData.result.value GET_INTERESTS = \(String(describing: responseData.result.value))")
            
            if ((responseData.result.value) != nil)
            {
                if let json = responseData.result.value as? NSDictionary
                {
                    completion(json)
                }
                else
                {
                    let json: NSDictionary = ["Error":"nil"]
                    completion(json)
                }
            }
            else
            {
                let json: NSDictionary = ["Error":"nil"]
                completion(json)
            }
        })
    }
    
    
     //MARK: ****** Update User's Profile Method ******
    class func updateUserProfile(userID: String, authToken: String,userName: String, emailID: String, password: String, profilePic: UIImage,currentLocation: String, dateOfBirth: String, gender: String, countryRepresenting: String, relationshipStatus: String, meetUpPreferences: String, interestedInMeetingWith: String, BodyType: String, HairColor: String, Smoking: String, Drinking: String, Education: String, typeOfUser: String, myDeviceToken: String, currentLatitude: Double, curentLongitue: Double, currentState: String, currentCity: String, currentCountry: String, deviceID: String ,completion: @escaping (NSDictionary) -> Swift.Void) {
        //         "email" : emailID,
       // "password_hash" : password,
        
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token": authToken,
            "username" : userName,
            "device_id" : deviceID,
            "dob" : dateOfBirth,
            "body_type" : BodyType,
            "hair_color" : HairColor,
            "relationship_status" : relationshipStatus,
            "smoking" : Smoking,
            "drinking" : Drinking,
            "education" : Education,
            "user_type" : typeOfUser,
            "gender" : gender,
            "meet_up_preferences" : meetUpPreferences,
            "intrested_in_meeting" : interestedInMeetingWith,
            "device_token" : myDeviceToken,
            "device_type" : "ios",
            "location" : currentLocation,
            "latitude" : String(currentLatitude),
            "longitude" : String(curentLongitue),
            "country" : countryRepresenting
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + UPDATE_PROFILE_URL = \(BASEURL + UPDATE_PROFILE_URL)")
        
        let imgData = UIImageJPEGRepresentation(profilePic,1.0)!
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "profile_pic",fileName: "profilePic.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + UPDATE_PROFILE_URL)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let json = response.result.value as? NSDictionary
                    {
                        completion(json)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSMutableDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
     //MARK: ****** Login Users Method ******
    class func loginUser(withEmail: String, password: String,myDeviceToken: String,deviceID: String,device_type: String, completion: @escaping (NSDictionary) -> Swift.Void) {
        
        let parameters:  [String: Any] = [
            "device_type" : device_type,
            "device_id" : deviceID,
            "device_token" : myDeviceToken,
            "email" : withEmail,
            "password_hash" : password
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + LOGIN_URL = \(BASEURL + LOGIN_URL)")
        
        Alamofire.request(BASEURL + LOGIN_URL, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                }
        }
        
     /*   Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + LOGIN_URL)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Delete Profile Visitors from wi visitors listing Method ******
    class func deleteProfileVisiors(user_id: String, auth_token: String,other_user_id: String, completion: @escaping (NSDictionary) -> Swift.Void) {
        
        let parameters:  [String: Any] = [
            "user_id" : user_id,
            "auth_token" : auth_token,
            "other_user_id" : other_user_id
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + DELETE_VISTIOR = \(BASEURL + DELETE_VISTIOR)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + DELETE_VISTIOR)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
     //MARK: ****** Logout Users Method ******
    class func logOutUser(userSavedID: String ,completion: @escaping (NSDictionary) -> Swift.Void) {
        
        let parameters:  [String: Any] = [
            "user_id" : userSavedID
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + LOGIN_URL = \(BASEURL + LOGOUT_URL)")
        
        Alamofire.request(BASEURL + LOGOUT_URL, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + LOGOUT_URL)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Users Profile Details Method ******
    class func getUserProfileDetails(forUserID: String, authToken: String,other_user_id: String,timestamp: Int, completion: @escaping (NSDictionary) -> Swift.Void) {
        
        let parameters:  [String: Any] = [
            "user_id" : forUserID,
            "auth_token": authToken,
            "other_user_id" : other_user_id,
            "timestamp" : String(timestamp)
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + VIEW_PROFILE_URL = \(BASEURL + VIEW_PROFILE_URL)")
        
        Alamofire.request(BASEURL + VIEW_PROFILE_URL, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + VIEW_PROFILE_URL)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":response.result.value]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError.localizedDescription]
                completion(json)
            }
        }*/
    }
    
     //MARK: ****** Add Pictures/Videos in albums under my profile Method ******
    class func addProfileAlbum(userID: String, authToken: String, type: String, captionAdded: String, mediaData: Data, nameOfFile: String, typeOfFile: String,timestamp: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "caption" : captionAdded,
            "type" : type,
            "timestamp" : String(timestamp)
        ]
        
        print("nameOfFile = \(nameOfFile)")
        print("typeOfFile = \(typeOfFile)")
        print("parameters = \(parameters)")
        print("BASEURL + ADD_MEDIA_PROFILE = \(BASEURL + ADD_MEDIA_PROFILE)")
        
//        let imgData = UIImageJPEGRepresentation("profilePic",1.0)!
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(mediaData, withName: "media",fileName: nameOfFile, mimeType: typeOfFile)
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } 
        },
                         to: BASEURL + ADD_MEDIA_PROFILE)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
     //MARK: ****** Update Profile Album Method ******
    class func updateProfileAlbum(userID: String, authToken: String, type: String, captionAdded: String, mediaID: String,timestamp: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "caption" : captionAdded,
            "id": mediaID,
            "timestamp": String(timestamp)
        ]
       
        print("parameters = \(parameters)")
        print("BASEURL + UPDATE_ALBUM = \(BASEURL + UPDATE_ALBUM)")
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURL + UPDATE_ALBUM)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
     //MARK: ****** Delete Particular Profile Album Method ******
    class func deleteProfileAlbum(userID: String, authToken: String, mediaID: String,timestamp: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "id" : mediaID,
            "timestamp": String(timestamp)
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + DELETE_ALBUM = \(BASEURL + DELETE_ALBUM)")
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURL + DELETE_ALBUM)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    
     //MARK: ****** Update App Settings Method ******
    class func updateSettings(forUserID: String, authToken: String, typeToBeUpdated:String, isSetTo: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : forUserID,
            "auth_token": authToken,
            "\(typeToBeUpdated)": isSetTo
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + UPDATE_PROFILE_URL = \(BASEURL + UPDATE_PROFILE_URL)")
        
        Alamofire.request(BASEURL + UPDATE_PROFILE_URL, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + UPDATE_PROFILE_URL)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
     //MARK: ****** Update Users current location Method ******
    class func updateCurrentLocation(forUserID: String, authToken: String, city:String, state: String,longitude: String,latitude: String,location: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : forUserID,
            "auth_token": authToken,
            "city": city,
            "state": state,
            "latitude": latitude,
            "longitude": longitude,
            "location": location
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + UPDATE_PROFILE_URL = \(BASEURL + UPDATE_PROFILE_URL)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + UPDATE_PROFILE_URL)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
     //MARK: ****** Get All Album pictures/videos Method ******
    class func getProfileAlbum(userID: String, authToken: String, page: String,other_user_id: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "other_user_id" : other_user_id,
            "page" : "1"
        ]
  
        print("parameters = \(parameters)")
        print("BASEURL + GET_USER_ALBUMS = \(BASEURL + GET_USER_ALBUMS)")
        
        Alamofire.request(BASEURL + GET_USER_ALBUMS, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
      /*  Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURL + GET_USER_ALBUMS)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                        
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
     //MARK: ****** Get All Packages Listing Method ******
    class func getDefaultTokenPackages(completion: @escaping (NSDictionary) -> Swift.Void)
    {
        Alamofire.request(BASEURLFORPURCHASE + DEFAULT_TOKENS).responseJSON(completionHandler: { (responseData) -> Void in
            
            print("responseData.result.value = \(String(describing: responseData.result.value))")
            
            if((responseData.result.value) != nil)
            {
                if let json = responseData.result.value as? NSDictionary
                {
                    completion(json)
                }
                else{
                    let json: NSDictionary = ["Error":"nil"]
                    completion(json)
                }
            }
            else{
                let json: NSDictionary = ["Error":"nil"]
                completion(json)
            }
        })
    }
    
     //MARK: ****** Purchase Tokens Method ******
    class func purchaseTokens(userID: String, authToken: String, tokenID: String, dateCreated: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "token_id" : tokenID,
            "purchased_date_time" : dateCreated
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + PURCHASE_TOKENS = \(BASEURLFORPURCHASE + PURCHASE_TOKENS)")
        
        Alamofire.request(BASEURLFORPURCHASE + PURCHASE_TOKENS, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + PURCHASE_TOKENS)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                        
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
     //MARK: ****** Get Status of Blocked Users Method ******
    class func getStatusOfBlockedUsersForMessages(userID: String, authToken: String, other_user_id: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "other_user_id" : other_user_id
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + GET_DETAILS_BLOCKED_USERS = \(BASEURLFORPURCHASE + GET_DETAILS_BLOCKED_USERS)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + GET_DETAILS_BLOCKED_USERS)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                        
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
     //MARK: ****** Update Report User and Continue Chat Method ******
    class func updateReportContentToContinueChat(userID: String, authToken: String, content_id: String,continue_chating: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "content_id" : content_id,
            "continue_chating" : continue_chating
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + UPDATE_REPORT_CONTENT_TO_CONTINUE_CHAT = \(BASEURLFORPURCHASE + UPDATE_REPORT_CONTENT_TO_CONTINUE_CHAT)")
        
        Alamofire.request(BASEURLFORPURCHASE + UPDATE_REPORT_CONTENT_TO_CONTINUE_CHAT, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + UPDATE_REPORT_CONTENT_TO_CONTINUE_CHAT)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                        
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Purchase Packages Method ******
    class func purchaseUpgradePackages(userID: String, authToken: String, tokenID: String, dateCreated: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "package_id" : tokenID,
            "purchased_date_time" : dateCreated
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + PURCHASE_PACKAGE = \(BASEURLFORPURCHASE + PURCHASE_PACKAGE)")
        
       /* Alamofire.request(BASEURLFORPURCHASE + PURCHASE_PACKAGE, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }*/
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + PURCHASE_PACKAGE)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                        
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
     //MARK: ****** Get Listing of Blocked Users Method ******
    class func getBlockedUsersListing(userID: String, authToken: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + BLOCKED_LISTING = \(BASEURLFORPURCHASE + BLOCKED_LISTING)")
        
        Alamofire.request(BASEURLFORPURCHASE + BLOCKED_LISTING, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + BLOCKED_LISTING)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                        
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
     //MARK: ****** Get Listing of Profile Visitors Method ******
    class func getProfileVisitorsListing(userID: String, authToken: String,search_text: String,paginationValue: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "search_text" : search_text,
            "page" : String(paginationValue)
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + VISITORS_LISTING = \(BASEURLFORPURCHASE + VISITORS_LISTING)")
        
        Alamofire.request(BASEURLFORPURCHASE + VISITORS_LISTING, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
      /*  Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + VISITORS_LISTING)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                        
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Update Users Interests Method ******
    class func updateUserInterests(userID: String, authToken: String, drinksArray: NSMutableArray, hobbiesArray: NSMutableArray,professionArray: NSMutableArray,sportsArray: NSMutableArray,musicArray: NSMutableArray, religiousStr: String, politicalStr: String , completion: @escaping (NSDictionary) -> Swift.Void)
    {
        print("drinksArray = \(drinksArray)")
        let dataOfInterests : [String: Any] = ["politicalreviews" : politicalStr,
                                               "religiousreviews" : religiousStr,
                                               "Userfooddrinks" : drinksArray,
                                               "Userhobbies" : hobbiesArray,
                                               "Userprofession" : professionArray,
                                               "Usersports" : sportsArray,
                                               "Usermusic" : musicArray
                                               ]
        
        let parametersData:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "data" : dataOfInterests
        ]
        
        print("parametersData = \(parametersData)")
        print("BASEURL + UPDATE_INTERESTS = \(BASEURL + UPDATE_INTERESTS)")
    
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for (key, value) in parametersData
                {
                    if key == "data" {
                        
                        let dict = value as! [String: Any]
                        
                        for (key, value) in dict {
                            print(key)
                            if key == "politicalreviews" || key == "religiousreviews" {
                                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: "data[\(key)]")
                            } else {
                                 let array = value as! NSMutableArray
                                
                                for index in 0..<array.count {
                                    print("index = \(index)")
                                    let valueStr = array[index] as! String
                                    multipartFormData.append(valueStr.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: "data[\(key)][\(index)]")
                                }
                            }
                        }
                    } else {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                    
                }
        },
            to: BASEURL + UPDATE_INTERESTS,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("response.result = \(response.result)")
                        print("response.result.value = \(String(describing: response.result.value))")
                        print("response.result = \(String(describing: response.error?.localizedDescription))")
                        
                        if let JSON = response.result.value as? NSDictionary {
                            print("JSON = \(JSON)")
                            completion(JSON)
                        }
                        else{
                            let json: NSDictionary = ["Error":"nil"]
                            completion(json)
                        }
                        
                    }
                    upload.uploadProgress(queue: DispatchQueue(label: "uploadQueue"), closure: { (progress) in
                        
                        
                    })
                    
                case .failure(let encodingError):
                    print(encodingError.localizedDescription)
                    let json: NSDictionary = ["Error":"nil"]
                    completion(json)
                }
        }
        )
        
        
        
      /*  Alamofire.request(BASEURL + UPDATE_INTERESTS, method: .post, parameters: parametersData, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { (response) in
            
            print(response)
            
            //to get JSON return value
            if let result = response.result.value {
                let JSON = result as! NSDictionary
                print(JSON)
                completion(JSON)
            }
            else{
                let json: NSDictionary = ["Error":"nil"]
                completion(json)
            }
        }*/
    }
    
     //MARK: ****** Report Paticular Content/User Method ******
    class func reportContentOrUser(userID: String, authToken: String, typeStr: String,content_id: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "type" : typeStr,
            "content_id" : content_id
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + REPORT_CONTENT = \(BASEURLFORPURCHASE + REPORT_CONTENT)")
      
        Alamofire.request(BASEURLFORPURCHASE + REPORT_CONTENT, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
      /*  Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + REPORT_CONTENT)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Block/Unblock Users Method ******
    class func blockUnblockUser(userID: String, authToken: String, other_user_id: String,typeOfBlockUser: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "other_user_id" : other_user_id,
            "type" : typeOfBlockUser
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + BLOCK_UNBLOCK = \(BASEURLFORPURCHASE + BLOCK_UNBLOCK)")
        
        Alamofire.request(BASEURLFORPURCHASE + BLOCK_UNBLOCK, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
      /*  Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + BLOCK_UNBLOCK)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                let json: NSDictionary = ["Error":encodingError.localizedDescription]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Get Users for Wi Match Method ******
    class func getUsersListingForMatch(userID: String, authToken: String, pageStr: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "page" : pageStr
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + LIST_SWIPE_USERS = \(BASEURLFORPURCHASE + LIST_SWIPE_USERS)")
        
        Alamofire.request(BASEURLFORPURCHASE + LIST_SWIPE_USERS, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + LIST_SWIPE_USERS)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Get Listing of Link Up Users Method ******
    class func getLinkUpUsers(userID: String, authToken: String, pageStr: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "page" : pageStr
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + LINK_UP_LIST = \(BASEURLFORPURCHASE + LINK_UP_LIST)")
        
        Alamofire.request(BASEURLFORPURCHASE + LINK_UP_LIST, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
      /*  Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + LINK_UP_LIST)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Redeem Spotlight Feature Method ******
    class func redeemSpotlight(userID: String, authToken: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + redeemToken = \(BASEURL + redeemToken)")
        
        Alamofire.request(BASEURL + redeemToken, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURL + redeemToken)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Get All Vibes Users Listing Method ******
    class func getRelVibesUsersListing(userID: String, authToken: String, pageStr: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "page" : pageStr
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + VIBES_LIST = \(BASEURLFORPURCHASE + VIBES_LIST)")
        
        Alamofire.request(BASEURLFORPURCHASE + VIBES_LIST, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
     /*   Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + VIBES_LIST)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Swipe Users Method ******
    class func swipeUserForMatch(userID: String, authToken: String, other_user_id: String, swipe_status: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "other_user_id" : other_user_id,
            "swipe_status" : swipe_status
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + SWIPE_USER = \(BASEURLFORPURCHASE + SWIPE_USER)")
        
        Alamofire.request(BASEURLFORPURCHASE + SWIPE_USER, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
      /*  Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + SWIPE_USER)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Like/Unlike Users Media Method ******
    class func likeUnlikeMedia(userID: String, authToken: String, media_id: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "media_id" : media_id
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + LIKE_UNLIKE_MEDIA = \(BASEURLFORPURCHASE + LIKE_UNLIKE_MEDIA)")
        
        Alamofire.request(BASEURLFORPURCHASE + LIKE_UNLIKE_MEDIA, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
      /*  Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORPURCHASE + LIKE_UNLIKE_MEDIA)
        { (result) in
            
            switch result {
                
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
//                    let dict = response.result.value as! NSDictionary
//                    print("dict = \(String(describing: dict))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    func convertToDictionary(text: String) -> NSDictionary? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
     //MARK: ****** Forgot Password Method ******
    class func ForgotPassword(withEmail: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "email" : withEmail
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + FORGOT_PASSWORD = \(BASEURL + FORGOT_PASSWORD)")
        
        Alamofire.request(BASEURL + FORGOT_PASSWORD, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + FORGOT_PASSWORD)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
     //MARK: ****** Change Password Method ******
    class func ChangePassword(userID: String, authToken: String, new_password: String,current_password: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "current_password" : current_password ,
            "new_password" : new_password
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + FORGOT_PASSWORD = \(BASEURL + CHANGE_PASSWORD)")
        Alamofire.request(BASEURL + CHANGE_PASSWORD, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + CHANGE_PASSWORD)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Delete Users Profile Method ******
    class func DeleteProfile(userID: String, authToken: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken
        ]

        print("parameters = \(parameters)")
        print("BASEURL + DELETE_PROFILE = \(BASEURL + DELETE_PROFILE)")
        
        Alamofire.request(BASEURL + DELETE_PROFILE, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
      /*  Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + DELETE_PROFILE)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
     //MARK: ****** Email Verification Method ******
    class func EmailVerification(withEmail: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "email" : withEmail
        ]
        
        print("parameters = \(parameters)")
        print("BASEURL + EMAIL_VERIFICATION = \(BASEURL + EMAIL_VERIFICATION)")
        
        Alamofire.request(BASEURL + EMAIL_VERIFICATION, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURL + EMAIL_VERIFICATION)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
    
    //MARK: - ****** CONFESSIONS API IMPLEMENTATION ******
//    class func createConfession(userID: String, authToken: String, mood: String, duration: String, confessionImage: UIImage, text: String, typeOfConfession: String,nameOfFile: String,typeOfFile: String,animatedGif: String,image_height: Int,image_width: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    class func createConfession(userID: String, authToken: String, typeOfConfession: String, location: String, confessionImage: UIImage, text: String, latitude: Double,longitude: Double,nameOfFile: String,typeOfFile: String,animatedGif: String,image_height: Int,image_width: Int,typeOfMedia: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "type" : typeOfConfession,
            "location" : location,
            "latitude" : String(latitude),
            "longitude" : String(longitude),
            "image_height" : String(image_height),
            "image_width" : String(image_width),
            "text" : text
        ]
        
        print("typeOfMedia = \(typeOfMedia)")
        print("typeOfFile = \(typeOfFile)")
        print("nameOfFile = \(nameOfFile)")
        print("animatedGif = \(animatedGif)")
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + CREATE_CONFESSION = \(BASEURLFORCONFESSIONS + CREATE_CONFESSION)")
        
        var mediaData = Data()
        if (typeOfMedia == "text")
        {
            
        }
        else
        {
            if (typeOfMedia == "photo")
            {
                mediaData = UIImageJPEGRepresentation(confessionImage,1.0)!
            }
            else
            {
                if let bundleURL = NSURL(string: animatedGif)
                {
                    if let animatedImageData = NSData(contentsOf: bundleURL as URL)
                    {
//                        print("animatedImageData = \(animatedImageData)")
                        mediaData = animatedImageData as Data
                    }
                    else
                    {
//                        print("image named \"\(animatedGif)\" into NSData")
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                else
                {
//                    print("image named \"\(animatedGif)\" doesn't exist")
                    let json: NSDictionary = ["Error":"nil"]
                    completion(json)
                }
            }
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(mediaData, withName: "image",fileName: nameOfFile, mimeType: typeOfFile)
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORCONFESSIONS + CREATE_CONFESSION)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    class func createReplyConfession(userID: String, authToken: String, mood: String, duration: String, confessionImage: UIImage, text: String, typeOfConfession: String,nameOfFile: String,typeOfFile: String,animatedGif: String, parent_comment_id: String, confession_id: String,image_height: Int,image_width: Int,typeOfMedia: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "parent_comment_id" : parent_comment_id,
            "confession_id" : confession_id,
            "text" : text,
            "image_height" : String(image_height),
            "image_width" : String(image_width)
        ]
        
        print("typeOfMedia = \(typeOfMedia)")
        print("typeOfFile = \(typeOfFile)")
        print("nameOfFile = \(nameOfFile)")
        print("animatedGif = \(animatedGif)")
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + CREATE_REPLY_ON_CONFESSIONS = \(BASEURLFORCONFESSIONS + CREATE_REPLY_ON_CONFESSIONS)")
        
        var mediaData = Data()
        if (typeOfMedia == "text")
        {
            
        }
        else
        {
            if (typeOfMedia == "photo")
            {
                mediaData = UIImageJPEGRepresentation(confessionImage,1.0)!
            }
            else
            {
                if let bundleURL = NSURL(string: animatedGif)
                {
                    if let animatedImageData = NSData(contentsOf: bundleURL as URL)
                    {
//                        print("animatedImageData = \(animatedImageData)")
                        mediaData = animatedImageData as Data
                    }
                    else
                    {
//                        print("image named \"\(animatedGif)\" into NSData")
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                else
                {
//                    print("image named \"\(animatedGif)\" doesn't exist")
                    let json: NSDictionary = ["Error":"nil"]
                    completion(json)
                }
            }
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(mediaData, withName: "image",fileName: nameOfFile, mimeType: typeOfFile)
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORCONFESSIONS + CREATE_REPLY_ON_CONFESSIONS)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    class func getReplyOnReplyListing(userID: String, userToken: String, confession_id: String, parent_comment_id: String, page: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "confession_id" : confession_id,
            "parent_comment_id" : parent_comment_id,
            "page" : String(page)
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + GET_REPLY_LISTING = \(BASEURLFORCONFESSIONS + GET_REPLY_Reply)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + GET_REPLY_Reply)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    
    class func getReplyListing(userID: String, userToken: String, confession_id: String, page: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "confession_id" : confession_id,
            "page" : String(page)
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + GET_REPLY_LISTING = \(BASEURLFORCONFESSIONS + GET_REPLY_LISTING)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + GET_REPLY_LISTING)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(String(describing: response.result))")
                    print("response.result.value = \(String(describing: response.result.value))")
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    
    class func getAllOnlineUsersForChatRoom(user_id: String, auth_token: String, page: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : user_id,
            "auth_token" : auth_token,
            "page" : page
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + ONLINE_USERS_LISTING = \(BASEURLFORPURCHASE + ONLINE_USERS_LISTING)")
        
        Alamofire.request(BASEURLFORPURCHASE + ONLINE_USERS_LISTING, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
      /*  Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORPURCHASE + ONLINE_USERS_LISTING)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
    class func getSingleMediaDetails(user_id: String, auth_token: String,mediaID: String,typeOfMedia: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : user_id,
            "auth_token" : auth_token,
            "media_id" : mediaID
            ]
        
        print("parameters = \(parameters)")
        print("BASEURL + GET_SINGLE_MEDIA_DETAILS = \(BASEURL + GET_SINGLE_MEDIA_DETAILS)")
        
        Alamofire.request(BASEURL + GET_SINGLE_MEDIA_DETAILS, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
    }
    
    
    class func getAllBadgeCount(user_id: String, auth_token: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : user_id,
            "auth_token" : auth_token,
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + GET_BADGES = \(BASEURLFORPURCHASE + GET_BADGES)")
        
        Alamofire.request(BASEURLFORPURCHASE + GET_BADGES, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
    }
    
    
    class func updateBadgeCountValue(user_id: String, auth_token: String,typeofBadgeCountToBeUpdated: String,totalBadgeCount: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : user_id,
            "auth_token" : auth_token,
            "badge_type" : typeofBadgeCountToBeUpdated,
            "badge_count" : totalBadgeCount
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORPURCHASE + UPDATE_BADGE_COUNT = \(BASEURLFORPURCHASE + UPDATE_BADGE_COUNT)")
        
        Alamofire.request(BASEURLFORPURCHASE + UPDATE_BADGE_COUNT, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
    }
    
    
    class func getMyOWnConfessions(user_id: String, auth_token: String, page: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : user_id,
            "auth_token" : auth_token
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + MY_CONFESSIONS_LISTING = \(BASEURLFORCONFESSIONS + MY_CONFESSIONS_LISTING)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + MY_CONFESSIONS_LISTING)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    
    class func updateReplyConfession(userID: String, authToken: String, mood: String, duration: String, confessionImage: UIImage, text: String, typeOfConfession: String,nameOfFile: String,typeOfFile: String,animatedGif: String, commnet_id: String, id: String,image_height: Int,image_width: Int,typeOfMedia: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "commnet_id" : commnet_id,
             "text" : text,
            "image_height" : String(image_height),
            "image_width" : String(image_width)
        ]
        
        
        
        print("typeOfMedia = \(typeOfMedia)")
        print("typeOfFile = \(typeOfFile)")
        print("nameOfFile = \(nameOfFile)")
        print("animatedGif = \(animatedGif)")
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + UPDATE_REPLY_COMMENTS = \(BASEURLFORCONFESSIONS + UPDATE_REPLY_COMMENTS)")
        
        var mediaData = Data()
        if (typeOfMedia == "text")
        {
            
        }
        else
        {
            if (typeOfMedia == "photo")
            {
                mediaData = UIImageJPEGRepresentation(confessionImage,1.0)!
            }
            else
            {
                if let bundleURL = NSURL(string: animatedGif)
                {
                    if let animatedImageData = NSData(contentsOf: bundleURL as URL)
                    {
//                        print("animatedImageData = \(animatedImageData)")
                        mediaData = animatedImageData as Data
                    }
                    else
                    {
//                        print("image named \"\(animatedGif)\" into NSData")
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                        return
                    }
                }
                else
                {
//                    print("image named \"\(animatedGif)\" doesn't exist")
                    let json: NSDictionary = ["Error":"nil"]
                    completion(json)
                    return
                }
            }
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(mediaData, withName: "image",fileName: nameOfFile, mimeType: typeOfFile)
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORCONFESSIONS + UPDATE_REPLY_COMMENTS)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    class func getNearByConfessionsListing(userID: String, userToken: String, searchText: String, currentLatitude: Double, currentLongitude: Double,paginationValue: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "f_lat" : String(currentLatitude),
            "f_long" : String(currentLongitude),
            "f_serach" : searchText,
            "page" : String(paginationValue)
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + NEARBY_CONFESSION_LISTING = \(BASEURLFORCONFESSIONS + NEARBY_CONFESSION_LISTING)")
        
        Alamofire.request(BASEURLFORCONFESSIONS + NEARBY_CONFESSION_LISTING, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + NEARBY_CONFESSION_LISTING)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    class func getPopularConfessionsListing(pageValue: Int,userID: String, userToken: String, searchText: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "f_serach" : searchText,
            "page" : String(pageValue)
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + POPULAR_CONFESSION_LISTING = \(BASEURLFORCONFESSIONS + POPULAR_CONFESSION_LISTING)")
        
        Alamofire.request(BASEURLFORCONFESSIONS + POPULAR_CONFESSION_LISTING, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + POPULAR_CONFESSION_LISTING)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
    class func getLatestConfessionsListing(pageValue: Int,userID: String, userToken: String, searchText: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "f_serach" : searchText,
            "page" : String(pageValue)
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + LATEST_CONFESSION_LISTING = \(BASEURLFORCONFESSIONS + LATEST_CONFESSION_LISTING)")
        
        Alamofire.request(BASEURLFORCONFESSIONS + LATEST_CONFESSION_LISTING, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
       /* Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + LATEST_CONFESSION_LISTING)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
   class func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
//    class func updateConfession(userID: String, authToken: String, mood: String, duration: String, confessionImage: UIImage, text: String, typeOfConfession: String,nameOfFile: String,typeOfFile: String,animatedGif: String, confessionIDToUpdate: String,image_height: Int,image_width: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    class func updateConfession(userID: String, authToken: String, typeOfConfession: String, location: String, confessionImage: UIImage, text: String, latitude: Double,longitude: Double,nameOfFile: String,typeOfFile: String,animatedGif: String,image_height: Int,image_width: Int,confessionIDToUpdate: String,typeOfMedia: String , completion: @escaping (NSDictionary) -> Swift.Void)
    {
        
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : authToken,
            "type" : typeOfConfession,
            "location" : location,
            "latitude" : String(latitude),
            "longitude" : String(longitude),
            "image_height" : String(image_height),
            "image_width" : String(image_width),
            "text" : text,
            "confession_id" :  confessionIDToUpdate
        ]
        
        print("typeOfMedia = \(typeOfMedia)")
        print("typeOfFile = \(typeOfFile)")
        print("nameOfFile = \(nameOfFile)")
        print("animatedGif = \(animatedGif)")
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + UPDATE_CONFESSION = \(BASEURLFORCONFESSIONS + UPDATE_CONFESSION)")
        
        var mediaData = Data()
        if (typeOfMedia == "text")
        {
            
        }
        else
        {
            if (typeOfMedia == "photo")
            {
                mediaData = UIImageJPEGRepresentation(confessionImage,1.0)!
            }
            else
            {
                if let bundleURL = NSURL(string: animatedGif)
                {
                    if let animatedImageData = NSData(contentsOf: bundleURL as URL)
                    {
//                        print("animatedImageData = \(animatedImageData)")
                        mediaData = animatedImageData as Data
                    }
                    else
                    {
//                        print("image named \"\(animatedGif)\" into NSData")
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                        return
                    }
                }
                else
                {
//                    print("image named \"\(animatedGif)\" doesn't exist")
                    let json: NSDictionary = ["Error":"nil"]
                    completion(json)
                    return
                }
            }
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(mediaData, withName: "image",fileName: nameOfFile, mimeType: typeOfFile)
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURLFORCONFESSIONS + UPDATE_CONFESSION)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else
                    {
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    
    //MARK: - ****** Like/Unlike Wi Confessions ******
    class func likeUnlikeConfessions(userID: String, userToken: String, confession_id: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "confession_id" : confession_id
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + LIKE_UNLIKE_CONFESSION = \(BASEURLFORCONFESSIONS + LIKE_UNLIKE_CONFESSION)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + LIKE_UNLIKE_CONFESSION)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    
    //MARK: - ****** Like/Unlike Confession Comments ******
    class func likeUnlikeConfessions_Comments(userID: String, userToken: String, comment_id: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "comment_id" : comment_id
            ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + LIKE_UNLIKE_COMMENTS = \(BASEURLFORCONFESSIONS + LIKE_UNLIKE_COMMENTS)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + LIKE_UNLIKE_COMMENTS)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    //MARK: - ****** Delete Particular Wi Confession ******
    class func deleteConfessions(userID: String, userToken: String, confession_id: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "id" : confession_id
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + DELETE_CONFESSION = \(BASEURLFORCONFESSIONS + DELETE_CONFESSION)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + DELETE_CONFESSION)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    //MARK: - ****** Delete Particular Reply For Wi Confessions ******
    class func deleteReplyForConfessions(userID: String, userToken: String, confession_id: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "id" : confession_id
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + DELETE_REPLY_COMMENTS = \(BASEURLFORCONFESSIONS + DELETE_REPLY_COMMENTS)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + DELETE_REPLY_COMMENTS)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    //MARK: - ****** Hide Particular Wi Confession From My Confessions Listing ******
    class func hideConfessionFromMyPosts(userID: String, userToken: String, confession_id: String, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        let parameters:  [String: Any] = [
            "user_id" : userID,
            "auth_token" : userToken,
            "confession_id" : confession_id
        ]
        
        print("parameters = \(parameters)")
        print("BASEURLFORCONFESSIONS + HIDE_CONFESSION = \(BASEURLFORCONFESSIONS + HIDE_CONFESSION)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: BASEURLFORCONFESSIONS + HIDE_CONFESSION)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }
    }
    
    //MARK: - ****** Get All Users Listing For Wi People ******
    class func getAllUsers(userID: String, authToken: String, genderSelected: String,minAge: String,maxAge: String, countryRepresentingStr: String, nearByStr: String, userStatusStr: String,currentLatitude: Double, currentLongitude: Double,paginationValue: Int, completion: @escaping (NSDictionary) -> Swift.Void)
    {
        var parameters = [String: Any]()
        
        var genderID = String()
        if (genderSelected == "Male")
        {
           genderID = "1"
        }
        else if (genderSelected == "Both")
        {
             genderID = ""
        }
        else if (genderSelected == "Female")
        {
             genderID = "2"
        }
        else
        {
            genderID = ""
        }
        
        var nearByValue = String()
        if (nearByStr == "Less than 5 miles")
        {
            nearByValue = "5"
        }
        else if (nearByStr == "more than 5 miles")
        {
            nearByValue = "9"
        }
        else if (nearByStr == "10 miles")
        {
            nearByValue = "10"
        }
        else if (nearByStr == "100 miles +")
        {
            nearByValue = "150"
        }
        else if (nearByStr == "all users")
        {
            nearByValue = ""
        }
        else
        {
            nearByValue = ""
        }
        
        var onlineStatus = String()
        if (userStatusStr == "Online")
        {
            onlineStatus = "1"
        }
        else{
             onlineStatus = ""
        }
        
        parameters = [
            "user_id" : userID,
            "auth_token" : authToken,
            "f_lat" : String(currentLatitude),
            "f_long" : String(currentLongitude),
            "f_gender" :  genderID,
            "f_min_age" : minAge,
            "f_max_age" : maxAge,
            "f_country" : countryRepresentingStr,
            "f_online_status" : onlineStatus,
            "f_nearby" : nearByValue,
            "page" : String(paginationValue)
        ]
         
        print("parameters = \(parameters)")
        print("BASEURL + GET_ALL_USERS_SEARCH = \(BASEURL + GET_ALL_USERS_SEARCH)")
        
        Alamofire.request(BASEURL + GET_ALL_USERS_SEARCH, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                    
                case .failure(let encodingError):
                    
                    print("encodingError = \(encodingError.localizedDescription)")
                    
                    let json: NSDictionary = ["Error":encodingError]
                    completion(json)
                    
                }
        }
        
    /*    Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to: BASEURL + GET_ALL_USERS_SEARCH)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print("response.result = \(String(describing: response.error?.localizedDescription))")
                    print("response.result = \(response.result)")
                    print("response.result.value = \(String(describing: response.result.value))")
                    
                    if let JSON = response.result.value as? NSDictionary {
                        print("JSON = \(JSON)")
                        completion(JSON)
                    }
                    else{
                        let json: NSDictionary = ["Error":"nil"]
                        completion(json)
                    }
                }
                
            case .failure(let encodingError):
                print("encodingError = \(encodingError.localizedDescription)")
                
                let json: NSDictionary = ["Error":encodingError]
                completion(json)
            }
        }*/
    }
    
    
    
    //MARK: Inits
    init(userName: String, email: String, currentLocation: String,dob: String,gender: String , countryRepresenting: String , meetUpPreferences: String, interestedInMeetingWith: String, bodyType: String, hairColor: String,smoking: String, drinking: String,education: String, profilePic: String, userID: String, age: String, currentLat: String, currentLong: String, userType: String,relationshipStatus: String)
    {
        self.userName = userName
        self.email = email
        self.currentLocation = currentLocation
        self.dob = dob
        self.gender = gender
        self.countryRepresenting = countryRepresenting
        self.meetUpPreferences = meetUpPreferences
        self.interestedInMeetingWith = interestedInMeetingWith
        self.bodyType = bodyType
        self.hairColor = hairColor
        self.smoking = smoking
        self.drinking = drinking
        self.education = education
        self.profilePic = profilePic
        self.userID = userID
        self.age = age
        self.currentLat = currentLat
        self.currentLong = currentLong
        self.userType = userType
        self.relationshipStatus = relationshipStatus
    }
}
