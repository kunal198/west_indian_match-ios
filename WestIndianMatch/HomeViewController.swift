//
//  HomeViewController.swift
//  WestIndianMatch
//
//  Created by brst on 22/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import FBSDKCoreKit
import SDWebImage
import AVKit
import AVFoundation
import CoreLocation
import Fabric
import Crashlytics


class HomeViewController: UIViewController,CLLocationManagerDelegate {
    
    // MARK: - ****** Variables ******
    var country = String()
    var state = String()
    var currentCity = String()
    var LocationStr = String()
    var localityStr = String()
    var subLocalityStr = String()
    var locationManager: CLLocationManager!
    let playerViewController = AVPlayerViewController()
    var player:AVPlayer?
    var currentLatitude = Double()
    var currentLongitude = Double()
    var messagesBtnBool = Bool()
    var matchBtnBool = Bool()
    var sayBtnBool = Bool()
    var groupChatBool = Bool()
    var visitorBool = Bool()
    var wiPeopleBool = Bool()
    var profileVisitorsCount = String()
    var NewMatchesCount = String()
    var LinkUpCount = String()
    var MessagesCount = String()
    var chatRoomCount = String()
    var profileImageURLForVideo = String()
    var totalCountArray = NSMutableArray()//[String]()
    
    var todayDateStr = String()
    
    var profileVisitorsPushBool = Bool()
    
    var linkUpPushBool = Bool()
    
    var commentsPushBool = Bool()
    
    var wiMatchBool = Bool()
    var typeOfPushForMatch = String()
    var opponentIDForMatch = String()
    var getTypeForMatch = String()
    var myIdForMatch = String()
    var matchDictionary = NSDictionary()
    var profilePushBool = Bool()
    var postIDProfile = String()
    var typeOfAlbumProfile = String()
    var typeOfPostStrProfile = String()
    
    var checkReplyTypeReplyProfile = String()
    var postIDOwnerReplyProfile = String()
    var replyIDReplyProfile = String()
    
    
    var repliesPushBool = Bool()
    var postIDReply = String()
    var replyIDReply = String()
    var userIDOwnerReply = String()
    var postIDOwnerReply = String()
    var checkReplyTypeReply = String()
    var differentiateTypeReply = String()
    var typeOfPostReply = String()
    var typeOfAlbumReply = String()
    var postIDOpenPostReply = String()
    var replyOnReplyId = String()
    
    
    var messagesPushBool = Bool()
    var typeOfUserMessage = String()
    var oppoIDMessage = String()
    var oppoFirebaseIDMessage = String()
    var oppoNameMessage = String()
    var postIDMessage = String()
    var oppoImageMessage = String()
    var myVisibilityMessage = String()
    var oppoVisibility = String()
    var checkVisibilityMessage = String()
    var mainConfessionCheck = String()
    var checkVisibiltyStr = String()
    
    var linkUpTotaCount = Int()
    var vibesTotalCount = Int()
    
    
    // MARK: - ****** Outlets ******
    @IBOutlet weak var wiPeopleLbl: UILabel!
    @IBOutlet weak var wiSayImage: UIImageView!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var wiMessagesView: UIView!
    @IBOutlet weak var wiChatRoomView: UIView!
    @IBOutlet weak var wiPeopleView: UIView!
    @IBOutlet weak var wiVisitorsView: UIView!
    @IBOutlet weak var wiMatchView: UIView!
    @IBOutlet weak var wiEventsView: UIView!
    @IBOutlet weak var wiEventsImage: UIImageView!
    @IBOutlet weak var wiMatchImage: UIImageView!
    @IBOutlet weak var wiVisitorsImage: UIImageView!
    @IBOutlet weak var wiChatRoomInage: UIImageView!
    @IBOutlet weak var wiMessageImage: UIImageView!
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var messagesBadge_lbl: UILabel!
    @IBOutlet weak var visitorsBadge_lbl: UILabel!
    @IBOutlet weak var newMatchesBadge_lbl: UILabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var chatRoomUser_lbl: UILabel!
    
    
    // MARK: - ****** ViewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newMatchesBadge_lbl.isHidden = true
        self.visitorsBadge_lbl.isHidden = true
        self.messagesBadge_lbl.isHidden = true
        self.NewMatchesCount = "0"
        self.LinkUpCount = "0"
        self.chatRoomUser_lbl.isHidden = true
        UIApplication.shared.isStatusBarHidden=false
        self.navigationController?.isNavigationBarHidden = true
        profileImage.layer.borderWidth = 2.0
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = 13
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMessageBadge), name: NSNotification.Name(rawValue: "updateMessageBadge"), object: nil)
        
        if let chatRoomSubscribe = UserDefaults.standard.value(forKey: "wi_chat_room") as? String
        {
            if (chatRoomSubscribe != "off")
            {
                FIRMessaging.messaging().subscribe(toTopic: "chatroom")
            }
        }
        else
        {
            FIRMessaging.messaging().subscribe(toTopic: "chatroom")
        }
        
        if (self.profileVisitorsPushBool == true)
        {
            self.view.isUserInteractionEnabled = false
            self.loadingView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                let eventsScreen = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVisitorsViewController") as! ProfileVisitorsViewController
                eventsScreen.appDelegatePush = "fromHomePage"
                self.navigationController?.pushViewController(eventsScreen, animated: true)
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
            }
        }
        else if (self.wiMatchBool == true)
        {
            self.view.isUserInteractionEnabled = false
            self.loadingView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                let newMatchesScreen = self.storyboard?.instantiateViewController(withIdentifier: "WiMatch_MeetUpViewController") as! WiMatch_MeetUpViewController
                newMatchesScreen.typeOfNotification = self.typeOfPushForMatch
                newMatchesScreen.opponentIDOnMatch = self.opponentIDForMatch
                newMatchesScreen.getTypeOfMatchStr = self.getTypeForMatch
                newMatchesScreen.myIdFromPush = self.myIdForMatch
                newMatchesScreen.appDelegatePush = "fromHomePage"
                newMatchesScreen.matchResultDict = self.matchDictionary
                self.navigationController?.pushViewController(newMatchesScreen, animated: true)
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
            }
        }
        else if (self.linkUpPushBool == true)
        {
            self.view.isUserInteractionEnabled = false
            self.loadingView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                let newMatchesScreen = self.storyboard?.instantiateViewController(withIdentifier: "WiMatch_MeetUpViewController") as! WiMatch_MeetUpViewController
                newMatchesScreen.typeOfNotification = "Link Up"
                newMatchesScreen.appDelegatePush = "fromHomePage"
                self.navigationController?.pushViewController(newMatchesScreen, animated: true)
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
            }
        }
        else if (self.profilePushBool == true)
        {
            self.view.isUserInteractionEnabled = false
            self.loadingView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                let openParticularPost = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                openParticularPost.postID = self.postIDProfile
                openParticularPost.typeOfAlbumsPush = self.typeOfAlbumProfile
                openParticularPost.appDelegatePush = "fromHomePage"
                openParticularPost.typeOfPostStr = self.typeOfPostStrProfile
                openParticularPost.replyIDReply = self.replyIDReplyProfile
                openParticularPost.checkReplyTypeReply = self.checkReplyTypeReplyProfile
                openParticularPost.postIDOwnerReply = self.postIDOwnerReplyProfile
                self.navigationController?.pushViewController(openParticularPost, animated: true)
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
            }
        }
        else if (self.repliesPushBool == true)
        {
            self.view.isUserInteractionEnabled = false
            self.loadingView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                let openParticularPost = self.storyboard?.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
                
                if self.checkReplyTypeReply == "replyOnReply"
                {
                    openParticularPost.postIDStr = self.postIDReply
                    openParticularPost.replyIDStr = self.replyIDReply
                    openParticularPost.userIDReplyOwner = self.userIDOwnerReply
                    openParticularPost.postIDOwner = self.postIDOwnerReply
                    openParticularPost.checkReplyType = self.checkReplyTypeReply
                    openParticularPost.differentiateTypereplyStr = self.differentiateTypeReply
                    openParticularPost.typeOfPostStr = self.typeOfPostReply
                    openParticularPost.typeOfAlbumsPush = self.typeOfAlbumReply
                    openParticularPost.postIDOpenPost = self.postIDOpenPostReply
                    openParticularPost.replyONReplyIDStr = self.replyOnReplyId
                }
                else
                {
                    openParticularPost.postIDStr = self.postIDReply
                    openParticularPost.userIDReplyOwner = self.userIDOwnerReply
                    openParticularPost.replyIDStr = self.replyIDReply
                    openParticularPost.postIDOwner = self.postIDOwnerReply
                    openParticularPost.checkReplyType = self.checkReplyTypeReply
                    openParticularPost.differentiateTypereplyStr = self.differentiateTypeReply
                    openParticularPost.typeOfPostStr = self.typeOfPostReply
                    openParticularPost.typeOfAlbumsPush = self.typeOfAlbumReply
                    openParticularPost.postIDOpenPost = self.postIDOpenPostReply
                }
                openParticularPost.appDelegatePush = "fromHomePage"
                self.navigationController?.pushViewController(openParticularPost, animated: true)
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
            }
        }
        else if (self.commentsPushBool == true)
        {
            self.view.isUserInteractionEnabled = false
            self.loadingView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                let openParticularPost = self.storyboard?.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
                
                if self.checkReplyTypeReply == "replyOnReply"
                {
                    openParticularPost.postIDStr = self.postIDReply
                    openParticularPost.replyIDStr = self.replyIDReply
                    openParticularPost.userIDReplyOwner = self.userIDOwnerReply
                    openParticularPost.postIDOwner = self.postIDOwnerReply
                    openParticularPost.checkReplyType = self.checkReplyTypeReply
                    openParticularPost.differentiateTypereplyStr = self.differentiateTypeReply
                    openParticularPost.typeOfPostStr = self.typeOfPostReply
                    openParticularPost.typeOfAlbumsPush = self.typeOfAlbumReply
                    openParticularPost.postIDOpenPost = self.postIDOpenPostReply
                    openParticularPost.replyONReplyIDStr = self.replyOnReplyId
                    openParticularPost.appDelegatePush = "fromHomePage"
                    self.navigationController?.pushViewController(openParticularPost, animated: true)
                }
                else
                {
                    let openParticularPost = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                    openParticularPost.postID = self.postIDReply
                    openParticularPost.typeOfAlbumsPush = self.typeOfAlbumReply
                    openParticularPost.typeOfPostStr = self.typeOfPostReply
                    openParticularPost.ownerIdForConfession = self.postIDOwnerReply
                    openParticularPost.appDelegatePush = "fromHomePage"
                    self.navigationController?.pushViewController(openParticularPost, animated: true)
                }
                
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
            }
        }
        else if (self.messagesPushBool == true)
        {
            self.view.isUserInteractionEnabled = false
            self.loadingView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
               
                let messageScreen = self.storyboard?.instantiateViewController(withIdentifier: "ChatListingViewController") as! ChatListingViewController
                
                print("self.typeOfUserMessage = \(self.typeOfUserMessage)")
                
                messageScreen.typeOfUser = "WithProfileUser"
                messageScreen.opponentFirebaseID = self.oppoFirebaseIDMessage
                messageScreen.opponentUserID = self.oppoIDMessage
                messageScreen.opponentName = self.oppoNameMessage
                messageScreen.typeOfUser = self.typeOfUserMessage
                messageScreen.postID = self.postIDMessage
                messageScreen.opponentImage = self.oppoImageMessage
                
                    if (self.checkVisibiltyStr == "withProfileChat") || (self.checkVisibiltyStr == "FromViewProfile")
                    {
                       messageScreen.myVisiblityFromChatStr = self.myVisibilityMessage
                       messageScreen.opponentVisibilityFromChatStr = self.oppoVisibility
                       messageScreen.mainConfessionCheck = "comingFromChatListing"
                       messageScreen.checkVisibiltyStr = "withProfileChat"
                    }
                    else{
                        messageScreen.checkVisibiltyStr = "withAnonymousChat"
                    }
                
                messageScreen.messagesPushBool = true
                messageScreen.appDelegatePush = "fromHomePage"
                self.navigationController?.pushViewController(messageScreen, animated: true)
                
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
            }
        }
    }
    
    func moveToProfileScreen(_ anote: Notification)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            let eventsScreen = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVisitorsViewController") as! ProfileVisitorsViewController
            eventsScreen.appDelegatePush = "fromHomePage"
            self.navigationController?.pushViewController(eventsScreen, animated: true)
        }
    }
    
    
    // MARK: - ****** ViewDid-DisAppear ******
    override func viewDidDisappear(_ animated: Bool) {
      
    }
    
     // MARK: -  ****** Update Messages Badge Count ******
    func updateMessageBadge()
    {
       /* let appDelegate = UIApplication.shared.delegate as! AppDelegate
        print("appDelegate.messagesCount = \(appDelegate.messagesCount)")
        
        if let messageBadge = UserDefaults.standard.value(forKey: "messagesCount") as? Int
        {
            if messageBadge > 0
            {
                self.messagesBadge_lbl.isHidden = false
                self.messagesBadge_lbl.text = String(messageBadge)
            }
            else
            {
                self.messagesBadge_lbl.isHidden = true
            }
        }
        else
        {
            self.messagesBadge_lbl.isHidden = true
        }*/
    }
 
    
    // MARK: - ****** Set Button's Border ******
    func setButtonsBorders()
    {
        messagesBtnBool = false
        matchBtnBool = false
        sayBtnBool = false
        groupChatBool = false
        visitorBool = false
        wiPeopleBool = false
        
        self.wiMessageImage.image = UIImage(named : "Messages Icon")
        self.wiChatRoomInage.image = UIImage(named : "Wi chat room Icon")
        self.wiSayImage.image = UIImage(named : "People Icon")
        self.wiVisitorsImage.image = UIImage(named : "Visitors Icon")
        self.wiMatchImage.image = UIImage(named : "Match Icon")
        self.wiPeopleLbl.textColor = UIColor.white
        wiEventsImage.layer.cornerRadius = wiEventsImage.frame.size.height/2
        wiEventsView.layer.borderWidth = 3.0
        wiEventsView.layer.masksToBounds = false
        wiEventsView.layer.borderColor = UIColor.white.cgColor
        wiEventsView.layer.cornerRadius = 13
        wiEventsView.layer.cornerRadius = wiEventsView.frame.size.height/2
        
        wiMatchView.layer.borderWidth = 3.0
        wiMatchView.layer.masksToBounds = false
        wiMatchView.layer.borderColor = UIColor.white.cgColor
        wiMatchView.layer.cornerRadius = 13
        wiMatchView.layer.cornerRadius = wiMatchView.frame.size.height/2
        
        wiChatRoomView.layer.borderWidth = 3.0
        wiChatRoomView.layer.masksToBounds = false
        wiChatRoomView.layer.borderColor = UIColor.white.cgColor
        wiChatRoomView.layer.cornerRadius = 13
        wiChatRoomView.layer.cornerRadius = wiChatRoomView.frame.size.height/2
        
        wiPeopleView.layer.borderWidth = 3.0
        wiPeopleView.layer.masksToBounds = false
        wiPeopleView.layer.borderColor = UIColor.white.cgColor
        wiPeopleView.layer.cornerRadius = 13
        wiPeopleView.layer.cornerRadius = wiPeopleView.frame.size.height/2
        
        wiVisitorsView.layer.borderWidth = 3.0
        wiVisitorsView.layer.masksToBounds = false
        wiVisitorsView.layer.borderColor = UIColor.white.cgColor
        wiVisitorsView.layer.cornerRadius = 13
        wiVisitorsView.layer.cornerRadius = wiVisitorsView.frame.size.height/2
        
        wiMessagesView.layer.borderWidth = 3.0
        wiMessagesView.layer.masksToBounds = false
        wiMessagesView.layer.borderColor = UIColor.white.cgColor
        wiMessagesView.layer.cornerRadius = 13
        wiMessagesView.layer.cornerRadius = wiMessagesView.frame.size.height/2
        
        settingsView.layer.cornerRadius = settingsView.frame.size.height/2
    }
    
    
    //MARK:- ****** didChangeAuthorization ******
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
     {
        
        /*switch status {
            
        case .notDetermined , .denied , .restricted :
            let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                UIApplication.shared.openURL(settingsURL!)
            }))
            
            alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
            }))
            self.present(alert, animated: true, completion: nil)

            break
            
        case .authorizedWhenInUse:
            
            self.locationManager.startMonitoringSignificantLocationChanges()
             locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            self.locationManager.startMonitoringSignificantLocationChanges()
             locationManager.startUpdatingLocation()
            break
            
        default:
            
            self.locationManager.startMonitoringSignificantLocationChanges()
             locationManager.startUpdatingLocation()
        }*/
     }
    
    
    
    // MARK: - ****** didUpdateLocations ******
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.locationManager.location != nil
        {
            if let getCoordinates = manager.location?.coordinate
            {
                print("getCoordinates = \(getCoordinates)")
                let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.locationManager.stopUpdatingLocation()
                
                //Brooklyn co-ordinates
                //        self.currentLatitude = 40.714224
                //        self.currentLongitude = -73.961452
                
                //New york points
                //        self.currentLatitude = 40.70979201
                //        self.currentLongitude = -74.0272522
                
                //        self.currentLatitude = 30.70815618309128
                //        self.currentLongitude = 76.69154036822799
                
                if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                    let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in // this is the last line that is being called
                        var placemark : CLPlacemark!
                        placemark = placemarks?[0]
                        
                        if placemark != nil
                        {
                            var vv = String()
                            if (placemark.subLocality != nil)
                            {
                                vv = placemark.subLocality!
                            }
                            
                            var countWhiteSpaces = Int()
                            if (vv.range(of: " ") != nil)
                            {
                                countWhiteSpaces = (vv.components(separatedBy: " ").count) - 1
                            }
                            else{
                                countWhiteSpaces = 0
                            }
                            
                            var objAtIndexBeforeLast = String()
                            var objAtIndexLast = String()
                            if (countWhiteSpaces > 0)
                            {
                                var fullNameArr = vv.components(separatedBy: " ")
                                
                                objAtIndexBeforeLast = (fullNameArr[(fullNameArr.count) - 2])
                                
                                objAtIndexLast = (fullNameArr.last)!
                                
                                self.subLocalityStr = objAtIndexBeforeLast + " " + objAtIndexLast
                            }
                            else
                            {
                                self.subLocalityStr = vv
                            }
                            
                            if (placemark.locality != nil)
                            {
                                self.localityStr = placemark.locality!
                            }
                            
                            var city = String()
                            if (placemark.addressDictionary?["City"] != nil)
                            {
                                city = (placemark.addressDictionary?["City"] as! String)
                                self.currentCity = (placemark.addressDictionary?["City"] as! String)
                            }
                            
                            if (placemark.addressDictionary?["Country"] != nil)
                            {
                                self.country = (placemark.addressDictionary?["Country"] as! String)
                            }
                            
                            if (placemark.addressDictionary?["State"] != nil)
                            {
                                self.state = (placemark.addressDictionary?["State"] as! String)
                            }
                            
                            if (self.subLocalityStr != "") && (self.localityStr != "")
                            {
                                self.LocationStr = self.subLocalityStr + ", " + self.localityStr
                            }
                            else if (self.currentCity != "") && (self.state != "")
                            {
                                self.LocationStr = self.currentCity + ", " + self.state
                            }
                            else if (self.currentCity == "") && (self.state != "") && (self.country != "")
                            {
                                self.LocationStr = self.state + ", " + self.country
                            }
                            else if (self.state == "") && (self.currentCity != "") && (self.country != "")
                            {
                                self.LocationStr = self.currentCity + ", " + self.country
                            }
                            else
                            {
                                self.LocationStr = self.country
                            }
                            
                            let statusInternet = Reach().connectionStatus()
                            switch statusInternet
                            {
                            case .unknown, .offline:
                                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            case .online(.wwan) , .online(.wiFi):
                                
                                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                                {
                                    if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                                    {
                                        ApiHandler.updateCurrentLocation(forUserID: userID, authToken: userToken, city: city, state: self.state, longitude: String(self.currentLongitude), latitude: String(self.currentLatitude), location: self.LocationStr, completion: { (responseData) in
                                            
                                            if (responseData.value(forKey: "status") as? String == "200")
                                            {
                                                UserDefaults.standard.set(self.state, forKey: "state")
                                                UserDefaults.standard.synchronize()
                                            }
                                        })
                                    }
                                }
                            }
                            
//                            if let u_id = UserDefaults.standard.value(forKey: "UserAuthID") as? String {
                                
                                var dataDict = [String: Any]()
                                dataDict["CurrentLatitude"] = self.currentLatitude
                                dataDict["CurrentLongitude"] = self.currentLongitude
                                
                                if self.country != ""
                                {
                                    dataDict["currentLocationCountry"] = self.country
                                }
                                
                                if self.subLocalityStr != ""
                                {
                                    dataDict["currentCity"] = self.subLocalityStr//city
                                }
                                
                                if (self.localityStr != "")
                                {
                                    dataDict["currentState"] = self.localityStr //state
                                }
//                            }
                        }
                    }
                }
            }
            
        }
    }
  
    func getUpdateBadges()
    {
        let connectonStatus = Reach().connectionStatus()
        switch connectonStatus
        {
        case .unknown, .offline:
            print("offline")
            
        case .online(.wwan) , .online(.wiFi):
            self.getAllBadgeCountFromDatabase(completion: { (result) in
                
            })
        }
    }
    
    
    // MARK: - ****** ViewWillAppear ******
    override func viewWillAppear(_ animated: Bool) {
        
//        FIRDatabase.database().reference().child("GroupChat").setValue(nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getUpdateBadges), name: NSNotification.Name(rawValue: "getUpdateBadges"), object: nil)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let connectonStatus = Reach().connectionStatus()
        switch connectonStatus
        {
        case .unknown, .offline:
            print("offline")
            
        case .online(.wwan) , .online(.wiFi):
            
            appDelegate.updateUsersDetails(notificationType: "online_status", getVisibility: "1", completion: { (response) in
                
                if (response == true)
                {
                    print("updated users status to online")
                }
                else
                {
                    print("not updated users status to online")
                }
                
                if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                {
                    let Ref = FIRDatabase.database().reference()
                    var dataDict = [String : Any]()
                    dataDict["userStatus"] = "online"
                    Ref.child("Users").child(userID).updateChildValues(dataDict)
                    
                    var dataDictBadges = [String: Int]()
                    dataDictBadges["AppBadgeCount"] = 0
                  Ref.child("Users").child(userID).updateChildValues(dataDictBadges)
                }
            })
            
            self.getAllBadgeCountFromDatabase(completion: { (result) in
                
            })
        }
        
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            print("MY SAVED USER ID = \(id)")
        }
        
        if let profileImage = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
        {
           DispatchQueue.main.async {
              self.profileImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "Default Icon"))
            }
        }
        
        self.profileImage.contentMode = UIViewContentMode.scaleAspectFill
        self.profileImage.clipsToBounds = true
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let status = CLLocationManager.authorizationStatus()
        switch status {
            
        case .notDetermined , .denied , .restricted :
            
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
            }
            
            break
            
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
       /* default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
        
       
        let statusInternet = Reach().connectionStatus()
        switch statusInternet
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
     
            self.getChatRoomCountFromFirebase()
        }
        
        setButtonsBorders()
        
        profileImage.layer.borderWidth = 2.0
        profileImage.layer.masksToBounds = true
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = 13
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
       
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        Reach().monitorReachabilityChanges()
    }
    
    func getChatRoomCountFromFirebase()
    {
        let ref = FIRDatabase.database().reference()
//        if let currentUserID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
//        {
           ref.child("UsersInChatRoom").observe(.value, with: { (snapshot) in
                if snapshot.exists()
                {
                    self.chatRoomUser_lbl.text = String(snapshot.childrenCount)
                    self.chatRoomUser_lbl.isHidden = false
                }
                else
                {
                    self.chatRoomUser_lbl.text = ""
                    self.chatRoomUser_lbl.isHidden = true
                }
            })
//        }
    }
    
    func getAllBadgeCountFromDatabase(completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                ApiHandler.getAllBadgeCount(user_id: userID, auth_token: userToken, completion: { (responseData) in
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        
                        if (dataDict != nil)
                        {
                            let visitorsCount = dataDict?.value(forKey: "profilevisitor_badgecount") as! Int
                            
                            if visitorsCount > 0
                            {
                                self.visitorsBadge_lbl.isHidden = false
                                self.visitorsBadge_lbl.text = String(visitorsCount)
                            }
                            else{
                                self.visitorsBadge_lbl.isHidden = true
                                self.visitorsBadge_lbl.text = ""
                            }
                            
                            self.linkUpTotaCount = dataDict?.value(forKey: "linkup_badgecount") as! Int
                            
                            self.vibesTotalCount = dataDict?.value(forKey: "vibes_badgecount") as! Int
                            
                            let totalLinkAndVibesCount = dataDict?.value(forKey: "total_badgecount") as! Int
                            if totalLinkAndVibesCount > 0
                            {
                                self.newMatchesBadge_lbl.isHidden = false
                                self.newMatchesBadge_lbl.text = String(totalLinkAndVibesCount)
                            }
                            else{
                                self.newMatchesBadge_lbl.isHidden = true
                                self.newMatchesBadge_lbl.text = ""
                            }
                            
                            let messages_badgecount = dataDict?.value(forKey: "messages_badgecount") as! Int
                            if messages_badgecount > 0
                            {
                                self.messagesBadge_lbl.isHidden = false
                                self.messagesBadge_lbl.text = String(messages_badgecount)
                            }
                            else
                            {
                                self.messagesBadge_lbl.isHidden = true
                                self.messagesBadge_lbl.text = ""
                            }
                        }
                    }
                    else
                    {
                        self.visitorsBadge_lbl.isHidden = true
                        self.visitorsBadge_lbl.text = ""
                        self.newMatchesBadge_lbl.isHidden = true
                        self.newMatchesBadge_lbl.text = ""
                    }
                })
            }
        }
    }
    
    
    //MARK:- ****** playerDidFinishPlaying ******
    func playerDidFinishPlaying(){
        //now use seek to make current playback time to the specified time in this case (O)
        let duration : Int64 = 0 //(can be Int32 also)
        let preferredTimeScale : Int32 = 1
        let seekTime : CMTime = CMTimeMake(duration, preferredTimeScale)
        self.player?.seek(to: seekTime)
        self.player?.play()
    }

 
    
    // MARK: -  ****** Get Network Status Reachibility Method ******
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        let getNetworkStatus = (userInfo?["Status"] as? String)!
        if getNetworkStatus == "Offline"
        {
            
        }
    }

    
    // MARK: - ****** View My Profile Button Action. ******
    @IBAction func viewProfile_btnAction(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() ) {
            let profileView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(profileView, animated: true)
        }
    }
    
    
    // MARK: - ****** Settings Button Action. ******
    @IBAction func settings_btnAction(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            let settingsScreen = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(settingsScreen, animated: true)
        }
    }
    
  
    
    // MARK: - ****** Wi-Match Button Action. ******
    @IBAction func wiMatch_btnAction(_ sender: Any) {
        matchBtnBool = true
        if matchBtnBool == true
        {
             self.wiMatchView.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
            self.wiMatchImage.image = UIImage(named : "Match Blue Icon")
            matchBtnBool = false
        }
        else{
            self.wiMatchView.layer.borderColor = UIColor.white.cgColor
            self.wiMatchImage.image = UIImage(named : "Match Icon")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() ) {
            let meetUpScreen = self.storyboard?.instantiateViewController(withIdentifier: "WiMatch_MeetUpViewController") as! WiMatch_MeetUpViewController
            meetUpScreen.newLinkUpBadge = self.linkUpTotaCount
            meetUpScreen.newVibesBadge = self.vibesTotalCount
            self.navigationController?.pushViewController(meetUpScreen, animated: true)
        }
    }
    
    // MARK: - ****** Wi-Visitors Button Action. ******
    @IBAction func wiVisitors_btnAction(_ sender: Any) {
        visitorBool = true
        if visitorBool == true
        {
            self.wiVisitorsView.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
            self.wiVisitorsImage.image = UIImage(named : "Visitors Blue Icon")
        }
        else{
            self.wiVisitorsView.layer.borderColor = UIColor.white.cgColor
            self.wiVisitorsImage.image = UIImage(named : "Visitors Icon")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() ) {
            let visitorsScreen = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVisitorsViewController") as! ProfileVisitorsViewController
            self.navigationController?.pushViewController(visitorsScreen, animated: true)
        }
       
    }
   
    
    // MARK: - ****** Wi-Say Button Action. ******
    @IBAction func WiSay_btnAction(_ sender: Any) {
        sayBtnBool = true
        if sayBtnBool == true
        {
            self.wiPeopleView.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
            self.wiSayImage.image = UIImage(named : "People Blue Icon")
            sayBtnBool = false
        }
        else
        {
            self.wiPeopleView.layer.borderColor = UIColor.white.cgColor
            self.wiSayImage.image = UIImage(named : "People Icon")
        }

        DispatchQueue.main.asyncAfter(deadline: .now() ) {
            let eventsScreen = self.storyboard?.instantiateViewController(withIdentifier: "WiConfessionsViewController") as! WiConfessionsViewController
            eventsScreen.currentLatitudeFromHomePage = self.currentLatitude
            eventsScreen.currentLongitudeFromHomePage = self.currentLongitude
            eventsScreen.currentLocationFromHomePage = self.LocationStr
            self.navigationController?.pushViewController(eventsScreen, animated: true)
        }
    }
    
    
    // MARK: - ****** Wi-Search People Button Action. ******
    @IBAction func wiPeopleSearch_btnAction(_ sender: Any) {
        wiPeopleBool = true
        if wiPeopleBool == true
        {
             self.wiEventsView.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
            wiPeopleBool = false
        }
        else
        {
             self.wiEventsView.layer.borderColor = UIColor.white.cgColor
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            let searchScreen = self.storyboard?.instantiateViewController(withIdentifier: "SearchPeopleViewController") as! SearchPeopleViewController
            self.navigationController?.pushViewController(searchScreen, animated: true)
        }
    }
    
    // MARK: - ****** Wi Chat Room Button Action. ******
    @IBAction func wiChatRoom_btnAction(_ sender: Any) {
        groupChatBool = true
        if groupChatBool == true
        {
            self.wiChatRoomView.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
            self.wiChatRoomInage.image = UIImage(named : "Wi chat room Blue Icon")
            groupChatBool = false
        }
        else{
            self.wiChatRoomView.layer.borderColor = UIColor.white.cgColor
            self.wiChatRoomInage.image = UIImage(named : "Wi chat room Icon")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() ) {
            let groupChatScreen = self.storyboard?.instantiateViewController(withIdentifier: "GroupChatMeassagesViewController") as! GroupChatMeassagesViewController
            groupChatScreen.appDelegatePush = "FromHomePage"
            self.navigationController?.pushViewController(groupChatScreen, animated: true)
        }
   
    }
    
    // MARK: - ****** Wi Messages Button Action. ******
    @IBAction func wiMessages_btnAction(_ sender: Any) {
        messagesBtnBool = true
        if messagesBtnBool == true
        {
            self.wiMessagesView.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
             self.wiMessageImage.image = UIImage(named : "Messages Blue Icon")
            messagesBtnBool = false
        }
        else
        {
            self.wiMessagesView.layer.borderColor = UIColor.white.cgColor
            self.wiMessageImage.image = UIImage(named : "Messages Icon")
        }

         DispatchQueue.main.asyncAfter(deadline: .now() ) {
            let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "ChatListingViewController") as! ChatListingViewController
            self.navigationController?.pushViewController(messagesScreen, animated: true)
        }
    }
    
    
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}
