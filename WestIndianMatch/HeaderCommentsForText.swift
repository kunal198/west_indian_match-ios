//
//  HeaderCommentsForText.swift
//  Wi Match
//
//  Created by brst on 01/08/18.
//  Copyright © 2018 brst. All rights reserved.
//

import UIKit

class HeaderCommentsForText: UITableViewHeaderFooterView {

    
    @IBOutlet weak var confessionLikeIcon: UIImageView!
    @IBOutlet weak var likesCountLbl: UILabel!
    @IBOutlet weak var usersProfilePic: UIImageView!
    
    @IBOutlet weak var messageBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var likesBtn: UIButton!
    @IBOutlet weak var repliesCountLbl: UILabel!
    @IBOutlet weak var dropDownOptionsBtn: UIButton!
    
    @IBOutlet weak var usersLocationLbl: UILabel!
    @IBOutlet weak var usersNameLbl: UILabel!
    
    @IBOutlet weak var statusPostedTxtView: UITextView!
    @IBOutlet weak var viewProfileBtn: UIButton!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
