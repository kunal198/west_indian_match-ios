//
//  ChatListingViewController.swift
//  Wi Match
//
//  Created by brst on 14/04/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
import AVKit

class ChatListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    //MARK:- ****** VARIABLES DECLARATION  ******
    
    var totalTotalChatMessages = Int()
    
    var itemsChat = [Conversation]()
    var selectedUser: User?
    var profilePicStr: String!
    var blockedByUserArray = NSMutableArray()
    var blockedUsersArray  = NSMutableArray()
    
    var messagesPushBool = Bool()
    var opponentUserID = String()
    var opponentFirebaseID = String()
    var opponentName = String()
    var typeOfUser = String()
    var postID = String()
    var opponentImage = String()
    var opponentVisibilityFromChatStr = String()
    var myVisiblityFromChatStr = String()
    var appDelegatePush = String()
    var checkVisibiltyStr = String()
    var mainConfessionCheck = String()
    
    var checkAnonymousConversationExistsOrNot = String()
    var checkConversationExistsOrNot = String()
    
    var chatLocationToBeDeleted = String()
    var checkRevealNotificationCalled = Bool()
    
    //MARK:- ****** OUTLETS DECLARATION  ******
    @IBOutlet weak var noMeesagesFoundView: UIView!
    @IBOutlet weak var LoadingView: UIView!
    @IBOutlet weak var chatListing_tableView: UITableView!
    @IBOutlet weak var loadingImage: UIImageView!
    
    // MARK: - ****** ViewDidLoad. ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.noMeesagesFoundView.isHidden = true
        self.chatListing_tableView.separatorColor = UIColor.clear
        self.checkRevealNotificationCalled = false
        
        if (self.messagesPushBool == true)
        {
            self.view.isUserInteractionEnabled = false
            self.LoadingView.isHidden = false
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                
               /* let messageScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                
                messageScreen.typeOfUser = "WithProfileUser"
                messageScreen.opponentUserID = self.opponentUserID
                messageScreen.opponentName = self.opponentName
                messageScreen.typeOfUser = self.typeOfUser
                messageScreen.postID = self.postID
                messageScreen.opponentImage = self.opponentImage
                messageScreen.opponentFirebaseUserID = self.opponentFirebaseID
                
                if (self.checkVisibiltyStr == "withProfileChat") || (self.checkVisibiltyStr == "FromViewProfile")
                {
                    messageScreen.myVisiblityFromChatStr = self.myVisiblityFromChatStr
                    messageScreen.opponentVisibilityFromChatStr = self.opponentVisibilityFromChatStr
                    messageScreen.mainConfessionCheck = "comingFromChatListing"
                    messageScreen.checkVisibiltyStr = "withProfileChat"
                }
                else{
                    messageScreen.checkVisibiltyStr = "withAnonymousChat"
                }
                
                messageScreen.appDelegatePush = "fromHomePage"
                self.navigationController?.pushViewController(messageScreen, animated: true)*/
                
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                messagesScreen.mainConfessionCheck = "comingFromChatListing"
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromListingScreen"
                
                messagesScreen.opponentUserID = self.opponentUserID
                messagesScreen.opponentName = self.opponentName
                messagesScreen.typeOfUser = self.typeOfUser
                messagesScreen.postID = self.postID
                messagesScreen.opponentImage = self.opponentImage
                messagesScreen.opponentFirebaseUserID = self.opponentFirebaseID
               
                print("self.typeOfUser = \(self.typeOfUser)")
                
                var userType = String()
                userType = self.typeOfUser
                
                if (userType == "AnonymousUser") || (userType == "WithProfileConfessionUser") || (userType == "withAnonymousChat")
                {
                    if (userType == "WithProfileConfessionUser")
                    {
                        messagesScreen.typeOfUser = "AnonymousUser"
                        messagesScreen.confessionTypeUser = "WithProfileConfessionUser"
                    }
                    else{
                        messagesScreen.typeOfUser = "AnonymousUser"
                        messagesScreen.confessionTypeUser = "AnonymousUser"
                    }
                }
                else
                {
                    messagesScreen.typeOfUser = "WithProfileUser"
                }
                
                if (self.checkVisibiltyStr == "withProfileChat") || (self.checkVisibiltyStr == "FromViewProfile")
                {
                    messagesScreen.myVisiblityFromChatStr = self.myVisiblityFromChatStr
                    messagesScreen.opponentVisibilityFromChatStr = self.opponentVisibilityFromChatStr
                    messagesScreen.mainConfessionCheck = "comingFromChatListing"
                    messagesScreen.checkVisibiltyStr = "withProfileChat"
                }
                else{
                    messagesScreen.checkVisibiltyStr = "withAnonymousChat"
                }
                
                messagesScreen.appDelegatePush = "fromHomePage"
                self.navigationController?.pushViewController(messagesScreen, animated: true)
                
                self.view.isUserInteractionEnabled = true
                self.LoadingView.isHidden = true
            }
        }
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                var dataDict = [String : Any]()
                dataDict["MessagesCount"] = "0"
               FIRDatabase.database().reference().child("Users").child(id).updateChildValues(dataDict)
            }
            
            self.updateBadgeValueForVisitors(totalBadgeCount: 0, completion: { (resultBadge) in
                
            })
            
            self.LoadingView.isHidden = false
            self.LoadingView.layer.cornerRadius = 15
            self.LoadingView.layer.masksToBounds = false
            self.chatListing_tableView.separatorColor = UIColor.clear
            self.view.isUserInteractionEnabled = false
      
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            print("appDelegate.messagesCount = \(appDelegate.messagesCount)")
            appDelegate.messagesCount = 0
            UserDefaults.standard.set(0, forKey: "messagesCount")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMessageBadge"), object: nil, userInfo: nil)
            
            // Register to receive notification
            NotificationCenter.default.addObserver(self, selector: #selector(ChatListingViewController.methodOfReceivedNotification), name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
            
             NotificationCenter.default.addObserver(self, selector: #selector(ChatListingViewController.updateChatOnReceive), name: NSNotification.Name(rawValue: "updateChatOnReceive"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(ChatListingViewController.updateListingOnRevealProfile), name: NSNotification.Name(rawValue: "updateListingOnRevealProfile"), object: nil)

            self.deleteConversation(completion: { (result) in
                
//                if (result == true)
//                {
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteChatNotification"), object: nil, userInfo: nil)
                
                for index in 0..<self.itemsChat.count
                {
//                    print("self.itemsChat[index].locationOfThread = \(self.itemsChat[index].locationOfThread)")
//                    print("self.chatLocationToBeDeleted = \(self.chatLocationToBeDeleted)")
                    
                    if (self.chatLocationToBeDeleted == self.itemsChat[index].locationOfThread)
                    {
                        print("chat to be deleted string at index is same")
                        self.itemsChat.remove(at: index)
                        self.chatListing_tableView.reloadData()
                        break
                    }
                }
//                }
            })
            
            self.deleteConversation_AnonymousUser(completion: { (resBool) in
                
                print("Anonymous Conversation is deleted")
                
//                if (resBool == true)
//                {
                for index in 0..<self.itemsChat.count
                {
//                    print("self.itemsChat[index].locationOfThread = \(self.itemsChat[index].locationOfThread)")
//                    print("self.chatLocationToBeDeleted = \(self.chatLocationToBeDeleted)")
                    
                    if (self.chatLocationToBeDeleted == self.itemsChat[index].locationOfThread)
                    {
                        self.itemsChat.remove(at: index)
                        self.chatListing_tableView.reloadData()
                        break
                    }
                }
//                }
                
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteChatNotification"), object: nil, userInfo: nil)
            })
            
            
            self.checkProfileChatExistsOrNot(completion: { (responseBool) in
                
                if (responseBool == true)
                {
                    self.LoadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    self.checkAnonymousConversationExistsOrNot(completion: { (statusResult) in
                        if (statusResult == true)
                        {
                            print("anonymous conversation exists")
                            
                            self.showNoDataFoundView(completion: { (result) in
                                
                            })
                        }
                        else
                        {
                            print("anonymous conversation doesnot exists")
                            self.showNoDataFoundView(completion: { (result) in
                                
                            })
                        }
                    })
                }
                else
                {
                    self.LoadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                   
                    self.checkAnonymousConversationExistsOrNot(completion: { (statusResult) in
                        if (statusResult == true)
                        {
                            print("anonymous conversation exists")
                            self.showNoDataFoundView(completion: { (result) in
                                
                            })
                        }
                        else
                        {
                            print("anonymous conversation doesnot exists")
                            self.showNoDataFoundView(completion: { (result) in
                                
                            })
                        }
                    })
                }
            })
        }
    }
    
    
    func updateBadgeValueForVisitors(totalBadgeCount: Int ,completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                ApiHandler.updateBadgeCountValue(user_id: userID, auth_token: userToken, typeofBadgeCountToBeUpdated: "messages_badgecount", totalBadgeCount: totalBadgeCount, completion: { (responseData) in
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
//                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        completion(true)
                    }
                    else
                    {
                        completion(false)
                    }
                })
            }
        }
    }
    
    func showNoDataFoundView(completion: @escaping (Bool) -> Swift.Void)
    {
        if (self.checkConversationExistsOrNot == "doesNotExists") && (self.checkAnonymousConversationExistsOrNot == "doesNotExists")
        {
            self.LoadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            self.itemsChat = []
            self.noMeesagesFoundView.isHidden = false
            self.noMeesagesFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.noMeesagesFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })
            
            self.chatListing_tableView.reloadData()
        }
        else
        {
            self.getTotalCountConversations(completion: { (result) in
                
                self.getTotalCountConversations_anonymous(completion: { (reultAnonym) in
                    
                    self.fetchData(completion: { (response) in
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        
                        print("self.totalTotalChatMessages = \(self.totalTotalChatMessages)")
                        print("appDelegate.threadCount = \(appDelegate.threadCount)")

                        if (self.totalTotalChatMessages ==  appDelegate.threadCount)
                        {
                            DispatchQueue.main.async {
                                self.LoadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.chatListing_tableView.reloadData()
                            }
                        }
                        else if (self.totalTotalChatMessages == 0)
                        {
                            DispatchQueue.main.async {
                                self.LoadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.chatListing_tableView.reloadData()
                            }
                        }
                        else if (self.checkRevealNotificationCalled == true)
                        {
                            self.checkRevealNotificationCalled = false
                            DispatchQueue.main.async {
                                self.LoadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.chatListing_tableView.reloadData()
                            }
                        }
                    })
                })
            })
        }
    }
    
    // MARK: - ****** Fetch List Of Anonymous Conversation for current user. ******
    func checkProfileChatExistsOrNot(completion: @escaping (Bool) -> Swift.Void)
    {
        self.LoadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
        
        let reef = FIRDatabase.database().reference()
        
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            reef.child("Users").child(currentUserID).child("conversations").keepSynced(true)
            reef.child("Users").child(currentUserID).child("conversations").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists()
                {
                    self.checkConversationExistsOrNot = "exists"
                    completion(true)
                }
                else
                {
                    self.checkConversationExistsOrNot = "doesNotExists"
                    completion(false)
                }
            })
        }
    }
    
    
    // MARK: - ****** Fetch List Of Anonymous Conversation for current user. ******
    func checkAnonymousConversationExistsOrNot(completion: @escaping (Bool) -> Swift.Void)
    {
        self.LoadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
        let reef = FIRDatabase.database().reference()
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            reef.child("Users").child(currentUserID).child("conversations_anonymous").keepSynced(true)
            reef.child("Users").child(currentUserID).child("conversations_anonymous").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists()
                {
                    self.checkAnonymousConversationExistsOrNot = "exists"
                    completion(true)
                }
                else
                {
                    self.checkAnonymousConversationExistsOrNot = "doesNotExists"
                    completion(false)
                }
            })
        }
    }
  
    
    func updateListingOnRevealProfile(notification: NSNotification)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
           
           self.checkProfileChatExistsOrNot(completion: { (responseBool) in
            
               self.checkRevealNotificationCalled = true
            
                if (responseBool == true)
                {
                    self.LoadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    self.checkAnonymousConversationExistsOrNot(completion: { (statusResult) in
                        
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                        appDel.threadCount = 0
                        
                        if (statusResult == true)
                        {
                            self.showNoDataFoundView(completion: { (result) in
                            })
                        }
                        else
                        {
                            self.showNoDataFoundView(completion: { (result) in
                                
                            })
                        }
                    })
                    
                }
                else
                {
                    self.LoadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    self.checkAnonymousConversationExistsOrNot(completion: { (statusResult) in
                        
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                        appDel.threadCount = 0
                        
                        if (statusResult == true)
                        {
                            self.showNoDataFoundView(completion: { (result) in
                               
                            })
                        }
                        else
                        {
                            self.showNoDataFoundView(completion: { (result) in
                                
                            })
                        }
                    })
                }
            })
        }
    }
    
    // MARK: - ****** Notification when no conversations exists. ******
    func methodOfReceivedNotification(notification: NSNotification)
    {
//        self.LoadingView.isHidden = true
//        self.view.isUserInteractionEnabled = true
//        self.itemsChat = []
//        DispatchQueue.main.async {
//            self.chatListing_tableView.reloadData()
//        }
        
    }
    
    // MARK: - ****** Notification when no conversations exists. ******
    func updateChatOnReceive(notification: NSNotification)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            print("online")
        }
    }


    
    // MARK: - ****** Fetch last message for all conversations. ******
    func fetchData(completion: @escaping (Bool) -> Swift.Void) {
        
        Conversation.showConversations { (conversations) in
            self.itemsChat = conversations
            self.itemsChat.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp }
            completion(true)
        }
    }
    
    
    func getTotalCountConversations(completion: @escaping (Bool) -> Swift.Void)
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            let ref = FIRDatabase.database().reference()
            ref.child("Users").child(currentUserID).child("conversations").keepSynced(true)
            ref.child("Users").child(currentUserID).child("conversations").observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists()
                {
                    self.totalTotalChatMessages = Int(snapshot.childrenCount)
                    completion(true)
                }
                else
                {
                    self.totalTotalChatMessages = 0
                    completion(true)
                }
            })
        }else{
            self.totalTotalChatMessages = 0
            completion(true)
        }
       
    }
    
    func getTotalCountConversations_anonymous(completion: @escaping (Bool) -> Swift.Void)
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            let ref = FIRDatabase.database().reference()
            ref.child("Users").child(currentUserID).child("conversations_anonymous").keepSynced(true)
            ref.child("Users").child(currentUserID).child("conversations_anonymous").observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists()
                {
                    let totalCount = Int(snapshot.childrenCount)
                    self.totalTotalChatMessages = self.totalTotalChatMessages + totalCount
                    completion(true)
                }
                else
                {
                    completion(true)
                }
            })
        }else{
            completion(true)
        }
        
    }
    
    
  /*  // MARK: - ****** Fetch last message for all conversations. ******
    func fetchDataAnonymousChat(completion: @escaping (Bool) -> Swift.Void) {
        Conversation.showAnonymousConversations { (conversations) in
            self.itemsChat = conversations
            self.itemsChat.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp }
            DispatchQueue.main.async {
                self.LoadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                self.chatListing_tableView.reloadData()
                completion(true)
            }
        }
    }*/
    
    
    // MARK: - ****** Back Button Action. ******
    @IBAction func back_btnAction(_ sender: Any) {
        
        if appDelegatePush == "fromAppDelegate"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        else
        {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    
    func getUpdatedMessage(forLocation: String,opponentIDWithVisibility: String,getUserType: String, completion: @escaping (String) -> Swift.Void) {
    FIRDatabase.database().reference().child("conversations").child(forLocation).observe(.childAdded, with: { (snapshot) in
               
               if snapshot.exists() {
                    
                    print("snapshot exists")
                    
                   let receivedMessage = snapshot.value as! [String: Any]
                   var message = String()
                
                   if (receivedMessage["content"] != nil)
                   {
                     message = receivedMessage["content"]! as! String
                     completion(message)
                   }
                }
                else
                {
                    print("snapshot doesnot exists")
                }
            })
    }
    
    
    func getUpdatedMessage_Anonymous(forLocation: String,opponentIDWithVisibility: String,getUserType: String, completion: @escaping (String) -> Swift.Void) {
        
        FIRDatabase.database().reference().child("conversations_anonymous").child(forLocation).observe(.childAdded, with: { (snapshot) in
            
            if snapshot.exists() {
                
                print("snapshot exists")
                
                let receivedMessage = snapshot.value as! [String: Any]
                var message = String()
                
                if (receivedMessage["content"] != nil)
                {
                    message = receivedMessage["content"]! as! String
                    completion(message)
                }
            }
            else
            {
                print("snapshot doesnot exists")
            }
        })
    }
    
    
    // MARK: - ****** UITableView Delegates and DataSource Methods. ******
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.itemsChat.count > 0
        {
            self.noMeesagesFoundView.isHidden = true
            return self.itemsChat.count
        }

        return 0
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ConversationsTBCell
        let ref = FIRDatabase.database().reference()
        
        if (self.itemsChat.count <= 0)
        {
            self.itemsChat = []
            return cell
        }
        else
        {
            if (self.blockedUsersArray.count > 0) || (self.blockedByUserArray.count > 0)
            {
                if (self.blockedUsersArray.contains(self.itemsChat[indexPath.row].user.firebase_user_id)) || (self.blockedByUserArray.contains(self.itemsChat[indexPath.row].user.firebase_user_id))
                {
                    print("blocked user and profile visitor is same")
                    cell.onlineIcon.isHidden = true
                }
                else
                {
                    
                }
            }
        }
    ref.child("Users").child(self.itemsChat[indexPath.row].user.firebase_user_id).child("userStatus").keepSynced(true)
    ref.child("Users").child(self.itemsChat[indexPath.row].user.firebase_user_id).child("userStatus").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if snapshot.value is NSNull
            {
                cell.onlineIcon.isHidden = true
            }
            else
            {
                if snapshot.value as! String == "offline"
                {
                    cell.onlineIcon.isHidden = true
                }
                else
                {
                    cell.onlineIcon.isHidden = false
                }
            }
        })
 
        cell.timeLabel.isHidden = false
        cell.wrapperBtn.tag = indexPath.row
        cell.wrapperBtn.isUserInteractionEnabled = false
        cell.wrapperBtn.addTarget(self, action: #selector(ChatListingViewController.SenderViewPhotoBtn(_:)), for: .touchUpInside)
        
        cell.onlineIcon.frame.size.width = 12
        cell.onlineIcon.frame.size.height = 12
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        cell.profilePic.frame.size.width = 60
        cell.profilePic.frame.size.height = 60
        cell.profilePic.layer.cornerRadius =  cell.profilePic.frame.size.height/2
        cell.profilePic.layer.masksToBounds = true
        cell.profilePic.clipsToBounds = true
        cell.profilePic.tag = indexPath.row
        cell.profilePic.contentMode = UIViewContentMode.scaleAspectFill
        
            switch self.itemsChat[indexPath.row].lastMessage.type {
                
            case .postedConfession:
                
                var message = String()
                
                if self.itemsChat[indexPath.row].lastMessage.postedConfessionCheck == "textWithImage"
                {
                    message = "Media"
                }
                else
                {
                    message = self.itemsChat[indexPath.row].lastMessage.postedText!
                }
                
                var myConversationTypeStr = String()
                myConversationTypeStr = self.itemsChat[indexPath.row].lastMessage.myConversationType!
                
                if (myConversationTypeStr == "withProfileChat")
                {
                    print("++++++++++++++++++++ withProfileChat ++++++++++++++++++++")
                    
                    let oppoIDWithVisibilityStr = self.itemsChat[indexPath.row].opponentIDWithVisibility
                    var getVisibilityStr = String()
                    
                    if (oppoIDWithVisibilityStr.range(of: "@") != nil)
                    {
                        let fullUserID = oppoIDWithVisibilityStr.components(separatedBy: "@")
                        let AfterAtTheRateStr: String = fullUserID[1]
                        getVisibilityStr = AfterAtTheRateStr
                    }
                    else
                    {
                        getVisibilityStr = "On"
                    }
                    
                    var myIDStr = String()
//                    if let currentUserID = FIRAuth.auth()?.currentUser?.uid {
                    if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
                        myIDStr = currentUserID
                    }
                    
                    if ((self.itemsChat[indexPath.row].user.firebase_user_id) == myIDStr)
                    {
                        if self.itemsChat[indexPath.row].user.profilePicType == "image"
                        {
                            if (self.itemsChat[indexPath.row].user.profilePic != nil)
                            {
                                DispatchQueue.main.async
                                    {
                                        cell.profilePic.image = nil
                                        if let image =
                                            self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                        {
                                            cell.profilePic.image = image
                                        }
                                }
                            }
                        }
                        else{
                            self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                            cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                        }
                        
                        cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                        cell.nameLabel.textColor = UIColor.white
                        cell.profilePic.backgroundColor = UIColor.clear
                    }
                    else if ((self.itemsChat[indexPath.row].user.firebase_user_id) != myIDStr)
                    {
                        FIRDatabase.database().reference().child("Users").child(self.itemsChat[indexPath.row].user.firebase_user_id).child("Settings").child("General").child("Profile_Visibility").observe(FIRDataEventType.value, with: { (snapshot) in
                            
                            if snapshot.value is NSNull
                            {
                                cell.profilePic.backgroundColor = UIColor.clear
                                
                                DispatchQueue.main.async {
                                    cell.profilePic.image = nil
                                    cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                }
                                
                                cell.nameLabel.text = "VIP Member"
                                cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                            }
                            else
                            {
                                if (getVisibilityStr == "on") || (getVisibilityStr == "On") //(snapshot.value as! String == "On")
                                {
                                    if self.itemsChat[indexPath.row].user.profilePicType == "image"
                                    {
                                        
                                        if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                        {
                                            DispatchQueue.main.async {
                                                cell.profilePic.image = nil
                                                if let image =
                                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                                {
                                                    cell.profilePic.image = image
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                        cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                                    }
                                    
                                    cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                                    cell.nameLabel.textColor = UIColor.white
                                    cell.profilePic.backgroundColor = UIColor.clear
                                }
                                else
                                {
                                    cell.profilePic.backgroundColor = UIColor.clear
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                    }
                                    
                                    cell.nameLabel.text = "VIP Member"
                                    cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                }
                            }
                        })
                    }
                    else
                    {
                        cell.profilePic.backgroundColor = UIColor.clear
                        DispatchQueue.main.async {
                            cell.profilePic.image = nil
                            cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                        }
                        
                        cell.nameLabel.text = "VIP Member"
                        cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    }
                }
                else
                {
                    print("++++++++++++++++++++ withAnonymousChat ++++++++++++++++++++")
                  
                    var myIDStr = String()
                    if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
                        myIDStr = currentUserID
                    }
                    
//                    if ((self.itemsChat[indexPath.row].lastMessage.toID) == myIDStr)
                    if (self.itemsChat[indexPath.row].user.id == myIDStr)
                    {
                        if ((self.itemsChat[indexPath.row].lastMessage.myRevealProfileStatus!) == "profileRevealed")
                        {
                            for subview in cell.profilePic.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            if self.itemsChat[indexPath.row].user.profilePicType == "image"
                            {
                                if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                {
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        if let image =
                                            self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                        {
                                            cell.profilePic.image = image
                                        }
                                    }
                                }
                            }
                            else
                            {
                                self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                            }
                            
                            cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                            cell.nameLabel.textColor = UIColor.white
                            cell.profilePic.backgroundColor = UIColor.clear
                        }
                        else
                        {
                            cell.profilePic.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                                cell.profilePic.image = nil
//                                cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "moodIcon-1"))
                                
                                if let image =
                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                {
                                    cell.profilePic.image = image
                                }
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.profilePic.bounds
                            cell.profilePic.addSubview(blurEffectView)
                            
                            cell.nameLabel.text = "Anonymous User"
                            cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                        }
                    }
                    else
                    {
                        if ((self.itemsChat[indexPath.row].lastMessage.opponentRevealProfileStatus!) == "profileRevealed")
                        {
                            for subview in cell.profilePic.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            if self.itemsChat[indexPath.row].user.profilePicType == "image"
                            {
                                if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                {
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        if let image =
                                            self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                        {
                                            cell.profilePic.image = image
                                        }
                                    }
                                }
                            }
                            else{
                                self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                            }
                            
                            cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                            cell.nameLabel.textColor = UIColor.white
                            cell.profilePic.backgroundColor = UIColor.clear
                        }
                        else
                        {
                            cell.profilePic.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                                cell.profilePic.image = nil
//                                cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "moodIcon-1"))
                                if let image =
                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                {
                                    cell.profilePic.image = image
                                }
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.profilePic.bounds
                            cell.profilePic.addSubview(blurEffectView)
                            
                            cell.nameLabel.text = "Anonymous User"
                            cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                        }
                    }
                }
                
                if message == "loading"
                {
                    cell.messageLabel.text = "loading.."
                }
                else
                {
                    if self.itemsChat[indexPath.row].lastMessage.isRead == false
                    {
                        cell.messageLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    }
                    else
                    {
                        cell.messageLabel.textColor = UIColor.white
                    }
                    
                    cell.messageLabel.text = message
                    
                    if (myConversationTypeStr == "withProfileChat")
                    {
                        self.getUpdatedMessage(forLocation: self.itemsChat[indexPath.row].locationOfThread, opponentIDWithVisibility: "", getUserType: "") { (result) in
                            cell.messageLabel.text = result
                        }
                    }
                    else
                    {
                        self.getUpdatedMessage_Anonymous(forLocation: self.itemsChat[indexPath.row].locationOfThread, opponentIDWithVisibility: "", getUserType: "") { (result) in
                            cell.messageLabel.text = result
                        }
                    }
                }
                
            case .text:
                
                let message = self.itemsChat[indexPath.row].lastMessage.content as! String
                
                var myConversationTypeStr = String()
                myConversationTypeStr = self.itemsChat[indexPath.row].lastMessage.myConversationType!
 
                if (myConversationTypeStr == "withProfileChat")
                {
                    print("++++++++++++++++++++ withProfileChat ++++++++++++++++++++")
                    
                    let oppoIDWithVisibilityStr = self.itemsChat[indexPath.row].opponentIDWithVisibility
//                    let myIDWithVisibilityStr = self.itemsChat[indexPath.row].myIDWithVisibility
                    var getVisibilityStr = String()
                    
                    if (oppoIDWithVisibilityStr.range(of: "@") != nil)
                    {
                        let fullUserID = oppoIDWithVisibilityStr.components(separatedBy: "@")
                        let AfterAtTheRateStr: String = fullUserID[1]
                        getVisibilityStr = AfterAtTheRateStr
                    }
                    else
                    {
                        getVisibilityStr = "On"
                    }
                    
                    
                    var myIDStr = String()
                    if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
                        myIDStr = currentUserID
                    }
                  
                    if ((self.itemsChat[indexPath.row].user.firebase_user_id) == myIDStr)
                    {
                        if self.itemsChat[indexPath.row].user.profilePicType == "image"
                        {
                            if (self.itemsChat[indexPath.row].user.profilePic != nil)
                            {
                                DispatchQueue.main.async {
                                    cell.profilePic.image = nil
                                    if let image =
                                        self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                    {
                                        cell.profilePic.image = image
                                    }
                                }
                            }
                        }
                        else{
                            self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                            cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                        }
                        
                        cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                        cell.nameLabel.textColor = UIColor.white
                        cell.profilePic.backgroundColor = UIColor.clear
                    }
                    else if ((self.itemsChat[indexPath.row].user.firebase_user_id) != myIDStr)
                    {
                        FIRDatabase.database().reference().child("Users").child(self.itemsChat[indexPath.row].user.firebase_user_id).child("Settings").child("General").child("Profile_Visibility").observe(FIRDataEventType.value, with: { (snapshot) in
                            
                            if snapshot.value is NSNull
                            {
                                cell.profilePic.backgroundColor = UIColor.clear
                                DispatchQueue.main.async {
                                   cell.profilePic.image = nil
                                   cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                }
                                
                                cell.nameLabel.text = "VIP Member"
                                cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                            }
                            else
                            {
                                if (getVisibilityStr == "on") || (getVisibilityStr == "On")
                                {
                                    if (self.itemsChat.count > 0 && self.itemsChat.count > indexPath.row)
                                    {
                                        if (self.itemsChat[indexPath.row].user != nil)
                                        {
                                            if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                            {
                                                if (self.itemsChat.count > indexPath.row)
                                                {
                                                    DispatchQueue.main.async {
                                                        cell.profilePic.image = nil
                                                        
                                                        if (self.itemsChat.count > indexPath.row)
                                                        {
                                                            if let image =
                                                                self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                                            {
                                                                cell.profilePic.image = image
                                                            }
                                                        }
                                                      
                                                    }
                                                }
                                                
                                            }
                                        }
                                        
                                        cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                                        cell.nameLabel.textColor = UIColor.white
                                        cell.profilePic.backgroundColor = UIColor.clear
                                    }
                                }
                                else
                                {
                                    
                                    cell.profilePic.backgroundColor = UIColor.clear
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                    }
                                    
                                    cell.nameLabel.text = "VIP Member"
                                    cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                }
                            }
                        })
                    }
                    else
                    {
                        
                        cell.profilePic.backgroundColor = UIColor.clear
                        DispatchQueue.main.async {
                            cell.profilePic.image = nil
                            cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                        }
                        
                        cell.nameLabel.text = "VIP Member"
                        cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    }
                }
                else
                {
                    print("++++++++++++++++++++ withAnonymousChat ++++++++++++++++++++")
                    
                    var myIDStr = String()
//                    if let currentUserID = FIRAuth.auth()?.currentUser?.uid {
                    
                    if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
                        myIDStr = currentUserID
                    }
                    
                    if (self.itemsChat.count > indexPath.row)
                    {
                       if (self.itemsChat[indexPath.row].lastMessage != nil)
                       {
                        if (self.itemsChat[indexPath.row].lastMessage.toID != nil)
                        {
                            if (self.itemsChat[indexPath.row].user.id == myIDStr)
                            {
                                if ((self.itemsChat[indexPath.row].lastMessage.myRevealProfileStatus!) == "profileRevealed")
                                {
                                    for subview in cell.profilePic.subviews
                                    {
                                        if subview.isKind(of: UIVisualEffectView.self)
                                        {
                                            subview.removeFromSuperview()
                                        }
                                    }
                                    
                                    if self.itemsChat[indexPath.row].user.profilePicType == "image"
                                    {
                                        if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                        {
                                            DispatchQueue.main.async {
                                                cell.profilePic.image = nil
                                                if let image =
                                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                                {
                                                    cell.profilePic.image = image
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                        cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                                    }
                                    
                                    cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                                    cell.nameLabel.textColor = UIColor.white
                                    cell.profilePic.backgroundColor = UIColor.clear
                                }
                                else
                                {
                                    
                                    cell.profilePic.backgroundColor = UIColor.clear
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        
                                        if let image =
                                            self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                        {
                                            cell.profilePic.image = image
                                        }
                                        
//                                        cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "moodIcon-1"))
                                    }
                                    
                                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                    blurEffectView.frame = cell.profilePic.bounds
                                    cell.profilePic.addSubview(blurEffectView)
                                    
                                    cell.nameLabel.text = "Anonymous User"
                                    cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                }
                            }
                            else
                            {
        
                                if ((self.itemsChat[indexPath.row].lastMessage.opponentRevealProfileStatus!) == "profileRevealed")
                                {
                                    for subview in cell.profilePic.subviews
                                    {
                                        if subview.isKind(of: UIVisualEffectView.self)
                                        {
                                            subview.removeFromSuperview()
                                        }
                                    }
                                    
                                    if self.itemsChat[indexPath.row].user.profilePicType == "image"
                                    {
                                        if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                        {
                                            DispatchQueue.main.async {
                                                cell.profilePic.image = nil
                                                if let image =
                                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                                {
                                                    cell.profilePic.image = image
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                        cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                                    }
                                    
                                    cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                                    cell.nameLabel.textColor = UIColor.white
                                    cell.profilePic.backgroundColor = UIColor.clear
                                    
                                }
                                else
                                {
                                    
                                    cell.profilePic.backgroundColor = UIColor.clear
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
//                                        cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "moodIcon-1"))
                                        
                                        if let image =
                                            self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                        {
                                            cell.profilePic.image = image
                                        }
                                        
                                    }
                                    
                                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                    blurEffectView.frame = cell.profilePic.bounds
                                    cell.profilePic.addSubview(blurEffectView)
                                    
                                    cell.nameLabel.text = "Anonymous User"
                                    cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                }
                            }
                        }
                      }
                    }
                }
                
                if message == "loading"
                {
                     cell.messageLabel.text = ""
                    
                    if (myConversationTypeStr == "withProfileChat")
                    {
                        self.getUpdatedMessage(forLocation: self.itemsChat[indexPath.row].locationOfThread, opponentIDWithVisibility: "", getUserType: "") { (result) in
                            
                            cell.messageLabel.text = result
                        }
                    }
                    else
                    {
                        self.getUpdatedMessage_Anonymous(forLocation: self.itemsChat[indexPath.row].locationOfThread, opponentIDWithVisibility: "", getUserType: "") { (result) in
                            
                            cell.messageLabel.text = result
                        }
                    }
                }
                else
                {
                    if self.itemsChat[indexPath.row].lastMessage.isRead == false
                    {
                        cell.messageLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    }
                    else
                    {
                        cell.messageLabel.textColor = UIColor.white
                    }
                    
                     cell.messageLabel.text = message
                    
                    if (myConversationTypeStr == "withProfileChat")
                    {
                        self.getUpdatedMessage(forLocation: self.itemsChat[indexPath.row].locationOfThread, opponentIDWithVisibility: "", getUserType: "") { (result) in
                            
                            cell.messageLabel.text = result
                        }
                    }
                    else
                    {
                        self.getUpdatedMessage_Anonymous(forLocation: self.itemsChat[indexPath.row].locationOfThread, opponentIDWithVisibility: "", getUserType: "") { (result) in
                            
                            cell.messageLabel.text = result
                        }
                    }
                }
                
               
            case .audio:
                
                if self.itemsChat[indexPath.row].lastMessage.isRead == false
                {
                    cell.messageLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                }
                else
                {
                    cell.messageLabel.textColor = UIColor.white
                }
                
                cell.messageLabel.text = "Audio"
                

                var myConversationTypeStr = String()
                myConversationTypeStr = self.itemsChat[indexPath.row].lastMessage.myConversationType!
                
                if (myConversationTypeStr == "withProfileChat")
                {
                    print("++++++++++++++++++++ withProfileChat ++++++++++++++++++++")
                    
                    let oppoIDWithVisibilityStr = self.itemsChat[indexPath.row].opponentIDWithVisibility
                    var getVisibilityStr = String()
                    
                    if (oppoIDWithVisibilityStr.range(of: "@") != nil)
                    {
                        let fullUserID = oppoIDWithVisibilityStr.components(separatedBy: "@")
                        let AfterAtTheRateStr: String = fullUserID[1]
                        getVisibilityStr = AfterAtTheRateStr
                    }
                    else
                    {
                        getVisibilityStr = "On"
                    }
                    
                    
                    var myIDStr = String()
                    
                    if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
                        myIDStr = currentUserID
                    }
                    
                    if ((self.itemsChat[indexPath.row].user.firebase_user_id) == myIDStr)
                    {
                        if self.itemsChat[indexPath.row].user.profilePicType == "image"
                        {
                            if (self.itemsChat[indexPath.row].user.profilePic != nil)
                            {
                                DispatchQueue.main.async {
                                    cell.profilePic.image = nil
                                    if let image =
                                        self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                    {
                                        cell.profilePic.image = image
                                    }
                                }
                            }
                        }
                        else{
                            self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                            cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                        }
                        
                        cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                        cell.nameLabel.textColor = UIColor.white
                        cell.profilePic.backgroundColor = UIColor.clear
                    }
                    else if ((self.itemsChat[indexPath.row].user.firebase_user_id) != myIDStr)
                    {
                        FIRDatabase.database().reference().child("Users").child(self.itemsChat[indexPath.row].user.firebase_user_id).child("Settings").child("General").child("Profile_Visibility").observe(FIRDataEventType.value, with: { (snapshot) in
                            
                            if snapshot.value is NSNull
                            {
                                
                                cell.profilePic.backgroundColor = UIColor.clear
                                DispatchQueue.main.async {
                                    cell.profilePic.image = nil
                                    cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                }
                                
                                cell.nameLabel.text = "VIP Member"
                                cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                            }
                            else
                            {
                                if (getVisibilityStr == "on") || (getVisibilityStr == "On") //(snapshot.value as! String == "On")
                                {
                                    if self.itemsChat[indexPath.row].user.profilePicType == "image"
                                    {
                                        
                                        if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                        {
                                            DispatchQueue.main.async {
                                                cell.profilePic.image = nil
                                                if let image =
                                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                                {
                                                    cell.profilePic.image = image
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                        cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                                    }
                                    
                                    cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                                    cell.nameLabel.textColor = UIColor.white
                                    cell.profilePic.backgroundColor = UIColor.clear
                                }
                                else
                                {
                                    
                                    cell.profilePic.backgroundColor = UIColor.clear
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                    }
                                    
                                    cell.nameLabel.text = "VIP Member"
                                    cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                }
                            }
                        })
                    }
                    else
                    {
                        
                        cell.profilePic.backgroundColor = UIColor.clear
                        DispatchQueue.main.async {
                            cell.profilePic.image = nil
                            cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                        }
                        
                        cell.nameLabel.text = "VIP Member"
                        cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    }
                }
                else
                {
                    print("++++++++++++++++++++ withAnonymousChat ++++++++++++++++++++")
                    
                    var myIDStr = String()
                    if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
                        myIDStr = currentUserID
                    }
                    
                    if (self.itemsChat[indexPath.row].user.id == myIDStr)
                    {
                        if ((self.itemsChat[indexPath.row].lastMessage.myPackagesStatus!) == "profileVisibilityPurchased") && ((self.itemsChat[indexPath.row].lastMessage.myRevealProfileStatus!) == "profileRevealed")
                        {
                            for subview in cell.profilePic.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            if self.itemsChat[indexPath.row].user.profilePicType == "image"
                            {
                                
                                if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                {
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        if let image =
                                            self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                        {
                                            cell.profilePic.image = image
                                        }
                                    }
                                }
                            }
                            else{
                                self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                            }
                            
                            cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                            cell.nameLabel.textColor = UIColor.white
                            cell.profilePic.backgroundColor = UIColor.clear
                        }
                        else
                        {
                            cell.profilePic.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                                cell.profilePic.image = nil
                                
                                if let image =
                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                {
                                    cell.profilePic.image = image
                                }
                                
//                                cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "moodIcon-1"))
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.profilePic.bounds
                            cell.profilePic.addSubview(blurEffectView)
                            
                            cell.nameLabel.text = "Anonymous User"
                            cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                        }
                    }
                    else
                    {
                        if ((self.itemsChat[indexPath.row].lastMessage.opponentRevealProfileStatus!) == "profileRevealed")
                        {
                            for subview in cell.profilePic.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            if self.itemsChat[indexPath.row].user.profilePicType == "image"
                            {
                                
                                if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                {
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        if let image =
                                            self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                        {
                                            cell.profilePic.image = image
                                        }
                                    }
                                }
                            }
                            else{
                                self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                            }
                            
                            cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                            cell.nameLabel.textColor = UIColor.white
                            cell.profilePic.backgroundColor = UIColor.clear
                            
                        }
                        else
                        {
                            cell.profilePic.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                                cell.profilePic.image = nil
//                                cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "moodIcon-1"))
                                
                                if let image =
                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                {
                                    cell.profilePic.image = image
                                }
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.profilePic.bounds
                            cell.profilePic.addSubview(blurEffectView)
                            
                            cell.nameLabel.text = "Anonymous User"
                            cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                        }
                    }
                }
                
              
            default:
                
                if self.itemsChat[indexPath.row].lastMessage.isRead == false
                {
                    cell.messageLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                }
                else
                {
                    cell.messageLabel.textColor = UIColor.white
                }
                
                cell.messageLabel.text = "Media"

           
                var myConversationTypeStr = String()
                myConversationTypeStr = self.itemsChat[indexPath.row].lastMessage.myConversationType!
                
              
                if (myConversationTypeStr == "withProfileChat")
                {
                    print("++++++++++++++++++++ withProfileChat ++++++++++++++++++++")
                    
                    let oppoIDWithVisibilityStr = self.itemsChat[indexPath.row].opponentIDWithVisibility
                    var getVisibilityStr = String()
                    
                    if (oppoIDWithVisibilityStr.range(of: "@") != nil)
                    {
                        let fullUserID = oppoIDWithVisibilityStr.components(separatedBy: "@")
                        let AfterAtTheRateStr: String = fullUserID[1]
                        getVisibilityStr = AfterAtTheRateStr
                    }
                    else
                    {
                        getVisibilityStr = "On"
                    }
                    
                    
                    var myIDStr = String()
                    if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
                        myIDStr = currentUserID
                    }
                    
                    if ((self.itemsChat[indexPath.row].user.firebase_user_id) == myIDStr)
                    {
                        if self.itemsChat[indexPath.row].user.profilePicType == "image"
                        {
                            if (self.itemsChat.count > 0 && self.itemsChat.count > indexPath.row)
                            {
                                if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                {
                                    if (self.itemsChat[indexPath.row].user != nil)
                                    {
                                        if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                        {
                                            DispatchQueue.main.async {
                                                cell.profilePic.image = nil
                                                if let image =
                                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                                {
                                                    cell.profilePic.image = image
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                           
                        }
                        else{
                            self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                            cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                        }
                        
                        cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                        cell.nameLabel.textColor = UIColor.white
                        cell.profilePic.backgroundColor = UIColor.clear
                    }
                    else if ((self.itemsChat[indexPath.row].user.firebase_user_id) != myIDStr)
                    {
                        FIRDatabase.database().reference().child("Users").child(self.itemsChat[indexPath.row].user.firebase_user_id).child("Settings").child("General").child("Profile_Visibility").observe(FIRDataEventType.value, with: { (snapshot) in
                            
                            if snapshot.value is NSNull
                            {
                                
                                cell.profilePic.backgroundColor = UIColor.clear
                                DispatchQueue.main.async {
                                    cell.profilePic.image = nil
                                    cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                }
                                
                                cell.nameLabel.text = "VIP Member"
                                cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                            }
                            else
                            {
                                if (getVisibilityStr == "on") || (getVisibilityStr == "On") //(snapshot.value as! String == "On")
                                {
                                    
                                    if (self.itemsChat.count > 0 && self.itemsChat.count > indexPath.row)
                                    {
                                        if (self.itemsChat[indexPath.row].user != nil)
                                        {
                                            if self.itemsChat[indexPath.row].user.profilePicType == "image"
                                            {
                                                if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                                {
                                                    if (self.itemsChat.count > indexPath.row)
                                                    {
                                                        DispatchQueue.main.async {
                                                            cell.profilePic.image = nil
                                                            
                                                            if (self.itemsChat.count > indexPath.row)
                                                            {
                                                                if let image =
                                                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                                                {
                                                                    cell.profilePic.image = image
                                                                }
                                                            }
                                                        }
                                                    }
                                                  
                                                }
                                            }
                                            else{
                                                self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                                cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                                            }
                                        }
                                        
                                        cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                                        cell.nameLabel.textColor = UIColor.white
                                        cell.profilePic.backgroundColor = UIColor.clear
                                    }
                                 
                                    cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                                    cell.nameLabel.textColor = UIColor.white
                                    cell.profilePic.backgroundColor = UIColor.clear
                                }
                                else
                                {
                                    
                                    cell.profilePic.backgroundColor = UIColor.clear
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                    }
                                    
                                    cell.nameLabel.text = "VIP Member"
                                    cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                }
                            }
                        })
                    }
                    else
                    {
                        
                        cell.profilePic.backgroundColor = UIColor.clear
                        DispatchQueue.main.async {
                            cell.profilePic.image = nil
                            cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                        }
                        
                        cell.nameLabel.text = "VIP Member"
                        cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                    }
                }
                else
                {
                    print("++++++++++++++++++++ withAnonymousChat ++++++++++++++++++++")
                    
                    var myIDStr = String()
                    if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
                        myIDStr = currentUserID
                    }
                    
                    if (self.itemsChat[indexPath.row].user.id == myIDStr)
                    {
                        if ((self.itemsChat[indexPath.row].lastMessage.myRevealProfileStatus!) == "profileRevealed")
                        {
                            for subview in cell.profilePic.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            if self.itemsChat[indexPath.row].user.profilePicType == "image"
                            {
                                
                                if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                {
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        if let image =
                                            self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                        {
                                            cell.profilePic.image = image
                                        }
                                    }
                                }
                            }
                            else{
                                self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                            }
                            
                            cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                            cell.nameLabel.textColor = UIColor.white
                            cell.profilePic.backgroundColor = UIColor.clear
                        }
                        else
                        {
                            
                            cell.profilePic.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                                cell.profilePic.image = nil
//                                cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "moodIcon-1"))
                                
                                if let image =
                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                {
                                    cell.profilePic.image = image
                                }
                                
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.profilePic.bounds
                            cell.profilePic.addSubview(blurEffectView)
                            
                            cell.nameLabel.text = "Anonymous User"
                            cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                        }
                    }
                    else
                    {
                        if ((self.itemsChat[indexPath.row].lastMessage.opponentRevealProfileStatus!) == "profileRevealed")
                        {
                            for subview in cell.profilePic.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            if self.itemsChat[indexPath.row].user.profilePicType == "image"
                            {
                                
                                if (self.itemsChat[indexPath.row].user.profilePic != nil)
                                {
                                    DispatchQueue.main.async {
                                        cell.profilePic.image = nil
                                        if let image =
                                            self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                        {
                                            cell.profilePic.image = image
                                        }
                                    }
                                }
                            }
                            else{
                                self.profilePicStr = self.itemsChat[indexPath.row].user.profilePicLink
                                cell.profilePic.image = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)
                            }
                            
                            cell.nameLabel.text = self.itemsChat[indexPath.row].user.name
                            cell.nameLabel.textColor = UIColor.white
                            cell.profilePic.backgroundColor = UIColor.clear
                          
                        }
                        else 
                        {
                            
                            cell.profilePic.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                                cell.profilePic.image = nil
//                               cell.profilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "moodIcon-1"))
                                
                                if let image =
                                    self.itemsChat[indexPath.row].user.profilePic as? UIImage
                                {
                                    cell.profilePic.image = image
                                }
                                
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.profilePic.bounds
                            cell.profilePic.addSubview(blurEffectView)
                            
                            cell.nameLabel.text = "Anonymous User"
                            cell.nameLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                        }
                    }
                }
            }
        
            cell.nameLabel.numberOfLines = 100
            cell.nameLabel.adjustsFontSizeToFitWidth = true
        
            let messageDate = Date.init(timeIntervalSince1970: TimeInterval(self.itemsChat[indexPath.row].lastMessage.timestamp))
            let dataformatter = DateFormatter.init()
            dataformatter.timeStyle = .short
            let date = dataformatter.string(from: messageDate)
            cell.timeLabel.text = date
            if self.itemsChat[indexPath.row].lastMessage.owner == .sender && self.itemsChat[indexPath.row].lastMessage.isRead == false {
            }
      
         cell.unseenMessages_lbl.isHidden = true

        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (self.itemsChat.count > 0 )
        {
            if (self.itemsChat[indexPath.row].user.firebase_user_id != "")
            {
                FIRDatabase.database().reference().child("Users").child(self.itemsChat[indexPath.row].user.firebase_user_id).keepSynced(true)
                FIRDatabase.database().reference().child("Users").child(self.itemsChat[indexPath.row].user.firebase_user_id).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                    
                    if snapshot.value is NSNull
                    {
                        let alert = UIAlertController(title: "", message: "Wi User no longer available", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        return
                    }
                    else
                    {
                        UserDefaults.standard.set(nil, forKey: "otherUsersDict")
                        DispatchQueue.main.asyncAfter(deadline: .now())
                        {
                            let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                            messagesScreen.mainConfessionCheck = "comingFromChatListing"
                            messagesScreen.chatUserDifferentiateStr = "Messages"
                            messagesScreen.viewOpponentProfile = "fromListingScreen"
                            messagesScreen.opponentName = self.itemsChat[indexPath.row].user.name
                            messagesScreen.opponentVisibilityFromChatStr = self.itemsChat[indexPath.row].opponentIDWithVisibility
                            messagesScreen.myVisiblityFromChatStr = self.itemsChat[indexPath.row].myIDWithVisibility
                            if self.itemsChat[indexPath.row].user.profilePicType == "image"
                            {
                                messagesScreen.opponentProfilePic = self.itemsChat[indexPath.row].user.profilePic
                                messagesScreen.opponentProfilePicType = self.itemsChat[indexPath.row].user.profilePicType
                            }
                            else{
                                messagesScreen.opponentProfilePic = self.createThumbnailOfVideoFromFileURL(self.itemsChat[indexPath.row].user.profilePicLink)!
                                messagesScreen.opponentProfilePicType = self.itemsChat[indexPath.row].user.profilePicType
                            }
                            
                            var myConversationTypeStr = String()
                            myConversationTypeStr = self.itemsChat[indexPath.row].lastMessage.myConversationType!
                            if (myConversationTypeStr == "withProfileChat")
                            {
                                messagesScreen.checkVisibiltyStr = "withProfileChat"
                            }
                            else{
                                messagesScreen.checkVisibiltyStr = "withAnonymousChat"
                            }
                            
                            messagesScreen.postID = self.itemsChat[indexPath.row].lastMessage.postID!
                            messagesScreen.opponentUserID = self.itemsChat[indexPath.row].user.id
                            messagesScreen.opponentFirebaseUserID = self.itemsChat[indexPath.row].user.firebase_user_id
                            var userType = String()
                            userType = self.itemsChat[indexPath.row].lastMessage.typeOfUser!
                            
                            if (userType == "AnonymousUser") || (userType == "WithProfileConfessionUser")
                            {
                                if (userType == "WithProfileConfessionUser")
                                {
                                    messagesScreen.typeOfUser = "AnonymousUser"
                                    messagesScreen.confessionTypeUser = "WithProfileConfessionUser"
                                }
                                else{
                                    messagesScreen.typeOfUser = "AnonymousUser"
                                    messagesScreen.confessionTypeUser = "AnonymousUser"
                                }
                            }
                            else
                            {
                                messagesScreen.typeOfUser = "WithProfileUser"
                            }
                            
                            self.navigationController?.pushViewController(messagesScreen, animated: true)
                        }
                    }
                })
            }
        }
    }

    //MARK:- ****** fetchUnseenMessagesCount ******
    func fetchUnseenMessagesCount(opponentId: String, userType: String,postId: String)-> String
    {
        var messageStr = String()
//         if let currentUserID = FIRAuth.auth()?.currentUser?.uid {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
           let postAndIdStr = opponentId + "@" + postId
            if userType == "AnonymousUser"
            {
                FIRDatabase.database().reference().child("UnseenChatCount").child(currentUserID).child(postAndIdStr).child("count").observe(.value, with:  { (snap) in
                        if snap.exists()
                        {
                            messageStr = snap.value as! String
                        }
                        else
                        {
                            messageStr = "0"
                        }
                 })
            }
            else
            {
                FIRDatabase.database().reference().child("UnseenChatCount").child(currentUserID).child(opponentId).child("count").observe(.value, with:  { (snap) in
                    if snap.exists()
                    {
                        messageStr = snap.value as! String
                    }
                    else
                    {
                        messageStr = "0"
                    }
                })
            }
        }
        return messageStr
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
//        switch self.itemsChat[indexPath.row].owner
//        {
//        case .receiver:
//            return false
//        case .sender:
            return true
//        }
    }
    
   /* public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            print("Delete clicked")
        }
        
        let block = UITableViewRowAction(style: .normal, title: "Block") { (action, indexPath) in
            // share item at indexPath
            print("Block clicked")
            
            let alert = UIAlertController(title: "", message: "Coming Soon.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        block.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        
        return [delete, block]
    }*/
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
            if editingStyle == .delete
            {
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .online(.wwan) , .online(.wiFi):
                    
                    var userType = String()
                    userType = self.itemsChat[indexPath.row].lastMessage.typeOfUser!
                    
                    if (userType == "AnonymousUser") || (userType == "WithProfileConfessionUser")
                    {
                        self.deleteParticularMessage_AnonymousUser(opponentUserID: self.itemsChat[indexPath.row].user.firebase_user_id, postIDStr: self.itemsChat[indexPath.row].lastMessage.postID!, completion: { (result) in
                            
                            if (result == true)
                            {
                                self.itemsChat.remove(at: indexPath.row)
                                self.chatListing_tableView.reloadData()
                            }
                        })
                    }
                    else
                    {
                        self.deleteParticularMessage(opponentUserID: self.itemsChat[indexPath.row].user.firebase_user_id, opponentProfileVisibility: self.itemsChat[indexPath.row].opponentIDWithVisibility, myProfileVisibility: self.itemsChat[indexPath.row].myIDWithVisibility, completion: { (result) in
                            
                            if (result == true)
                            {
                                self.itemsChat.remove(at: indexPath.row)
                                self.chatListing_tableView.reloadData()
                            }
                        })
                    }
                }
            }
    }

    
    // MARK: - ****** Generate Thumbnail from video Url. ******
    func createThumbnailOfVideoFromFileURL(_ strVideoURL: String) -> UIImage?{
        let asset = AVAsset(url: URL(string: strVideoURL)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), 100)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            /* error handling here */
            var thumbnailImage = UIImage()
            thumbnailImage = self.createThumbnailOfVideoFromFileURL(profilePicStr)!
            return thumbnailImage
        }
    }


    // MARK: - ****** Using Different Swipe Gestures. ******
    func respondToSwipeGesture(gesture: UIGestureRecognizer)
    {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer
        {
            switch swipeGesture.direction
            {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    // MARK: - ****** View Opponent user's profile. ******
    func SenderViewPhotoBtn(_ sender: UIButton!) {
        UserDefaults.standard.set(nil, forKey: "otherUsersDict")
        DispatchQueue.main.asyncAfter(deadline: .now() ) {
            let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
            messagesScreen.chatUserDifferentiateStr = "Messages"
            messagesScreen.mainConfessionCheck = "comingFromChatListing"
            messagesScreen.viewOpponentProfile = "fromListingScreen"
            messagesScreen.opponentName = self.itemsChat[sender.tag].user.name
            messagesScreen.opponentProfilePic = self.itemsChat[sender.tag].user.profilePic
            messagesScreen.opponentUserID = self.itemsChat[sender.tag].user.id
            messagesScreen.typeOfUser = "WithProfileUser"
            self.navigationController?.pushViewController(messagesScreen, animated: true)
        }
        
    }

    // MARK: - ****** Delete Particular Message By the Chat Owner. ******
    func deleteParticularMessage(opponentUserID : String, opponentProfileVisibility: String, myProfileVisibility: String, completion: @escaping (Bool) -> Swift.Void)
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            var getVisibilityOpponentStr = String()
            if (opponentProfileVisibility.range(of: "@") != nil)
            {
                let fullUserID = opponentProfileVisibility.components(separatedBy: "@")
                let AfterAtTheRateStr: String = fullUserID[1]
                getVisibilityOpponentStr = AfterAtTheRateStr
            }
            else
            {
                getVisibilityOpponentStr = "On"
            }
            
            
            var getVisibilityMyStr = String()
            if (myProfileVisibility.range(of: "@") != nil)
            {
                let fullUserID = myProfileVisibility.components(separatedBy: "@")
                let AfterAtTheRateStr: String = fullUserID[1]
                getVisibilityMyStr = AfterAtTheRateStr
            }
            else
            {
                getVisibilityMyStr = "On"
            }
            
            var visibilityStr = String()
            visibilityStr = opponentUserID + "@" + getVisibilityOpponentStr + "@" + getVisibilityMyStr
            
            var visibilityOppoStr = String()
            visibilityOppoStr = currentUserID + "@" + getVisibilityMyStr + "@" + getVisibilityOpponentStr
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(visibilityStr).setValue(nil)
            FIRDatabase.database().reference().child("Users").child(opponentUserID).child("conversations").child(visibilityOppoStr).setValue(nil)
            
            completion(true)
        }
        else{
            completion(false)
        }
    }
    
    // MARK: - ****** Delete Particular Message For Anonymous User By the Chat Owner. ******
    func deleteParticularMessage_AnonymousUser(opponentUserID : String,postIDStr: String, completion: @escaping (Bool) -> Swift.Void)
    {
        //        if let currentUserID = FIRAuth.auth()?.currentUser?.uid {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            let deleteOpponentMessageStr = opponentUserID + "@" + postIDStr
            let deleteMyMessageStr = currentUserID + "@" + postIDStr
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(deleteOpponentMessageStr).setValue(nil)
            FIRDatabase.database().reference().child("Users").child(opponentUserID).child("conversations_anonymous").child(deleteMyMessageStr).setValue(nil)
            
            completion(true)
            
        }
        else{
            completion(false)
        }
    }
    
    
    // MARK: - ****** Delete Message From Firebase conversations ******
    func deleteConversation(completion: @escaping (Bool) -> Swift.Void)
    {
        //        if let currentUserID = FIRAuth.auth()?.currentUser?.uid
        
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").observe(.childRemoved, with: { (snap) in
                if snap.exists()
                {
                    let receivedMessage = snap.value as! [String: Any]
                    
                    if ( receivedMessage["location"] != nil)
                    {
                        let locationOfConversation = receivedMessage["location"] as! String
                        
                        self.chatLocationToBeDeleted = locationOfConversation
                        FIRDatabase.database().reference().child("conversations").child(locationOfConversation).observe(.value, with: { (snap) in
                            if snap.exists()
                            {
                                FIRDatabase.database().reference().child("conversations").child(locationOfConversation).setValue(nil)
                                
                                completion(true)
                            }
                            else{
                                completion(false)
                            }
                        })
                    }else{
                        completion(false)
                    }
                   
                }
                else{
                    completion(false)
                }
            })
        }
    }
    
    
    // MARK: - ****** Delete Message From Firebase conversations For Anonymous User. ******
    func deleteConversation_AnonymousUser(completion: @escaping (Bool) -> Swift.Void)
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").observe(.childRemoved, with: { (snap) in
                
                if snap.exists()
                {
                    let receivedMessage = snap.value as! [String: Any]
                    
                    if (receivedMessage["location"] != nil)
                    {
                        let locationOfConversation = receivedMessage["location"] as! String
                        
                        self.chatLocationToBeDeleted = locationOfConversation
                        FIRDatabase.database().reference().child("conversations_anonymous").child(locationOfConversation).observe(.value, with: { (snap) in
                            
                            if snap.exists()
                            {
                                FIRDatabase.database().reference().child("conversations_anonymous").child(locationOfConversation).setValue(nil)
                                
                                completion(true)
                            }
                            else{
                                completion(false)
                            }
                            
                        })
                    }else{
                        completion(false)
                    }
                    
                }
                else{
                    completion(false)
                }
            })
        }
    }
   
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
