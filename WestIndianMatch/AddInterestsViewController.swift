//
//  AddInterestsViewController.swift
//  WestIndianMatch
//
//  Created by brst on 27/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import CoreLocation

class getSubInterests
{
    var id: String
    var interestsName: String
    
    init(id: String, name: String) {
        self.id = id
        self.interestsName = name
    }
}

class AddInterestsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,CLLocationManagerDelegate {

    
    // MARK: - ****** Variables ******
    
    var interestsDict = NSDictionary()
    var locationManager: CLLocationManager!
    var otherBool = Bool()
     var tField: UITextField!
    var emailFromRegistrationStr = String()
    var passwordFromRegistrationStr = String()
    var subCatBool = Bool()
    var interestsArrayBool = [Bool]()
    var interestsTitleArray = [String]()
    var interestsImagesArray = [String]()
    var mostPopularArray = [String]()
    var newMostPopularArray = [getSubInterests]()
    var saveInterestsArray = NSMutableArray()
    var selectedSection = Int()
    var selectedHeadingStr = String()
    var gamesArray = [String]()
    var religiousViewStr = String()
    var politicalViewStr = String()
    var professionStr = String()
    var sportsStr = String()
    var musicStr = String()
    var preferencesArray = [String]()
    var meetingArray = [String]()
    var preferenceInt = Int()
    var meetingInt = Int()
    var checkViewType = String()
    var religiousViewIndex = Int()
    var politicalViewIndex = Int()
    var professionStrIndex = Int()
    var MusicStrIndex = Int()
    var politicalBool = Bool()
    var religiousBool = Bool()
    var currentLatitude = Double()
    var currentLongitude = Double()
    var countryStr = String()
    
    var foodDrinkArray = NSMutableArray()
    var hobbiesArray = NSMutableArray()
    var professionArray = NSMutableArray()
    var sportsArray = NSMutableArray()
    var musicArray = NSMutableArray()
    var religiousEnteredStr = String()
    var politicalEnteredStr = String()
    var selectedInterestsArray = NSMutableArray()
    
    var isTapBool = Bool()
    
    
    
    // MARK: - ****** Outlets ******
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var backIcon: UIImageView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var interests_tableView: UITableView!
    @IBOutlet weak var loadingImage: UIImageView!
    
    
    // MARK: - ****** ViewDidLoad. ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isTapBool = false
        locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        subCatBool =  false
        otherBool =  false
        
        ApiHandler.getAllDefaultInterests { (responseData) in
            
            if (responseData.value(forKey: "status") as? String == "200")
            {
                if let dataDict = responseData.value(forKey: "data") as? NSDictionary
                {
                    print("dataDict = \(dataDict)")
//                    let keysArray = dataDict.allKeys as! [String]
                    self.interestsDict =  (responseData.value(forKey: "data") as? NSDictionary)!
                }
            }
        }
        
        interestsTitleArray = ["FOOD AND DRINK","HOBBIES","PROFESSION","SPORTS","MUSIC","RELIGIOUS VIEWS","POLITICAL VIEWS"]
        interestsImagesArray = ["Most Popular Icon","Music Icon","Other Icon","Travel Icon","Profession Icon","Hobbies Icon","Other Icon"]
        preferenceInt = 0
        meetingInt = 0
        preferencesArray = ["Looking For A Potential Relationship","Not Looking For A Relationship","Maybe Dinner & Conversation","Meet Up For Drinks","Meet Up For A Fete","Meet Up To Make A New Friendship"]
        meetingArray = ["Women","Men","Both","Interested in just socializing"]
        interestsArrayBool = [false,false,false,false,false,false,false,false,false,false]
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        self.politicalBool = false
        self.religiousBool = false
        
        if checkViewType == "fromRegisterScreen"
        {
            self.backIcon.isHidden = true
            self.backBtn.isHidden = true
            saveInterestsArray = []
           
            religiousViewIndex = 1400
            politicalViewIndex = 1400
            professionStrIndex = 1400
        }
        else if checkViewType == "fromProfileScreen"
        {
            self.backIcon.isHidden = false
            self.backBtn.isHidden = false
            
            for index in 0..<self.saveInterestsArray.count
            {
                var interestString = String()
                interestString = self.saveInterestsArray.object(at: index) as! String
                var textBefore = String()
                var textAfter = String()
                if interestString.contains("@")
                {
                    let fullNameArr = interestString.components(separatedBy: "@")
                    textBefore = fullNameArr[0]
                    textAfter = fullNameArr[1]
                    
                    if textAfter == "religiousView"
                    {
                        religiousViewStr = textBefore
                        religiousViewIndex = index
                        self.religiousBool = true
                    }
                    else if textAfter == "politicalView"
                    {
                        politicalViewStr = textBefore
                        politicalViewIndex = index
                        self.politicalBool = true
                    }
                    else if textAfter == "professionStr"
                    {
                        professionStr = interestString
                        professionStrIndex = index
                    }
                    else if textAfter == "SportsStr"
                    {
                        sportsStr = interestString
                    }
                    else if textAfter == "MusicStr"
                    {
                        musicStr = interestString
                    }
                }
                else
                {
                    if self.religiousBool == false
                    {
                        religiousViewIndex = 1400
                    }
                    
                    if self.politicalBool == false
                    {
                        politicalViewIndex = 1400
                    }

                    professionStrIndex = 1400
                }
            }
        }
    }
    
    //MARK:- ****** didChangeAuthorization ******
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        
        switch status {
            
        case .notDetermined , .denied , .restricted :
           
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
                }
            }
            
            break
            
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
      /*  default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
    }
    
    // MARK: - ****** didUpdateLocations ******
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.locationManager.location != nil
        {
            if let getCoordinates = manager.location?.coordinate
            {
                print("getCoordinates = \(getCoordinates)")
                let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.locationManager.stopUpdatingLocation()
                
                if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                    let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in // this is the last line that is being called
                        var placemark : CLPlacemark!
                        placemark = placemarks?[0]
                        if placemark != nil
                        {
                            var city = String()
                            if (placemark.addressDictionary?["City"] != nil)
                            {
                                city = (placemark.addressDictionary?["City"] as! String)
                            }
                            
                            var state = String()
                            if (placemark.addressDictionary?["State"] != nil)
                            {
                                state = (placemark.addressDictionary?["State"] as! String)
                            }
                            
                            self.countryStr = city + "," + state
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - ****** ViewWillAppear. ******
    override func viewWillAppear(_ animated: Bool)
    {
        NotificationCenter.default.addObserver(self, selector: #selector(AddInterestsViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
    }
    
    
    // MARK: - ****** Get Network Status Reachibility Method ******
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        let getNetworkStatus = (userInfo?["Status"] as? String)!
        if getNetworkStatus == "Offline"
        {

        }
    }

    // MARK: - ****** Back Button Action. ******
    @IBAction func backBtn_Action(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    // MARK: - ****** UITableView Delegates and DataSource Methods. ******
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return interestsTitleArray.count
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let sectionView =  UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        sectionView.backgroundColor = .clear
        sectionView.tag = 100+section
        
        let cellWrapperView = UIView(frame: CGRect(x: 0 , y: 0 , width: tableView.frame.size.width , height : 70))
        cellWrapperView.backgroundColor = UIColor.clear
        sectionView.addSubview(cellWrapperView)
        
        let backView = UIView(frame: CGRect(x: 10 , y: 10 , width: tableView.frame.size.width - 20 , height : 50))
        backView.backgroundColor = UIColor.white
        cellWrapperView.addSubview(backView)
        
        let titleImageView = UIImageView(frame: CGRect(x: 20 , y: 25 , width: 20, height : 22))
     
        let version = UILabel(frame: CGRect(x: 60 , y: 0 , width: 250, height : 70))
        version.font = version.font.withSize(16)
        
        var dropDownImageView = UIImageView()
        
        if UIScreen.main.bounds.size.width == 320
        {
            dropDownImageView = UIImageView(frame: CGRect(x: 280 , y: 28 , width: 20, height : 12))
        }
        else if UIScreen.main.bounds.size.width == 375
        {
            dropDownImageView = UIImageView(frame: CGRect(x: 320 , y: 28 , width: 20, height : 12))
        }
        else if UIScreen.main.bounds.size.width == 414
        {
            dropDownImageView = UIImageView(frame: CGRect(x: 370 , y: 28 , width: 20, height : 12))
        }
        else{
            dropDownImageView = UIImageView(frame: CGRect(x: 370 , y: 28 , width: 20, height : 12))
        }
        
        dropDownImageView.image = UIImage(named: "DropDown Icon")
        cellWrapperView.addSubview(dropDownImageView)
        
        switch (section) {
        case 0:
            version.text = "FOOD AND DRINK";
            titleImageView.image = UIImage(named: "Food Drink Icon")
            dropDownImageView.isHidden = false
        case 1:
            version.text = "HOBBIES";
            titleImageView.image = UIImage(named: "Hobbies Icon")
            dropDownImageView.isHidden = false
        case 2:
            version.text = "PROFESSION";
            titleImageView.image = UIImage(named: "Profession Icon")
            dropDownImageView.isHidden = false
        case 3:
            version.text = "SPORTS";
            titleImageView.image = UIImage(named: "Sports Icon")
            dropDownImageView.isHidden = false
        case 4:
            version.text = "MUSIC";
            titleImageView.image = UIImage(named: "Music Icon")
            dropDownImageView.isHidden = false
     
        case 5:
            version.text = "RELIGIOUS VIEWS";
            titleImageView.image = UIImage(named: "Religious Icon")
            dropDownImageView.isHidden = true
        default:
            version.text = "POLITICAL VIEWS";
            titleImageView.image = UIImage(named: "Political Icon")
            dropDownImageView.isHidden = true
        }
        
        titleImageView.contentMode = UIViewContentMode.scaleAspectFit
        cellWrapperView.addSubview(titleImageView)
        version.textColor = UIColor.darkGray
        version.textAlignment = .left;
        cellWrapperView.addSubview(version)
    
        let  tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        sectionView.addGestureRecognizer(tap)
        
        return sectionView
    }
    
    

    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 70
    }

    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == selectedSection-100 {
            return newMostPopularArray.count
        }
        return 0
    }
    
    
    
    
    // MARK: - ****** Handles Tap Gesture On Click Of Particular Section. ******
    func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
        selectedSection = (gestureRecognizer.view?.tag)! as Int
        if selectedSection == 100
        {
            if (self.isTapBool == false)
            {
                self.isTapBool = true
                
                self.selectedHeadingStr = "Userfooddrinks"
                
                self.newMostPopularArray.removeAll()
                
                if let foodDictArray = self.interestsDict.value(forKey: "food_drinks") as? NSArray
                {
                    for index in 0..<foodDictArray.count
                    {
                        let foodDict = foodDictArray[index] as! NSDictionary
                        
                        let idStr = foodDict.value(forKey: "id") as! String
                        let name = foodDict.value(forKey: "name") as! String
                        
                        let foodDetails = getSubInterests.init(id: idStr, name: name)
                        self.newMostPopularArray.append(foodDetails)
                    }
                }
                else
                {
                    mostPopularArray = ["Roti & Doubles","Anything Curry","Anything Jerk","Veagan","Seamoss Punch","Peanut Punch","Fruit Punch","Rum & Coke","Beer","Scotch & Coconut Water","Scotch & Energy Drink","Vodka & Cranberry","Vodka & Orange Juice","Anything with Liquor"]
                }
            }
            else
            {
                self.isTapBool = false
                self.newMostPopularArray.removeAll()
            }
        }
        else if selectedSection == 101
        {
            if (self.isTapBool == false)
            {
                self.isTapBool = true
                
                self.selectedHeadingStr = "Userhobbies"
                
                if let hobbiesArray = self.interestsDict.value(forKey: "hobbies") as? NSArray
                {
                    self.newMostPopularArray.removeAll()
                    
                    for index in 0..<hobbiesArray.count
                    {
                        let hobbiesDict = hobbiesArray[index] as! NSDictionary
                        
                        let idStr = hobbiesDict.value(forKey: "id") as! String
                        let name = hobbiesDict.value(forKey: "name") as! String
                        
                        let hobbiesDictDetails = getSubInterests.init(id: idStr, name: name)
                        self.newMostPopularArray.append(hobbiesDictDetails)
                    }
                }
                else
                {
                    mostPopularArray = ["Fetein/Wining/Whining","Reading"," Bar Hopping","Cinema","Island Hopping","Carnival Chaser","Homebody","Gym","Soca or Dancehall Aerobics","Smoking Hookah","Cooking","Singing","Dancing","420 Friendly"]
                }
            }
            else
            {
                self.isTapBool = false
                self.newMostPopularArray.removeAll()
            }
        }
        else if selectedSection == 102
        {
            if (self.isTapBool == false)
            {
                self.isTapBool = true
                
                self.selectedHeadingStr = "Userprofession"
                
                var valueof = String()
                if professionStr == ""
                {
                    valueof = "Other"
                }
                else
                {
                    valueof = professionStr
                }
                
                if let professionArray = self.interestsDict.value(forKey: "profession") as? NSArray
                {
                    self.newMostPopularArray.removeAll()
                    
                    for index in 0..<professionArray.count
                    {
                        let professionDict = professionArray[index] as! NSDictionary
                        
                        let idStr = professionDict.value(forKey: "id") as! String
                        let name = professionDict.value(forKey: "name") as! String
                        
                        let professionDictDetails = getSubInterests.init(id: idStr, name: name)
                        self.newMostPopularArray.append(professionDictDetails)
                    }
                }
                else
                {
                    mostPopularArray = ["Lawyer","Manager","Construction","Handy Man (Plumber, Electrician etc)","Doctor","DJ","Party Promoter","Barber","Hair Dresser","Taxi Driver","Entrepeneur","Engineering","Nurse","Self Employed","Unemployed (But Looking)","Clerical (Secretary Etc)","Airline Industry","Automotive","Store Clerk","Chef","Student",valueof]
                }
            }
            else
            {
                self.isTapBool = false
                self.newMostPopularArray.removeAll()
            }
        }
        else if selectedSection == 103
        {
            if (self.isTapBool == false)
            {
                self.isTapBool = true
                
                self.selectedHeadingStr = "Usersports"
                
                var valueof = String()
                if sportsStr == ""
                {
                    valueof = "Other"
                }
                else
                {
                    valueof = sportsStr
                }
                
                if let sportsArray = self.interestsDict.value(forKey: "sports") as? NSArray
                {
                    self.newMostPopularArray.removeAll()
                    
                    for index in 0..<sportsArray.count
                    {
                        let sportsDict = sportsArray[index] as! NSDictionary
                        
                        let idStr = sportsDict.value(forKey: "id") as! String
                        let name = sportsDict.value(forKey: "name") as! String
                        
                        let sportsDictDetails = getSubInterests.init(id: idStr, name: name)
                        self.newMostPopularArray.append(sportsDictDetails)
                    }
                }
                else{
                    mostPopularArray = ["Football (Soccer)","American Football","Basketball","Cricket","Volleyball","Track 7 Field","Swimming","Baseball","Don’t Like Sports",valueof]
                }
            }
            else
            {
                self.isTapBool = false
                self.newMostPopularArray.removeAll()
            }
            
        }
        else if selectedSection == 104
        {
            if (self.isTapBool == false)
            {
                self.isTapBool = true
                
                self.selectedHeadingStr = "Usermusic"
                
                var valueof = String()
                if musicStr == ""
                {
                    valueof = "Other"
                }
                else
                {
                    valueof = musicStr
                }
                
                if let musicArray = self.interestsDict.value(forKey: "music") as? NSArray
                {
                    self.newMostPopularArray.removeAll()
                    
                    for index in 0..<musicArray.count
                    {
                        let musicDict = musicArray[index] as! NSDictionary
                        
                        let idStr = musicDict.value(forKey: "id") as! String
                        let name = musicDict.value(forKey: "name") as! String
                        
                        let musicDictDetails = getSubInterests.init(id: idStr, name: name)
                        self.newMostPopularArray.append(musicDictDetails)
                    }
                }
                else
                {
                    mostPopularArray = ["Soca","Dancehall","Reggae","Edm","Afrobeats","Hip Hop","Soul","R&B","Gospel",valueof]
                }
            }
            else
            {
                self.isTapBool = false
                self.newMostPopularArray.removeAll()
            }
        }
        else if selectedSection == 105
        {
            mostPopularArray = []
            self.newMostPopularArray.removeAll()
            
            self.selectedHeadingStr = "religiousreviews"
            
            let alert = UIAlertController(title: "Enter Religious Views", message: "", preferredStyle: .alert)
        
            alert.addTextField(configurationHandler: configurationTextField)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:handleCancel))
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler:{ (UIAlertAction) in
                self.otherBool =  true
            
                if self.tField.text == ""
                {
                    self.religiousViewStr = ""
                }
                else
                {
                    var textMessage = String()
                    textMessage = self.tField.text!
                    if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false)
                    {
                        self.religiousViewStr = self.tField.text!
                    }
                    else
                    {
                        self.religiousViewStr = ""
                    }
                }
            }))
            self.present(alert, animated: true, completion: {
                
            })
        }
        else
        {
            mostPopularArray = []
            self.newMostPopularArray.removeAll()
            
            self.selectedHeadingStr = "politicalreviews"
            
            let alert = UIAlertController(title: "Enter Political Views", message: "", preferredStyle: .alert)
            
            alert.addTextField(configurationHandler: configurationTextField)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:handleCancel))
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler:{ (UIAlertAction) in
                
                self.otherBool =  true
               
                if self.tField.text == ""
                {
                    self.politicalViewStr = ""
                }
                else
                {
                    var textMessage = String()
                    textMessage = self.tField.text!
                    if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false)
                    {
                        self.politicalViewStr = self.tField.text!
                    }
                    else
                    {
                        self.politicalViewStr = ""
                    }
                }
            }))
            self.present(alert, animated: true, completion: {
                
            })
        }
        self.interests_tableView.reloadData()
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let interestsCell = tableView.dequeueReusableCell(withIdentifier: "interestsCell", for: indexPath)as! AddInterestsTableViewCell
        
        self.interests_tableView.backgroundColor = UIColor.clear
        self.interests_tableView.separatorColor = UIColor.clear
        
        interestsCell.subInterests_wwrapperView.layer.borderWidth = 1
        interestsCell.subInterests_wwrapperView.layer.borderColor = UIColor.darkGray.cgColor
        
        if (newMostPopularArray.count > 0)
        {
            var interestString = String()
            interestString = newMostPopularArray[indexPath.row].interestsName
            
//            var interestID = String()
//            interestID = newMostPopularArray[indexPath.row].id
            interestsCell.interests_titleLbl.text = interestString
            
            if saveInterestsArray.contains(interestString)
            {
                var textBefore = String()
                var textAfter = String()
                if interestString.contains("@")
                {
                    let fullNameArr = interestString.components(separatedBy: "@")
                    textBefore = fullNameArr[0]
                    textAfter = fullNameArr[1]
                    interestsCell.interests_titleLbl.text = "Other"
                }
                
                interestsCell.dropDownImage.isHidden = false
                interestsCell.subInterests_wwrapperView.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            }
            else
            {
                var textBefore = String()
                var textAfter = String()
                if interestString.contains("@")
                {
                    let fullNameArr = interestString.components(separatedBy: "@")
                    textBefore = fullNameArr[0]
                    textAfter = fullNameArr[1]
                    interestsCell.interests_titleLbl.text = "Other"
                }
                else
                {
                    interestsCell.interests_titleLbl.text = interestString
                }
                
                interestsCell.dropDownImage.isHidden = true
                interestsCell.subInterests_wwrapperView.backgroundColor = UIColor.init(red: 241.0/255, green: 241.0/255, blue: 241.0/255, alpha: 1.0)
            }
        }
        else
        {
            var interestString = String()
            interestString = mostPopularArray[indexPath.row]
            interestsCell.interests_titleLbl.text = interestString
            if saveInterestsArray.contains(interestString)
            {
                var textBefore = String()
                var textAfter = String()
                
                if interestString.contains("@")
                {
                    let fullNameArr = interestString.components(separatedBy: "@")
                    textBefore = fullNameArr[0]
                    textAfter = fullNameArr[1]
                    print("textBefore = \(textBefore)")
                    print("textAfter = \(textAfter)")
                    interestsCell.interests_titleLbl.text = "Other"
                }
                
                interestsCell.dropDownImage.isHidden = false
                interestsCell.subInterests_wwrapperView.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            }
            else
            {
                var textBefore = String()
                var textAfter = String()
                if interestString.contains("@")
                {
                    let fullNameArr = interestString.components(separatedBy: "@")
                    textBefore = fullNameArr[0]
                    textAfter = fullNameArr[1]
                    print("textBefore = \(textBefore)")
                    print("textAfter = \(textAfter)")
                    interestsCell.interests_titleLbl.text = "Other"
                }
                else
                {
                    interestsCell.interests_titleLbl.text = interestString
                }
                
                interestsCell.dropDownImage.isHidden = true
                interestsCell.subInterests_wwrapperView.backgroundColor = UIColor.init(red: 241.0/255, green: 241.0/255, blue: 241.0/255, alpha: 1.0)
            }
        }
        
        interestsCell.selectionStyle = UITableViewCellSelectionStyle.none
        return interestsCell
    }
    
    
    
     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
     {
        if selectedSection == indexPath.section + 100
        {
            if self.selectedSection == 102 || self.selectedSection == 103 || self.selectedSection == 104
            {
                if (self.newMostPopularArray.count == (indexPath.row+1)) {
                    
                    let alert = UIAlertController(title: "Enter Input", message: "", preferredStyle: .alert)
                    alert.addTextField(configurationHandler: configurationTextField)
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:handleCancel))
                    alert.addAction(UIAlertAction(title: "Done", style: .default, handler:{ (UIAlertAction) in
                        
                        self.otherBool =  true
                        
                        if self.selectedSection == 102
                        {
                            if self.tField.text == ""
                            {
                                self.professionStr = "textdeleted"
                            }
                            else{
                                self.professionStr = self.tField.text!
                            }
                        }
                        else if self.selectedSection == 103
                        {
                            if self.tField.text == ""
                            {
                                self.sportsStr = "textdeleted"
                            }
                            else{
                                  self.sportsStr = self.tField.text!
                            }
                          
                        }
                        else if self.selectedSection == 104
                        {
                            if self.tField.text == ""
                            {
                                self.musicStr = "textdeleted"
                            }
                            else
                            {
                               self.musicStr = self.tField.text!
                            }
                        }
                        
                        if indexPath.section + 100 == 102 {
                            var valueProfession = String()
                            if (self.tField.text! == "")  || (self.tField.text == "textdeleted")
                            {
                                valueProfession = "Other"
                            }
                            else
                            {
                                valueProfession = self.tField.text!
//                                valueProfession = valueProfession + "@" + "professionStr"

                              self.mostPopularArray = ["Lawyer","Manager","Construction","Handy Man (Plumber, Electrician etc)","Doctor","DJ","Party Promoter","Barber","Hair Dresser","Taxi Driver","Entrepeneur","Engineering","Nurse","Self Employed","Unemployed (But Looking)","Clerical (Secretary Etc)","Airline Industry","Automotive","Store Clerk","Chef","Student",valueProfession]
                                
                                self.professionArray.add(valueProfession)
                            }
                        }
                        else if indexPath.section + 100 == 103
                        {
                            var valueSports = String()
                            if (self.tField.text! == "") || (self.tField.text == "textdeleted") {
                                valueSports = "Other"
                            }
                            else
                            {
                                valueSports = self.tField.text!
//                                valueSports = valueSports + "@" + "SportsStr"
                            }
                            
                            self.mostPopularArray = ["Football (Soccer)","American Football","Basketball","Cricket","Volleyball","Track 7 Field","Swimming","Baseball","Don’t Like Sports",valueSports]
                            
                             self.sportsArray.add(valueSports)
                        }
                        else if indexPath.section + 100 == 104
                        {
                            var valueMusic = String()
                            if (self.tField.text! == "") || (self.tField.text == "textdeleted") {
                                valueMusic = "Other"
                            }
                            else
                            {
                                valueMusic = self.tField.text!
//                                valueMusic = valueMusic + "@" + "MusicStr"
                            }
                            
                            self.mostPopularArray = ["Soca","Dancehall","Reggae","Edm","Afrobeats","Hip Hop","Soul","R&B","Gospel",valueMusic]
                            
                            self.musicArray.add(valueMusic)
                        }
                        
                        self.interests_tableView.reloadData()
                        
                        if !(self.saveInterestsArray.contains(self.newMostPopularArray[indexPath.row]))
                        {
                            self.saveInterestsArray.add(self.newMostPopularArray[indexPath.row].interestsName)
                       
                        }
                        else
                        {
                            self.saveInterestsArray.remove(self.newMostPopularArray[indexPath.row].interestsName)
                        }
                        
                        self.interests_tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                    }))
                    
                    self.present(alert, animated: true, completion: {
                        
                    })
                    
                } //ends for checking last row in a section
                // Rows other than last row
                else
                {
                    if (newMostPopularArray.count > 0)
                    {
                        if !(saveInterestsArray.contains(newMostPopularArray[indexPath.row].interestsName))
                        {
                            saveInterestsArray.add(newMostPopularArray[indexPath.row].interestsName)
                            
                            if (self.selectedHeadingStr == "Userfooddrinks")
                            {
                                if !(self.foodDrinkArray.contains(newMostPopularArray[indexPath.row].id))
                                {
                                    self.foodDrinkArray.add(newMostPopularArray[indexPath.row].id)
                                }
                            }
                            else if (self.selectedHeadingStr == "Userhobbies")
                            {
                                if !(self.hobbiesArray.contains(newMostPopularArray[indexPath.row].id))
                                {
                                    self.hobbiesArray.add(newMostPopularArray[indexPath.row].id)
                                }
                            }
                            else if (self.selectedHeadingStr == "Userprofession")
                            {
                                if !(self.professionArray.contains(newMostPopularArray[indexPath.row].id))
                                {
                                    self.professionArray.add(newMostPopularArray[indexPath.row].id)
                                }
                            }
                            else if (self.selectedHeadingStr == "Usersports")
                            {
                                if !(self.sportsArray.contains(newMostPopularArray[indexPath.row].id))
                                {
                                    self.sportsArray.add(newMostPopularArray[indexPath.row].id)
                                }
                            }
                            else if (self.selectedHeadingStr == "Usermusic")
                            {
                                if !(self.musicArray.contains(newMostPopularArray[indexPath.row].id))
                                {
                                    self.musicArray.add(newMostPopularArray[indexPath.row].id)
                                }
                            }
                        }
                        else
                        {
                            saveInterestsArray.remove(newMostPopularArray[indexPath.row].interestsName)
                            
                            if (self.selectedHeadingStr == "Userfooddrinks")
                            {
                                self.foodDrinkArray.remove(newMostPopularArray[indexPath.row].id)
                            }
                            else if (self.selectedHeadingStr == "Userhobbies")
                            {
                                self.hobbiesArray.remove(newMostPopularArray[indexPath.row].id)
                            }
                            else if (self.selectedHeadingStr == "Userprofession")
                            {
                                self.professionArray.remove(newMostPopularArray[indexPath.row].id)
                            }
                            else if (self.selectedHeadingStr == "Usersports")
                            {
                                self.sportsArray.remove(newMostPopularArray[indexPath.row].id)
                            }
                            else if (self.selectedHeadingStr == "Usermusic")
                            {
                                self.musicArray.remove(newMostPopularArray[indexPath.row].id)
                            }
                        }
                    }
                    else
                    {
                        if !(saveInterestsArray.contains(mostPopularArray[indexPath.row]))
                        {
                            saveInterestsArray.add(mostPopularArray[indexPath.row])
                        }
                        else
                        {
                            saveInterestsArray.remove(mostPopularArray[indexPath.row])
                        }
                    }
                    
                    interests_tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                }
            }
            else
            {
                if (newMostPopularArray.count > 0)
                {
                    if !(saveInterestsArray.contains(newMostPopularArray[indexPath.row].interestsName))
                    {
                        saveInterestsArray.add(newMostPopularArray[indexPath.row].interestsName)
                        
                        if (self.selectedHeadingStr == "Userfooddrinks")
                        {
                            if !(self.foodDrinkArray.contains(newMostPopularArray[indexPath.row].id))
                            {
                                self.foodDrinkArray.add(newMostPopularArray[indexPath.row].id)
                            }
                        }
                        else if (self.selectedHeadingStr == "Userhobbies")
                        {
                            if !(self.hobbiesArray.contains(newMostPopularArray[indexPath.row].id))
                            {
                                self.hobbiesArray.add(newMostPopularArray[indexPath.row].id)
                            }
                        }
                        else if (self.selectedHeadingStr == "Userprofession")
                        {
                            if !(self.professionArray.contains(newMostPopularArray[indexPath.row].id))
                            {
                                self.professionArray.add(newMostPopularArray[indexPath.row].id)
                            }
                        }
                        else if (self.selectedHeadingStr == "Usersports")
                        {
                            if !(self.sportsArray.contains(newMostPopularArray[indexPath.row].id))
                            {
                                self.sportsArray.add(newMostPopularArray[indexPath.row].id)
                            }
                        }
                        else if (self.selectedHeadingStr == "Usermusic")
                        {
                            if !(self.musicArray.contains(newMostPopularArray[indexPath.row].id))
                            {
                                self.musicArray.add(newMostPopularArray[indexPath.row].id)
                            }
                        }
                    }
                    else
                    {
                        saveInterestsArray.remove(newMostPopularArray[indexPath.row].interestsName)
                        
                        if (self.selectedHeadingStr == "Userfooddrinks")
                        {
                           self.foodDrinkArray.remove(newMostPopularArray[indexPath.row].id)
                        }
                        else if (self.selectedHeadingStr == "Userhobbies")
                        {
                           self.hobbiesArray.remove(newMostPopularArray[indexPath.row].id)
                        }
                        else if (self.selectedHeadingStr == "Userprofession")
                        {
                           self.professionArray.remove(newMostPopularArray[indexPath.row].id)
                        }
                        else if (self.selectedHeadingStr == "Usersports")
                        {
                           self.sportsArray.remove(newMostPopularArray[indexPath.row].id)
                        }
                        else if (self.selectedHeadingStr == "Usermusic")
                        {
                            self.musicArray.remove(newMostPopularArray[indexPath.row].id)
                        }
                    }
                }
                else
                {
                    if !(saveInterestsArray.contains(mostPopularArray[indexPath.row + 1]))
                    {
                        saveInterestsArray.add(mostPopularArray[indexPath.row + 1])
                    }
                    else
                    {
                        saveInterestsArray.remove(mostPopularArray[indexPath.row + 1])
                    }
                }
                
                
                interests_tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            }
        }
     }

    
    // MARK: - ****** Enter Text Values in Alert TextField's. ******
    func configurationTextField(textField: UITextField!)
    {
        if selectedSection == 105
        {
            if religiousViewStr == ""
            {
                 textField.placeholder = "Enter an item"
                 religiousViewStr = textField.text!
                 tField = textField
            }
            else
            {
                 textField.text = religiousViewStr
                tField = textField
            }
        }
        else if selectedSection == 106
        {
            if politicalViewStr == ""
            {
                textField.placeholder = "Enter an item"
                politicalViewStr = textField.text!
                tField = textField
            }
            else
            {
                textField.text = politicalViewStr
                tField = textField
            }
        }
        else if selectedSection == 102
        {
            if professionStr == ""
            {
                textField.placeholder = "Enter an item"
                professionStr = textField.text!
                tField = textField
            }
            else
            {
                
                var textBefore = String()
                var textAfter = String()
                if professionStr.contains("@")
                {
                    let fullNameArr = professionStr.components(separatedBy: "@")
                    textBefore = fullNameArr[0]
                    textAfter = fullNameArr[1]
                    
                    textField.text = textBefore
                }
                else{
                    textField.text = professionStr
                }
                
                
                tField = textField
            }
        }
        else if selectedSection == 103
        {
            if sportsStr == ""
            {
                textField.placeholder = "Enter an item"
                sportsStr = textField.text!
                tField = textField
            }
            else
            {
                var textBefore = String()
                var textAfter = String()
                if sportsStr.contains("@")
                {
                    let fullNameArr = sportsStr.components(separatedBy: "@")
                    textBefore = fullNameArr[0]
                    textAfter = fullNameArr[1]
                    
                    textField.text = textBefore
                }
                else{
                   textField.text = sportsStr
                }
                
                tField = textField
            }
        }
        else if selectedSection == 104
        {
            if musicStr == ""
            {
                textField.placeholder = "Enter an item"
                musicStr = textField.text!
                tField = textField
            }
            else
            {
                var textBefore = String()
                var textAfter = String()
                if musicStr.contains("@")
                {
                    let fullNameArr = musicStr.components(separatedBy: "@")
                    textBefore = fullNameArr[0]
                    textAfter = fullNameArr[1]
                    textField.text = textBefore
                }
                else{
                   textField.text = musicStr
                }
                
                tField = textField
            }
        }
    }
    
    
    // MARK: - ****** Cancel Alert View. ******
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    
    //MARK:- ****** interestsSelection_btn  ******
    @IBAction func interestsSelection_btn(_ sender: Any) {

    }
    
    //MARK:- ****** subCategory_btnAction  ******
    @IBAction func subCategory_btnAction(_ sender: UIButton) {
        let sen: UIButton = sender
        let g : NSIndexPath = NSIndexPath(row: sen.tag, section: 0)
        let t : AddInterestsTableViewCell = self.interests_tableView.cellForRow(at: g as IndexPath) as! AddInterestsTableViewCell
        
        if subCatBool == false
        {
            t.subInterests_wwrapperView.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            subCatBool = true
        }
        else
        {
             t.subInterests_wwrapperView.backgroundColor = UIColor.init(red: 241.0/255, green: 241.0/255, blue: 241.0/255, alpha: 1.0)
            subCatBool = false
        }
    }
    
    
    // MARK: - ****** Save Interests Button Action. ******
    @IBAction func saveInterests_btnAction(_ sender: Any) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if self.religiousViewStr != ""
            {
                var religiousViewWithType = String()
                religiousViewWithType = religiousViewStr + "@" + "religiousView"
                if religiousViewIndex == 1400
                {
                    saveInterestsArray.add(religiousViewWithType)
                }
                else if self.religiousViewStr == "textdeleted"{
                    saveInterestsArray.removeObject(at: religiousViewIndex)
                }
                else{
                    saveInterestsArray.replaceObject(at: religiousViewIndex, with: religiousViewWithType)
                }
            }
            
            if politicalViewStr != ""
            {
                var politicalViewString = String()
                politicalViewString = politicalViewStr + "@" + "politicalView"
                
                if politicalViewIndex == 1400
                {
                    saveInterestsArray.add(politicalViewString)
                }
                else if self.politicalViewStr == "textdeleted"{
                    saveInterestsArray.removeObject(at: politicalViewIndex)
                }
                else{
                    saveInterestsArray.replaceObject(at: politicalViewIndex, with: politicalViewString)
                }
            }
            
            
            if self.saveInterestsArray.count == 0{
                let alert = UIAlertController(title: "", message:  "Please select interests for your profile.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)

            }
            else if (self.saveInterestsArray.count <  5)
            {
                let alert = UIAlertController(title: "", message:  "Please select maximum 5 interests for your profile", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)

            }
            else
            {
                self.view.isUserInteractionEnabled = false
                self.loadingView.isHidden = false

                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                    {
                        self.loadingView.isHidden = false
                        self.view.isUserInteractionEnabled = false
                        
                        ApiHandler.updateUserInterests(userID: userID, authToken: userToken, drinksArray: self.foodDrinkArray, hobbiesArray: self.hobbiesArray, professionArray: self.professionArray, sportsArray: self.sportsArray, musicArray: self.musicArray, religiousStr: self.religiousViewStr, politicalStr: politicalViewStr, completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                
                                if self.checkViewType == "fromRegisterScreen"
                                {
                                    if ((UserDefaults.standard.value(forKey: "userType") == nil)  || (UserDefaults.standard.value(forKey: "userType") is NSNull) || (UserDefaults.standard.value(forKey: "userType") as! String == ""))
                                    {
                                        print("usertype is nsnull")
                                    }
                                    else
                                    {
                                        if (UserDefaults.standard.value(forKey: "userType") as! String == "facebookUser")
                                        {
                                            let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                            UserDefaults.standard.set("success", forKey: "userRegistered")
                                            UserDefaults.standard.set("facebookUser", forKey: "userType")
                                            UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                                            
                                            self.navigationController?.pushViewController(homeScreen, animated: true)
                                        }
                                        else if (UserDefaults.standard.value(forKey: "userType") as! String == "emailUser")
                                        {
                                            let alert = UIAlertController(title: "", message: "An email activation link has been sent on your email. Please verify your email.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                                                
                                                let loginScreen = self.storyboard!.instantiateViewController(withIdentifier: "LoginWithEmailViewController") as! LoginWithEmailViewController
                                                loginScreen.registerStr = "throughAddInterests"
                                                loginScreen.emailStr = self.emailFromRegistrationStr
                                                loginScreen.passwordStr = self.passwordFromRegistrationStr
                                                UserDefaults.standard.set("emailUser", forKey: "userType")
                                                UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                                                self.navigationController?.pushViewController(loginScreen, animated: true)
                                                
                                            }))
                                            
                                            self.present(alert, animated: true, completion: nil)
                                          
                                        }
                                        else if (UserDefaults.standard.value(forKey: "userType") as! String == "instagramUser")
                                        {
                                            let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                            UserDefaults.standard.set("success", forKey: "userRegistered")
                                            UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                                            UserDefaults.standard.set("instagramUser", forKey: "userType")
                                            
                                            self.navigationController?.pushViewController(homeScreen, animated: true)
                                        }
                                    }
                                }
                                else if (self.checkViewType == "fromProfileScreen")
                                {
                                    let alert = UIAlertController(title: "", message: "Interests updated successfully.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                                        
                                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdateInterests"), object: nil)
                                        
                                        self.navigationController!.popViewController(animated: true)
                                        
                                    }))
                                    
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        })
                    }
                }
            }
        }
    }
    
    
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
