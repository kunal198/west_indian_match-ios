//
//  GroupChatMeassagesViewController.swift
//  Wi Match
//
//  Created by brst on 05/05/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import AVKit
import Photos
import Firebase
import FirebaseMessaging
import AVFoundation
import SDWebImage

class GroupChatMeassagesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,AVAudioRecorderDelegate,AVAudioPlayerDelegate, UIScrollViewDelegate {
    
    //MARK:- ***** VARIABLES DECLARATION *****
    
    var myProfilePicLink = String()
    
    var isTextEditingBegins = Bool()
    
    var statusOfContinueStr = Bool()
    var onlineUsersArray = NSMutableArray()
    var onlineUsersCopyArray = NSMutableArray()
    var opponentUserID = String()
    var opponentName = String()
    var opponentImage = String()
    var items = [Message]()
    var currentUser: User?
    var opponentProfilePic = UIImage()
    var viewOpponentProfile = String()
    var readMsgBool = Bool()
    var usersDetailsDict = NSMutableDictionary()
    var blockedUsersArray = NSMutableArray()
    var blockedByUserArray = NSMutableArray()
    var blockedIdStr = String()
    var usersInChatRoomArray = NSMutableArray()
    var userLeftBool = Bool()
    var opponentWiChatRoomVisibilityStr = String()
    var myProfileVisibilityStr = String()
    var showAlertOnce = Bool()
    var reportedUsersByMeArray = NSMutableArray()
    var  userWhoReportedMeArray = NSMutableArray()
    
    var badgeCount = Int()
    
    var TotalPages = Int()
    var currentPageOnlineUsers = Int()
    
    var onlineBtnClick = Bool()
    var openingPhotoLibrary = Bool()

    var wiChatRoomVisibilityStr = String()
    
    var clickedIndex = Int()
    
    var blockedByMeStatus = String()
    var blockedByOpponentStatus = String()
    var reportedByMeStatus = String()
    var reportedByMe_ContinueChat = String()
    
    var viewProfileBool = Bool()
    
    // Variables for Audio recording
    var recordingSession : AVAudioSession!
    var audioRecorder    :AVAudioRecorder!
    var settings = [String : Int]()
    
    //Variables for audio playing
    var audioPlayer : AVAudioPlayer!
    var audioRecordedUrl = NSURL()
    var loadMoreMessagesBool = Bool()
    
    var myProfileVisibilityStatus = String()
    var opponentProfileVisibilityStatus = String()
    
    var refreshControl: UIRefreshControl!
    var getLastKeyForPaging = String()
    var getNodeOnceBool = Bool()
    var fetchingMessagesWithPaging = Bool()
    var loadingFirstTime = Bool()

    var isChatScrolled = Bool()
    var pageCount = Int64()
    
    var totalMessagesInt = Int()
    var getLastKeyValue = String()
    
    var getArrayOfLastKeys = [String]()
    var appDelegatePush = String()

    var getLastNodeOfMessage = String()
    
    var lastMessageStr = String()
    var lastTimeStamp = Double()
    
    //MARK:- ***** OUTLETS DECLARATION *****
    
    @IBOutlet weak var wrapperTitleView: UIView!
    @IBOutlet weak var AudioUpdate_Lbl: UILabel!
    @IBOutlet weak var sendAudioBtn: UIButton!
    @IBOutlet weak var startAudioBtn: UIButton!
    @IBOutlet weak var finishAudioBtn: UIButton!
    @IBOutlet weak var playAudioBtn: UIButton!
    @IBOutlet weak var noMeesagesFoundView: UIView!
    @IBOutlet weak var audioRecordingView: UIView!
    @IBOutlet weak var notifyUsersEnter_lbl: UILabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var noOnlineUsers_lbl: UILabel!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var particularImage: UIImageView!
    @IBOutlet weak var viewImageVIew: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var selectedImageView: UIView!
    @IBOutlet weak var onlineUsers_tableView: UITableView!
    @IBOutlet weak var onlineUsersView: UIView!
    @IBOutlet weak var OptionView: UIView!
    @IBOutlet weak var message_txtView: UITextView!
    @IBOutlet weak var SendMessageView: UIView!
    @IBOutlet weak var ChatView: UIView!
    @IBOutlet weak var groupChat_tableView: UITableView!
    @IBOutlet weak var loadingImage: UIImageView!
    
    
    // MARK: - ****** ViewDidLoad. ******
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTap(_:)))
//        tap.numberOfTapsRequired = 1
//        self.groupChat_tableView.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshGroupChatAgain), name: NSNotification.Name(rawValue: "refreshGroupChat"), object: nil)
        
        openingPhotoLibrary = false
        self.fetchingMessagesWithPaging = false
        
        self.isTextEditingBegins = false
        
        self.viewProfileBool = false
        self.isChatScrolled = false
        
        self.loadMoreMessagesBool = false
        self.loadingFirstTime = true
        self.pageCount = 20
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = UIColor.clear
        self.refreshControl.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.refreshControl.addTarget(self, action: #selector(refreshGroupChat), for: .valueChanged)
//        self.groupChat_tableView.addSubview(refreshControl)
        
        UserDefaults.standard.set("", forKey: "otherUsersDict")
        self.myProfileVisibilityStr = "On"
        self.showAlertOnce = false
        self.onlineBtnClick = false
        self.groupChat_tableView.rowHeight = UITableViewAutomaticDimension
        self.groupChat_tableView.estimatedRowHeight = 90.0
        self.onlineUsers_tableView.separatorColor = UIColor.clear
        readMsgBool = false
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        loadingView.layer.masksToBounds = false
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(GroupChatMeassagesViewController.userBlockedNotification), name: NSNotification.Name(rawValue: "userBlocked"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLastKeyNodeForMessage(_:)), name: NSNotification.Name(rawValue: "updateLastKeyNodeForMessage"), object: nil)
        
        let status = Reach().connectionStatus()
        self.userLeftBool = true
        switch status
        {
        case .unknown, .offline:
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):

            self.getLastKeyForPagination()
            self.updateUserInChatRoom()
            self.getTotalMessagesCountInGroupChat()
            
            self.fetchGroupChatExistsOrNot(completion: {(result) in
                
                self.getMyProfileVisibility(completion: {(statusBool) in
                    
                    self.getMyWiChatRoomVisibility(completion: { (resBool) in
                        
                        self.allUsersInChatRoom(completion: { (result) in
                            
                            self.checkUser(completion: {(statusResult) in
                                self.fetchMyProfileVisibilityStatus()
                            })
                            
                        })
                    })
                })
            })
        }
        
        self.items = []
        self.noOnlineUsers_lbl.isHidden = true
        self.ChatView.frame.size.width = self.view.frame.size.width
        self.SendMessageView.frame.size.width = self.ChatView.frame.size.width
        self.OptionView.frame.size.width = self.ChatView.frame.size.width
        self.SendMessageView.frame.size.height = self.ChatView.frame.size.height
        self.OptionView.frame.size.height = self.ChatView.frame.size.height
        
        self.groupChat_tableView.separatorColor = UIColor.clear
        self.message_txtView.delegate=self
        message_txtView.layer.borderColor = UIColor.lightGray.cgColor
        message_txtView.layer.borderWidth = 1
        message_txtView.layer.cornerRadius = 5
        
        self.ChatView.clipsToBounds = true
        self.OptionView.isHidden = false
        self.animateExtraButtons(toHide: true)
        self.getUserDetails()
        self.getNodeOnceBool = false
    }
    
    
    // MARK: - ****** FromMeetUpScreen ******
    func refreshGroupChatAgain(_ anote: Notification)
    {
        print("refreshGroupChatAgain")
        let userInfo = (anote as NSNotification).userInfo
        let swipeStatus = (userInfo?["userInfo"] as? String)!
        print("swipeStatus = \(swipeStatus)")
        
        
        let status = Reach().connectionStatus()
        self.userLeftBool = true
        switch status
        {
        case .unknown, .offline:
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.getLastKeyForPagination()
            self.updateUserInChatRoom()
            self.getTotalMessagesCountInGroupChat()
            
            self.items = []
            self.fetchGroupChatExistsOrNot(completion: {(result) in
                
                self.getMyProfileVisibility(completion: {(statusBool) in
                    
                    self.getMyWiChatRoomVisibility(completion: { (resBool) in
                        
                        self.allUsersInChatRoom(completion: { (result) in
                            
                            self.checkUser(completion: {(statusResult) in
                                self.fetchMyProfileVisibilityStatus()
                            })
                            
                        })
                    })
                })
            })
        }
    }
    
      // MARK: - ****** Tap Gesture Method. ******
  /*  func handleTap(_ recognizer: UITapGestureRecognizer?) {
        // Do your thing.
        
        print("handleTap oon table view")
        self.view.endEditing(true)
        self.message_txtView.resignFirstResponder();
    }*/
    
    // MARK: - ****** Get Last Node Key For Downloaded Message. ******
    func updateLastKeyNodeForMessage(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        
//        if (self.getNodeOnceBool == false)
//        {
            self.getNodeOnceBool = true
            self.getLastKeyForPaging = (userInfo?["userInfo"] as? String)!
//        }
    }
    
    
    // MARK: - ****** Get Total Number of Messages in Group chat. ******
    func getTotalMessagesCountInGroupChat()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            
        case .online(.wwan) , .online(.wiFi):
             print("connected")
            
         /*   FIRDatabase.database().reference().child("GroupChat").observe(.value, with: { (snap) in
                
                if snap.exists()
                {
                    self.totalMessagesInt = Int(snap.childrenCount)
                }
            })*/
        }
    }
    
    // MARK: - ****** Get Last Key For Pagination Method. ******
    func getLastKeyForPagination()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
          
        case .online(.wwan) , .online(.wiFi):
    
            FIRDatabase.database().reference().child("GroupChat").queryOrderedByKey().queryLimited(toLast: 20).keepSynced(true)
            FIRDatabase.database().reference().child("GroupChat").queryOrderedByKey().queryLimited(toLast: 20).observeSingleEvent(of: .value, with: { (snap) in
                
                if snap.exists()
                {
                    let sna = snap.children.allObjects.first as! FIRDataSnapshot
                    self.getLastKeyForPaging = sna.key
                }
            })
        }
    }
    
    // MARK: - ****** Get Last Key From Pagination Messages. ******
    func getlastKeyFromPaginationMessages()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            
        case .online(.wwan) , .online(.wiFi):
            
           FIRDatabase.database().reference().child("GroupChat").queryOrderedByKey().queryEnding(atValue: self.getLastKeyForPaging).queryLimited(toLast: 20).keepSynced(true)
            FIRDatabase.database().reference().child("GroupChat").queryOrderedByKey().queryEnding(atValue: self.getLastKeyForPaging).queryLimited(toLast: 20).observeSingleEvent(of: .value, with: { (snap) in
                
                if snap.exists()
                {
                    let sna = snap.children.allObjects.first as! FIRDataSnapshot
                    self.getLastKeyForPaging = sna.key
                }
            })
        }
    }
    
    
    /// MARK: - ****** Fetch All Chat Messages With Pagination. ******
    func fetchDataWithPagination()
    {
        if (self.fetchingMessagesWithPaging == false)
        {
            let Count = self.items.count
            
            if  Count%20 != 0
            {
                return
            }
        }
        
        Message.downloadAllMessages_InGroupChat_Pagination(lastKetValue: self.getLastKeyForPaging,pageCount: Int64(self.totalMessagesInt), completion: {[weak weakSelf = self] (message) in
            
            if (self.totalMessagesInt > 20)
            {
//                self.totalMessagesInt = self.totalMessagesInt - 20
            }
       
            self.lastMessageStr = message.content as! String
            self.lastTimeStamp = message.timestamp
            
            weakSelf?.items.append(message)
            weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
            
            
            self.getlastKeyFromPaginationMessages()
            
            DispatchQueue.main.async {
                if let state = weakSelf?.items.isEmpty, state == false {
                    weakSelf?.groupChat_tableView.reloadData()
                    
                    self.fetchingMessagesWithPaging = false
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    let Count = weakSelf?.items.count ?? 0
                    if  (Count > 19) && (self.isChatScrolled == true)
                    {
                        self.isChatScrolled = false
                        
                        print("scroll to position  ////////////////////////////////////////")
                        
                        self.groupChat_tableView.scrollToRow(at: IndexPath.init(row: 19, section: 0), at: .top, animated: false)
                    }
                }
            }
         })
    }
    
    
    // MARK: - ****** UIScrollView Delegate Method. ******
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if (scrollView == self.groupChat_tableView)
        {
            print("scrollView.contentOffset.y = \(scrollView.contentOffset.y )")
            
            print("self.groupChat_tableView.contentOffset.y = \(self.groupChat_tableView.contentOffset.y)")
            
            print("self.groupChat_tableView.contentSize.height = \(self.groupChat_tableView.contentSize.height)")
            
            if (scrollView.contentOffset.y <= self.groupChat_tableView.contentOffset.y)
            {
                print("scrollView y is less than or equals to groupChat_tableView y")
            }
            
            
            if (scrollView.contentOffset.y == 0)
            {
//                if self.fetchingMessagesWithPaging == false
//                {
                  /*  let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    case .online(.wwan) , .online(.wiFi):
                        
                        self.pageCount += 20
                        self.getNodeOnceBool = false
                        
                        self.fetchingMessagesWithPaging = true
                        self.isChatScrolled = true
//                        self.loadingFirstTime = false
                        DispatchQueue.global(qos: .background).async {
                            self.fetchDataWithPagination()
                        }
                    }*/
//                }
            }
        }
        
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if (scrollView == self.onlineUsers_tableView)
        {
            if (self.onlineUsers_tableView.contentOffset.y >= (self.onlineUsers_tableView.contentSize.height - self.onlineUsers_tableView.frame.size.height) ) {
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    
                    if self.currentPageOnlineUsers < self.TotalPages {
                        
                        //                        print("self.TotalPages = \(self.TotalPages)")
                        //                        print("self.currentPageOnlineUsers = \(self.currentPageOnlineUsers)")
                        
                        let bottomEdge: Float = Float(self.onlineUsers_tableView.contentOffset.y + onlineUsers_tableView.frame.size.height)
                        if bottomEdge >= Float(onlineUsers_tableView.contentSize.height)  {
                            
                            print("we are at end of table")
                            
                            self.currentPageOnlineUsers = self.currentPageOnlineUsers + 1
                            
                            //                            print("self.currentPageOnlineUsers = \(self.currentPageOnlineUsers)")
                            
                            self.getAllOnlineUsersListing(paginationStr: self.currentPageOnlineUsers, completion: { (responseBool) in
                                if (responseBool == true)
                                {
                                    if (self.onlineUsersArray.count > 0)
                                    {
                                        DispatchQueue.main.async {
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            self.noOnlineUsers_lbl.isHidden = true
                                            self.onlineUsers_tableView.reloadData()
                                        }
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.onlineUsers_tableView.isHidden = false
                                        self.noOnlineUsers_lbl.isHidden = true
                                        self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                            self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                        }, completion: { (finished) in
                                        })
                                        self.onlineUsers_tableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                    }
                                }
                                else
                                {
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.onlineUsers_tableView.isHidden = false
                                    self.noOnlineUsers_lbl.isHidden = true
                                    self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                        self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                    }, completion: { (finished) in
                                    })
                                    self.onlineUsers_tableView.reloadData()
                                    self.refreshControl.endRefreshing()
                                }
                            })
                        }
                    }
                }
            }
        }else if (scrollView == self.groupChat_tableView) {
            
            if (scrollView.contentOffset.y == 0)
            {
                print("self.groupChat_tableView equals to 0")
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .online(.wwan) , .online(.wiFi):
                    
                    if self.fetchingMessagesWithPaging == false
                    {
                        self.pageCount += 20
                        self.getNodeOnceBool = false
                        self.fetchingMessagesWithPaging = true
                        self.isChatScrolled = true
                        
//                        self.loadingView.isHidden = false
//                        self.view.isUserInteractionEnabled = false
                        DispatchQueue.global(qos: .background).async {
                            self.fetchDataWithPagination()
                        }
                    }
                }
            }
            
         /*   if (self.groupChat_tableView.contentOffset.y >= (self.groupChat_tableView.contentSize.height - self.groupChat_tableView.frame.size.height) ) {
                
                let bottomEdge: Float = Float(self.groupChat_tableView.contentOffset.y + groupChat_tableView.frame.size.height)
                if bottomEdge >= Float(groupChat_tableView.contentSize.height)  {
                    
                    print("end of groupChat_tableView ")
                    self.loadingFirstTime = false
                    print("self.loadingFirstTime = false set")
                }
            }*/
        }
    }
    
    // MARK: - ****** Refresh Group Chat Method. ******
    func refreshGroupChat(_ sender: Any) {
        //  your code to refresh tableView
        
    }
    
    
    //MARK: - ***** userBlocked *****
    func userBlockedNotification()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.items = []
            
            self.updateUserInChatRoom()
            
            self.onlineUsersArray = []
            self.currentPageOnlineUsers = 1
            self.getAllOnlineUsersListing(paginationStr: self.currentPageOnlineUsers, completion: { (responseBool) in
                if (responseBool == true)
                {
                    if (self.onlineUsersArray.count > 0)
                    {
//                        print("onlineUsersArray = \(self.onlineUsersArray)")
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.noOnlineUsers_lbl.isHidden = true
                            self.onlineUsers_tableView.reloadData()
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.onlineUsersArray = []
                        self.onlineUsers_tableView.isHidden = false
                        self.noOnlineUsers_lbl.isHidden = false
                       /* self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })*/
                        self.onlineUsers_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.onlineUsersArray = []
                    self.onlineUsers_tableView.isHidden = false
                    self.noOnlineUsers_lbl.isHidden = false
                  /*  self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                    })*/
                    self.onlineUsers_tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
            
            
            self.fetchGroupChatExistsOrNot(completion: {(result) in
                
                self.getMyProfileVisibility(completion: {(statusBool) in
                    
                    self.getMyWiChatRoomVisibility(completion: { (resBool) in
                        self.checkUser(completion: {(statusResult) in
                            self.fetchMyProfileVisibilityStatus()
                        })
                    })
                })
            })
        }
    }
    
    
    //MARK: - ***** Fetch value of reported user *****
    func getValueOfOpponentReportUserInGroup(oppo_Id: String, completion: @escaping (Bool) -> Swift.Void)
    {
        if let id = UserDefaults.standard.value(forKey: "v") as? String
        {
            FIRDatabase.database().reference().child("ReportedUsersByMe").child(id).child(oppo_Id).child("continueAfterReportingStatus").observe(FIRDataEventType.value, with: { (snapshot) in
                
                if snapshot.exists()
                {
                    self.statusOfContinueStr = true
                    completion(true)
                }
                else{
                    self.statusOfContinueStr = false
                    completion(false)
                }
            })
        }
    }
    
    
    // MARK: - ****** Fetch My Profile Visibility Status from Firebase ******
    func fetchMyProfileVisibilityStatus()
    {
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            let ref = FIRDatabase.database().reference()
            ref.child("Users").child(id).child("statusCreditsPackages")
            ref.child("Users").child(id).child("statusCreditsPackages").observeSingleEvent(of:FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull{
                    self.myProfileVisibilityStatus = "profileVisibilityNotPurchased"
                }
                else
                {
                    self.myProfileVisibilityStatus = snapshot.value as! String
                    
                    if (self.myProfileVisibilityStatus == "generalCreditsPurchased") || (self.myProfileVisibilityStatus == "vipCreditsPurchased") || (self.myProfileVisibilityStatus == "vvipCreditsPurchased")
                    {
                        self.myProfileVisibilityStatus = "profileVisibilityPurchased"
                    }
                    else{
                        self.myProfileVisibilityStatus = "profileVisibilityNotPurchased"
                    }
                }
            })
        }
    }
    
    
    // MARK: - ****** Fetch Opponent Profile Visibility Status from Firebase ******
    func fetchOpponentProfileVisibilityStatus()
    {
        if (self.opponentUserID != "")
        {
            let ref = FIRDatabase.database().reference()
            ref.child("Users").child(self.opponentUserID).child("statusCreditsPackages")
            ref.child("Users").child(self.opponentUserID).child("statusCreditsPackages").observeSingleEvent(of:FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull{
                    self.opponentProfileVisibilityStatus = "profileVisibilityNotPurchased"
                    self.groupChat_tableView.reloadData()
                    
                }
                else
                {
                    self.opponentProfileVisibilityStatus = snapshot.value as! String
                    
                    if (self.opponentProfileVisibilityStatus == "generalCreditsPurchased") || (self.opponentProfileVisibilityStatus == "vipCreditsPurchased") || (self.opponentProfileVisibilityStatus == "vvipCreditsPurchased")
                    {
                        self.opponentProfileVisibilityStatus = "profileVisibilityPurchased"
                    }
                    else{
                        self.opponentProfileVisibilityStatus = "profileVisibilityNotPurchased"
                    }
                    
                    self.groupChat_tableView.reloadData()
                }
            })
        }
    }
    
    
    // MARK: - ****** getMyProfileVisibility ******
    func getMyWiChatRoomVisibility(completion: @escaping (Bool) -> Swift.Void)
    {
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            FIRDatabase.database().reference().child("Users").child(id).child("Settings").child("Notification").child("Wi_Chat_Room").observe(FIRDataEventType.value, with: { (snapshot) in
                
                if snapshot.value is NSNull{
                    self.wiChatRoomVisibilityStr = "On"
                    completion(true)
                }
                else
                {
                    self.wiChatRoomVisibilityStr = snapshot.value as! String
                    
                    if (self.wiChatRoomVisibilityStr == "on") || (self.wiChatRoomVisibilityStr == "On")
                    {
                        completion(true)
                    }
                    else{
                        completion(false)
                    }
                }
            })
        }
    }
    

     // MARK: - ****** getMyProfileVisibility ******
    func getMyProfileVisibility(completion: @escaping (Bool) -> Swift.Void)
    {
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
           FIRDatabase.database().reference().child("Users").child(id).child("Settings").child("General").child("Profile_Visibility").observe(FIRDataEventType.value, with: { (snapshot) in
            
                if snapshot.value is NSNull
                {
                    self.myProfileVisibilityStr = "On"
                    completion(true)
                }
                else
                {
                    self.myProfileVisibilityStr = snapshot.value as! String
                    if (self.myProfileVisibilityStr == "on")  || (self.myProfileVisibilityStr == "On")
                    {
                        completion(true)
                    }
                    else
                    {
                        completion(false)
                    }
                 }
            })
        }
    }

    
    // MARK: - ****** Fetch All Users In Chat Room. ******
    func allUsersInChatRoom(completion: @escaping (Bool) -> Swift.Void)
    {
        self.usersInChatRoomArray = []
        FIRDatabase.database().reference().child("UsersInChatRoom").keepSynced(true)
        FIRDatabase.database().reference().child("UsersInChatRoom").observeSingleEvent(of: .value, with: { (snapshot) in
            self.usersInChatRoomArray = []
            if snapshot.value is NSNull
            {
                self.usersInChatRoomArray = []
                completion(true)
                return
            }
            
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? FIRDataSnapshot
            {
                if let myID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                {
                    if rest.key != myID
                    {
//                        print("rest.value! = \(rest.key)")
                        if !(self.usersInChatRoomArray.contains(rest.key))
                        {
                            self.usersInChatRoomArray.add(rest.key)
                        }
                    }
                }
                
//                print("usersInChatRoomArray = \(self.usersInChatRoomArray)")
            }
            
            completion(true)
            
            if self.usersInChatRoomArray.count == 0
            {
                self.usersInChatRoomArray = []
                completion(true)
                return
            }
        })
    }
    
    
    // MARK: - ****** Update User In ChatRoom. ******
    func updateUserInChatRoom()
    {
        
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            var dataDict = [String : Any]()
            dataDict["UserID"] = currentUserID
            
            if let userName = UserDefaults.standard.value(forKey: "userName") as? String
            {
                dataDict["UserName"] = userName
                FIRDatabase.database().reference().child("UsersInChatRoom").child(currentUserID).updateChildValues(dataDict)
            }
        }
    }
    
    
    // MARK: - ****** checkUser ******
    func checkUser(completion: @escaping (Bool) -> Swift.Void)
    {
//        if let currentUserID = FIRAuth.auth()?.currentUser?.uid
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
                    FIRDatabase.database().reference().child("UsersInChatRoom").observe(.childAdded, with: { (snap) in
                        
                        if snap.exists()
                        {
                            let receivedMessage = snap.value as! [String: Any]
//                            print("receivedMessage = \(receivedMessage)")
                            let userEnteredID = receivedMessage["UserID"] as! String
                            let userEnteredName = receivedMessage["UserName"] as! String
                            
//                            if self.opponentWiChatRoomVisibilityStr == "On"
//                            {
                                if (userEnteredID != currentUserID) && !(self.usersInChatRoomArray.contains(userEnteredID))
                                {
                                    if !(self.usersInChatRoomArray.contains(userEnteredID))
                                    {
                                         self.usersInChatRoomArray.add(userEnteredID)
                                    }
                                    
                                    var profileVisibilityStr = String()
                                    
                                    if (self.blockedUsersArray.contains(userEnteredID)) || (self.blockedByUserArray.contains(userEnteredID))
                                    {
                                        print("blocked user and profile visitor is same")
                                    }
                                    else
                                    {
                                        FIRDatabase.database().reference().child("Users").child(userEnteredID).child("Settings").child("General").child("Profile_Visibility").keepSynced(true)
                                         FIRDatabase.database().reference().child("Users").child(userEnteredID).child("Settings").child("General").child("Profile_Visibility").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                                            if snapshot.value is NSNull{
                                                profileVisibilityStr = "On"
                                            }
                                            else
                                            {
                                                profileVisibilityStr = snapshot.value as! String
                                            }
                                            
                                            if (profileVisibilityStr == "")
                                            {
                                                profileVisibilityStr = "On"
                                            }
                                            
                                            if  profileVisibilityStr == "On"
                                            {
                                                if (self.wiChatRoomVisibilityStr == "On")
                                                {
                                                    if (userEnteredID != currentUserID)
                                                    {
                                                        self.notifyUsersEnter_lbl.isHidden = false
                                                        self.notifyUsersEnter_lbl.text = "\(userEnteredName) entered in the chat room."
                                                        self.notifyUsersEnter_lbl.adjustsFontSizeToFitWidth = true
                                                        
                                                        UIView.animate(withDuration: 1, animations: {
                                                            self.notifyUsersEnter_lbl.frame.origin.y = self.wrapperTitleView.frame.origin.y
                                                        }, completion: {(finished) in
                                                            
                                                            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                                                                
                                                                UIView.animate(withDuration: 1, animations: {
                                                                    self.notifyUsersEnter_lbl.frame.origin.y -= 51
                                                                }, completion: {(finished) in
                                                                    self.notifyUsersEnter_lbl.isHidden = true
                                                                })
                                                                
                                                            }
                                                        })
                                                    }
                                                    
                                                }
                                                else{
                                                    self.notifyUsersEnter_lbl.isHidden = true
                                                }
                                                
                                                 completion(true)
                                            }
                                            else
                                            {
                                                if (self.wiChatRoomVisibilityStr == "on")  || (self.wiChatRoomVisibilityStr == "On")
                                                {
                                                    self.notifyUsersEnter_lbl.isHidden = false
                                                    self.notifyUsersEnter_lbl.text = "VIP member entered in the chat room."
                                                    self.notifyUsersEnter_lbl.adjustsFontSizeToFitWidth = true
                                                    
                                                    UIView.animate(withDuration: 1, animations: {
                                                        self.notifyUsersEnter_lbl.frame.origin.y = self.wrapperTitleView.frame.origin.y
                                                    }, completion: {(finished) in
                                                        
                                                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                                                            UIView.animate(withDuration: 1, animations: {
                                                                self.notifyUsersEnter_lbl.frame.origin.y -= 51
                                                            }, completion: {(finished) in
                                                                self.notifyUsersEnter_lbl.isHidden = true
                                                            })
                                                        }
                                                    })
                                                }
                                                else{
                                                     self.notifyUsersEnter_lbl.isHidden = true
                                                }
                                            }
                                        })
                                    }
                                    completion(true)
                                }
                                else
                                {
                                    print("user id and opponent id is same")
                                    completion(true)
                                }
                        }
                        else{
                            completion(true)
                        }
                    })
                    
                    
                    FIRDatabase.database().reference().child("UsersInChatRoom").observe(.childRemoved, with: { (snap) in
                        if snap.exists()
                        {
                            let receivedMessage = snap.value as! [String: Any]
//                            print("receivedMessage = \(receivedMessage)")
                            let userEnteredID = receivedMessage["UserID"] as! String
                            let userEnteredName = receivedMessage["UserName"] as! String
                            
                            if (self.blockedUsersArray.contains(userEnteredID)) || (self.blockedByUserArray.contains(userEnteredID))
                            {
                                print("blocked user and profile visitor is same")
                            }
                            else
                            {
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                if (self.userLeftBool == true) || (appDelegate.cameFromHome == true)
                                {
                                    appDelegate.cameFromHome = false
//                                    if self.opponentWiChatRoomVisibilityStr == "On"
//                                    {
                                        if (userEnteredID != currentUserID) //&& (self.usersInChatRoomArray.contains(userEnteredID))
                                        {
                                            var profileVisibilityStr = String()
                                            FIRDatabase.database().reference().child("Users").child(userEnteredID).child("Settings").child("General").child("Profile_Visibility").keepSynced(true)
                                            FIRDatabase.database().reference().child("Users").child(userEnteredID).child("Settings").child("General").child("Profile_Visibility").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                                                if snapshot.value is NSNull{
                                                    profileVisibilityStr = "On"
                                                }
                                                else
                                                {
                                                    profileVisibilityStr = snapshot.value as! String
                                                }
                                                
                                                if (profileVisibilityStr == "")
                                                {
                                                    profileVisibilityStr = "On"
                                                }
                                                
                                                if (self.blockedUsersArray.contains(userEnteredID)) || (self.blockedByUserArray.contains(userEnteredID))
                                                {
                                                    print("blocked user and profile visitor is same")
                                                }
                                                else
                                                {
                                                    if  profileVisibilityStr == "On"
                                                    {
                                                        if (self.wiChatRoomVisibilityStr == "on") || (self.wiChatRoomVisibilityStr == "On")
                                                        {
                                                            self.notifyUsersEnter_lbl.isHidden = false
                                                            
                                                            self.notifyUsersEnter_lbl.text = "\(userEnteredName) left the chat room."
                                                        self.notifyUsersEnter_lbl.adjustsFontSizeToFitWidth = true
                                                            
                                                            UIView.animate(withDuration: 1, animations: {
                                                                self.notifyUsersEnter_lbl.frame.origin.y = self.wrapperTitleView.frame.origin.y
                                                            }, completion: {(finished) in
                                                                
                                                                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                                                                    UIView.animate(withDuration: 1, animations: {
                                                                        self.notifyUsersEnter_lbl.frame.origin.y -= 51
                                                                    }, completion: {(finished) in
                                                                        self.notifyUsersEnter_lbl.isHidden = true
                                                                    })
                                                                }
                                                            })
                                                        }
                                                        else
                                                        {
                                                            self.notifyUsersEnter_lbl.isHidden = true
                                                        }
                                                     
                                                    }
                                                    else
                                                    {
                                                        if (self.wiChatRoomVisibilityStr == "on") || (self.wiChatRoomVisibilityStr == "On")
                                                        {
                                                            self.notifyUsersEnter_lbl.isHidden = false
                                                            self.notifyUsersEnter_lbl.text = "VIP member left the chat room."
                                                            self.notifyUsersEnter_lbl.adjustsFontSizeToFitWidth = true
                                                            
                                                            UIView.animate(withDuration: 1, animations: {
                                                                self.notifyUsersEnter_lbl.frame.origin.y = self.wrapperTitleView.frame.origin.y
                                                            }, completion: {(finished) in
                                                                
                                                                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                                                                    UIView.animate(withDuration: 1, animations: {
                                                                        self.notifyUsersEnter_lbl.frame.origin.y -= 51
                                                                    }, completion: {(finished) in
                                                                        self.notifyUsersEnter_lbl.isHidden = true
                                                                    })
                                                                }
                                                            })
                                                        }
                                                        else
                                                        {
                                                            self.notifyUsersEnter_lbl.isHidden = true
                                                        }
                                                    }
                                                }
                                            })
                                            completion(true)
                                        }
                                }
                                else
                                {
                                    self.userLeftBool = false
                                    completion(true)
                                }
                            }
                        }
                    })
            }
    }
    
    
    
    // MARK: - ****** viewDidDisappear ******
    override func viewDidDisappear(_ animated: Bool) {
        
        let statusInternet = Reach().connectionStatus()
        switch statusInternet
        {
        case .unknown, .offline:
            print("Not connected")
            
        case .online(.wwan) , .online(.wiFi):
      
            let ref = FIRDatabase.database().reference()
            if openingPhotoLibrary == false
            {
                if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                {
                ref.child("Users").child(id).child("Settings").child("General").child("Profile_Visibility").removeAllObservers()
                    
                    ref.child("Users").child(id).child("blockedByUser").removeAllObservers()
                    ref.child("BlockedUsers").child(id).removeAllObservers()
                    ref.child("GroupChat").removeAllObservers()
                    ref.child("GroupChat").queryOrderedByKey().queryLimited(toLast: 50).removeAllObservers()
                    ref.child("GroupChat").removeAllObservers()
                    ref.child("Users").child(id).child("Wi_Chat_Room").removeAllObservers()
                }
            }
        }
    }


    // MARK: - ****** fetchGroupChatExistsOrNot. ******
    func fetchGroupChatExistsOrNot(completion: @escaping (Bool) -> Swift.Void)
    {
        FIRDatabase.database().reference().child("GroupChat").keepSynced(true)
        FIRDatabase.database().reference().child("GroupChat").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if snapshot.exists()
            {
                self.fetchData()
                completion(true)
            }
            else
            {
                self.items = []
                self.noMeesagesFoundView.isHidden = false
                self.noMeesagesFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.noMeesagesFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                })
                
                completion(true)
                self.groupChat_tableView.reloadData()
            }
        })
    }
    
    
    /// MARK: - ****** Fetch All Chat Messages. ******
    func fetchData()
    {
        Message.downloadAllMessages_InGroupChat(pageCount: Int64(self.totalMessagesInt), completion: {[weak weakSelf = self] (message) in
            
            if (self.totalMessagesInt > 20)
            {
//                self.totalMessagesInt = self.totalMessagesInt - 20
            }
            
            weakSelf?.items.append(message)
            weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
            
            if (self.loadMoreMessagesBool == false)
            {
                DispatchQueue.main.async {
                    
                    if let state = weakSelf?.items.isEmpty, state == false {
                        
                        weakSelf?.groupChat_tableView.reloadData()
                        
                        print("loadingFirstTime = \(self.loadingFirstTime)")
                        
                        if (self.appDelegatePush == "FromHomePage") && (self.loadingFirstTime == true)
                        {
                            print("scroll to position @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                            
                            weakSelf?.groupChat_tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                            
                            if (self.groupChat_tableView.contentOffset.y >= (self.groupChat_tableView.contentSize.height - self.groupChat_tableView.frame.size.height) ) {
                                
                                let bottomEdge: Float = Float(self.groupChat_tableView.contentOffset.y + self.groupChat_tableView.frame.size.height)
                                if bottomEdge >= Float(self.groupChat_tableView.contentSize.height)  {
                                    
                                    print("end of groupChat_tableView ")
                                    print("###################################")
                                    print("self.items.count = \(self.items.count)")
                                    if (self.items.count >= 20)
                                    {
                                        print("self.loadingFirstTime = false set")
                                        self.loadingFirstTime = false
                                    }
                                }
                            }
                        }
                        else if (self.appDelegatePush == "fromAppDelegate") && (self.isChatScrolled == false)
                        {
                            if (self.items.count > 1)
                            {
                                print("scroll to position  ********************************************")
                                
                                weakSelf?.groupChat_tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                                
                                if (self.groupChat_tableView.contentOffset.y >= (self.groupChat_tableView.contentSize.height - self.groupChat_tableView.frame.size.height) ) {
                                    
                                    let bottomEdge: Float = Float(self.groupChat_tableView.contentOffset.y + self.groupChat_tableView.frame.size.height)
                                    if bottomEdge >= Float(self.groupChat_tableView.contentSize.height)  {
                                        
                                        print("end of groupChat_tableView ")
                                        print("###################################")
                                        print("self.items.count = \(self.items.count)")
                                        if (self.items.count >= 20)
                                        {
                                            print("self.isChatScrolled = true set")
                                            self.isChatScrolled = true
                                        }
                                    }
                                }
                            }
                        }
                        else if (self.groupChat_tableView.contentOffset.y + self.view.frame.size.height) > (self.groupChat_tableView.contentSize.height) || (self.loadingFirstTime == true)
                        {
                            print("scroll to position ................................")
                            
                            weakSelf?.groupChat_tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                            
                            if (self.groupChat_tableView.contentOffset.y >= (self.groupChat_tableView.contentSize.height - self.groupChat_tableView.frame.size.height) ) {
                                
                                let bottomEdge: Float = Float(self.groupChat_tableView.contentOffset.y + self.groupChat_tableView.frame.size.height)
                                if bottomEdge >= Float(self.groupChat_tableView.contentSize.height)  {
                                    
                                    print("end of groupChat_tableView ")
                                    print("###################################")
                                    print("self.items.count = \(self.items.count)")
                                    if (self.items.count >= 20)
                                    {
                                        print("self.loadingFirstTime = false set")
                                        self.loadingFirstTime = false
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }

    
    
    // MARK: - ****** Get Current Users Details From Firebase. ******
    func getUserDetails()
    {
        if (UserDefaults.standard.value(forKey: "userDetailsDict") != nil)
        {
            usersDetailsDict = UserDefaults.standard.value(forKey: "userDetailsDict") as! NSMutableDictionary
        }
    }
    

    // MARK: - ****** Animation Button Action. ******
    func animateExtraButtons(toHide: Bool)  {
            switch toHide {
            case true:
                var frameOfOptionView = self.SendMessageView.frame as CGRect
                UIView.animate(withDuration: 0.1) {
                    self.SendMessageView.frame = CGRect.init(x: frameOfOptionView.origin.x, y: 0, width: frameOfOptionView.size.width, height: frameOfOptionView.size.height)
                    
                    frameOfOptionView = self.OptionView.frame as CGRect
                    self.OptionView.frame = CGRect.init(x: frameOfOptionView.origin.x, y: 0-frameOfOptionView.size.height, width: frameOfOptionView.size.width, height: frameOfOptionView.size.height)
                }
            default:
                
                var frameOfOptionView = self.OptionView.frame as CGRect
                UIView.animate(withDuration: 0.1) {
                    self.OptionView.frame = CGRect.init(x: frameOfOptionView.origin.x, y: 0, width: frameOfOptionView.size.width, height: frameOfOptionView.size.height)
                    
                    frameOfOptionView = self.SendMessageView.frame as CGRect
                    
                    self.SendMessageView.frame = CGRect.init(x: frameOfOptionView.origin.x, y: self.OptionView.frame.origin.y + self.OptionView.frame.size.height, width: frameOfOptionView.size.width, height: frameOfOptionView.size.height)
                }
            }
    }
    
    
    // MARK: - ****** viewWillAppear. ******
    override func viewWillAppear(_ animated: Bool) {
        
        self.userLeftBool = true
        
        if let profile_pic_thumb = UserDefaults.standard.value(forKey: "profile_pic_thumb") as? String
        {
            if profile_pic_thumb != ""
            {
                self.myProfilePicLink = profile_pic_thumb
            }
            else
            {
                if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                {
                    if userProfilePicture != ""
                    {
                        self.myProfilePicLink = userProfilePicture
                    }
                }
            }
        }
        else
        {
            if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
            {
                if userProfilePicture != ""
                {
                    self.myProfilePicLink = userProfilePicture
                }
            }
        }
      
    }
    
    // MARK: - ****** ViewWillDisAppear. ******
    override func viewWillDisappear(_ animated: Bool) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if readMsgBool == false
            {
//                 Message.markMessagesRead_InGroupChat()
            }
            else
            {
                self.readMsgBool = false
            }
        }
    }
    
    //MARK: ****** ViewDidAppear. ******
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(GroupChatMeassagesViewController.showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(GroupChatMeassagesViewController.hideKeyboard(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    //MARK: ****** NotificationCenter handlers ******
    func hideKeyboard(notification: Notification) {
        self.ChatView.frame.origin.y = self.groupChat_tableView.frame.origin.y + self.groupChat_tableView.frame.size.height
        self.groupChat_tableView.contentInset = UIEdgeInsets.zero
        self.groupChat_tableView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    
    func showKeyboard(notification: Notification) {
            if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let height = frame.cgRectValue.height
                self.ChatView.frame.origin.y = frame.cgRectValue.origin.y - self.ChatView.frame.size.height
                self.groupChat_tableView.contentInset.bottom = height
                self.groupChat_tableView.scrollIndicatorInsets.bottom = height
//                if (self.isTextEditingBegins == true)
//                {
                    if self.items.count > 1 {
                        
                     print("scroll to position  -------------------------------------")
                        
                        self.groupChat_tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
                    }
//                }
            }
    }

    
    // MARK: - ****** Close View Button Action. ******
    @IBAction func closeViewImage_btnAction(_ sender: Any) {
        self.viewImageVIew.isHidden = true
    }
    
    
    // MARK: - ****** Upload Chat Image In Firebase Button Acton. ******
    @IBAction func uploadImage_btnAction(_ sender: Any) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.loadingView.isHidden = false
            self.view.isUserInteractionEnabled = false
            self.composeMessage(type: .photo, content: self.selectedImage.image!)
        }
    }
    
    
    // MARK: - ****** Cancel Image Upload Button Action. ******
    @IBAction func cancelImageUpload_btnAction(_ sender: Any) {
        self.selectedImageView.isHidden = true
    }
    
    
    // MARK: - ****** Close Online Users View Button Action. ******
    @IBAction func closeOnlineUsersView_btnAction(_ sender: Any) {
        self.onlineUsersView.isHidden = true
    }
    
    
    
    // MARK: - ****** Show Online Users View Button Action. ******
    @IBAction func users_btnAction(_ sender: Any) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.onlineUsersView.isHidden = false
            self.subView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.subView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
               }, completion: { (finished) in
            })
            
            self.onlineBtnClick = true
            
            self.onlineUsersArray = []
            self.currentPageOnlineUsers = 1
            self.getAllOnlineUsersListing(paginationStr: self.currentPageOnlineUsers, completion: { (responseBool) in
                if (responseBool == true)
                {
                    if (self.onlineUsersArray.count > 0)
                    {
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.noOnlineUsers_lbl.isHidden = true
                            self.onlineUsers_tableView.reloadData()
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.onlineUsersArray = []
                        self.onlineUsers_tableView.isHidden = false
                        self.noOnlineUsers_lbl.isHidden = false
                        self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })
                        self.onlineUsers_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.onlineUsersArray = []
                    self.onlineUsers_tableView.isHidden = false
                    self.noOnlineUsers_lbl.isHidden = false
                    self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.noOnlineUsers_lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                    })
                    self.onlineUsers_tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
        }
    }
    
    // MARK: - ****** Get All online users listing from database. ******
    func getAllOnlineUsersListing(paginationStr: Int,completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                ApiHandler.getAllOnlineUsersForChatRoom(user_id: userID, auth_token: userToken, page: String(paginationStr), completion: { (responseData) in
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        
                        if (dataDict!["meta"] != nil)
                        {
                            let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                            
                            self.TotalPages = (metaDict?.value(forKey: "totalpages") as? Int)!
                        }
                        
                        if (dataDict!["list"] != nil)
                        {
                            let listingArray = dataDict?.value(forKey: "list") as! NSArray
                            
                            if (listingArray.count == 0)
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                self.onlineUsersArray = []
                                completion(false)
                            }
                            
                            for index in 0..<listingArray.count
                            {
                                var albumDict = NSDictionary()
                                albumDict = listingArray[index] as! NSDictionary
                                self.onlineUsersArray.add(albumDict)
                            }
                            completion(true)
                        }
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
//                        print("message = \(String(describing: message))")
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.onlineUsersArray = []
                        completion(false)
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.onlineUsersArray = []
                        completion(false)
                    }
                })
            }
        }
    }
    
    // MARK: - ****** Method For Sending Push Notification to the Post's Owner. ******
    func sendNotifications(message: String, title: String)
    {
        let titleStr = "Chatroom"
        let typeOfPush = "Chatroom"
        self.badgeCount = self.badgeCount + 1
            
            var post = NSString()
            //\"badge\":\"\(self.badgeCount)\",
            post = NSString(format:"{\"to\":\"/topics/chatroom\",\"notification\":{\"body\":\"\(message)\", \"title\":\"\(titleStr)\", \"type\":\"\(typeOfPush)\"},\"priority\":\"high\"}" as NSString)
            
            print("post = \(post)")
            
            var dataModel = post.data(using:  String.Encoding.nonLossyASCII.rawValue, allowLossyConversion: true)!
            let postLength = String(dataModel.count)
            let url = NSURL(string: "https://fcm.googleapis.com/fcm/send")
            let urlRequest = NSMutableURLRequest(url: url! as URL)
            urlRequest.httpMethod = "POST"
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("key=AAAA3VkUSMY:APA91bEnZp6IjfNV1ejbgr0coBzQeQoTLB3vZxq4OhXkbBhbruPfugX6IKXM69Zht9voYGOnPCU4IgLtvIzAUlqq1nrJMIrpi44YG2t0Ngl-8detdPjYF8Dr1wFlH3bnLibe-54NM6OQ", forHTTPHeaderField: "Authorization")
            urlRequest.httpBody = dataModel
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            let task = URLSession.shared.dataTask(with: urlRequest as URLRequest) { (data, response, error) -> Void in
                if let urlContent = data {
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers)
                        print(jsonResult)
                        
                    } catch {
                        print("JSON serialization failed = \(error.localizedDescription)")
                    }
                    
                } else {
                    print("ERROR FOUND HERE = \(String(describing: error?.localizedDescription))")
                }
            }
            task.resume()
    }
    
    
    // MARK: - ****** Back Button Action. ******
    @IBAction func backBtnAction(_ sender: Any)
    {
        if appDelegatePush == "fromAppDelegate"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        else
        {
            if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                FIRDatabase.database().reference().child("UsersInChatRoom").child(currentUserID).setValue(nil)
            }
            
            FIRDatabase.database().reference().child("Users").removeAllObservers()
            
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    
    // MARK: - ****** Open More Option Button Action. ******
    @IBAction func openOptions_btnAction(_ sender: Any) {
        self.animateExtraButtons(toHide: false)
    }
 
    
    // MARK: - ****** Send Message In A Group Button Action. ******
    @IBAction func sendMessage_btnAction(_ sender: Any) {
        if let text = self.message_txtView.text {
            if text.count > 0 {
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    
                    var textMessage = String()
                    textMessage = self.message_txtView.text!
                    if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false){
                        
                        var search_txt: String = self.message_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        self.composeMessage(type: .text, content: search_txt)
                    }
                    else{
                        print("you have entered empty string")
                    }
                    
                    self.message_txtView.text = ""
                }
            }
        }
    }
    
    // MARK: - ****** Update Message In Firebase Conversation. ******
    func composeMessage(type: MessageType, content: Any)
    {
        if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            var picture = String()
            if let profile_pic_thumb = UserDefaults.standard.value(forKey: "profile_pic_thumb") as? String
            {
                if profile_pic_thumb != ""
                {
                    picture = profile_pic_thumb
                }
                else{
                    picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
                }
            }
            else{
                picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
            }
            
            let name = UserDefaults.standard.value(forKey: "userName") as! String
            var myAUthToken = String()
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                 myAUthToken = userToken
            }
            
            var serverID = String()
            if let UserAuthID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                serverID = UserAuthID
            }
            
            let localTimestamp = Double(NSDate().timeIntervalSince1970)
            let message = Message.init(type: type, content: content, owner: .sender, timestamp: localTimestamp, isRead: false, profilePicture: picture, userName: name,userID: userID,typeOfUser: "",checkRevealProfile: "" , postID: "", postedText: "", postedImageType: "", postedConfessionCheck: "", myProfileInvisibilitySavedStr: self.myProfileVisibilityStr + "@" + self.myProfileVisibilityStatus, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "", opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: myAUthToken, serverID: serverID)
            
            Message.sendMessage_InGroupChat(message: message, completion: {(_) in
                if type == .photo
                {
                    self.selectedImageView.isHidden = true
                }
                else if type == .audio
                {
                    self.audioRecordingView.isHidden = true
                }
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                
                if type == .photo
                {
                    if (self.myProfileVisibilityStr == "on") || (self.myProfileVisibilityStr == "On")
                    {
                       self.sendNotifications(message: "\(name) sent a new photo.", title: "Chatroom")
                    }else {
                        self.sendNotifications(message: "VIP Member sent a new photo.", title: "Chatroom")
                    }
                    
                }
                else if type == .audio
                {
                    if (self.myProfileVisibilityStr == "on") || (self.myProfileVisibilityStr == "On")
                    {
                        self.sendNotifications(message: "\(name) sent a new audio.", title: "Chatroom")
                    }else {
                        self.sendNotifications(message: "VIP Member sent a new audio.", title: "Chatroom")
                    }
                }
                else
                {
                    if (self.myProfileVisibilityStr == "on") || (self.myProfileVisibilityStr == "On")
                    {
                        self.sendNotifications(message: "\(name): \(content as! String)", title: "Chatroom")
                    }else {
                        self.sendNotifications(message: "VIP Member: \(content as! String)", title: "Chatroom")
                    }
                }
            })
        }
    }
    
    
    // MARK: - ****** Open Message View Fron Options Button Action. ******
    @IBAction func openSendMessage_btnAction(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
    }
    
    
    // MARK: - ****** Open Gallary Button Action. ******
    @IBAction func openGallery_BtnAction(_ sender: Any) {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if  status == .denied {
            PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                let alert = UIAlertController(
                    title: "IMPORTANT",
                    message: "\("WI Match") Would like to access the Photos",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                }))
                
                self.present(alert, animated: true, completion: nil)
            })
        }
        else if status == .authorized || status == .notDetermined
        {
            if (self.myProfileVisibilityStr == "on") || (self.myProfileVisibilityStr == "On")
            {
                self.photoLibrary()
            }
            else
            {
                let alert = UIAlertController(title: "", message: "You can't send more messages in chat room until Turn ON your profile visibility from settings.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - ****** Choose Image From Photo Library. ******
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        myPickerController.allowsEditing = false
        openingPhotoLibrary = true
        self.present(myPickerController, animated: true, completion: nil)
    }

    // MARK: - ****** Open Camera Button Action. ******
    @IBAction func openCamera_btnAction(_ sender: Any) {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        if status == .denied {
            PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                let alert = UIAlertController(
                    title: "IMPORTANT",
                    message: "\("WI Match") Would like to capture the Photos",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                }))
                self.present(alert, animated: true, completion: nil)
            })
        }
        else if status == .authorized  || status == .notDetermined
        {
            if (self.myProfileVisibilityStr == "on") || (self.myProfileVisibilityStr == "On")
            {
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    let myPickerController = UIImagePickerController()
                    myPickerController.delegate = self;
                    myPickerController.sourceType = UIImagePickerControllerSourceType.camera
                    myPickerController.allowsEditing = false
                    
                    openingPhotoLibrary = true
                    self.present(myPickerController, animated: true, completion: nil)
                }
                else {
                    let alert = UIAlertController(title: "", message: "Sorry,Camera is not available to take picture.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                let alert = UIAlertController(title: "", message: "You can't send more messages in chat room until Turn ON your profile visibility from settings.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: - ****** UIImagePicker Delegate Methods. ******
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let pickedImage : UIImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        self.selectedImageView.isHidden = false
        self.selectedImage.image = pickedImage  //self.resizeImage(image: pickedImage, targetSize: CGSize(width: self.selectedImage.frame.size.width, height: self.selectedImage.frame.size.height))
        self.selectedImage.contentMode = UIViewContentMode.scaleAspectFit
        self.dismiss(animated: true, completion: nil)
    }
    
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - ****** Upload Audio Button Action. ******
    @IBAction func audio_btnAction(_ sender: Any) {

    }
    
    
    // MARK: - ****** UITextView Delegate Method. ******
     public func textViewDidBeginEditing(_ textView: UITextView)
     {
        print("textViewDidBeginEditing")
        self.isTextEditingBegins = true
      }
    
     public func textViewDidEndEditing(_ textView: UITextView)
     {
        print("textViewDidEndEditing")
        self.isTextEditingBegins = false
     }
    
   /* public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if (text == "\n")
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }*/
    
    // MARK: - ****** UITextView Delegate. ******
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let maxCharacter: Int = 200
        return (textView.text?.utf16.count ?? 0) + text.utf16.count - range.length <= maxCharacter
    }

    
    // MARK: - ****** UITouch Began Method. ******
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        // Hiding the Keyboard when the User Taps the Background
        if touches.first != nil
        {
            let touch: UITouch = (touches.first)!
            if (touch.view == self.groupChat_tableView){
                print("touchesBegan | This is an groupChat_tableView")
            }else{
                print("touchesBegan | This is not an groupChat_tableView")
            }
            
            self.isTextEditingBegins = false
            self.message_txtView.resignFirstResponder();
        }
        super.touchesBegan(touches, with: event)
    }

    
    // MARK: - ****** UITableView Delegate And DataSource Methods. ******
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == self.groupChat_tableView
        {
            return self.items.count
        }
        else
        {
            return self.onlineUsersArray.count
        }
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.groupChat_tableView
        {
            self.groupChat_tableView.separatorColor = UIColor.clear
            if (self.items.count > 0)
            {
                switch self.items[indexPath.row].owner {
                case .receiver:
                    let messagesCell = tableView.dequeueReusableCell(withIdentifier: "messagesCell", for: indexPath)as! MessagesTableViewCell
                    messagesCell.clearCellData()
                    messagesCell.selectionStyle = UITableViewCellSelectionStyle.none
                    messagesCell.usersProfileViewBtn.tag = indexPath.row
                    messagesCell.usersProfileViewBtn.isMultipleTouchEnabled = false
                    messagesCell.usersProfileViewBtn.addTarget(self, action: #selector(GroupChatMeassagesViewController.ViewOpponentProfileBtn(_:)), for: .touchUpInside)
                    
                    messagesCell.isRead_lbl.isHidden = true
                    messagesCell.userProfileImage.layer.cornerRadius = messagesCell.userProfileImage.frame.size.height/2
                    messagesCell.userProfileImage.layer.masksToBounds = true
                    messagesCell.userProfileImage.clipsToBounds = true
                    
                    switch self.items[indexPath.row].type {
                        
                    case .postedConfession:
                        print("postedConfession")
                        
                    case .text:
                        
                        messagesCell.message_txtView.font = UIFont.systemFont(ofSize: 16)
                        
                        /*  let inputString: NSString = self.items[indexPath.row].content as! NSString
                         let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                         let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                         cell.senderMsg_txtView.dataDetectorTypes = UIDataDetectorTypes.link
                         cell.senderMsg_txtView.text = modifiedString*/
                        
                        messagesCell.message_txtView.text = self.items[indexPath.row].content as! String
                        messagesCell.message_txtView.textAlignment = .left
                        
                        messagesCell.receiverBackImage.isHidden = true
                        messagesCell.receiverViewImage_btn.isHidden = true
                        
                        
                        var dateString = String()
                        if (String(self.items[indexPath.row].timestamp) != "")
                        {
                            let timeStampMessage = String(self.items[indexPath.row].timestamp)
                            if (timeStampMessage.characters.count > 12)
                            {
                                let timestampDate = NSDate(timeIntervalSince1970: Double((timeStampMessage))!/1000)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: timestampDate as Date)
                            }
                            else{
                                let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: date as Date)
                            }
                        }
                        
                        let fullNameStr: String = self.items[indexPath.row].myProfileInvisibilitySaved!
                        let fullNameArr = fullNameStr.components(separatedBy: "@")
                        
                        let firstName: String = fullNameArr[0]
                        if (firstName == "on") || (firstName == "On")
                        {
                            let opponentUserName = self.items[indexPath.row].userName
                            let attributedStr = NSMutableAttributedString(string: opponentUserName! + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                            messagesCell.isRead_lbl.isHidden = true
                            
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: self.items[indexPath.row].profilePicture!), placeholderImage: UIImage(named: "Default Icon"))
                            }
                        }
                        else
                        {
                            messagesCell.userProfileImage.image = nil
                            messagesCell.userProfileImage.contentMode = UIViewContentMode.scaleAspectFill
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                            }
                            
                            var attributedStr = NSMutableAttributedString()
                            
                            attributedStr = NSMutableAttributedString(string: "VIP Member" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:10))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                            messagesCell.isRead_lbl.isHidden = true
                        }
                        
                    case .photo:
                        
                        messagesCell.message_txtView.font = UIFont.systemFont(ofSize: 16)
                        messagesCell.receiverBackImage.isHidden = false
                        messagesCell.receiverViewImage_btn.isHidden = true
                        
                        if (UIScreen.main.bounds.height <= 568)
                        {
                            messagesCell.message_txtView.text = "ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
                        }
                        else
                        {
                            messagesCell.message_txtView.text = "ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
                        }
                        
                        
                        if let image = self.items[indexPath.row].image {
                            messagesCell.receiverBackImage.image = image
                            messagesCell.message_txtView.isHidden = true
                        } else {
                            messagesCell.message_txtView.isHidden = true
                            DispatchQueue.main.async {
                                messagesCell.receiverBackImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "loading"))
                            }
                        }
                        
                        messagesCell.receiverBackImage.contentMode  = UIViewContentMode.scaleAspectFill
                        messagesCell.receiverBackImage.clipsToBounds = true
                        
                        var dateString = String()
                        if (String(self.items[indexPath.row].timestamp) != "")
                        {
                            let timeStampMessage = String(self.items[indexPath.row].timestamp)
                            if (timeStampMessage.characters.count > 12)
                            {
                                let timestampDate = NSDate(timeIntervalSince1970: Double((timeStampMessage))!/1000)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: timestampDate as Date)
                            }
                            else{
                                let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: date as Date)
                            }
                        }
                        
                        let fullNameStr: String = self.items[indexPath.row].myProfileInvisibilitySaved!
                        let fullNameArr = fullNameStr.components(separatedBy: "@")
                        
                        let firstName: String = fullNameArr[0]
                        if (firstName == "on") || (firstName == "On")
                        {
                            let opponentUserName = self.items[indexPath.row].userName
                            let attributedStr = NSMutableAttributedString(string: opponentUserName! + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                            messagesCell.isRead_lbl.isHidden = true
                            
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: self.items[indexPath.row].profilePicture!), placeholderImage: UIImage(named: "Default Icon"))
                            }
                        }
                        else
                        {
                            
                            messagesCell.userProfileImage.image = nil
                            messagesCell.userProfileImage.contentMode = UIViewContentMode.scaleAspectFill
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                            }
                            
                            var attributedStr = NSMutableAttributedString()
                            
                            attributedStr = NSMutableAttributedString(string: "VIP Member" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:10))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                            messagesCell.isRead_lbl.isHidden = true
                        }
                        
                    case .audio:
                        
                        print("audio case")
                        
                    }
                    
                    return messagesCell
                    
                case .sender:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath) as! SenderCellTableViewCell
                    
                    cell.clearCellData()
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    
                    cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    cell.senderImage.layer.masksToBounds = true
                    cell.senderImage.clipsToBounds = true
                    cell.senderImage.contentMode = UIViewContentMode.scaleAspectFill
                    
                    cell.senderIsRead_lbl.isHidden = true
                    
                    switch self.items[indexPath.row].type {
                        
                    case .postedConfession:
                        print("postedConfession")
                        
                    case .text:
                        
                        cell.senderMsg_txtView.font = UIFont.systemFont(ofSize: 16)
                        
                        /*  let inputString: NSString = self.items[indexPath.row].content as! NSString
                         let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                         let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                         cell.senderMsg_txtView.dataDetectorTypes = UIDataDetectorTypes.link
                         cell.senderMsg_txtView.text = modifiedString*/
                        
                        cell.senderMsg_txtView.text = self.items[indexPath.row].content as! String
                        cell.senderMsg_txtView.textAlignment = .right
                        cell.senderBackImage.isHidden = true
                        cell.senderViewImage_btn.isHidden = true
                        
                        let isReadMessage = self.items[indexPath.row].isRead
                        if isReadMessage == true
                        {
                            cell.senderIsRead_lbl.text = "Sent"
                            cell.senderIsRead_lbl.isHidden = false
                        }
                        else
                        {
                            cell.senderIsRead_lbl.isHidden = true
                            cell.senderIsRead_lbl.text = ""
                        }
                        
                        var dateString = String()
                        if (String(self.items[indexPath.row].timestamp) != "")
                        {
                            let timeStampMessage = String(self.items[indexPath.row].timestamp)
                            
                            if (timeStampMessage.characters.count > 12)
                            {
                                let timestampDate = NSDate(timeIntervalSince1970: Double((timeStampMessage))!/1000)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: timestampDate as Date)
                            }
                            else{
                                let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: date as Date)
                            }
                        }
                        
                        let fullNameStr: String = self.items[indexPath.row].myProfileInvisibilitySaved!
                        let fullNameArr = fullNameStr.components(separatedBy: "@")
                        let firstName: String = fullNameArr[0]
                        if (firstName == "on") || (firstName == "On")
                        {
                            let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
                                cell.senderImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                        }
                        else
                        {
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
                                //                            cell.senderImage.image = UIImage(named: "VIP Member")
                                cell.senderImage.sd_setImage(with: URL(string:""), placeholderImage: UIImage(named: "VIP Member"))
                            }
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "VIP Member" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:10))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                        
                        cell.senderImage.layer.masksToBounds = true
                        cell.senderImage.clipsToBounds = true
                        cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                        
                    case .photo:
                        
                        cell.senderMsg_txtView.font = UIFont.systemFont(ofSize: 16)
                        cell.senderBackImage.isHidden = false
                        cell.senderViewImage_btn.isHidden = true
                        
                        if (UIScreen.main.bounds.height <= 568)
                        {
                            cell.senderMsg_txtView.text = "ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
                        }
                        else
                        {
                            cell.senderMsg_txtView.text = "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
                        }
                        
                        if let image = self.items[indexPath.row].image {
                            cell.senderBackImage.image = image
                            
                            cell.senderMsg_txtView.isHidden = true
                        } else {
                            cell.senderMsg_txtView.isHidden = true
                            DispatchQueue.main.async {
                                
                                if (self.items.count > 0)
                                {
                                    if ((self.items[indexPath.row].content as! String) != nil)
                                    {
                                        if ((self.items[indexPath.row].content as! String) != "")
                                        {
                                            cell.senderBackImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "loading"))
                                        }else{
                                            cell.senderBackImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "loading"))
                                        }
                                    }
                                    else{
                                        cell.senderBackImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "loading"))
                                    }
                                }
                            }
                        }
                        
                        cell.senderBackImage.contentMode = UIViewContentMode.scaleAspectFill
                        cell.senderBackImage.clipsToBounds = true
                        
                        let isReadMessage = self.items[indexPath.row].isRead
                        if isReadMessage == true{
                            cell.senderIsRead_lbl.text = "Sent"
                            cell.senderIsRead_lbl.isHidden = false
                        }
                        else{
                            cell.senderIsRead_lbl.isHidden = true
                            cell.senderIsRead_lbl.text = ""
                        }
                        
                        var dateString = String()
                        if (String(self.items[indexPath.row].timestamp) != "")
                        {
                            let timeStampMessage = String(self.items[indexPath.row].timestamp)
                            if (timeStampMessage.characters.count > 12)
                            {
                                let timestampDate = NSDate(timeIntervalSince1970: Double((timeStampMessage))!/1000)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: timestampDate as Date)
                            }
                            else{
                                let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: date as Date)
                            }
                        }
                        
                        let fullNameStr: String = self.items[indexPath.row].myProfileInvisibilitySaved!
                        let fullNameArr = fullNameStr.components(separatedBy: "@")
                        let firstName: String = fullNameArr[0]
                        if (firstName == "on") || (firstName == "On")
                        {
                            let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
                                cell.senderImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                        }
                        else
                        {
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
                                cell.senderImage.sd_setImage(with: URL(string:""), placeholderImage: UIImage(named: "VIP Member"))
                            }
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "VIP Member" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:10))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                        
                        cell.senderImage.layer.masksToBounds = true
                        cell.senderImage.clipsToBounds = true
                        cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                        
                    case .audio:
                        cell.senderMsg_txtView.font = UIFont.systemFont(ofSize: 16)
                        cell.senderMsg_txtView.isHidden = true
                        cell.senderBackImage.isHidden = false
                        cell.senderViewImage_btn.isHidden = true
                        //                    cell.senderAudioCell.isHidden = true
                        //                    cell.audioImage.isHidden = false
                        
                        let isReadMessage = self.items[indexPath.row].isRead
                        if isReadMessage == true{
                            cell.senderIsRead_lbl.text = "Sent"
                            cell.senderIsRead_lbl.isHidden = false
                        }
                        else{
                            cell.senderIsRead_lbl.text = ""
                            cell.senderIsRead_lbl.isHidden = true
                        }
                        
                        
                        var dateString = String()
                        if (String(self.items[indexPath.row].timestamp) != "")
                        {
                            let timeStampMessage = String(self.items[indexPath.row].timestamp)
                            if (timeStampMessage.characters.count > 12)
                            {
                                let timestampDate = NSDate(timeIntervalSince1970: Double((timeStampMessage))!/1000)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: timestampDate as Date)
                            }
                            else{
                                let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                                let dayTimePeriodFormatter = DateFormatter()
                                dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                                dateString = dayTimePeriodFormatter.string(from: date as Date)
                            }
                        }
                        
                        let fullNameStr: String = self.items[indexPath.row].myProfileInvisibilitySaved!
                        let fullNameArr = fullNameStr.components(separatedBy: "@")
                        let firstName: String = fullNameArr[0]
                        if (firstName == "on") || (firstName == "On")
                        {
                            let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
                                cell.senderImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                        }
                        else
                        {
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
                                cell.senderImage.sd_setImage(with: URL(string:""), placeholderImage: UIImage(named: "VIP Member"))
                            }
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "VIP Member" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:10))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                        
                        cell.senderImage.layer.masksToBounds = true
                        cell.senderImage.clipsToBounds = true
                        cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    }
                    
                    return cell
                }
            }else{
                 let messagesCell = tableView.dequeueReusableCell(withIdentifier: "messagesCell", for: indexPath)as! MessagesTableViewCell
                return messagesCell
            }
        }
        else
        {
            let onlineUsersCell = tableView.dequeueReusableCell(withIdentifier: "onlineUsersCell", for: indexPath) as! OnlineUsersTableViewCell
            
            self.onlineUsers_tableView.separatorColor = UIColor.clear
            
            onlineUsersCell.selectionStyle = UITableViewCellSelectionStyle.none
            
            if (self.onlineUsersArray.count <= 0)
            {
                return onlineUsersCell
            }
            else
            {
                let dict = ((self.onlineUsersArray[indexPath.row]) as AnyObject)
                let keysArray = dict.allKeys as! [String]
                
                if (((self.onlineUsersArray[indexPath.row]) as AnyObject) != nil)
                {
                    if (keysArray.contains("profile_pic") && keysArray.contains("location"))
                    {
                        let urlStr = ((self.onlineUsersArray[indexPath.row]) as AnyObject).value(forKey: "profile_pic") as! String
                        
                        if (urlStr != "")
                        {
                            DispatchQueue.main.async {
                                
                                onlineUsersCell.usersOnlineIcon.isHidden = true
                                
                                onlineUsersCell.usersProfilePic.sd_setImage(with: URL(string: urlStr), placeholderImage: UIImage(named: ""), completed: { (image, err, imageCacheType, imageUrl) in
                                    
                                    onlineUsersCell.usersProfilePic.contentMode = UIViewContentMode.scaleAspectFill
                                    
                                    onlineUsersCell.usersProfilePic.clipsToBounds = true
                                    
                                    if (self.onlineUsersArray.count > 0)
                                    {
                                        let usersStatus = ((self.onlineUsersArray[indexPath.row]) as AnyObject).value(forKey: "online_status") as? String
                                        
                                        if (usersStatus == "")
                                        {
                                            onlineUsersCell.usersOnlineIcon.isHidden = true
                                            onlineUsersCell.usersOnlineIcon.image = UIImage(named: "Offline Icon")
                                        }
                                        else
                                        {
                                            if usersStatus == "1"
                                            {
                                                onlineUsersCell.usersOnlineIcon.isHidden = false
                                                onlineUsersCell.usersOnlineIcon.image = UIImage(named: "Online Icon")
                                            }
                                            else
                                            {
                                                onlineUsersCell.usersOnlineIcon.isHidden = true
                                                onlineUsersCell.usersOnlineIcon.image = UIImage(named: "Offline Icon")
                                            }
                                        }
                                    }
                                    else{
                                      onlineUsersCell.usersOnlineIcon.isHidden = true
                                      onlineUsersCell.usersOnlineIcon.image = UIImage(named: "Offline Icon")
                                    }
                                })
                            }
                            
                            let userName = ((self.onlineUsersArray[indexPath.row]) as AnyObject).value(forKey: "username") as? String
                            let userAge = ((self.onlineUsersArray[indexPath.row]) as AnyObject).value(forKey: "age") as? String
                            onlineUsersCell.userName_lbl.text = userName! + ", " +  userAge!
                            onlineUsersCell.userName_lbl.adjustsFontSizeToFitWidth = true
                            
                            let usersDetailsDict = self.onlineUsersArray[indexPath.row] as! NSDictionary
                            let keysArray = usersDetailsDict.allKeys as! [String]
                            
                            var userCurrentCity = String()
                            var userCurrentState = String()
                            if keysArray.contains("city")
                            {
                                userCurrentCity = usersDetailsDict.value(forKey: "city") as! String
                            }
                            else{
                                userCurrentCity = ""
                            }
                            
                            if keysArray.contains("state")
                            {
                                userCurrentState = (usersDetailsDict.value(forKey: "state") as? String)!
                            }
                            else{
                                userCurrentState = ""
                            }
                            
                            if (userCurrentCity != "") && (userCurrentState != "")
                            {
                                onlineUsersCell.usersLocation_lbl.text = userCurrentCity + ", " + userCurrentState
                            }
                            else
                            {
                                onlineUsersCell.usersLocation_lbl.text = ((self.onlineUsersArray[indexPath.row]) as AnyObject).value(forKey: "location") as? String
                            }
                            
                            onlineUsersCell.usersLocation_lbl.adjustsFontSizeToFitWidth = true
                            
                            return onlineUsersCell
                        }
                        else{
                            return onlineUsersCell
                        }
                    }
                    else{
                        return onlineUsersCell
                    }
                }
                else{
                    return onlineUsersCell
                }
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == self.groupChat_tableView
        {
            self.view.endEditing(true)
            self.isTextEditingBegins = false
            self.message_txtView.resignFirstResponder()
            
            if (self.items.count > 0)
            {
                switch self.items[indexPath.row].owner {
                    
                case .receiver:
                    switch self.items[indexPath.row].type {
                        
                    case .photo:
                        
                         if (self.items.count > 0)
                         {
                            let messagecell = self.groupChat_tableView.cellForRow(at: indexPath) as! MessagesTableViewCell
                            
                            messagecell.receiverBackImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "loading"))
                            
                            self.particularImage.image = messagecell.receiverBackImage.image
                            
                            self.particularImage.contentMode = UIViewContentMode.scaleAspectFit
                            self.viewImageVIew.isHidden = false
                         }
                        
                    case .audio: break
                        
                    default: break
                    }
                    
                case .sender:
                    
                    switch self.items[indexPath.row].type {
                    case .photo:
                        
                        if (self.items.count > 0)
                        {
                            let messagecell = self.groupChat_tableView.cellForRow(at: indexPath) as! SenderCellTableViewCell
                            
                            messagecell.senderBackImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "loading"))
                            
                            self.particularImage.image = messagecell.senderBackImage.image
                            self.particularImage.contentMode = UIViewContentMode.scaleAspectFit
                            self.viewImageVIew.isHidden = false
                        }
                        
                    case .audio: break
                        
                    default: break
                    }
                }
            }
        }
        else
        {
            if (self.onlineUsersArray.count > 0)
            {
                let userID =  ((self.onlineUsersArray[indexPath.row]) as AnyObject).value(forKey: "id") as? String
                
                if self.blockedIdStr == "userAlreadyBlocked"
                {
                    let alert = UIAlertController(title: "", message: "You’ve Been Blocked For Being Outta Timin.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.blockedIdStr == "usersBlockedByMe"
                {
                    let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.blockedIdStr == "userAlreadyReportedByOtherUser"
                {
                    let alert = UIAlertController(title: "", message: "User Reported You.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.blockedIdStr == "userAlreadyReportedByMe"
                {
                    let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    
                    
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            print("Not connected")
                            
                        case .online(.wwan) , .online(.wiFi):
                            
                            if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                            {
                                var dict = [String : Any]()
                                dict["continueAfterReportingStatus"] = "continueTrue"
                                FIRDatabase.database().reference().child("ReportedUsersByMe").child(id).child(userID!).updateChildValues(dict, withCompletionBlock: { (error, referResult) -> Void in
                                    
                                    
                                })
                            }
                        }
                    }))
                    
                    self.getValueOfOpponentReportUserInGroup(oppo_Id: userID!, completion: { (status) in
                        
                        if (status == false)
                        {
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            let profile_visibility =  ((self.onlineUsersArray[indexPath.row]) as AnyObject).value(forKey: "profile_visibility") as? String
                            self.viewOnlineUsersProfile(senderValueInt: indexPath.row,profile_visibility: profile_visibility!)
                        }
                    })
                }
                else
                {
                    let profile_visibility =  ((self.onlineUsersArray[indexPath.row]) as AnyObject).value(forKey: "profile_visibility") as? String
                    
                    self.viewOnlineUsersProfile(senderValueInt: indexPath.row,profile_visibility: profile_visibility!)
                }
            }
        }
    }
    
    
    // MARK: - ****** View Online Users Profile Method. ******
    func viewOnlineUsersProfile(senderValueInt: Int,profile_visibility: String)
    {
        self.loadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
        if (profile_visibility == "on") || (profile_visibility == "On")
        {
            let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
            
            let auth_token =  ((self.onlineUsersArray[senderValueInt]) as AnyObject).value(forKey: "auth_token") as? String
            let userID =  ((self.onlineUsersArray[senderValueInt]) as AnyObject).value(forKey: "id") as? String
            
            viewProfileScreen.ownerID = userID!
            viewProfileScreen.ownerAuthToken = auth_token!
            viewProfileScreen.isKindOfScreen = "GroupChat"
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            self.navigationController?.pushViewController(viewProfileScreen, animated: true)
        }
        else{
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }

    }
    
    
    // MARK: - ****** View Opponent's profile button Action. ******
    func ViewOpponentProfileBtn(_ sender: UIButton!)
    {
        self.clickedIndex = sender.tag
        
        let indexPathRow = IndexPath(row: sender.tag, section: 0)
        let messagesCell = self.groupChat_tableView.cellForRow(at: indexPathRow) as! MessagesTableViewCell
        
        var checkUserNameStr = String()
        checkUserNameStr = messagesCell.userName_lbl.text!
        
        if (self.items.count > 0)
        {
            if (self.items[sender.tag].serverID != "")
            {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                self.getStatusOfOpponent(oppoIDStr: self.items[sender.tag].serverID!, completion: { (result) in
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    if self.blockedByOpponentStatus == "yes"
                    {
                        let alert = UIAlertController(title: "", message: "You’ve Been Blocked For Being Outta Timin.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if self.blockedByMeStatus == "yes"
                    {
                        let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if self.reportedByMeStatus == "yes"
                    {
                        let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                        
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                            
                            let status = Reach().connectionStatus()
                            switch status
                            {
                            case .unknown, .offline:
                                print("Not connected")
                                
                            case .online(.wwan) , .online(.wiFi):
                                
                             self.updateContinueChatWithReportedUser(opponentIDToContinue: self.items[sender.tag].serverID!, completion: { (result) in
                                    
                                    if (result == true)
                                    {
//                                    messagesCell.usersProfileViewBtn.isUserInteractionEnabled = false
                                        self.viewOtherUsersProfile(senderValueInt: sender.tag)
                                    }
                                })
                            }
                        }))
                        
                        
                        if (self.reportedByMe_ContinueChat == "0")
                        {
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
//                            messagesCell.usersProfileViewBtn.isUserInteractionEnabled = false
                            self.viewOtherUsersProfile(senderValueInt: sender.tag)
                        }
                    }
                    else if (checkUserNameStr.range(of: "VIP Member") != nil)
                    {
//            let alert = UIAlertController(title: "", message: "VIP Member", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
                    }
                    else if (checkUserNameStr.range(of: "Anonymous User") != nil)
                    {
//            let alert = UIAlertController(title: "", message: "Anonymous User", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {
//                        messagesCell.usersProfileViewBtn.isUserInteractionEnabled = false
                        self.viewOtherUsersProfile(senderValueInt: sender.tag)
                    }
                })
            }
            else{
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
            }
        }
        else{
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }
    }
    
    // MARK: - ****** Get the status of opponent users on click on opponent profile Method. ******
    func getStatusOfOpponent(oppoIDStr: String,completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getStatusOfBlockedUsersForMessages(userID: userID, authToken: userToken, other_user_id: oppoIDStr, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                            print("dataDict = \(String(describing: dataDict))")
                            
                            self.blockedByMeStatus = dataDict?.value(forKey: "blocked_byme") as! String
                            self.blockedByOpponentStatus = dataDict?.value(forKey: "blocked_byhim") as! String
                            self.reportedByMeStatus = dataDict?.value(forKey: "reported_byme") as! String
                            self.reportedByMe_ContinueChat = dataDict?.value(forKey: "reported_bymecontinuechat") as! String
                         
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
//                            print("message = \(String(describing: message))")
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    // MARK: - ****** Update Continue Chat with the reported user. ******
    func updateContinueChatWithReportedUser(opponentIDToContinue: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.view.isUserInteractionEnabled = false
                    self.loadingView.isHidden = false
                    
                    ApiHandler.updateReportContentToContinueChat(userID: userID, authToken: userToken, content_id: opponentIDToContinue, continue_chating: "1", completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                            print("dataDict = \(String(describing: dataDict))")
                            
                            self.reportedByMe_ContinueChat = "1"
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
//                            print("message = \(String(describing: message))")
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    
    func viewOtherUsersProfile(senderValueInt: Int)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            let fullNameStr: String = self.items[senderValueInt].myProfileInvisibilitySaved!
            let fullNameArr = fullNameStr.components(separatedBy: "@")
            let firstNameStr: String = fullNameArr[0]
            self.fetchCurrentUserDetails(currentUserID: self.items[senderValueInt].serverID!, firstName: firstNameStr, auth_Token: self.items[senderValueInt].auth_Token!)
        }
    }
    
    
    // MARK: - ****** Fetch Opponent's details from Firebase. ******
    func fetchCurrentUserDetails(currentUserID : String, firstName: String, auth_Token: String)
    {
        self.loadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
        
        self.viewProfileBool = true
        if (self.viewProfileBool == true)
        {
            if (firstName == "on") || (firstName == "On")
            {
                DispatchQueue.main.asyncAfter(deadline: .now())
                {
                    self.viewProfileBool = false
                    let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                    viewProfileScreen.ownerID = currentUserID
                    viewProfileScreen.ownerAuthToken = auth_Token
                    viewProfileScreen.isKindOfScreen = "GroupChat"
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                }
            }
            else
            {
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                self.viewProfileBool = false
            }
        }
        
       
        
        /*var currentUserDict = NSMutableDictionary()
        let ref = FIRDatabase.database().reference()
        ref.child("Users").keepSynced(true)
        ref.child("Users").child(currentUserID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if snapshot.value is NSNull {
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                //"User is no more longer available."
                let alert = UIAlertController(title: "", message: "Wi User no longer available", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                currentUserDict = snapshot.value as! NSMutableDictionary
                print("currentUserDict current user details = \(currentUserDict)")
                
                let allKeysArray = currentUserDict.allKeys as! [String]
                if  allKeysArray.contains("Age")
                {
                    FIRDatabase.database().reference().child("Users").child(currentUserID).keepSynced(true)
                    FIRDatabase.database().reference().child("Users").child(currentUserID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                        if snapshot.value is NSNull{
//                            profileVisibilityStr = "On"
                        }
                        else
                        {
//                            profileVisibilityStr = snapshot.value as! String
                        }
                        
                        
                        let userDict = snapshot.value as? NSDictionary
                        print("userDict = \(String(describing: userDict))")
                        
                        var auth_token = String()
                        if userDict!["auth_token"] != nil
                        {
                            auth_token = userDict?.value(forKey: "auth_token") as! String
                        }
                        else{
                            auth_token = ""
                        }
                        print("auth_token = \(String(describing: auth_token))")
                        
                        if (firstName == "on") || (firstName == "On")
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now())
                            {
                                let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                                viewProfileScreen.ownerID = currentUserID
                                viewProfileScreen.ownerAuthToken = auth_token
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                            }
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        }
                    })
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "", message: "Wi User no longer available", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })*/
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    /*
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
       if tableView == self.groupChat_tableView
       {
            switch self.items[indexPath.row].owner
           {
               case .receiver:
                      return false
               case .sender:
                      return false
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if tableView == self.groupChat_tableView
        {
            switch self.items[indexPath.row].owner
            {
            case .receiver: break
                
            case .sender:
                
                if editingStyle == .delete
                {
       
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                   
                       self.deleteParticularMessage(content: self.items[indexPath.row].content, timestamp: self.items[indexPath.row].timestamp)
                       self.items.remove(at: indexPath.row)
                    }
                }
            }
        }
     
    }
    
    // MARK: - ****** Delete Particular Message by the Owner. ******
    func deleteParticularMessage(content : Any , timestamp: Double)
    {
        if (FIRAuth.auth()?.currentUser?.uid) != nil
        {
           FIRDatabase.database().reference().child("GroupChat").observe(.value, with: { (snapshot) in
                if snapshot.exists()
                {
                    for item in snapshot.children
                    {
                        let receivedMessage = (item as! FIRDataSnapshot).value as! [String: Any]
                        let contentReceived = receivedMessage["content"] as! String
                        let timestampReceived = receivedMessage["timestamp"] as! Double
                        if (content as! String == contentReceived) && (timestamp == timestampReceived)
                        {
                            FIRDatabase.database().reference().child("GroupChat").child((item as! FIRDataSnapshot).key).setValue(nil)
                            self.groupChat_tableView.reloadData()
                        }
                    }
                }
            })
        }
    }*/
    
    
    // MARK: - ****** Get URL of Saved Audio File. ******
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent("sound.m4a")
        return soundURL as NSURL?
    }
    
    // MARK: - ****** Start Recording Method. ******
    func startRecording()
    {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
        }
    }
    
    
    // MARK: - ****** Finish Recording Method. ******
    func finishRecording(success: Bool) {
    
         let audioSession = AVAudioSession.sharedInstance()
        audioRecorder.stop()
        if success {
            print(success)
            self.finishAudioBtn.isUserInteractionEnabled = false
            do {
                try audioSession.setActive(false)
            } catch {
            }
            
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
    }
    
    
    // MARK: - ****** Audio Recording Delegate Method . ******
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            self.audioRecordedUrl = recorder.url as NSURL
            finishRecording(success: false)
        }
    }

    // MARK: - ****** Start Audio Recording Btn. ******
    @IBAction func startRecording_btnAction(_ sender: Any) {
        audioRecorder = nil
        self.finishAudioBtn.isUserInteractionEnabled = true
        if audioRecorder == nil
        {
            self.AudioUpdate_Lbl.isHidden = false
            self.AudioUpdate_Lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.AudioUpdate_Lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.AudioUpdate_Lbl.text = "Recording Started."
                }, completion: { (finished) in
                    self.AudioUpdate_Lbl.isHidden = true
            })
            self.startRecording()
        }
        else
        {
            print("audio recorder is not nil")
        }
    }
    
     // MARK: - ****** Stop Recording Btn. ******
    @IBAction func finishRecording_btnAction(_ sender: Any) {
        if audioRecorder != nil
        {
            self.AudioUpdate_Lbl.isHidden = false
            self.AudioUpdate_Lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.AudioUpdate_Lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.AudioUpdate_Lbl.text = "Recording Finished."
            }, completion: { (finished) in
                self.AudioUpdate_Lbl.isHidden = true
            })
            self.finishRecording(success: true)
        }
        else
        {
            self.finishAudioBtn.isUserInteractionEnabled = true
        }
    }
    
    
    // MARK: - ****** Play recorded audio btn action. ******
    @IBAction func playRecordedAudio_btnAction(_ sender: Any) {
        self.finishAudioBtn.isUserInteractionEnabled = true
        if audioRecorder != nil
        {
            if !audioRecorder.isRecording {
                self.audioPlayer = try! AVAudioPlayer(contentsOf: audioRecorder.url)
                self.audioPlayer.prepareToPlay()
                self.audioPlayer.delegate = self
                self.audioPlayer.play()
                
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                    try AVAudioSession.sharedInstance().setActive(true)
                } catch {
                    print(error)
                }
            }
        }
        else
        {
            print("audio recorder is nil")
        }
    }
    
    
    // MARK: - ****** Close Audio recording view. ******
    @IBAction func closeAudioRecordingView_btnAction(_ sender: Any) {
        self.finishAudioBtn.isUserInteractionEnabled = true
        self.audioRecorder?.stop()
        self.audioPlayer?.stop()
        audioRecorder = nil
        self.audioRecordingView.isHidden = true
    }
    
    
    
    // MARK: - ****** Audio Player Delegate Methods. ******
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print(flag)
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?){
        print(error.debugDescription)
    }
    internal func audioPlayerBeginInterruption(_ player: AVAudioPlayer){
        print(player.debugDescription)
    }
    
    // MARK: - ****** Upload Audio to Firebase. ******
    @IBAction func uploadAudioToFirebase_btnAction(_ sender: Any) {
        if self.blockedIdStr == "userAlreadyBlocked"
        {
            let alert = UIAlertController(title: "", message: "You’ve Been Blocked For Being Outta Timin.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.blockedIdStr == "usersBlockedByMe"
        {
            let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.blockedIdStr == "userAlreadyReportedByOtherUser"
        {
            let alert = UIAlertController(title: "", message: "User Reported You.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.blockedIdStr == "userAlreadyReportedByMe"
        {
            let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            
            
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    print("Not connected")
                    
                case .online(.wwan) , .online(.wiFi):
          
                    if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                    {
                        var dict = [String : Any]()
                        dict["continueAfterReportingStatus"] = "continueTrue"
                        FIRDatabase.database().reference().child("ReportedUsersByMe").child(id).child(self.opponentUserID).updateChildValues(dict, withCompletionBlock: { (error, referResult) -> Void in
                           
                            
                        })
                    }
                    
//                   self.uploadAudioMethod()
                }
                
            }))
            
            
            self.getValueOfOpponentReportUserInGroup(oppo_Id: self.items[(sender as AnyObject).tag].userID!, completion: { (status) in
                
                if (status == false)
                {
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                   self.uploadAudioMethod()
                }
            })
            
        }
        else
        {
           self.uploadAudioMethod()
        }
    }
    
    func uploadAudioMethod()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if audioRecorder != nil
            {
                if audioRecorder.isRecording
                {
                    self.AudioUpdate_Lbl.isHidden = false
                    self.AudioUpdate_Lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.AudioUpdate_Lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        self.AudioUpdate_Lbl.text = "Please finish recording before sending."
                    }, completion: { (finished) in
                        self.AudioUpdate_Lbl.isHidden = true
                    })
                }
                else
                {
                    self.composeMessage(type: .audio, content: self.audioRecorder.url)
                }
            }
            else
            {
                self.AudioUpdate_Lbl.isHidden = false
                self.AudioUpdate_Lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.AudioUpdate_Lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    self.AudioUpdate_Lbl.text = "Please record audio to send."
                }, completion: { (finished) in
                    self.AudioUpdate_Lbl.isHidden = true
                })
            }
        }
    }

    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
