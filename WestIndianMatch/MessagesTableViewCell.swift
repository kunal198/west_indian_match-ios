//
//  MessagesTableViewCell.swift
//  WestIndianMatch
//
//  Created by brst on 27/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import FLAnimatedImage

class MessagesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var bubbleReceivedIcon: UIImageView!
    
    @IBOutlet weak var receiverAudioImage: UIImageView!
    @IBOutlet weak var receiverViewImage_btn: UIButton!
    @IBOutlet weak var isRead_lbl: UILabel!
    @IBOutlet weak var receiverBackImage: UIImageView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var message_txtView: UITextView!
    @IBOutlet weak var dateTime_lbl: UILabel!
    @IBOutlet weak var userName_lbl: UILabel!
    @IBOutlet weak var usersProfileViewBtn: UIButton!
    
    @IBOutlet weak var receiverSlider: UISlider!
    
    @IBOutlet weak var confessionReceiverImage: FLAnimatedImageView!
    
    
    func clearCellData()
    {
        self.message_txtView.text = nil
        self.message_txtView.isHidden = false
        self.userProfileImage.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        changeImageReceived("buuble_Received_Icon")
        if #available(iOS 11.0, *) {
//            bubbleReceivedIcon.tintColor = UIColor(named: "chat_bubble_color_sent")
        } else {
            // Fallback on earlier versions
        }
    }
    
    func changeImageReceived(_ name: String) {
        guard let image = UIImage(named: name) else { return }
        bubbleReceivedIcon.image = image
            .resizableImage(withCapInsets:
                UIEdgeInsetsMake(17, 21, 17, 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
        bubbleReceivedIcon.tintColor = UIColor.init(red: 241.0/255, green: 241.0/255, blue: 242.0/255, alpha: 1.0) // Dark Gray Color Code Acc. to App's theme
        bubbleReceivedIcon.isHidden = true
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
