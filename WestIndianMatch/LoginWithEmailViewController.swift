//
//  LoginWithEmailViewController.swift
//  WestIndianMatch
//
//  Created by brst on 25/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import AVFoundation

class LoginWithEmailViewController: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate {
    
    // MARK: - ****** Varables ******
    var emailStr = String()
    var passwordStr = String()
    var registerStr = String()
    var locationManager: CLLocationManager!
    var currentLatitude = Double()
    var currentLongitude = Double()
    var countryStr = String()
    var currentLocationCountry = String()
    var city = String()
    var state = String()
    var localityStr = String()
    var subLocalityStr = String()
    
    var getConversationStr = String()
    var getConversationAnonymousStr = String()
    
  
    // MARK: - ****** Outlets ******
    @IBOutlet weak var wrapperScrollView: UIScrollView!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var createAccountView: UIView!
    @IBOutlet weak var forgotPasswordView: UIView!
    @IBOutlet weak var signInBtnWrapperView: UIView!
    @IBOutlet weak var password_txtField: UITextField!
    @IBOutlet weak var passwordWrapperView: UIView!
    @IBOutlet weak var email_txtField: UITextField!
    @IBOutlet weak var emailWrapperView: UIView!
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var matchTitleImage: UIImageView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var loadingImage: UIImageView!
    
    
    // MARK: - ****** ViewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.isStatusBarHidden=false
        self.navigationController?.isNavigationBarHidden = true
        
        email_txtField.attributedPlaceholder = NSAttributedString(string: "Email Address",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        password_txtField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginWithEmailViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        Reach().monitorReachabilityChanges()
        
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        UserDefaults.standard.set(false, forKey: "isEmailVerified")
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    
    //MARK:- ****** didChangeAuthorization ******
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
            
        case .notDetermined , .denied , .restricted :
            
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
            }
            
            break
            
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
       /* default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
    }
    
    
    // MARK: - ****** didUpdateLocations ******
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.locationManager.location != nil
        {
            if let getCoordinates = manager.location?.coordinate
            {
                print("getCoordinates = \(getCoordinates)")
                
                let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.locationManager.stopUpdatingLocation()
                
                if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                    let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in // this is the last line that is being called
                        var placemark : CLPlacemark!
                        placemark = placemarks?[0]
                        if placemark != nil
                        {
                            var city = String()
                            if (placemark.addressDictionary?["City"] != nil)
                            {
                                city = (placemark.addressDictionary?["City"] as! String)
                            }
                            
//                            var country = String()
                            if (placemark.addressDictionary?["City"] != nil)
                            {
//                                country = (placemark.addressDictionary?["Country"] as! String)
                            }
                            
                            if (placemark.addressDictionary?["Country"] != nil)
                            {
                                self.currentLocationCountry = (placemark.addressDictionary?["Country"] as! String)
                            }
                            
                            var state = String()
                            if (placemark.addressDictionary?["State"] != nil)
                            {
                                state = (placemark.addressDictionary?["State"] as! String)
                            }
                            
                            self.city = city
                            self.state = state
                            self.countryStr = city + "," + state
                            
                            var vv =  String()
                            if (placemark.subLocality != nil)
                            {
                                vv = placemark.subLocality!
                            }
                            
                            var countWhiteSpaces = Int()
                            if (vv.range(of: " ") != nil)
                            {
                                countWhiteSpaces = (vv.components(separatedBy: " ").count) - 1
                            }
                            else
                            {
                                countWhiteSpaces = 0
                            }
                            
                            var objAtIndexBeforeLast = String()
                            var objAtIndexLast = String()
                            if (countWhiteSpaces > 0)
                            {
                                var fullNameArr = vv.components(separatedBy: " ")
                                objAtIndexBeforeLast = (fullNameArr[(fullNameArr.count) - 2])
                                objAtIndexLast = (fullNameArr.last)!
                                self.subLocalityStr = objAtIndexBeforeLast + " " + objAtIndexLast
                            }
                            else
                            {
                                self.subLocalityStr = vv
                            }
                            
                            if (placemark.locality != nil)
                            {
                                self.localityStr = placemark.locality!
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    // MARK: - ****** ViewWillAppear ******
    override func viewWillAppear(_ animated: Bool)
    {
        UIApplication.shared.isStatusBarHidden=false
        self.navigationController?.isNavigationBarHidden = true
        
        if registerStr == "throughRegistration"
        {
            self.email_txtField.text = emailStr
            self.password_txtField.text = passwordStr
        }
    }



    // MARK: - ****** Get Network Status Reachibility Method ******
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        let getNetworkStatus = (userInfo?["Status"] as? String)!
        if getNetworkStatus == "Offline"
        {

        }
    }
    
    // MARK: - ****** Back button action. ******
    @IBAction func back_btnAction(_ sender: Any) {
        
        if registerStr == "throughAddInterests"
        {
            let regitrationScreen = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(regitrationScreen, animated: true)
        }
        else{
            self.navigationController!.popViewController(animated: true)
        }
        
    }
    
    
    // MARK: - ****** Forgot Password Button Action. ******
    @IBAction func forgotPassword_btnAction(_ sender: Any) {
        let forgotScreen = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotScreen, animated: true)
    }
    
    
    // MARK: - ****** Register With Email Button Action. ******
    @IBAction func createNewAccount_btnAction(_ sender: Any) {
        let regitrationScreen = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(regitrationScreen, animated: true)
    }
    
    // MARK: - ****** Login With Email Button Action. ******
    @IBAction func signInWithEmail_btnAction(_ sender: Any) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
        
        if self.email_txtField.text == ""
        {
            let alert = UIAlertController(title: "", message: "Please enter your email", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if !isValidEmail(testStr: self.email_txtField.text!)
        {
            let alert = UIAlertController(title: "", message: "Please enter valid email for Sign In", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.password_txtField.text == ""
        {
            let alert = UIAlertController(title: "", message: "Please enter your password", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.loadingView.isHidden = false
            self.view.isUserInteractionEnabled = false
            
            var deviceID = String()
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                deviceID = uuid
            }
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if let refreshedToken = FIRInstanceID.instanceID().token() {
                appDelegate.myDeviceToken = refreshedToken
            }
            
            ApiHandler.loginUser(withEmail: self.email_txtField.text!, password: self.password_txtField.text!, myDeviceToken: appDelegate.myDeviceToken, deviceID: deviceID,device_type: "ios", completion: { (responseData) in
                
                if (responseData.value(forKey: "status") as? String == "200")
                {
                    let dataDict = responseData.value(forKey: "data") as? NSDictionary
                    let keysArray = dataDict?.allKeys as! [String]
                    if keysArray.contains("purchased_package")
                    {
                        let purchased_package_Array = dataDict?.value(forKey: "purchased_package") as! NSArray
                        if (purchased_package_Array.count <= 0)
                        {
                            UserDefaults.standard.setValue("packageNotPurchased", forKey: "checkPurchasedPackage")
                            UserDefaults.standard.synchronize()
                        }
                        else
                        {
                            for index in 0..<purchased_package_Array.count
                            {
                                var packagePurchasedDict = NSDictionary()
                                packagePurchasedDict = purchased_package_Array[index] as! NSDictionary
                                if (packagePurchasedDict["packages"] != nil)
                                {
                                    var packageDict = NSDictionary()
                                    packageDict = packagePurchasedDict.value(forKey: "packages") as! NSDictionary
                                    
                                    if (packageDict["description"] != nil)
                                    {
                                        let packageDetails = packageDict.value(forKey: "description") as! String
                                        
                                        UserDefaults.standard.setValue(packageDetails, forKey: "checkPurchasedPackageDetails")
                                        UserDefaults.standard.synchronize()
                                    }
                                }
                            }
                            
                            UserDefaults.standard.setValue("packagePurchased", forKey: "checkPurchasedPackage")
                            UserDefaults.standard.synchronize()
                        }
                    }
 
                    UserDefaults.standard.set(dataDict, forKey: "userDetailsDict")
                    UserDefaults.standard.set(self.password_txtField.text, forKey: "passwordSaved")
                    UserDefaults.standard.setValue(dataDict?.value(forKey: "auth_token")!, forKey: "auth_token")
                    UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic")!, forKey: "userProfilePicture")
                    UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic_thumb")!, forKey: "profile_pic_thumb")
                    UserDefaults.standard.set(dataDict?.value(forKey: "username")!, forKey: "userName")
                    UserDefaults.standard.set(dataDict?.value(forKey: "id")!, forKey: "UserAuthID")
                    
                    UserDefaults.standard.setValue(dataDict?.value(forKey: "firebase_user_id"), forKey: "firebase_user_id")
                    
                    UserDefaults.standard.set(dataDict?.value(forKey: "age") as? String, forKey: "Age")
                    UserDefaults.standard.set(dataDict?.value(forKey: "location") as? String, forKey: "Location")
                    UserDefaults.standard.set(dataDict?.value(forKey: "state") as? String, forKey: "state")
                    
                    if (dataDict!["status"] != nil)
                    {
                        let statusVerifyEmail = dataDict?.value(forKey: "status") as? String
                        if (statusVerifyEmail == "1")
                        {
                            UserDefaults.standard.setValue(true, forKey: "isEmailVerified")
                        }
                        else
                        {
                             UserDefaults.standard.setValue(false, forKey: "isEmailVerified")
                        }
                    }
                    
                    UserDefaults.standard.synchronize()
                    
                    UserDefaults.standard.set(dataDict?.value(forKey: "location_sharing") as? String, forKey: "location_sharing")
                    UserDefaults.standard.set(dataDict?.value(forKey: "wi_chat_room") as? String, forKey: "wi_chat_room")
                    UserDefaults.standard.set(dataDict?.value(forKey: "post_likes") as? String, forKey: "post_likes")
                    UserDefaults.standard.set(dataDict?.value(forKey: "new_matches") as? String, forKey: "new_matches")
                    UserDefaults.standard.set(dataDict?.value(forKey: "messages") as? String, forKey: "messages")
                    UserDefaults.standard.set(dataDict?.value(forKey: "comments") as? String, forKey: "comments")
                    UserDefaults.standard.set(dataDict?.value(forKey: "profile_visitors") as? String, forKey: "profile_visitors")
                    UserDefaults.standard.set(dataDict?.value(forKey: "profile_visibility") as? String, forKey: "profile_visibility")
                    UserDefaults.standard.synchronize()
                    
                    let usersFirebaseID = dataDict?.value(forKey: "firebase_user_id") as! String
                    
//                    let userID = dataDict?.value(forKey: "id") as? String
                    let ref = FIRDatabase.database().reference()
                    var userDetails = [String: Any]()
                    userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                    userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                    userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                    userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                    userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                    userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                    userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String

                    if let refreshedToken = FIRInstanceID.instanceID().token()
                    {
                        userDetails["myDeviceToken"] = refreshedToken
                    }
                    
                    ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                    
                    var dataDictForGeneralCategory = [String : Any]()
                    let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                    if (profile_visibility == "on")
                    {
                        dataDictForGeneralCategory["Profile_Visibility"] = "On"
                    }
                    else{
                        dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                    }
                ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                    
                    var settingsDict = [String : Any]()
                    let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                    if (chatRoomValue == "on")
                    {
                        settingsDict["Wi_Chat_Room"] = "On"
                    }
                    else{
                        settingsDict["Wi_Chat_Room"] = "Off"
                    }
                    
                    let messages = dataDict?.value(forKey: "messages") as? String
                    if (messages == "on")
                    {
                        settingsDict["Messages"] = "On"
                    }
                    else
                    {
                        settingsDict["Messages"] = "Off"
                    }
                    
                ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                    
                    if (dataDict!["is_firsttime_login"] != nil)
                    {
                        let is_firsttime_login = dataDict?.value(forKey: "is_firsttime_login") as? String
                        if (is_firsttime_login == "yes")
                        {
                            self.getConversation(usersFirebaseID: usersFirebaseID, completion: { (resultConversations) in
                                
                                self.getAnonymousConversations(usersFirebaseID: usersFirebaseID, completion: { (resultAnonymousConversations) in
                                    
                                    let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                                    UserDefaults.standard.set("success", forKey: "userRegistered")
                                    UserDefaults.standard.set("emailUser", forKey: "userType")
                                   self.navigationController?.pushViewController(homeScreen, animated: true)
                                })
                            })
                        }
                        else
                        {
//                            let userID = dataDict?.value(forKey: "id") as? String
                            let ref = FIRDatabase.database().reference()
                            var userDetails = [String: Any]()
                            userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                            userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                            userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                            userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                            userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                            userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                            userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                            
                            if let refreshedToken = FIRInstanceID.instanceID().token()
                            {
                                userDetails["myDeviceToken"] = refreshedToken
                            }
                            
                        ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                            
                            
                            var dataDictForGeneralCategory = [String : Any]()
                            let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                            if (profile_visibility == "on")
                            {
                                dataDictForGeneralCategory["Profile_Visibility"] = "On"
                            }
                            else{
                                dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                            }
                            ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                            
                            var settingsDict = [String : Any]()
                            let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                            if (chatRoomValue == "on")
                            {
                                settingsDict["Wi_Chat_Room"] = "On"
                            }
                            else{
                                settingsDict["Wi_Chat_Room"] = "Off"
                            }
                            
                            let messages = dataDict?.value(forKey: "messages") as? String
                            if (messages == "on")
                            {
                                settingsDict["Messages"] = "On"
                            }
                            else
                            {
                                settingsDict["Messages"] = "Off"
                            }
                            
                        ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                            
                            
                            let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                            UserDefaults.standard.set("success", forKey: "userRegistered")
                            UserDefaults.standard.set("emailUser", forKey: "userType")
                            self.navigationController?.pushViewController(homeScreen, animated: true)
                        }
                    }
                    else
                    {
//                        let userID = dataDict?.value(forKey: "id") as? String
                        let ref = FIRDatabase.database().reference()
                        var userDetails = [String: Any]()
                        userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                        userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                        userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                        userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                        userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                        userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                        userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                        
                        if let refreshedToken = FIRInstanceID.instanceID().token()
                        {
                            userDetails["myDeviceToken"] = refreshedToken
                        }
                        
                    ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                        
                        
                    var dataDictForGeneralCategory = [String : Any]()
                    let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                        if (profile_visibility == "on")
                        {
                            dataDictForGeneralCategory["Profile_Visibility"] = "On"
                        }
                        else{
                            dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                        }
                  ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                        
                        var settingsDict = [String : Any]()
                        let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                        if (chatRoomValue == "on")
                        {
                            settingsDict["Wi_Chat_Room"] = "On"
                        }
                        else{
                            settingsDict["Wi_Chat_Room"] = "Off"
                        }
                        
                        let messages = dataDict?.value(forKey: "messages") as? String
                        if (messages == "on")
                        {
                            settingsDict["Messages"] = "On"
                        }
                        else
                        {
                            settingsDict["Messages"] = "Off"
                        }
                        
                  ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                        
                        
                        let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                        UserDefaults.standard.set("success", forKey: "userRegistered")
                        UserDefaults.standard.set("emailUser", forKey: "userType")
                        self.navigationController?.pushViewController(homeScreen, animated: true)
                    }
                }
                else if (responseData.value(forKey: "status") as? String == "400")
                {
                    let message = responseData.value(forKey: "message") as? String
                    if (message == "Please verify your email.")
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        
                        let alert = UIAlertController(title: "", message: "Please verify your email.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                            
                            let emailVerifyScreen = self.storyboard!.instantiateViewController(withIdentifier: "EmailVerificationViewController") as! EmailVerificationViewController
                            self.navigationController?.pushViewController(emailVerifyScreen, animated: true)
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else if (responseData.value(forKey: "status") as? String == "405")
                {
                    let message = responseData.value(forKey: "message") as? String
                    
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
    }
  }

    
    func getConversation(usersFirebaseID: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let databaseRef = FIRDatabase.database().reference()
    databaseRef.child("Users").child(usersFirebaseID).child("conversations").observe(FIRDataEventType.value, with: { (snapshot) in
            
            if (snapshot.exists())
            {
                self.getConversationStr = "conversationsExists"
                
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot
                {
                    let conversationDict = rest.value as! NSDictionary
                    
                    let typeOfUser = conversationDict.value(forKey: "userType") as! String
                    let opponentStatus = conversationDict.value(forKey: "opponentProfileVisibilityStatus") as! String
                    let myStatus = conversationDict.value(forKey: "myProfileVisibilityStatus") as! String
                    let location = conversationDict.value(forKey: "location") as! String
                    
                    let data = ["location": location, "userType": typeOfUser,"myProfileVisibilityStatus": myStatus, "opponentProfileVisibilityStatus": opponentStatus]
                databaseRef.child("Users").child(usersFirebaseID).child("conversations").child(rest.key).updateChildValues(data)
                }
                
                completion(true)
            }
            else
            {
                self.getConversationStr = "conversationsDoesNotExists"
                completion(true)
            }
        })
    }
    
    
    func getAnonymousConversations(usersFirebaseID: String, completion: @escaping (Bool) -> Swift.Void)
    {
       let databaseRef = FIRDatabase.database().reference()
    databaseRef.child("Users").child(usersFirebaseID).child("conversations_anonymous").observe(FIRDataEventType.value, with: { (snapshotAnonymousConvo) in
            
            if (snapshotAnonymousConvo.exists())
            {
                self.getConversationAnonymousStr = "conversationsExists"
                
                let enumerator = snapshotAnonymousConvo.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot
                {
                    let conversationDict = rest.value as! NSDictionary
                    
                    let typeOfUser = conversationDict.value(forKey: "userType") as! String
                    
                    let location = conversationDict.value(forKey: "location") as! String
                    
                    if conversationDict["checkRevealProfile"] != nil
                    {
                        let checkRevealProfile = conversationDict.value(forKey: "checkRevealProfile") as! String
                        
                        let data = ["location": location, "userType": typeOfUser,"checkRevealProfile": checkRevealProfile]
                    databaseRef.child("Users").child(usersFirebaseID).child("conversations_anonymous").child(rest.key).updateChildValues(data)
                    }
                    else
                    {
                        let data = ["location": location, "userType": typeOfUser]
                    databaseRef.child("Users").child(usersFirebaseID).child("conversations_anonymous").child(rest.key).updateChildValues(data)
                    }
                }
                
                completion(true)
            }
            else
            {
                self.getConversationAnonymousStr = "conversationsDoesNotExists"
                completion(true)
            }
        })
    }

    
    //MARK:- ****** EMAIL VALIDATION ******
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
 
    //MARK:- ****** UITEXTFIELD DELEGATE ******
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
       
    }
    
  
     public func textFieldDidEndEditing(_ textField: UITextField)
     {
       
      }
    
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func keyboardWillShow(notification:NSNotification)
    {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.wrapperScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        wrapperScrollView.contentInset = contentInset
        
        self.wrapperScrollView.isScrollEnabled=true
       
        
        if (self.view.frame.size.height == 736)
        {
            self.wrapperScrollView.contentSize.height = self.createAccountView.frame.origin.y + self.createAccountView.frame.size.height + ((self.view.frame.size.height/2) - 170)
            self.wrapperScrollView.contentOffset = CGPoint(x:0, y:50)
        }
        else if (self.view.frame.size.height == 667)
        {
            self.wrapperScrollView.contentSize.height = self.createAccountView.frame.origin.y + self.createAccountView.frame.size.height + ((self.view.frame.size.height/2) - 120)
            self.wrapperScrollView.contentOffset = CGPoint(x:0, y:100)
        }
        else
        {
            self.wrapperScrollView.contentSize.height = self.createAccountView.frame.origin.y + self.createAccountView.frame.size.height + ((self.view.frame.size.height/2) - 200)
            self.wrapperScrollView.contentOffset = CGPoint(x:0, y:100)
        }
    }
    
    
    func keyboardWillHide(notification:NSNotification)
    {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        wrapperScrollView.contentInset = contentInset
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.wrapperScrollView.isScrollEnabled=true
        self.wrapperScrollView.contentSize.height = self.createAccountView.frame.origin.y + self.createAccountView.frame.size.height + 10
    }
    
    
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
