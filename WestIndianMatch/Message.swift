//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import UIKit
import Firebase
import FLAnimatedImage

class Message {
    //MARK: Properties
    var owner: MessageOwner
    var type: MessageType
    var content: Any
    var timestamp: Double
    var isRead: Bool
    var image: UIImage?
    var toID: String?
    var fromID: String?
    var profilePicture : String?
    var userName : String?
    var userID : String?
    var typeOfUser : String?
    var getRevealProfileStr : String?
    var postID: String?
    var postedText: String?
    var postedImageType: String?
    var postedConfessionCheck: String?
    var myProfileInvisibilitySaved: String?
    var myConversationType: String?
    var myRevealProfileStatus: String?
    var opponentRevealProfileStatus: String?
    var myPackagesStatus: String?
    var opponentPackageStatus: String?
    var auth_Token: String?
    var serverID: String?
    
    //MARK: Methods
    class func downloadAllMessages(forUserID: String,opponentProfileVisibilityStr: String,myProfileVisibilityStr: String, getPaginationCount: Int, completion: @escaping (Message) -> Swift.Void)
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            var opponentVisibilityStr = String()
            opponentVisibilityStr = forUserID + "@" + opponentProfileVisibilityStr
            
            let myAndOppoStatusStr = opponentVisibilityStr + "@" + myProfileVisibilityStr
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(myAndOppoStatusStr).observe(.value, with: { (snapshot) in
            
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    var userType = String()
                    var getRevealStr = String()
                    if data["userType"] != nil
                    {
                        userType  = data["userType"]!
                    }
                    else{
                        userType = "WithProfileUser"
                    }
                   
                    if data["checkRevealProfile"] != nil
                    {
                        getRevealStr = data["checkRevealProfile"]!
                    }
                    else{
                        getRevealStr = "profileRevealed"
                    }
                    
                    //.queryOrderedByKey().queryLimited(toLast: UInt(getPaginationCount))
                FIRDatabase.database().reference().child("conversations").child(location).observe(.childAdded, with: { (snap) in
                        if snap.exists()
                        {
                            let receivedMessage = snap.value as! [String: Any]
                            if receivedMessage["type"] != nil
                            {
                                let messageType = receivedMessage["type"] as! String
                                var type = MessageType.text
                                switch messageType
                                {
                                case "photo":
                                    type = .photo
                                case "audio":
                                    type = .audio
                                case "postedConfession":
                                    type = .postedConfession
                                    
                                default: break
                                }
                                
                                let content = receivedMessage["content"] as! String
                                let fromID = receivedMessage["fromID"] as! String
                                let timestamp = receivedMessage["timestamp"] as! Double
                                let profilepic = receivedMessage["profilePicture"] as! String
                                let userName = receivedMessage["userName"] as! String
                                let userID = receivedMessage["UserID"] as! String
                                
                                var postedConfessionText = String()
                                if receivedMessage["confessionStatus"] != nil
                                {
                                    postedConfessionText = receivedMessage["confessionStatus"] as! String
                                }
                                else{
                                    postedConfessionText = ""
                                }
                                
                                
                                var postedConfessionImageType = String()
                                if receivedMessage["confessionImageType"] != nil
                                {
                                    postedConfessionImageType = receivedMessage["confessionImageType"] as! String
                                }
                                else{
                                    postedConfessionImageType = ""
                                }
                                
                                
                                var postedConfessionData = String()
                                if receivedMessage["confessionData"] != nil
                                {
                                    postedConfessionData = receivedMessage["confessionData"] as! String
                                }
                                else{
                                    postedConfessionData = ""
                                }
                                
                                var mySettingProfileInvisibility = String()
                                if receivedMessage["mySettingProfileInvisibility"] != nil
                                {
                                    mySettingProfileInvisibility = receivedMessage["mySettingProfileInvisibility"] as! String
                                }
                                else{
                                    mySettingProfileInvisibility = ""
                                }
                                
                                var auth_Token = String()
                                if receivedMessage["auth_Token"] != nil
                                {
                                    auth_Token = receivedMessage["auth_Token"] as! String
                                }
                                else{
                                    auth_Token = ""
                                }

                                
                                print("fromID = \(fromID)")
                                if fromID == currentUserID
                                {
                                    let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: userID,typeOfUser: userType,checkRevealProfile: getRevealStr,postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                    
                                    completion(message)
                                }
                                else
                                {
                                    let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: userID,typeOfUser: userType,checkRevealProfile: getRevealStr,postID: "", postedText: postedConfessionText, postedImageType: postedConfessionImageType, postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                    completion(message)
                                }
                            }
                        }
                    })
                }
                else{
                    print("conversation doesnot exists")
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteChatNotification"), object: nil, userInfo:  ["Status": myAndOppoStatusStr])
                }
            })
        }
    }
    
    
    class func downloadAllMessages_anonymousUser(forUserID: String, postId: String, completion: @escaping (Message) -> Swift.Void) {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String { //FIRAuth.auth()?.currentUser?.uid {
        
            var timeStampGetAlready = Double()
            
            let hh = forUserID + "@" + postId
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(hh).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    var userType = String()
                    var getRevealStr = String()
                    
                    if data["userType"] != nil
                    {
                        userType = data["userType"]!
                    }
                    else{
                        userType = "WithProfileUser"
                    }
                    
                    if data["checkRevealProfile"] != nil
                    {
                        getRevealStr  = data["checkRevealProfile"]!
                    }
                    else{
                        getRevealStr = "profileRevealed"
                    }
                    
               FIRDatabase.database().reference().child("conversations_anonymous").child(location).observe(.childAdded, with: { (snap) in
                        if snap.exists()
                        {
                            let receivedMessage = snap.value as! [String: Any]
                            print("receivedMessage = \(receivedMessage)")
                            
                            if receivedMessage["type"] != nil
                            {
                                let messageType = receivedMessage["type"] as! String
                                var type = MessageType.text
                                switch messageType
                                {
                                case "photo":
                                    type = .photo
                                case "audio":
                                    type = .audio
                                case "postedConfession":
                                    type = .postedConfession
                                default: break
                                }
                                
                                let content = receivedMessage["content"] as! String
                                let fromID = receivedMessage["fromID"] as! String
                                let timestamp = receivedMessage["timestamp"] as! Double
                                let profilepic = receivedMessage["profilePicture"] as! String
                                let userName = receivedMessage["userName"] as! String
                                let userID = receivedMessage["UserID"] as! String
                                let postIDStr = receivedMessage["postID"] as! String
                                
                                
                                var postedConfessionText = String()
                                if receivedMessage["confessionStatus"] != nil
                                {
                                    postedConfessionText = receivedMessage["confessionStatus"] as! String
                                }
                                else{
                                    postedConfessionText = ""
                                }
                                
                                
                                var postedConfessionImageType = String()
                                if receivedMessage["confessionImageType"] != nil
                                {
                                    postedConfessionImageType = receivedMessage["confessionImageType"] as! String
                                }
                                else{
                                    postedConfessionImageType = ""
                                }
                                
                                
                                var postedConfessionData = String()
                                if receivedMessage["confessionData"] != nil
                                {
                                    postedConfessionData = receivedMessage["confessionData"] as! String
                                }
                                else{
                                    postedConfessionData = ""
                                }
                                
                                var mySettingProfileInvisibility = String()
                                if receivedMessage["mySettingProfileInvisibility"] != nil
                                {
                                    mySettingProfileInvisibility = receivedMessage["mySettingProfileInvisibility"] as! String
                                }
                                else{
                                    mySettingProfileInvisibility = ""
                                }
                                
                                var auth_Token = String()
                                if receivedMessage["auth_Token"] != nil
                                {
                                    auth_Token = receivedMessage["auth_Token"] as! String
                                }
                                else{
                                    auth_Token = ""
                                }
                                
                                print("fromID = \(fromID)")
                                
                                if fromID == currentUserID
                                {
                                    let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: userID,typeOfUser: userType,checkRevealProfile: getRevealStr,postID: postIDStr, postedText: postedConfessionText, postedImageType: postedConfessionImageType, postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                    
                                    if timestamp != timeStampGetAlready
                                    {
                                        timeStampGetAlready = timestamp
                                        completion(message)
                                    }
                                    else{
                                        print("timestamp matches")
                                    }
                                }
                                else
                                {
                                    let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: userID,typeOfUser: userType,checkRevealProfile: getRevealStr,postID: postIDStr,postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                    
                                    if timestamp != timeStampGetAlready
                                    {
                                        timeStampGetAlready = timestamp
                                        completion(message)
                                    }
                                    else
                                    {
                                        print("timestamp matches")
                                    }
                                }
                            }
                        }
                    })
                }
                else
                {
                    print("conversation doesnot exists")
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteChatNotification"), object: nil, userInfo: ["Status": hh])
                }
            })
        }
    }
    
  
    
    //MARK: Methods
    //    class func downloadAllMessages_InGroupChat(forUserID: String, completion: @escaping (Message) -> Swift.Void)
    class func downloadAllMessages_InGroupChat_Pagination(lastKetValue: String,pageCount: Int64,completion: @escaping (Message) -> Swift.Void)
    {
        //if let currentUserID = FIRAuth.auth()?.currentUser?.uid
        print("********************* pageCount ********************* = \(pageCount)")
        
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            var getPages = Int64()
            getPages = pageCount
            
            if (pageCount >= 20)
            {
                if (pageCount%20 == 0)
                {
                    getPages = 20
                }
                else
                {
                    getPages = pageCount
                }
            }
            else
            {
                getPages = pageCount
            }
            
            print("getPages = \(getPages)")
            FIRDatabase.database().reference().child("GroupChat").queryOrderedByKey().queryEnding(atValue: lastKetValue).queryLimited(toLast: 20).observe(.value, with: { (snap) in
                
                if snap.exists()
                {
                    if let sna = snap.children.allObjects.first as? FIRDataSnapshot
                    {
                        print("first object of sna = \(sna)")
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateLastKeyNodeForMessage"), object: nil, userInfo:  ["userInfo": sna.key])
                    }
                    
                    for childSnap in snap.children.allObjects
                    {
                        let snap1 = childSnap as! FIRDataSnapshot
                       
                        print("snap1.key = \(snap1.key)")
                        print("lastKetValue = \(lastKetValue)")
                        
                        if (snap1.key == lastKetValue)
                        {
                            print("+++++++++++++++++++++++ snap1.key == lastKetValue are same +++++++++++++++++++++++")
                        }
                        else
                        {
                            let receivedMessage = snap1.value as! [String: Any]
                            print("receivedMessage = \(receivedMessage)")
                            
                            if receivedMessage["type"] != nil
                            {
                                let messageType = receivedMessage["type"] as! String
                                var type = MessageType.text
                                switch messageType
                                {
                                case "photo":
                                    type = .photo
                                case "audio":
                                    type = .audio
                                case "postedConfession":
                                    type = .postedConfession
                                default: break
                                }
                                
                                let content = receivedMessage["content"] as! String
                                let fromID = receivedMessage["fromID"] as! String
                                let timestamp = receivedMessage["timestamp"] as! Double  //Int((receivedMessage["timestamp"] as? NSNumber)!)
                                let profilepic = receivedMessage["profilePicture"] as! String
                                let userName = receivedMessage["userName"] as! String
                                print("fromID = \(fromID)")
                                
                                var postedConfessionText = String()
                                if receivedMessage["confessionStatus"] != nil
                                {
                                    postedConfessionText = receivedMessage["confessionStatus"] as! String
                                }
                                else
                                {
                                    postedConfessionText = ""
                                }
                                
                                var postedConfessionImageType = String()
                                if receivedMessage["confessionImageType"] != nil
                                {
                                    postedConfessionImageType = receivedMessage["confessionImageType"] as! String
                                }
                                else{
                                    postedConfessionImageType = ""
                                }
                                
                                var postedConfessionData = String()
                                if receivedMessage["confessionData"] != nil
                                {
                                    postedConfessionData = receivedMessage["confessionData"] as! String
                                }
                                else{
                                    postedConfessionData = ""
                                }
                                
                                var  typeOfUser = String()
                                
                                if receivedMessage["typeOfUser"] != nil
                                {
                                    typeOfUser = receivedMessage["typeOfUser"] as! String
                                }
                                
                                var mySettingProfileInvisibility = String()
                                if receivedMessage["mySettingProfileInvisibility"] != nil
                                {
                                    mySettingProfileInvisibility = receivedMessage["mySettingProfileInvisibility"] as! String
                                }
                                else{
                                    mySettingProfileInvisibility = "On"
                                }
                                
                                var auth_Token = String()
                                if receivedMessage["auth_Token"] != nil
                                {
                                    auth_Token = receivedMessage["auth_Token"] as! String
                                }
                                else{
                                    auth_Token = ""
                                }
                                
                                var serverID = String()
                                if receivedMessage["serverID"] != nil
                                {
                                    serverID = receivedMessage["serverID"] as! String
                                }
                                else{
                                    serverID = ""
                                }
                                
                                if fromID == currentUserID
                                {
                                    let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: serverID)
                                    
                                    
                                    completion(message)
                                    
                                }
                                else
                                {
                                   /* let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: serverID)
                                    
                                    completion(message)*/
                                    
                                    FIRDatabase.database().reference().child("Users").child(fromID).keepSynced(true)
                                    FIRDatabase.database().reference().child("Users").child(fromID).observeSingleEvent(of: .value, with: { (snapshot) in
                                        
                                        if snapshot.value is NSNull
                                        {
                                            let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: serverID)
                                            
                                            completion(message)
                                        }
                                        else
                                        {
                                            let filterResultsDict = snapshot.value as! NSMutableDictionary
                                            let allKeys = filterResultsDict.allKeys as! [String]
                                            
                                            if (allKeys.contains("UserName") && allKeys.contains("ProfilePicture"))
                                            {
                                                let name = filterResultsDict.value(forKey: "UserName") as! String
                                                
                                                let ProfilePicture = filterResultsDict.value(forKey: "ProfilePicture") as! String
                                                
                                                let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: ProfilePicture, userName: name,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: serverID)
                                                completion(message)
                                                
                                            }
                                            else{
                                                let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: serverID)
                                                
                                                completion(message)
                                            }
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
            })
        }
    }
    
  /*  class func downloadlatestMessages_InGroupChat(pageCount: Int64,completion: @escaping (Message) -> Swift.Void)
    {
        if let currentUserID = FIRAuth.auth()?.currentUser?.uid
        {
            // .queryOrderedByKey().queryLimited(toLast: UInt(pageCount))
            FIRDatabase.database().reference().child("GroupChat").observe(.childAdded, with: { (snap1) in
                
                if snap1.exists()
                {
                    
//                    for childSnap in snap.children.allObjects
//                    {
                        
                        if let receivedMessage = snap1.value as? [String: Any]
                        {
                            if receivedMessage["type"] != nil
                            {
                                let messageType = receivedMessage["type"] as! String
                                var type = MessageType.text
                                switch messageType
                                {
                                case "photo":
                                    type = .photo
                                case "audio":
                                    type = .audio
                                case "postedConfession":
                                    type = .postedConfession
                                default: break
                                }
                                let content = receivedMessage["content"] as! String
                                let fromID = receivedMessage["fromID"] as! String
                                let timestamp = receivedMessage["timestamp"] as! Int
                                let profilepic = receivedMessage["profilePicture"] as! String
                                let userName = receivedMessage["userName"] as! String
                                //                                let userID = receivedMessage["UserID"] as! String
                                print("fromID = \(fromID)")
                                
                                var postedConfessionText = String()
                                if receivedMessage["confessionStatus"] != nil
                                {
                                    postedConfessionText = receivedMessage["confessionStatus"] as! String
                                }
                                else{
                                    postedConfessionText = ""
                                }
                                
                                
                                var postedConfessionImageType = String()
                                if receivedMessage["confessionImageType"] != nil
                                {
                                    postedConfessionImageType = receivedMessage["confessionImageType"] as! String
                                }
                                else{
                                    postedConfessionImageType = ""
                                }
                                
                                
                                var postedConfessionData = String()
                                if receivedMessage["confessionData"] != nil
                                {
                                    postedConfessionData = receivedMessage["confessionData"] as! String
                                }
                                else{
                                    postedConfessionData = ""
                                }
                                
                                var  typeOfUser = String()
                                
                                if receivedMessage["typeOfUser"] != nil
                                {
                                    typeOfUser = receivedMessage["typeOfUser"] as! String
                                }
                                
                                var mySettingProfileInvisibility = String()
                                if receivedMessage["mySettingProfileInvisibility"] != nil
                                {
                                    mySettingProfileInvisibility = receivedMessage["mySettingProfileInvisibility"] as! String
                                }
                                else{
                                    mySettingProfileInvisibility = ""
                                }
                                
                                if fromID == currentUserID
                                {
                                    let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "")
                                    completion(message)
                                }
                                else
                                {
                                    let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "")
                                    completion(message)
                                }
                            }
                        }
                }
            })
        }
    }*/
    
    //MARK: Methods
//    class func downloadAllMessages_InGroupChat(forUserID: String, completion: @escaping (Message) -> Swift.Void)
    class func downloadAllMessages_InGroupChat(pageCount: Int64,completion: @escaping (Message) -> Swift.Void)
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            FIRDatabase.database().reference().child("GroupChat").queryOrderedByKey().queryLimited(toLast: 20).observe(.childAdded, with: { (snap) in
                
                        if snap.exists()
                        {
//                            let sna = snap.children.allObjects.first as! FIRDataSnapshot
                            
                            let receivedMessage = snap.value as! [String: Any]
                            print("receivedMessage = \(receivedMessage)")

                                if receivedMessage["type"] != nil
                                {
                                    let messageType = receivedMessage["type"] as! String
                                    var type = MessageType.text
                                    switch messageType
                                    {
                                    case "photo":
                                        type = .photo
                                    case "audio":
                                        type = .audio
                                    case "postedConfession":
                                        type = .postedConfession
                                    default: break
                                    }
                                    
                                    let content = receivedMessage["content"] as! String
                                    let fromID = receivedMessage["fromID"] as! String
                                    let timestamp = receivedMessage["timestamp"] as! Double
                                    
                                    let profilepic = receivedMessage["profilePicture"] as! String
                                    let userName = receivedMessage["userName"] as! String
                                    print("fromID = \(fromID)")
                                    
                                    var postedConfessionText = String()
                                    if receivedMessage["confessionStatus"] != nil
                                    {
                                        postedConfessionText = receivedMessage["confessionStatus"] as! String
                                    }
                                    else{
                                        postedConfessionText = ""
                                    }
                                    
                                    var postedConfessionImageType = String()
                                    if receivedMessage["confessionImageType"] != nil
                                    {
                                        postedConfessionImageType = receivedMessage["confessionImageType"] as! String
                                    }
                                    else{
                                        postedConfessionImageType = ""
                                    }
                                    
                                    var postedConfessionData = String()
                                    if receivedMessage["confessionData"] != nil
                                    {
                                        postedConfessionData = receivedMessage["confessionData"] as! String
                                    }
                                    else{
                                        postedConfessionData = ""
                                    }
                                    
                                    var  typeOfUser = String()
                                    
                                    if receivedMessage["typeOfUser"] != nil
                                    {
                                        typeOfUser = receivedMessage["typeOfUser"] as! String
                                    }
                                    
                                    var mySettingProfileInvisibility = String()
                                    if receivedMessage["mySettingProfileInvisibility"] != nil
                                    {
                                        mySettingProfileInvisibility = receivedMessage["mySettingProfileInvisibility"] as! String
                                    }
                                    else{
                                        mySettingProfileInvisibility = "On"
                                    }
                                    
                                    var auth_Token = String()
                                    if receivedMessage["auth_Token"] != nil
                                    {
                                        auth_Token = receivedMessage["auth_Token"] as! String
                                    }
                                    else{
                                        auth_Token = ""
                                    }
                                    
                                    var serverID = String()
                                    if receivedMessage["serverID"] != nil
                                    {
                                        serverID = receivedMessage["serverID"] as! String
                                    }
                                    else{
                                        serverID = ""
                                    }
                                    
                                    if fromID == currentUserID
                                    {
                                        let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: serverID)
                                        
                                        completion(message)
                                    }
                                    else
                                    {
                                        FIRDatabase.database().reference().child("Users").child(fromID).keepSynced(true)
                                        FIRDatabase.database().reference().child("Users").child(fromID).observeSingleEvent(of: .value, with: { (snapshot) in
                                            
                                            if snapshot.value is NSNull
                                            {
                                                let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: serverID)
                                                completion(message)
                                            }
                                            else
                                            {
                                                print("snapshot.value = \(String(describing: snapshot.value))")
                                                
                                                let filterResultsDict = snapshot.value as! NSMutableDictionary
                                                let allKeys = filterResultsDict.allKeys as! [String]
                                                
                                                if (allKeys.contains("UserName") && allKeys.contains("ProfilePicture"))
                                                {
                                                    let name = filterResultsDict.value(forKey: "UserName") as! String
                                                    
                                                    let ProfilePicture = filterResultsDict.value(forKey: "ProfilePicture") as! String
                                                    
                                                    let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: ProfilePicture, userName: name,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: serverID)
                                                    completion(message)
                                                    
                                                }
                                                else{
                                                    let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: fromID,typeOfUser: typeOfUser,checkRevealProfile: "",postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: serverID)
                                                    completion(message)
                                                }
                                            }
                                        })
                                    }
                                }
                        }
                    })
//                }
//            })
        }
    }
    
    
    
    //MARK: Methods
    class func downloadAllMessagesAfterChildRemoved(forUserID: String, completion: @escaping (Message) -> Swift.Void) {
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String  {     //FIRAuth.auth()?.currentUser?.uid {
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(forUserID).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    var getRevealStr = String()
                    if data["checkRevealProfile"] != nil
                    {
                        getRevealStr  = data["checkRevealProfile"]!
                    }
                    else{
                        getRevealStr = "profileRevealed"
                    }
                    FIRDatabase.database().reference().child("conversations").child(location).observe(.value, with: { (snap) in
                        if snap.exists()
                        {
                            let receivedMessage = snap.value as! [String: Any]
                            if receivedMessage["type"] != nil
                            {
                                let messageType = receivedMessage["type"] as! String
                                var type = MessageType.text
                                switch messageType
                                {
                                case "photo":
                                    type = .photo
                                case "audio":
                                    type = .audio
                                case "postedConfession":
                                    type = .postedConfession
                                    
                                default: break
                                }
                                let content = receivedMessage["content"] as! String
                                let fromID = receivedMessage["fromID"] as! String
                                let timestamp = receivedMessage["timestamp"] as! Double
                                let profilepic = receivedMessage["profilePicture"] as! String
                                let userName = receivedMessage["userName"] as! String
                                let userID = receivedMessage["UserID"] as! String
                                print("fromID = \(fromID)")
                                
                                var  typeOfUser = String()
                                
                                if receivedMessage["typeOfUser"] != nil
                                {
                                    typeOfUser = receivedMessage["typeOfUser"] as! String
                                }
                                
                                
                                var postedConfessionText = String()
                                if receivedMessage["confessionStatus"] != nil
                                {
                                    postedConfessionText = receivedMessage["confessionStatus"] as! String
                                }
                                else{
                                    postedConfessionText = ""
                                }
                                
                                
                                var postedConfessionImageType = String()
                                if receivedMessage["confessionImageType"] != nil
                                {
                                    postedConfessionImageType = receivedMessage["confessionImageType"] as! String
                                }
                                else{
                                    postedConfessionImageType = ""
                                }
                                
                                
                                var postedConfessionData = String()
                                if receivedMessage["confessionData"] != nil
                                {
                                    postedConfessionData = receivedMessage["confessionData"] as! String
                                }
                                else{
                                    postedConfessionData = ""
                                }
                                
                                var mySettingProfileInvisibility = String()
                                if receivedMessage["mySettingProfileInvisibility"] != nil
                                {
                                    mySettingProfileInvisibility = receivedMessage["mySettingProfileInvisibility"] as! String
                                }
                                else{
                                    mySettingProfileInvisibility = ""
                                }
                                
                                var auth_Token = String()
                                if receivedMessage["auth_Token"] != nil
                                {
                                    auth_Token = receivedMessage["auth_Token"] as! String
                                }
                                else{
                                    auth_Token = ""
                                }

                                
//                                let name = UserDefaults.standard.value(forKey: "UserName") as! String
//                                let picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
                                if fromID == currentUserID
                                {
                                    let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: userID,typeOfUser: typeOfUser,checkRevealProfile: getRevealStr,postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                    completion(message)
                                }
                                else
                                {
                                    let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: userID,typeOfUser: typeOfUser, checkRevealProfile: getRevealStr,postID: "",postedText: postedConfessionText,postedImageType: postedConfessionImageType,postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                    completion(message)
                                }
                            }
                            
                        }
                    })
                }
            })
        }
    }
    
    
    func downloadAudio(indexpathRow: Int, completion: @escaping (Bool, Int) -> Swift.Void)  {
        if self.type == .audio {
            let audioLink = self.content as! String
            let audioURL = URL.init(string: audioLink)
            URLSession.shared.dataTask(with: audioURL!, completionHandler: { (data, response, error) in
                if error == nil {
//                    self.image = UIImage.init(data: data!)
                    completion(true, indexpathRow)
                }
            }).resume()
        }
    }
    
    
    func downloadImage(indexpathRow: Int, completion: @escaping (Bool, Int) -> Swift.Void)  {
        if (self.type == .photo) || (self.type == .postedConfession) {
            let imageLink = self.content as! String
            let imageURL = URL.init(string: imageLink)
            URLSession.shared.dataTask(with: imageURL!, completionHandler: { (data, response, error) in
                if error == nil {
                    self.image = UIImage.init(data: data!)
                    completion(true, indexpathRow)
                }
            }).resume()
        }
    }
    
    func downloadUserProfileImage(indexpathRow: Int, completion: @escaping (Bool, Int) -> Swift.Void)  {
//        if (self.type == .photo) || (self.type == .postedConfession) {
            let imageLink = self.content as! String
            let imageURL = URL.init(string: imageLink)
            URLSession.shared.dataTask(with: imageURL!, completionHandler: { (data, response, error) in
                if error == nil {
                    self.image = UIImage.init(data: data!)
                    completion(true, indexpathRow)
                }
            }).resume()
//        }
    }
    
    class func markMessagesRead(forUserID: String, opponentVisibility: String, myVisibility: String)  {
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String  {
            //FIRAuth.auth()?.currentUser?.uid {
            
            var oppoIDWithVisibilityStr = String()
            oppoIDWithVisibilityStr = forUserID + "@" + opponentVisibility + "@" + myVisibility
            print("oppoIDWithVisibilityStr = \(oppoIDWithVisibilityStr)")
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(oppoIDWithVisibilityStr).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    FIRDatabase.database().reference().child("conversations").child(location).observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists() {
                            for item in snap.children {
                                let receivedMessage = (item as! FIRDataSnapshot).value as! [String: Any]
                                 if receivedMessage["fromID"] != nil
                                 {
                                    let fromID = receivedMessage["fromID"] as! String
                                    if fromID != currentUserID {
                                        FIRDatabase.database().reference().child("conversations").child(location).child((item as! FIRDataSnapshot).key).child("isRead").setValue(true)
                                   }
                                }
                            }
                        }
                    })
                }
            })
        }
    }
    
    
    class func markMessagesRead_AnonymousUser(forUserID: String)  {
        
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String  {
            //FIRAuth.auth()?.currentUser?.uid {
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    FIRDatabase.database().reference().child("conversations_anonymous").child(location).observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists() {
                            for item in snap.children {
                                let receivedMessage = (item as! FIRDataSnapshot).value as! [String: Any]
                                
                                if receivedMessage["fromID"] != nil
                                {
                                    let fromID = receivedMessage["fromID"] as! String
                                    if fromID != currentUserID {
                                        FIRDatabase.database().reference().child("conversations_anonymous").child(location).child((item as! FIRDataSnapshot).key).child("isRead").setValue(true)
                                    }
                                }
                            }
                        }
                    })
                }
            })
        }
    }
    
    
    /*class func markMessagesRead_InGroupChat()
    {
        if let currentUserID = FIRAuth.auth()?.currentUser?.uid
        {
            FIRDatabase.database().reference().child("GroupChat").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists()
                {
                    FIRDatabase.database().reference().child("GroupChat").observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists()
                        {
                            for item in snap.children {
                                let receivedMessage = (item as! FIRDataSnapshot).value as! [String: Any]
                                
                                if receivedMessage["fromID"] != nil
                                {
                                    let fromID = receivedMessage["fromID"] as! String
                                    if fromID != currentUserID
                                    {
                                        FIRDatabase.database().reference().child("GroupChat").child((item as! FIRDataSnapshot).key).child("isRead").setValue(true)
                                    }
                                }
                            }
                        }
                    })
                }
            })
        }
    }*/

    
   
    func downloadLastMessage(forLocation: String,opponentIDWithVisibility: String,getUserType: String, completion: @escaping (Void) -> Swift.Void) {
        
        print("opponentIDWithVisibility = \(opponentIDWithVisibility)")
        
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
//            FIRDatabase.database().reference().child("conversations").child(forLocation).observe(.value, with: { (snapshot) in
            
        FIRDatabase.database().reference().child("conversations").child(forLocation).keepSynced(true)
    FIRDatabase.database().reference().child("conversations").child(forLocation).observeSingleEvent(of: .value, with: { (snapshot) in
        
                if snapshot.exists() {
                    
                    for snap in snapshot.children
                    {
                        let receivedMessage = (snap as! FIRDataSnapshot).value as! [String: Any]
                        
                        print(receivedMessage)
                        
                        if receivedMessage["content"] != nil
                        {
                            self.content = receivedMessage["content"]!
                            self.timestamp = receivedMessage["timestamp"] as! Double
                            let messageType = receivedMessage["type"] as! String
                            let fromID = receivedMessage["fromID"] as! String
                            let toIDStr = receivedMessage["toID"] as! String
                            self.isRead = receivedMessage["isRead"] as! Bool

                            var  typeOfUser = String()
                            if getUserType != ""
                            {
                                typeOfUser = getUserType
                                self.typeOfUser = typeOfUser
                            }
                            else{
                                self.typeOfUser = "WithProfileUser"
                            }
                            
                            if receivedMessage["myConversationType"] != nil
                            {
                                self.myConversationType = receivedMessage["myConversationType"] as? String
                            }
                            else{
                                self.myConversationType = "withProfileChat"
                            }
                            
                            if receivedMessage["checkRevealProfile"] != nil{
                                self.getRevealProfileStr = receivedMessage["checkRevealProfile"] as? String
                            }
                            else
                            {
                                self.getRevealProfileStr =  "profileRevealed"
                            }
                            
                            if (receivedMessage["mySettingProfileInvisibility"] != nil)
                            {
                                self.myProfileInvisibilitySaved = receivedMessage["mySettingProfileInvisibility"] as? String
                            }
                            else{
                                self.myProfileInvisibilitySaved = "On"
                            }
                            
                            if (toIDStr == currentUserID)
                            {
                                print("************* toID == currentUserID *************")
                                
                                self.myProfileInvisibilitySaved = "Unknown"
                            }
                         
                            
                            var type = MessageType.text
                            switch messageType
                            {
                            case "text":
                                type = .text
                            case "photo":
                                type = .photo
                            case "audio":
                                type = .audio
                            case "postedConfession":
                                type = .postedConfession
                                
                            default: break
                            }
                            
                            self.type = type
                            if currentUserID == fromID
                            {
                                self.owner = .sender
                            }
                            else {
                                self.owner = .receiver
                            }
                            
                            completion()
                        }
                        else {
                            print("key is not present in array")
                        }
                    }
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
                }
            })
        }
    }
    
    
    
    func downloadLastMessage_AnonymousUser(forLocation: String,getUserType: String,getUserProfileRevealed: String, completion: @escaping (Void) -> Swift.Void)
    {
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
//            FIRDatabase.database().reference().child("conversations_anonymous").child(forLocation).observe(.value, with: { (snapshot) in
    FIRDatabase.database().reference().child("conversations_anonymous").child(forLocation).keepSynced(true)
    FIRDatabase.database().reference().child("conversations_anonymous").child(forLocation).observeSingleEvent(of: .value, with: { (snapshot) in
        
                if snapshot.exists()
                {
                    for snap in snapshot.children
                    {
                        let receivedMessage = (snap as! FIRDataSnapshot).value as! [String: Any]
                        print(receivedMessage)
                        
                        if receivedMessage["content"] != nil
                        {
                            self.content = receivedMessage["content"]!
                            self.timestamp = receivedMessage["timestamp"] as! Double
                            let messageType = receivedMessage["type"] as! String
                            let fromID = receivedMessage["fromID"] as! String
                            self.isRead = receivedMessage["isRead"] as! Bool
                            let toID = receivedMessage["toID"] as! String
                            
                            self.toID = receivedMessage["toID"] as? String
                            self.fromID = receivedMessage["fromID"] as? String
                            
                            let postID = receivedMessage["postID"] as! String
                    
                            if receivedMessage["confessionStatus"] != nil
                            {
                                self.postedText = receivedMessage["confessionStatus"] as? String
                            }
                            else
                            {
                                self.postedText = ""
                            }
                            
                            if receivedMessage["confessionImageType"] != nil
                            {
                                self.postedImageType = receivedMessage["confessionImageType"] as? String
                            }
                            else
                            {
                                self.postedImageType = ""
                            }
                            
                            
                            if receivedMessage["myConversationType"] != nil
                            {
                                self.myConversationType = receivedMessage["myConversationType"] as? String
                            }
                            else
                            {
                                self.myConversationType = "withAnonymousChat"
                            }
                            
                            if receivedMessage["confessionData"] != nil
                            {
                                self.postedConfessionCheck = receivedMessage["confessionData"] as? String
                            }
                            else{
                                self.postedConfessionCheck = ""
                            }
                            
                            
                            if receivedMessage["postID"] != nil{
                                self.postID = receivedMessage["postID"] as? String
                            }
                            
                            var  typeOfUser = String()
                            if getUserType != ""
                            {
                                typeOfUser = getUserType
                                self.typeOfUser = typeOfUser
                            }
                            else
                            {
                                self.typeOfUser = "WithProfileUser"
                            }
                            
                            if receivedMessage["checkRevealProfile"] != nil
                            {
                                self.getRevealProfileStr = receivedMessage["checkRevealProfile"] as? String
                            }
                            else
                            {
                                self.getRevealProfileStr =  "profileRevealed"
                            }
                            
                            if (fromID == currentUserID)
                            {
                                if receivedMessage["mySettingProfileInvisibility"] != nil
                                {
                                    self.myProfileInvisibilitySaved = receivedMessage["mySettingProfileInvisibility"] as? String
                                }
                                else{
                                    self.myProfileInvisibilitySaved = "On"
                                }
                            }
                            else{
                                self.myProfileInvisibilitySaved = "Unknown"
                                self.userID = receivedMessage["toID"] as? String
                            }
                            

                                if receivedMessage["myPackagesStatus"] != nil{
                                    self.myPackagesStatus = receivedMessage["myPackagesStatus"] as? String
                                }
                                else
                                {
                                    self.myPackagesStatus =  "profileVisibilityPurchased"
                                }
                                
                                
                            /*   if receivedMessage["myRevealProfileStatus"] != nil{
                                    self.myRevealProfileStatus = receivedMessage["myRevealProfileStatus"] as? String
                                }
                                else
                                {
                                    self.myRevealProfileStatus =  "profileRevealed"
                                }*/
                            
                            print("getUserProfileRevealed = \(getUserProfileRevealed)")
                            
                            self.myRevealProfileStatus = getUserProfileRevealed

                                if receivedMessage["opponentPackageStatus"] != nil
                                {
                                    self.opponentPackageStatus = receivedMessage["opponentPackageStatus"] as? String
                                }
                                else
                                {
                                    self.opponentPackageStatus =  "profileVisibilityPurchased"
                                }
                            
                                print("getUserProfileRevealed = \(getUserProfileRevealed)")
                            
                                if receivedMessage["opponentRevealProfileStatus"] != nil
                                {
                                    self.opponentRevealProfileStatus = receivedMessage["opponentRevealProfileStatus"] as? String
                                }
                                else
                                {
                                    self.opponentRevealProfileStatus =  "profileRevealed"
                                }
                            
                            var type = MessageType.text
                            switch messageType
                            {
                            case "text":
                                type = .text
                            case "photo":
                                type = .photo
                            case "audio":
                                type = .audio
                            case "postedConfession":
                                type = .postedConfession
                                
                            default: break
                            }
                            
                            self.type = type
                            if currentUserID == fromID
                            {
                                self.owner = .sender
                            }
                            else {
                                self.owner = .receiver
                            }
                            
                        print("fromID = \(fromID)")
                        print("toID = \(toID)")
                        print("currentUserID = \(currentUserID)")
                            
                        var oppoIDWithPostID = String()
                        var userIDStr = String()
                        if (toID == currentUserID)
                        {
                            oppoIDWithPostID = toID + "@" + postID
                            userIDStr = fromID
                        }
                        else
                        {
                            oppoIDWithPostID = fromID + "@" + postID
                            userIDStr = toID
                        }
                            
                        print("userIDStr = \(userIDStr)")
                        print("oppoIDWithPostID = \(oppoIDWithPostID)")
                        print("toID = \(toID)")
//                        FIRDatabase.database().reference().child("Users").child(userIDStr).child("conversations_anonymous").child(oppoIDWithPostID).observe(.value, with: { (snapshot) in
                            
    FIRDatabase.database().reference().child("Users").child(userIDStr).child("conversations_anonymous").child(oppoIDWithPostID).keepSynced(true)
    FIRDatabase.database().reference().child("Users").child(userIDStr).child("conversations_anonymous").child(oppoIDWithPostID).observeSingleEvent(of: .value, with: { (snapshot) in
                                
                                if snapshot.exists() {
                                    print("snapshot exists")
                                    print("oppoIDWithPostID = \(oppoIDWithPostID)")
                                    print("snapshot exists = \(String(describing: snapshot.value))")
                                    let values = snapshot.value as! [String: String]
                                    
                                    if values["checkRevealProfile"] != nil
                                    {
                                      self.opponentRevealProfileStatus = "profileRevealed"
                                    }
                                    else
                                    {
                                       self.opponentRevealProfileStatus = "profileNotRevealed"
                                    }
                                }
                                else
                                {
                                    print("snapshot doesnot exists")
                                    self.opponentRevealProfileStatus = "profileNotRevealed"
                                }
                            
                                completion()
                            })
     
                        }
                        else {
                            print("key is not present in array")
                        }
                    }
                }
                else
                {
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
                }
            })
        }
    }
    
    
    class func sendMessage_AnonymousUser(postID: String,message: Message, typeOfUser: String, toID: String,opponentProfileVisibilityStatus: String,myProfileVisibilityStatus: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let name = UserDefaults.standard.value(forKey: "userName") as! String
        
        let UserAuthID = UserDefaults.standard.value(forKey: "firebase_user_id") as! String
        
        var picture = String()
        if let profile_pic_thumb = UserDefaults.standard.value(forKey: "profile_pic_thumb") as? String
        {
            if profile_pic_thumb != ""
            {
                picture = profile_pic_thumb
            }
            else{
                picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
            }
        }
        else{
            picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
        }
        
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            //FIRAuth.auth()?.currentUser?.uid
        {
            switch message.type
            {
            case .audio:
                let audioData = message.content as! NSURL
                let child = UUID().uuidString
                FIRStorage.storage().reference().child("messageAudio").child(child).putFile(audioData as URL, metadata: nil, completion: { (metadata, error) in
                    if error == nil
                    {
                        let revealStr: String = message.getRevealProfileStr!
                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["type": "audio", "content": path!, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"postID": postID,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]
                        
                        
                        Message.uploadMessage_AnonymousUser(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID,myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                            completion(status)
                        })
                    }
                })
            case .photo:
                let imageData = UIImageJPEGRepresentation((message.content as! UIImage), 1.0)
                let child = UUID().uuidString
                FIRStorage.storage().reference().child("messagePics").child(child).put(imageData!, metadata: nil, completion: { (metadata, error) in
                    if error == nil
                    {
                       let revealStr: String = message.getRevealProfileStr!
                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["type": "photo", "content": path!, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"postID": postID,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]
//                        Message.uploadMessage_AnonymousUser(withValues: values, toID: toID,typeOfUser: typeOfUser, postID: postID,completion: { (status) in
//                            completion(status)
//                        })
                        
                        Message.uploadMessage_AnonymousUser(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID,myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                            completion(status)
                        })
                    }
                })
            case .text:
                let revealStr: String = message.getRevealProfileStr!
                let values = ["type": "text", "content": message.content, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"postID": postID,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!]
//                Message.uploadMessage_AnonymousUser(withValues: values, toID: toID,typeOfUser: typeOfUser , postID: postID,completion: { (status) in
//                    completion(status)
//                })
                
                Message.uploadMessage_AnonymousUser(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID,myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                    completion(status)
                })
                
            case .postedConfession:
                
                let imageData = message.content as! String//UIImageJPEGRepresentation((message.content as! FLAnimatedImage), 0.5)
                print("imageData = \(imageData)")
                
                if imageData != ""
                {
                    if message.postedImageType == "photo"
                    {
                        let revealStr: String = message.getRevealProfileStr!
//                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["type": "postedConfession", "content": imageData, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"postID": postID , "confessionStatus": message.postedText!, "confessionImageType": message.postedImageType!, "confessionData": message.postedConfessionCheck!,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]
                        
//                        Message.uploadMessage_AnonymousUser(withValues: values, toID: toID,typeOfUser: typeOfUser, postID: postID,completion: { (status) in
//                            completion(status)
//                        })
                        
                        Message.uploadMessage_AnonymousUser(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID,myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                            completion(status)
                        })
                    }
                    else if message.postedImageType == "gif"
                    {
                        let revealStr: String = message.getRevealProfileStr!
                        let values = ["type": "postedConfession", "content": imageData, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"postID": postID , "confessionStatus": message.postedText!, "confessionImageType": message.postedImageType!, "confessionData": message.postedConfessionCheck!,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]
                        
                        Message.uploadMessage_AnonymousUser(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID,myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                            completion(status)
                        })
                    }
                }
                else{
                    let revealStr: String = message.getRevealProfileStr!
                    let values = ["type": "postedConfession", "content": "", "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"postID": postID , "confessionStatus": message.postedText!, "confessionImageType": message.postedImageType!, "confessionData": message.postedConfessionCheck!,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]

                    Message.uploadMessage_AnonymousUser(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID,myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                        completion(status)
                    })
                }
            }
        }
    }
    
    
    class func uploadMessage_AnonymousUser(withValues: [String: Any], toID: String,typeOfUser: String, postID : String,myProfileVisbilityStatusStr: String,opponentProfileVisibilityStatusStr: String ,completion: @escaping (Bool) -> Swift.Void) {
        
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String  {
            //FIRAuth.auth()?.currentUser?.uid {
            
            let thread1 = currentUserID + "@" + postID
            let thread = toID + "@" + postID
            
            print("typeOfUser while uploading a message = \(typeOfUser)")
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(thread).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    let data1 = ["userType": typeOfUser]
                    
                   
                FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(thread).updateChildValues(data1)
                    FIRDatabase.database().reference().child("conversations_anonymous").child(location).childByAutoId().setValue(withValues, withCompletionBlock: { (error, _) in
                        if error == nil {
                            completion(true)
                        } else {
                            completion(false)
                        }
                    })
                }
                else
                {
                  FIRDatabase.database().reference().child("conversations_anonymous").childByAutoId().childByAutoId().setValue(withValues, withCompletionBlock: { (error, reference) in
                        
                     var data = [String : String]()
                    
                    if (typeOfUser == "AnonymousUser")
                    {
                        data = ["location": reference.parent!.key, "userType": typeOfUser]
                    }
                    else{
                        data = ["location": reference.parent!.key, "userType": typeOfUser,"checkRevealProfile": "profileRevealed"]
                    }
                    
                       FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(thread).updateChildValues(data)
                       FIRDatabase.database().reference().child("Users").child(toID).child("conversations_anonymous").child(thread1).updateChildValues(data)
                        
                        completion(true)
                    })
                }
            })
        }
    }
    
    
    class func send(postID: String,message: Message, typeOfUser: String,opponentProfileVisibilityStatus: String,myProfileVisibilityStatus: String, toID: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let name = UserDefaults.standard.value(forKey: "userName") as! String
//        let picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
        let UserAuthID = UserDefaults.standard.value(forKey: "firebase_user_id") as! String
        
        var picture = String()
        if let profile_pic_thumb = UserDefaults.standard.value(forKey: "profile_pic_thumb") as? String
        {
            if profile_pic_thumb != ""
            {
                picture = profile_pic_thumb
            }
            else{
                picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
            }
        }
        else{
            picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
        }
        
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String  //FIRAuth.auth()?.currentUser?.uid
        {
            switch message.type
            {
            case .audio:

                let audioData = message.content as! NSURL
                let child = UUID().uuidString
                FIRStorage.storage().reference().child("messageAudio").child(child).putFile(audioData as URL, metadata: nil, completion: { (metadata, error) in
                    if error == nil
                    {
                         let revealStr: String = message.getRevealProfileStr!
                        
                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["type": "audio", "content": path!, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]
                        
                        Message.uploadMessage(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID, myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                            completion(status)
                        })
                    }
                })
            case .photo:
                let imageData = UIImageJPEGRepresentation((message.content as! UIImage), 1.0)
                let child = UUID().uuidString
                FIRStorage.storage().reference().child("messagePics").child(child).put(imageData!, metadata: nil, completion: { (metadata, error) in
                    if error == nil
                    {
                         let revealStr: String = message.getRevealProfileStr!
                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["type": "photo", "content": path!, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]

                        Message.uploadMessage(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID, myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                            completion(status)
                        })
                    }
                })
                
            case .text:
                
                let revealStr: String = message.getRevealProfileStr!
                let values = ["type": "text", "content": message.content, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser , "checkRevealProfile": revealStr,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!]

                Message.uploadMessage(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID, myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                    completion(status)
                  })
                
            case .postedConfession:
                print("postedConfession")
                
                let imageData = message.content as! String
                print("imageData = \(imageData)")
                
                if imageData != ""
                {
                    if message.postedImageType == "photo"
                    {
                        let revealStr: String = message.getRevealProfileStr!
                        let values = ["type": "postedConfession", "content": imageData, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"postID": postID , "confessionStatus": message.postedText!, "confessionImageType": message.postedImageType!, "confessionData": message.postedConfessionCheck!,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]
                 
                        Message.uploadMessage(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID, myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                            completion(status)
                        })
                    }
                    else if message.postedImageType == "gif"
                    {
                        let revealStr: String = message.getRevealProfileStr!
                        let values = ["type": "postedConfession", "content": imageData, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"postID": postID , "confessionStatus": message.postedText!, "confessionImageType": message.postedImageType!, "confessionData": message.postedConfessionCheck!,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]
                        
                        Message.uploadMessage(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID, myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                            completion(status)
                        })
                    }
                }
                else{
                    let revealStr: String = message.getRevealProfileStr!
                    let values = ["type": "postedConfession", "content": "", "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID, "typeOfUser": typeOfUser, "checkRevealProfile": revealStr,"postID": postID , "confessionStatus": message.postedText!, "confessionImageType": message.postedImageType!, "confessionData": message.postedConfessionCheck!,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "myConversationType": (message.myConversationType)!, "myRevealProfileStatus": (message.myRevealProfileStatus)!, "opponentRevealProfileStatus": (message.opponentRevealProfileStatus)!, "myPackagesStatus": (message.myPackagesStatus)!, "opponentPackageStatus": (message.opponentPackageStatus)!] as [String : Any]
                    
                    Message.uploadMessage(withValues: values, toID: toID, typeOfUser: typeOfUser, postID: postID, myProfileVisbilityStatusStr: myProfileVisibilityStatus, opponentProfileVisibilityStatusStr: opponentProfileVisibilityStatus, completion: { (status) in
                        completion(status)
                    })
                }
            }
        }
    }
    
    class func uploadMessage(withValues: [String: Any], toID: String,typeOfUser: String, postID : String, myProfileVisbilityStatusStr: String,opponentProfileVisibilityStatusStr: String, completion: @escaping (Bool) -> Swift.Void) {
        
        if let currentUserID =  UserDefaults.standard.value(forKey: "firebase_user_id") as? String  {
            //FIRAuth.auth()?.currentUser?.uid {
        
            var opponentIdWithVisibilityStr = String()
            opponentIdWithVisibilityStr = toID + "@" + opponentProfileVisibilityStatusStr
            
            var currentIdWithVisibilityStr = String()
            currentIdWithVisibilityStr = currentUserID + "@" + myProfileVisbilityStatusStr
            
            let myAndOpponentStatus = opponentIdWithVisibilityStr + "@" + myProfileVisbilityStatusStr
            
            let oppoAndMyStatus = currentIdWithVisibilityStr + "@" + opponentProfileVisibilityStatusStr
            
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(myAndOpponentStatus).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    
                    let data = snapshot.value as! [String: String]
                    
                    let location = data["location"]!
                    
                    let data1 = ["userType": typeOfUser,"myProfileVisibilityStatus": currentIdWithVisibilityStr, "opponentProfileVisibilityStatus": opponentIdWithVisibilityStr]
                    
                    FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(myAndOpponentStatus).updateChildValues(data1)
                    FIRDatabase.database().reference().child("conversations").child(location).childByAutoId().setValue(withValues, withCompletionBlock: { (error, _) in
                        if error == nil {
                            completion(true)
                        } else {
                            completion(false)
                        }
                    })
                }
                else
                {
                    FIRDatabase.database().reference().child("conversations").childByAutoId().childByAutoId().setValue(withValues, withCompletionBlock: { (error, reference) in
                        
                        let data = ["location": reference.parent!.key, "userType": typeOfUser,"myProfileVisibilityStatus": currentIdWithVisibilityStr, "opponentProfileVisibilityStatus": opponentIdWithVisibilityStr]
                        
                         let data22 = ["location": reference.parent!.key, "userType": typeOfUser,"myProfileVisibilityStatus": opponentIdWithVisibilityStr, "opponentProfileVisibilityStatus": currentIdWithVisibilityStr]
                        
                        FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(myAndOpponentStatus).updateChildValues(data)
                        
                        FIRDatabase.database().reference().child("Users").child(toID).child("conversations").child(oppoAndMyStatus).updateChildValues(data22)
                        
                        completion(true)
                    })
                }
            })
        }
    }
    
    

     class func sendMessage_InGroupChat(message: Message, completion: @escaping (Bool) -> Swift.Void)
    {
        let name = UserDefaults.standard.value(forKey: "userName") as! String
        let UserAuthID = UserDefaults.standard.value(forKey: "firebase_user_id") as! String
        
        var picture = String()
        if let profile_pic_thumb = UserDefaults.standard.value(forKey: "profile_pic_thumb") as? String
        {
            if profile_pic_thumb != ""
            {
                picture = profile_pic_thumb
            }
            else{
                picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
            }
        }
        else{
            picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
        }
        
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String         //FIRAuth.auth()?.currentUser?.uid
        {
            let firebaseTimestamp = FIRServerValue.timestamp()
            print("firebaseTimestamp = \(firebaseTimestamp)")
            
            switch message.type
            {
            case .audio:
                let audioData = message.content as! NSURL
                let child = UUID().uuidString
                FIRStorage.storage().reference().child("groupChatAudio").child(child).putFile(audioData as URL, metadata: nil, completion: { (metadata, error) in
                    if error == nil
                    {
                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["type": "audio", "content": path!, "fromID": currentUserID, "timestamp": firebaseTimestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "auth_Token": (message.auth_Token)!, "serverID": (message.serverID)!] as [String : Any]
                        Message.uploadMessage_InGroupChat(withValues: values, completion: { (status) in
                            completion(status)
                        })
                    }
                })
            case .photo:
                let imageData = UIImageJPEGRepresentation((message.content as! UIImage), 1.0)
                let child = UUID().uuidString
                FIRStorage.storage().reference().child("groupChatPics").child(child).put(imageData!, metadata: nil, completion: { (metadata, error) in
                    if error == nil
                    {
                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["type": "photo", "content": path!, "fromID": currentUserID, "timestamp": firebaseTimestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "auth_Token": (message.auth_Token)!, "serverID": (message.serverID)!] as [String : Any]
                        Message.uploadMessage_InGroupChat(withValues: values, completion: { (status) in
                            completion(status)
                        })
                    }
                })
            case .text:
                 let values = ["type": "text", "content": message.content, "fromID": currentUserID, "timestamp": firebaseTimestamp, "isRead": false, "profilePicture": picture, "userName": name, "UserID": UserAuthID,"mySettingProfileInvisibility": (message.myProfileInvisibilitySaved)!, "auth_Token": (message.auth_Token)!, "serverID": (message.serverID)!]
                Message.uploadMessage_InGroupChat(withValues: values, completion: { (status) in
                    completion(status)
                })
             
            case .postedConfession:
                print("postedConfession")
            }
        }
    }
    

    class func uploadMessage_InGroupChat(withValues: [String: Any], completion: @escaping (Bool) -> Swift.Void)
    {
        print("withValues = \(withValues)")
//         if let currentUserID = FIRAuth.auth()?.currentUser?.uid
        
//        if (FIRAuth.auth()?.currentUser?.uid) != nil {
//        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
//        {
        FIRDatabase.database().reference().child("GroupChat").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists()
                {
                    FIRDatabase.database().reference().child("GroupChat").childByAutoId().setValue(withValues, withCompletionBlock: { (error, _) in
                        if error == nil {
                            completion(true)
                        } else {
                            completion(false)
                        }
                    })
                }
                else
                {
                    FIRDatabase.database().reference().child("GroupChat").childByAutoId().setValue(withValues, withCompletionBlock: { (error, _) in
                        if error == nil {
                            completion(true)
                        } else {
                            completion(false)
                        }
                    })
                }
            })
//        }
    }
    
    
    
    //MARK: Methods
    class func downloadAllMessagesAfterChildRemoved_InGroupChat(completion: @escaping (Message) -> Swift.Void) {
        if let currentUserID =  UserDefaults.standard.value(forKey: "UserAuthID") as? String{
            
        //FIRAuth.auth()?.currentUser?.uid {
        FIRDatabase.database().reference().child("GroupChat").observe(.value, with: { (snapshot) in
            
                if snapshot.exists()
                {
                    FIRDatabase.database().reference().child("GroupChat").observe(.value, with: { (snap) in
                        if snap.exists()
                        {
                            let receivedMessage = snap.value as! [String: Any]
                            if receivedMessage["type"] != nil
                            {
                                let messageType = receivedMessage["type"] as! String
                                var type = MessageType.text
                                switch messageType
                                {
                                case "photo":
                                    type = .photo
                                case "audio":
                                    type = .audio
                                case "postedConfession":
                                    type = .postedConfession
                                    
                                default: break
                                }
                                let content = receivedMessage["content"] as! String
                                let fromID = receivedMessage["fromID"] as! String
                                let timestamp = receivedMessage["timestamp"] as! Double
                                let profilepic = receivedMessage["profilePicture"] as! String
                                let userName = receivedMessage["userName"] as! String
                                let userID = receivedMessage["UserID"] as! String
                                print("fromID = \(fromID)")
                                
                                var postedConfessionText = String()
                                if receivedMessage["confessionStatus"] != nil
                                {
                                    postedConfessionText = receivedMessage["confessionStatus"] as! String
                                }
                                else
                                {
                                    postedConfessionText = ""
                                }
                                
                                
                                var postedConfessionImageType = String()
                                if receivedMessage["confessionImageType"] != nil
                                {
                                    postedConfessionImageType = receivedMessage["confessionImageType"] as! String
                                }
                                else{
                                    postedConfessionImageType = ""
                                }
                                
                                
                                var postedConfessionData = String()
                                if receivedMessage["confessionData"] != nil
                                {
                                    postedConfessionData = receivedMessage["confessionData"] as! String
                                }
                                else{
                                    postedConfessionData = ""
                                }

                                var mySettingProfileInvisibility = String()
                                if receivedMessage["mySettingProfileInvisibility"] != nil
                                {
                                    mySettingProfileInvisibility = receivedMessage["mySettingProfileInvisibility"] as! String
                                }
                                else{
                                    mySettingProfileInvisibility = ""
                                }
                                
                                var auth_Token = String()
                                if receivedMessage["auth_Token"] != nil
                                {
                                    auth_Token = receivedMessage["auth_Token"] as! String
                                }
                                else{
                                    auth_Token = ""
                                }
                                
                                if fromID == currentUserID
                                {
                                    let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: userID,typeOfUser: "",checkRevealProfile: "",postID: "", postedText: postedConfessionText, postedImageType: postedConfessionImageType, postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                    completion(message)
                                }
                                else
                                {
                                    let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, profilePicture: profilepic, userName: userName,userID: userID,typeOfUser: "", checkRevealProfile: "",postID: "", postedText: postedConfessionText,postedImageType: postedConfessionImageType, postedConfessionCheck: postedConfessionData, myProfileInvisibilitySavedStr: mySettingProfileInvisibility, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: "",opponentRevealProfileStatusStr: "", myPackagesStatusStr: "", opponentPackageStatusStr: "", auth_Token: auth_Token, serverID: "")
                                    
                                    completion(message)
                                }
                            }
                        }
                    })
                }
            })
        }
    }
    
    

  
    //MARK: Inits
    init(type: MessageType, content: Any, owner: MessageOwner, timestamp: Double, isRead: Bool, profilePicture: String, userName: String,userID : String,typeOfUser : String,checkRevealProfile : String,postID: String,postedText: String,postedImageType: String,postedConfessionCheck: String,myProfileInvisibilitySavedStr: String, myConvoversationTypeStr: String, myRevealProfileStatusStr: String, opponentRevealProfileStatusStr: String, myPackagesStatusStr: String, opponentPackageStatusStr: String,auth_Token: String,serverID: String) {
        self.type = type
        self.content = content
        self.owner = owner
        self.timestamp = timestamp
        self.isRead = isRead
        self.profilePicture = profilePicture
        self.userName = userName
        self.userID = userID
        self.typeOfUser = typeOfUser
        self.getRevealProfileStr = checkRevealProfile
        self.postID = postID
        self.postedText = postedText
        self.postedImageType = postedImageType
        self.postedConfessionCheck = postedConfessionCheck
        self.myProfileInvisibilitySaved = myProfileInvisibilitySavedStr
        self.myConversationType = myConvoversationTypeStr
        self.myRevealProfileStatus = myRevealProfileStatusStr
        self.opponentRevealProfileStatus = opponentRevealProfileStatusStr
        self.myPackagesStatus = myPackagesStatusStr
        self.opponentPackageStatus = opponentPackageStatusStr
        self.auth_Token = auth_Token
        self.serverID = serverID
    }
}
