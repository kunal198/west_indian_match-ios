//
//  WiCreateConfessionViewController.swift
//  Wi Match
//
//  Created by brst on 08/11/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Photos
import SDWebImage
import MobileCoreServices
import AssetsLibrary
import AVFoundation
import FLAnimatedImage
import GiphyCoreSDK
import CoreLocation

class WiCreateConfessionViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIPopoverPresentationControllerDelegate,ImageCropViewControllerDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,CLLocationManagerDelegate
{
    //MARK:- ***** VARIABLES DECLARATION *****
    var getGifURl = String()
    var widthOfGif = Int()
    var heightOfGif = Int()
    var searchText = String()
    var myLocationStr = String()
    var myStateStr = String()
    var getDeviceTokenOfOwner = String()
    var timeFrameSelected = String()
    var typeOfPost = String()
    var defaultImagesArray = [String]()
    var ImagesArray = [String]()
    var imageSelectedForPost = UIImageView()
    var backgroundImageView = UIImageView()
    var selectedIndexPath: NSIndexPath = NSIndexPath()
    var selectBool = Bool()
    var selectedRow = Int()
    var customImageView = UIImageView()
    var currentUserGender = String()
    var postTypeStr = String()
    var currentPostIDStr = String()
    var currentReplyIDStr = String()
    var getReplyCountStr = String()
    var directPickImageBool = Bool()
    var userOwnerIDStr = String()
    var ownerProfileVisibility_postComments = String()
    var postIdOwner = String()
    var selectedHexColor = String()
    var getImageType = String()
    var badgeCount = Int()
    var getUserStatus = String()
    var getReplyOnReplyId = String()
    var photoUploadType = String()
    var differentiateStrForGifs = String()
    var gallaryTypeStr = String()
    var postedUserInfoStr = String()
    var postedUserLocationStr = String()
    var postIDStr = String()
    var gifImageData = NSData()
    var selectedMoodTypeImageStr = String()
    var typeOfConfessionSelected = String()
    var mediaType = String()
    var media_uploadedFrom = String()
    var typeOFReplyStr = String()
    var getHeightOfKeyBoard = CGFloat()
    var timeFrameArray = [String]()
    var checkedTimeFrameArray = [String]()
    var selectedRowForTimeFrame = Int()
    var selectBoolForTimeFrame = Bool()
    var selectedIndexPathForTimeFrame: NSIndexPath = NSIndexPath()
    var gifImageChangedStr = String()
    var gifArray = NSMutableArray()
    var offset = Int()
    var searchOffset = Int()
    var moreScroll = Bool()
    
    var currentLatitude = Double()
    var currentLongitude = Double()
    var localityStr = String()
    var subLocalityStr = String()
    
    var locationManager: CLLocationManager!
    
    var autoKeyForRepliesStr = String()
    
    var currentLatitudeFromHomePage = Double()
    var currentLongitudeFromHomePage = Double()
    var currentLocationFromHomePage = String()
    
    
    var country = String()
    var state = String()
    var currentCity = String()
    var LocationStr = String()
    
    
    // Update(Edit) Confession Variables
    var updateStatusText = String()
    var updateBackgroundImageUrl = String()
    var updateTimeframe = String()
    var updatePhotoTypeStr = String()
    var updateMoodTypeStr = String()
    var updateMoodUrl = String()
    var updateStr = String()
    var updatePostID = String()
    var updateReplyID = String()
    var updateReplyToReplyID = String()
    var differentiateString = String()
    var updateLikeCountStr = String()
    var updateReplyCountStr = String()
    var updateGifHeight = Int()
    var updateGifWidth = Int()
    
    var croppedImageWidth = Int64()
    var croppedImageheight = Int64()
    
    var videoPath = NSURL()
    var videoPlayer = AVPlayer()
    var videoURL = NSURL()
    
    
    //MARK:- ***** OUTLETS DECLARATION *****
    @IBOutlet weak var searchMainView: UIView!
    @IBOutlet weak var wrapperButtonsView: UIView!
    @IBOutlet weak var noGifAvailableView: UIView!
    @IBOutlet weak var giphyCollectionView: UICollectionView!
    @IBOutlet weak var giphyImagesView: UIView!
    @IBOutlet weak var TitleConfessionLbl: UILabel!
    @IBOutlet weak var wrapperBackImageView: UIView!
    @IBOutlet weak var selectedBackgroundImage: UIImageView!
    @IBOutlet weak var placeHolderTextView: UILabel!
    @IBOutlet weak var moodCollectionView: UICollectionView!
    @IBOutlet weak var wrapperScrollView: UIScrollView!
    @IBOutlet weak var selectedAnimatedGifImage: FLAnimatedImageView!
    @IBOutlet weak var wrapperDescriptionView: UIView!
    @IBOutlet weak var savePostView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var titlePostBtn_lbl: UILabel!
    @IBOutlet weak var addDescription_txtView: UITextView!
    @IBOutlet weak var selectTimeFrameView1: UIView!
    @IBOutlet weak var timeFrameCollectionView: UICollectionView!
    @IBOutlet weak var loadMoreBtn: UIButton!
    @IBOutlet weak var searchGifView: UIView!
    @IBOutlet weak var searchGif_TxtField: UITextField!
    
    
    //MARK:- ****** viewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
//        print("self.postIdOwner = \(self.postIdOwner)")
        
        self.userOwnerIDStr = self.postIdOwner

        self.setDoneOnKeyboard()
        
        self.searchMainView.layer.cornerRadius = 15
        self.searchMainView.layer.masksToBounds = false
        
        self.getGifURl = ""
        self.widthOfGif = 100
        self.heightOfGif = 100
        
        self.offset = 0
        self.searchOffset = 0
        self.gifImageChangedStr = ""
        self.giphyImagesView.isHidden = true
        self.differentiateStrForGifs = "allGifs"
        self.searchGifView.isHidden = true
        self.searchGif_TxtField.attributedPlaceholder = NSAttributedString(string: "Search by name",
                                                                        attributes: [NSForegroundColorAttributeName: UIColor.white])
        
       /* let giphy = GPHClient.init(apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B")

        /// Gif Search
        let op = giphy.search("emotions") { (response, error) in
            
            if let error = error as NSError? {
                // Do what you want with the error
            }
            
            if let response = response, let data = response.data, let pagination = response.pagination {

                for result in data {
                }
            } else {
                print("No Results Found")
            }
        }*/
        
       
        self.getGifs(gifType: "applause+actions+awww+celebrities", apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25,pageOffset: self.offset)
        
        var frame = self.addDescription_txtView.frame
        frame.size.height = self.addDescription_txtView.contentSize.height + 15
        self.addDescription_txtView.frame = frame
        
        self.selectTimeFrameView1.isHidden = true

        self.wrapperScrollView.isScrollEnabled=true
        self.wrapperBackImageView.isHidden = true
        self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
        self.wrapperScrollView.contentSize.height = self.view.frame.size.height + 100
        
        self.timeFrameCollectionView.flashScrollIndicators()
        self.moodCollectionView.flashScrollIndicators()
        
        self.gallaryTypeStr = ""
        
        self.selectedMoodTypeImageStr = ""
        self.photoUploadType = ""
        self.selectedRowForTimeFrame = 6
        self.selectBoolForTimeFrame = true
//        print("selectedRowForTimeFrame= \(self.selectedRowForTimeFrame)")
        let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
        selectedIndexPathForTimeFrame = indexPath as NSIndexPath
        self.timeFrameCollectionView.reloadData()
        
        self.selectBool = false
        self.directPickImageBool = false

        self.addDescription_txtView.text = ""
        self.addDescription_txtView.backgroundColor = UIColor.clear
        
        self.getImageType = ""
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        self.timeFrameSelected = "24 hrs"
        
        self.ImagesArray = ["moodIcon-1","moodIcon-2","moodIcon-5","moodIcon-3","moodIcon-4"]
        
    self.timeFrameArray = ["1 hr","4 hrs","8 hrs","12 hrs","16 hrs","20 hrs","24 hrs"]
    self.checkedTimeFrameArray = ["Radio unfilled say","Radio unfilled say","Radio unfilled say","Radio unfilled say","Radio unfilled say","Radio unfilled say","Radio Filled Say"]

        if self.postTypeStr == "CreateNewPost"
        {
            self.titlePostBtn_lbl.text = "save and confess"
             self.TitleConfessionLbl.text = "What's your confession?"
            self.selectTimeFrameView1.isHidden = true
            self.widthOfGif = 100
            self.heightOfGif = 100
        }
        else if self.postTypeStr == "ForReplyOnPost"
        {
            self.typeOFReplyStr = "replyOnPost"
            
            self.titlePostBtn_lbl.text = "Reply On Post"
            self.TitleConfessionLbl.text = "Reply"
            self.selectTimeFrameView1.isHidden = true
            self.savePostView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 30
            self.widthOfGif = 100
            self.heightOfGif = 100
        }
        else if self.postTypeStr == "ForReplyOnReply"
        {
            self.typeOFReplyStr = "replyOnReply"
            self.TitleConfessionLbl.text = "Reply"
            self.titlePostBtn_lbl.text = "Reply On Post"
            self.selectTimeFrameView1.isHidden = true
            self.savePostView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 30
            self.widthOfGif = 100
            self.heightOfGif = 100
        }
        
        if updateStr == "EditConfession"
        {
            self.widthOfGif = self.updateGifWidth
            self.heightOfGif = self.updateGifHeight
            
            if self.updateStatusText == ""
            {
                self.placeHolderTextView.isHidden = false
            }
            else
            {
                self.placeHolderTextView.isHidden = true
            }
            
            self.addDescription_txtView.dataDetectorTypes = UIDataDetectorTypes.link
            let inputString: NSString = self.updateStatusText as NSString
            let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
            let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
            
            self.addDescription_txtView.text = modifiedString
            
            let fixedWidth = self.addDescription_txtView.frame.size.width
            self.addDescription_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.addDescription_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.addDescription_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.addDescription_txtView.frame = newFrame;
            self.photoUploadType = self.updatePhotoTypeStr
            if self.updatePhotoTypeStr == "photo"
            {
                self.selectedAnimatedGifImage.isHidden = true
                self.selectedBackgroundImage.isHidden = false
                
                self.backgroundImageView.sd_setImage(with: URL(string: self.updateBackgroundImageUrl), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)

                self.selectedBackgroundImage.sd_setImage(with: URL(string: self.updateBackgroundImageUrl), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                
                self.wrapperBackImageView.isHidden = false
                
                self.wrapperScrollView.isScrollEnabled=true
                
                self.selectedBackgroundImage.layer.borderWidth = 2.0
                self.selectedBackgroundImage.layer.masksToBounds = false
                self.selectedBackgroundImage.layer.borderColor = UIColor.lightGray.cgColor
                
                self.backgroundImageView.layer.borderWidth = 2.0
                self.backgroundImageView.layer.masksToBounds = false
                self.backgroundImageView.layer.borderColor = UIColor.lightGray.cgColor
                
                self.wrapperBackImageView.frame.size.width = self.view.frame.size.width - 10
                self.wrapperBackImageView.frame.size.height = self.view.frame.size.width - 10
                self.backgroundImageView.frame.size.width = self.view.frame.size.width
                self.backgroundImageView.frame.size.height = self.view.frame.size.width

                self.selectTimeFrameView1.isHidden = true
                
                self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + newSize.height + 5
                
                self.wrapperButtonsView.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
                
                self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + 50
            }
            else if self.updatePhotoTypeStr == "gif"
            {
                self.gifImageChangedStr = ""
                self.selectedAnimatedGifImage.isHidden = false
                self.selectedBackgroundImage.isHidden = true
                
                self.selectedAnimatedGifImage.layer.borderWidth = 2.0
                self.selectedAnimatedGifImage.layer.masksToBounds = false
                self.selectedAnimatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                
                self.wrapperBackImageView.frame.size.width = self.view.frame.size.width - 10
                self.wrapperBackImageView.frame.size.height = self.view.frame.size.width - 10
                self.backgroundImageView.frame.size.width = self.view.frame.size.width
                self.backgroundImageView.frame.size.height = self.view.frame.size.width
                
                 self.backgroundImageView.sd_setImage(with: URL(string: self.updateBackgroundImageUrl), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                self.selectedAnimatedGifImage.sd_setImage(with: URL(string: self.updateBackgroundImageUrl), placeholderImage: UIImage(named: "Default Icon"))
    
                self.wrapperBackImageView.isHidden = false
                
                self.wrapperScrollView.isScrollEnabled=true
                
                self.selectTimeFrameView1.isHidden = true
                
                self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + newSize.height + 5
                
                self.wrapperButtonsView.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
                
                self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + 50
            }
            else
            {
                self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                self.wrapperScrollView.contentSize.height = self.view.frame.size.height + 100
                
                self.wrapperScrollView.isScrollEnabled=true
                self.wrapperBackImageView.isHidden = true
            }
            
            if self.updateMoodUrl == "moodIcon-1"
            {
                self.selectBool = true
                self.selectedRow = 0
                self.imageSelectedForPost.image = UIImage(named: "moodIcon-1")
                self.selectedMoodTypeImageStr = "moodIcon-1"
                let indexPath = IndexPath(row: 0, section: 0)
                self.selectedIndexPath = indexPath as NSIndexPath
                self.moodCollectionView.reloadData()
            }
            else if self.updateMoodUrl == "moodIcon-2"
            {
                self.selectBool = true
                self.selectedRow = 1
                self.imageSelectedForPost.image = UIImage(named: "moodIcon-2")
                self.selectedMoodTypeImageStr = "moodIcon-2"
                let indexPath = IndexPath(row: 1, section: 0)
                self.selectedIndexPath = indexPath as NSIndexPath
                self.moodCollectionView.reloadData()
            }
            else if self.updateMoodUrl == "moodIcon-3"
            {
                self.selectBool = true
                self.selectedRow = 3
                self.selectedMoodTypeImageStr = "moodIcon-3"
                self.imageSelectedForPost.image = UIImage(named: "moodIcon-3")
                let indexPath = IndexPath(row: 3, section: 0)
                self.selectedIndexPath = indexPath as NSIndexPath
                self.moodCollectionView.reloadData()
            }
            else if self.updateMoodUrl == "moodIcon-4"
            {
                self.selectBool = true
                self.selectedRow = 4
                self.selectedMoodTypeImageStr = "moodIcon-4"
                self.imageSelectedForPost.image = UIImage(named: "moodIcon-4")
                let indexPath = IndexPath(row: 4, section: 0)
                self.selectedIndexPath = indexPath as NSIndexPath
                self.moodCollectionView.reloadData()
            }
            else if self.updateMoodUrl == "moodIcon-5"
            {
                self.selectBool = true
                self.selectedRow = 2
                self.selectedMoodTypeImageStr = "moodIcon-5"
                self.imageSelectedForPost.image = UIImage(named: "moodIcon-5")
                let indexPath = IndexPath(row: 2, section: 0)
                self.selectedIndexPath = indexPath as NSIndexPath
                self.moodCollectionView.reloadData()
            }
            else if self.updateMoodUrl == "moodIcon-6"
            {
                self.selectBool = true
                self.selectedRow = 5
                self.selectedMoodTypeImageStr = "moodIcon-6"
                self.imageSelectedForPost.image = UIImage(named: "moodIcon-6")
                let indexPath = IndexPath(row: 5, section: 0)
                self.selectedIndexPath = indexPath as NSIndexPath
                self.moodCollectionView.reloadData()
            }
            
            if self.updateTimeframe == "1"
            {
                self.timeFrameSelected = "1 hr"
                self.selectedRowForTimeFrame = 0
                self.selectBoolForTimeFrame = true
                let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                self.timeFrameCollectionView.reloadData()
            }
            else if self.updateTimeframe == "4"
            {
                self.timeFrameSelected = "4 hrs"
                
                self.selectedRowForTimeFrame = 1
                self.selectBoolForTimeFrame = true
                let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                self.timeFrameCollectionView.reloadData()
            }
            else if self.updateTimeframe == "8"
            {
                self.timeFrameSelected = "8 hrs"
                
                self.selectedRowForTimeFrame = 2
                self.selectBoolForTimeFrame = true
                let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                self.timeFrameCollectionView.reloadData()
            }
            else if self.updateTimeframe == "12"
            {
                self.timeFrameSelected = "12 hrs"
                
                self.selectedRowForTimeFrame = 3
                self.selectBoolForTimeFrame = true
                let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                self.timeFrameCollectionView.reloadData()
            }
            else if self.updateTimeframe == "16"
            {
                self.timeFrameSelected = "16 hrs"
                
                self.selectedRowForTimeFrame = 4
                self.selectBoolForTimeFrame = true
                let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                self.timeFrameCollectionView.reloadData()
            }
            else if self.updateTimeframe == "20"
            {
                self.timeFrameSelected = "20 hrs"
                
                self.selectedRowForTimeFrame = 5
                self.selectBoolForTimeFrame = true
                let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                self.timeFrameCollectionView.reloadData()
            }
            else
            {
                self.timeFrameSelected = "24 hrs"
                
                self.selectedRowForTimeFrame = 6
                self.selectBoolForTimeFrame = true
                let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                self.timeFrameCollectionView.reloadData()
            }
        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(mediaCapturedforCreateCampaign), name: NSNotification.Name(rawValue: "mediaCapturedforCreateCampaign"), object: nil)
    }
    
    
    //MARK:- ****** didChangeAuthorization ******
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined , .denied , .restricted :
            
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
            }
            
            break
            
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        /*default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
    }
    
    
    
    // MARK: - ****** didUpdateLocations ******
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.locationManager.location != nil
        {
            if let getCoordinates = manager.location?.coordinate
            {
                print("getCoordinates = \(getCoordinates)")
                
                let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                //            print("locations = \(locValue.latitude) \(locValue.longitude)")
                
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.locationManager.stopUpdatingLocation()
                
                if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                    
                    let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in // this is the last line that is being called
                        var placemark : CLPlacemark!
                        placemark = placemarks?[0]
                        
                        if placemark != nil
                        {
                            var vv = String()
                            if (placemark.subLocality != nil)
                            {
                                vv = placemark.subLocality!
                            }
                            
                            
                            var countWhiteSpaces = Int()
                            if (vv.range(of: " ") != nil)
                            {
                                countWhiteSpaces = (vv.components(separatedBy: " ").count) - 1
                            }
                            else{
                                countWhiteSpaces = 0
                            }
                            
                            var objAtIndexBeforeLast = String()
                            var objAtIndexLast = String()
                            if (countWhiteSpaces > 0)
                            {
                                var fullNameArr = vv.components(separatedBy: " ")
                                
                                objAtIndexBeforeLast = (fullNameArr[(fullNameArr.count) - 2])
                                
                                objAtIndexLast = (fullNameArr.last)!
                                
                                self.subLocalityStr = objAtIndexBeforeLast + " " + objAtIndexLast
                            }
                            else
                            {
                                self.subLocalityStr = vv
                            }
                            
                            if (placemark.locality != nil)
                            {
                                self.localityStr = placemark.locality!
                            }
                            
//                            var city = String()
                            if (placemark.addressDictionary?["City"] != nil)
                            {
//                                city = (placemark.addressDictionary?["City"] as! String)
                                self.currentCity = (placemark.addressDictionary?["City"] as! String)
                            }
                            
                            if (placemark.addressDictionary?["Country"] != nil)
                            {
                                self.country = (placemark.addressDictionary?["Country"] as! String)
                            }
                            
                            if (placemark.addressDictionary?["State"] != nil)
                            {
                                self.state = (placemark.addressDictionary?["State"] as! String)
                            }
                            
                            if (self.subLocalityStr != "") && (self.localityStr != "")
                            {
                                self.LocationStr = self.subLocalityStr + ", " + self.localityStr
                            }
                            else if (self.currentCity != "") && (self.state != "")
                            {
                                self.LocationStr = self.currentCity + ", " + self.state
                            }
                            else if (self.currentCity == "") && (self.state != "") && (self.country != "")
                            {
                                self.LocationStr = self.state + ", " + self.country
                            }
                            else if (self.state == "") && (self.currentCity != "") && (self.country != "")
                            {
                                self.LocationStr = self.currentCity + ", " + self.country
                            }
                            else
                            {
                                self.LocationStr = self.country
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    // MARK: - ****** ViewDidAppear ******
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    
    func mediaCapturedforCreateCampaign(value : Notification)
    {
        let dic = value.object as! Dictionary<String, Any>
        
        media_uploadedFrom = dic["media_uploadedFrom"] as! String
        
        self.mediaType = dic["mediaType"] as! String
        
        if mediaType  == "public.image" {
            
            if (dic["capturedImage"] != nil)
            {
                let chosenImage: UIImage = dic["capturedImage"] as! UIImage
                
                self.selectedBackgroundImage.isHidden = false
                self.selectedAnimatedGifImage.isHidden = true
                self.photoUploadType = "photo"
                
                self.selectedBackgroundImage.image = chosenImage
                self.backgroundImageView.image = chosenImage
                
                self.selectedBackgroundImage.layer.borderWidth = 2.0
                self.selectedBackgroundImage.layer.masksToBounds = false
                self.selectedBackgroundImage.layer.borderColor = UIColor.lightGray.cgColor
                
                self.backgroundImageView.layer.borderWidth = 2.0
                self.backgroundImageView.layer.masksToBounds = false
                self.backgroundImageView.layer.borderColor = UIColor.lightGray.cgColor
                
                self.photoUploadType = "photo"
                self.widthOfGif = Int(self.view.frame.size.width)
                self.heightOfGif = Int(self.view.frame.size.width)
                
                self.wrapperBackImageView.frame.size.width = self.view.frame.size.width - 10
                self.wrapperBackImageView.frame.size.height = self.view.frame.size.width - 10
                self.backgroundImageView.frame.size.width = self.view.frame.size.width
                self.backgroundImageView.frame.size.height = self.view.frame.size.width
                
                self.wrapperBackImageView.isHidden = false
                
                self.wrapperScrollView.isScrollEnabled=true
                self.selectTimeFrameView1.isHidden = true
   
                self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 5
                
                self.wrapperButtonsView.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
                
                self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + 50
            }
        }
        
        if mediaType == "public.movie" {
            
            if (dic["videoUrl"] != nil)
            {
                videoURL = dic["videoUrl"] as! NSURL;
                
                let asset : AVURLAsset = AVURLAsset(url: videoURL as URL)
                
                self.videoPath = asset.url as NSURL
                
                self.videoPlayer = AVPlayer(url: videoURL as URL)
                let playerLayer = AVPlayerLayer(player: self.videoPlayer)
                playerLayer.frame = self.backgroundImageView.bounds
                self.videoPlayer.play()
                self.videoPlayer.isMuted = false
                
                 self.backgroundImageView.image = nil
                 self.backgroundImageView.backgroundColor = UIColor.black
                 self.backgroundImageView.layer.addSublayer(playerLayer)
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer.currentItem, queue: nil, using: { (_) in
                    DispatchQueue.main.async {
                        self.videoPlayer.seek(to: kCMTimeZero)
                        self.videoPlayer.play()
                        self.videoPlayer.isMuted = false
//                        self.playBtn.isSelected  = true
                    }
                })

                self.photoUploadType = "video"
                
                self.selectedBackgroundImage.layer.borderWidth = 2.0
                self.selectedBackgroundImage.layer.masksToBounds = false
                self.selectedBackgroundImage.layer.borderColor = UIColor.lightGray.cgColor
                
                self.backgroundImageView.layer.borderWidth = 2.0
                self.backgroundImageView.layer.masksToBounds = false
                self.backgroundImageView.layer.borderColor = UIColor.lightGray.cgColor
                
                self.photoUploadType = "video"
                self.widthOfGif = Int(self.view.frame.size.width)
                self.heightOfGif = Int(self.view.frame.size.width)
                
                self.wrapperBackImageView.frame.size.width = self.view.frame.size.width - 10
                self.wrapperBackImageView.frame.size.height = self.view.frame.size.width - 10
                self.backgroundImageView.frame.size.width = self.view.frame.size.width
                self.backgroundImageView.frame.size.height = self.view.frame.size.width
                
                self.wrapperBackImageView.isHidden = false
                
                self.wrapperScrollView.isScrollEnabled=true
                self.selectTimeFrameView1.isHidden = true
   
                self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 5
                
                self.wrapperButtonsView.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
                
                self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + 50
            }
        }
    }
    
    @IBAction func openSearchStartView_BtnAction(_ sender: Any) {
        
    }
    
    
    //MARK:- ****** closeGiphyImageView_btnAction ******
    @IBAction func closeGiphyImageView_btnAction(_ sender: Any) {
        
        UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: { _ in
            
            self.getGifURl = ""
            self.widthOfGif = 100
            self.heightOfGif = 100
            self.giphyImagesView.isHidden = true
            
        }, completion: nil)
    }
    
    
    //MARK:- ****** UITextView Delegate ******
    public func textViewDidChange(_ textView: UITextView)
    {

        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame;
        
        if textView.text.count > 0
        {
            self.placeHolderTextView.isHidden = true
        }
        else{
            self.placeHolderTextView.isHidden = false
        }
        
        if self.wrapperBackImageView.isHidden == false
        {
            self.selectTimeFrameView1.isHidden=true
            self.wrapperScrollView.isScrollEnabled=true

            self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + textView.frame.height + 5
            
            self.wrapperButtonsView.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
            
            self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + 50
        }
        else
        {
            self.selectTimeFrameView1.isHidden=true
            self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + textView.frame.height + 5
            
            self.wrapperScrollView.isScrollEnabled=true
            self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + (self.view.frame.size.height/2)
        }
    }
    
    
    func keyboardWillShow(notification:NSNotification){
        let fixedWidth = self.addDescription_txtView.frame.size.width
        self.addDescription_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = self.addDescription_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = self.addDescription_txtView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        self.addDescription_txtView.frame = newFrame;
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.wrapperScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        wrapperScrollView.contentInset = contentInset
        
        self.getHeightOfKeyBoard = keyboardFrame.size.height
        
        if self.wrapperBackImageView.isHidden == false
        {
            self.selectTimeFrameView1.isHidden=true
            self.wrapperScrollView.isScrollEnabled=true
            
            self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + newSize.height + 5
            
            self.wrapperButtonsView.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
            
            self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + 50
        }
        else
        {
            self.selectTimeFrameView1.isHidden=true
            self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + newSize.height + 5
            
            self.wrapperScrollView.isScrollEnabled=true
            self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + (self.view.frame.size.height/2)
        }
    }
    
    
    func keyboardWillHide(notification:NSNotification){
        
        let fixedWidth = self.addDescription_txtView.frame.size.width
        self.addDescription_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = self.addDescription_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = self.addDescription_txtView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        self.addDescription_txtView.frame = newFrame;
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        wrapperScrollView.contentInset = contentInset
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        if self.wrapperBackImageView.isHidden == false
        {
            self.selectTimeFrameView1.isHidden=true
            self.wrapperScrollView.isScrollEnabled=true
            
            self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + newSize.height + 5
            
            self.wrapperButtonsView.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
            
            self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + 50
        }
        else
        {
            self.selectTimeFrameView1.isHidden=true
            self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + newSize.height + 5
            
            self.wrapperScrollView.isScrollEnabled=true
            self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + (self.view.frame.size.height/2)
        }
    }
    
    
    // MARK: - ****** UICollectionView Delegates and DataSource Methods. ******
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.moodCollectionView
        {
            return self.ImagesArray.count
        }
        else if collectionView == self.giphyCollectionView
        {
            return self.gifArray.count
        }
        else
        {
            return self.timeFrameArray.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == self.giphyCollectionView
        {
            let cell = self.giphyCollectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotosCollectionViewCell
            
            
            if (self.gifArray.count == 0)
            {
               return cell
            }
            else{
                let imagesDict: NSDictionary = ((self.gifArray[indexPath.row] as AnyObject).value(forKey: "images")) as! NSDictionary
                
                var preview_gif = NSDictionary()
                var gifURL = String()
                
                if imagesDict["preview_gif"] != nil
                {
                    preview_gif = imagesDict.value(forKey: "preview_gif") as! NSDictionary
                    
                    if preview_gif["url"] != nil
                    {
                        gifURL = preview_gif.value(forKey: "url") as! String
                    }else{
                        gifURL = ""
                    }
                    
                }
                else if imagesDict["fixed_height"] != nil{
                    preview_gif = imagesDict.value(forKey: "fixed_height") as! NSDictionary
                    
                    if preview_gif["url"] != nil
                    {
                        gifURL = preview_gif.value(forKey: "url") as! String
                    }else{
                        gifURL = ""
                    }
                }
                
                DispatchQueue.main.async {
                    
                    cell.acitivityIndicator.isHidden = false
                    cell.giphyAnimatedImageView.sd_setImage(with: URL(string: gifURL), placeholderImage: UIImage(named: ""),options: SDWebImageOptions(rawValue: 0), completed: { (img, err, cacheType, imgURL) in
                        cell.acitivityIndicator.isHidden = true
                    })
                }
                
                cell.giphyAnimatedImageView.contentMode = UIViewContentMode.scaleAspectFit
                
                return cell
            }
        }
        else if collectionView == self.moodCollectionView
        {
            let favouriteSearchCell = self.moodCollectionView.dequeueReusableCell(withReuseIdentifier: "favouriteSearchCell", for: indexPath as IndexPath) as! FavoriteListPeopleCollectionViewCell
            
            var borderColor: CGColor! = UIColor.clear.cgColor
            var borderWidth: CGFloat = 0
            
            if self.selectBool == true
            {
                if (indexPath == selectedIndexPath as IndexPath)
                {
                    borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
                    borderWidth = 3
                    favouriteSearchCell.selectedImage.isHidden = false
                }
                else
                {
                    borderColor = UIColor.clear.cgColor
                    borderWidth = 0
                    favouriteSearchCell.selectedImage.isHidden = true
                }
            }
            else
            {
                borderColor = UIColor.clear.cgColor
                borderWidth = 0
                favouriteSearchCell.selectedImage.isHidden = true
            }
            
            favouriteSearchCell.selectBackgroundImage.image = UIImage(named: self.ImagesArray[indexPath.row])
            
            favouriteSearchCell.selectBackgroundImage.layer.borderWidth = borderWidth
            favouriteSearchCell.selectBackgroundImage.layer.borderColor  = borderColor
          
            return favouriteSearchCell
        }
        else{
            let favouriteSearchCell = self.timeFrameCollectionView.dequeueReusableCell(withReuseIdentifier: "favouriteSearchCell", for: indexPath as IndexPath) as! FavoriteListPeopleCollectionViewCell
            
            
            if self.selectBoolForTimeFrame == true
            {
                if (indexPath == selectedIndexPathForTimeFrame as IndexPath)
                {
                    favouriteSearchCell.radioABtnIcon.image = UIImage(named: "Radio Filled Say")
                }
                else
                {
                    favouriteSearchCell.radioABtnIcon.image = UIImage(named: "Radio unfilled say")
                }
            }
            else
            {
                favouriteSearchCell.radioABtnIcon.image = UIImage(named: "Radio unfilled say")
            }
            
            favouriteSearchCell.hoursLbl.text = self.timeFrameArray[indexPath.row]
            
            return favouriteSearchCell
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        if collectionView == self.giphyCollectionView
        {
            var frameOfCell = self.giphyCollectionView.frame as CGRect
            frameOfCell.origin.y = 0
            return CGSize.init(width: self.view.frame.size.width/2 - 1, height: self.view.frame.size.width/2 + 15)
        }
        else if collectionView == self.moodCollectionView
        {
            return CGSize.init(width: 110, height: 110)
        }
        else
        {
            return CGSize.init(width: 75, height: 40)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == self.moodCollectionView
        {
            let favouriteCell = self.moodCollectionView.cellForItem(at: indexPath) as! FavoriteListPeopleCollectionViewCell
            self.imageSelectedForPost.image = favouriteCell.selectBackgroundImage.image
            
             self.selectedMoodTypeImageStr = self.ImagesArray[indexPath.row]
            
             self.selectBool = true
             self.selectedRow = indexPath.row as Int
             selectedIndexPath = indexPath as NSIndexPath
             self.moodCollectionView.reloadData()
        }
        else if collectionView == self.timeFrameCollectionView
        {
            self.selectBoolForTimeFrame = true
            self.selectedRowForTimeFrame = indexPath.row as Int
            self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
            
            self.timeFrameSelected = self.timeFrameArray[indexPath.row]
        
            self.timeFrameCollectionView.reloadData()
        }
        else
        {
            self.getGifURl = ""
            self.widthOfGif = 100
            self.heightOfGif = 100
            self.selectedAnimatedGifImage.animatedImage = nil
            
            let photosCell = collectionView.cellForItem(at: indexPath) as! PhotosCollectionViewCell
            
            self.selectedBackgroundImage.isHidden = true
            self.selectedAnimatedGifImage.isHidden = false
            
             DispatchQueue.main.async {
             
             self.selectedAnimatedGifImage.animatedImage = photosCell.giphyAnimatedImageView.animatedImage
                
                let imagesDict: NSDictionary = ((self.gifArray[indexPath.row] as AnyObject).value(forKey: "images")) as! NSDictionary
                
                var preview_gif = NSDictionary()
                
                if imagesDict["fixed_height"] != nil{
                    preview_gif = imagesDict.value(forKey: "fixed_height") as! NSDictionary
                }
                else if imagesDict["preview_gif"] != nil{
                    preview_gif = imagesDict.value(forKey: "preview_gif") as! NSDictionary
                }
                
                self.getGifURl = preview_gif.value(forKey: "url") as! String
                self.updateBackgroundImageUrl = preview_gif.value(forKey: "url") as! String
                
                if (preview_gif["width"] != nil) && (preview_gif["height"] != nil)
                {
                    self.widthOfGif = Int(preview_gif.value(forKey: "width") as! String)!
                    self.heightOfGif = Int(preview_gif.value(forKey: "height") as! String)!
                }
                else
                {
                    self.widthOfGif = 100
                    self.heightOfGif = 100
                }
                
               self.backgroundImageView.sd_setImage(with: URL(string: self.getGifURl), placeholderImage: UIImage(named: "Default Icon"),options: SDWebImageOptions(rawValue: 0), completed: { (img, err, cacheType, imgURL) in
                    
                    //cell.acitivityIndicator.isHidden = true
                })
                
               self.selectedAnimatedGifImage.contentMode = UIViewContentMode.scaleAspectFit
                
                UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: { _ in
                   
                    self.giphyImagesView.isHidden = true
                    self.selectedAnimatedGifImage.isHidden = false
                    self.selectedBackgroundImage.isHidden = true
                    
                    self.selectedAnimatedGifImage.layer.borderWidth = 2.0
                    self.selectedAnimatedGifImage.layer.masksToBounds = false
                    self.selectedAnimatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                    
                    self.wrapperBackImageView.frame.size.width = self.view.frame.size.width - 10
                    self.wrapperBackImageView.frame.size.height = self.view.frame.size.width - 10
                    self.backgroundImageView.frame.size.width = self.view.frame.size.width
                    self.backgroundImageView.frame.size.height = self.view.frame.size.width
                    
                }, completion: nil)
                
                self.photoUploadType = "gif"
                
                self.gifImageChangedStr = "gifChanged"
                
                self.wrapperBackImageView.isHidden = false
                self.selectTimeFrameView1.isHidden = true

                self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 5
                
                self.wrapperButtonsView.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
                
                self.wrapperScrollView.contentSize.height = self.wrapperButtonsView.frame.origin.y + self.wrapperButtonsView.frame.size.height + 50
             }
        }
    }
 
    // MARK: - ****** viewWillAppear ******
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        UIApplication.shared.isStatusBarHidden=false
    }
    

    // MARK: - ****** back_btnAction ******
    @IBAction func back_btnAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    // MARK: - ****** selectPhoto_btnAction ******
    @IBAction func selectPhoto_btnAction(_ sender: Any) {
//        uploadPhotoVideoInWiPost()
//        return
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if #available(iOS 10.0, *) {
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "CameraOverlayViewController") as! CameraOverlay
            homeScreen.parentClass = self
            homeScreen.differentiateStr = "WiCreateConfessionViewController"
            self.navigationController?.present(homeScreen, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    // MARK: - ****** ActionSheet For Choosing upload option. ******
    func uploadPhotoVideoInWiPost() {
        
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Post a video in WiPost", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
  
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if #available(iOS 10.0, *) {
                let homeScreen = storyboard.instantiateViewController(withIdentifier: "CameraOverlayViewController") as! CameraOverlay
                homeScreen.parentClass = self
                homeScreen.differentiateStr = "WiCreateConfessionViewController"
                homeScreen.differentiateMediaStr = "Upload a video"
                self.navigationController?.present(homeScreen, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Post a photo in WiPost", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if #available(iOS 10.0, *) {
                let homeScreen = storyboard.instantiateViewController(withIdentifier: "CameraOverlayViewController") as! CameraOverlay
                homeScreen.parentClass = self
                homeScreen.differentiateStr = "WiCreateConfessionViewController"
                homeScreen.differentiateMediaStr = "Upload a photo"
                self.navigationController?.present(homeScreen, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: - ****** selectGif_btnAction ******
    @IBAction func selectGif_btnAction(_ sender: Any) {
        self.addDescription_txtView.resignFirstResponder()
        UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromTop, animations: { _ in
            
            self.giphyImagesView.isHidden = false
            
        }, completion: nil)
    }
    
    // MARK: - ****** openTextvieDescription_btnAction ******
    @IBAction func openTextvieDescription_btnAction(_ sender: Any) {
        
        
    }
    
    // MARK: - ****** Show Action Sheet From select options. ******
    func showActionSheet() {
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            if status == .denied {
                PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                    let alert = UIAlertController(
                        title: "IMPORTANT",
                        message: "\("WI Match") Would like to capture the Photos",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else if status == .authorized  || status == .notDetermined
            {
                self.gallaryTypeStr = "camera"
                self.camera()
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            if  status == .denied {
                PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                    let alert = UIAlertController(
                        title: "IMPORTANT",
                        message: "\("WI Match") Would like to access the Photos",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else if status == .authorized || status == .notDetermined
            {
                self.gallaryTypeStr = "photoLibrary"
               self.photoLibrary()
            }
            
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** Method to open Photo Library. ******
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        myPickerController.allowsEditing = false
        myPickerController.mediaTypes = [kUTTypeImage as String]
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** Method To Open Camera. ******
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.camera
            myPickerController.mediaTypes = [kUTTypeImage as String]
            myPickerController.allowsEditing = false
            self.present(myPickerController, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "", message: "Sorry,Camera is not available to take picture.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - ****** UIImagePicker Delegate Methods. ******
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (self.gallaryTypeStr == "camera") || (self.gallaryTypeStr == "photoLibrary")
        {
            self.photoUploadType = "photo"
            self.selectedBackgroundImage.isHidden = false
            self.selectedAnimatedGifImage.isHidden = true
            
            let pickedImage : UIImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            self.dismiss(animated: true, completion: { () -> Void in
                // Use view controller
                self.navigationController?.isNavigationBarHidden = false
                let controller = ImageCropViewController(image: pickedImage)
                controller?.delegate = self
                controller?.blurredBackground = true
                self.navigationController?.pushViewController(controller!, animated: true)
            })
        }
        else
        {
            let assetURL = info[UIImagePickerControllerReferenceURL] as! URL
            let asset = PHAsset.fetchAssets(withALAssetURLs: [assetURL], options: nil).lastObject
            if asset != nil
            {
                let requestOptions = PHImageRequestOptions()
                requestOptions.isSynchronous = true // adjust the parameters as you wish
                PHImageManager.default().requestImageData(for: asset!, options: requestOptions, resultHandler: { (imageData, dataUTI, _, _) in
                    if let uti = dataUTI,let data = imageData ,
                        // you can also use UTI to make sure it's a gif
                        UTTypeConformsTo(uti as CFString, kUTTypeGIF) {
                        // save data here
                        self.photoUploadType = "gif"
                        self.gifImageChangedStr = "gifChanged"
                        self.wrapperBackImageView.isHidden = false
                        
                        self.wrapperScrollView.isScrollEnabled=true
                        
                        self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 5
                        
                        self.selectTimeFrameView1.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
                        
                        self.wrapperScrollView.contentSize.height = self.selectTimeFrameView1.frame.origin.y + self.selectTimeFrameView1.frame.size.height + 50
                        
                        let pickedImage : UIImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
                        self.backgroundImageView.image = pickedImage
                        self.dismiss(animated: true, completion: { () -> Void in
                            // Use view controller
                            self.gifImageData = data as NSData
                            self.selectedAnimatedGifImage.isHidden = false
                            self.selectedBackgroundImage.isHidden = true
                            let fff=FLAnimatedImage(gifData: data as Data?)
                            self.selectedAnimatedGifImage.animatedImage=fff
                     
                        })
                    }
                    else
                    {
                        self.selectedBackgroundImage.isHidden = false
                        self.selectedAnimatedGifImage.isHidden = true
                        self.photoUploadType = "photo"
                        let pickedImage : UIImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
                        self.dismiss(animated: true, completion: { () -> Void in
                            // Use view controller
                            self.navigationController?.isNavigationBarHidden = false
                            let controller = ImageCropViewController(image: pickedImage)
                            controller?.delegate = self
                            controller?.blurredBackground = true
                            self.navigationController?.pushViewController(controller!, animated: true)
                        })
                    }
                })
            }
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.directPickImageBool = false
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- ****** ImageCrop View Controller ******
    func imageCropViewControllerSuccess(_ controller: UIViewController, didFinishCroppingImage croppedImage: UIImage)
    {
        self.selectedBackgroundImage.image = croppedImage
        self.backgroundImageView.image = croppedImage
        
        self.croppedImageWidth = Int64(croppedImage.size.width)
        self.croppedImageheight = Int64(croppedImage.size.height)
        
        if (self.view.frame.size.height == 736) || (self.view.frame.size.height == 667)
        {
            self.croppedImageWidth = 650
            self.croppedImageheight = 1150
        }
        else{
            self.croppedImageWidth = 450
            self.croppedImageheight = 950
        }
        
        self.wrapperBackImageView.isHidden = false
        
        self.wrapperScrollView.isScrollEnabled=true
        
        self.wrapperBackImageView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 5
        
        self.selectTimeFrameView1.frame.origin.y = self.wrapperBackImageView.frame.origin.y + self.wrapperBackImageView.frame.size.height + 5
        
        self.wrapperScrollView.contentSize.height = self.selectTimeFrameView1.frame.origin.y + self.selectTimeFrameView1.frame.size.height + 50
        
        self.navigationController!.popViewController(animated: true)
    }
    
    
    func imageCropViewControllerDidCancel(_ controller: UIViewController)
    {
        self.directPickImageBool = false
        self.navigationController!.popViewController(animated: true)
    }
    
    
    // MARK: - ****** createNewConfession_btnAction ******
    @IBAction func createNewConfession_btnAction(_ sender: Any) {
       /* if self.imageSelectedForPost.image == nil
        {
            let alert = UIAlertController(title: "", message: "Please select mood type.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }*/
        if ((self.addDescription_txtView.text == "") && (self.backgroundImageView.image == nil))
        {
            let alert = UIAlertController(title: "", message: "Please add status/select media to upload.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if self.postTypeStr == "CreateNewPost"
            {
                let actionSheet = UIAlertController(title: "Please select your option for WiPost and your wipost will be available for next 24 hours", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                
                actionSheet.addAction(UIAlertAction(title: "Post as Anonymous", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
                    
                    self.typeOfConfessionSelected = "anonymous"
                    
                    self.savePostMethod(completion: { (resultData) in
//                        print("resultData = \(resultData)")
                    })
                }))
                
                actionSheet.addAction(UIAlertAction(title: "Post with profile", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
                    
                    self.typeOfConfessionSelected = "withprofile"
                    
                    self.savePostMethod(completion: { (resultData) in
//                        print("resultData = \(resultData)")
                    })
                }))
                
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(actionSheet, animated: true, completion: nil)
            }
            else
            {
                self.savePostMethod(completion: { (resultData) in
//                    print("resultData = \(resultData)")
                })
            }
        }
    }
    
    func savePostMethod(completion: @escaping (Bool) -> Swift.Void)
    {
        if updateStr == "EditConfession"
        {
            if self.postTypeStr == "CreateNewPost"
            {
                if self.updatePostID != ""
                {
                    self.updateConfessionAndSave(completion: { (responseData) in
//                        print("responseData = \(responseData)")
                    })
                }
            }
            else if self.postTypeStr == "ForReplyOnPost"
            {
                if (self.updateReplyID != "")
                {
                    self.updateRepliesOnConfessionAndSave(completion: { (responseData) in
//                        print("responseData = \(responseData)")
                    })
                }
            }
            else if self.postTypeStr == "ForReplyOnReply"
            {
                if (self.updateReplyToReplyID != "")
                {
                    self.updateRepliesOnRepliesAndSave(completion: { (responseData) in
//                        print("responseData = \(responseData)")
                    })
                }
            }
        }
        else
        {
            if self.postTypeStr == "CreateNewPost"
            {
                self.createConfessionAndSave(completion: { (responseBool) in
                })
            }
            else if self.postTypeStr == "ForReplyOnPost"
            {
                self.createConfessionReplyAndSave(completion: { (responseBool) in
                })
            }
            else if self.postTypeStr == "ForReplyOnReply"
            {
                self.createConfessionFurtherReplyAndSave(completion: { (responseBool) in
                })
            }
        }
        
        completion(true)
    }
    
    // MARK: - ****** savePost_btnAction ******
    @IBAction func savePost_btnAction(_ sender: Any) {
       
    }
    
    func updateConfessionAndSave(completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    var typeOfFileofSave = String()
                    var typeOfConfessionStr = String()
                    var mediaExtension = String()
                    if self.photoUploadType == "photo"
                    {
                        typeOfFileofSave = "image/jpg"
                        typeOfConfessionStr = "photo"
                        mediaExtension = "jpg"
                    }
                    else if self.photoUploadType == "gif"
                    {
                        typeOfFileofSave = "image/gif"
                        typeOfConfessionStr = "gif"
                        mediaExtension = "gif"
                    }
                    else{
                        typeOfFileofSave = ""
                        typeOfConfessionStr = "text"
                    }
                    
                    let date = NSDate();
                    let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
                    // set upload path
                    let filePath = "\(currentTimeStamp).\(mediaExtension)"
                    
                    let fullNameArr = self.timeFrameSelected.components(separatedBy: " ")
                    
                    if fullNameArr.count > 0
                    {
                        if fullNameArr.count == 1
                        {
                            let firstName: String = fullNameArr[0]
                            self.timeFrameSelected = firstName
                        }
                        else
                        {
                            let firstName: String = fullNameArr[0]
//                            let lastName: String = fullNameArr[1]

                            self.timeFrameSelected = firstName
                        }
                    }
                    
                    if (self.backgroundImageView.image != nil) || (self.selectedAnimatedGifImage.image != nil)
                    {
                        var image = UIImage()
//                        var animatedImage = FLAnimatedImage()
                        
                        if self.photoUploadType == "photo"
                        {
                            image = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            let image2 = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            image = ApiHandler.resizeImage(image: image2, newWidth: self.view.frame.size.width)
                        }
                        else if self.photoUploadType == "gif"
                        {
//                            animatedImage = self.selectedAnimatedGifImage.animatedImage
                        }
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.updateConfession(userID: userID, authToken: authToken, typeOfConfession: self.typeOfConfessionSelected, location: self.LocationStr, confessionImage: image, text: search_txt, latitude: self.currentLatitude, longitude: self.currentLongitude, nameOfFile: filePath, typeOfFile: typeOfFileofSave, animatedGif: self.updateBackgroundImageUrl, image_height: self.heightOfGif, image_width: self.widthOfGif, confessionIDToUpdate: self.updatePostID, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                        
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                                print("dataDict = \(String(describing: dataDict))")
                                let message = responseData.value(forKey: "message") as? String
//                                print("message = \(String(describing: message))")
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnCreatePost"), object: nil)
                                self.navigationController!.popViewController(animated: true)
                                    
                                    return
                                        
                                 /*   self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
//                                print("message = \(String(describing: message))")
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                    else
                    {
                        let image = UIImage()
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.updateConfession(userID: userID, authToken: authToken, typeOfConfession: self.typeOfConfessionSelected, location: self.LocationStr, confessionImage: image, text: search_txt, latitude: self.currentLatitude, longitude: self.currentLongitude, nameOfFile: "", typeOfFile: typeOfFileofSave, animatedGif: self.updateBackgroundImageUrl, image_height: self.heightOfGif, image_width: self.widthOfGif, confessionIDToUpdate: self.updatePostID, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                                
//                            print("responseData = \(responseData)")
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                                print("dataDict = \(String(describing: dataDict))")
                                let message = responseData.value(forKey: "message") as? String
//                                print("message = \(String(describing: message))")
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnCreatePost"), object: nil)
                                self.navigationController!.popViewController(animated: true)
                                    
                                    return
                                        
                                  /*  self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                    
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
            }
        }
    }
    
    func updateRepliesOnConfessionAndSave(completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    var typeOfFileofSave = String()
                    var typeOfConfessionStr = String()
                    var mediaExtension = String()
                    if self.photoUploadType == "photo"
                    {
                        typeOfFileofSave = "image/jpg"
                        typeOfConfessionStr = "photo"
                        mediaExtension = "jpg"
                    }
                    else if self.photoUploadType == "gif"
                    {
                        typeOfFileofSave = "image/gif"
                        typeOfConfessionStr = "gif"
                        mediaExtension = "gif"
                    }
                    else{
                        typeOfFileofSave = ""
                        typeOfConfessionStr = "text"
                    }
                    
                    let date = NSDate();
                    let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
                    // set upload path
                    let filePath = "\(currentTimeStamp).\(mediaExtension)"
                    
                    let fullNameArr = self.timeFrameSelected.components(separatedBy: " ")
                    
                    if fullNameArr.count > 0
                    {
                        if fullNameArr.count == 1
                        {
                            let firstName: String = fullNameArr[0]
                            self.timeFrameSelected = firstName
                        }
                        else
                        {
                            let firstName: String = fullNameArr[0]
//                            let lastName: String = fullNameArr[1]
                            self.timeFrameSelected = firstName
                        }
                    }
                    
                    if (self.backgroundImageView.image != nil) || (self.selectedAnimatedGifImage.image != nil)
                    {
                        var image = UIImage()
//                        var animatedImage = FLAnimatedImage()
                        
                        if self.photoUploadType == "photo"
                        {
                            image = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            let image2 = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            image = ApiHandler.resizeImage(image: image2, newWidth: self.view.frame.size.width)
                        }
                        else if self.photoUploadType == "gif"
                        {
//                            animatedImage = self.selectedAnimatedGifImage.animatedImage
                        }
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.updateReplyConfession(userID: userID, authToken: authToken, mood: self.selectedMoodTypeImageStr, duration: self.timeFrameSelected, confessionImage: image, text: search_txt, typeOfConfession: typeOfConfessionStr, nameOfFile: filePath, typeOfFile: typeOfFileofSave, animatedGif: self.updateBackgroundImageUrl, commnet_id: self.updateReplyID, id: "",image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
//                            print("responseData = \(responseData)")
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                                print("dataDict = \(String(describing: dataDict))")
                                let message = responseData.value(forKey: "message") as? String
//                                print("message = \(String(describing: message))")
                                
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    self.navigationController!.popViewController(animated: true)
                                    return
                                        
                                  /*  self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                    else
                    {
                        let image = UIImage()
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.updateReplyConfession(userID: userID, authToken: authToken, mood: self.selectedMoodTypeImageStr, duration: self.timeFrameSelected, confessionImage: image, text: search_txt, typeOfConfession: typeOfConfessionStr, nameOfFile: "", typeOfFile: typeOfFileofSave, animatedGif: self.updateBackgroundImageUrl, commnet_id: self.updateReplyID, id: "",image_height: self.heightOfGif,image_width:self.widthOfGif,typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    self.navigationController!.popViewController(animated: true)
                                    return
                                        
                                 /*   self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                   
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
            }
        }
    }
    
    func updateRepliesOnRepliesAndSave(completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    var typeOfFileofSave = String()
                    var typeOfConfessionStr = String()
                    var mediaExtension = String()
                    if self.photoUploadType == "photo"
                    {
                        typeOfFileofSave = "image/jpg"
                        typeOfConfessionStr = "photo"
                        mediaExtension = "jpg"
                    }
                    else if self.photoUploadType == "gif"
                    {
                        typeOfFileofSave = "image/gif"
                        typeOfConfessionStr = "gif"
                        mediaExtension = "gif"
                    }
                    else{
                        typeOfFileofSave = ""
                        typeOfConfessionStr = "text"
                    }
                    
                    let date = NSDate();
                    let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
                    // set upload path
                    let filePath = "\(currentTimeStamp).\(mediaExtension)"
                    
                    let fullNameArr = self.timeFrameSelected.components(separatedBy: " ")
                    
                    if fullNameArr.count > 0
                    {
                        if fullNameArr.count == 1
                        {
                            let firstName: String = fullNameArr[0]
                             self.timeFrameSelected = firstName
                        }
                        else
                        {
                            let firstName: String = fullNameArr[0]
//                            let lastName: String = fullNameArr[1]

                            self.timeFrameSelected = firstName
                        }
                    }
                    
                    if (self.backgroundImageView.image != nil) || (self.selectedAnimatedGifImage.image != nil)
                    {
                        var image = UIImage()
//                        var animatedImage = FLAnimatedImage()
                        
                        if self.photoUploadType == "photo"
                        {
                            image = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            let image2 = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            image = ApiHandler.resizeImage(image: image2, newWidth: self.view.frame.size.width)
                        }
                        else if self.photoUploadType == "gif"
                        {
//                            animatedImage = self.selectedAnimatedGifImage.animatedImage
                        }
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.updateReplyConfession(userID: userID, authToken: authToken, mood: self.selectedMoodTypeImageStr, duration: self.timeFrameSelected, confessionImage: image, text: search_txt, typeOfConfession: typeOfConfessionStr, nameOfFile: filePath, typeOfFile: typeOfFileofSave, animatedGif: self.updateBackgroundImageUrl, commnet_id: self.updateReplyToReplyID, id: "",image_height: self.heightOfGif,image_width:self.widthOfGif,typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                let message = responseData.value(forKey: "message") as? String
                                
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    self.navigationController!.popViewController(animated: true)
                                    return
                                        
                                   /* self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
//                                    print("selectedRowForTimeFrame= \(self.selectedRowForTimeFrame)")
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
//                                print("message = \(String(describing: message))")
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                    else
                    {
                        let image = UIImage()
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.updateReplyConfession(userID: userID, authToken: authToken, mood: self.selectedMoodTypeImageStr, duration: self.timeFrameSelected, confessionImage: image, text: search_txt, typeOfConfession: typeOfConfessionStr, nameOfFile: "", typeOfFile: typeOfFileofSave, animatedGif: self.updateBackgroundImageUrl, commnet_id: self.updateReplyToReplyID, id: "",image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    self.navigationController!.popViewController(animated: true)
                                    return
                                        
                                   /* self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
            }
        }
    }
    
    func createConfessionAndSave(completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    var typeOfFileofSave = String()
                    var typeOfConfessionStr = String()
                    var mediaExtension = String()
                    if self.photoUploadType == "photo"
                    {
                        typeOfFileofSave = "image/jpg"
                        typeOfConfessionStr = "photo"
                        mediaExtension = "jpg"
                    }
                    else if self.photoUploadType == "gif"
                    {
                        typeOfFileofSave = "image/gif"
                        typeOfConfessionStr = "gif"
                        mediaExtension = "gif"
                    }
                    else{
                        typeOfFileofSave = ""
                        typeOfConfessionStr = "text"
                    }
                    
                    let date = NSDate();
                    let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
                    // set upload path
                    let filePath = "\(currentTimeStamp).\(mediaExtension)"
                    
                    let fullNameArr = self.timeFrameSelected.components(separatedBy: " ")
                    
                    if fullNameArr.count > 0
                    {
                        if fullNameArr.count == 1
                        {
                            let firstName: String = fullNameArr[0]
                            self.timeFrameSelected = firstName
                        }
                        else
                        {
                            let firstName: String = fullNameArr[0]
//                            let lastName: String = fullNameArr[1]
                            self.timeFrameSelected = firstName
                        }
                    }
                    
                    if (self.backgroundImageView.image != nil) || (self.selectedAnimatedGifImage.image != nil)
                    {
                        var image = UIImage()
//                        var animatedImage = FLAnimatedImage()
                        
                        if self.photoUploadType == "photo"
                        {
                             image = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            let image2 = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            image = ApiHandler.resizeImage(image: image2, newWidth: self.view.frame.size.width)
                        }
                        else if self.photoUploadType == "gif"
                        {
//                            animatedImage = self.selectedAnimatedGifImage.animatedImage
                        }
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.createConfession(userID: userID, authToken: authToken, typeOfConfession: self.typeOfConfessionSelected, location: self.LocationStr, confessionImage: image, text: search_txt, latitude: self.currentLatitude, longitude: self.currentLongitude, nameOfFile: filePath, typeOfFile: typeOfFileofSave, animatedGif: self.getGifURl, image_height: self.heightOfGif, image_width: self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnCreatePost"), object: nil)

                                    self.navigationController!.popViewController(animated: true)
                                    return
                                    
                                    /*self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                    else
                    {
                        let image = UIImage()
                      
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.createConfession(userID: userID, authToken: authToken, typeOfConfession: self.typeOfConfessionSelected, location: self.LocationStr, confessionImage: image, text: search_txt, latitude: self.currentLatitude, longitude: self.currentLongitude, nameOfFile: "", typeOfFile: typeOfFileofSave, animatedGif: self.getGifURl, image_height: self.heightOfGif, image_width: self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in

                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnCreatePost"), object: nil)
                                    self.navigationController!.popViewController(animated: true)
                                    return
                                        
                                   /* self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                    
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
            }
        }
    }
    
    func createConfessionReplyAndSave(completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    var typeOfFileofSave = String()
                    var typeOfConfessionStr = String()
                    var mediaExtension = String()
                    if self.photoUploadType == "photo"
                    {
                        typeOfFileofSave = "image/jpg"
                        typeOfConfessionStr = "photo"
                        mediaExtension = "jpg"
                    }
                    else if self.photoUploadType == "gif"
                    {
                        typeOfFileofSave = "image/gif"
                        typeOfConfessionStr = "gif"
                        mediaExtension = "gif"
                    }
                    else{
                        typeOfFileofSave = ""
                        typeOfConfessionStr = "text"
                        mediaExtension = ""
                    }
                    
                    let date = NSDate();
                    let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
                    // set upload path
                    let filePath = "\(currentTimeStamp).\(mediaExtension)"
                    
                    let fullNameArr = self.timeFrameSelected.components(separatedBy: " ")
                    
                    if fullNameArr.count > 0
                    {
                        if fullNameArr.count == 1
                        {
                            let firstName: String = fullNameArr[0]
                            self.timeFrameSelected = firstName
                        }
                        else
                        {
                            let firstName: String = fullNameArr[0]
//                            let lastName: String = fullNameArr[1]
                            self.timeFrameSelected = firstName
                        }
                    }
                    
                    if (self.backgroundImageView.image != nil) || (self.selectedAnimatedGifImage.image != nil)
                    {
                        var image = UIImage()
//                        var animatedImage = FLAnimatedImage()
                        
                        if self.photoUploadType == "photo"
                        {
                            image = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            let image2 = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            image = ApiHandler.resizeImage(image: image2, newWidth: self.view.frame.size.width)
                        }
                        else if self.photoUploadType == "gif"
                        {
//                            animatedImage = self.selectedAnimatedGifImage.animatedImage
                        }
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.createReplyConfession(userID: userID, authToken: authToken, mood: self.selectedMoodTypeImageStr, duration: self.timeFrameSelected, confessionImage: image, text: search_txt, typeOfConfession: typeOfConfessionStr, nameOfFile: filePath, typeOfFile: typeOfFileofSave, animatedGif: self.getGifURl, parent_comment_id: "", confession_id: self.currentPostIDStr,image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    self.navigationController!.popViewController(animated: true)
                                    return
                                        
                                   /* self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                    else
                    {
                        let image = UIImage()
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.createReplyConfession(userID: userID, authToken: authToken, mood: self.selectedMoodTypeImageStr, duration: self.timeFrameSelected, confessionImage: image, text: search_txt, typeOfConfession: typeOfConfessionStr, nameOfFile: "", typeOfFile: typeOfFileofSave, animatedGif: self.getGifURl, parent_comment_id: "", confession_id: self.currentPostIDStr,image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    self.navigationController!.popViewController(animated: true)
                                    return
                                        
                                   /* self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
            }
        }
    }
    
    
    func createConfessionFurtherReplyAndSave(completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    var typeOfFileofSave = String()
                    var typeOfConfessionStr = String()
                    var mediaExtension = String()
                    if self.photoUploadType == "photo"
                    {
                        typeOfFileofSave = "image/jpg"
                        typeOfConfessionStr = "photo"
                        mediaExtension = "jpg"
                    }
                    else if self.photoUploadType == "gif"
                    {
                        typeOfFileofSave = "image/gif"
                        typeOfConfessionStr = "gif"
                        mediaExtension = "gif"
                    }
                    else{
                        typeOfFileofSave = ""
                        typeOfConfessionStr = "text"
                        mediaExtension = ""
                    }
                    
                    let date = NSDate();
                    let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
                    // set upload path
                    let filePath = "\(currentTimeStamp).\(mediaExtension)"
                    
                    let fullNameArr = self.timeFrameSelected.components(separatedBy: " ")
                    
                    if fullNameArr.count > 0
                    {
                        if fullNameArr.count == 1
                        {
                            let firstName: String = fullNameArr[0]
                            self.timeFrameSelected = firstName
                        }
                        else
                        {
                            let firstName: String = fullNameArr[0]
//                            let lastName: String = fullNameArr[1]

                            self.timeFrameSelected = firstName
                        }
                    }
                    
                    if (self.backgroundImageView.image != nil) || (self.selectedAnimatedGifImage.image != nil)
                    {
                        var image = UIImage()
//                        var animatedImage = FLAnimatedImage()
                        
                        if self.photoUploadType == "photo"
                        {
                            image = self.rotateImageFix(image: self.backgroundImageView.image!)
                            
//                            let image2 = self.rotateImageFix(image: self.backgroundImageView.image!)
//                            image = ApiHandler.resizeImage(image: image2, newWidth: self.view.frame.size.width)
                        }
                        else if self.photoUploadType == "gif"
                        {
//                            animatedImage = self.selectedAnimatedGifImage.animatedImage
                        }
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.createReplyConfession(userID: userID, authToken: authToken, mood: self.selectedMoodTypeImageStr, duration: self.timeFrameSelected, confessionImage: image, text: search_txt, typeOfConfession: typeOfConfessionStr, nameOfFile: filePath, typeOfFile: typeOfFileofSave, animatedGif: self.getGifURl, parent_comment_id: self.currentReplyIDStr, confession_id: self.currentPostIDStr,image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    self.navigationController!.popViewController(animated: true)
                                    return
                                 /*
                                    self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                    else
                    {
                        let image = UIImage()
                        
                        var search_txt: String = self.addDescription_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        ApiHandler.createReplyConfession(userID: userID, authToken: authToken, mood: self.selectedMoodTypeImageStr, duration: self.timeFrameSelected, confessionImage: image, text: search_txt, typeOfConfession: typeOfConfessionStr, nameOfFile: "", typeOfFile: typeOfFileofSave, animatedGif: self.getGifURl, parent_comment_id: self.currentReplyIDStr, confession_id: self.currentPostIDStr,image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                  self.navigationController!.popViewController(animated: true)
                                    return
                                        
                                    /*self.addDescription_txtView.text = ""
                                    self.timeFrameSelected = "24 hrs"
                                    self.imageSelectedForPost.image = nil
                                    self.backgroundImageView.image = nil
                                    self.selectedBackgroundImage.image = nil
                                    self.selectedAnimatedGifImage.animatedImage=nil
                                    
                                    self.selectedRowForTimeFrame = 6
                                    self.selectBoolForTimeFrame = true
                                    
                                    let indexPath = IndexPath(row: self.selectedRowForTimeFrame, section: 0)
                                    self.selectedIndexPathForTimeFrame = indexPath as NSIndexPath
                                    
                                    self.timeFrameCollectionView.reloadData()
                                    
                                    self.selectBool = false
                                    
                                    self.wrapperScrollView.isScrollEnabled=true
                                    self.wrapperBackImageView.isHidden = true
                                    
                                    self.wrapperButtonsView.frame.origin.y = self.addDescription_txtView.frame.origin.y + self.addDescription_txtView.frame.size.height + 20
                                    self.wrapperScrollView.contentSize.height =  self.view.frame.size.height + 100
                                    
                                    self.moodCollectionView.reloadData()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)*/
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
            }
        }
    }

    
    // MARK: - ****** Rotate Image with Fixed Orientation. ******
    func rotateImageFix(image: UIImage) -> UIImage {
        
        if (image.imageOrientation == UIImageOrientation.up ) {
            return image
        }
        
        UIGraphicsBeginImageContext(image.size)
        
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return copy!
    }
    
 
    
    //MARK: Common Methodes
    func fixOrientation(img: UIImage) -> UIImage {
        
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
 
    
    // MARK: - ****** viewDidDisappear ******
    override func viewDidDisappear(_ animated: Bool)
    {

    }

    
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(WiCreateConfessionViewController.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.addDescription_txtView.inputAccessoryView = keyboardToolbar
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    
    // MARK: - ****** UITextView Delegate. ******
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let maxCharacter: Int = 500

        //return true
        return (textView.text?.utf16.count ?? 0) + text.utf16.count - range.length <= maxCharacter
    }
    
    
     // MARK: - ****** startSearch_btnAction ******
    @IBAction func startSearch_btnAction(_ sender: Any) {
            self.searchGif_TxtField.text = ""
            self.searchText = ""
            self.searchGifView.isHidden = false
            self.searchGif_TxtField.becomeFirstResponder()
    }
    
    
   
    
    func isValidInput(Input:String) -> Bool {
        let myCharSet=CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let output: String = Input.trimmingCharacters(in: myCharSet.inverted)
        let isValid: Bool = (Input == output)
        
        return isValid
    }
    
    // MARK: - ****** UITextField Delegate ******
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
        
            if self.searchGif_TxtField.text != ""
            {
                self.differentiateStrForGifs = "searchNewGifs"
                
                self.searchText = self.searchGif_TxtField.text!
                self.searchText = self.searchText.replacingOccurrences(of: " ", with: "")
                
                if isValidInput(Input: self.searchText) == false {
                    let alert = UIAlertController(title: "", message:"Search by name field accepts only alphabatics", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    self.gifArray = []
                    self.giphyCollectionView.isHidden = true
                    self.loadingView.isHidden = false
                    self.searchOffset = 0
                    self.loadMoreBtn.isUserInteractionEnabled = false
                    searchText = removeSpecialCharsFromString(text: searchText) // "pre"
                    self.getSearchGifs(gifType: searchText, apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25, pageOffset: self.searchOffset)
                }
            }
            else
            {
                self.differentiateStrForGifs = "allGifs"
                self.searchGifView.isHidden = true
                self.offset = 0
                self.gifArray = []
                self.giphyCollectionView.isHidden = true
                self.loadingView.isHidden = false
                self.getGifs(gifType: "applause+actions+awww+celebrities", apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25,pageOffset: self.offset)
            }
        }
        return true
    }
    
    
    // MARK: - ****** cancelSearchGif_btnAction ******
    @IBAction func cancelSearcgGif_btnAction(_ sender: Any) {
        self.searchGifView.isHidden = true
        self.searchGif_TxtField.text = ""
        self.differentiateStrForGifs = "allGifs"
        self.searchText = ""
        self.searchGif_TxtField.resignFirstResponder()
        
        if (self.differentiateStrForGifs == "allGifs")
        {
            self.differentiateStrForGifs = "allGifs"
            self.searchGifView.isHidden = true
            self.offset = 0
            self.gifArray = []
            self.searchText = ""
            self.giphyCollectionView.isHidden = true
            self.loadingView.isHidden = false
            self.getGifs(gifType: "applause+actions+awww+celebrities", apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25,pageOffset: self.offset)
        }
        else{
            self.differentiateStrForGifs = "searchNewGifs"
            self.searchText = self.searchGif_TxtField.text!
            self.searchText = self.searchText.replacingOccurrences(of: " ", with: "")
            
            if isValidInput(Input: self.searchText) == false {
                let alert = UIAlertController(title: "", message:"Search by name field accepts only alphabatics", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
            else{
                self.gifArray = []
                self.giphyCollectionView.isHidden = true
                self.loadingView.isHidden = false
                self.searchOffset = 0
                self.loadMoreBtn.isUserInteractionEnabled = false
                searchText = removeSpecialCharsFromString(text: searchText) // "pre"
                self.getSearchGifs(gifType: searchText, apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25, pageOffset: self.searchOffset)
            }
        }
    }
    
    
    // MARK: - ****** Load More Gif's Button Action ******
    @IBAction func loadMoreGif_btnAction(_ sender: Any) {
        
         self.moreScroll = false
         if (self.differentiateStrForGifs == "allGifs")
         {
            //+reactions+stickers+nature
            self.getGifs(gifType: "applause+actions+awww+celebrities", apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25,pageOffset: self.offset)
         }
        else{
            if (self.searchText != "")
            {
                self.loadMoreBtn.isUserInteractionEnabled = false
                searchText = removeSpecialCharsFromString(text: searchText) // "pre"
               self.getSearchGifs(gifType: self.searchText, apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25, pageOffset: self.searchOffset)
            }
            else{
//                print("self.searchText is blank")
            }
            
         }
        
    }
    
    
    
    // MARK: - ****** Method For Sending Push Notification to the Post's Owner. ******
    func getGifs(gifType: String, apiKey: String, limit: Int,pageOffset: Int)
    {
        //self.lodaingView.isHidden = false
        let url = URL(string: "https://api.giphy.com/v1/gifs/search?q=\(gifType)&api_key=\(apiKey)&limit=\(limit)&offset=\(pageOffset)")
        
        if (url != nil){
            URLSession.shared.dataTask(with: url!, completionHandler: {
                (data, response, error) in
                if(error != nil){
                    self.gifArray = []
                    self.loadMoreBtn.isHidden = true
                    self.loadingView.isHidden = true
                    self.giphyCollectionView.isHidden = true
                    self.noGifAvailableView.isHidden = false
                    self.loadingView.isHidden = true
                }else{
                    do{
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! NSDictionary
//                        print("json = \(json)")
//                        print("json keys = \(json.allKeys)")
                        
                        let data = json.value(forKey: "data") as! NSArray
                        
                        self.gifArray.addObjects(from: data as! [Any])
//                        print("gifArray = \(self.gifArray)")
                        
                        let pageDict:NSDictionary = json.value(forKey: "pagination") as! NSDictionary
                        
                        self.offset = pageDict.value(forKey: "offset") as! Int
                        
                        if(self.offset == 119) {
                            self.moreScroll = false
                        } else
                        {
                            self.offset = self.offset + 25
                            self.moreScroll = true
                        }
   
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                            self.giphyCollectionView.isHidden = false
                            self.noGifAvailableView.isHidden = true
                            
                            self.loadMoreBtn.isHidden = false
                            self.giphyCollectionView.isHidden = false
                            self.loadingView.isHidden = true
                            self.giphyCollectionView.reloadData()
                        }
                        
                    }catch let error as NSError {
                        self.gifArray = []
                        self.loadMoreBtn.isHidden = true
                        self.loadingView.isHidden = true
                        self.giphyCollectionView.isHidden = true
                        self.noGifAvailableView.isHidden = false
                        self.loadingView.isHidden = true
                    }
                }
            }).resume()
        }
    }
    
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_'".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    // MARK: - ****** Method For Sending Push Notification to the Post's Owner. ******
    func getSearchGifs(gifType: String, apiKey: String, limit: Int,pageOffset: Int)
    {
       if (pageOffset >= 0)
       {
        let url = URL(string: "https://api.giphy.com/v1/gifs/search?q=\(gifType)&api_key=\(apiKey)&limit=\(limit)&offset=\(pageOffset)")
        
//        print("url for api call = \(String(describing: url))")
        
        if (url != nil){
            URLSession.shared.dataTask(with: url!, completionHandler: {
                (data, response, error) in
                if(error != nil)
                {
                    print("error")
                    self.gifArray = []
                    self.loadMoreBtn.isHidden = true
                    self.loadingView.isHidden = true
                    self.giphyCollectionView.isHidden = true
                    self.noGifAvailableView.isHidden = false
                    self.giphyCollectionView.isHidden = true
                    self.loadingView.isHidden = true
                    
                    self.loadMoreBtn.isUserInteractionEnabled = true
                    
                }
                else
                {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! NSDictionary
                        
//                        print("json = \(json)")
//                        print("json keys = \(json.allKeys)")
                        
                        if (json["data"] != nil)
                        {
                            let data = json.value(forKey: "data") as! NSArray
                            
                            if (data != [])
                            {
                                self.gifArray.addObjects(from: data as! [Any])
                                
                                let pageDict:NSDictionary = json.value(forKey: "pagination") as! NSDictionary
                                
                                self.searchOffset = pageDict.value(forKey: "offset") as! Int
                                
                                if(self.searchOffset == 119) {
                                    self.moreScroll = false
                                } else {
                                    self.searchOffset = self.searchOffset + 25
                                    self.moreScroll = true
                                }
                                
                                DispatchQueue.main.async {
                                    self.loadMoreBtn.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    self.giphyCollectionView.isHidden = false
                                    self.noGifAvailableView.isHidden = true
                                    
                                    
                                    self.loadMoreBtn.isHidden = false
                                    self.giphyCollectionView.isHidden = false
                                    self.loadingView.isHidden = true
                                    self.loadMoreBtn.isUserInteractionEnabled = true
                                    self.giphyCollectionView.reloadData()
                                    
                                }
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                    self.gifArray = []
                                    self.loadMoreBtn.isHidden = true
                                    self.loadingView.isHidden = true
                                    self.giphyCollectionView.isHidden = true
                                    self.noGifAvailableView.isHidden = false
                                    self.loadMoreBtn.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                }
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self.gifArray = []
                                self.loadMoreBtn.isHidden = true
                                self.loadingView.isHidden = true
                                self.giphyCollectionView.isHidden = true
                                self.noGifAvailableView.isHidden = false
                                self.loadingView.isHidden = true
                                self.loadMoreBtn.isUserInteractionEnabled = true
                            }
                        }
                        
                        
                    }catch let error as NSError {
                        self.gifArray = []
                        self.loadMoreBtn.isHidden = true
                        self.loadingView.isHidden = true
                        self.giphyCollectionView.isHidden = true
                        self.noGifAvailableView.isHidden = false
                        self.loadingView.isHidden = true
                        self.loadMoreBtn.isUserInteractionEnabled = true
                    }
                }
            }).resume()
        }else{
//            let alert = UIAlertController(title: "", message: "Please ", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
        }
       
       }
    }

    
    
    
    // MARK: - ****** findAllGif ******
    func findAllGif(gifType: String, apiKey: String, limit: Int,pageOffset: Int)
    {
      /*  let giphy = GPHClient.init(apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B")
        let op1 =  giphy.categoriesForGifs(self.offset, limit: 25, sort: "", completionHandler: { (response, error) in
            
            if let error = error as NSError? {
                // Do what you want with the error
//                print("error = \(error)")
            }
            
            if let response = response, let data = response.data, let pagination = response.pagination {
               
                for result in data {
//                    print("result = \(result)")
                }
            } else {
                print("No Top Categories Found")
            }
        })*/
    }
  
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    // MARK: - ****** UIScrollView Delegates ******
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // print("scrollViewDidScroll called")
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        print("@@@@@@@@@@@@@@@ scrollViewDidEndDragging called @@@@@@@@@@@@@@@@")
     
    }
    
    // MARK: - ****** viewDidLayoutSubviews ******
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.wrapperScrollView.isScrollEnabled=true
        self.wrapperScrollView.contentSize.height = self.selectTimeFrameView1.frame.origin.y + self.selectTimeFrameView1.frame.size.height + 200
    }
    
    // MARK: - ****** didReceiveMemoryWarning ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
