//
//  SearchPeopleViewController.swift
//  Wi Match
//
//  Created by brst on 20/03/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage
import AVKit
import AVFoundation

class getUserDetailsModel{
    var userID: String
    var userProfilePic: String
    var userName: String
    var userAge: String
    var userLocation: String
    var onlineStatus: String
    var auth_Token: String
    
    init(id: String, name: String, profilePic: String, age: String, location: String, userStatus: String, tokenAuth: String) {
        self.userID = id
        self.userName = name
        self.userAge = age
        self.userProfilePic = profilePic
        self.userLocation = location
        self.onlineStatus = userStatus
        self.auth_Token = tokenAuth
    }
}

class SearchPeopleViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout,CLLocationManagerDelegate {
    
    // MARK: - ***** VARIABLES DECLARATION *****
    var getloadedusersCount = Int()
    var placedSpotlightusersArray = [String]()
    var todayDateStr = String()
    var spotlightCheck = String()
    var profileImageURLForVideo = String()
    var userID = String()
    var applyFilterStr = String()
    var blockedUsersArray = NSMutableArray()
    var blockedByUserArray = NSMutableArray()
    var reportedUsersByMeArray = NSMutableArray()
    var applyFilterBool = Bool()
    var typeOfFilter = String()
    var filterTypeArray = [String]()
    var selectedGender = String()
    var selectedMinAge = String()
    var selectedMaxAge = String()
    var selectedMinHeight = String()
    var selectedMaxHeight = String()
    var selectedUserStatus = String()
    var selectedNearByLocation = String()
    var totalTokensPurchased = String()
    var isSpotlightCOntainsMyID = Bool()
    var countrySelected = String()
    var filterResultsDict =  NSMutableDictionary()
    var spotlightUsersDict = NSMutableDictionary()
    var filterArray = NSMutableArray()
    var filterCopyArray = NSMutableArray()
    var spotlightArray = NSMutableArray()
    var spotlightIdsArray = NSMutableArray()
    var genderSelected = String()
    var checkSpotlight = String()
    var usersDetailsDict = NSMutableDictionary()
    var countryNameArray = NSArray()
    var CountryIDArray = NSMutableArray()
    var testArray = NSMutableArray()
    var nameIDArray = NSMutableArray()
    var bothGenderBool = Bool()
    var timerForSlider = Timer()
    var currentItem: Int = 0
    var frameOfPreviousCell = CGRect()
    var ownerLatitude = Double()
    var ownerLongitude = Double()
    var myLatitude = Double()
    var myLongitude = Double()
    var currentDateStr = String()
    var checkSpotlightEndStr = String()
    
    var getAllUsersArray = [getUserDetailsModel]()
    var getAllSpotlightUsers = [getUserDetailsModel]()
    var locationManager: CLLocationManager!
    var currentLatitude = Double()
    var currentLongitude = Double()
    var paginationCount = Int()
    var countrySelectedIDStr = String()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var currentPage = Int()
    var TotalPages = Int()

    var pageCount = Int()
    var getLastKeyValue = String()
    var refreshBool = Bool()
    var refreshControl_AllUsers: UIRefreshControl!
    var refreshControl_Spotlight: UIRefreshControl!
    
    
    // MARK: - ***** OUTLETS DECLARATION *****
    @IBOutlet weak var clearFilterView: UIView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var placeMe_titleLbl: UILabel!
    @IBOutlet weak var userStatus_txtField: UITextField!
    @IBOutlet weak var countryRepresenting_txtField: UITextField!
    @IBOutlet weak var filters_pickerView: UIPickerView!
    @IBOutlet weak var spotlightPaidFeature_lbl: UILabel!
    @IBOutlet weak var filterSearchBtn: UIButton!
    @IBOutlet weak var filterSearchImage: UIImageView!
    @IBOutlet weak var filtersView: UIView!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var search_txtField: UITextField!
    @IBOutlet weak var searchInputView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var favouriteView: UIView!
    @IBOutlet weak var searchPeople_collectionView: UICollectionView!
    @IBOutlet weak var favouritePeopleSearch_collectionView: UICollectionView!
    @IBOutlet weak var gender_txtfield: UITextField!
    @IBOutlet weak var LoadingView: UIView!
    
    @IBOutlet weak var maxAge_txtField: UITextField!
    @IBOutlet weak var minAge_txtField: UITextField!
    @IBOutlet weak var applyFilterWrapperView: UIView!
    @IBOutlet weak var filterScrollView: UIScrollView!
    @IBOutlet weak var applyFilterBtnView: UIView!
    @IBOutlet weak var nodata_lbl: UILabel!
    @IBOutlet weak var nearByLocation_txtField: UITextField!
    @IBOutlet weak var loadingImage: UIImageView!
    
    
    // MARK: - ***** viewDidLoad *****
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print("appDelegate.newNameIDArray = \(appDelegate.newNameIDArray)")
//        print("appDelegate.getAllDefaultValuesDict = \(appDelegate.getAllDefaultValuesDict)")
        
        self.nodata_lbl.isHidden = true
        self.spotlightPaidFeature_lbl.isHidden = true
        
        self.isSpotlightCOntainsMyID = false
        self.currentPage = 1
        self.refreshControl_AllUsers = UIRefreshControl()
       
        self.refreshControl_AllUsers.backgroundColor = UIColor.clear
        self.refreshControl_AllUsers.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.refreshControl_AllUsers.addTarget(self, action: #selector(refreshAllUsersData), for: .valueChanged)
        self.searchPeople_collectionView.addSubview(self.refreshControl_AllUsers)
        
        self.refreshControl_Spotlight = UIRefreshControl()
  
        self.refreshControl_Spotlight.backgroundColor = UIColor.clear
        self.refreshControl_Spotlight.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.refreshControl_Spotlight.addTarget(self, action: #selector(refreshSpotlightData), for: .valueChanged)
        self.favouritePeopleSearch_collectionView.addSubview(self.refreshControl_Spotlight)
        
        let startDate = Date()
        
        self.refreshBool = false
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.currentDateStr = dateFormatter.string(from: startDate ) //pass Date here
        
        self.spotlightCheck = ""
        favouritePeopleSearch_collectionView.delegate = self
        favouritePeopleSearch_collectionView.dataSource = self
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 10)
        
        if UIScreen.main.bounds.size.width == 414
        {
            self.placeMe_titleLbl.font = UIFont.systemFont(ofSize: 12)
        }
        else
        {
            self.placeMe_titleLbl.font = UIFont.systemFont(ofSize: 10)
        }
        
        self.applyFilterStr = "fetchAllUsersForDefault"
    
        applyFilterBool = false
        bothGenderBool = false
        self.filterScrollView.isHidden = true
        self.applyFilterWrapperView.isHidden = true
        self.LoadingView.isHidden = true
        self.LoadingView.layer.cornerRadius = 15
        self.LoadingView.layer.masksToBounds = false
        
        self.searchView.isHidden = false
        self.filtersView.isHidden = true
        self.searchPeople_collectionView.isHidden = true

        searchPeople_collectionView.delegate = self
        searchPeople_collectionView.dataSource = self
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(SearchPeopleViewController.userBlocked), name: NSNotification.Name(rawValue: "userBlocked"), object: nil)
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.nodata_lbl.isHidden = true
            self.getAllUsersArray = []
            self.getAllSpotlightUsers = []
            
            DispatchQueue.main.async {
                self.LoadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
            }
            
            self.getAllUsersListing(pageCount: self.currentPage,genderSelectedStr: "", minAgeStr: "", maxAgeStr: "", countryRepresentingStr: "", nearByStr: "", userStatusStr: "", completion: { (response) in
                
                if (response == true)
                {
                    self.refreshControl_AllUsers.endRefreshing()
                    self.LoadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    if (self.getAllUsersArray.count <= 0)
                    {
                        self.nodata_lbl.isHidden = false
                        self.searchPeople_collectionView.isHidden = true
                    }
                    else
                    {
                        self.nodata_lbl.isHidden = true
                        self.searchPeople_collectionView.isHidden = false
                    }
                    
                    if (self.getAllSpotlightUsers.count <= 0)
                    {
                        self.spotlightPaidFeature_lbl.isHidden = false
                        self.favouritePeopleSearch_collectionView.isHidden = true
                        self.spotlightPaidFeature_lbl.text = "Get instantly notice by placing your profile in spotlight for 7 days! "
                    }
                    else
                    {
                        self.spotlightPaidFeature_lbl.isHidden = true
                        self.favouritePeopleSearch_collectionView.isHidden = false
                    }
                    
                    self.favouritePeopleSearch_collectionView.reloadData()
                    self.searchPeople_collectionView.reloadData()
                    
                    if !self.timerForSlider.isValid
                    {
                        self.timerForSlider =  Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.moveToNextPage), userInfo: nil, repeats: true)
                    }
                }
                else
                {
                    self.refreshControl_AllUsers.endRefreshing()
                    self.LoadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.getAllUsersArray = []
                    self.getAllSpotlightUsers = []
                    self.favouritePeopleSearch_collectionView.reloadData()
                    self.searchPeople_collectionView.reloadData()
                }
                
                if (self.currentPage < self.TotalPages)
                {
                    self.currentPage = self.currentPage + 1
                    self.paginationCount = self.paginationCount + 1
                    self.refreshBool = true
                    
                  DispatchQueue.main.async {
                        
                        self.getAllUsersListing(pageCount: self.currentPage, genderSelectedStr: self.gender_txtfield.text!, minAgeStr: self.minAge_txtField.text!, maxAgeStr: self.maxAge_txtField.text!, countryRepresentingStr: self.countrySelectedIDStr, nearByStr: self.nearByLocation_txtField.text!, userStatusStr: self.userStatus_txtField.text!, completion: { (response) in
                            
                            self.refreshControl_AllUsers.endRefreshing()
                            
                            if (response == true)
                            {
                                self.LoadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.refreshControl_AllUsers.endRefreshing()
                                if (self.getAllUsersArray.count <= 0)
                                {
                     
                                }
                                else
                                {
                                    self.nodata_lbl.isHidden = true
                                    self.searchPeople_collectionView.isHidden = false
                                }
                                
                                if (self.getAllSpotlightUsers.count <= 0)
                                {
                                    self.spotlightPaidFeature_lbl.text = "Get instantly notice by placing your profile in spotlight for 7 days! "
                                    self.spotlightPaidFeature_lbl.isHidden = false
                                   self.favouritePeopleSearch_collectionView.isHidden = true
                                }
                                else
                                {
                                    self.spotlightPaidFeature_lbl.isHidden = true
                                 self.favouritePeopleSearch_collectionView.isHidden = false
                                }
                                
                                self.favouritePeopleSearch_collectionView.reloadData()
                                self.searchPeople_collectionView.reloadData()
                            }
                            else
                            {
                                self.refreshControl_AllUsers.endRefreshing()
                                self.LoadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.favouritePeopleSearch_collectionView.reloadData()
                                self.searchPeople_collectionView.reloadData()
                            }
                        })
                    }
                }
            })
             self.populateCustomers()
        }
    }
    
    // MARK: - ****** didUpdateLocations ******
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.locationManager.location != nil
        {
            if let getCoordinates = manager.location?.coordinate
            {
                print("getCoordinates = \(getCoordinates)")
                
                let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.locationManager.stopUpdatingLocation()
                
                if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                    let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in // this is the last line that is being called
                        var placemark : CLPlacemark!
                        placemark = placemarks?[0]
                        
                        if placemark != nil
                        {
                           /* var vv = String()
                            if (placemark.subLocality != nil)
                            {
                                vv = placemark.subLocality!
                            }
                            
                            var countWhiteSpaces = Int()
                            if (vv.range(of: " ") != nil)
                            {
                                countWhiteSpaces = (vv.components(separatedBy: " ").count) - 1
                            }
                            else{
                                countWhiteSpaces = 0
                            }
                            
                            var objAtIndexBeforeLast = String()
                            var objAtIndexLast = String()
                            if (countWhiteSpaces > 0)
                            {
                                var fullNameArr = vv.components(separatedBy: " ")
                                
                                objAtIndexBeforeLast = (fullNameArr[(fullNameArr.count) - 2])
                                
                                objAtIndexLast = (fullNameArr.last)!
                                
                            }
                            else
                            {
                            }
                            
                            if (placemark.locality != nil)
                            {
                            }
                            
                            var city = String()
                            if (placemark.addressDictionary?["City"] != nil)
                            {
                                city = (placemark.addressDictionary?["City"] as! String)
                            }
                            
                            var country = String()
                            if (placemark.addressDictionary?["Country"] != nil)
                            {
                                country = (placemark.addressDictionary?["Country"] as! String)
                            }
                            
                            var state = String()
                            if (placemark.addressDictionary?["State"] != nil)
                            {
                                state = (placemark.addressDictionary?["State"] as! String)
                            }*/
                        }
                    }
                }
            }
        }
    }
    
    func getAllUsersListing(pageCount: Int,genderSelectedStr: String, minAgeStr: String, maxAgeStr: String, countryRepresentingStr: String, nearByStr: String, userStatusStr: String,completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                
                ApiHandler.getAllUsers(userID: userID, authToken: userToken, genderSelected: genderSelectedStr, minAge: minAgeStr, maxAge: maxAgeStr, countryRepresentingStr: countryRepresentingStr, nearByStr: nearByStr, userStatusStr: userStatusStr,currentLatitude: self.currentLatitude,currentLongitude: self.currentLongitude,paginationValue: pageCount, completion: { (responseData) in
                    
                    self.getAllSpotlightUsers = []
                 
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        
                        let listingArray = dataDict?.value(forKey: "list") as! NSArray
                        if (dataDict!["meta"] != nil)
                        {
                            let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                            self.TotalPages = (metaDict?.value(forKey: "totalpages") as? Int)!
                        }
                        
                        self.totalTokensPurchased = (dataDict?.value(forKey: "my_tokens")  as? String)!
                        
                        if (self.totalTokensPurchased == "")
                        {
                            self.totalTokensPurchased = "0"
                        }
                        
                        let spotlightArray = dataDict?.value(forKey: "spotlight") as! NSArray
                        
                        for index in 0..<listingArray.count
                        {
                            var userDict = NSDictionary()
                            userDict = listingArray[index] as! NSDictionary
                            
                            let u_id: String = userDict.value(forKey: "id") as! String
                            let u_age: String = userDict.value(forKey: "age") as! String
                            let u_name: String = userDict.value(forKey: "username") as! String
                            let u_location: String = userDict.value(forKey: "location") as! String
                            let u_pic: String = userDict.value(forKey: "profile_pic") as! String
                            let u_status: String = userDict.value(forKey: "online_status") as! String
                            let u_token: String = userDict.value(forKey: "auth_token") as! String
                            
                            let userDetails = getUserDetailsModel.init(id: u_id, name: u_name, profilePic: u_pic, age: u_age, location: u_location, userStatus: u_status,tokenAuth: u_token)
                            
                            
                            if (u_id != userID)
                            {
                                
                                self.getAllUsersArray.append(userDetails)
                            }
                        }
                        
                        for index in 0..<spotlightArray.count
                        {
                            var userDict = NSDictionary()
                            userDict = spotlightArray[index] as! NSDictionary
                            
                            let allKeys = userDict.allKeys as! [String]
                            
                            var u_status = String()
                            if (allKeys.contains("online_status"))
                            {
                                u_status = userDict.value(forKey: "online_status") as! String
                            }
                            else{
                                u_status = "0"
                            }
                            
                            var u_token = String()
                            if (allKeys.contains("auth_token"))
                            {
                                u_token = userDict.value(forKey: "auth_token") as! String
                            }
                            else
                            {
                                u_token = ""
                            }
                            
                            let u_id: String = userDict.value(forKey: "id") as! String
                            let u_age: String = userDict.value(forKey: "age") as! String
                            let u_name: String = userDict.value(forKey: "username") as! String
                            let u_pic: String = userDict.value(forKey: "profile_pic") as! String
                            
                            let userDetails = getUserDetailsModel.init(id: u_id, name: u_name, profilePic: u_pic, age: u_age, location: "", userStatus: u_status,tokenAuth: u_token)
                            
                           self.getAllSpotlightUsers.append(userDetails)
                            
                            if (u_id == userID)
                            {
                                self.isSpotlightCOntainsMyID = true
                            }
                        }
                        
                        completion(true)
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
                        self.view.isUserInteractionEnabled = true
                        self.LoadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        completion(false)
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.LoadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        completion(false)
                    }
                })
            }
        }
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let offset = scrollView.contentOffset
        let inset = scrollView.contentInset
        let y: CGFloat = offset.x - inset.left
        let reload_distance: CGFloat = -75
        if y < reload_distance{
            scrollView.bounces = false
        }
        
        if (self.searchPeople_collectionView.contentOffset.y >= (self.searchPeople_collectionView.contentSize.height - self.searchPeople_collectionView.frame.size.height))
        {
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                print("Connected via WWAN")
              
            }
        }
        else if (scrollView == self.favouritePeopleSearch_collectionView)
        {
            print("self.favouritePeopleSearch_collectionView")
        }
    }
    
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if (scrollView == self.searchPeople_collectionView)
        {
            if (self.searchPeople_collectionView.contentOffset.y >= (self.searchPeople_collectionView.contentSize.height - self.searchPeople_collectionView.frame.size.height) ) {
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    
                    if self.currentPage < self.TotalPages {
                        
                        let bottomEdge: Float = Float(self.searchPeople_collectionView.contentOffset.y + searchPeople_collectionView.frame.size.height)
                        
                        if bottomEdge >= Float(searchPeople_collectionView.contentSize.height)  {
                            
                            print("we are at end of table")
                            
                            self.currentPage = self.currentPage + 1
                            
                            self.paginationCount = self.paginationCount + 1
                            self.refreshBool = true
                                
                        DispatchQueue.main.async {
                            
                            self.getAllSpotlightUsers = []
                            
                            self.getAllUsersListing(pageCount: self.currentPage, genderSelectedStr: self.gender_txtfield.text!, minAgeStr: self.minAge_txtField.text!, maxAgeStr: self.maxAge_txtField.text!, countryRepresentingStr: self.countrySelectedIDStr, nearByStr: self.nearByLocation_txtField.text!, userStatusStr: self.userStatus_txtField.text!, completion: { (response) in
                                        
                                        if (response == true)
                                        {
                                            self.refreshControl_AllUsers.endRefreshing()
                                            if (self.getAllUsersArray.count <= 0)
                                            {
                                            }
                                            else
                                            {
                                                self.nodata_lbl.isHidden = true
                                             self.searchPeople_collectionView.isHidden = false
                                            }
                                            
                                        if (self.getAllSpotlightUsers.count <= 0)
                                        {
                                            self.spotlightPaidFeature_lbl.text = "Get instantly notice by placing your profile in spotlight for 7 days! "
                                             self.spotlightPaidFeature_lbl.isHidden = false
                                               
                                             self.favouritePeopleSearch_collectionView.isHidden = true
                                        }
                                        else
                                        {
                                            self.spotlightPaidFeature_lbl.isHidden = true
                                            self.favouritePeopleSearch_collectionView.isHidden = false
                                        }
                                            
                                            DispatchQueue.main.async {
                                                
                                                let indexrow = self.searchPeople_collectionView.numberOfItems(inSection: 0)
                                                
                                                var itemsArray = [IndexPath]()
                                                
                                                for index in indexrow..<self.getAllUsersArray.count
                                                {
                                                    let indexpath = IndexPath.init(row: index, section: 0)
                                                    itemsArray.append(indexpath)
                                                }
                                                
                                                self.searchPeople_collectionView.insertItems(at: itemsArray)
                                                
                                                if self.favouritePeopleSearch_collectionView.numberOfItems(inSection: 0) != self.getAllSpotlightUsers.count
                                                {
                                                    self.favouritePeopleSearch_collectionView.reloadData()
                                                }
                                            }
                                            
                                            if !self.timerForSlider.isValid
                                            {
                                                self.timerForSlider =  Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.moveToNextPage), userInfo: nil, repeats: true)
                                            }
                                        }
                                        else
                                        {
                                            self.refreshControl_AllUsers.endRefreshing()
                                            self.LoadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                        self.favouritePeopleSearch_collectionView.reloadData()
                                            self.searchPeople_collectionView.reloadData()
                                        }
                                    })
                                }
                        }
                    }
                }
            }
        }
        else if (scrollView == self.favouritePeopleSearch_collectionView)
        {
            print("self.favouritePeopleSearch_collectionView")
        }
        
    }
    

    // MARK: - ***** viewWillAppear *****
    override func viewWillAppear(_ animated: Bool) {
        let startDate = Date()
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.todayDateStr = dateFormatter.string(from: startDate) //pass Date here
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if (appDelegate.getValueFromSpotlight == true)
        {
            appDelegate.getValueFromSpotlight = false
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
             
                self.getUserDetails()
                self.populateCustomers()
            }
        }
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let CLLocationManagerStatus = CLLocationManager.authorizationStatus()
        switch CLLocationManagerStatus {
        case .notDetermined , .denied , .restricted :
            
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
            }
            break
            
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        /*default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
       
    }
    
    
    func refreshSpotlightData(_ sender: Any) {
       
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
  
            self.refreshBool = true
        }
    }

    
    func refreshAllUsersData(_ sender: Any) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            self.pageCount += 15
            self.refreshBool = true

        }
    }
    
    
    

    //MARK: - ***** viewDidDisappear *****
    override func viewDidDisappear(_ animated: Bool)
    {
        
        
    }
    
    
    //MARK: - ***** userBlocked *****
    func userBlocked()
    {
        
        self.applyFilterStr = "fetchAllUsersForDefault"
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.currentPage = 1
            
             self.getAllSpotlightUsers = []
            self.getAllUsersArray = []
            DispatchQueue.main.async {
                self.LoadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
            }
            self.getAllUsersListing(pageCount: self.currentPage, genderSelectedStr: "", minAgeStr: "", maxAgeStr: "", countryRepresentingStr: "", nearByStr: "", userStatusStr: "", completion: { (response) in
                
                if (response == true)
                {
                    self.refreshControl_AllUsers.endRefreshing()
                    self.LoadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    if (self.getAllUsersArray.count <= 0)
                    {
                        self.nodata_lbl.isHidden = false
                        self.searchPeople_collectionView.isHidden = true
                    }
                    else
                    {
                        self.nodata_lbl.isHidden = true
                        self.searchPeople_collectionView.isHidden = false
                    }
                    
                    if (self.getAllSpotlightUsers.count <= 0)
                    {
                        self.spotlightPaidFeature_lbl.isHidden = false
                        self.favouritePeopleSearch_collectionView.isHidden = true
                        self.spotlightPaidFeature_lbl.text = "Get instantly notice by placing your profile in spotlight for 7 days! "
                    }
                    else
                    {
                        self.spotlightPaidFeature_lbl.isHidden = true
                        self.favouritePeopleSearch_collectionView.isHidden = false
                    }
                    
                    self.favouritePeopleSearch_collectionView.reloadData()
                    self.searchPeople_collectionView.reloadData()
                    if !self.timerForSlider.isValid
                    {
                        self.timerForSlider =  Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.moveToNextPage), userInfo: nil, repeats: true)
                    }
                }
                else
                {
                    self.refreshControl_AllUsers.endRefreshing()
                    self.LoadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.getAllUsersArray = []
                    self.getAllSpotlightUsers = []
                    self.favouritePeopleSearch_collectionView.reloadData()
                    self.searchPeople_collectionView.reloadData()
                }
            })
        }
    }
 
    
    // MARK: - ***** populateCustomers *****
    func populateCustomers() {
        let db = FMDBDataAccess()
        self.countryNameArray = db.getCustomers()
        var country = String()
        var countryID = String()
        var countryObj = Country()
        for i in 0..<self.countryNameArray.count {
            countryObj = self.countryNameArray[i] as! Country
            country = "\(countryObj.firstName!)"
            countryID = "\(countryObj.customerId)"
            testArray.add(country)
            CountryIDArray.add(countryID)
        }
        
        for i in 0..<testArray.count {
            var dict1 = [AnyHashable: Any]()
            dict1["c_name"] = testArray[i]
            dict1["c_id"] = CountryIDArray[i]
            let country_id: String = "\(CountryIDArray[i])"
            if !nameIDArray.contains(country_id) {
                nameIDArray.insert(dict1, at: i)
            }
        }
    }

    
    // MARK: - ***** getUserDetails *****
    func getUserDetails()
    {
        if (UserDefaults.standard.value(forKey: "userDetailsDict") != nil)
        {
             usersDetailsDict = UserDefaults.standard.value(forKey: "userDetailsDict") as! NSMutableDictionary
            
            let keysArray = usersDetailsDict.allKeys as! [String]
            if keysArray.contains("CurrentLongitude")
            {
                self.myLongitude = (usersDetailsDict.value(forKey: "CurrentLongitude") as? Double)!
            }
            else{
                self.myLongitude = 0.0
            }
            if keysArray.contains("CurrentLatitude")
            {
                self.myLatitude = (usersDetailsDict.value(forKey: "CurrentLatitude") as? Double)!
            }
            else{
                self.myLatitude = 0.0
            }
            
              self.favouritePeopleSearch_collectionView.isHidden = false
              self.spotlightPaidFeature_lbl.isHidden = true
        }
    }
   
    
    // MARK: - ***** moveToNextPage *****
     func moveToNextPage()
     {
       if currentItem < self.getAllSpotlightUsers.count
        {
            self.favouritePeopleSearch_collectionView.scrollToItem(at: IndexPath(row: currentItem, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
            self.currentItem += 1
        }
        else
        {
             self.currentItem = 0
        }
    }
    
    
    // MARK: - ***** UICollectionView DataSource & Delegate *****
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.searchPeople_collectionView
        {
            return self.getAllUsersArray.count
        }
        else if collectionView == self.favouritePeopleSearch_collectionView
        {
            return self.getAllSpotlightUsers.count
        }
        else
        {
            return 0
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let index = indexPath.row
        if (collectionView == self.searchPeople_collectionView)
        {
            // get a reference to our storyboard cell
            let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchCell", for: indexPath as IndexPath) as! SearchPeopleCollectionViewCell
            
            if index%3 == 0
            {
                frameOfPreviousCell = searchCell.frame
            }
            
            if (index - 1) % 3 == 0 && index > 0
            {
                print("frameOfPreviousCell.origin.y = \(frameOfPreviousCell.origin.y)")
                print("frameOfPreviousCell.size.width/2 = \(frameOfPreviousCell.size.width/2)")
                
                searchCell.frame = CGRect.init(x: searchCell.frame.origin.x, y: frameOfPreviousCell.origin.y+(frameOfPreviousCell.size.width/2), width: searchCell.frame.size.width, height: searchCell.frame.size.width )
            }
            
            if (self.getAllUsersArray.count <= 0)
            {
               return searchCell
            }
            else
            {
              let urlStr = self.getAllUsersArray[indexPath.row].userProfilePic
                
                DispatchQueue.main.async {
                    
                    searchCell.usersStatusOnlineImage.isHidden = true
                    searchCell.usersProfileImage.sd_setImage(with: URL(string: urlStr), placeholderImage: UIImage(named: ""), completed: { (image, err, imageCacheType, imageUrl) in
                        
                        if (self.getAllUsersArray.count > 0)
                        {
                            let usersStatus = self.getAllUsersArray[indexPath.row].onlineStatus
                            if usersStatus == "1"
                            {
                                searchCell.usersStatusOnlineImage.isHidden = false
                                searchCell.usersStatusOnlineImage.image = UIImage(named: "Online Icon")
                                searchCell.usersStatusOnlineImage.contentMode = UIViewContentMode.scaleAspectFit
                            }
                            else
                            {
                                searchCell.usersStatusOnlineImage.isHidden = true
                                searchCell.usersStatusOnlineImage.image = UIImage(named: "Offline Icon")
                                searchCell.usersStatusOnlineImage.contentMode = UIViewContentMode.scaleAspectFit
                            }
                        }
                        else
                        {
                            searchCell.usersStatusOnlineImage.isHidden = true
                            searchCell.usersStatusOnlineImage.image = UIImage(named: "Offline Icon")
                            searchCell.usersStatusOnlineImage.contentMode = UIViewContentMode.scaleAspectFit
                        }
                        
                    })
                }
                
                searchCell.usersProfileImage.layer.cornerRadius = searchCell.usersProfileImage.frame.size.height/2
                searchCell.usersProfileImage.clipsToBounds = true
                searchCell.usersProfileImage.layer.masksToBounds = true
                searchCell.usersProfileImage.contentMode = UIViewContentMode.scaleAspectFill
                
                return searchCell
            }

        }
        else if collectionView == self.favouritePeopleSearch_collectionView
        {
            let favouriteSearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "favouriteSearchCell", for: indexPath as IndexPath) as! FavoriteListPeopleCollectionViewCell
            
            if (self.getAllSpotlightUsers.count <= 0)
            {
                return favouriteSearchCell
            }
            else
            {
                if (self.getAllSpotlightUsers[indexPath.row].userProfilePic == nil)
                {
//                    let urlStr = self.getAllSpotlightUsers[indexPath.row].userProfilePic
                    favouriteSearchCell.onlineUserIcon.isHidden = true
                    favouriteSearchCell.usersProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: ""), completed: { (image, err, imageCacheType, imageUrl) in
                        
                        let usersStatus = self.getAllSpotlightUsers[indexPath.row].onlineStatus
                        if usersStatus == "1"
                        {
                            favouriteSearchCell.onlineUserIcon.isHidden = false
                            favouriteSearchCell.onlineUserIcon.image = UIImage(named: "Online Icon")
                            favouriteSearchCell.onlineUserIcon.contentMode = UIViewContentMode.scaleAspectFit
                        }
                        else
                        {
                            favouriteSearchCell.onlineUserIcon.isHidden = true
                            favouriteSearchCell.onlineUserIcon.image = UIImage(named: "Offline Icon")
                            favouriteSearchCell.onlineUserIcon.contentMode = UIViewContentMode.scaleAspectFit
                        }
                        
                    })
                }
                else{
                    let urlStr = self.getAllSpotlightUsers[indexPath.row].userProfilePic
                    favouriteSearchCell.onlineUserIcon.isHidden = true
                    favouriteSearchCell.usersProfileImage.sd_setImage(with: URL(string: urlStr), placeholderImage: UIImage(named: ""), completed: { (image, err, imageCacheType, imageUrl) in
                        
                        let usersStatus = self.getAllSpotlightUsers[indexPath.row].onlineStatus
                        if usersStatus == "1"
                        {
                            favouriteSearchCell.onlineUserIcon.isHidden = false
                            favouriteSearchCell.onlineUserIcon.image = UIImage(named: "Online Icon")
                            favouriteSearchCell.onlineUserIcon.contentMode = UIViewContentMode.scaleAspectFit
                        }
                        else
                        {
                            favouriteSearchCell.onlineUserIcon.isHidden = true
                            favouriteSearchCell.onlineUserIcon.image = UIImage(named: "Offline Icon")
                            favouriteSearchCell.onlineUserIcon.contentMode = UIViewContentMode.scaleAspectFit
                        }
                    })
                }
       
                return favouriteSearchCell
            }
        }
        else
        {
            let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchCell", for: indexPath as IndexPath) as! SearchPeopleCollectionViewCell
            return searchCell
        }
    }
    
    
    // MARK: - ***** Generate Thumbnail from video Url. *****
    func createThumbnailOfVideoFromFileURL(_ strVideoURL: String) -> UIImage?{
        let asset = AVAsset(url: URL(string: strVideoURL)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), 100)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            /* error handling here */
            var thumbnailImage = UIImage()
            thumbnailImage = self.createThumbnailOfVideoFromFileURL(self.profileImageURLForVideo)!
            return thumbnailImage
        }
    }

    
    // MARK: - ***** UICOllectionView DataSource & Delegate *****
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
          if collectionView == self.searchPeople_collectionView{
            if (self.getAllUsersArray.count > 0)
            {
                let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                viewProfileScreen.ownerID = self.getAllUsersArray[indexPath.row].userID
                viewProfileScreen.ownerAuthToken = self.getAllUsersArray[indexPath.row].auth_Token
                self.navigationController?.pushViewController(viewProfileScreen, animated: true)
            }
 
        }
        else if collectionView == self.favouritePeopleSearch_collectionView {
            
            if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if id != self.getAllSpotlightUsers[indexPath.row].userID
                {
                    let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                    viewProfileScreen.ownerID = self.getAllSpotlightUsers[indexPath.row].userID
                    viewProfileScreen.ownerAuthToken = self.getAllSpotlightUsers[indexPath.row].auth_Token
                    self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                }
                else
                {
                    let profileView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                    self.navigationController?.pushViewController(profileView, animated: true)
                }
            }
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == favouritePeopleSearch_collectionView
        {
            var frameOfCell = favouritePeopleSearch_collectionView.frame as CGRect
            frameOfCell.origin.y = 0
            
            favouritePeopleSearch_collectionView.frame = frameOfCell
            
            return CGSize.init(width: favouritePeopleSearch_collectionView.frame.size.height, height: favouritePeopleSearch_collectionView.frame.size.height)
        }
        else
        {
             return CGSize.init(width: 100, height: 100)
        }
    }
    
    
    //MARK: - ***** Apply Filters Button *****
    @IBAction func applyFilters_btnAction(_ sender: Any) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            
            // showing alert when all fields are empty
            if self.gender_txtfield.text == "" && self.minAge_txtField.text == "" && self.maxAge_txtField.text == "" && self.countryRepresenting_txtField.text == "" && self.userStatus_txtField.text == "" && self.nearByLocation_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select atleast 1 to filter users.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
           // Filter when only Gender selected
            else if ((self.gender_txtfield.text != "") && ((self.minAge_txtField.text == "") && (self.maxAge_txtField.text == ""))  && ((self.countryRepresenting_txtField.text == "") && (self.userStatus_txtField.text == "") && (self.nearByLocation_txtField.text == "")))
            {
                self.applyFilterStr = "fetchUsers_usingGenderSearch"
                self.filterUsers()
            }
            // Filter when only NearBy selected
            else if ((self.nearByLocation_txtField.text != "") && ((self.minAge_txtField.text == "") && (self.maxAge_txtField.text == ""))  && ((self.countryRepresenting_txtField.text == "") && (self.userStatus_txtField.text == "") && (self.gender_txtfield.text == "")))
            {
                self.applyFilterStr = "fetchAllNearByUsers"
                self.filterUsers()
            }
            // Filters results on the basis of Gender and NearBy
            else if ((self.gender_txtfield.text != "") && (self.nearByLocation_txtField.text != "") && (self.maxAge_txtField.text == "")  && (self.countryRepresenting_txtField.text == "")  && (self.userStatus_txtField.text == "") && (self.minAge_txtField.text == ""))
            {
                self.applyFilterStr = "fetchGenderAndNearByUsers"
                self.filterUsers()
            }
            // Filters results on the basis of Min age , Max age and NearBy
            else if ((self.gender_txtfield.text == "") && (self.nearByLocation_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.countryRepresenting_txtField.text == "")  && (self.userStatus_txtField.text == "") && (self.minAge_txtField.text != ""))
            {
                self.applyFilterStr = "fetchNearByAndAgeBasedUsers"
                self.filterUsers()
            }
            // Filters results on the basis of Country and NearBy
            else if ((self.gender_txtfield.text == "") && (self.nearByLocation_txtField.text != "") && (self.maxAge_txtField.text == "")  && (self.countryRepresenting_txtField.text != "")  && (self.userStatus_txtField.text == "") && (self.minAge_txtField.text == ""))
            {
                self.applyFilterStr = "fetchNearByUsersAndCountryBasedUsers"
                self.filterUsers()
            }
            // Filters results on the basis of Online Users and NearBy
            else if ((self.gender_txtfield.text == "") && (self.nearByLocation_txtField.text != "") && (self.maxAge_txtField.text == "")  && (self.countryRepresenting_txtField.text == "")  && (self.userStatus_txtField.text != "") && (self.minAge_txtField.text == ""))
            {
                self.applyFilterStr = "fetchOnlineAndNearByUsers"
                self.filterUsers()
            }
            // Filters results on the basis of Age, Gender and NearBy
            else if ((self.gender_txtfield.text != "") && (self.userStatus_txtField.text == "") && (self.maxAge_txtField.text != "")  && (self.minAge_txtField.text != "")  && (self.countryRepresenting_txtField.text == "") && (self.nearByLocation_txtField.text != ""))
            {
                self.applyFilterStr = "fetchGenderAgeAndNearByUsers"
                self.filterUsers()
            }
            // Filters results on the basis of Country, Gender and NearBy
            else if ((self.gender_txtfield.text != "") && (self.userStatus_txtField.text == "") && (self.maxAge_txtField.text == "")  && (self.minAge_txtField.text == "")  && (self.countryRepresenting_txtField.text != "") && (self.nearByLocation_txtField.text != ""))
            {
                self.applyFilterStr = "fetchGenderCountryAndNearByUsers"
                self.filterUsers()
            }
            // Filters results on the basis of Online, Gender and NearBy
            else if ((self.gender_txtfield.text != "") && (self.userStatus_txtField.text != "") && (self.maxAge_txtField.text == "")  && (self.minAge_txtField.text == "")  && (self.countryRepresenting_txtField.text == "") && (self.nearByLocation_txtField.text != ""))
            {
                self.applyFilterStr = "fetchGenderOnlineAndNearByUsers"
                self.filterUsers()
            }
            // Filters results on the basis of Age, Country and NearBy
            else if ((self.gender_txtfield.text == "") && (self.userStatus_txtField.text == "") && (self.maxAge_txtField.text != "")  && (self.minAge_txtField.text != "")  && (self.countryRepresenting_txtField.text != "") && (self.nearByLocation_txtField.text != ""))
            {
                self.applyFilterStr = "fetchCountryAgeAndNearByUsers"
                self.filterUsers()
            }
            // Filters results on the basis of Age, Online and NearBy
            else if ((self.gender_txtfield.text == "") && (self.userStatus_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.minAge_txtField.text != "")  && (self.countryRepresenting_txtField.text == "") && (self.nearByLocation_txtField.text != ""))
            {
                self.applyFilterStr = "fetchUsersBasedOnAgeOnlineAndNearBy"
                self.filterUsers()
            }
            // Filters results on the basis of Country, Online and NearBy
            else if ((self.gender_txtfield.text == "") && (self.userStatus_txtField.text != "") && (self.maxAge_txtField.text == "")  && (self.minAge_txtField.text == "")  && (self.countryRepresenting_txtField.text != "") && (self.nearByLocation_txtField.text != ""))
            {
                self.applyFilterStr = "fetchUsersBasedOnCountryOnlineAndNearBy"
                self.filterUsers()
            }
            // Filter when Min age and Max age selected
            else if ((self.minAge_txtField.text != "" && self.maxAge_txtField.text != "") && (self.gender_txtfield.text == "")  && (self.countryRepresenting_txtField.text == "") && (self.userStatus_txtField.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                 self.applyFilterStr = "fetchUsers_usingAgeSearch"
                self.filterUsers()
            }
            // Filter results when all the fields are filled (i.e. - Filter for all values)
            else if (self.gender_txtfield.text != "") && (self.minAge_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.countryRepresenting_txtField.text != "") && (self.userStatus_txtField.text != "") && (self.nearByLocation_txtField.text != "")
            {
                self.applyFilterStr = "fetchUsers_withAgeAndGender"
                self.filterUsers()
            }
            // Filter results when Gender, Age, Country And Online.
            else if (self.gender_txtfield.text != "") && (self.minAge_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.countryRepresenting_txtField.text != "") && (self.userStatus_txtField.text != "") && (self.nearByLocation_txtField.text == "")
            {
                self.applyFilterStr = "fetchUsersForGenderAgeCountryAndOnline"
                self.filterUsers()
                
            }
            // Filter results when Gender, Age, Country And NearBy.
            else if (self.gender_txtfield.text != "") && (self.minAge_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.countryRepresenting_txtField.text != "") && (self.userStatus_txtField.text == "") && (self.nearByLocation_txtField.text != "")
            {
                self.applyFilterStr = "fetchUsersForGenderAgeCountryAndNearBy"
                self.filterUsers()
            }
            // Filter results when Gender, Age, Online And NearBy.
            else if (self.gender_txtfield.text != "") && (self.minAge_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.countryRepresenting_txtField.text == "") && (self.userStatus_txtField.text != "") && (self.nearByLocation_txtField.text != "")
            {
                self.applyFilterStr = "fetchUsersForGenderAgeOnlineAndNearBy"
                self.filterUsers()
            }
            // Filter results when Country, Age, Online And NearBy.
            else if (self.gender_txtfield.text == "") && (self.minAge_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.countryRepresenting_txtField.text != "") && (self.userStatus_txtField.text != "") && (self.nearByLocation_txtField.text != "")
            {
                self.applyFilterStr = "fetchUsersForCountryAgeOnlineAndNearBy"
                self.filterUsers()
            }
            // Filter results when Country, Gender, Online And NearBy.
            else if (self.gender_txtfield.text != "") && (self.minAge_txtField.text == "") && (self.maxAge_txtField.text == "")  && (self.countryRepresenting_txtField.text != "") && (self.userStatus_txtField.text != "") && (self.nearByLocation_txtField.text != "")
            {
                self.applyFilterStr = "fetchUsersforGenderCountryOnlineAndNearBy"
                self.filterUsers()
            }
            // showing alert when Max age is empty
            else if (self.minAge_txtField.text != "") && (self.maxAge_txtField.text == "")
            {
                let alert = UIAlertController(title: "", message: "Please select maximum age to filter users.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            // showing alert when Min age is empty
            else if (self.minAge_txtField.text == "") && (self.maxAge_txtField.text != "")
            {
                let alert = UIAlertController(title: "", message: "Please select minimum age to filter users.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            //Filter results for country only.
            else if ((self.countryRepresenting_txtField.text != "") && (self.minAge_txtField.text == "") && (self.maxAge_txtField.text == "")  && (self.userStatus_txtField.text == "")  && (self.gender_txtfield.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                self.applyFilterStr = "onlyCountrySearch"
                self.filterUsers()
            }
            //Filter results for online users only.
            else if ((self.userStatus_txtField.text != "") && (self.minAge_txtField.text == "") && (self.maxAge_txtField.text == "")  && (self.countryRepresenting_txtField.text == "")  && (self.gender_txtfield.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                 self.applyFilterStr = "showOnlineUsersSearch"
                self.filterUsers()
                
            }
            // Filters results on the basis of Gender and Min age and max age only
            else if ((self.gender_txtfield.text != "") && (self.minAge_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.countryRepresenting_txtField.text == "")  && (self.userStatus_txtField.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                self.applyFilterStr = "filterUsersForGenderAndAgeOnly"
                
                self.filterUsers()
            }
            // Filters results on the basis of Gender and Country Representing only
            else if ((self.gender_txtfield.text != "") && (self.countryRepresenting_txtField.text != "") && (self.maxAge_txtField.text == "")  && (self.minAge_txtField.text == "")  && (self.userStatus_txtField.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                self.applyFilterStr = "fetchUsersBasedOnGenderAndCountryOnly"
                
                self.filterUsers()
            }
            // Filters results on the basis of Gender and Online Users only
            else if ((self.gender_txtfield.text != "") && (self.userStatus_txtField.text != "") && (self.maxAge_txtField.text == "")  && (self.minAge_txtField.text == "")  && (self.countryRepresenting_txtField.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                self.applyFilterStr = "fetchUsersBasedOnGenderAndOnlineOnly"
                
                self.filterUsers()
                
            }
            // Filters results on the basis of Min Age, Max Age and Country Representing only
            else if ((self.countryRepresenting_txtField.text != "") && (self.minAge_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.gender_txtfield.text == "")  && (self.userStatus_txtField.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                self.applyFilterStr = "FilterForCountryAndAge"
                
                self.filterUsers()
            }
            // Filters results on the basis of Min Age, Max Age and Online Users only
            else if ((self.userStatus_txtField.text != "") && (self.minAge_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.gender_txtfield.text == "")  && (self.countryRepresenting_txtField.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                self.applyFilterStr = "FilterForAgeAndOnlineUsers"
                
                self.filterUsers()
            }
            // Filters results on the basis of Country and Online Users only
            else if ((self.userStatus_txtField.text != "") && (self.countryRepresenting_txtField.text != "") && (self.maxAge_txtField.text == "")  && (self.gender_txtfield.text == "")  && (self.minAge_txtField.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                self.applyFilterStr = "fetchCountryAndOnlineUsers"
                
                self.filterUsers()
            }
            // Filters results on the basis of Age, Country and Online Users only
            else if ((self.gender_txtfield.text != "") && (self.countryRepresenting_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.minAge_txtField.text != "")  && (self.userStatus_txtField.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                self.applyFilterStr = "FetchUsersAgeGenderCountry"
                
                self.filterUsers()
            }
            // Filters results on the basis of Age, Gender and Online Users only
            else if ((self.gender_txtfield.text != "") && (self.userStatus_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.minAge_txtField.text != "")  && (self.countryRepresenting_txtField.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                self.applyFilterStr = "fetchUsers_AgeGenderOnline"
                
                self.filterUsers()
            }
            // Filters results on the basis of Age, Country and Online Users only
            else if ((self.countryRepresenting_txtField.text != "") && (self.userStatus_txtField.text != "") && (self.maxAge_txtField.text != "")  && (self.minAge_txtField.text != "")  && (self.gender_txtfield.text == "") && (self.nearByLocation_txtField.text == ""))
            {
                  self.applyFilterStr = "FilterUsers_AgeCountryAndOnline"
                self.filterUsers()
            }
            // Filters results on the basis of Gender, Country and Online Users only
            else if ((self.countryRepresenting_txtField.text != "") && (self.userStatus_txtField.text != "") && (self.gender_txtfield.text != "")  && (self.minAge_txtField.text == "")  && (self.maxAge_txtField.text == "")  && (self.nearByLocation_txtField.text == ""))
            {
                 self.applyFilterStr = "fetchUsers_CountryGenderAndOnlineUsers"
                self.filterUsers()
            }
        }
    }
    
    func filterUsers()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.currentPage = 1
//            self.getAllSpotlightUsers = []
            self.getAllUsersArray = []
            
            DispatchQueue.main.async {
                self.LoadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
            }
            
            self.getAllUsersListing(pageCount: self.currentPage, genderSelectedStr: self.gender_txtfield.text!, minAgeStr: self.minAge_txtField.text!, maxAgeStr: self.maxAge_txtField.text!, countryRepresentingStr: self.countrySelectedIDStr, nearByStr: self.nearByLocation_txtField.text!, userStatusStr: self.userStatus_txtField.text!, completion: { (response) in
                
                if (response == true)
                {
                    self.refreshControl_AllUsers.endRefreshing()
                    self.LoadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    if (self.getAllUsersArray.count <= 0)
                    {
                        self.LoadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.nodata_lbl.isHidden = false
                        self.favouriteView.isHidden = false
                        self.searchView.isHidden = false
                        self.filtersView.isHidden = true
                        self.filterScrollView.isHidden = true
                        self.filterSearchBtn.isHidden = false
                        self.filterSearchImage.isHidden = false
                        
                        self.getAllUsersArray = []
                        self.searchPeople_collectionView.reloadData()
                    }
                    else
                    {
                        self.nodata_lbl.isHidden = true
                        self.searchPeople_collectionView.isHidden = false
                        
                        self.favouriteView.isHidden = false
                        self.searchView.isHidden = false
                        self.filtersView.isHidden = true
                        self.filterScrollView.isHidden = true
                        self.filterSearchBtn.isHidden = false
                        self.filterSearchImage.isHidden = false
                    }
                    
                    if (self.getAllSpotlightUsers.count <= 0)
                    {
                        self.spotlightPaidFeature_lbl.isHidden = false
                        self.favouritePeopleSearch_collectionView.isHidden = true
                        self.spotlightPaidFeature_lbl.text = "Get instantly notice by placing your profile in spotlight for 7 days! "
                    }
                    else
                    {
                        self.spotlightPaidFeature_lbl.isHidden = true
                        self.favouritePeopleSearch_collectionView.isHidden = false
                    }
                    
                    self.favouritePeopleSearch_collectionView.reloadData()
                    self.searchPeople_collectionView.reloadData()
                    if !self.timerForSlider.isValid
                    {
                        self.timerForSlider =  Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.moveToNextPage), userInfo: nil, repeats: true)
                    }
                }
                else
                {
                    self.refreshControl_AllUsers.endRefreshing()
                    self.LoadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    self.favouriteView.isHidden = false
                    self.searchView.isHidden = false
                    self.filtersView.isHidden = true
                    self.filterScrollView.isHidden = true
                    self.filterSearchBtn.isHidden = false
                    self.filterSearchImage.isHidden = false
                    
                    self.getAllUsersArray = []
                    self.getAllSpotlightUsers = []
                    self.favouritePeopleSearch_collectionView.reloadData()
                    self.searchPeople_collectionView.reloadData()
                }
            })
        }
    }
    

    // MARK: - ***** placeMeHere_BtnAction *****
    @IBAction func placeMeHere_BtnAction(_ sender: Any)
    {
        if (self.isSpotlightCOntainsMyID == true)
        {
            let alert = UIAlertController(title: "", message: "You're already in the spotlight.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "SpotlightInAppViewController") as! SpotlightInAppViewController
                messagesScreen.tokenAvailableInt = Int(self.totalTokensPurchased)!
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else if (Int(self.totalTokensPurchased)! < 7)
        {
            let alert = UIAlertController(title: "", message: "You don't have enough tokens.Please buy tokens to highlight your profile in the spotlight.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "SpotlightInAppViewController") as! SpotlightInAppViewController
                messagesScreen.tokenAvailableInt = Int(self.totalTokensPurchased)!
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if (self.isSpotlightCOntainsMyID == false)
            {
                let alert = UIAlertController(title: "", message: "Redeem spotlight feature again and be seen for another 7 days.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Redeem", style: .destructive, handler: { (action: UIAlertAction!) in
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                      
                        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                        {
                            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                            {
                                self.LoadingView.isHidden = false
                                self.view.isUserInteractionEnabled = false
                                
                                ApiHandler.redeemSpotlight(userID: userID, authToken: userToken, completion: { (responseData) in
                                    
                                    if (responseData.value(forKey: "status") as? String == "200")
                                    {
                                        let alert = UIAlertController(title: "", message: "Congratulations! You will now be seen in spotlight for the next 7 days.", preferredStyle: UIAlertControllerStyle.alert)
                                        
                                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action: UIAlertAction!) in
                                            
                                            self.LoadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            self.isSpotlightCOntainsMyID = true
                                            
                                         let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "SpotlightInAppViewController") as! SpotlightInAppViewController
                                       
                                           
                                            messagesScreen.tokenAvailableInt = Int(self.totalTokensPurchased)!
                                        self.navigationController?.pushViewController(messagesScreen, animated: true)
                                        }))
                                        
                                        self.present(alert, animated: true, completion: nil)
                                        
                                    }
                                    else if (responseData.value(forKey: "status") as? String == "400")
                                    {
                                        let message = responseData.value(forKey: "message") as? String
                                        self.view.isUserInteractionEnabled = true
                                        self.LoadingView.isHidden = true
                                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        self.view.isUserInteractionEnabled = true
                                        self.LoadingView.isHidden = true
                                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                })
                            }
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Not Now", style: .cancel, handler: { (action: UIAlertAction!) in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "", message: "You're already in the spotlight.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                    
                    let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "SpotlightInAppViewController") as! SpotlightInAppViewController
                    messagesScreen.tokenAvailableInt = Int(self.totalTokensPurchased)!
                    self.navigationController?.pushViewController(messagesScreen, animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
   
    }
    
    
    // MARK: - ***** cancel_pickerView *****
    @IBAction func cancel_pickerView(_ sender: Any) {
        self.applyFilterWrapperView.isHidden = true
    }
    
    
    // MARK: - ***** done_pickerView *****
    @IBAction func done_pickerView(_ sender: Any) {
        if typeOfFilter == "gender_txtfield"
        {
            self.applyFilterWrapperView.isHidden = true
            if selectedGender == ""
            {
                self.gender_txtfield.text = filterTypeArray[0]
                self.selectedGender = filterTypeArray[0]
            }
            else{
                self.gender_txtfield.text = selectedGender
            }
        }
        else if typeOfFilter == "minAge_txtField"
        {
            self.applyFilterWrapperView.isHidden = true
            if selectedMinAge == ""
            {
                self.minAge_txtField.text = filterTypeArray[0]
                self.selectedMinAge = filterTypeArray[0]
            }
            else{
                self.minAge_txtField.text = selectedMinAge
            }
        }
        else if typeOfFilter == "maxAge_txtField"
        {
            self.applyFilterWrapperView.isHidden = true
            if selectedMaxAge == ""
            {
                self.maxAge_txtField.text = filterTypeArray[0]
                self.selectedMaxAge = filterTypeArray[0]
            }
            else{
                self.maxAge_txtField.text = selectedMaxAge
            }
        }
        else if typeOfFilter == "countryRepresenting_txtField"
        {
            self.applyFilterWrapperView.isHidden = true
            if self.countrySelectedIDStr == ""
            {
                self.countryRepresenting_txtField.text = ((nameIDArray[0] as! NSDictionary).value(forKey: "c_name") as? String)
                
                self.countrySelected = ((nameIDArray[0] as! NSDictionary).value(forKey: "c_name") as? String)!
                
                self.countrySelectedIDStr = "1"
            }
            else
            {
                self.countryRepresenting_txtField.text = countrySelected
            }
        }
        else if typeOfFilter == "userStatus_txtField"
        {
            self.applyFilterWrapperView.isHidden = true
            if selectedUserStatus == ""
            {
                self.userStatus_txtField.text = filterTypeArray[0]
                self.selectedUserStatus = filterTypeArray[0]
            }
            else{
                self.userStatus_txtField.text = selectedUserStatus
            }
        }
        else if typeOfFilter == "nearByLocation_txtField"
        {
            self.applyFilterWrapperView.isHidden = true
            if selectedNearByLocation == ""
            {
                self.nearByLocation_txtField.text = filterTypeArray[0]
                self.selectedNearByLocation = filterTypeArray[0]
            }
            else{
                self.nearByLocation_txtField.text = selectedNearByLocation
            }
        }
    }
    
    
    // MARK: - ***** UIPickerView delegates *****
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if typeOfFilter == "countryRepresenting_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                return appDelegate.newNameIDArray.count;
            }
            else
            {
                return nameIDArray.count
            }
        }
        else
        {
             return filterTypeArray.count;
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        
        var str = String()
        if typeOfFilter == "countryRepresenting_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                str = (appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "country_name") as! String
            }
            else{
                str = (nameIDArray[row] as! NSDictionary).value(forKey: "c_name") as! String
            }
        }
        else
        {
            str = filterTypeArray[row]
        }
        
        return str
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if typeOfFilter == "gender_txtfield"
        {
            selectedGender = filterTypeArray[row]  
        }
        else if typeOfFilter == "minAge_txtField"
        {
            selectedMinAge = filterTypeArray[row]
        }
        else if  typeOfFilter == "maxAge_txtField"
        {
            selectedMaxAge = filterTypeArray[row]
        }
        else if typeOfFilter == "countryRepresenting_txtField"
        {
//            countrySelected = ((nameIDArray[row] as! NSDictionary).value(forKey: "c_name") as? String)!
            
            if (appDelegate.newNameIDArray.count > 0)
            {
                countrySelected = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "country_name") as? String)!
                
                self.countrySelectedIDStr = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "id") as? String)!
            }
            else
            {
                countrySelected = ((nameIDArray[row] as! NSDictionary).value(forKey: "c_name") as? String)!
                self.countrySelectedIDStr = String(describing: row)
            }
        }
        else if typeOfFilter == "userStatus_txtField"
        {
          selectedUserStatus = filterTypeArray[row]
        }
        else if typeOfFilter == "nearByLocation_txtField"
        {
            selectedNearByLocation = filterTypeArray[row]
        }
    }

    
    
    //MARK:- ***** UITEXTFIELD DELEGATE *****
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.gender_txtfield
        {
            self.gender_txtfield.resignFirstResponder()
            self.minAge_txtField.resignFirstResponder()
            self.maxAge_txtField.resignFirstResponder()
            self.countryRepresenting_txtField.resignFirstResponder()
            self.userStatus_txtField.resignFirstResponder()
            self.nearByLocation_txtField.resignFirstResponder()
            self.selectedGender = ""
            typeOfFilter = "gender_txtfield"
            self.applyFilterWrapperView.isHidden = false
            self.filterTypeArray = ["Male","Female","Both"]
            self.filters_pickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.filters_pickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.minAge_txtField
        {
            self.gender_txtfield.resignFirstResponder()
            self.minAge_txtField.resignFirstResponder()
            self.maxAge_txtField.resignFirstResponder()
           
            self.countryRepresenting_txtField.resignFirstResponder()
            self.userStatus_txtField.resignFirstResponder()
            self.nearByLocation_txtField.resignFirstResponder()
            self.selectedMinAge = ""
            typeOfFilter = "minAge_txtField"
            self.applyFilterWrapperView.isHidden = false
            self.filterTypeArray = ["18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60"]
            self.filters_pickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.filters_pickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.maxAge_txtField
        {
            self.gender_txtfield.resignFirstResponder()
            self.minAge_txtField.resignFirstResponder()
            self.maxAge_txtField.resignFirstResponder()
           
            self.countryRepresenting_txtField.resignFirstResponder()
            self.userStatus_txtField.resignFirstResponder()
            self.nearByLocation_txtField.resignFirstResponder()
            self.selectedMaxAge = ""
            typeOfFilter = "maxAge_txtField"
            self.applyFilterWrapperView.isHidden = false
            self.filterTypeArray = ["18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60"]
            self.filters_pickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.filters_pickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.countryRepresenting_txtField
        {
            self.gender_txtfield.resignFirstResponder()
            self.minAge_txtField.resignFirstResponder()
            self.maxAge_txtField.resignFirstResponder()
           
            self.countryRepresenting_txtField.resignFirstResponder()
            self.userStatus_txtField.resignFirstResponder()
            self.nearByLocation_txtField.resignFirstResponder()
            self.countrySelected = ""
            typeOfFilter = "countryRepresenting_txtField"
            self.applyFilterWrapperView.isHidden = false
            self.filters_pickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.filters_pickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.userStatus_txtField
        {
            self.gender_txtfield.resignFirstResponder()
            self.minAge_txtField.resignFirstResponder()
            self.maxAge_txtField.resignFirstResponder()
           
            self.countryRepresenting_txtField.resignFirstResponder()
            self.userStatus_txtField.resignFirstResponder()
            self.nearByLocation_txtField.resignFirstResponder()
            self.selectedUserStatus = ""
            typeOfFilter = "userStatus_txtField"
            self.applyFilterWrapperView.isHidden = false
            self.filterTypeArray = ["Online"]
            self.filters_pickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.filters_pickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.nearByLocation_txtField
        {
            self.gender_txtfield.resignFirstResponder()
            self.minAge_txtField.resignFirstResponder()
            self.maxAge_txtField.resignFirstResponder()
           
            self.countryRepresenting_txtField.resignFirstResponder()
            self.userStatus_txtField.resignFirstResponder()
            self.nearByLocation_txtField.resignFirstResponder()
            self.selectedNearByLocation = ""
            typeOfFilter = "nearByLocation_txtField"
            self.applyFilterWrapperView.isHidden = false
            self.filterTypeArray = ["Less than 5 miles","more than 5 miles","10 miles","100 miles +","all users"]
            self.filters_pickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.filters_pickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }

        return true
    }
    
    
    
     func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WWAN")
        
            /*
           if self.search_txtField.text != ""
           {
               fetchUsers_searchBased(_textFieldValue: self.search_txtField.text! )
            }
            else
            {
                print("text field is blank")
            }*/
        }
       
        return true
    }

    // MARK: - ***** viewDidLayoutSubviews *****
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.filterScrollView.isScrollEnabled=true
        self.filterScrollView.contentSize.height = self.clearFilterView.frame.origin.y + self.clearFilterView.frame.size.height + 100
        print("self.scrollView.contentSize.height = \(self.filterScrollView.contentSize.height)")
    }
    
    
    // MARK: - ***** filterBtn_navBar *****
    @IBAction func filterBtn_navBar(_ sender: Any) {
        self.gender_txtfield.text = ""
        self.minAge_txtField.text = ""
        self.maxAge_txtField.text = ""
      
        self.search_txtField.text = ""
        self.countryRepresenting_txtField.text = ""
        self.nearByLocation_txtField.text = ""
        self.userStatus_txtField.text = ""
        self.countrySelectedIDStr = ""
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromTop, animations: { _ in
            self.search_txtField.isUserInteractionEnabled = true
            self.favouriteView.isHidden = true
            self.searchView.isHidden = true
            self.filtersView.isHidden = false
            self.filterScrollView.isHidden = false
            self.filterSearchBtn.isHidden = true
            self.filterSearchImage.isHidden = true
             self.applyFilterBool = true
        }, completion: nil)
    }
    
    
    
    // MARK: - ***** backBtn_navBar *****
    @IBAction func backBtn_navBar(_ sender: Any)
    {
//        if self.timerForSlider.isValid
//        {
//            self.timerForSlider.invalidate()
//        }
        self.timerForSlider.invalidate()
        
        if applyFilterBool == true
        {
            self.search_txtField.isUserInteractionEnabled = true
            self.gender_txtfield.text = ""
            self.minAge_txtField.text = ""
            self.maxAge_txtField.text = ""
          
            self.countryRepresenting_txtField.text = ""
            self.nearByLocation_txtField.text = ""
            self.userStatus_txtField.text = ""
            self.applyFilterWrapperView.isHidden = true

            UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: { _ in
             
                self.favouriteView.isHidden = false
                self.searchView.isHidden = false
                self.filtersView.isHidden = true
                self.filterScrollView.isHidden = true
                self.filterSearchBtn.isHidden = false
                self.filterSearchImage.isHidden = false
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    
                    self.applyFilterStr = "fetchAllUsersForDefault"
                    self.applyFilterBool = false
                }
                
            }, completion: nil)
        }
        else
        {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    // MARK: - ***** clearFilter_btnAction *****
    @IBAction func clearFilter_btnAction(_ sender: Any) {
        self.filterArray = []
        self.search_txtField.text = ""
        self.gender_txtfield.text = ""
        self.minAge_txtField.text = ""
        self.maxAge_txtField.text = ""
        
        self.countryRepresenting_txtField.text = ""
        self.nearByLocation_txtField.text = ""
        self.userStatus_txtField.text = ""
        self.nearByLocation_txtField.text = ""
    }
    
    
    
    // MARK: - ***** didReceiveMemoryWarning *****
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
