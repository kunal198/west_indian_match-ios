//
//  ConfessionStatusTableViewCell.swift
//  Wi Match
//
//  Created by brst on 03/11/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class ConfessionStatusTableViewCell: UITableViewCell {

    
    //Wi Confessions Variable for Confession Status Only.
    
    
    @IBOutlet weak var userDetailsLbl_Y_Frame: NSLayoutConstraint!
    
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var viewProfile_btn: UIButton!
    @IBOutlet weak var usersStatusTextView: UITextView!
    @IBOutlet weak var confessionReplyImage: UIImageView!
    @IBOutlet weak var confessionLikeIcon: UIImageView!
    @IBOutlet weak var moodIcon: UIImageView!
    @IBOutlet weak var postDropDownBtn: UIButton!
    @IBOutlet weak var postMessageBtn: UIButton!
    @IBOutlet weak var postReplyBtn: UIButton!
    @IBOutlet weak var postLikesBtn: UIButton!
    @IBOutlet weak var userLocationLbl: UILabel!
    @IBOutlet weak var replyCountLbl: UILabel!
    @IBOutlet weak var likeCountLbl: UILabel!
    @IBOutlet weak var userPostedStatusLbl: UILabel!
    @IBOutlet weak var userDetailsLbl: UILabel!
    
    @IBOutlet weak var decreasePostTimeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
