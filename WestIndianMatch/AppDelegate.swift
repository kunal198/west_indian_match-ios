                    

//
//  AppDelegate.swift
//  WestIndianMatch
//
//  Created by brst on 22/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import FBSDKCoreKit
import UserNotifications
import FirebaseMessaging
import AudioToolbox
import SDWebImage
import Fabric
import Crashlytics
import GiphyCoreSDK
//import PhotoEditorSDK
import StoreKit
        
           
protocol updateOpponentDetails {
    func updateDetails(oppoId: String, oppoName: String, oppoImage: String, oppoImageType: String)
}
               
@UIApplicationMain
               
class AppDelegate: UIResponder, UIApplicationDelegate {//, SKPaymentTransactionObserver {
    


    // MARK: - ****** Appdelegate Variables ******
    var threadCount = Int()
    var confessionLocationPoint = Int()
    var myDeviceToken = String()
    var newerVersionOfApp = String()
    var newerBuildNumberOfApp = String()
    var installedVersionofApp = String()
    var installedBuildNumberApp = String()
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let ProfileScrollViewSize  = CGSize()
    var getUsersArray = NSMutableArray()
    var messagesCount = 0
    var matchesCount = 0
    var visitorsCount = 0
    var messagesBool = Bool()
    var badgeCount = Int()
    var opponentNameForMatch = String()
    var opponentImageForMatch = String()
    var opponentImageType = String()
    var opponentIDForMatch = String()
    var getTypeOfMatch = String()
    var getMyNameOnRelVibes = String()
    var getOppoNameOnRelVibes = String()
    var myIdFromAppDelegatePush = String()
    var getValueFromSpotlight = Bool()
    
    var messageOnMatch = String()
    
    var newNameIDArray = NSMutableArray()
    var getAllDefaultValuesDict = NSDictionary()
    
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    var getBadgeCountFromDB = Int()

    var customDelegateUpdate: updateOpponentDetails? = nil

    // MARK: - ****** Instagram Variables ******
   /* let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    let INSTAGRAM_APIURl = "https://api.instagram.com/v1/users/"
    let INSTAGRAM_CLIENT_ID = "bf7c3d28453a411e85e1458413f2d80f"
    let INSTAGRAM_CLIENTSERCRET = "f521ea4fccac4ed381c70102eafe9c70"
    let INSTAGRAM_REDIRECT_URI = "http://negiysem.com"
    let INSTAGRAM_ACCESS_TOKEN = "access_token"
    let INSTAGRAM_SCOPE = "likes+comments+relationships"*/
    
    let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    let INSTAGRAM_APIURl = "https://api.instagram.com/v1/users/"
    let INSTAGRAM_CLIENT_ID = "27bd360f951a4a83938dbe7aa0627db1"
    let INSTAGRAM_CLIENTSERCRET = "35816e32fba7477893644ab4a36aa2ad"
    let INSTAGRAM_REDIRECT_URI = "http://example.com" //"https://wimatchapp.com"
    let INSTAGRAM_ACCESS_TOKEN = "access_token"
    let INSTAGRAM_SCOPE = "likes+comments+relationships"
    
    var cameFromHome = Bool()
    
    // MARK: - ****** didFinishLaunchingWithOptions ******
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        if UserDefaults.standard.bool(forKey: "firstlaunch") == false
        {
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(true, forKey: "firstlaunch")
        }
        
        opponentImageType = ""
        opponentImageForMatch = ""
        opponentNameForMatch = ""
        opponentIDForMatch = ""
        getTypeOfMatch = ""
        getValueFromSpotlight = false
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        FIRApp.configure()
        FIRDatabase.database().persistenceEnabled = true
        messagesBool = false

        self.getNewAppBundleIDFromFirebase()
        self.getAllDefaultValues()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
        NotificationCenter.default.addObserver(self, selector:
            #selector(tokenRefreshNotification), name:
            NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        
        // Changing the colour of the bar button items
        UINavigationBar.appearance().tintColor = UIColor.white
        // Changing the navigation controller's background colour
        UINavigationBar.appearance().barTintColor =  UIColor.init(red: 40.0/255, green: 46.0/255, blue: 61.0/255, alpha: 1.0)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        
        if ((UserDefaults.standard.value(forKey: "userType") == nil)  || (UserDefaults.standard.value(forKey: "userType") is NSNull) || (UserDefaults.standard.value(forKey: "userType") as! String == "" ))
        {
            print("usertype is nsnull")
        }
        else
        {
            if UserDefaults.standard.value(forKey: "userType") as! String == "facebookUser" || UserDefaults.standard.value(forKey: "userType") as! String == "instagramUser"
            {
                let userTypeStr = UserDefaults.standard.value(forKey: "userType") as! String
                
                if ((UserDefaults.standard.value(forKey: "userRegistered") == nil)  || (UserDefaults.standard.value(forKey: "userRegistered") is NSNull))
                {
                    print("userRegistered is nsnull")
                }
                else
                {
                    if (UserDefaults.standard.value(forKey: "userRegistered") as! String == "success")
                    {
                        if ((UserDefaults.standard.value(forKey: "userInterests") == nil)  || (UserDefaults.standard.value(forKey: "userInterests") is NSNull) || (UserDefaults.standard.value(forKey: "userInterests") as! String == "" ))
                        {
                            print("userRegistered is nsnull")
                            let interestsScreen = storyboard.instantiateViewController(withIdentifier: "AddInterestsViewController") as! AddInterestsViewController
                            interestsScreen.checkViewType = "fromRegisterScreen"
                            UserDefaults.standard.set("success", forKey: "userRegistered")
                            UserDefaults.standard.set(userTypeStr, forKey: "userType")
                            navigationController.viewControllers = [interestsScreen]
                        }
                        else
                        {
                            if (UserDefaults.standard.value(forKey: "userInterests") as! String == "addedSuccess")
                            {
                                let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                navigationController.viewControllers = [homeScreen]
                            }
                            else{
                                let interestsScreen = storyboard.instantiateViewController(withIdentifier: "AddInterestsViewController") as! AddInterestsViewController
                                interestsScreen.checkViewType = "fromRegisterScreen"
                                UserDefaults.standard.set("success", forKey: "userRegistered")
                                UserDefaults.standard.set(userTypeStr, forKey: "userType")
                                navigationController.viewControllers = [interestsScreen]
                            }
                        }
                    }
                    else
                    {
                        let registerScreen = storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                         registerScreen.typeOfUser = userTypeStr
                         registerScreen.appDelCheck = "fromAppDelegate"
                        if let name = UserDefaults.standard.value(forKey: "userName") as? String
                        {
                             registerScreen.getDisplayNameStr = name
                        }
                        
                        registerScreen.getEmailStr = ""
                        navigationController.viewControllers = [registerScreen]
                    }
                }
            }
            else
            {
                var emailString = String()
                var passwrdString = String()
                
                if ((UserDefaults.standard.value(forKey: "emailFromRegistrationStr") == nil)  || (UserDefaults.standard.value(forKey: "emailFromRegistrationStr") is NSNull) || (UserDefaults.standard.value(forKey: "emailFromRegistrationStr") as! String == "" ))
                {
                    emailString = ""
                }
                else
                {
                   emailString = UserDefaults.standard.value(forKey: "emailFromRegistrationStr") as! String
                }
                
                
                if ((UserDefaults.standard.value(forKey: "passwordFromRegistrationStr") == nil)  || (UserDefaults.standard.value(forKey: "passwordFromRegistrationStr") is NSNull) || (UserDefaults.standard.value(forKey: "passwordFromRegistrationStr") as! String == "" ))
                {
                    passwrdString = ""
                }
                else{
                    passwrdString = UserDefaults.standard.value(forKey: "passwordFromRegistrationStr") as! String
                }
                
                if ((UserDefaults.standard.value(forKey: "userInterests") == nil)  || (UserDefaults.standard.value(forKey: "userInterests") is NSNull) || (UserDefaults.standard.value(forKey: "userInterests") as! String == "" ))
                    {
                        print("userRegistered is nsnull")
                        let interestsScreen = storyboard.instantiateViewController(withIdentifier: "AddInterestsViewController") as! AddInterestsViewController
                        UserDefaults.standard.set("success", forKey: "userRegistered")
                        UserDefaults.standard.set("emailUser", forKey: "userType")
                        interestsScreen.emailFromRegistrationStr = emailString
                        interestsScreen.passwordFromRegistrationStr = passwrdString
                        interestsScreen.checkViewType = "fromRegisterScreen"
                        navigationController.viewControllers = [interestsScreen]
                    }
                    else
                    {
                        if (UserDefaults.standard.value(forKey: "userInterests") as! String == "addedSuccess")
                        {
                            if UserDefaults.standard.value(forKey: "isEmailVerified") as! Bool == true
                            {
                                 let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                 navigationController.viewControllers = [homeScreen]
                            }
                        }
                        else{
                            let interestsScreen = storyboard.instantiateViewController(withIdentifier: "AddInterestsViewController") as! AddInterestsViewController
                            UserDefaults.standard.set("success", forKey: "userRegistered")
                            UserDefaults.standard.set("emailUser", forKey: "userType")
                            interestsScreen.checkViewType = "fromRegisterScreen"
                            interestsScreen.emailFromRegistrationStr = emailString
                            interestsScreen.passwordFromRegistrationStr = passwrdString
                            navigationController.viewControllers = [interestsScreen]
                        }
                }
            }
        }
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        Fabric.sharedSDK().debug = true
        
       /* if let licenseURL = Bundle.main.url(forResource: "ios_license", withExtension: "") {
            PESDK.unlockWithLicense(at: licenseURL)
        }*/
        
        Fabric.with([Crashlytics.self()])
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func getAllDefaultValues()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            self.newNameIDArray = []
            self.getAllDefaultValuesDict = [:]
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            DispatchQueue.global(qos: .background).async {
                
                ApiHandler.getAllDefaultValues(completion: { (responseValue) in
                    print("responseValue = \(responseValue)")
                    
                    if (responseValue.value(forKey: "status") as? String == "200") //&& (responseValue.value(forKey: "message") as? String == "Data Returned.")
                    {
                        if let dataDict = responseValue.value(forKey: "data") as? NSDictionary
                        {
                            self.getAllDefaultValuesDict = dataDict
                            print("self.getAllDefaultValues = \(String(describing: self.getAllDefaultValuesDict))")
                            
                            let allKeys = self.getAllDefaultValuesDict.allKeys as! [String]
                            if allKeys.contains("countries")
                            {
                                let countriesArray = self.getAllDefaultValuesDict.value(forKey: "countries") as! NSArray
                                
                                for index in 0..<countriesArray.count {
                                    
                                    let countryDict = countriesArray[index]
                                    print("countryDict = \(countryDict)")
                                    
                                    if !self.newNameIDArray.contains(countryDict) {
                                        self.newNameIDArray.insert(countryDict, at: index)
                                    }
                                }
                            }
                            else
                            {
                                self.newNameIDArray = []
                            }
                            print("newNameIDArray = \(self.newNameIDArray)")
                        }
                        else{
                            self.newNameIDArray = []
                            self.getAllDefaultValuesDict = [:]
                        }
                    }
                    else{
                        self.newNameIDArray = []
                        self.getAllDefaultValuesDict = [:]
                    }
                })
            }
        }
    }
    

    func getNewAppBundleIDFromFirebase()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
             print("Not connected")
        case .online(.wwan) , .online(.wiFi):
            
            let ref = FIRDatabase.database().reference()
//            ref.child("AppStoreLatestBuildVersion").keepSynced(true)
            ref.child("AppStoreLatestBuildVersion").observe(.value, with: { (snapshot) in
                
                if snapshot.exists()
                {
                    print("AppStoreLatestBuildVersion exists")
                    
                    let dataDict = snapshot.value as! NSMutableDictionary
                    print("dataDict = \(dataDict)")
                    
                    let keysArray = dataDict.allKeys as! [String]
                    
                    if (keysArray.contains("AppStoreLatestBuildVersion") && keysArray.contains("BuildNumber"))
                    {
                        self.newerVersionOfApp = (dataDict.value(forKey: "AppStoreLatestBuildVersion") as? String)!
                        
                        self.newerBuildNumberOfApp = (dataDict.value(forKey: "BuildNumber") as? String)!
                        
                        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                        {
                            print("+++++++++++++++++++++++++ CFBundleShortVersionString +++++++++++++++++++++++++ = \(version)")
                            self.installedVersionofApp = version
                        }
                        
                        if self.newerVersionOfApp.compare(self.installedVersionofApp, options: NSString.CompareOptions.numeric) == ComparisonResult.orderedSame
                        {
                            print("Both versions are same versions")
                            
                            if let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
                            {
                                print("+++++++++++++++++++++++++ CFBundleVersion +++++++++++++++++++++++++ = \(buildNumber)")
                                self.installedBuildNumberApp = buildNumber
                            }
                            
                            if self.newerBuildNumberOfApp.compare(self.installedBuildNumberApp, options: NSString.CompareOptions.numeric) == ComparisonResult.orderedSame
                            {
                                print("Both build numbers are same builds")
                            }
                            else
                            {
                                if self.newerBuildNumberOfApp.compare(self.installedBuildNumberApp, options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
                                {
                                    print("self.newerBuildNumberOfApp build is higher")
                                }
                                else{
                                    print("self.installedBuildNumberApp build is higher")
                                    
                                    let alert = UIAlertController(title: "", message: "Newer Version of app is available on AppStore. Please update.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                        
                                        let urlStr = "https://itunes.apple.com/us/app/wimatch-chat-meet-fete/id1225557150?ls=1&mt=8"
                                        if #available(iOS 10.0, *) {
                                            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                                        }
                                        else
                                        {
                                            UIApplication.shared.openURL(URL(string: urlStr)!)
                                        }
                                        
                                    }))
                                    
                                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                        else
                        {
                            if self.newerVersionOfApp.compare(self.installedVersionofApp, options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending {
                                print("self.newerVersionOfApp version is higher")
                            }
                            else
                            {
                                print("self.installedVersionofApp version is higher")
                                let alert = UIAlertController(title: "", message: "Newer Version of app is available on AppStore. Please update.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    let urlStr = "https://itunes.apple.com/us/app/wimatch-chat-meet-fete/id1225557150?ls=1&mt=8"
                                    if #available(iOS 10.0, *) {
                                        UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                                        
                                    } else {
                                        UIApplication.shared.openURL(URL(string: urlStr)!)
                                    }
                                    
                                }))
                                
                                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                }
                else
                {
                    print("AppStoreLatestBuildVersion not exists")
                    
                    var dict = [String:String]()
                    dict["AppStoreLatestBuildVersion"] = "1.1"
                    ref.child("AppStoreLatestBuildVersion").updateChildValues(dict)
                }
            })
        }
    }
    
    
    func tokenRefreshNotification(_ notification: Notification)
    {
        if let refreshedToken = FIRInstanceID.instanceID().token()
        {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    // [START connect_to_fcm]
    func connectToFcm()
    {
        FIRMessaging.messaging().connect { (error) in
            if (error != nil)
            {
                print("Unable to connect with FCM. \(String(describing: error))")
            }
            else
            {
                print("Connected to FCM.")
            }
        }
    }
    
    
     
   
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            myDeviceToken = refreshedToken
        }
        
        print("deviceToken.count = \(deviceToken.count)")
        
        var token = ""
        for i in 0..<deviceToken.count
        {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        print("++++++++++++++ myDeviceToken +++++++++++++++ = \(myDeviceToken)")
        
        // for development
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("offline")
            
        case .online(.wwan) , .online(.wiFi):
            
            self.updateUsersDetails(notificationType: "device_token", getVisibility: myDeviceToken, completion: { (response) in
                
                if (response == true)
                {
                    print("myDeviceToken updated")
                }
                else
                {
                    print("myDeviceToken not updated")
                }
            })
        }
    }

    
   func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
   {
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    // MARK: - ****** sourceApplication for facebook login ******
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool 
    {
        if (FBSDKApplicationDelegate.sharedInstance() != nil)
        {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        
        return true
    }
    
    
    // MARK: - ****** applicationWillResignActive ******
    func applicationWillResignActive(_ application: UIApplication) {
        
        self.doBackgroundTask()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("offline")

        case .online(.wwan) , .online(.wiFi):
             print("online")
            
          /*if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                let childRef = FIRDatabase.database().reference()
                var dataDict = [String : Any]()
                dataDict["userStatus"] = "offline"
                childRef.child("Users").child(userID).updateChildValues(dataDict)
            }
            
            DispatchQueue.global(qos: .background).async {
             
                self.updateUsersDetails(notificationType: "online_status", getVisibility: "0", completion: { (response) in
                    if (response == true)
                    {
                        print("updated users status to offline")
                    }
                    else
                    {
                        print("not updated users status to offline")
                    }
                })
             
            }*/
        }
    }

    
     // MARK: - ****** applicationDidEnterBackground ******
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        self.doBackgroundTask()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("offline")
            
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                let childRef = FIRDatabase.database().reference()
                var dataDict = [String : Any]()
                dataDict["userStatus"] = "offline"
                childRef.child("Users").child(userID).updateChildValues(dataDict)
                
                childRef.child("UsersInChatRoom").child(userID).setValue(nil)
            }
            
            self.updateUsersDetails(notificationType: "online_status", getVisibility: "0", completion: { (response) in
                if (response == true)
                {
                    print("updated users status to offline")
                }
                else
                {
                    print("not updated users status to offline")
                }
            })
        }
    }

    
    // MARK: - ****** applicationWillEnterForeground ******
    func applicationWillEnterForeground(_ application: UIApplication) {
        
       UIApplication.shared.applicationIconBadgeNumber = 0
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getUpdateBadges"), object: nil, userInfo: nil)
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("offline")
            
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                let childRef = FIRDatabase.database().reference()
                var dataDict = [String : Any]()
                dataDict["userStatus"] = "online"
                childRef.child("Users").child(userID).updateChildValues(dataDict)
            }
            
            if let viewControllers = self.window?.rootViewController?.childViewControllers {
                if let vc = viewControllers.last
                {
                    print("last or top vc is = \(vc)")
                    if vc.isKind(of: GroupChatMeassagesViewController.self)
                    {
                        print("current opened screen is GroupChatMeassagesViewController")
                        
                        let childRef = FIRDatabase.database().reference()
                        
                        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                        {
                            var dataDict = [String : Any]()
                            dataDict["UserID"] = currentUserID
                            
                            if let userName = UserDefaults.standard.value(forKey: "userName") as? String
                            {
                                dataDict["UserName"] = userName
                            childRef.child("UsersInChatRoom").child(currentUserID).updateChildValues(dataDict)
                            }
                        }
                    }
                    else{
                        print("current opened screen is not GroupChatMeassagesViewController from appdelegate")
                    }
                }
            }
            
            self.updateUsersDetails(notificationType: "online_status", getVisibility: "1", completion: { (response) in
                if (response == true)
                {
                    print("updated users status to online")
                }
                else
                {
                    print("not updated users status to online")
                }
            })
        }
    }

    
    // MARK: - ****** applicationDidBecomeActive ******
    func applicationDidBecomeActive(_ application: UIApplication) {
        
       connectToFcm()
       FBSDKAppEvents.activateApp()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("offline")
            
        case .online(.wwan) , .online(.wiFi):
            
            if let viewControllers = self.window?.rootViewController?.childViewControllers {
                if let vc = viewControllers.last
                {
                    print("last or top vc is = \(vc)")
                    if vc.isKind(of: GroupChatMeassagesViewController.self)
                    {
                        print("current opened screen is GroupChatMeassagesViewController")
                        
                        let childRef = FIRDatabase.database().reference()
                        
                        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                        {
                            var dataDict = [String : Any]()
                            dataDict["UserID"] = currentUserID
                            
                            if let userName = UserDefaults.standard.value(forKey: "userName") as? String
                            {
                                dataDict["UserName"] = userName
                                childRef.child("UsersInChatRoom").child(currentUserID).updateChildValues(dataDict)
                            }
                        }
                    }
                    else{
                         print("current opened screen is not GroupChatMeassagesViewController from appdelegate")
                    }
                }
            }
            
           /*  DispatchQueue.global(qos: .background).async {
                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    let childRef = FIRDatabase.database().reference()
                    var dataDict = [String : Any]()
                    dataDict["userStatus"] = "online"
                    childRef.child("Users").child(userID).updateChildValues(dataDict)
                }
             }*/
           
     
           /* self.updateUsersDetails(notificationType: "online_status", getVisibility: "1", completion: { (response) in
                if (response == true)
                {
                    print("updated users status to online")
                }
                else
                {
                   print("not updated users status to online")
                }
            })*/
        }
    }

    
     // MARK: - ****** applicationWillTerminate ******
    func applicationWillTerminate(_ application: UIApplication) {
        
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        FBSDKAccessToken.setCurrent(nil)
        FBSDKProfile.setCurrent(nil)
        
        self.doBackgroundTask()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("offline")
            
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                let childRef = FIRDatabase.database().reference()
                var dataDict = [String : Any]()
                dataDict["userStatus"] = "offline"
                childRef.child("Users").child(userID).updateChildValues(dataDict)
                childRef.child("UsersInChatRoom").child(userID).setValue(nil)
            }
            
            self.updateUsersDetails(notificationType: "online_status", getVisibility: "0", completion: { (response) in
                
                if (response == true)
                {
                    print("updated users status to offline")
                }
                else
                {
                    print("not updated users status to offline")
                }
            })
        }
    }
    
    // MARK: - Method for updating Background tasks and terminate application
    func doBackgroundTask()
    {
        print("doBackgroundTask called")
        
        self.beginBackgroundUpdateTask()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("offline")
            
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                let Ref = FIRDatabase.database().reference()
                var dataDict = [String : Any]()
                dataDict["AppBadgeCount"] = self.getBadgeCountFromDB
                Ref.child("Users").child(userID).updateChildValues(dataDict)
            }
        }
    }
    
    // MARK: - ****** Update About Me Values ******
    func updateUsersDetails(notificationType: String, getVisibility: String, completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                ApiHandler.updateSettings(forUserID: userID, authToken: userToken, typeToBeUpdated: notificationType, isSetTo: getVisibility, completion: { (responseData) in
                    
                    print("responseData = \(responseData)")
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        print("dataDict = \(String(describing: dataDict))")
                        
                        completion(true)
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
                        print("message = \(String(describing: message))")
                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
                        completion(false)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
                        completion(false)
                    }
                })
            }
        }
    }
    
    // MARK: - Begin Background tasks update Method
    func beginBackgroundUpdateTask() {
        
        print("beginBackgroundUpdateTask called")
        
        self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundUpdateTask()
        })
    }
    
    // MARK: - End Background Update tasks Method
    func endBackgroundUpdateTask() {
        
        print("endBackgroundUpdateTask called")
        
        UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskInvalid
    }
    
    func convertToDictionary(text: String) -> NSDictionary? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
               



// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
     
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("userInfo for notifications = \(userInfo)")
        
        self.getTypeOfMatch = ""
        self.myIdFromAppDelegatePush = ""
        
        if let type = userInfo["gcm.notification.type"] as? String
        {
            print("type = \(type)")
            if userInfo["gcm.notification.type"] as! String == "Messages"
            {
                if let aps = userInfo["aps"] as? NSDictionary
                {
                    if let badgeCount = aps["badge"] as? Int
                    {
                        UIApplication.shared.applicationIconBadgeNumber = Int(badgeCount)
                    }
                    else
                    {
                        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                    }
                }
                else
                {
                    UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                }
            }
            else
            {
                if let aps = userInfo["aps"] as? NSDictionary
                {
                    if let badgeCount = aps["badge"] as? String
                    {
                        self.getBadgeCountFromDB = Int(badgeCount)!
                        UIApplication.shared.applicationIconBadgeNumber = Int(badgeCount)!
                    }
                    else
                    {
                        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                    }
                }
                else
                {
                    UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                }
            }
        }
        else
        {
            UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        }
        
        var messageStr = String()
        
        let state: UIApplicationState = UIApplication.shared.applicationState
        if  state == .active
        {
            var titleStr = String()
            if let aps = userInfo["aps"] as? NSDictionary
            {
                if let alert = aps["alert"] as? NSDictionary
                {
                    if let alertMessage = alert["body"] as? String
                    {
                        messageStr = alertMessage
                        self.messageOnMatch = messageStr
                        
                        if let tilte = alert["title"] as? String
                        {
                            titleStr = tilte
                        }
                    }
                }
            }
            else
            {
                var resultDict = NSDictionary()
                resultDict = userInfo["notification"] as! NSDictionary
                messageStr = resultDict["body"] as! String
                titleStr = resultDict["title"] as! String
            }
            
            let fullNameArr = messageStr.components(separatedBy: ":")
            if fullNameArr.count > 0
            {
                if fullNameArr.count == 1
                {
                    let firstName: String = fullNameArr[0]
                    
                    if let name = UserDefaults.standard.value(forKey: "userName") as? String
                    {
                        if (name != firstName)
                        {
                            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                        }
                    }
                }
                else
                {
                    let firstName: String = fullNameArr[0]
                    let lastName: String = fullNameArr[1]
                    print("firstName = \(firstName)")
                    print("lastName = \(lastName)")
                    
                    if let name = UserDefaults.standard.value(forKey: "userName") as? String
                    {
                        if (name != firstName)
                        {
                            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                        }
                    }
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getUpdateBadges"), object: nil, userInfo: nil)
            
            if (messageStr.range(of: "Vibes") != nil)
            {
                if let viewControllers = self.window?.rootViewController?.childViewControllers {
                    if let vc = viewControllers.last
                    {
                        print("last or top vc is = \(vc)")
                        if vc.isKind(of: WiMatch_MeetUpViewController.self)
                        {
                            print("current opened screen is WiMatch_MeetUpViewController")
                            if let type = userInfo["gcm.notification.type"] as? String
                            {
                                if type == "new_match" //"New Matches"
                                {
                                    print("type of push is New Matches")
                                    
                                    let dataDictStr = userInfo["gcm.notification.data"] as! String
                                    let dict = convertToDictionary(text: dataDictStr)
                                    print("dict = \(String(describing: dict))")
                                    
                                    if (dict != nil)
                                    {
                                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlertForRelVibes"), object: nil, userInfo:  ["Status": dict as! NSDictionary])
                                    }
                                }
                            }
                        }
                        else
                        {
                            print("current opened screen is not WiMatch_MeetUpViewController")
                            
                            if (userInfo["gcm.notification.data"] != nil)
                            {
                                let dataDictStr = userInfo["gcm.notification.data"] as! String
                                let dict = convertToDictionary(text: dataDictStr)
                                print("dict = \(String(describing: dict))")
                                
                                if (dict != nil)
                                {
                                    let opponent_id: String = dict?.value(forKey: "opponent_id") as! String
                                    //String(dict?.value(forKey: "opponent_id") as! Int)
                                    print("opponent_id = \(opponent_id)")
                                    
                                    let my_id: String = dict?.value(forKey: "my_id") as! String
                                    /*  let opponent_username: String = (dict!.value(forKey: "opponent_username") as? String)!
                                     let my_username: String = (dict!.value(forKey: "my_username") as? String)!
                                     let opponent_image: String = (dict!.value(forKey: "opponent_image") as? String)!
                                     let my_image: String = (dict!.value(forKey: "my_image") as? String)! */
                                    let match_type: String = dict?.value(forKey: "match_type") as! String
                                    
                                    if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                                    {
                                        if (id == my_id)
                                        {
                                            self.opponentIDForMatch = opponent_id
                                            
                                            self.getTypeOfMatch = match_type
                                            print("getTypeOfMatch from appdelegate = \(self.getTypeOfMatch)")
                                            
                                            self.myIdFromAppDelegatePush = my_id
                                            
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlertForRelVibes"), object: nil, userInfo:  ["Status": dict as! NSDictionary])
                                            
                                        }
                                        else
                                        {
                                            self.opponentIDForMatch = my_id
                                            self.getTypeOfMatch = match_type
                                            print("getTypeOfMatch from appdelegate = \(self.getTypeOfMatch)")
                                            self.myIdFromAppDelegatePush = opponent_id
                                            
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlertForRelVibes"), object: nil, userInfo:  ["Status": userInfo["gcm.notification.myID"] as! String])
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
           else //if (messageStr.range(of: "sent") != nil)
            {
                print("messageStr contains the word `sent`")
                
                if let type = userInfo["gcm.notification.type"] as? String
                {
                    if type == "Messages"
                    {
                        print("type of push is Messages")
                        
                        if let viewControllers = self.window?.rootViewController?.childViewControllers {
                            if let vc = viewControllers.last
                            {
                                print("last or top vc is = \(vc)")
                                
                                if vc.isKind(of: MessagesViewController.self)
                                {
                                    print("current opened screen is MessagesViewController")
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateChatOnReceive"), object: nil, userInfo: nil)
                                    
                                    return
                                }
                                else
                                {
                                    print("current opened screen is not MessagesViewController")
                                    messagesCount = messagesCount + 1
                                    
                                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateChatOnReceive"), object: nil, userInfo: nil)
                                    
                                    if var messageBadge = UserDefaults.standard.value(forKey: "messagesCount") as? Int
                                    {
                                        messageBadge = messageBadge + 1
                                        UserDefaults.standard.set(messageBadge, forKey: "messagesCount")
                                        UserDefaults.standard.synchronize()
                                    }
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMessageBadge"), object: nil, userInfo: nil)
                                }
                            }
                        }
                    }
                    else if  type == "Chatroom"
                    {
                        if let viewControllers = self.window?.rootViewController?.childViewControllers {
                            if let vc = viewControllers.last
                            {
                                print("last or top vc is = \(vc)")
                                if vc.isKind(of: GroupChatMeassagesViewController.self)
                                {
                                    print("current opened screen is GroupChatMeassagesViewController")
                                    return
                                }
                                else
                                {
                                    let fullNameArr = messageStr.components(separatedBy: ":")
                                    if fullNameArr.count > 0
                                    {
                                        if fullNameArr.count == 1
                                        {
                                            let firstName: String = fullNameArr[0]
                                            
                                            if let name = UserDefaults.standard.value(forKey: "userName") as? String
                                            {
                                                if (name == firstName)
                                                {
                                                   return
                                                }
                                            }
                                        }
                                        else
                                        {
                                            let firstName: String = fullNameArr[0]
                                            let lastName: String = fullNameArr[1]
                                            print("firstName = \(firstName)")
                                            print("lastName = \(lastName)")
                                            
                                            if let name = UserDefaults.standard.value(forKey: "userName") as? String
                                            {
                                                if (name == firstName)
                                                {
                                                    return
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if type == "new_match" //"New Matches"
                    {
                        print("type of push is New Matches")
                        
                        let dataDictStr = userInfo["gcm.notification.data"] as! String
                        let dict = convertToDictionary(text: dataDictStr)
                        print("dict = \(String(describing: dict))")
                        
                        if (dict != nil)
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlertForRelVibes"), object: nil, userInfo:  ["Status": dict as! NSDictionary])
                        }
                    }
                    else
                    {
                        print("messageStr does not contain `sent`")
                        if messageStr == "REL VIBES!"
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
            }
        }
        
        // Change this to your preferred presentation option
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void)
    {
         AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        
        let userInfo = response.notification.request.content.userInfo
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
          UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1

            if let type = userInfo["gcm.notification.type"] as? String
            {
                if userInfo["gcm.notification.type"] as! String == "profile_visitors"//"Profile Visitors"
                {
                    print("type of push is Profile Visitors")
                    
                    visitorsCount = visitorsCount + 1
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                    let profileVisitorsScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    
                    profileVisitorsScreen.profileVisitorsPushBool = true
//                    profileVisitorsScreen.appDelegatePush = "fromAppDelegate"
                    navigationController.viewControllers = [profileVisitorsScreen]
                    
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "moveToProfileScreen"), object: nil, userInfo: nil)
                    
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                }
                else if userInfo["gcm.notification.type"] as! String == "new_match"//"New Matches"
                {
                    print("type of push is New Matches")
                    
                    if let viewControllers = self.window?.rootViewController?.childViewControllers {
                        if let vc = viewControllers.last
                        {
                            print("last or top vc is = \(vc)")
                            if vc.isKind(of: WiMatch_MeetUpViewController.self)
                            {
                                print("current opened screen is WiMatch_MeetUpViewController")
                                let dataDictStr = userInfo["gcm.notification.data"] as! String
                                let dict = convertToDictionary(text: dataDictStr)
                                print("dict = \(String(describing: dict))")
                                
                                if (dict != nil)
                                {
                                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlertForRelVibes"), object: nil, userInfo:  ["Status": dict as! NSDictionary])
                                }
                            }
                            else
                            {
                                print("current opened screen is not WiMatch_MeetUpViewController")
                                
                                let dataDictStr = userInfo["gcm.notification.data"] as! String
                                let dict = convertToDictionary(text: dataDictStr)
                                print("dict = \(String(describing: dict))")
                                
                                if (dict != nil)
                                {
                                    let opponent_id: String = dict?.value(forKey: "opponent_id") as! String
                                    //String(dict?.value(forKey: "opponent_id") as! Int)
                                    print("opponent_id = \(opponent_id)")
                                    
                                    let my_id: String = dict?.value(forKey: "my_id") as! String 
                                  /*  let opponent_username: String = (dict!.value(forKey: "opponent_username") as? String)!
                                    let my_username: String = (dict!.value(forKey: "my_username") as? String)!
                                    let opponent_image: String = (dict!.value(forKey: "opponent_image") as? String)!
                                    let my_image: String = (dict!.value(forKey: "my_image") as? String)!*/
                                    let match_type: String = dict?.value(forKey: "match_type") as! String
                                    
                                    if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                                    {
                                        if (id == my_id)
                                        {
                                            self.opponentIDForMatch = opponent_id
                                            
                                            self.getTypeOfMatch = match_type
                                            print("getTypeOfMatch from appdelegate = \(self.getTypeOfMatch)")
                                            
                                            self.myIdFromAppDelegatePush = my_id
                                            
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlertForRelVibes"), object: nil, userInfo:  ["Status": dict as! NSDictionary])
                                            
                                        }
                                        else
                                        {
                                            self.opponentIDForMatch = my_id
                                            self.getTypeOfMatch = match_type
                                            print("getTypeOfMatch from appdelegate = \(self.getTypeOfMatch)")
                                            self.myIdFromAppDelegatePush = opponent_id
                                            
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlertForRelVibes"), object: nil, userInfo:  ["Status": dict as! NSDictionary])
                                        }
                                        
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                                        let newMatchesScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                        newMatchesScreen.wiMatchBool = true
                                        newMatchesScreen.matchDictionary = dict!
                                        
                                        newMatchesScreen.typeOfPushForMatch = "New Matches From Didreceive"
                                        newMatchesScreen.opponentIDForMatch = self.opponentIDForMatch
                                        newMatchesScreen.getTypeForMatch = match_type
                                        newMatchesScreen.myIdForMatch = self.myIdFromAppDelegatePush
                                        navigationController.viewControllers = [newMatchesScreen]
                                        self.window?.rootViewController = navigationController
                                        self.window?.makeKeyAndVisible()
                                    }
                                }
                            }
                        }
                    }
                }
                else if userInfo["gcm.notification.type"] as! String == "link_up"//"Link Up"
                {
                    print("type of push is Link Up")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                    let newMatchesScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                    newMatchesScreen.appDelegatePush = "fromAppDelegate"
                    newMatchesScreen.linkUpPushBool = true
                    newMatchesScreen.typeOfPushForMatch = "Link Up"
                    navigationController.viewControllers = [newMatchesScreen]
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                }
                else if userInfo["gcm.notification.type"] as! String == "Comments"
                {
                    print("type of push is Comments")
                    
                    if var wiFeedsBadge = UserDefaults.standard.value(forKey: "wiFeedsBadge") as? Int
                    {
                        wiFeedsBadge = wiFeedsBadge + 1
                        UserDefaults.standard.set(wiFeedsBadge, forKey: "wiFeedsBadge")
                        UserDefaults.standard.synchronize()
                    }
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                    let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    
                    if userInfo["gcm.notification.typeOFReplyStr"] as! String == "replyOnReply"
                    {
//                        openParticularPost.appDelegatePush = "fromAppDelegate"
                        navigationController.viewControllers = [openParticularPost]
                        openParticularPost.postIDReply = userInfo["gcm.notification.postID"] as! String
                        openParticularPost.replyIDReply = userInfo["gcm.notification.replyIdStr"] as! String
                        openParticularPost.userIDOwnerReply = userInfo["gcm.notification.postOwnerID"] as! String
                        openParticularPost.postIDOwnerReply = userInfo["gcm.notification.postOwnerID"] as! String
                        openParticularPost.checkReplyTypeReply = userInfo["gcm.notification.typeOFReplyStr"] as! String
                        openParticularPost.differentiateTypeReply = userInfo["gcm.notification.differentiateString"] as! String
                        openParticularPost.typeOfPostReply = userInfo["gcm.notification.photoUploadType"] as! String
                        openParticularPost.typeOfAlbumReply = userInfo["gcm.notification.type"] as! String
                        openParticularPost.postIDOpenPostReply = userInfo["gcm.notification.replyIdStr"] as! String
                        openParticularPost.replyOnReplyId = userInfo["gcm.notification.replyOnReplyID"] as! String
                        openParticularPost.commentsPushBool = true
                        
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                    else
                    {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                        
                        if userInfo["gcm.notification.differentiateString"] as! String == "wiFeedsReply"
                        {
                            let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            
                            navigationController.viewControllers = [openParticularPost]
                            
                            openParticularPost.profilePushBool = true

                            
                            openParticularPost.postIDProfile = userInfo["gcm.notification.postID"] as! String
                            openParticularPost.typeOfAlbumProfile = userInfo["gcm.notification.type"] as! String
                            openParticularPost.typeOfPostStrProfile = userInfo["gcm.notification.photoUploadType"] as! String
                            openParticularPost.postIDOwnerReplyProfile = userInfo["gcm.notification.postOwnerID"] as! String
                            openParticularPost.replyIDReplyProfile = userInfo["gcm.notification.replyIdStr"] as! String
                            openParticularPost.checkReplyTypeReplyProfile = userInfo["gcm.notification.typeOFReplyStr"] as! String
                        }
                        else
                        {
                            let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            //                        openParticularPost.appDelegatePush = "fromAppDelegate"
                            navigationController.viewControllers = [openParticularPost]
                            openParticularPost.postIDReply = userInfo["gcm.notification.postID"] as! String
                            openParticularPost.typeOfAlbumReply = userInfo["gcm.notification.type"] as! String
                            openParticularPost.typeOfPostReply = userInfo["gcm.notification.photoUploadType"] as! String
                            openParticularPost.postIDOwnerReply = userInfo["gcm.notification.postOwnerID"] as! String
                            
                            openParticularPost.commentsPushBool = true
                        }
                
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }
                else if userInfo["gcm.notification.type"] as! String == "Messages"
                {
                    print("type of push is Messages")
                    
                    messagesCount = messagesCount + 1
                    if var messageBadge = UserDefaults.standard.value(forKey: "messagesCount") as? Int
                    {
                        messageBadge = messageBadge + 1
                        UserDefaults.standard.set(messageBadge, forKey: "messagesCount")
                        UserDefaults.standard.synchronize()
                    }
                    
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMessageBadge"), object: nil, userInfo: nil)
                    
                    if let viewControllers = self.window?.rootViewController?.childViewControllers {
                        if let vc = viewControllers.last
                        {
                            print("last or top vc is = \(vc)")
                            if vc.isKind(of: ChatListingViewController.self)
                            {
                                print("current opened screen is ChatListingViewController")
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                                let messageScreen = storyboard.instantiateViewController(withIdentifier: "ChatListingViewController") as! ChatListingViewController
                                
                                messageScreen.opponentFirebaseID = userInfo["gcm.notification.OpponentFirebaseUserID"] as! String
                                messageScreen.opponentUserID  = userInfo["gcm.notification.OpponentUserID"] as! String
                                messageScreen.opponentName = userInfo["gcm.notification.OpponentName"] as! String
                                messageScreen.typeOfUser = userInfo["gcm.notification.UsersType"] as! String
                                messageScreen.postID = userInfo["gcm.notification.postIDStr"] as! String
                                messageScreen.opponentImage = userInfo["gcm.notification.OpponentProfileImage"] as! String
                                
                                if userInfo["gcm.notification.checkVisibiltyStr"] != nil
                                {
                                    messageScreen.checkVisibiltyStr = userInfo["gcm.notification.checkVisibiltyStr"] as! String
                                    
                                    if (userInfo["gcm.notification.checkVisibiltyStr"] as! String == "withProfileChat") || (userInfo["gcm.notification.checkVisibiltyStr"] as! String == "FromViewProfile")
                                    {
                                        if userInfo["gcm.notification.opponentSettingProfileInvisibilityStr"] != nil
                                        {
                                            messageScreen.myVisiblityFromChatStr = userInfo["gcm.notification.opponentSettingProfileInvisibilityStr"] as! String
                                        }
                                        
                                        if userInfo["gcm.notification.mySettingProfileInvisibilityStr"] != nil
                                        {
                                            messageScreen.opponentVisibilityFromChatStr = userInfo["gcm.notification.mySettingProfileInvisibilityStr"] as! String
                                        }
                                        
                                        messageScreen.mainConfessionCheck = "comingFromChatListing"
                                        messageScreen.checkVisibiltyStr = "withProfileChat"
                                    }
                                    else{
                                        messageScreen.checkVisibiltyStr = "withAnonymousChat"
                                    }
                                }
                                
                                messageScreen.messagesPushBool = true
                                
                                messageScreen.appDelegatePush = "fromAppDelegate"
                                navigationController.viewControllers = [messageScreen]
                                self.window?.rootViewController = navigationController
                                self.window?.makeKeyAndVisible()
                                
                                return
                            }
                        }
                    }
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                    let messageScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    
                    messageScreen.typeOfUserMessage = "WithProfileUser"
                    messageScreen.oppoFirebaseIDMessage = userInfo["gcm.notification.OpponentFirebaseUserID"] as! String
                    messageScreen.oppoIDMessage = userInfo["gcm.notification.OpponentUserID"] as! String
                    messageScreen.oppoNameMessage = userInfo["gcm.notification.OpponentName"] as! String
                    messageScreen.typeOfUserMessage = userInfo["gcm.notification.UsersType"] as! String
                     messageScreen.postIDMessage = userInfo["gcm.notification.postIDStr"] as! String
                    messageScreen.oppoImageMessage = userInfo["gcm.notification.OpponentProfileImage"] as! String
                  
                    if userInfo["gcm.notification.checkVisibiltyStr"] != nil
                    {
                        messageScreen.checkVisibiltyStr = userInfo["gcm.notification.checkVisibiltyStr"] as! String
                        
                        if (userInfo["gcm.notification.checkVisibiltyStr"] as! String == "withProfileChat") || (userInfo["gcm.notification.checkVisibiltyStr"] as! String == "FromViewProfile")
                        {
                            if userInfo["gcm.notification.opponentSettingProfileInvisibilityStr"] != nil
                            {
                                messageScreen.myVisibilityMessage = userInfo["gcm.notification.opponentSettingProfileInvisibilityStr"] as! String
                            }
                            
                            if userInfo["gcm.notification.mySettingProfileInvisibilityStr"] != nil
                            {
                                messageScreen.oppoVisibility = userInfo["gcm.notification.mySettingProfileInvisibilityStr"] as! String
                            }
                            
                            messageScreen.mainConfessionCheck = "comingFromChatListing"
                            messageScreen.checkVisibilityMessage = "withProfileChat"
                        }
                        else{
                            messageScreen.checkVisibilityMessage = "withAnonymousChat"
                        }
                    }
                    
//                    messageScreen.appdel = "fromHomePage"
                    messageScreen.messagesPushBool = true
                    navigationController.viewControllers = [messageScreen]
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                }
                else if userInfo["gcm.notification.type"] as! String == "post_likes" //"ConfessionLikes"
                {
                    print("type of push is Confession Likes")
                    
                    if var wiFeedsBadge = UserDefaults.standard.value(forKey: "wiFeedsBadge") as? Int
                    {
                        wiFeedsBadge = wiFeedsBadge + 1
                        UserDefaults.standard.set(wiFeedsBadge, forKey: "wiFeedsBadge")
                        UserDefaults.standard.synchronize()
                    }
                    
                    let dataDictStr = userInfo["gcm.notification.data"] as! String
                    let dict = convertToDictionary(text: dataDictStr)
                    print("dict = \(String(describing: dict))")
                    
                    if (dict != nil)
                    {
                        let post_id: String = dict?.value(forKey: "post_id") as! String//String(dict?.value(forKey: "post_id") as! Int)
                        print("post_id = \(post_id)")
                        
                        let post_image: String = dict?.value(forKey: "post_image") as! String //String(dict?.value(forKey: "post_image") as! Int)
//                        let post_owner_id: String = (dict!.value(forKey: "post_owner_id") as? String)!
//                        let post_text: String = (dict!.value(forKey: "post_text") as? String)!
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                        let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        navigationController.viewControllers = [openParticularPost]
                        openParticularPost.profilePushBool = true
                        openParticularPost.postIDProfile = post_id
                        
                        if (post_image == "")
                        {
                          openParticularPost.typeOfPostStrProfile = "text"
                        }
                        else
                        {
                            if (post_image != "")
                            {
                                if (post_image.contains("jpg"))
                                {
                                    openParticularPost.typeOfPostStrProfile = "photo"
                                }
                                else
                                {
                                    openParticularPost.typeOfPostStrProfile = "gif"
                                }
                            }
                            else{
                                openParticularPost.typeOfPostStrProfile = "text"
                            }
                        }
                     
                        openParticularPost.typeOfAlbumProfile = "ConfessionLikes"
                        
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }
                else if userInfo["gcm.notification.type"] as! String == "confession_comment" //"ConfessionLikes"
                {
                    print("type of push is Confession confession_comment")
                    
                    let dataDictStr = userInfo["gcm.notification.data"] as! String
                    let dict = convertToDictionary(text: dataDictStr)
                    print("dict = \(String(describing: dict))")
                    
                    if (dict != nil)
                    {
                        let post_id: String = dict?.value(forKey: "post_id") as! String//String(dict?.value(forKey: "post_id") as! Int)
                        print("post_id = \(post_id)")
                        
                        let post_image: String = dict?.value(forKey: "post_image") as! String //String(dict?.value(forKey: "post_image") as! Int)
//                        let post_owner_id: String = (dict!.value(forKey: "post_owner_id") as? String)!
//                        let post_text: String = (dict!.value(forKey: "post_text") as? String)!
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                        let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        navigationController.viewControllers = [openParticularPost]
                        openParticularPost.profilePushBool = true
                        openParticularPost.postIDProfile = post_id
                        
                        if (post_image == "")
                        {
                            openParticularPost.typeOfPostStrProfile = "text"
                        }
                        else
                        {
                            if (post_image != "")
                            {
                                if (post_image.contains("jpg"))
                                {
                                    openParticularPost.typeOfPostStrProfile = "photo"
                                }
                                else
                                {
                                    openParticularPost.typeOfPostStrProfile = "gif"
                                }
                            }
                            else{
                                openParticularPost.typeOfPostStrProfile = "text"
                            }
                        }
                        
                        openParticularPost.typeOfAlbumProfile = "ConfessionLikes"
                        
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }
                else if userInfo["gcm.notification.type"] as! String == "post_comment_likes"
                {
                    print("type of push is Confession confession_comment")
                    
                    let dataDictStr = userInfo["gcm.notification.data"] as! String
                    let dict = convertToDictionary(text: dataDictStr)
                    print("dict = \(String(describing: dict))")
                    
                    if (dict != nil)
                    {
                        let post_id: String = dict?.value(forKey: "post_id") as! String
                        print("post_id = \(post_id)")
                        
                        let post_image: String = dict?.value(forKey: "post_image") as! String
                        let post_owner_id: String = (dict!.value(forKey: "post_owner_id") as? String)!
//                        let post_text: String = (dict!.value(forKey: "post_text") as? String)!
                        
                        let reply_id: String = (dict!.value(forKey: "reply_id") as? String)!
                        let parant_reply_id: String = (dict!.value(forKey: "parant_reply_id") as? String)!
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                        let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        
                        navigationController.viewControllers = [openParticularPost]
                        
                        openParticularPost.postIDReply = post_id
                       
                        openParticularPost.userIDOwnerReply = post_owner_id
                        openParticularPost.postIDOwnerReply = post_owner_id
                        openParticularPost.replyOnReplyId = parant_reply_id
                        
                        if (parant_reply_id == "")
                        {
                            openParticularPost.typeOfAlbumReply = "ReplyLikes"
                            openParticularPost.checkReplyTypeReply = "replyOnPost"
                            openParticularPost.differentiateTypeReply = "firstReplyStr"
                            openParticularPost.postIDOpenPostReply = reply_id //userInfo["gcm.notification.replyIdStr"] as! String
                             openParticularPost.replyIDReply = reply_id
                        }
                        else
                        {
                            openParticularPost.typeOfAlbumReply = "ReplyLikes"
                            openParticularPost.checkReplyTypeReply = "replyOnReply"
                            openParticularPost.differentiateTypeReply = "furtherReply"
                            openParticularPost.postIDOpenPostReply = parant_reply_id //userInfo["gcm.notification.replyIdStr"] as! String
                             openParticularPost.replyIDReply = parant_reply_id
                        }
                  
                        if (post_image == "")
                        {
                            openParticularPost.typeOfPostReply = "text"
                        }
                        else
                        {
                            if (post_image != "")
                            {
                                if (post_image.contains("jpg"))
                                {
                                    openParticularPost.typeOfPostReply = "photo"
                                }
                                else
                                {
                                    openParticularPost.typeOfPostReply = "gif"
                                }
                            }
                            else{
                                openParticularPost.typeOfPostReply = "text"
                            }
                        }
                        
                        openParticularPost.repliesPushBool = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }
                else if userInfo["gcm.notification.type"] as! String == "reply_on_reply"
                {
                    print("type of push is Confession reply_on_reply")
                    
                    let dataDictStr = userInfo["gcm.notification.data"] as! String
                    let dict = convertToDictionary(text: dataDictStr)
                    print("dict = \(String(describing: dict))")
                    
                    if (dict != nil)
                    {
                        let post_id: String = dict?.value(forKey: "post_id") as! String
                        print("post_id = \(post_id)")
                        
                        let post_image: String = dict?.value(forKey: "post_image") as! String
                        let post_owner_id: String = (dict!.value(forKey: "post_owner_id") as? String)!
//                        let post_text: String = (dict!.value(forKey: "post_text") as? String)!
                        
                        let reply_id: String = (dict!.value(forKey: "reply_id") as? String)!
                        let parant_reply_id: String = (dict!.value(forKey: "parant_reply_id") as? String)!
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                        let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        
                        navigationController.viewControllers = [openParticularPost]
                        
                        openParticularPost.postIDReply = post_id
                        
                        openParticularPost.userIDOwnerReply = post_owner_id
                        openParticularPost.postIDOwnerReply = post_owner_id
                        openParticularPost.replyOnReplyId = parant_reply_id
                        
                        if (parant_reply_id == "")
                        {
                            openParticularPost.typeOfAlbumReply = "Comments"
                            openParticularPost.checkReplyTypeReply = "replyOnPost"
                            openParticularPost.differentiateTypeReply = "firstReplyStr"
                            openParticularPost.postIDOpenPostReply = reply_id
                            openParticularPost.replyIDReply = reply_id
                        }
                        else
                        {
                            openParticularPost.typeOfAlbumReply = "Comments"
                            openParticularPost.checkReplyTypeReply = "replyOnReply"
                            openParticularPost.differentiateTypeReply = "furtherReply"
                            openParticularPost.postIDOpenPostReply = parant_reply_id 
                            openParticularPost.replyIDReply = parant_reply_id
                        }
                        
                        if (post_image == "")
                        {
                            openParticularPost.typeOfPostReply = "text"
                        }
                        else
                        {
                            if (post_image != "")
                            {
                                if (post_image.contains("jpg"))
                                {
                                    openParticularPost.typeOfPostReply = "photo"
                                }
                                else
                                {
                                    openParticularPost.typeOfPostReply = "gif"
                                }
                            }
                            else{
                                openParticularPost.typeOfPostReply = "text"
                            }
                        }
                        
                        openParticularPost.repliesPushBool = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }
                else if userInfo["gcm.notification.type"] as! String == "like_media"
                {
                    print("type of push is Confession reply_on_reply")
                    
                    let dataDictStr = userInfo["gcm.notification.data"] as! String
                    let dict = convertToDictionary(text: dataDictStr)
                    print("dict = \(String(describing: dict))")
                    
                    if (dict != nil)
                    {
                        /*
                        let owner: String = (dict!.value(forKey: "owner") as? String)!
                        
                        let caption: String = (dict!.value(forKey: "caption") as? String)!
                        let media: String = (dict!.value(forKey: "media") as? String)!
                         */
                        
                        let media_type: String = (dict!.value(forKey: "media_type") as? String)!
                        let media_id: String = (dict!.value(forKey: "media_id") as? String)!
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                        let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        navigationController.viewControllers = [openParticularPost]
                        openParticularPost.profilePushBool = true
                        openParticularPost.postIDProfile = media_id
                        
                        if (media_type == "1")
                        {
                            openParticularPost.typeOfAlbumProfile = "Pictures"
                        }
                        else{
                            openParticularPost.typeOfAlbumProfile = "Videos"
                        }
                        
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }
                else if userInfo["gcm.notification.type"] as! String == "ReplyLikes"
                {
                    print("type of push is Reply Likes")

                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                    let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    
                    if userInfo["gcm.notification.typeOFReplyStr"] as! String == "replyOnReply"
                    {
//                        openParticularPost.appDelegatePush = "fromAppDelegate"
                        navigationController.viewControllers = [openParticularPost]
                        openParticularPost.postIDReply = userInfo["gcm.notification.postID"] as! String
                        openParticularPost.replyIDReply = userInfo["gcm.notification.replyIdStr"] as! String
                        openParticularPost.userIDOwnerReply = userInfo["gcm.notification.postOwnerID"] as! String
                        openParticularPost.postIDOwnerReply = userInfo["gcm.notification.postOwnerID"] as! String
                        openParticularPost.checkReplyTypeReply = userInfo["gcm.notification.typeOFReplyStr"] as! String
                        openParticularPost.differentiateTypeReply = userInfo["gcm.notification.differentiateString"] as! String
                        openParticularPost.typeOfPostReply = userInfo["gcm.notification.photoUploadType"] as! String
                        openParticularPost.typeOfAlbumReply = userInfo["gcm.notification.type"] as! String
                        openParticularPost.postIDOpenPostReply = userInfo["gcm.notification.replyIdStr"] as! String
                        
                        openParticularPost.replyOnReplyId = userInfo["gcm.notification.replyOnReplyID"] as! String
                    }
                    else
                    {
                        navigationController.viewControllers = [openParticularPost]
                        openParticularPost.postIDReply = userInfo["gcm.notification.postID"] as! String
                        openParticularPost.userIDOwnerReply = userInfo["gcm.notification.postOwnerID"] as! String
                        
                        openParticularPost.replyIDReply = userInfo["gcm.notification.replyIdStr"] as! String
                        openParticularPost.postIDOwnerReply = userInfo["gcm.notification.postOwnerID"] as! String
                        openParticularPost.checkReplyTypeReply = userInfo["gcm.notification.typeOFReplyStr"] as! String
                        openParticularPost.differentiateTypeReply = userInfo["gcm.notification.differentiateString"] as! String
                        openParticularPost.typeOfPostReply = userInfo["gcm.notification.photoUploadType"] as! String
                        openParticularPost.typeOfAlbumReply = userInfo["gcm.notification.type"] as! String
                        openParticularPost.postIDOpenPostReply = userInfo["gcm.notification.replyIdStr"] as! String
                    }
                    openParticularPost.repliesPushBool = true
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                }
                else if userInfo["gcm.notification.type"] as! String == "Albums"
                {
                    print("type of push is Albums")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                    let openParticularPost = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    navigationController.viewControllers = [openParticularPost]
                    openParticularPost.profilePushBool = true
                    openParticularPost.postIDProfile = userInfo["gcm.notification.pictureID"] as! String
                    openParticularPost.typeOfAlbumProfile = userInfo["gcm.notification.albumType"] as! String
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                }
                else if userInfo["gcm.notification.type"] as! String == "Chatroom"
                {
                    print("type of push is Albums")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
                    let openParticularPost = storyboard.instantiateViewController(withIdentifier: "GroupChatMeassagesViewController") as! GroupChatMeassagesViewController
                    openParticularPost.appDelegatePush = "fromAppDelegate"
                    navigationController.viewControllers = [openParticularPost]
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                }
            }
        completionHandler()
    }
}
// [END ios_10_message_handling]

