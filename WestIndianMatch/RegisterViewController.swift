//
//  RegisterViewController.swift
//  WestIndianMatch
//
//  Created by brst on 22/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import MapKit
import CoreLocation
import FBSDKCoreKit
import FBSDKLoginKit
import AVKit
import AVFoundation
import MobileCoreServices
import Photos

class values
{
    var name: String
    var id: String
    
    init(name: String, id: String) {
        self.name = name
        self.id = id
    }
}

class RegisterViewController: UIViewController,UIScrollViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate,UIWebViewDelegate,ImageCropViewControllerDelegate {

     // MARK: - ******  Variables ******
     let playerViewController = AVPlayerViewController()
     var player:AVPlayer?
     var usersAge = Int()
     var instagramUserIdAsPassword = String()
     var getEmailStr = String()
     var userNameStr = String()
     var getDisplayNameStr = String()
     var typeOfUser = String()
     var imagebase64Str = String()
     var getCountryID = String()
     var typeOfStr = String()
     var countrySelectedStr = String()
     var citySelectedStr = String()
     var bodyTypeSelectedStr = String()
     var hairColorSelectedStr = String()
     var relationshipSelectedStr = String()
     var smokingSelectedStr = String()
     var drinkingSelectedStr = String()
     var educationSelectedStr = String()
     var termsBool = Bool()
     var dateSelectedFromPicker = String()
     var categoryStr = ""
     var bodyTypeArray = [String]()
    
    var newBodyTypeArray = [values]()
    
    //var alertSuccess = UIAlertView()
    var femaleCheck = Bool()
    var maleCheck = Bool()
    var gender_type = String()
    var countryNameArray = NSArray()
    var CountryIDArray = NSMutableArray()
    var testArray = NSMutableArray()
    var nameIDArray = NSMutableArray()
    
    var locationManager: CLLocationManager!
    var appDelCheck = String()
    var meetUpStr = String()
    var interestedInStr = String()
    var imageStr = String()
    var faceBookID = String()
    var webViewBool = Bool()
    var useCameraStr = String()
    var videoURL = NSURL()
    var localityStr = String()
    var subLocalityStr = String()
    var mediaType = String()
    var media_uploadedFrom = String()
    
    var currentState = String()
    var currentCity = String()
    var currentCountry = String()
    var currentLatitude = Double()
    var currentLongitude = Double()
    
    var countrySelectedID = String()
    var relationshipSelectedID = String()
    var meetUpSelectedID = String()
    var interestedInSelectedID = String()
    var bodyTypeSelectedID = String()
    var hairColorSelectedID = String()
    var smokingSelectedID = String()
    var drinkingSelectedID = String()
    var educationSelectedID = String()
    var countryApiSelectedID = String()
    
    var isProfileImageSelected = Bool()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    // MARK: - ******  Outlets ******
    
    @IBOutlet weak var registerBtn: UIButton!
    
    @IBOutlet weak var profileWrapperView: UIView!
    @IBOutlet weak var TermsWebView: UIWebView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var meetUpView: UIView!
    @IBOutlet weak var interestedInView: UIView!
    @IBOutlet weak var interestedIn_txtField: UITextField!
    @IBOutlet weak var meetUp_textField: UITextField!
    @IBOutlet weak var country_pickerView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var drinking_wrapperView: UIView!
    @IBOutlet weak var smoking_wrapperView: UIView!
    @IBOutlet weak var relationship_wrapperView: UIView!
    @IBOutlet weak var hairColor_wrapperView: UIView!
    @IBOutlet weak var body_wrapperView: UIView!
    @IBOutlet weak var alreadyMemberView: UIView!
    @IBOutlet weak var GenderView: UIView!
    @IBOutlet weak var educationWrapperView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var education_txtField: UITextField!
    @IBOutlet weak var drinking_txtField: UITextField!
    @IBOutlet weak var smoking_txtField: UITextField!
    @IBOutlet weak var relationshipStatus_txtField: UITextField!
    @IBOutlet weak var hairColor_txtField: UITextField!
    @IBOutlet weak var bodyType_txtField: UITextField!
    @IBOutlet weak var dob_txtField: UITextField!
    @IBOutlet weak var location_txtField: UITextField!
    @IBOutlet weak var island_txtField: UITextField!
    @IBOutlet weak var password_txtField: UITextField!
    @IBOutlet weak var email_txtField: UITextField!
    @IBOutlet weak var username_txtField: UITextField!
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet var AgreementLbl: FRHyperLabel!
    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var termsView: UIView!
    @IBOutlet weak var usersView: UIView!
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var islandView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var countryPickerView: UIPickerView!
    @IBOutlet weak var dateOfBirth_pickerView: UIDatePicker!
    @IBOutlet weak var femaleImage: UIImageView!
    @IBOutlet weak var MaleImagge: UIImageView!
    @IBOutlet weak var termsImage: UIImageView!
    @IBOutlet weak var wrapperPickerView: UIView!
    
    @IBOutlet weak var fullProfileImage: UIImageView!
    @IBOutlet weak var viewProfileFullSizeView: UIView!
    @IBOutlet weak var loadingImage: UIImageView!
    @IBOutlet weak var heading_lbl: UILabel!
    
    
    // MARK: - ****** ViewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isProfileImageSelected = false
        
//        print("appDelegate.newNameIDArray = \(appDelegate.newNameIDArray)")
//        print("appDelegate.getAllDefaultValuesDict = \(appDelegate.getAllDefaultValuesDict)")

        self.registerBtn.isUserInteractionEnabled = true
        self.TermsWebView.isHidden = true
        self.webViewBool = false
        self.useCameraStr = "Upload a photo using camera"
        self.country_pickerView.isHidden = true
        self.countryPickerView.delegate=self
        self.countryPickerView.dataSource=self
        maleCheck = true
        femaleCheck = false
        termsBool = false
        gender_type = "male"
        self.scrollView.delegate = self
        self.scrollView.isScrollEnabled = true
        self.alreadyMemberView.layer.borderWidth = 1
        self.alreadyMemberView.layer.borderColor = UIColor.darkGray.cgColor
        username_txtField.attributedPlaceholder = NSAttributedString(string: "Username",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        email_txtField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        password_txtField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        island_txtField.attributedPlaceholder = NSAttributedString(string: "Which Country Are You Representing?",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        location_txtField.attributedPlaceholder = NSAttributedString(string: "Where are you located?",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        dob_txtField.attributedPlaceholder = NSAttributedString(string: "D.O.B",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        bodyType_txtField.attributedPlaceholder = NSAttributedString(string: "Body Type?",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        hairColor_txtField.attributedPlaceholder = NSAttributedString(string: "Hair Color?",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        relationshipStatus_txtField.attributedPlaceholder = NSAttributedString(string: "Relationship Status",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        smoking_txtField.attributedPlaceholder = NSAttributedString(string: "Smoking?",
                                                                   attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        drinking_txtField.attributedPlaceholder = NSAttributedString(string: "Drinking?",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        education_txtField.attributedPlaceholder = NSAttributedString(string: "Education?",
                                                                attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        meetUp_textField.attributedPlaceholder = NSAttributedString(string: "Meet Up Preferences",
                                                                      attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        interestedIn_txtField.attributedPlaceholder = NSAttributedString(string: "Interested In Meeting Up With",
                                                                      attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        self.wrapperPickerView.isHidden=true
        dateOfBirth_pickerView.setValue(UIColor.white, forKeyPath: "textColor")
        dateOfBirth_pickerView.datePickerMode = .date
        dateOfBirth_pickerView.backgroundColor = UIColor.init(red: 42.0/255, green: 48.0/255, blue: 63.0/255, alpha: 1.0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(mediaCapturedforRegisterProfile), name: NSNotification.Name(rawValue: "mediaCapturedforRegisterProfile"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        Reach().monitorReachabilityChanges()
        
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        
        populateCustomers()
        
        let normalText = "I agree to the"
        let boldText  = "Terms and Privacy Policy"
        let attributedString = NSMutableAttributedString(string:normalText)
        let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15)]
        let boldString = NSMutableAttributedString(string:boldText, attributes:attrs)
        attributedString.append(boldString)

        self.AgreementLbl.attributedText = attributedString
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            if substring == "Terms"
            {
                self.webViewBool = true

                UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                    self.TermsWebView.isHidden = false
                    self.heading_lbl.text = "Terms & Conditions"
                }, completion: nil)
 
                let setURL=URL(string:"https://wimatchapp.com/terms-of-service")
                let setURLRequest=URLRequest(url:setURL!)
                self.TermsWebView.loadRequest(setURLRequest)
            }
            else if substring == "Privacy Policy"
            {
                self.webViewBool = true
                
                UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                    self.TermsWebView.isHidden = false
                    self.heading_lbl.text = "Privacy Policy"
                }, completion: nil)

                let setURL=URL(string:"https://wimatchapp.com/privacy-policy")
                let setURLRequest=URLRequest(url:setURL!)
                self.TermsWebView.loadRequest(setURLRequest)
            }
        }
        
        self.AgreementLbl.setLinksForSubstrings(["Terms" ,"Privacy Policy"], withLinkHandler: handler)
        
        self.getDataFromFacebookAndInstagram()
        UserDefaults.standard.set(" ", forKey: "userRegistered")
        UIApplication.shared.isStatusBarHidden=false
        
        let value = self.profileImageView.frame.size as NSValue
        let Data = NSKeyedArchiver.archivedData(withRootObject: value)
        UserDefaults.standard.set(Data, forKey: "self.imagesScrollView.frame.size")
    }
    
    
    func mediaCapturedforRegisterProfile(value : Notification){
        
        let dic = value.object as! Dictionary<String, Any>
        
        media_uploadedFrom = dic["media_uploadedFrom"] as! String
        
        self.mediaType = dic["mediaType"] as! String
        
        if mediaType == "public.image" {
            
            if (dic["capturedImage"] != nil)
            {
                self.isProfileImageSelected = true
                
                let chosenImage: UIImage = dic["capturedImage"] as! UIImage
                self.profileImageView.image = chosenImage
                self.profileImageView.contentMode = UIViewContentMode.scaleAspectFit
                self.profileImageView.layer.masksToBounds=true
            }
        }
    }

    
    
    // MARK: - ****** viewDidDisappear ******
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    
    // MARK: - ****** UIWebView Delegates ******
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        self.loadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
    }

    
    // MARK: - ****** closeFullImageView_btnAction ******
    @IBAction func closeFullImageView_btnAction(_ sender: Any) {
        self.viewProfileFullSizeView.isHidden = true
    }
    
    
    // MARK: - ****** ViewWillAppear ******
    override func viewWillAppear(_ animated: Bool) {
        self.useCameraStr = "Upload a photo using camera"
        UIApplication.shared.isStatusBarHidden=false
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    //MARK:- ****** didChangeAuthorization ******
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined , .denied , .restricted :
            
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce") //|| (getAllowStr != "alreadyAuthorized")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
            }
            
            break
       
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
      /*  default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
    }
    
    // MARK: - ****** didUpdateLocations ******
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

    if self.locationManager.location != nil
    {
        if let getCoordinates = manager.location?.coordinate
        {
            print("getCoordinates = \(getCoordinates)")
            
            let locValue:CLLocationCoordinate2D = manager.location!.coordinate
            
            self.currentLatitude = locValue.latitude
            self.currentLongitude = locValue.longitude
            
            self.locationManager.stopUpdatingLocation()
            
            if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                
                let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in // this is the last line that is being called
                    var placemark : CLPlacemark!
                    placemark = placemarks?[0]
                    
                    if placemark != nil
                    {
                        var vv =  String()
                        if (placemark.subLocality != nil)
                        {
                            vv =  placemark.subLocality!
                        }
                        
                        var countWhiteSpaces = Int()
                        if (vv.range(of: " ") != nil)
                        {
                          countWhiteSpaces = (vv.components(separatedBy: " ").count) - 1
                        }
                        else
                        {
                            countWhiteSpaces = 0
                        }
                        
                        var objAtIndexBeforeLast = String()
                        var objAtIndexLast = String()
                        if (countWhiteSpaces > 0)
                        {
                            var fullNameArr = vv.components(separatedBy: " ")
                            objAtIndexBeforeLast = (fullNameArr[(fullNameArr.count) - 2])
                            objAtIndexLast = (fullNameArr.last)!
                            self.subLocalityStr = objAtIndexBeforeLast + " " + objAtIndexLast
                        }
                        else{
                            self.subLocalityStr = vv
                        }
                        
                        if (placemark.locality != nil)
                        {
                            self.localityStr = placemark.locality!
                        }
                        
//                        var city = String()
                        if (placemark.addressDictionary?["City"] != nil)
                        {
//                            city = (placemark.addressDictionary?["City"] as! String)
                        }
                        
//                        var country = String()
                        if (placemark.addressDictionary?["Country"] != nil)
                        {
//                            country = (placemark.addressDictionary?["Country"] as! String)
                            self.currentCountry = (placemark.addressDictionary?["Country"] as! String)
                        }
                        else{
                            self.currentCountry = ""
                        }
                        
//                        var state = String()
                        if (placemark.addressDictionary?["State"] != nil)
                        {
//                            state = (placemark.addressDictionary?["State"] as! String)
                        }
                        
                        self.location_txtField.text = self.subLocalityStr + ", " + self.localityStr
                    }
                }
            }
        }
    }
 }
    
    
    
    // MARK: - ****** Get details from Instagram and Facebook login. ******
    func getDataFromFacebookAndInstagram()
    {
        if typeOfUser == "facebookUser"
        {
            //UserDefaults.standard.value(forKey: "userProfilePicture") as! String
            let imageURL: NSURL = NSURL(string: self.imageStr)!
            if let data : NSData = NSData(contentsOf: imageURL as URL)
            {
                self.isProfileImageSelected = true
                
                self.profileImageView.image = UIImage(data: data as Data)!
                self.profileImageView.contentMode = UIViewContentMode.scaleAspectFill
                profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
                profileImageView.layer.masksToBounds=true
            }
            else
            {
                self.profileImageView.image = UIImage(named: "Profile Icon")
            }
            
            self.username_txtField.text = self.getDisplayNameStr
            
            if (self.getEmailStr == "")
            {
                self.email_txtField.text = self.faceBookID + "@gmail.com"
            }
            else
            {
                self.email_txtField.text = self.getEmailStr
            }
            
            self.password_txtField.text = "12345"
            
            self.emailView.isHidden = true
            self.passwordView.isHidden = true
            
            self.locationView.frame.origin.y = self.usersView.frame.origin.y + self.usersView.frame.size.height + 10
            self.dobView.frame.origin.y = self.locationView.frame.origin.y + self.locationView.frame.size.height + 10
            self.GenderView.frame.origin.y = self.dobView.frame.origin.y + self.dobView.frame.size.height + 10
             self.islandView.frame.origin.y = self.GenderView.frame.origin.y + self.GenderView.frame.size.height + 10
            
            self.relationship_wrapperView.frame.origin.y = self.islandView.frame.origin.y + self.islandView.frame.size.height + 10
            self.meetUpView.frame.origin.y = self.relationship_wrapperView.frame.origin.y + self.relationship_wrapperView.frame.size.height + 10
            self.interestedInView.frame.origin.y = self.meetUpView.frame.origin.y + self.meetUpView.frame.size.height + 10
            
            self.body_wrapperView.frame.origin.y = self.interestedInView.frame.origin.y + self.interestedInView.frame.size.height + 10
            self.hairColor_wrapperView.frame.origin.y = self.body_wrapperView.frame.origin.y + self.body_wrapperView.frame.size.height + 10
            self.smoking_wrapperView.frame.origin.y = self.hairColor_wrapperView.frame.origin.y + self.hairColor_wrapperView.frame.size.height + 10
            self.drinking_wrapperView.frame.origin.y = self.smoking_wrapperView.frame.origin.y + self.smoking_wrapperView.frame.size.height + 10
            self.educationWrapperView.frame.origin.y = self.drinking_wrapperView.frame.origin.y + self.drinking_wrapperView.frame.size.height + 10
            self.termsView.frame.origin.y = self.educationWrapperView.frame.origin.y + self.educationWrapperView.frame.size.height + 10
            self.registerView.frame.origin.y = self.termsView.frame.origin.y + self.termsView.frame.size.height + 20
            self.alreadyMemberView.frame.origin.y = self.registerView.frame.origin.y + self.registerView.frame.size.height + 20
        }
        else if typeOfUser == "instagramUser"
        {
            let imageURL: NSURL = NSURL(string: self.imageStr)!
            if let data : NSData = NSData(contentsOf: imageURL as URL)
            {
                self.isProfileImageSelected = true
                
                self.profileImageView.image = UIImage(data: data as Data)!
                self.profileImageView.contentMode = UIViewContentMode.scaleAspectFill
                profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
                profileImageView.layer.masksToBounds=true
            }
            else
            {
                self.profileImageView.image = UIImage(named: "Profile Icon")
            }
            
            self.username_txtField.text = self.getDisplayNameStr
            
            if (self.getEmailStr == "")
            {
                self.email_txtField.text = self.instagramUserIdAsPassword + "@gmail.com"
            }
            else
            {
                self.email_txtField.text = self.getEmailStr
            }
            
            self.password_txtField.text = "12345"
            
            self.emailView.isHidden = true
            self.passwordView.isHidden = true
            
            self.locationView.frame.origin.y = self.usersView.frame.origin.y + self.usersView.frame.size.height + 10
            self.dobView.frame.origin.y = self.locationView.frame.origin.y + self.locationView.frame.size.height + 10
            self.GenderView.frame.origin.y = self.dobView.frame.origin.y + self.dobView.frame.size.height + 10
            self.islandView.frame.origin.y = self.GenderView.frame.origin.y + self.GenderView.frame.size.height + 10
            
            self.relationship_wrapperView.frame.origin.y = self.islandView.frame.origin.y + self.islandView.frame.size.height + 10
            
            self.meetUpView.frame.origin.y = self.relationship_wrapperView.frame.origin.y + self.relationship_wrapperView.frame.size.height + 10
            self.interestedInView.frame.origin.y = self.meetUpView.frame.origin.y + self.meetUpView.frame.size.height + 10
            
            self.body_wrapperView.frame.origin.y = self.interestedInView.frame.origin.y + self.interestedInView.frame.size.height + 10
            self.hairColor_wrapperView.frame.origin.y = self.body_wrapperView.frame.origin.y + self.body_wrapperView.frame.size.height + 10
            self.smoking_wrapperView.frame.origin.y = self.hairColor_wrapperView.frame.origin.y + self.hairColor_wrapperView.frame.size.height + 10
            self.drinking_wrapperView.frame.origin.y = self.smoking_wrapperView.frame.origin.y + self.smoking_wrapperView.frame.size.height + 10
            self.educationWrapperView.frame.origin.y = self.drinking_wrapperView.frame.origin.y + self.drinking_wrapperView.frame.size.height + 10
            self.termsView.frame.origin.y = self.educationWrapperView.frame.origin.y + self.educationWrapperView.frame.size.height + 10
            self.registerView.frame.origin.y = self.termsView.frame.origin.y + self.termsView.frame.size.height + 20
            self.alreadyMemberView.frame.origin.y = self.registerView.frame.origin.y + self.registerView.frame.size.height + 20
        }
        else
        {

            
        }
    }
    
    // MARK: - ****** Fetch Countries and States From DB. ******
    func populateCustomers() {
        let db = FMDBDataAccess()
        self.countryNameArray = db.getCustomers()
        
        var country = String()
        var countryID = String()
        var countryObj = Country()
        
        for i in 0..<self.countryNameArray.count {
            countryObj = self.countryNameArray[i] as! Country
            country = "\(countryObj.firstName!)"
            countryID = "\(countryObj.customerId)"
            testArray.add(country)
            CountryIDArray.add(countryID)
        }
        
        for i in 0..<testArray.count {
            var dict1 = [AnyHashable: Any]()
            dict1["c_name"] = testArray[i]
            dict1["c_id"] = CountryIDArray[i]
            let country_id: String = "\(CountryIDArray[i])"
            if !nameIDArray.contains(country_id) {
                nameIDArray.insert(dict1, at: i)
            }
        }
    }
    
    
    
    // MARK: - ****** Get Network Status Reachibility Method ******
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        let getNetworkStatus = (userInfo?["Status"] as? String)!
        if getNetworkStatus == "Offline"
        {

        }
        else if getNetworkStatus == "online"
        {
            
        }
    }

    // MARK: - ****** UIScrollView Delegate ******
     override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollView.isScrollEnabled=true
        self.scrollView.contentSize.height = self.alreadyMemberView.frame.origin.y + self.alreadyMemberView.frame.size.height + 50
    }
    
    
    // MARK: - ****** Choose Profile Image Button Action. ******
    @IBAction func profileImage_btnAction(_ sender: Any) {
        self.uploadPhotoVideo()
    }
    
    
    // MARK: - ****** ActionSheet For Choosing upload option. ******
    func uploadPhotoVideo() {
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        if self.useCameraStr == " "
        {
            actionSheet.addAction(UIAlertAction(title: "Select a New Profile Picture", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
                self.showActionSheet()
            }))
        }
        else if (self.useCameraStr == "Upload a photo using camera") || (self.useCameraStr == "Upload a photo using gallery") || ( self.useCameraStr == "Upload a video")
        {
            actionSheet.addAction(UIAlertAction(title: "Select a New Profile Picture", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if #available(iOS 10.0, *) {
                    let homeScreen = storyboard.instantiateViewController(withIdentifier: "CameraOverlayViewController") as! CameraOverlay
                    homeScreen.parentClass = self
                    homeScreen.differentiateStr = "RegisterViewController"
                    homeScreen.differentiateMediaStr = "Register Picture"
                    self.navigationController?.present(homeScreen, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: - ****** ActionSheet For choosing Photos. ******
    func showActionSheet() {
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.useCameraStr = "Upload a photo using camera"
            
            let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            if status == .denied {
                PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                    let alert = UIAlertController(
                        title: "IMPORTANT",
                        message: "\("WI Match") Would like to capture the Photos",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else if status == .authorized  || status == .notDetermined
            {
                self.camera()
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.useCameraStr = "Upload a photo using gallery"
            
            let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            if  status == .denied {
                PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                    let alert = UIAlertController(
                        title: "IMPORTANT",
                        message: "\("WI Match") Would like to access the Photos",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else if status == .authorized || status == .notDetermined
            {
                self.photoLibrary()
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** Open Photo Library Method. ******
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        myPickerController.allowsEditing = false
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** ActionSheet For Video uploading. ******
    func showActionSheet_forVideo() {
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.useCameraStr = "Upload a video"
            
            if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                    DispatchQueue.main.async() {
                        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                        switch authStatus {
                        case .authorized: self.camera() // Do your stuff here i.e. callCameraMethod()
                        case .denied: self.alertToEncourageCameraAccessInitially()
                        case .notDetermined: self.alertPromptToAllowCameraAccessViaSetting()
                        default: self.alertToEncourageCameraAccessInitially()
                        }
                    }
                }
            }
            else
            {
                let alert = UIAlertController(title: "", message: "Sorry,Camera is not available to take picture.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }

        }))
   
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.useCameraStr = "Upload a video"
            let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
            switch authStatus {
            case .authorized: self.videoLibrary() // Do your stuff here i.e. callCameraMethod()
            case .denied: self.alertToEncourageCameraAccessInitially()
            case .notDetermined: self.alertPromptToAllowCameraAccessViaSetting()
            default: self.alertToEncourageCameraAccessInitially()
            }
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** Open Camera Method. ******
    func camera()
    {
        
        let microPhoneStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio)
        switch microPhoneStatus {
        case .authorized:
            accessVideoCapture()
            break
        case .notDetermined:
            accessVideoCapture()
            break
        // Has access
        case .denied:
            showAlertForMicrophone()
            break
        case .restricted:
            showAlertForMicrophone()
            break
            
        }
    }

    
    // MARK: - ****** Capture Video Method. ******
    func accessVideoCapture()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.camera
            if self.useCameraStr == "Upload a photo using camera"
            {
                myPickerController.mediaTypes = [kUTTypeImage as String]
                myPickerController.allowsEditing = false
            }
            else if self.useCameraStr == "Upload a video"
            {
                myPickerController.mediaTypes = [kUTTypeMovie as String]
                myPickerController.allowsEditing = false
                myPickerController.videoMaximumDuration = 20.0
            }
            
            self.present(myPickerController, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "", message: "Sorry,Camera is not available to take picture.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - ****** Select Video From Library. ******
    func videoLibrary()
    {
        let microPhoneStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio)
        switch microPhoneStatus {
        case .authorized:
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = true
            myPickerController.videoMaximumDuration = 20.0
            myPickerController.mediaTypes = [kUTTypeMovie as String]
            self.present(myPickerController, animated: true, completion: nil)
            break
        case .notDetermined:
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = true
            myPickerController.videoMaximumDuration = 20.0
            myPickerController.mediaTypes = [kUTTypeMovie as String]
            self.present(myPickerController, animated: true, completion: nil)
            break
        // Has access
        case .denied:
            showAlertForMicrophone()
            break
        case .restricted:
            showAlertForMicrophone()
            break
            
        }
    }
    
    // MARK: - ****** Check Microphone Access Method. ******
    func showAlertForMicrophone()
    {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "\("WI Match") would like to access the microphones to record video.",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Access", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    
    
    // MARK: - ****** Access of Camera For Capturing Videos. ******
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "\("WI Match") Would like to access the Camera for capturing videos",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                DispatchQueue.main.async() {
                    let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                    switch authStatus {
                    case .authorized: self.videoLibrary() // Do your stuff here i.e. callCameraMethod()
                    case .denied: self.alertToEncourageCameraAccessInitially()
                    case .notDetermined: self.alertPromptToAllowCameraAccessViaSetting()
                    default: self.alertToEncourageCameraAccessInitially()
                    }
                }
            }
        }
    }
    
    
    
    // MARK: - ****** UIImagePicker Delegates ******
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
    if self.useCameraStr == "Upload a photo using camera" || self.useCameraStr == "Upload a photo using gallery"
    {
        let pickedImage : UIImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        self.dismiss(animated: true, completion: { () -> Void in
            // Use view controller
            let controller = ImageCropViewController(image: pickedImage)
            controller?.delegate = self
            controller?.blurredBackground = true
            // set the cropped area
            // controller.cropArea = CGRectMake(0, 0, 100, 200);
            self.navigationController?.pushViewController(controller!, animated: true)
        })
    }
    else if self.useCameraStr == "Upload a video"
    {
        self.videoURL = info[UIImagePickerControllerMediaURL] as! NSURL;
        
        self.dismiss(animated: true, completion: { () -> Void in
            
            self.profileImageView.image = self.createThumbnailOfVideoFromFileURL(self.videoURL.absoluteString!)
            self.profileImageView.contentMode = UIViewContentMode.scaleAspectFit
            
        })
        
    }
 }
    
    // MARK: - ****** Generate Thumbnail from video Url. ******
    func createThumbnailOfVideoFromFileURL(_ strVideoURL: String) -> UIImage?{
        let asset = AVAsset(url: URL(string: strVideoURL)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), 100)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            /* error handling here */
            var thumbnailImage = UIImage()
            thumbnailImage = self.createThumbnailOfVideoFromFileURL(self.videoURL.absoluteString!)!
            return thumbnailImage
        }
    }
    
    
    //MARK:- ****** playerDidFinishPlaying ******
    func playerDidFinishPlaying(){
        //now use seek to make current playback time to the specified time in this case (O)
        let duration : Int64 = 0 //(can be Int32 also)
        let preferredTimeScale : Int32 = 1
        let seekTime : CMTime = CMTimeMake(duration, preferredTimeScale)
        self.player?.seek(to: seekTime)
        self.player?.play()
    }
  

    func imageCropViewControllerSuccess(_ controller: UIViewController, didFinishCroppingImage croppedImage: UIImage)
    {
        self.navigationController!.popViewController(animated: true)
        DispatchQueue.main.async{
            self.profileImageView.image = croppedImage
            self.profileImageView.contentMode = UIViewContentMode.scaleAspectFit
            self.profileImageView.layer.masksToBounds=true
        }
    }
    
    
    func imageCropViewControllerDidCancel(_ controller: UIViewController)
    {
        self.navigationController!.popViewController(animated: true)
    }
   

    // MARK: - ****** Rotate Image with Fixed Orientation. ******
    func rotateImageFix(image: UIImage) -> UIImage {
        
        if (image.imageOrientation == UIImageOrientation.up ) {
            return image
        }
        
        UIGraphicsBeginImageContext(image.size)
        
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return copy!
    }
    

    //This method first checks the current orientation of UIImage and then it changes the orientation in a clockwise way and return UIImage.
    func rotateImage(image:UIImage)->UIImage
    {
        var rotatedImage = UIImage();
        switch image.imageOrientation
        {
        case UIImageOrientation.right:
            rotatedImage = UIImage(cgImage:image.cgImage!, scale: 1, orientation:UIImageOrientation.down);
            
        case UIImageOrientation.down:
            rotatedImage = UIImage(cgImage:image.cgImage!, scale: 1, orientation:UIImageOrientation.left);
            
        case UIImageOrientation.left:
            rotatedImage = UIImage(cgImage:image.cgImage!, scale: 1, orientation:UIImageOrientation.up);
            
        default:
            rotatedImage = UIImage(cgImage:image.cgImage!, scale: 1, orientation:UIImageOrientation.right);
        }
        return rotatedImage;
    }
    
    
    
    // MARK: - ****** Country Selection Button Action. ******
    @IBAction func countrySelection_btnAction(_ sender: Any) {
        self.country_pickerView.isHidden = false
        self.countryPickerView.reloadAllComponents()
    }
    
    
    
    // MARK: - ****** Close Picker View Button Action ******
    @IBAction func cancel_pickerView(_ sender: Any) {
            self.country_pickerView.isHidden = true
    }
    
    // MARK: - ****** Done PickerView Button Action. ******
    @IBAction func done_pickerView_btnAction(_ sender: Any) {
        if typeOfStr == "island_txtField"
        {
            self.country_pickerView.isHidden = true
            if countrySelectedStr == ""
            {
                 self.island_txtField.text = ((nameIDArray[0] as! NSDictionary).value(forKey: "c_name") as? String)
                self.countryApiSelectedID = "1"
            }
            else{
                 self.island_txtField.text = countrySelectedStr
            }
        }
        else if typeOfStr == "location_txtField"
        {
            self.country_pickerView.isHidden = true
            if citySelectedStr == ""
            {
                self.location_txtField.text = testArray[0]  as? String
            }
            else{
                self.location_txtField.text = citySelectedStr
            }
        }
        else if typeOfStr == "bodyType_txtField"
        {
            self.country_pickerView.isHidden = true
            if bodyTypeSelectedStr == ""
            {
                self.bodyType_txtField.text = bodyTypeArray[0]
                self.bodyTypeSelectedID = "1"
            }
            else{
                self.bodyType_txtField.text = bodyTypeSelectedStr
            }
        }
        else if  typeOfStr == "hairColor_txtField"
        {
            self.country_pickerView.isHidden = true
            if hairColorSelectedStr == ""
            {
                self.hairColor_txtField.text = bodyTypeArray[0]
                self.hairColorSelectedID = "1"
            }
            else{
               self.hairColor_txtField.text = hairColorSelectedStr
            }
        }
        else if  typeOfStr == "relationshipStatus_txtField"
        {
            self.country_pickerView.isHidden = true
            if relationshipSelectedStr == ""
            {
                self.relationshipStatus_txtField.text = bodyTypeArray[0]
                self.relationshipSelectedID = "1"
            }
            else{
                 self.relationshipStatus_txtField.text = relationshipSelectedStr
            }
        }
        else if  typeOfStr == "smoking_txtField"
        {
            self.country_pickerView.isHidden = true
            if smokingSelectedStr == ""
            {
                self.smoking_txtField.text = bodyTypeArray[0]
                self.smokingSelectedID = "1"
            }
            else{
                self.smoking_txtField.text = smokingSelectedStr
            }
        }
        else if typeOfStr == "drinking_txtField"
        {
            self.country_pickerView.isHidden = true
            if drinkingSelectedStr == ""
            {
                self.drinking_txtField.text = bodyTypeArray[0]
                self.drinkingSelectedID = "1"
            }
            else{
               self.drinking_txtField.text = drinkingSelectedStr
            }
        }
        else if  typeOfStr == "education_txtField"
        {
            self.country_pickerView.isHidden = true
            if educationSelectedStr == ""
            {
                self.education_txtField.text = bodyTypeArray[0]
                self.educationSelectedID = "1"
            }
            else{
                self.education_txtField.text = educationSelectedStr
            }
        }
        else if typeOfStr == "meetUp_textField"
        {
//            meetUpStr = bodyTypeArray[row]
            self.country_pickerView.isHidden = true
            if meetUpStr == ""
            {
                self.meetUp_textField.text = bodyTypeArray[0]
                self.meetUpSelectedID = "1"
            }
            else{
                self.meetUp_textField.text = meetUpStr
            }
        }
        else if   typeOfStr == "interestedIn_txtField"
        {
//            interestedInStr = bodyTypeArray[row]
            self.country_pickerView.isHidden = true
            if interestedInStr == ""
            {
                self.interestedIn_txtField.text = bodyTypeArray[0]
                self.interestedInSelectedID = "1"
            }
            else{
                self.interestedIn_txtField.text = interestedInStr
            }
        }
    }
    
    
    
    // MARK: - ****** UIPickerView Delegates. ******
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
      func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
          if typeOfStr == "island_txtField"
          {
            if (appDelegate.newNameIDArray.count > 0)
            {
                return appDelegate.newNameIDArray.count;
            }
            else
             {
               return nameIDArray.count
             }
           }
           else if typeOfStr == "location_txtField"
           {
            return testArray.count;
          }
           else
           {
             if (self.newBodyTypeArray.count > 0)
             {
                return newBodyTypeArray.count;
             }
             else{
                return bodyTypeArray.count;
            }
         }
    }
    
     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var str = String()
        
        if typeOfStr == "island_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                str = (appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "country_name") as! String
            }
            else{
                str = (nameIDArray[row] as! NSDictionary).value(forKey: "c_name") as! String
            }
        }
        else if typeOfStr == "location_txtField"
        {
            str = testArray[row]  as! String
        }
        else
        {
            if (self.newBodyTypeArray.count > 0)
            {
                str = newBodyTypeArray[row].name
            }
            else{
                 str = bodyTypeArray[row]
            }
            
        }
         return str
    }
    
      func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if typeOfStr == "island_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                countrySelectedStr = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "country_name") as? String)!
                
                self.countryApiSelectedID = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "id") as? String)!
                if countrySelectedStr == "United States"
                {
                    getCountryID = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "id") as? String)!
                    let db = FMDBDataAccess()
                    self.countryNameArray = db.getStatesForCountrySelected(getCountryID)
                    
                    var state: String = ""
                    var countryy = Country()
                    
                    testArray.removeAllObjects()
                    for i in 0..<countryNameArray.count {
                        countryy = countryNameArray[i] as! Country
                        state = "\(countryy.lastName!)"
                        testArray.add(state)
                    }
                    
                    self.countryApiSelectedID = String(describing: row)
                }
            }
            else
            {
                countrySelectedStr = ((nameIDArray[row] as! NSDictionary).value(forKey: "c_name") as? String)!
                
                self.countryApiSelectedID = String(describing: row + 1)
                
                if countrySelectedStr == "United States"
                {
                    getCountryID = ((nameIDArray[row] as! NSDictionary).value(forKey: "c_id") as? String)!
                    let db = FMDBDataAccess()
                    self.countryNameArray = db.getStatesForCountrySelected(getCountryID)
                    
                    var state: String = ""
                    var countryy = Country()
                    
                    testArray.removeAllObjects()
                    for i in 0..<countryNameArray.count {
                        countryy = countryNameArray[i] as! Country
                        state = "\(countryy.lastName!)"
                        testArray.add(state)
                    }
                    
                    self.countryApiSelectedID = String(describing: row)
                }
            }
        }
        else if typeOfStr == "location_txtField"
        {
            citySelectedStr = testArray[row]  as! String
        }
        else if typeOfStr == "bodyType_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                bodyTypeSelectedStr = newBodyTypeArray[row].name
                self.bodyTypeSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                bodyTypeSelectedStr = bodyTypeArray[row]
                self.bodyTypeSelectedID = String(describing: row + 1)
            }
        }
        else if  typeOfStr == "hairColor_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                hairColorSelectedStr = newBodyTypeArray[row].name
                self.hairColorSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                hairColorSelectedStr = bodyTypeArray[row]
                self.hairColorSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "relationshipStatus_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                relationshipSelectedStr = newBodyTypeArray[row].name
                self.relationshipSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                relationshipSelectedStr = bodyTypeArray[row]
                self.relationshipSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "smoking_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                smokingSelectedStr = newBodyTypeArray[row].name
                self.smokingSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                smokingSelectedStr = bodyTypeArray[row]
                self.smokingSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "drinking_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                drinkingSelectedStr = newBodyTypeArray[row].name
                self.drinkingSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                drinkingSelectedStr = bodyTypeArray[row]
                self.drinkingSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "education_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                educationSelectedStr = newBodyTypeArray[row].name
                self.educationSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                educationSelectedStr = bodyTypeArray[row]
                self.educationSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "meetUp_textField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                meetUpStr = newBodyTypeArray[row].name
                self.meetUpSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                meetUpStr = bodyTypeArray[row]
                self.meetUpSelectedID = String(describing: row + 1)
            }
           
        }
        else if typeOfStr == "interestedIn_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                interestedInStr = newBodyTypeArray[row].name
                self.interestedInSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                interestedInStr = bodyTypeArray[row]
                self.interestedInSelectedID = String(describing: row + 1)
            }
        }
    }
    
    // MARK: - ****** Open Date Picker Button Action. ******
    @IBAction func openPicker_btnAction(_ sender: Any) {
        self.username_txtField.resignFirstResponder()
        self.email_txtField.resignFirstResponder()
        self.password_txtField.resignFirstResponder()
        self.location_txtField.resignFirstResponder()
        self.island_txtField.resignFirstResponder()
        self.bodyType_txtField.resignFirstResponder()
        self.hairColor_txtField.resignFirstResponder()
        self.relationshipStatus_txtField.resignFirstResponder()
        self.smoking_txtField.resignFirstResponder()
        self.drinking_txtField.resignFirstResponder()
        self.education_txtField.resignFirstResponder()
        self.wrapperPickerView.isHidden=false
    }
    
    
    // MARK: - ****** Date Picker Button Action ******
    @IBAction func datePicker_btnAction(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
       dateSelectedFromPicker = dateFormatter.string(from: dateOfBirth_pickerView.date)
    }
    
    // MARK: - ****** Selected Date Picker Button Action. ******
    @IBAction func done_btnAction(_ sender: Any) {
        self.dob_txtField.text = dateSelectedFromPicker
        self.wrapperPickerView.isHidden=true
        calculateAge()
    }
    
    //MARK:- ****** hairColor_btnAction ******
    @IBAction func hairColor_btnAction(_ sender: Any) {
        self.categoryStr = "Hair Color"
        bodyTypeArray = ["Red","Black","Dark Brown","Weave (Any color I choose for it to be)","Bald","Light Brown"]
    }
    
    @IBAction func bodyType_btnAction(_ sender: Any) {
        self.categoryStr = "Body Type"
        bodyTypeArray = ["Slimz","Average","Big Boned","Athletic (Ah Have ah Lil Muscle)","Fluffy (More meat than rice)","Petite (Smallie)","Curvy (In All the right places)"]
    }
    
    
    
    @IBAction func relation_btnAction(_ sender: Any) {
        self.categoryStr = "Relationship"
        bodyTypeArray = ["Single (Like ah dolla)","Married (Happily)","In a relationship","Open relationship","Not looking"]
    }
    
    
    @IBAction func smoking_btnAction(_ sender: Any) {
        self.categoryStr = "Smoking"
        bodyTypeArray = ["Always","Sometimes","Never","Trying to quit"]
    }
    
    @IBAction func drinking_btnAction(_ sender: Any) {
        self.categoryStr = "Drinking"
        bodyTypeArray = ["Not a drinker","Social Drinker","Frequent drinker","Communion wine (if that counts)"]
    }
    
    
    @IBAction func education_btnAction(_ sender: Any) {
        self.categoryStr = "Education"
        bodyTypeArray = ["High school (Yes I went to school)","Some College ( I wanted to be smart)","Associates degree (Realize a lil bright)","Bachelors Degree ( Wanted a bess wuk)","Masters ( I rel bright inno)","PhD ( Yah dun kno)","Trade 9 Good with my hands"]
    }
    
    // MARK: - ****** Calculate D.O.B ******
    func calculateAge()
    {
        let now = NSDate()
        let calendar : NSCalendar = NSCalendar.current as NSCalendar
        let ageComponents = calendar.components(.year, from: dateOfBirth_pickerView.date, to: now as Date, options: [])
        usersAge = ageComponents.year!
        if usersAge < 18
        {
            let alert = UIAlertController(title: "", message: "Your age must be 18 years or older.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.dob_txtField.text = ""
            self.wrapperPickerView.isHidden=false
        }
    }
    
    
    
    // MARK: - ****** Close Button Action ******
    @IBAction func cancel_btnAction(_ sender: Any) {
         self.wrapperPickerView.isHidden=true
    }
    
    
    
    // MARK: - ****** Terms & Conditions Button Action. ******
    @IBAction func agreeTerms_btnAction(_ sender: Any) {
         if termsBool == false
         {
            self.termsImage.image = UIImage(named: "Terms Agree Icon")
            termsBool = true
        }
        else
        {
            self.termsImage.image = UIImage(named: "Terms Disagree Icon")
            termsBool = false
        }
    }
    
    
    // MARK: - ****** Male selection button Action. ******
    @IBAction func maleSelection_btnAction(_ sender: Any) {
        if (maleCheck == false)
        {
            MaleImagge.image = UIImage(named: "check gender Icon")
            maleCheck = true
            gender_type = "male"
            femaleImage.image = UIImage(named: "Uncheck gender Icon")
            femaleCheck = false
        }
        else
        {
            MaleImagge.image = UIImage(named: "Uncheck gender Icon")
            maleCheck = false
            gender_type = ""
            femaleImage.image = UIImage(named: "check gender Icon")
            femaleCheck = true
        }
    }
    
    // MARK: - ****** Female Selection Button Action. ******
    @IBAction func femaleSelection_btnAction(_ sender: Any) {
        if (femaleCheck == false)
        {
            femaleImage.image = UIImage(named: "check gender Icon")
            femaleCheck = true
            gender_type = "female"
            MaleImagge.image = UIImage(named: "Uncheck gender Icon")
            maleCheck = false
        }
        else
        {
            femaleImage.image = UIImage(named: "Uncheck gender Icon")
            femaleCheck = false
            gender_type = ""
            MaleImagge.image = UIImage(named: "check gender Icon")
            maleCheck = true
        }
    }
    
    // MARK: - ****** Back Button Action. ******
    @IBAction func back_btnAction(_ sender: Any) {
        if self.typeOfUser == "facebookUser"
        {
            let loginManager: FBSDKLoginManager = FBSDKLoginManager()
            loginManager.logOut()
            FBSDKAccessToken.setCurrent(nil)
            FBSDKProfile.setCurrent(nil)
        }
        else if self.typeOfUser == "InstagramUser"
        {
            let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
            for cookie in cookieJar.cookies! as [HTTPCookie]
            {
                NSLog("cookie.domain = %@", cookie.domain)
                
                if cookie.domain == "www.instagram.com" ||
                    cookie.domain == "api.instagram.com"
                {
                    cookieJar.deleteCookie(cookie)
                }
            }
        }
       
        if appDelCheck == "fromAppDelegate"
        {
           let homeScreen = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
           self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        else
        {
            if self.webViewBool == true
            {
                self.webViewBool = false
                UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                    self.TermsWebView.isHidden = true
                    self.heading_lbl.text = "Register"
                }, completion: nil)
            }
            else
            {
                self.navigationController!.popViewController(animated: true)
            }
        }
    }
    
    // MARK: - ****** Already Member Button Action. ******
    @IBAction func alreadyMember_btnAction(_ sender: Any) {
        let loginScreen = storyboard?.instantiateViewController(withIdentifier: "LoginWithEmailViewController") as! LoginWithEmailViewController
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
   
    
    // MARK: - ****** Register Button Action. ******
    @IBAction func register_btnAction(_ sender: Any) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.isProfileImageSelected == false) || (self.profileImageView.image == UIImage(named: "Profile Icon")) //(self.profileImageView.image == nil) || (self.profileImageView.image == UIImage(named: "Profile Icon"))
            {
                let alert = UIAlertController(title: "", message: "Please add your profile picture", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.username_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please enter your username", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if (self.username_txtField.text?.trimmingCharacters(in: .whitespaces).isEmpty == true){
                let alert = UIAlertController(title: "", message: "Please enter correct username", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.email_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please enter your email", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.password_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please enter your password", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.location_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please enter your location", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.dob_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your date of birth", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if gender_type == ""
            {
                let alert = UIAlertController(title: "", message: "Please choose your gender", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.island_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Island which you are representing", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.relationshipStatus_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your Relationship Status", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.meetUp_textField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your Meet Up Preferences", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.interestedIn_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your Interested In Meeting Up With", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.bodyType_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Body Type", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.hairColor_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Hair Color", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.smoking_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Smoking", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.drinking_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Drinking", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.education_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your Education", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if termsBool == false
            {
                let alert = UIAlertController(title: "", message: "Please agree with the Terms and Privacy Policy", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                self.registerBtn.isUserInteractionEnabled = false
                
                var deviceID = String()
                if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                    deviceID = uuid
                }
                
                var genderID = String()
                if (self.gender_type == "male")
                {
                    genderID = "1"
                }
                else if (self.gender_type == "female")
                {
                     genderID = "2"
                }
                else{
                     genderID = "1"
                }
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                if let refreshedToken = FIRInstanceID.instanceID().token() {
                    appDelegate.myDeviceToken = refreshedToken
                }
                
                
                if (self.typeOfUser == "facebookUser")
                {
                    let img = ApiHandler.resizeImage(image: self.profileImageView.image!, newWidth: 600)
                    ApiHandler.registerUser(userName: self.username_txtField.text!, emailID: self.email_txtField.text!, password: "wima@123", profilePic: img, currentLocation: self.location_txtField.text!, dateOfBirth: self.dob_txtField.text!, gender: genderID, countryRepresenting: self.countryApiSelectedID, relationshipStatus: self.relationshipSelectedID, meetUpPreferences: self.meetUpSelectedID, interestedInMeetingWith: self.interestedInSelectedID, BodyType: self.bodyTypeSelectedID, HairColor: self.hairColorSelectedID, Smoking: self.smokingSelectedID, Drinking: self.drinkingSelectedID, Education: self.educationSelectedID, typeOfUser:"2", myDeviceToken: appDelegate.myDeviceToken, currentLatitude: self.currentLatitude, curentLongitue: currentLongitude, currentState: "", currentCity: "", currentCountry: "", socialID: self.faceBookID, deviceID: deviceID, completion: { (resultDict) in
                        
                        if (resultDict.value(forKey: "status") as? String == "200") //&& (resultDict.value(forKey: "message") as? String == "Registered successfully.")
                        {
                            let dataDict = resultDict.value(forKey: "data") as? NSDictionary
                            
                            UserDefaults.standard.set(dataDict, forKey: "userDetailsDict")
                            UserDefaults.standard.set("12345", forKey: "passwordSaved")
                            UserDefaults.standard.set("facebookUser", forKey: "userType")
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "auth_token")!, forKey: "auth_token")
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic")!, forKey: "userProfilePicture")
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic_thumb")!, forKey: "profile_pic_thumb")
                            UserDefaults.standard.set(dataDict?.value(forKey: "username")!, forKey: "userName")
                            
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "firebase_user_id"), forKey: "firebase_user_id")
                            
                            UserDefaults.standard.set(dataDict?.value(forKey: "id")!, forKey: "UserAuthID")
                            UserDefaults.standard.set(dataDict?.value(forKey: "age") as? String, forKey: "Age")
                            UserDefaults.standard.set(dataDict?.value(forKey: "state") as? String, forKey: "state")
                            UserDefaults.standard.set(dataDict?.value(forKey: "location") as? String, forKey: "Location")
                            UserDefaults.standard.synchronize()
                            
                            let usersFirebaseID = dataDict?.value(forKey: "firebase_user_id") as? String
                           
                            let ref = FIRDatabase.database().reference()
                            var userDetails = [String: Any]()
                            userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                            userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                            userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                            userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                            userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                            userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                            userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String

                            if let refreshedToken = FIRInstanceID.instanceID().token()
                            {
                                userDetails["myDeviceToken"] = refreshedToken
                            }
                            
                            ref.child("Users").child(usersFirebaseID!).updateChildValues(userDetails)
                            
                            var dataDictForGeneralCategory = [String : Any]()
//                            let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                            dataDictForGeneralCategory["Profile_Visibility"] = "On"
                          
                            ref.child("Users").child(usersFirebaseID!).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                            
                            var settingsDict = [String : Any]()
//                            let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                            settingsDict["Wi_Chat_Room"] = "On"
                            settingsDict["Messages"] = "On"
                          
                            ref.child("Users").child(usersFirebaseID!).child("Settings").child("Notification").updateChildValues(settingsDict)
                            
                            let interestsScreen = self.storyboard?.instantiateViewController(withIdentifier: "AddInterestsViewController") as! AddInterestsViewController
                            interestsScreen.checkViewType = "fromRegisterScreen"
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.registerBtn.isUserInteractionEnabled = true
                            UserDefaults.standard.set("success", forKey: "userRegistered")
                            UserDefaults.standard.set("facebookUser", forKey: "userType")
                            UserDefaults.standard.set("", forKey: "userInterests")
                            self.navigationController?.pushViewController(interestsScreen, animated: true)
                        }
                        else if (resultDict.value(forKey: "status") as? String == "400")  && (resultDict.value(forKey: "message") as? String == "Invalid email or password.")
                        {
                            let message = resultDict.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.registerBtn.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.registerBtn.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
                else if (self.typeOfUser == "instagramUser")
                {
                     let img = ApiHandler.resizeImage(image: self.profileImageView.image!, newWidth: 600)
                    ApiHandler.registerUser(userName: self.username_txtField.text!, emailID: self.email_txtField.text!, password: "wima@123", profilePic: img, currentLocation: self.location_txtField.text!, dateOfBirth: self.dob_txtField.text!, gender: genderID, countryRepresenting: self.countryApiSelectedID, relationshipStatus: self.relationshipSelectedID, meetUpPreferences: self.meetUpSelectedID, interestedInMeetingWith: self.interestedInSelectedID, BodyType: self.bodyTypeSelectedID, HairColor: self.hairColorSelectedID, Smoking: self.smokingSelectedID, Drinking: self.drinkingSelectedID, Education: self.educationSelectedID, typeOfUser:"3", myDeviceToken: appDelegate.myDeviceToken, currentLatitude: self.currentLatitude, curentLongitue: currentLongitude, currentState: "", currentCity: "", currentCountry: "", socialID: self.faceBookID, deviceID: deviceID, completion: { (resultDict) in
                        
                        if (resultDict.value(forKey: "status") as? String == "200") //&& (resultDict.value(forKey: "message") as? String == "Registered successfully.")
                        {
                            let dataDict = resultDict.value(forKey: "data") as? NSDictionary
                            
                            UserDefaults.standard.set(dataDict, forKey: "userDetailsDict")
                            UserDefaults.standard.set("12345", forKey: "passwordSaved")
                            UserDefaults.standard.set("instagramUser", forKey: "userType")
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "auth_token")!, forKey: "auth_token")
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic")!, forKey: "userProfilePicture")
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic_thumb")!, forKey: "profile_pic_thumb")
                            UserDefaults.standard.set(dataDict?.value(forKey: "username")!, forKey: "userName")
                           
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "firebase_user_id"), forKey: "firebase_user_id")
                            
                            UserDefaults.standard.set(dataDict?.value(forKey: "id")!, forKey: "UserAuthID")
                            UserDefaults.standard.set(dataDict?.value(forKey: "age") as? String, forKey: "Age")
                            UserDefaults.standard.set(dataDict?.value(forKey: "location") as? String, forKey: "Location")
                            UserDefaults.standard.set(dataDict?.value(forKey: "state") as? String, forKey: "state")
                            UserDefaults.standard.synchronize()
                            
                            let usersFirebaseID = dataDict?.value(forKey: "firebase_user_id") as? String

                            let ref = FIRDatabase.database().reference()
                            var userDetails = [String: Any]()
                            userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                            userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                            userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                            userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                            userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                            userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                            userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                            
                            if let refreshedToken = FIRInstanceID.instanceID().token()
                            {
                                userDetails["myDeviceToken"] = refreshedToken
                            }
                            
                            ref.child("Users").child(usersFirebaseID!).updateChildValues(userDetails)
                            
                            
                            var dataDictForGeneralCategory = [String : Any]()
//                            let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                            dataDictForGeneralCategory["Profile_Visibility"] = "On"
                           
                            ref.child("Users").child(usersFirebaseID!).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                            
                            var settingsDict = [String : Any]()
//                            let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                            settingsDict["Wi_Chat_Room"] = "On"
                            settingsDict["Messages"] = "On"
                            
                            ref.child("Users").child(usersFirebaseID!).child("Settings").child("Notification").updateChildValues(settingsDict)
                            
                            let interestsScreen = self.storyboard?.instantiateViewController(withIdentifier: "AddInterestsViewController") as! AddInterestsViewController
                            interestsScreen.checkViewType = "fromRegisterScreen"
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.registerBtn.isUserInteractionEnabled = true
                            UserDefaults.standard.set("success", forKey: "userRegistered")
                            UserDefaults.standard.set("instagramUser", forKey: "userType")
                            UserDefaults.standard.set("", forKey: "userInterests")
                            self.navigationController?.pushViewController(interestsScreen, animated: true)
                        }
                        else if (resultDict.value(forKey: "status") as? String == "400")
                        {
                            let message = resultDict.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.registerBtn.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.registerBtn.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
                else
                {
                    let img = ApiHandler.resizeImage(image: self.profileImageView.image!, newWidth: 600)
                    ApiHandler.registerUser(userName: self.username_txtField.text!, emailID: self.email_txtField.text!, password: self.password_txtField.text!, profilePic: img, currentLocation: self.location_txtField.text!, dateOfBirth: self.dob_txtField.text!, gender: genderID, countryRepresenting: self.countryApiSelectedID, relationshipStatus: self.relationshipSelectedID, meetUpPreferences: self.meetUpSelectedID, interestedInMeetingWith: self.interestedInSelectedID, BodyType: self.bodyTypeSelectedID, HairColor: self.hairColorSelectedID, Smoking: self.smokingSelectedID, Drinking: self.drinkingSelectedID, Education: self.educationSelectedID, typeOfUser:"1", myDeviceToken: appDelegate.myDeviceToken, currentLatitude: self.currentLatitude, curentLongitue: currentLongitude, currentState: "", currentCity: "", currentCountry: "", socialID: self.faceBookID, deviceID: deviceID, completion: { (resultDict) in
                        
                        if (resultDict.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = resultDict.value(forKey: "data") as? NSDictionary
                            
                            UserDefaults.standard.set(dataDict, forKey: "userDetailsDict")
                            UserDefaults.standard.set(self.password_txtField.text, forKey: "passwordSaved")
                            UserDefaults.standard.set("emailUser", forKey: "userType")
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "auth_token")!, forKey: "auth_token")
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic")!, forKey: "userProfilePicture")
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic_thumb")!, forKey: "profile_pic_thumb")
                            UserDefaults.standard.set(dataDict?.value(forKey: "username")!, forKey: "userName")
                            UserDefaults.standard.set(dataDict?.value(forKey: "id")!, forKey: "UserAuthID")
                            UserDefaults.standard.set(dataDict?.value(forKey: "id")!, forKey: "UserAuthID")
                            
                            UserDefaults.standard.setValue(dataDict?.value(forKey: "firebase_user_id"), forKey: "firebase_user_id")
                            
                            UserDefaults.standard.set(dataDict?.value(forKey: "age") as? String, forKey: "Age")
                            UserDefaults.standard.set(dataDict?.value(forKey: "location") as? String, forKey: "Location")
                            UserDefaults.standard.set(dataDict?.value(forKey: "state") as? String, forKey: "state")
                            UserDefaults.standard.synchronize()
                            
                            let usersFirebaseID = dataDict?.value(forKey: "firebase_user_id") as? String
                            
                            
                            let ref = FIRDatabase.database().reference()
                            var userDetails = [String: Any]()
                            userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                            userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                            userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                            userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                            userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                            userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                            userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                            
                            if let refreshedToken = FIRInstanceID.instanceID().token()
                            {
                                userDetails["myDeviceToken"] = refreshedToken
                            }
                            
                            ref.child("Users").child(usersFirebaseID!).updateChildValues(userDetails)
                            
                            
                            var dataDictForGeneralCategory = [String : Any]()
//                            let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                            dataDictForGeneralCategory["Profile_Visibility"] = "On"
                        
                            
                            ref.child("Users").child(usersFirebaseID!).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                            
                            var settingsDict = [String : Any]()
                            settingsDict["Wi_Chat_Room"] = "On"
                            settingsDict["Messages"] = "On"
                            
                            ref.child("Users").child(usersFirebaseID!).child("Settings").child("Notification").updateChildValues(settingsDict)
                            
                            let interestsScreen = self.storyboard?.instantiateViewController(withIdentifier: "AddInterestsViewController") as! AddInterestsViewController
                            interestsScreen.checkViewType = "fromRegisterScreen"
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.registerBtn.isUserInteractionEnabled = true
                            UserDefaults.standard.set("success", forKey: "userRegistered")
                            UserDefaults.standard.set("emailUser", forKey: "userType")
                            UserDefaults.standard.set("", forKey: "userInterests")
                            self.navigationController?.pushViewController(interestsScreen, animated: true)
                        }
                        else if (resultDict.value(forKey: "status") as? String == "400")
                        {
                            let message = resultDict.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.registerBtn.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.registerBtn.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
            }
        }
    }
    
    
    // MARK: - ****** Upload Video Status Method ******
    func uploadVideo(_ path: URL, _ userID: String,
                     metadataEsc: @escaping (URL, FIRStorageReference)->(),
                     progressEsc: @escaping (Progress)->(),
                     completionEsc: @escaping ()->(),
                     errorEsc: @escaping (Error)->()) {
        
        let localFile: URL = path
        let videoName = getName()
        let nameRef = FIRStorage.storage().reference().child(UserDefaults.standard.value(forKey: "UserAuthID") as! String).child(videoName)
        let metadata = FIRStorageMetadata()
        metadata.contentType = "video"
        
        let uploadTask = nameRef.putFile(localFile, metadata: metadata) { metadata, error in
            if error != nil {
                errorEsc(error!)
            } else {
                if let meta = metadata {
                    if let url = meta.downloadURL() {
                        metadataEsc(url, nameRef)
                    }
                }
            }
        }
        
        _ = uploadTask.observe(.progress, handler: { snapshot in
            if let progressSnap = snapshot.progress {
                progressEsc(progressSnap)
            }
        })
        
        _ = uploadTask.observe(.success, handler: { snapshot in
            if snapshot.status == .success {
                uploadTask.removeAllObservers()
                completionEsc()
            }
        })
    }

    // MARK: - ****** Generate Video naem with date and time. ******
    func getName() -> String {
        let dateFormatter = DateFormatter()
        let dateFormat = "yyyyMMddHHmmss"
        dateFormatter.dateFormat = dateFormat
        let name = "profileimage.mp4"
        return name
    }
    
    
   
            
    //MARK:- ****** EMAIL VALIDATION ******
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK:- ****** UITEXTFIELD DELEGATE ******
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
       
    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.island_txtField
        {
            self.island_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            typeOfStr = "island_txtField"
            self.country_pickerView.isHidden = false
            self.countryPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.location_txtField
        {
             typeOfStr = "location_txtField"
            self.location_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()

            if countrySelectedStr == "United States"
            {
                self.country_pickerView.isHidden = false
                self.countryPickerView.reloadAllComponents()
                //use this line for going at the top of the label index 0:
                self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
                return false
            }
        }
        else if textField == self.bodyType_txtField
        {
            self.bodyType_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            self.newBodyTypeArray.removeAll()
           
            if let body_type_Array =  appDelegate.getAllDefaultValuesDict.value(forKey: "body_type") as? NSArray
            {
                for index in 0..<body_type_Array.count
                {
                    let dict = body_type_Array[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            typeOfStr = "bodyType_txtField"
            self.country_pickerView.isHidden = false
            bodyTypeArray = ["Slimz","Average","Big Boned","Athletic (Ah Have ah Lil Muscle)","Fluffy (More meat than rice)","Petite (Smallie)","Curvy (In All the right places)","Ask meh"]
            self.countryPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.hairColor_txtField
        {
            self.hairColor_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
             self.newBodyTypeArray.removeAll()
            
            if let haircolor =  appDelegate.getAllDefaultValuesDict.value(forKey: "haircolor") as? NSArray
            {
                for index in 0..<haircolor.count
                {
                    let dict = haircolor[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            typeOfStr = "hairColor_txtField"
            self.country_pickerView.isHidden = false
           bodyTypeArray = ["Red","Black","Dark Brown","Weave (Any color I choose for it to be)","Bald","Light Brown","Ask meh"]
            self.countryPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.relationshipStatus_txtField
        {
            self.relationshipStatus_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
             self.newBodyTypeArray.removeAll()
            
            if let relationshipstatus =  appDelegate.getAllDefaultValuesDict.value(forKey: "relationshipstatus") as? NSArray
            {
                for index in 0..<relationshipstatus.count
                {
                    let dict = relationshipstatus[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            typeOfStr = "relationshipStatus_txtField"
            self.country_pickerView.isHidden = false
            bodyTypeArray = ["Single (Like ah dolla)","Married (Happily)","In a relationship","Open relationship","Not looking"]
            self.countryPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.smoking_txtField
        {
            self.smoking_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
             self.newBodyTypeArray.removeAll()
            
            if let smoking = appDelegate.getAllDefaultValuesDict.value(forKey: "smoking") as? NSArray
            {
                for index in 0..<smoking.count
                {
                    let dict = smoking[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            typeOfStr = "smoking_txtField"
            self.country_pickerView.isHidden = false
            bodyTypeArray = ["Always","Sometimes","Never","Trying to quit","Ask meh"]
            self.countryPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.drinking_txtField
        {
            self.drinking_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
             self.newBodyTypeArray.removeAll()
            
            if let drinking =  appDelegate.getAllDefaultValuesDict.value(forKey: "drinking") as? NSArray
            {
                for index in 0..<drinking.count
                {
                    let dict = drinking[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            typeOfStr = "drinking_txtField"
            self.country_pickerView.isHidden = false
            bodyTypeArray = ["Not a drinker","Social Drinker","Frequent drinker","Communion wine (if that counts)","Ask meh"]
            self.countryPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.education_txtField
        {
            self.education_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            self.newBodyTypeArray.removeAll()
            if let education =  appDelegate.getAllDefaultValuesDict.value(forKey: "education") as? NSArray
            {
                for index in 0..<education.count
                {
                    let dict = education[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }

            typeOfStr = "education_txtField"
            self.country_pickerView.isHidden = false
            bodyTypeArray = ["High school (Yes I went to school)","Some College ( I wanted to be smart)","Associates degree (Realize a lil bright)","Bachelors Degree ( Wanted a bess wuk)","Masters ( I rel bright inno)","PhD ( Yah dun kno)","Trade (I'm good with my hands)","Ask meh"]
            self.countryPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.meetUp_textField
        {
            self.education_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
             self.newBodyTypeArray.removeAll()
            
            if let meetuppreferences =  appDelegate.getAllDefaultValuesDict.value(forKey: "meetuppreferences") as? NSArray
            {
                for index in 0..<meetuppreferences.count
                {
                    let dict = meetuppreferences[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            typeOfStr = "meetUp_textField"
            self.country_pickerView.isHidden = false
            bodyTypeArray = ["Looking For A Potential Relationship","Not Looking For A Relationship","Maybe Dinner & Conversation","Meet Up For Drinks","Meet Up For A Fete","Meet Up To Make A New Friendship"]
            self.countryPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.interestedIn_txtField
        {
            self.education_txtField.resignFirstResponder()
            self.username_txtField.resignFirstResponder()
            self.email_txtField.resignFirstResponder()
            self.password_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_txtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationshipStatus_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_textField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            self.newBodyTypeArray.removeAll()
            
            if let interestesin =  appDelegate.getAllDefaultValuesDict.value(forKey: "interestesin") as? NSArray
            {
                for index in 0..<interestesin.count
                {
                    let dict = interestesin[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            typeOfStr = "interestedIn_txtField"
            self.country_pickerView.isHidden = false
            bodyTypeArray = ["Women","Men","Both","Interested in just socializing"]
            self.countryPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countryPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
