//
//  ConversationsTBCell.swift
//  Wi Match
//
//  Created by brst on 14/04/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class ConversationsTBCell: UITableViewCell {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var onlineIcon: UIImageView!
    @IBOutlet weak var wrapperBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var unseenMessages_lbl: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
  
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
