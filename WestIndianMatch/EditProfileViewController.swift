//
//  EditProfileViewController.swift
//  Wi Match
//
//  Created by brst on 05/04/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Firebase
import SDWebImage
import Photos
import AVKit
import MobileCoreServices

class EditProfileViewController: UIViewController ,UIScrollViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate,UITextFieldDelegate,ImageCropViewControllerDelegate{
    
    
    // MARK: - ***** VARIABLES DECLARATION *****
    var profileVideoStr = String()
    var profileImageType = String()
    let playerViewController = AVPlayerViewController()
    var player:AVPlayer?
    var typeOfStr = String()
    var countryNameArray = NSArray()
    var CountryIDArray = NSMutableArray()
    var testArray = NSMutableArray()
    var nameIDArray = NSMutableArray()
    var locationManager: CLLocationManager!
    var usersAge = Int()
    var dateSelectedFromPicker = String()
    var countrySelectedStr = String()
    var citySelectedStr = String()
    var bodyTypeSelectedStr = String()
    var hairColorSelectedStr = String()
    var relationshipSelectedStr = String()
    var smokingSelectedStr = String()
    var drinkingSelectedStr = String()
    var educationSelectedStr = String()
    var bodyTypeArray = [String]()
    var meetUpStr = String()
    var interestedInStr = String()
    var imagebase64Str = String()
    var getCountryID = String()
    var femaleCheck = Bool()
    var maleCheck = Bool()
    var gender_type = String()
    var usersDetailsDict = NSDictionary()
    var currentLatitude = Double()
    var currentLongitude = Double()
    var useCameraStr = String()
    var videoURL = NSURL()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var newBodyTypeArray = [values]()
    var countrySelectedID = String()
    var relationshipSelectedID = String()
    var meetUpSelectedID = String()
    var interestedInSelectedID = String()
    var bodyTypeSelectedID = String()
    var hairColorSelectedID = String()
    var smokingSelectedID = String()
    var drinkingSelectedID = String()
    var educationSelectedID = String()
    var countryApiSelectedID = String()
    
    var mediaType = String()
    var media_uploadedFrom = String()
    
    var city = String()
    var state = String()
    var localityStr = String()
    var subLocalityStr = String()
    var currentLocationCountry = String()
    
    var userType_Str = String()
    var emailAddressStr = String()
    
    
    // MARK: - ***** OUTLETS DECLARATION *****
    @IBOutlet weak var fullImageView: UIImageView!
    @IBOutlet weak var viewProfileImageView: UIView!
    @IBOutlet weak var profileWrapperView: UIView!
    @IBOutlet weak var maleImageView: UIImageView!
    @IBOutlet weak var femaleImageView: UIImageView!
    @IBOutlet weak var wrapperPickerView: UIView!
    @IBOutlet weak var countrPickerView: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePicker_wrapperView: UIView!
    @IBOutlet weak var wrapperScrollView: UIScrollView!
    @IBOutlet weak var education_txtField: UITextField!
    @IBOutlet weak var drinking_txtField: UITextField!
    @IBOutlet weak var smoking_txtField: UITextField!
    @IBOutlet weak var hairColor_txtField: UITextField!
    @IBOutlet weak var bodyType_ttxtField: UITextField!
    @IBOutlet weak var interestedIn_txtField: UITextField!
    @IBOutlet weak var meetUp_txtField: UITextField!
    @IBOutlet weak var relationship_txtField: UITextField!
    @IBOutlet weak var island_txtField: UITextField!
    @IBOutlet weak var dob_txtField: UITextField!
    @IBOutlet weak var location_txtField: UITextField!
    @IBOutlet weak var userName_txtField: UITextField!
    @IBOutlet weak var ProfileImageView: UIImageView!
    @IBOutlet weak var EditButton: UIBarButtonItem!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var updateBtnView: UIView!
    @IBOutlet weak var loadingImage: UIImageView!
    @IBOutlet weak var updateProfileBtn: UIButton!
    
    
    
    // MARK: - ****** ViewDidLoad. ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print("appDelegate.newNameIDArray = \(appDelegate.newNameIDArray)")
//        print("appDelegate.getAllDefaultValuesDict = \(appDelegate.getAllDefaultValuesDict)")
        
        self.useCameraStr = "Upload a photo using gallery"
        self.datePicker_wrapperView.isHidden=true
        self.wrapperPickerView.isHidden = true
        self.countrPickerView.delegate=self
        self.countrPickerView.dataSource=self
        maleCheck = true
        femaleCheck = false
        gender_type = "male"
        populateCustomers()
        self.wrapperScrollView.delegate = self
        self.wrapperScrollView.isScrollEnabled = true
        userName_txtField.attributedPlaceholder = NSAttributedString(string: "Username",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        island_txtField.attributedPlaceholder = NSAttributedString(string: "Which Country Are You Representing?",
                                                                   attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        location_txtField.attributedPlaceholder = NSAttributedString(string: "Where are you located?",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        dob_txtField.attributedPlaceholder = NSAttributedString(string: "D.O.B",
                                                                attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        bodyType_ttxtField.attributedPlaceholder = NSAttributedString(string: "Body Type?",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        hairColor_txtField.attributedPlaceholder = NSAttributedString(string: "Hair Color?",
                                                                      attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        relationship_txtField.attributedPlaceholder = NSAttributedString(string: "Relationship Status",
                                                                               attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        smoking_txtField.attributedPlaceholder = NSAttributedString(string: "Smoking?",
                                                                    attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        drinking_txtField.attributedPlaceholder = NSAttributedString(string: "Drinking?",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        education_txtField.attributedPlaceholder = NSAttributedString(string: "Education?",
                                                                      attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        meetUp_txtField.attributedPlaceholder = NSAttributedString(string: "Meet Up Preferences",
                                                                    attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        interestedIn_txtField.attributedPlaceholder = NSAttributedString(string: "Interested In Meeting Up With",
                                                                         attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        
        self.wrapperPickerView.isHidden=true
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = UIColor.init(red: 42.0/255, green: 48.0/255, blue: 63.0/255, alpha: 1.0)
      
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        
        UIApplication.shared.isStatusBarHidden=false
        
        let value = self.ProfileImageView.frame.size as NSValue
        let Data = NSKeyedArchiver.archivedData(withRootObject: value)
        
        UserDefaults.standard.set(Data, forKey: "self.imagesScrollView.frame.size")
        
        NotificationCenter.default.addObserver(self, selector: #selector(mediaCapturedforEditProfile), name: NSNotification.Name(rawValue: "mediaCapturedforEditProfile"), object: nil)
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            self.userName_txtField.text = ""
            self.location_txtField.text = ""
            self.dob_txtField.text = ""
            self.island_txtField.text = ""
            self.relationship_txtField.text = ""
            self.meetUp_txtField.text = ""
            self.interestedIn_txtField.text = ""
            self.bodyType_ttxtField.text = ""
            self.hairColor_txtField.text = ""
            self.smoking_txtField.text = ""
            self.drinking_txtField.text = ""
            self.education_txtField.text = ""
            self.ProfileImageView.image = UIImage(named: "Default Icon")
            self.maleImageView.image = UIImage(named: "Uncheck gender Icon")
            self.femaleImageView.image = UIImage(named: "Uncheck gender Icon")
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.getUsersDetails()
        }
    }
    
    
    func mediaCapturedforEditProfile(value : Notification){
        
        let dic = value.object as! Dictionary<String, Any>
        media_uploadedFrom = dic["media_uploadedFrom"] as! String
        self.mediaType = dic["mediaType"] as! String
        
        if mediaType == "public.image" {
            
            if (dic["capturedImage"] != nil)
            {
                let chosenImage: UIImage = dic["capturedImage"] as! UIImage
                self.ProfileImageView.image = chosenImage
                self.ProfileImageView.contentMode = UIViewContentMode.scaleAspectFit
                self.ProfileImageView.layer.masksToBounds=true
                let newData = UIImageJPEGRepresentation(self.rotateImageFix(image: self.ProfileImageView.image!),1.0)
                self.imagebase64Str = (newData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)))!
            }
        }
    }

    
    // MARK: - ****** ViewWillAppear. ******
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.isStatusBarHidden=false
         self.navigationController?.isNavigationBarHidden = true
    }
    
    
    // MARK: - ****** closeViewProfileImage_btnAction. ******
    @IBAction func closeViewProfileImage_btnAction(_ sender: Any) {
        self.viewProfileImageView.isHidden = true
        if self.player != nil
        {
            playerViewController.player!.pause()
            if let play = player {
                play.pause()
                player = nil
            } else {
                print("player was already deallocated")
            }
        }
    }
    
    func getUsersDetails()
    {
        if let myDetailsDict = UserDefaults.standard.value(forKey: "userDetailsDict") as? NSMutableDictionary
        {
            self.usersDetailsDict = myDetailsDict
            var userGenderTypeStr  = String()
            userGenderTypeStr = self.usersDetailsDict.value(forKey: "gender") as! String
            if (userGenderTypeStr == "male") || (userGenderTypeStr == "Male") || (userGenderTypeStr == "1")
            {
                maleCheck = true
                femaleCheck = false
                gender_type = "male"
                maleImageView.image = UIImage(named: "check gender Icon")
                femaleImageView.image = UIImage(named: "Uncheck gender Icon")
            }
            else
            {
                femaleCheck = true
                gender_type = "female"
                maleCheck = false
                femaleImageView.image = UIImage(named: "check gender Icon")
                maleImageView.image = UIImage(named: "Uncheck gender Icon")
            }

            self.userType_Str = (self.usersDetailsDict.value(forKey: "user_type") as? String)!
            self.emailAddressStr = (self.usersDetailsDict.value(forKey: "email") as? String)!
            
            self.userName_txtField.text = self.usersDetailsDict.value(forKey: "username") as? String
            
            let keysArray = self.usersDetailsDict.allKeys as! [String]
            
            self.dob_txtField.text = self.usersDetailsDict.value(forKey: "dob") as? String
            
            if keysArray.contains("location")
            {
                self.location_txtField.text = self.usersDetailsDict.value(forKey: "location") as? String
            }
            else{
                self.location_txtField.text = ""
            }
            
         
            if let countryStr = self.usersDetailsDict.value(forKey: "country") as? NSDictionary
            {
                if let country_name = countryStr.value(forKey: "country_name") as? String
                {
                    self.island_txtField.text  = country_name
                }
                else{
                    self.island_txtField.text = ""
                }
                
                if let id = countryStr.value(forKey: "id") as? String
                {
                    self.countryApiSelectedID  = id
                }
                else{
                    self.countryApiSelectedID = "1"
                }
            }
            else{
               self.island_txtField.text = ""
            }
            
            
            if keysArray.contains("profileImageType")
            {
                self.profileImageType = (self.usersDetailsDict.value(forKey: "profileImageType") as? String)!
            }
            else{
                self.profileImageType = "image"
            }
            
            if let bodytype = self.usersDetailsDict.value(forKey: "bodytype") as? NSDictionary
            {
                if let bodytypeName = bodytype.value(forKey: "name") as? String
                {
                    self.bodyType_ttxtField.text = bodytypeName
                }
                
                if let id = bodytype.value(forKey: "id") as? String
                {
                    self.bodyTypeSelectedID  = id
                }
                else{
                    self.bodyTypeSelectedID = "1"
                }
            }
            
            if let drinkingtype = self.usersDetailsDict.value(forKey: "drinking") as? NSDictionary
            {
                if let drinkingName = drinkingtype.value(forKey: "name") as? String
                {
                    self.drinking_txtField.text = drinkingName
                }
                
                if let id = drinkingtype.value(forKey: "id") as? String
                {
                    self.drinkingSelectedID  = id
                }
                else{
                    self.drinkingSelectedID = "1"
                }
            }
            
            if let educationtype = self.usersDetailsDict.value(forKey: "education") as? NSDictionary
            {
                if let educationName = educationtype.value(forKey: "name") as? String
                {
                    self.education_txtField.text = educationName
                }
                
                if let id = educationtype.value(forKey: "id") as? String
                {
                    self.educationSelectedID  = id
                }
                else{
                    self.educationSelectedID = "1"
                }
            }
            
            if let haircolortype = self.usersDetailsDict.value(forKey: "haircolor") as? NSDictionary
            {
                if let haircolorName = haircolortype.value(forKey: "name") as? String
                {
                    self.hairColor_txtField.text = haircolorName
                }
                
                if let id = haircolortype.value(forKey: "id") as? String
                {
                    self.hairColorSelectedID  = id
                }
                else{
                    self.hairColorSelectedID = "1"
                }
            }
            
            if let interestesintype = self.usersDetailsDict.value(forKey: "interestesin") as? NSDictionary
            {
                if let interestesinName = interestesintype.value(forKey: "name") as? String
                {
                    self.interestedIn_txtField.text = interestesinName
                }
                
                if let id = interestesintype.value(forKey: "id") as? String
                {
                    self.interestedInSelectedID  = id
                }
                else{
                    self.interestedInSelectedID = "1"
                }
            }
            
            
            if let meetuppreferences = self.usersDetailsDict.value(forKey: "meetuppreferences") as? NSDictionary
            {
                if let meetuppreferencesName = meetuppreferences.value(forKey: "name") as? String
                {
                    self.meetUp_txtField.text = meetuppreferencesName
                }
                
                if let id = meetuppreferences.value(forKey: "id") as? String
                {
                    self.meetUpSelectedID  = id
                }
                else{
                    self.meetUpSelectedID = "1"
                }
            }
            
            if let relationshipstatus = self.usersDetailsDict.value(forKey: "relationshipstatus") as? NSDictionary
            {
                if let relationshipstatusName = relationshipstatus.value(forKey: "name") as? String
                {
                    self.relationship_txtField.text = relationshipstatusName
                }
                
                if let id = relationshipstatus.value(forKey: "id") as? String
                {
                    self.relationshipSelectedID  = id
                }
                else{
                    self.relationshipSelectedID = "1"
                }
            }
            
            if let smoking = self.usersDetailsDict.value(forKey: "smoking") as? NSDictionary
            {
                if let smokingName = smoking.value(forKey: "name") as? String
                {
                    self.smoking_txtField.text = smokingName
                }
                
                if let id = smoking.value(forKey: "id") as? String
                {
                    self.smokingSelectedID  = id
                }
                else{
                    self.smokingSelectedID = "1"
                }
            }
            
            self.ProfileImageView.contentMode = UIViewContentMode.scaleAspectFill
            self.ProfileImageView.clipsToBounds = true
            
            let profileURL = (self.usersDetailsDict.value(forKey: "profile_pic") as? String)!
            DispatchQueue.main.async {
                self.ProfileImageView.sd_setImage(with: URL(string: profileURL), placeholderImage: UIImage(named: "Default Icon"))
            }
            
            UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "username") as? String, forKey: "UserName")
            UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "age") as? String, forKey: "Age")
            UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "location") as? String, forKey: "Location")
            UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "profile_pic") as? String, forKey: "userProfilePicture")
            UserDefaults.standard.synchronize()
            
        }
    }
  
    
    // MARK: - ****** Generate Thumbnail from video Url. ******
    func createThumbnailOfVideoFromFileURL(_ strVideoURL: String) -> UIImage?{
        let asset = AVAsset(url: URL(string: strVideoURL)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), 100)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            /* error handling here */
            var thumbnailImage = UIImage()
            thumbnailImage = self.createThumbnailOfVideoFromFileURL(self.profileVideoStr)!
            return thumbnailImage
        }
    }
    
    //MARK:- ****** didChangeAuthorization ******
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined , .denied , .restricted :
            
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
            }
            
            break
            
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
     /*   default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
    }

    
    
    // MARK: - ****** Fetch Users Current Location. ******
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.locationManager.location != nil
        {
            if let getCoordinates = manager.location?.coordinate
            {
                print("getCoordinates = \(getCoordinates)")
                
                let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.locationManager.stopUpdatingLocation()
                
                if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                    let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in
                        // this is the last line that is being called
                        var placemark : CLPlacemark!
                        placemark = placemarks?[0]
                        if placemark != nil
                        {
                            if (placemark.addressDictionary?["Country"] != nil)
                            {
                                self.currentLocationCountry = (placemark.addressDictionary?["Country"] as! String)
                            }
                            
                            var city = String()
                            if (placemark.addressDictionary?["City"] != nil)
                            {
                                city = (placemark.addressDictionary?["City"] as! String)
                            }
                            
                            var state = String()
                            if (placemark.addressDictionary?["State"] != nil)
                            {
                                state = (placemark.addressDictionary?["State"] as! String)
                            }
                            
                            self.city = city
                            self.state = state
                            
                            var vv = String()
                            if (placemark.subLocality != nil)
                            {
                                vv =  placemark.subLocality!
                            }
                            
                            var countWhiteSpaces = Int()
                            if (vv.range(of: " ") != nil)
                            {
                                countWhiteSpaces = (vv.components(separatedBy: " ").count) - 1
                            }
                            else
                            {
                                countWhiteSpaces = 0
                            }
                            
                            var objAtIndexBeforeLast = String()
                            var objAtIndexLast = String()
                            
                            if (countWhiteSpaces > 0)
                            {
                                var fullNameArr = vv.components(separatedBy: " ")
                                objAtIndexBeforeLast = (fullNameArr[(fullNameArr.count) - 2])
                                objAtIndexLast = (fullNameArr.last)!
                                
                                self.subLocalityStr = objAtIndexBeforeLast + " " + objAtIndexLast
                            }
                            else
                            {
                                self.subLocalityStr = vv
                            }
                            
                            if (placemark.locality != nil)
                            {
                                self.localityStr = placemark.locality!
                            }
                            
                            self.location_txtField.text = self.subLocalityStr + "," + self.localityStr
                        }
                    }
                }
            }
        }
    }

    
    // MARK: - ****** Fetch Countries and States From Database. ******
    func populateCustomers() {
        let db = FMDBDataAccess()
        self.countryNameArray = db.getCustomers()
        var country = String()
        var countryID = String()
        var countryObj = Country()
        for i in 0..<self.countryNameArray.count {
            countryObj = self.countryNameArray[i] as! Country
            country = "\(countryObj.firstName!)"
            countryID = "\(countryObj.customerId)"
           
            testArray.add(country)
            CountryIDArray.add(countryID)
           
        }
        
        for i in 0..<testArray.count {
            var dict1 = [AnyHashable: Any]()
            dict1["c_name"] = testArray[i]
         
            dict1["c_id"] = CountryIDArray[i]
           
            let country_id: String = "\(CountryIDArray[i])"
            if !nameIDArray.contains(country_id) {
             
                nameIDArray.insert(dict1, at: i)
            }
        }
    }
    
    
    // MARK: - ****** ViewDidLayoutSubViews. ******
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.wrapperScrollView.isScrollEnabled=true
        self.wrapperScrollView.contentSize.height = self.updateBtnView.frame.origin.y + self.updateBtnView.frame.size.height + 100
    }
    
    
    // MARK: - ****** Back Button Action. ******
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    // MARK: - ****** Change Profile Picture Button Action. ******
    @IBAction func changeProfileImage_btnAction(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Select a New Profile Picture", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if #available(iOS 10.0, *) {
                let homeScreen = storyboard.instantiateViewController(withIdentifier: "CameraOverlayViewController") as! CameraOverlay
                homeScreen.parentClass = self
                homeScreen.differentiateStr = "EditProfileViewController"
                homeScreen.differentiateMediaStr = "Edit Profile Picture"
                self.navigationController?.present(homeScreen, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    
    // MARK: - ****** ActionSheet For choosing Photos. ******
    func showActionSheet() {
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.useCameraStr = "Upload a photo using camera"
            
            let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            if status == .denied {
                PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                    let alert = UIAlertController(
                        title: "IMPORTANT",
                        message: "\("WI Match") Would like to capture the Photos",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else if status == .authorized  || status == .notDetermined
            {
                self.camera()
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.useCameraStr = "Upload a photo using gallery"
            
            let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            if  status == .denied {
                PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                    let alert = UIAlertController(
                        title: "IMPORTANT",
                        message: "\("WI Match") Would like to access the Photos",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else if status == .authorized || status == .notDetermined
            {
                self.photoLibrary()
            }
            
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: - ****** Open Photo Library Method. ******
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        myPickerController.allowsEditing = false
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    
    
    // MARK: - ****** ActionSheet For Video uploading. ******
    func showActionSheet_forVideo() {
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.useCameraStr = "Upload a video"
            
            if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                    DispatchQueue.main.async() {
                        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                        switch authStatus {
                        case .authorized: self.camera() // Do your stuff here i.e. callCameraMethod()
                        case .denied: self.alertToEncourageCameraAccessInitially()
                        case .notDetermined: self.alertPromptToAllowCameraAccessViaSetting()
                        default: self.alertToEncourageCameraAccessInitially()
                        }
                    }
                }
            }
            else
            {
                let alert = UIAlertController(title: "", message: "Sorry,Camera is not available to take picture.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }))
        
        
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.useCameraStr = "Upload a video"
            let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
            switch authStatus {
            case .authorized: self.videoLibrary() // Do your stuff here i.e. callCameraMethod()
            case .denied: self.alertToEncourageCameraAccessInitially()
            case .notDetermined: self.alertPromptToAllowCameraAccessViaSetting()
            default: self.alertToEncourageCameraAccessInitially()
            }
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** Open Camera Method. ******
    func camera()
    {
        let microPhoneStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio)
        switch microPhoneStatus {
        case .authorized:
            accessVideoCapture()
            break
        case .notDetermined:
            accessVideoCapture()
            break
        // Has access
        case .denied:
            showAlertForMicrophone()
            break
        case .restricted:
            showAlertForMicrophone()
            break
            
        }
    }
    
    
    // MARK: - ****** Capture Video Method. ******
    func accessVideoCapture()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.camera
            if self.useCameraStr == "Upload a photo using camera"
            {
                myPickerController.mediaTypes = [kUTTypeImage as String]
                myPickerController.allowsEditing = false
            }
            else if self.useCameraStr == "Upload a video"
            {
                myPickerController.mediaTypes = [kUTTypeMovie as String]
                myPickerController.allowsEditing = false
                myPickerController.videoMaximumDuration = 20.0
            }
            
            self.present(myPickerController, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "", message: "Sorry,Camera is not available to take picture.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - ****** Select Video From Library. ******
    func videoLibrary()
    {
        let microPhoneStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio)
        switch microPhoneStatus {
        case .authorized:
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = true
            myPickerController.videoMaximumDuration = 20.0
            myPickerController.mediaTypes = [kUTTypeMovie as String]
            self.present(myPickerController, animated: true, completion: nil)
            break
        case .notDetermined:
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = true
            myPickerController.videoMaximumDuration = 20.0
            myPickerController.mediaTypes = [kUTTypeMovie as String]
            self.present(myPickerController, animated: true, completion: nil)
            break
        // Has access
        case .denied:
            showAlertForMicrophone()
            break
        case .restricted:
            showAlertForMicrophone()
            break
            
        }
    }
    
    // MARK: - ****** Check Microphone Access Method. ******
    func showAlertForMicrophone()
    {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "\("WI Match") would like to access the microphones to record video.",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Access", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    // MARK: - ****** Access of Camera For Capturing Videos. ******
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "\("WI Match") Would like to access the Camera for capturing videos",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                DispatchQueue.main.async() {
                    let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                    switch authStatus {
                    case .authorized: self.videoLibrary() // Do your stuff here i.e. callCameraMethod()
                    case .denied: self.alertToEncourageCameraAccessInitially()
                    case .notDetermined: self.alertPromptToAllowCameraAccessViaSetting()
                    default: self.alertToEncourageCameraAccessInitially()
                    }
                }
            }
        }
    }
    
    
    //MARK:- ****** viewProfileImage ******
    func viewProfileImage()
    {
        if self.profileImageType == "image"
        {
            self.fullImageView.image = self.ProfileImageView.image
            self.fullImageView.contentMode = UIViewContentMode.scaleAspectFit
            self.fullImageView.clipsToBounds = true
            
            self.viewProfileImageView.isHidden=false
            self.fullImageView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.fullImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })
        }
        else
        {
            if let url = URL(string:UserDefaults.standard.value(forKey: "userProfilePicture") as! String){
                self.viewProfileImageView.isHidden = false
                self.player = AVPlayer(url: url)
                self.playerViewController.player=self.player
                self.playerViewController.view.frame = self.fullImageView.frame
                self.viewProfileImageView.addSubview(self.playerViewController.view)
                self.addChildViewController(self.playerViewController)
                self.playerViewController.willMove(toParentViewController: self)
                self.playerViewController.didMove(toParentViewController: self)
                self.player?.play()
                
                NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.playerDidFinishPlaying), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
            }
        }
    }

    
    // MARK: - ****** UIImagePicker Delegate Methods. ******
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if self.useCameraStr == "Upload a photo using camera" || self.useCameraStr == "Upload a photo using gallery"
        {
            let pickedImage : UIImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            self.dismiss(animated: true, completion: { () -> Void in
                // Use view controller
                let controller = ImageCropViewController(image: pickedImage)
                controller?.delegate = self
                controller?.blurredBackground = true
                self.navigationController?.pushViewController(controller!, animated: true)
            })
        }
        else if self.useCameraStr == "Upload a video"
        {
            self.videoURL = info[UIImagePickerControllerMediaURL] as! NSURL;
            self.dismiss(animated: true, completion: { () -> Void in
                
                if let url = info[UIImagePickerControllerMediaURL] as? URL{//URL(string: "http://devstreaming.apple.com/videos/wwdc/2016/102w0bsn0ge83qfv7za/102/hls_vod_mvp.m3u8"){
                    
                    self.profileVideoStr =  url.absoluteString
                    self.ProfileImageView.image = self.createThumbnailOfVideoFromFileURL(url.absoluteString)
                }
            })
        }
        
    }
    
    
    //MARK:- ****** playerDidFinishPlaying ******
    func playerDidFinishPlaying(){
        //now use seek to make current playback time to the specified time in this case (O)
        let duration : Int64 = 0 //(can be Int32 also)
        let preferredTimeScale : Int32 = 1
        let seekTime : CMTime = CMTimeMake(duration, preferredTimeScale)
        self.player?.seek(to: seekTime)
        self.player?.play()
    }
 
    
    //MARK:- ****** didFinishCroppingImage ******
    func imageCropViewControllerSuccess(_ controller: UIViewController, didFinishCroppingImage croppedImage: UIImage)
    {
        self.navigationController!.popViewController(animated: true)
        DispatchQueue.main.async{
            let imageResized:UIImage = self.resizeImage(image: croppedImage, targetSize: CGSize(width: 200, height: 200))
            self.ProfileImageView.image = imageResized
            self.ProfileImageView.contentMode = UIViewContentMode.scaleAspectFit
            self.ProfileImageView.layer.masksToBounds=true
            let newData = UIImageJPEGRepresentation(self.rotateImageFix(image: self.ProfileImageView.image!), 1)
            self.imagebase64Str = (newData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)))!
        }
    }
    
    //MARK:- ****** imageCropViewControllerDidCancel ******
    func imageCropViewControllerDidCancel(_ controller: UIViewController)
    {
        self.navigationController!.popViewController(animated: true)
    }

    
    
    
    
    //MARK:- ****** Resize Image ******
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
   
    
    // MARK: - ****** Method For Image Rotation with Fixed Orientation. ******
    func rotateImageFix(image: UIImage) -> UIImage {
        
        if (image.imageOrientation == UIImageOrientation.up ) {
            return image
        }
        
        UIGraphicsBeginImageContext(image.size)
        
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return copy!
    }
    
    
    
    // MARK: - ****** Select Female Gender Option Button Action. ******
    @IBAction func femaleSelection_btnAction(_ sender: Any) {
        if femaleCheck == false
        {
            femaleImageView.image = UIImage(named: "check gender Icon")
            femaleCheck = true
            gender_type = "female"
            maleImageView.image = UIImage(named: "Uncheck gender Icon")
            
            maleCheck = false
        }
        else
        {
            femaleImageView.image = UIImage(named: "Uncheck gender Icon")
            femaleCheck = false
            gender_type = ""
            maleImageView.image = UIImage(named: "check gender Icon")
            maleCheck = true
        }
    }
    
    
    // MARK: - ****** Select Male Gender Option Button Action. ******
    @IBAction func maleSelection_btnAction(_ sender: Any) {
        if maleCheck == false
        {
            maleImageView.image = UIImage(named: "check gender Icon")
            maleCheck = true
            gender_type = "male"
            femaleImageView.image = UIImage(named: "Uncheck gender Icon")
            femaleCheck = false
        }
        else
        {
            maleImageView.image = UIImage(named: "Uncheck gender Icon")
            maleCheck = false
            gender_type = ""
            femaleImageView.image = UIImage(named: "check gender Icon")
            femaleCheck = true
        }
    }
    
    
    // MARK: - ****** Select Date Of Birth Button Action. ******
    @IBAction func selectDate_pickerView(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateSelectedFromPicker = dateFormatter.string(from: datePicker.date)
    }
    
    
    // MARK: - ****** Close Date Picker View. ******
    @IBAction func cancel_datePickerView(_ sender: Any) {
        self.datePicker_wrapperView.isHidden=true
    }
    
    
    // MARK: - ****** Selected Date Picker View. ******
    @IBAction func done_datePickerView(_ sender: Any) {
        self.dob_txtField.text = dateSelectedFromPicker
        self.datePicker_wrapperView.isHidden=true
        calculateAge()
    }
    
    
    // MARK: - ****** Calculate Age w.r.t D.O.B. ******
    func calculateAge()
    {
        let now = NSDate()
        let calendar : NSCalendar = NSCalendar.current as NSCalendar
        let ageComponents = calendar.components(.year, from: datePicker.date, to: now as Date, options: [])
        usersAge = ageComponents.year!
        
        if usersAge < 18
        {
            let alert = UIAlertController(title: "", message: "Your age must be 18 years or older.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.dob_txtField.text = ""
            self.datePicker_wrapperView.isHidden=false
        }
    }
    
    
    // MARK: -  ****** Select D.O.B Button Action. ******
    @IBAction func selectDob_btnAction(_ sender: Any) {
        self.userName_txtField.resignFirstResponder()
        self.location_txtField.resignFirstResponder()
        self.island_txtField.resignFirstResponder()
        self.bodyType_ttxtField.resignFirstResponder()
        self.hairColor_txtField.resignFirstResponder()
        self.relationship_txtField.resignFirstResponder()
        self.smoking_txtField.resignFirstResponder()
        self.drinking_txtField.resignFirstResponder()
        self.education_txtField.resignFirstResponder()
        self.datePicker_wrapperView.isHidden=false
    }
    
    
    // MARK: -  ****** Close Picker View Button Action. ******
    @IBAction func cancelPickerView(_ sender: Any) {
        self.wrapperPickerView.isHidden = true
    }
    
    // MARK: - ****** Done Country/State Picker View. ******
    @IBAction func donePickerView_btnAction(_ sender: Any) {
        if typeOfStr == "island_txtField"
        {
            self.wrapperPickerView.isHidden = true
            if countrySelectedStr == ""
            {
                self.island_txtField.text = ((nameIDArray[0] as! NSDictionary).value(forKey: "c_name") as? String)
                self.countryApiSelectedID = "1"
            }
            else{
                self.island_txtField.text = countrySelectedStr
            }
        }
        else if typeOfStr == "location_txtField"
        {
             self.wrapperPickerView.isHidden = true
            if citySelectedStr == ""
            {
                self.location_txtField.text = testArray[0]  as? String
            }
            else{
                self.location_txtField.text = citySelectedStr
            }
            
        }
        else if typeOfStr == "bodyType_txtField"
        {
             self.wrapperPickerView.isHidden = true
            if bodyTypeSelectedStr == ""
            {
                self.bodyType_ttxtField.text = bodyTypeArray[0]
            }
            else{
                self.bodyType_ttxtField.text = bodyTypeSelectedStr
            }
            
        }
        else if  typeOfStr == "hairColor_txtField"
        {
             self.wrapperPickerView.isHidden = true
            if hairColorSelectedStr == ""
            {
                self.hairColor_txtField.text = bodyTypeArray[0]
            }
            else{
                self.hairColor_txtField.text = hairColorSelectedStr
            }
            
        }
        else if  typeOfStr == "relationshipStatus_txtField"
        {
             self.wrapperPickerView.isHidden = true
            if relationshipSelectedStr == ""
            {
                self.relationship_txtField.text = bodyTypeArray[0]
            }
            else{
                self.relationship_txtField.text = relationshipSelectedStr
            }
        }
        else if  typeOfStr == "smoking_txtField"
        {
            self.wrapperPickerView.isHidden = true
            if smokingSelectedStr == ""
            {
                self.smoking_txtField.text = bodyTypeArray[0]
            }
            else{
                self.smoking_txtField.text = smokingSelectedStr
            }
        }
        else if typeOfStr == "drinking_txtField"
        {
             self.wrapperPickerView.isHidden = true
            if drinkingSelectedStr == ""
            {
                self.drinking_txtField.text = bodyTypeArray[0]
            }
            else{
                self.drinking_txtField.text = drinkingSelectedStr
            }
        }
        else if  typeOfStr == "education_txtField"
        {
             self.wrapperPickerView.isHidden = true
            if educationSelectedStr == ""
            {
                self.education_txtField.text = bodyTypeArray[0]
            }
            else{
                self.education_txtField.text = educationSelectedStr
            }
        }
        else if typeOfStr == "meetUp_textField"
        {
             self.wrapperPickerView.isHidden = true
            if meetUpStr == ""
            {
                self.meetUp_txtField.text = bodyTypeArray[0]
            }
            else{
                self.meetUp_txtField.text = meetUpStr
            }
            
        }
        else if   typeOfStr == "interestedIn_txtField"
        {
           self.wrapperPickerView.isHidden = true
            if interestedInStr == ""
            {
                self.interestedIn_txtField.text = bodyTypeArray[0]
            }
            else{
                self.interestedIn_txtField.text = interestedInStr
            }
            
        }
    }
    
    
    // MARK: - ****** Update Profile Button Action. ******
    @IBAction func updateProfile_btnAction(_ sender: Any) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.ProfileImageView.image == UIImage(named: "Profile Icon"))
            {
                let alert = UIAlertController(title: "", message: "Please add your profile picture", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.userName_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please enter your username", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if (self.userName_txtField.text?.trimmingCharacters(in: .whitespaces).isEmpty == true){
                let alert = UIAlertController(title: "", message: "Please enter correct username", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
          
            else if self.location_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please enter your location", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.dob_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your date of birth", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if gender_type == ""
            {
                let alert = UIAlertController(title: "", message: "Please choose your gender", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.island_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Island which you are representing", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.relationship_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your Relationship Status", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.meetUp_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your Meet Up Preferences", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.interestedIn_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your Interested In Meeting Up With", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.bodyType_ttxtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Body Type", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.hairColor_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Hair Color", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.smoking_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Smoking", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.drinking_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select Drinking", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.education_txtField.text == ""
            {
                let alert = UIAlertController(title: "", message: "Please select your Education", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else{
                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                    {
                        self.loadingView.isHidden = false
                        self.view.isUserInteractionEnabled = false
                        self.updateProfileBtn.isUserInteractionEnabled = false
                        
                        var deviceID = String()
                        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                            deviceID = uuid
                        }
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        if let refreshedToken = FIRInstanceID.instanceID().token() {
                            appDelegate.myDeviceToken = refreshedToken
                        }
                        
                        var savedPassword = String()
                        if let savedPasswordStr = UserDefaults.standard.value(forKey: "passwordSaved") as? String
                        {
                            savedPassword = savedPasswordStr
                        }
                        else{
                            savedPassword = ""
                        }
                        
                        var genderID = String()
                        if (self.gender_type == "male")
                        {
                            genderID = "1"
                        }
                        else if (self.gender_type == "female")
                        {
                            genderID = "2"
                        }
                        else{
                            genderID = "1"
                        }
                        
                        if (self.userName_txtField.text != "")
                        {
                            var textMessage = String()
                            textMessage = self.userName_txtField.text!
                            if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false)
                            {
                                print("self.userName_txtField.text! is not empty string")
                            }
                            else
                            {
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                let alert = UIAlertController(title: "", message: "Please enter correct username", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                return
                            }
                        }
                        
                        
                        let img = ApiHandler.resizeImage(image: self.ProfileImageView.image!, newWidth: 600)
                        
                        ApiHandler.updateUserProfile(userID: userID, authToken: authToken,userName: self.userName_txtField.text!, emailID: self.emailAddressStr, password: savedPassword, profilePic: img, currentLocation: self.location_txtField.text!, dateOfBirth: self.dob_txtField.text!, gender: genderID, countryRepresenting: self.countryApiSelectedID, relationshipStatus: self.relationshipSelectedID, meetUpPreferences: self.meetUpSelectedID, interestedInMeetingWith: self.interestedInSelectedID, BodyType: self.bodyTypeSelectedID, HairColor: self.hairColorSelectedID, Smoking: self.smokingSelectedID, Drinking: self.drinkingSelectedID, Education: self.educationSelectedID, typeOfUser: self.userType_Str, myDeviceToken: appDelegate.myDeviceToken, currentLatitude: self.currentLatitude, curentLongitue: self.currentLongitude, currentState: self.localityStr, currentCity: self.subLocalityStr, currentCountry: self.currentLocationCountry, deviceID: deviceID, completion: { (responseDict) in
                            
                            if (responseDict.value(forKey: "status") as? String == "200")
                            {
//                                let dataDict = responseDict.value(forKey: "data") as? NSDictionary
                                
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.updateProfileBtn.isUserInteractionEnabled = true
                                
                                let alert = UIAlertController(title: "", message: "Profile updated.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                    action in
                                    
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
                                    
                                    self.navigationController!.popViewController(animated: true)
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            else if (responseDict.value(forKey: "status") as? String == "400")
                            {
//                                let dataDict = responseDict.value(forKey: "data") as? NSDictionary
                                
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.updateProfileBtn.isUserInteractionEnabled = true
                                
                                let message = responseDict.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else if (responseDict.value(forKey: "status") as? String == "400")
                            {
//                                let dataDict = responseDict.value(forKey: "data") as? NSDictionary
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.updateProfileBtn.isUserInteractionEnabled = true
                                
                                let message = responseDict.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else{
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.updateProfileBtn.isUserInteractionEnabled = true
                                
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        })
                    }
                }
            }
        }
    }
    
  
    
    // MARK: - ****** Generate Video naem with date and time. ******
    func getName() -> String {
        let dateFormatter = DateFormatter()
        let dateFormat = "yyyyMMddHHmmss"
        dateFormatter.dateFormat = dateFormat
       // let date = dateFormatter.string(from: Date())
        //        let name = date.appending(".mp4")
        let name = "profileimage.mp4"
        return name
    }

    
    
    // MARK: - ****** UIPickerView Delegate and Datasource Methods. ******
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if typeOfStr == "island_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                return appDelegate.newNameIDArray.count;
            }
            else
            {
                return nameIDArray.count
            }
        }
        else if typeOfStr == "location_txtField"
        {
            return testArray.count;
        }
        else
        {
            if (self.newBodyTypeArray.count > 0)
            {
                return newBodyTypeArray.count;
            }
            else{
                return bodyTypeArray.count;
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var str = String()
        if typeOfStr == "island_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                str = (appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "country_name") as! String
            }
            else{
                str = (nameIDArray[row] as! NSDictionary).value(forKey: "c_name") as! String
            }
        }
        else if typeOfStr == "location_txtField"
        {
            str = testArray[row]  as! String
        }
        else
        {
            if (self.newBodyTypeArray.count > 0)
            {
                str = newBodyTypeArray[row].name
            }
            else{
                str = bodyTypeArray[row]
            }
        }
        return str
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if typeOfStr == "island_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                countrySelectedStr = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "country_name") as? String)!
                
                self.countryApiSelectedID = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "id") as? String)!
                
                if countrySelectedStr == "United States"
                {
                    getCountryID = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "id") as? String)!
                    let db = FMDBDataAccess()
                    self.countryNameArray = db.getStatesForCountrySelected(getCountryID)
                    
                    var state: String = ""
                    var countryy = Country()
                    
                    testArray.removeAllObjects()
                    for i in 0..<countryNameArray.count {
                        countryy = countryNameArray[i] as! Country
                        state = "\(countryy.lastName!)"
                        testArray.add(state)
                    }
                    
                    self.countryApiSelectedID = String(describing: row)
                }
            }
            else
            {
                countrySelectedStr = ((nameIDArray[row] as! NSDictionary).value(forKey: "c_name") as? String)!
                self.countryApiSelectedID = String(describing: row + 1)
                if countrySelectedStr == "United States"
                {
                    getCountryID = ((nameIDArray[row] as! NSDictionary).value(forKey: "c_id") as? String)!
                    let db = FMDBDataAccess()
                    self.countryNameArray = db.getStatesForCountrySelected(getCountryID)
                    
                    var state: String = ""
                    var countryy = Country()
                    
                    testArray.removeAllObjects()
                    for i in 0..<countryNameArray.count {
                        countryy = countryNameArray[i] as! Country
                        state = "\(countryy.lastName!)"
                        testArray.add(state)
                    }
                    self.countryApiSelectedID = String(describing: row)
                }
            }
        }
        else if typeOfStr == "location_txtField"
        {
            citySelectedStr = testArray[row]  as! String
        }
        else if typeOfStr == "bodyType_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                bodyTypeSelectedStr = newBodyTypeArray[row].name
                self.bodyTypeSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                bodyTypeSelectedStr = bodyTypeArray[row]
                self.bodyTypeSelectedID = String(describing: row + 1)
            }
        }
        else if  typeOfStr == "hairColor_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                hairColorSelectedStr = newBodyTypeArray[row].name
                self.hairColorSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                hairColorSelectedStr = bodyTypeArray[row]
                self.hairColorSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "relationshipStatus_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                relationshipSelectedStr = newBodyTypeArray[row].name
                self.relationshipSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                relationshipSelectedStr = bodyTypeArray[row]
                self.relationshipSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "smoking_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                smokingSelectedStr = newBodyTypeArray[row].name
                self.smokingSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                smokingSelectedStr = bodyTypeArray[row]
                self.smokingSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "drinking_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                drinkingSelectedStr = newBodyTypeArray[row].name
                self.drinkingSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                drinkingSelectedStr = bodyTypeArray[row]
                self.drinkingSelectedID = String(describing: row + 1)
            }
            
        }
        else if typeOfStr == "education_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                educationSelectedStr = newBodyTypeArray[row].name
                self.educationSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                educationSelectedStr = bodyTypeArray[row]
                self.educationSelectedID = String(describing: row + 1)
            }
            
        }
        else if typeOfStr == "meetUp_textField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                meetUpStr = newBodyTypeArray[row].name
                self.meetUpSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                meetUpStr = bodyTypeArray[row]
                self.meetUpSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "interestedIn_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                interestedInStr = newBodyTypeArray[row].name
                self.interestedInSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                interestedInStr = bodyTypeArray[row]
                self.interestedInSelectedID = String(describing: row + 1)
            }
        }
    }

    
    
    //MARK:- ****** UITEXTFIELD DELEGATE ******
   
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.island_txtField
        {
            self.userName_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            self.dob_txtField.resignFirstResponder()
            
            typeOfStr = "island_txtField"
            self.wrapperPickerView.isHidden = false
            self.countrPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.location_txtField
        {
            typeOfStr = "location_txtField"
            
            self.dob_txtField.resignFirstResponder()
            self.userName_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            if countrySelectedStr == "United States"
            {
                self.wrapperPickerView.isHidden = false
                self.countrPickerView.reloadAllComponents()
                //use this line for going at the top of the label index 0:
                self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
                return false
            }
        }
        else if textField == self.bodyType_ttxtField
        {
            self.dob_txtField.resignFirstResponder()
            self.userName_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            typeOfStr = "bodyType_txtField"
            
            self.newBodyTypeArray.removeAll()
            
            if let body_type_Array =  appDelegate.getAllDefaultValuesDict.value(forKey: "body_type") as? NSArray
            {
                for index in 0..<body_type_Array.count
                {
                    let dict = body_type_Array[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }

            bodyTypeArray = ["Slimz","Average","Big Boned","Athletic (Ah Have ah Lil Muscle)","Fluffy (More meat than rice)","Petite (Smallie)","Curvy (In All the right places)","Ask meh"]
            
            self.wrapperPickerView.isHidden = false
            self.countrPickerView.reloadAllComponents()
            self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
            
            return false
        }
        else if textField == self.hairColor_txtField
        {
            self.island_txtField.resignFirstResponder()
            self.dob_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            typeOfStr = "hairColor_txtField"
            
            self.newBodyTypeArray.removeAll()
            
            if let haircolor =  appDelegate.getAllDefaultValuesDict.value(forKey: "haircolor") as? NSArray
            {
                for index in 0..<haircolor.count
                {
                    let dict = haircolor[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Red","Black","Dark Brown","Weave (Any color I choose for it to be)","Bald","Light Brown","Ask meh"]
            self.wrapperPickerView.isHidden = false
            self.countrPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
            
            return false
        }
        else if textField == self.relationship_txtField
        {
            self.dob_txtField.resignFirstResponder()
            self.userName_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            typeOfStr = "relationshipStatus_txtField"
            
            self.newBodyTypeArray.removeAll()
            
            if let relationshipstatus =  appDelegate.getAllDefaultValuesDict.value(forKey: "relationshipstatus") as? NSArray
            {
                for index in 0..<relationshipstatus.count
                {
                    let dict = relationshipstatus[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Single (Like ah dolla)","Married (Happily)","In a relationship","Open relationship","Not looking"]
            self.wrapperPickerView.isHidden = false
            self.countrPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.smoking_txtField
        {
            self.dob_txtField.resignFirstResponder()
            self.userName_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            typeOfStr = "smoking_txtField"
            self.newBodyTypeArray.removeAll()
            
            if let smoking = appDelegate.getAllDefaultValuesDict.value(forKey: "smoking") as? NSArray
            {
                for index in 0..<smoking.count
                {
                    let dict = smoking[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
           
            bodyTypeArray = ["Always","Sometimes","Never","Trying to quit","Ask meh"]
            self.wrapperPickerView.isHidden = false
            self.countrPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.drinking_txtField
        {
            self.dob_txtField.resignFirstResponder()
            self.userName_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            typeOfStr = "drinking_txtField"
            
            self.newBodyTypeArray.removeAll()
            
            if let drinking =  appDelegate.getAllDefaultValuesDict.value(forKey: "drinking") as? NSArray
            {
                for index in 0..<drinking.count
                {
                    let dict = drinking[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Not a drinker","Social Drinker","Frequent drinker","Communion wine (if that counts)","Ask meh"]
            self.wrapperPickerView.isHidden = false
            self.countrPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.education_txtField
        {
            self.dob_txtField.resignFirstResponder()
            self.userName_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            typeOfStr = "education_txtField"
            
            self.newBodyTypeArray.removeAll()
            
            if let education =  appDelegate.getAllDefaultValuesDict.value(forKey: "education") as? NSArray
            {
                for index in 0..<education.count
                {
                    let dict = education[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["High school (Yes I went to school)","Some College ( I wanted to be smart)","Associates degree (Realize a lil bright)","Bachelors Degree ( Wanted a bess wuk)","Masters ( I rel bright inno)","PhD ( Yah dun kno)","Trade (I'm good with my hands)","Ask meh"]
            self.wrapperPickerView.isHidden = false
            self.countrPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.meetUp_txtField
        {
            self.dob_txtField.resignFirstResponder()
            self.userName_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            typeOfStr = "meetUp_textField"
            
            self.newBodyTypeArray.removeAll()
            
            if let meetuppreferences =  appDelegate.getAllDefaultValuesDict.value(forKey: "meetuppreferences") as? NSArray
            {
                for index in 0..<meetuppreferences.count
                {
                    let dict = meetuppreferences[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Looking For A Potential Relationship","Not Looking For A Relationship","Maybe Dinner & Conversation","Meet Up For Drinks","Meet Up For A Fete","Meet Up To Make A New Friendship"]
            self.wrapperPickerView.isHidden = false
            self.countrPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        else if textField == self.interestedIn_txtField
        {
            self.dob_txtField.resignFirstResponder()
            self.userName_txtField.resignFirstResponder()
            self.location_txtField.resignFirstResponder()
            self.island_txtField.resignFirstResponder()
            self.bodyType_ttxtField.resignFirstResponder()
            self.hairColor_txtField.resignFirstResponder()
            self.relationship_txtField.resignFirstResponder()
            self.smoking_txtField.resignFirstResponder()
            self.drinking_txtField.resignFirstResponder()
            self.education_txtField.resignFirstResponder()
            self.meetUp_txtField.resignFirstResponder()
            self.interestedIn_txtField.resignFirstResponder()
            
            typeOfStr = "interestedIn_txtField"
            
            self.newBodyTypeArray.removeAll()
            
            if let interestesin =  appDelegate.getAllDefaultValuesDict.value(forKey: "interestesin") as? NSArray
            {
                for index in 0..<interestesin.count
                {
                    let dict = interestesin[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Women","Men","Both","Interested in just socializing"]
            self.wrapperPickerView.isHidden = false
            self.countrPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.countrPickerView.selectRow(0, inComponent: 0, animated: false)
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
}
