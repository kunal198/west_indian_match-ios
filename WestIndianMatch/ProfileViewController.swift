//
//  ProfileViewController.swift
//  WestIndianMatch
//
//  Created by brst on 06/03/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Firebase
import  MediaPlayer
import MobileCoreServices
import SDWebImage
import Photos
import FLAnimatedImage


class compareTimestampForMyPosts
{
    var name: String
    var location: String
    var age: String
    var profilePic: String
    var timestamp:Int64
    var userIDStr: String
    var postedImage: String
    var gender: String
    var reportCount: String
    var likesCount: Int
    var replyCount: String
    var timeFrame: String
    var statusAdded: String
    var postIDStr: String
    var hexColor: String
    var moodTypeStr: String
    var photoAlbumTypeStr: String
    var likedUsersArray: NSMutableDictionary
    var width: Int
    var height: Int
    var removePostStatus: String
    
    init(name: String, location: String, age: String, pic: String,timestamp: Int64,u_id: String, postedImage: String,gender: String, reportCount: String, likesCount: Int, replyCount: String, timeFrame: String, status: String,postID: String, selectedHexColor: String, photoType: String, moodType: String, likedUsersArray: NSMutableDictionary,width: Int,height: Int, removePostStr: String)
    {
        self.name = name
        self.location = location
        self.age = age
        self.profilePic = pic
        self.timestamp = timestamp
        self.userIDStr = u_id
        self.postedImage = postedImage
        self.gender = gender
        self.reportCount = reportCount
        self.likesCount = likesCount
        self.replyCount = replyCount
        self.timeFrame = timeFrame
        self.statusAdded = status
        self.postIDStr = postID
        self.hexColor = selectedHexColor
        self.moodTypeStr = moodType
        self.photoAlbumTypeStr = photoType
        self.likedUsersArray = likedUsersArray
        self.width = width
        self.height = height
        self.removePostStatus = removePostStr
    }
}

class ConfessionDetailsForPosts
{
    var location: String
    var age: String
    var createdAt:String
    var userIDStr: String
    var postedImage: String
    var gender: String
    var likesCount: String
    var replyCount: String
    var totalTime: String
    var statusAdded: String
    var postIDStr: String
    var moodTypeStr: String
    var photoAlbumTypeStr: String
    var width: Int
    var height: Int
    var postHours: String
    var likedByMeStatus: String
    var name: String
    var profilePic: String
    var typeOfConfession: String
    
    init(location: String, age: String, createdAt: String, userID: String, postedImage: String, gender: String, likesCount: String, replyCount: String, totalTime: String, Status: String,postID: String, moodType: String, photoTypeAlbum: String, width: Int, height: Int, postHoursStr: String, likedByMeStr: String, username: String, profileStr: String,typeOfConfession: String)
    {
        self.location = location
        self.age = age
        self.createdAt = createdAt
        self.userIDStr = userID
        self.postedImage = postedImage
        self.gender = gender
        self.likesCount = likesCount
        self.replyCount = replyCount
        self.totalTime = totalTime
        self.statusAdded = Status
        self.postIDStr = postID
        self.moodTypeStr = moodType
        self.photoAlbumTypeStr = photoTypeAlbum
        self.width = width
        self.height = height
        self.postHours = postHoursStr
        self.likedByMeStatus = likedByMeStr
        self.name = username
        self.profilePic = profileStr
        self.typeOfConfession = typeOfConfession
    }
}

//Milliseconds to date
extension Int {
    func dateFromMilliseconds() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(self)/1000)
    }
}


class ProfileViewController: UIViewController,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UITextViewDelegate ,UIPickerViewDelegate,UIPickerViewDataSource,UICollectionViewDelegateFlowLayout,ImageCropViewControllerDelegate,updatePopInfo, UIGestureRecognizerDelegate {
    
    // MARK: - ****** Variables ******
    
    var getReloadIndexPath = Int()
    var indexPathSelected = IndexPath()
    var likedUserDict = NSMutableDictionary()
    var MyDetailsArray = [ApiHandler]()
    private var lastContentOffset: CGFloat = 0
    
    var isClickedPostsbtn = Bool()
    
    var isScrollView = Bool()
    
    
    
    var repliesBool = Bool()

    var postLikedStr = Bool()
    
    var typeOftextView = String()
    
    var userIDToReportStr = String()
    var getReportTotalCount = String()
    var ownerIdForConfession = String()
    let playerViewController = AVPlayerViewController()
    var player:AVPlayer?
    var profileImageType = String()
    var viewAlbumCount = Int()
    var albumArray = NSMutableArray()
    var listingArray = NSArray()
    var appDelegatePush = String()
    var typeOfPostStr = String()
    var myPostsArray = [ConfessionDetailsForPosts]()
    var photosUploadedArray = NSDictionary()
    var profileImagesArray = [String]()
    var interestsArray = [String]()
    var usersDetailsDict = NSDictionary()
    var photoArray = [String]()
    var imagebase64Str = String()
    var deleteInterestIndex = Int()
    var useCameraStr = String()
    var videoURL = NSURL()
    var videoController =  MPMoviePlayerController()
    var likesCountArray = [String]()
    var superLikesCountArray = [String]()
    var videoArray = [String]()
    var videoUploadbool = Bool()
    var timerForSlider = Timer()
    var thumbNailArray = [UIImage]()
    var captionVideoArray = [String]()
    var dict1 = NSDictionary()
    var selectImageBool = Bool()
    var getPhotoKeyArray = [String]()
    var photoDeleteIndex = String()
    var mediaCaptionText = String()
    var mediaCaptionType = String()
    var newImageView = UIImageView()
    var viewAlbumPhotoBtn = UIButton()
    var interestVal = Int()
    var isLoadMore = Bool()
    var showInterestsArray = [String]()
    var getVideoURLArray = [String]()
    var deleteVideoURL = String()
    var totalInterestsSaved = NSMutableArray()
    var femaleCheck = Bool()
    var maleCheck = Bool()
    var gender_type = String()
    var typeOfStr = String()
    var bodyTypeSelectedStr = String()
    var hairColorSelectedStr = String()
    var relationshipSelectedStr = String()
    var smokingSelectedStr = String()
    var drinkingSelectedStr = String()
    var educationSelectedStr = String()
    var bodyTypeArray = [String]()
    var getURLPhotoStr = String()
    var meetUpStr = String()
    var interestedInStr = String()
    var timestampArray = [String]()
    var timestampVideoArray = [String]()
    var getVideoURLStr = String()
    var lastPhotoUrl = String()
    var lastVideoUrl = UIImage()
    var videoUrlAtIndex = String()
    var countryNameArray = NSArray()
    var CountryIDArray = NSMutableArray()
    var testArray = NSMutableArray()
    var nameIDArray = NSMutableArray()
    var countrySelectedStr = String()
    var checkSpotlight = String()
    var getUpdatedInterestsArray = NSMutableArray()
    var userID = String()
    var deletePostID = String()
    var deletePostImageStr = String()
    var allRepliesArray = NSMutableArray()
    var postLikedByArray = NSMutableArray()
    var postID = String()
    var getPostBool = Bool()
    var picturesLikedByArray = NSMutableArray()
    var picturesSuperLikedByArray = NSMutableArray()
    var videosLikedByArray = NSMutableArray()
    var videosSuperLikedByArray = NSMutableArray()
    var videoLikedBool = Bool()
    
    
    var newBodyTypeArray = [values]()
    var getCountryID = String()
    var countrySelectedID = String()
    var relationshipSelectedID = String()
    var meetUpSelectedID = String()
    var interestedInSelectedID = String()
    var bodyTypeSelectedID = String()
    var hairColorSelectedID = String()
    var smokingSelectedID = String()
    var drinkingSelectedID = String()
    var educationSelectedID = String()
    var countryApiSelectedID = String()
    
    var typeOfAlbumsPush = String()
    var checkReplyTypeReply = String()
    var postIDOwnerReply = String()
    var replyIDReply = String()
    
    //var postLikedByArray = NSMutableArray()
    var likePostBool = Bool()
    var getLikesCountStr = String()
    var indexToClick = Int()
    var checkReplyType = String()
    var differentiateTypereplyStr = String()
    var totalCountArray = NSMutableArray()
    
    var cameFromHome = String()
    var viewAlbumBool = Bool()
    
    var getFullDataStr = String()
    var latestIntArr = NSMutableArray()
    
    var mediaType = String()
    var media_uploadedFrom = String()
    
    var tappedBtnStr = String()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // MARK: - ****** Outlets ******
    @IBOutlet weak var postsBtnTop: UIButton!
    @IBOutlet weak var albumBtnTop: UIButton!
    @IBOutlet weak var aboutMeTop: UIButton!
    @IBOutlet weak var topViewButtons: UIView!
    @IBOutlet weak var wrapperViewBtns: UIView!
    @IBOutlet weak var fullImageViewBtn: UIButton!
    @IBOutlet weak var editPhotoUploadedLbl: UILabel!
    @IBOutlet weak var editPhotoUploadedBtn: UIButton!
    @IBOutlet weak var uploadVideoBtn: UIButton!
    @IBOutlet weak var wrapperScrollToUploadVideo: UIScrollView!
    @IBOutlet weak var uploadPhotoBtn: UIButton!
    @IBOutlet weak var wrapperScrollToUploadPhoto: UIScrollView!
    @IBOutlet weak var crossViewVideoAlbum: UIButton!
    @IBOutlet weak var wrapperScrollForVideoAlbum: UIScrollView!
    @IBOutlet weak var crossViewPhotoAlbum: UIButton!
    @IBOutlet weak var wrapperScrollForPhotoAlbum: UIScrollView!
    @IBOutlet weak var imagesButtonWrapperView: UIView!
    @IBOutlet var videoLikeImage: UIImageView!
    @IBOutlet weak var postGifWrapperView: UIView!
    @IBOutlet var likeAlbumImage: UIImageView!
    @IBOutlet weak var confessionReplyLbl: UILabel!
    @IBOutlet weak var confessionLikeLbl: UILabel!
    @IBOutlet weak var fullSizeAnimatedImageConfession: FLAnimatedImageView!
    @IBOutlet weak var fullSizeImageConfession: UIImageView!
    @IBOutlet weak var viewFullSizePostView: UIView!
    @IBOutlet weak var myPosts_tableView: UITableView!
    @IBOutlet weak var aboutMe_placeholderLbl: UILabel!
    @IBOutlet weak var currentPostedImageView: UIImageView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var about_wrapperScrollView: UIScrollView!
    @IBOutlet weak var wrapperScrollView: UIScrollView!
    @IBOutlet weak var videoSuperLikeCount_lbl: UILabel!
    @IBOutlet weak var videoLikeCount_lbl: UILabel!
    @IBOutlet weak var imageSuperLikeCount_Lbl: UILabel!
    @IBOutlet weak var imageLikeCount_lbl: UILabel!
    @IBOutlet weak var aboutMe_txtView: UITextView!
    @IBOutlet weak var representCountry_lbl: UILabel!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var albumBtn: UIButton!
    @IBOutlet weak var displayTime_lbl: UILabel!
    @IBOutlet weak var displayVideoTime_lbl: UILabel!
    @IBOutlet weak var editPhotoCaption_lbl: UILabel!
    @IBOutlet weak var meetup_lbl: UILabel!
    @IBOutlet weak var editVideoCaption_lbl: UILabel!
    @IBOutlet weak var interestedIn_lbl: UILabel!
    @IBOutlet weak var edit_btnVideo: UIButton!
    @IBOutlet weak var edit_lblVideo: UILabel!
    @IBOutlet weak var myGender_lbl: UILabel!
    @IBOutlet weak var questionsPickerView: UIPickerView!
    @IBOutlet weak var wrapperQuestionsView: UIView!
    @IBOutlet weak var education_lbl: UILabel!
    @IBOutlet weak var drinking_lbl: UILabel!
    @IBOutlet weak var smoking_lbl: UILabel!
    @IBOutlet weak var relationship_lbl: UILabel!
    @IBOutlet weak var hairColor_lbl: UILabel!
    @IBOutlet weak var bodyType_lbl: UILabel!
    @IBOutlet weak var displayLikeView: UIView!
    @IBOutlet weak var displayVideoCaption_txtView: UITextView!
    @IBOutlet weak var videoCaption_lbl: UILabel!
    @IBOutlet weak var videoCaption_txtView: UITextView!
    @IBOutlet weak var interestsTxtView: UITextView!
    @IBOutlet weak var loadMoreInterestsBtn: UIButton!
    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var playVideoView: UIView!
    @IBOutlet weak var buttonsScrollView: UIScrollView!
    @IBOutlet weak var postsBtn: UIButton!
    @IBOutlet weak var displaycaption_textView: UITextView!
    @IBOutlet weak var addCaptionLbl: UILabel!
    @IBOutlet weak var addcaption_txtView: UITextView!
    @IBOutlet weak var noDataFoundLbl: UILabel!
    @IBOutlet weak var fullImage: UIImageView!
    @IBOutlet weak var viewFullImage_view: UIView!
    @IBOutlet weak var ImagesActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var uploadVideoView: UIView!
    @IBOutlet weak var addPhotoImageView: UIImageView!
    @IBOutlet weak var photoUploadView: UIView!
    @IBOutlet weak var videoBtn: UIButton!
    @IBOutlet weak var photoBtn: UIButton!
    @IBOutlet weak var aboutBtn: UIButton!
    @IBOutlet weak var videosView: UIView!
    @IBOutlet weak var videoTableView: UITableView!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var usersLocation_lbl: UILabel!
    @IBOutlet weak var usersName_lbl: UILabel!
    @IBOutlet weak var usersProfileImage: UIImageView!
    @IBOutlet weak var userImagesView: UIImageView!
    @IBOutlet weak var imagesScrollView: UIScrollView!
    @IBOutlet weak var userName_lbl: UILabel!
    @IBOutlet weak var aboutPhotosVideosView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var repliesCollectionView: UICollectionView!
    @IBOutlet weak var postedStatus_lbl: UILabel!
    @IBOutlet weak var noReplies_lbl: UILabel!
    @IBOutlet weak var postReplied_Lbl: UILabel!
    @IBOutlet weak var postLiked_lbl: UILabel!
    @IBOutlet weak var timeFrame_lbl: UILabel!
    @IBOutlet weak var postedImage: UIImageView!
    @IBOutlet weak var viewParticularPostView: UIView!
    @IBOutlet weak var noPostsView: UIView!
    @IBOutlet weak var myPosts_collectionView: UICollectionView!
    @IBOutlet weak var myPostsView: UIView!
    @IBOutlet weak var confessionLikeIcon: UIImageView!
    @IBOutlet weak var loadingImage: UIImageView!
    @IBOutlet weak var albumsScrollView: UIScrollView!
    @IBOutlet weak var profileWrapperView: UIView!
    @IBOutlet weak var postPostStatus_lbl: UILabel!
    
    // MARK: - ****** ViewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print("appDelegate.newNameIDArray = \(appDelegate.newNameIDArray)")
//        print("appDelegate.getAllDefaultValuesDict = \(appDelegate.getAllDefaultValuesDict)")
    
        self.isClickedPostsbtn = false
        self.viewAlbumBool = false
        self.postLikedStr = false
        self.cameFromHome = "cameFromHome"
        self.myPosts_tableView.rowHeight = UITableViewAutomaticDimension
        self.myPosts_tableView.estimatedRowHeight = 109.0
        
       self.isScrollView = false
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
           
            self.loadingView.isHidden = false
            self.view.isUserInteractionEnabled = false
            
            self.viewUserDetails(completion: { (response) in
                  DispatchQueue.global(qos: .background).async {
                    self.getUploadedMediaFiles()
                }
            })
       
            if appDelegatePush == "fromHomePage"
            {
                if self.typeOfAlbumsPush == "Pictures"
                {
                    if self.postID != ""
                    {
                        self.photoDeleteIndex = postID
                        self.getSingleMediaDetailsFromDatabase(postIDStr: self.photoDeleteIndex, completion: { (responseData) in
                        })
                    }
                }
                else if self.typeOfAlbumsPush == "Videos"
                {
                    self.deleteVideoURL = postID
                    self.getSingleMediaDetailsFromDatabase(postIDStr: self.deleteVideoURL, completion: { (responseData) in
                        
                    })
                }
                else if (self.typeOfAlbumsPush == "ConfessionLikes") || (self.typeOfAlbumsPush == "Comments")
                {
                    self.getAllMyConfessions(paginationStr: "1") { (responseBool) in
                        if (responseBool == true)
                        {
                            if (self.myPostsArray.count > 0)
                            {
                                DispatchQueue.main.async {
                                    self.noPostsView.isHidden = true
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.myPosts_tableView.reloadData()
                                }
                            }
                            else
                            {
                                self.myPostsArray = []
                                self.loadingView.isHidden = true
                                self.noDataFoundLbl.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.myPosts_collectionView.isHidden = true
                                self.noPostsView.isHidden = false
                                
                                self.noPostsView.frame.size.height = 60
                                
                                self.myPostsView.frame = CGRect(x: self.myPostsView.frame.origin.x , y: self.myPostsView.frame.origin.y, width: self.myPostsView.frame.size.width, height: self.about_wrapperScrollView.contentSize.height)
                                
                                self.myPosts_tableView.frame = CGRect(x: self.myPosts_tableView.frame.origin.x , y:0, width: self.myPosts_tableView.frame.size.width, height: self.myPostsView.frame.size.height)
                                
                                self.wrapperScrollView.isScrollEnabled = true
                        
                                self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                                
                                self.myPostsView.isHidden = false
                                self.myPosts_tableView.isHidden = true
                                self.noPostsView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                    self.noPostsView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                }, completion: { (finished) in
                                    
                                })
                            }
                        }
                        else
                        {
                            self.myPostsArray = []
                            self.loadingView.isHidden = true
                            self.noDataFoundLbl.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.myPosts_collectionView.isHidden = true
                            self.noPostsView.isHidden = false
                            
                            self.noPostsView.frame.size.height = 60
                            
                            self.myPostsView.frame = CGRect(x: self.myPostsView.frame.origin.x , y: self.myPostsView.frame.origin.y, width: self.myPostsView.frame.size.width, height: self.about_wrapperScrollView.contentSize.height)
                            
                            self.myPosts_tableView.frame = CGRect(x: self.myPosts_tableView.frame.origin.x , y:0, width: self.myPosts_tableView.frame.size.width, height: self.myPostsView.frame.size.height)
                            
                            self.wrapperScrollView.isScrollEnabled = true
                            self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                            
                            self.myPostsView.isHidden = false
                            self.myPosts_tableView.isHidden = true
                            self.noPostsView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                self.noPostsView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                            }, completion: { (finished) in
                                
                            })
                        }
                    }
                }
            }
        }
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.updateUserProfile), name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.notifyOnUpdateInterests), name: NSNotification.Name(rawValue: "notifyOnUpdateInterests"), object: nil)
        
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        self.videoLikedBool = false
        self.getPostBool = false

        
        var panRecognizer: UIPanGestureRecognizer?
        panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.wasDraggedBtn))
        // cancel touches so that touchUpInside touches are ignored
        panRecognizer?.cancelsTouchesInView = true
        self.chatBtn.addGestureRecognizer(panRecognizer!)
        
        self.chatBtn.addTarget(self, action:#selector(Chat_tbnAction(_:with:)), for:.touchUpInside)

        interestVal = 0
        videoUploadbool = false
        isLoadMore = false
        selectImageBool = false
        useCameraStr = ""
        
        if self.appDelegatePush == "fromHomePage"
        {
            if self.typeOfAlbumsPush == "Pictures"
            {
                self.about_wrapperScrollView.isHidden = true
                self.photosCollectionView.isHidden = false
                self.videosView.isHidden = true
                self.myPostsView.isHidden = true
                self.myPosts_collectionView.isHidden = true
                self.noDataFoundLbl.isHidden = true
                
                self.tappedBtnStr = "albumBtn"
                
                self.albumBtn.setTitleColor(UIColor.black, for: .normal)
                self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
                
                self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
                self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
            }
            else if self.typeOfAlbumsPush == "Videos"
            {
                self.about_wrapperScrollView.isHidden = true
                self.photosCollectionView.isHidden = false
                self.videosView.isHidden = true
                self.myPostsView.isHidden = true
                self.myPosts_collectionView.isHidden = true
                self.noDataFoundLbl.isHidden = true
                
                self.tappedBtnStr = "albumBtn"
                
                self.albumBtn.setTitleColor(UIColor.black, for: .normal)
                self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
                
                self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
                self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
            }
            else if (self.typeOfAlbumsPush == "ConfessionLikes") || (self.typeOfAlbumsPush == "Comments")
            {
                self.getFullDataStr = "fetchAllData"
                self.postLikedStr = false
                
                self.wrapperScrollView.isScrollEnabled = true
                self.wrapperScrollView.contentSize.height = self.myPostsView.frame.origin.y + self.myPosts_tableView.contentSize.height
                
                self.cameFromHome = "postsClicked"
                self.myPosts_collectionView.isHidden = true
                self.about_wrapperScrollView.isHidden = true
                self.myPostsView.isHidden = false
                self.isClickedPostsbtn = false
                self.videosView.isHidden = true
                self.photosCollectionView.isHidden = true
                
                self.tappedBtnStr = "postsBtn"
                
                self.postsBtn.setTitleColor(UIColor.black, for: .normal)
                self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.albumBtn.setTitleColor(UIColor.gray, for: .normal)
                
                self.postsBtnTop.setTitleColor(UIColor.black, for: .normal)
                self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
                
                self.deleteVideoURL = postID

                
                if (self.typeOfPostStr == "photo") || (self.typeOfPostStr == "gif")
                {
                    self.viewFullSizePostView.backgroundColor = UIColor.clear
                    self.viewFullSizePostView.isHidden = false
                    self.fullSizeImageConfession.backgroundColor = UIColor.clear
                    
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
               
                }
                else
                {
                    self.viewFullSizePostView.isHidden = true
                    
                    if (self.typeOfAlbumsPush == "Comments")
                    {
                        self.view.isUserInteractionEnabled = false
                        self.loadingView.isHidden = false
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            let openParticularPost = self.storyboard?.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
                            
                            if self.checkReplyTypeReply == "replyOnReply"
                            {
                                /*openParticularPost.postIDStr = self.postIDReply
                                 openParticularPost.replyIDStr = self.replyIDReply
                                 openParticularPost.userIDReplyOwner = self.userIDOwnerReply
                                 openParticularPost.postIDOwner = self.postIDOwnerReply
                                 openParticularPost.checkReplyType = self.checkReplyTypeReply
                                 openParticularPost.differentiateTypereplyStr = self.differentiateTypeReply
                                 openParticularPost.typeOfPostStr = self.typeOfPostReply
                                 openParticularPost.typeOfAlbumsPush = self.typeOfAlbumReply
                                 openParticularPost.postIDOpenPost = self.postIDOpenPostReply
                                 openParticularPost.replyONReplyIDStr = self.replyOnReplyId*/
                            }
                            else
                            {
                                openParticularPost.checkReplyType = "replyOnPost"
                                openParticularPost.postIDStr = self.postID
                                openParticularPost.replyIDStr = self.replyIDReply
                                openParticularPost.postIDOwner = self.postIDOwnerReply
                                openParticularPost.checkReplyType = self.checkReplyTypeReply
                                openParticularPost.typeOfPostStr = self.typeOfPostStr
                                openParticularPost.typeOfAlbumsPush = self.typeOfAlbumsPush
                                openParticularPost.postIDOpenPost = self.replyIDReply
                            }
                        
                            openParticularPost.appDelegatePush = "fromHomePage"
                        self.navigationController?.pushViewController(openParticularPost, animated: false)
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                    }
                }
            }
        }
        else
        {
            self.myPostsView.isHidden = true
            self.about_wrapperScrollView.isHidden = false
            self.interestsTxtView.isHidden = false
            self.videosView.isHidden = true
            self.noDataFoundLbl.isHidden = true
            self.photosCollectionView.isHidden = true
            self.myPosts_collectionView.isHidden = true
            
            self.tappedBtnStr = "aboutBtn"
            
            self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
            self.aboutBtn.setTitleColor(UIColor.black, for: .normal)
            self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
            self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
            
            self.aboutMeTop.setTitleColor(UIColor.black, for: .normal)
            self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
            self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
        }
        
        self.wrapperScrollView.isScrollEnabled = true
        self.about_wrapperScrollView.isScrollEnabled = false
        self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
        self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height + 50

        chatBtn.layer.cornerRadius = chatBtn.frame.size.height/2
        chatBtn.layer.shadowColor = UIColor.black.cgColor
        chatBtn.layer.shadowOpacity = 0.8;
        chatBtn.layer.shadowRadius = 12;
        chatBtn.layer.shadowOffset = CGSize.init(width: 4.0, height: 4.0);
        
        self.editPhotoCaption_lbl.isHidden = true
        self.editVideoCaption_lbl.isHidden = true
        populateCustomers()
        self.setDoneOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(mediaCapturedforProfileAlbums), name: NSNotification.Name(rawValue: "mediaCapturedforProfileAlbums"), object: nil)
    }
    
    func notifyOnUpdateInterests()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.viewUserDetails(completion: { (response) in
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
            })
        }
    }
    
    func updateUserProfile()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            self.loadingView.isHidden = false
            self.view.isUserInteractionEnabled = false
            self.viewUserDetails(completion: { (response) in
                DispatchQueue.global(qos: .background).async {
                    self.getUploadedMediaFiles()
                }
            })
        }
    }
    
    func pinchGestureDetected(_ recognizer: UIPinchGestureRecognizer?) {
        let state: UIGestureRecognizerState? = recognizer?.state
        if state == .began || state == .changed {
            let scale: CGFloat? = recognizer?.scale
            recognizer?.view?.transform = (recognizer?.view?.transform.scaledBy(x: scale!, y: scale!))!
            recognizer?.scale = 1.0
        }
        else if state == .ended
        {
            print("gestureRecognizerShouldEnded")
        }
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        print("gestureRecognizerShouldBegin")
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    func viewUserDetails(completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                let date = NSDate();
                /// GET TIMESTAMP IN UNIX
                let currentTimeStamp = Int64(date.timeIntervalSince1970)

                ApiHandler.getUserProfileDetails(forUserID: userID, authToken: userToken,other_user_id: userID,timestamp: Int(currentTimeStamp), completion: { (responseData) in
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        
                        if let usersFirebaseID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                        {
                            let ref = FIRDatabase.database().reference()
                            var userDetails = [String: Any]()
                            userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                            userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                            userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                            userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                            userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                            userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                            
                            if let refreshedToken = FIRInstanceID.instanceID().token()
                            {
                                userDetails["myDeviceToken"] = refreshedToken
                            }
                            
                            ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                            
                            
                            var dataDictForGeneralCategory = [String : Any]()
                            let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                            if (profile_visibility == "on")
                            {
                                dataDictForGeneralCategory["Profile_Visibility"] = "On"
                            }
                            else
                            {
                                dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                            }
                            ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                            
                            var settingsDict = [String : Any]()
                            let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                            if (chatRoomValue == "on")
                            {
                                settingsDict["Wi_Chat_Room"] = "On"
                            }
                            else{
                                settingsDict["Wi_Chat_Room"] = "Off"
                            }
                            
                            let messages = dataDict?.value(forKey: "messages") as? String
                            if (messages == "on")
                            {
                                settingsDict["Messages"] = "On"
                            }
                            else
                            {
                                settingsDict["Messages"] = "Off"
                            }
                            
                            ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                        }
                        
                        
                        
                        self.usersDetailsDict = responseData.value(forKey: "data") as! NSDictionary
                        
                        UserDefaults.standard.setValue(self.usersDetailsDict, forKey: "userDetailsDict")
                        UserDefaults.standard.synchronize()
                  
                        var userGenderTypeStr  = String()
                        userGenderTypeStr = self.usersDetailsDict.value(forKey: "gender") as! String
                        if (userGenderTypeStr == "male") || (userGenderTypeStr == "Male")  || (userGenderTypeStr == "1")
                        {
                            self.maleCheck = true
                            self.femaleCheck = false
                            self.gender_type = "male"
                            self.myGender_lbl.text = "Male"
                        }
                        else
                        {
                            self.femaleCheck = true
                            self.gender_type = "female"
                            self.maleCheck = false
                            self.myGender_lbl.text = "Female"
                        }
                        
                        let userName = self.usersDetailsDict.value(forKey: "username") as? String
                        let userAge = self.usersDetailsDict.value(forKey: "age") as? String
                        self.usersName_lbl.text = userName! + ", " +  userAge!
                        self.userName_lbl.text = self.usersDetailsDict.value(forKey: "username") as? String
                        
                        let keysArray = self.usersDetailsDict.allKeys as! [String]
                        
                        if keysArray.contains("purchased_package")
                        {
                            let purchased_package_Array = self.usersDetailsDict.value(forKey: "purchased_package") as! NSArray
                            
                            if (purchased_package_Array.count <= 0)
                            {
                                UserDefaults.standard.setValue("packageNotPurchased", forKey: "checkPurchasedPackage")
                                UserDefaults.standard.synchronize()
                            }
                            else
                            {
                                
                                for index in 0..<purchased_package_Array.count
                                {
                                    
                                    var packagePurchasedDict = NSDictionary()
                                    packagePurchasedDict = purchased_package_Array[index] as! NSDictionary
                                    
                                    if (packagePurchasedDict["packages"] != nil)
                                    {
                                        var packageDict = NSDictionary()
                                        packageDict = packagePurchasedDict.value(forKey: "packages") as! NSDictionary
                                        
                                        if (packageDict["description"] != nil)
                                        {
                                            let packageDetails = packageDict.value(forKey: "description") as! String
                                            
                                            UserDefaults.standard.setValue(packageDetails, forKey: "checkPurchasedPackageDetails")
                                            UserDefaults.standard.synchronize()
                                        }
                                    }
                                }
                                
                                UserDefaults.standard.setValue("packagePurchased", forKey: "checkPurchasedPackage")
                                UserDefaults.standard.synchronize()
                            }
                        }
                        
                        if keysArray.contains("about")
                        {
                            var descriptionStr = String()
                            
                            if (descriptionStr.count > 0)
                            {
                                print("descriptionStr count is greater than 0")
                            }else{
                                print("descriptionStr count is less than 0")
                            }
                            
                            descriptionStr = (self.usersDetailsDict.value(forKey: "about") as? String)!
                            
                            if descriptionStr.count > 0
                            {
                                self.aboutMe_txtView.text = self.usersDetailsDict.value(forKey: "about") as? String
                                self.aboutMe_placeholderLbl.isHidden = true
                            }
                            else{
                                self.aboutMe_txtView.text = ""
                                self.aboutMe_placeholderLbl.isHidden = false
                            }
                        }else{
                            self.aboutMe_txtView.text = ""
                            self.aboutMe_placeholderLbl.isHidden = false
                        }
                        
                        if keysArray.contains("spotlight")
                        {
                            self.checkSpotlight = self.usersDetailsDict.value(forKey: "spotlight") as! String
                            if self.checkSpotlight == "spotlightPurchased"
                            {
                                UserDefaults.standard.set(true, forKey: "spotlightPurchased")
                                UserDefaults.standard.synchronize()
                            }
                            else
                            {
                                UserDefaults.standard.set(false, forKey: "spotlightPurchased")
                                UserDefaults.standard.synchronize()
                            }
                        }
                        else
                        {
                            UserDefaults.standard.set(false, forKey: "spotlightPurchased")
                            UserDefaults.standard.synchronize()
                        }
                      
                        let location_sharing = dataDict?.value(forKey: "location_sharing") as! String
                        print("location_sharing = \(String(describing: location_sharing))")
                        
                        if keysArray.contains("location") && (location_sharing == "on")
                        {
                            self.usersLocation_lbl.text = self.usersDetailsDict.value(forKey: "location") as? String
                        }
                        else{
                            self.usersLocation_lbl.text = ""
                        }
                        
                        if let countryStr = self.usersDetailsDict.value(forKey: "country") as? NSDictionary
                        {
                            if let country_name = countryStr.value(forKey: "country_name") as? String
                            {
                                self.representCountry_lbl.text  = country_name
                            }
                            else{
                                self.representCountry_lbl.text = ""
                            }
                        }
                        else{
                            self.representCountry_lbl.text = ""
                        }
                    
                        
                        if keysArray.contains("profileImageType")
                        {
                            self.profileImageType = (self.usersDetailsDict.value(forKey: "profileImageType") as? String)!
                        }
                        else{
                            self.profileImageType = "image"
                        }
                        
                        if let bodytype = self.usersDetailsDict.value(forKey: "bodytype") as? NSDictionary
                        {
                            if let bodytypeName = bodytype.value(forKey: "name") as? String
                            {
                                self.bodyType_lbl.text = bodytypeName
                            }
                        }
                        
                        if let drinkingtype = self.usersDetailsDict.value(forKey: "drinking") as? NSDictionary
                        {
                            if let drinkingName = drinkingtype.value(forKey: "name") as? String
                            {
                               self.drinking_lbl.text = drinkingName
                            }
                        }
                        
                        if let educationtype = self.usersDetailsDict.value(forKey: "education") as? NSDictionary
                        {
                            if let educationName = educationtype.value(forKey: "name") as? String
                            {
                                self.education_lbl.text = educationName
                            }
                        }
            
                        if let haircolortype = self.usersDetailsDict.value(forKey: "haircolor") as? NSDictionary
                        {
                            if let haircolorName = haircolortype.value(forKey: "name") as? String
                            {
                                 self.hairColor_lbl.text = haircolorName
                            }
                        }
                        
                        if let interestesintype = self.usersDetailsDict.value(forKey: "interestesin") as? NSDictionary
                        {
                            if let interestesinName = interestesintype.value(forKey: "name") as? String
                            {
                                 self.interestedIn_lbl.text = interestesinName
                            }
                        }
                        
                        
                        if let meetuppreferences = self.usersDetailsDict.value(forKey: "meetuppreferences") as? NSDictionary
                        {
                            if let meetuppreferencesName = meetuppreferences.value(forKey: "name") as? String
                            {
                                self.meetup_lbl.text = meetuppreferencesName
                            }
                        }
                        
                        if let relationshipstatus = self.usersDetailsDict.value(forKey: "relationshipstatus") as? NSDictionary
                        {
                            if let relationshipstatusName = relationshipstatus.value(forKey: "name") as? String
                            {
                                self.relationship_lbl.text = relationshipstatusName
                            }
                        }
                        
                        if let smoking = self.usersDetailsDict.value(forKey: "smoking") as? NSDictionary
                        {
                            if let smokingName = smoking.value(forKey: "name") as? String
                            {
                                self.smoking_lbl.text = smokingName
                            }
                        }
 
                        let profileURL = (self.usersDetailsDict.value(forKey: "profile_pic") as? String)!
                        
                        DispatchQueue.main.async {
                            self.usersProfileImage.sd_setImage(with: URL(string: profileURL), placeholderImage: UIImage(named: ""))
                        }
                        
                        self.usersProfileImage.contentMode = UIViewContentMode.scaleAspectFill
                        self.usersProfileImage.clipsToBounds = true
                        
                        UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "username") as? String, forKey: "userName")
                        UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "age") as? String, forKey: "Age")
                        UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "location") as? String, forKey: "Location")
                        UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "profile_pic") as? String, forKey: "userProfilePicture")
                        UserDefaults.standard.setValue(self.usersDetailsDict.value(forKey: "profile_pic_thumb")!, forKey: "profile_pic_thumb")
                        UserDefaults.standard.synchronize()
                        
                        if let userfooddrinks = self.usersDetailsDict.value(forKey: "userfooddrinks") as? NSArray
                        {
                            for index in 0..<userfooddrinks.count
                            {
                                var userfooddrinksDict = NSDictionary()
                                userfooddrinksDict = userfooddrinks[index] as! NSDictionary
                                
                                if (userfooddrinksDict["other_text"] != nil)
                                {
                                    var other_text = (userfooddrinksDict.value(forKey: "other_text") as? String)!
                                    if (other_text != "")
                                    {
                                        other_text = other_text + "@otherFoodDrink"
                                        
                                        if !(self.getUpdatedInterestsArray.contains(other_text))
                                        {
                                            self.getUpdatedInterestsArray.add(other_text)
                                        }
                                    }
                                }
                                
                                if (userfooddrinksDict["fooddrinks"] != nil)
                                {
                                    if let fooddrinksDict = userfooddrinksDict.value(forKey: "fooddrinks") as? NSDictionary
                                    {
                                        if (fooddrinksDict["name"] != nil)
                                        {
                                            let name = fooddrinksDict.value(forKey: "name") as! String
                                            
                                            if !(self.getUpdatedInterestsArray.contains(name))
                                            {
                                                self.getUpdatedInterestsArray.add(name)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if let userhobbies = self.usersDetailsDict.value(forKey: "userhobbies") as? NSArray
                        {
                            for index in 0..<userhobbies.count
                            {
                                var userhobbiesDict = NSDictionary()
                                userhobbiesDict = userhobbies[index] as! NSDictionary
                                
                                if (userhobbiesDict["other_text"] != nil)
                                {
                                    var other_text = (userhobbiesDict.value(forKey: "other_text") as? String)!
                                    if (other_text != "")
                                    {
                                        other_text = other_text + "@otherHobbies"
                                        
                                        if !(self.getUpdatedInterestsArray.contains(other_text))
                                        {
                                            self.getUpdatedInterestsArray.add(other_text)
                                        }
                                    }
                                }
                                
                                if (userhobbiesDict["hobbies"] != nil)
                                {
                                    if let fooddrinksDict = userhobbiesDict.value(forKey: "hobbies") as? NSDictionary
                                    {
                                        if (fooddrinksDict["name"] != nil)
                                        {
                                            let name = fooddrinksDict.value(forKey: "name") as! String
                                            
                                            if !(self.getUpdatedInterestsArray.contains(name))
                                            {
                                                self.getUpdatedInterestsArray.add(name)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if let usermusic = self.usersDetailsDict.value(forKey: "usermusic") as? NSArray
                        {
                            for index in 0..<usermusic.count
                            {
                                var usermusicDict = NSDictionary()
                                usermusicDict = usermusic[index] as! NSDictionary
                                
                                if (usermusicDict["other_text"] != nil)
                                {
                                    var other_text = (usermusicDict.value(forKey: "other_text") as? String)!
                                    if (other_text != "")
                                    {
                                        other_text = other_text + "@otherMusic"
                                        
                                        if !(self.getUpdatedInterestsArray.contains(other_text))
                                        {
                                            self.getUpdatedInterestsArray.add(other_text)
                                        }
                                    }
                                }
                                
                                if (usermusicDict["music"] != nil)
                                {
                                    if let fooddrinksDict = usermusicDict.value(forKey: "music") as? NSDictionary
                                    {
                                        if (fooddrinksDict["name"] != nil)
                                        {
                                            let name = fooddrinksDict.value(forKey: "name") as! String
                                            
                                            if !(self.getUpdatedInterestsArray.contains(name))
                                            {
                                                self.getUpdatedInterestsArray.add(name)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if let userprofession = self.usersDetailsDict.value(forKey: "userprofession") as? NSArray
                        {
                            for index in 0..<userprofession.count
                            {
                                var userprofessionDict = NSDictionary()
                                userprofessionDict = userprofession[index] as! NSDictionary
                                
                                if (userprofessionDict["other_text"] != nil)
                                {
                                    var other_text = (userprofessionDict.value(forKey: "other_text") as? String)!
                                    if (other_text != "")
                                    {
                                        other_text = other_text + "@otherProfession"
                                        
                                        if !(self.getUpdatedInterestsArray.contains(other_text))
                                        {
                                            self.getUpdatedInterestsArray.add(other_text)
                                        }
                                    }
                                }
                                
                                if (userprofessionDict["profession"] != nil)
                                {
                                    if let fooddrinksDict = userprofessionDict.value(forKey: "profession") as? NSDictionary
                                    {
                                        if (fooddrinksDict["name"] != nil)
                                        {
                                            let name = fooddrinksDict.value(forKey: "name") as! String
                                            
                                            if !(self.getUpdatedInterestsArray.contains(name))
                                            {
                                                self.getUpdatedInterestsArray.add(name)
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        }
                        
                        
                        if let usersports = self.usersDetailsDict.value(forKey: "usersports") as? NSArray
                        {
                            for index in 0..<usersports.count
                            {
                                
                                var usersportsDict = NSDictionary()
                                usersportsDict = usersports[index] as! NSDictionary
                                
                                if (usersportsDict["other_text"] != nil)
                                {
                                    var other_text = (usersportsDict.value(forKey: "other_text") as? String)!
                                    
                                    if (other_text != "")
                                    {
                                        other_text = other_text + "@otherSports"
                                        if !(self.getUpdatedInterestsArray.contains(other_text))
                                        {
                                            self.getUpdatedInterestsArray.add(other_text)
                                        }
                                    }
                                }
                                
                                if (usersportsDict["sports"] != nil)
                                {
                                    if let fooddrinksDict = usersportsDict.value(forKey: "sports") as? NSDictionary
                                    {
                                        if (fooddrinksDict["name"] != nil)
                                        {
                                            let name = fooddrinksDict.value(forKey: "name") as! String
                                            
                                            if !(self.getUpdatedInterestsArray.contains(name))
                                            {
                                                self.getUpdatedInterestsArray.add(name)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if let religiousreviews = self.usersDetailsDict.value(forKey: "religiousreviews") as? String
                        {
                            if !(self.getUpdatedInterestsArray.contains(religiousreviews))
                            {
                                self.getUpdatedInterestsArray.add(religiousreviews)
                            }
                        }
                        
                        if let politicalreviews = self.usersDetailsDict.value(forKey: "politicalreviews") as? String
                        {
                            if !(self.getUpdatedInterestsArray.contains(politicalreviews))
                            {
                                self.getUpdatedInterestsArray.add(politicalreviews)
                            }
                        }
                        
                        for index in 0..<self.getUpdatedInterestsArray.count
                        {
                            if !(self.interestsArray.contains(self.getUpdatedInterestsArray[index] as! String))
                            {
                                self.interestsArray.append(self.getUpdatedInterestsArray[index] as! String)
                            }
                            
                            
                            if index <= 4
                            {
                                if !(self.showInterestsArray.contains(self.getUpdatedInterestsArray[index] as! String))
                                {
                                    self.showInterestsArray.append(self.getUpdatedInterestsArray[index] as! String)
                                }
                            }
                            else{
                                print("index greater than 4")
                            }
                        }
                        
                        self.interestsTxtView.isHidden = false
                        
                        if self.isLoadMore == false{
                            for index in 0..<self.showInterestsArray.count
                            {
                                print("index = \(index)")
                                var string = self.showInterestsArray.joined(separator: ", ")
                                
                                if (string.range(of: "@") != nil)
                                {
                                    let fullNameArr = string.components(separatedBy: "@")
                                    if fullNameArr.count > 0
                                    {
                                        if fullNameArr.count == 1
                                        {
                                            let firstName: String = fullNameArr[0]
                                            string = firstName
                                        }
                                        else
                                        {
                                            let firstName: String = fullNameArr[0]
//                                            let lastName: String = fullNameArr[1]

                                            string = firstName
                                        }
                                    }
                                }
                                self.interestsTxtView.text = string
                            }
                        }
                        else
                        {
                            for index in 0..<self.interestsArray.count
                            {
                                print("index = \(index)")
                                let string = self.interestsArray.joined(separator: ", ")
                                self.interestsTxtView.text = string
                            }
                        }
                        
                        completion(true)
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        completion(true)
                    }
                    else
                    {
                        if let myDetailsDict = UserDefaults.standard.value(forKey: "userDetailsDict") as? NSMutableDictionary
                        {
                            self.usersDetailsDict = myDetailsDict
                            
                            var userGenderTypeStr  = String()
                            userGenderTypeStr = self.usersDetailsDict.value(forKey: "gender") as! String
                            if userGenderTypeStr == "male" || userGenderTypeStr == "Male"
                            {
                                self.maleCheck = true
                                self.femaleCheck = false
                                self.gender_type = "male"
                                self.myGender_lbl.text = "Male"
                            }
                            else
                            {
                                self.femaleCheck = true
                                self.gender_type = "female"
                                self.maleCheck = false
                                self.myGender_lbl.text = "Female"
                            }
                            
                            let userName = self.usersDetailsDict.value(forKey: "username") as? String
                            let userAge = self.usersDetailsDict.value(forKey: "age") as? String
                            self.usersName_lbl.text = userName! + ", " +  userAge!
                            self.userName_lbl.text = self.usersDetailsDict.value(forKey: "username") as? String
                            
                            let keysArray = self.usersDetailsDict.allKeys as! [String]
                            if keysArray.contains("AboutMe_description")
                            {
                                var descriptionStr = String()
                                descriptionStr = (self.usersDetailsDict.value(forKey: "AboutMe_description") as? String)!
                                if descriptionStr.count > 0
                                {
                                    self.aboutMe_txtView.text = self.usersDetailsDict.value(forKey: "AboutMe_description") as? String
                                    self.aboutMe_placeholderLbl.isHidden = true
                                }
                                else{
                                    self.aboutMe_txtView.text = ""
                                    self.aboutMe_placeholderLbl.isHidden = false
                                }
                            }else{
                                self.aboutMe_txtView.text = ""
                                self.aboutMe_placeholderLbl.isHidden = false
                            }
                            
                            if keysArray.contains("spotlight")
                            {
                                self.checkSpotlight = self.usersDetailsDict.value(forKey: "spotlight") as! String
                                if self.checkSpotlight == "spotlightPurchased"
                                {
                                    UserDefaults.standard.set(true, forKey: "spotlightPurchased")
                                    UserDefaults.standard.synchronize()
                                }
                                else
                                {
                                    UserDefaults.standard.set(false, forKey: "spotlightPurchased")
                                    UserDefaults.standard.synchronize()
                                }
                            }
                            else
                            {
                                UserDefaults.standard.set(false, forKey: "spotlightPurchased")
                                UserDefaults.standard.synchronize()
                            }
                            
                            
                            if keysArray.contains("location")
                            {
                                self.usersLocation_lbl.text = self.usersDetailsDict.value(forKey: "location") as? String
                            }
                            else{
                                self.usersLocation_lbl.text = ""
                            }
                            
                            if keysArray.contains("country")
                            {
                                self.representCountry_lbl.text = self.usersDetailsDict.value(forKey: "country") as? String
                            }
                            else{
                                self.representCountry_lbl.text = ""
                            }
                            
                            
                            if keysArray.contains("profileImageType")
                            {
                                self.profileImageType = (self.usersDetailsDict.value(forKey: "profileImageType") as? String)!
                            }
                            else{
                                self.profileImageType = "image"
                            }
                            
                            if let bodytype = self.usersDetailsDict.value(forKey: "bodytype") as? NSDictionary
                            {
                                if let bodytypeName = bodytype.value(forKey: "name") as? String
                                {
                                    self.bodyType_lbl.text = bodytypeName
                                }
                            }
                            
                            if let drinkingtype = self.usersDetailsDict.value(forKey: "drinking") as? NSDictionary
                            {
                                if let drinkingName = drinkingtype.value(forKey: "name") as? String
                                {
                                    self.drinking_lbl.text = drinkingName
                                }
                            }
                            
                            if let educationtype = self.usersDetailsDict.value(forKey: "education") as? NSDictionary
                            {
                                if let educationName = educationtype.value(forKey: "name") as? String
                                {
                                    self.education_lbl.text = educationName
                                }
                            }
                            
                            if let haircolortype = self.usersDetailsDict.value(forKey: "haircolor") as? NSDictionary
                            {
                                if let haircolorName = haircolortype.value(forKey: "name") as? String
                                {
                                    self.hairColor_lbl.text = haircolorName
                                }
                            }
                            
                            if let interestesintype = self.usersDetailsDict.value(forKey: "interestesin") as? NSDictionary
                            {
                                if let interestesinName = interestesintype.value(forKey: "name") as? String
                                {
                                    self.interestedIn_lbl.text = interestesinName
                                }
                            }
                            
                            if let meetuppreferences = self.usersDetailsDict.value(forKey: "meetuppreferences") as? NSDictionary
                            {
                                if let meetuppreferencesName = meetuppreferences.value(forKey: "name") as? String
                                {
                                    self.meetup_lbl.text = meetuppreferencesName
                                }
                            }
                            
                            if let relationshipstatus = self.usersDetailsDict.value(forKey: "relationshipstatus") as? NSDictionary
                            {
                                if let relationshipstatusName = relationshipstatus.value(forKey: "name") as? String
                                {
                                    self.relationship_lbl.text = relationshipstatusName
                                }
                            }
                            
                            if let smoking = self.usersDetailsDict.value(forKey: "smoking") as? NSDictionary
                            {
                                if let smokingName = smoking.value(forKey: "name") as? String
                                {
                                    self.smoking_lbl.text = smokingName
                                }
                            }
                            
                            let profileURL = (self.usersDetailsDict.value(forKey: "profile_pic") as? String)!
                            DispatchQueue.main.async {
                                self.usersProfileImage.sd_setImage(with: URL(string: profileURL), placeholderImage: UIImage(named: ""))
                            }
                            
                            UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "username") as? String, forKey: "UserName")
                            UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "age") as? String, forKey: "Age")
                            UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "location") as? String, forKey: "Location")
                            UserDefaults.standard.set(self.usersDetailsDict.value(forKey: "profile_pic") as? String, forKey: "userProfilePicture")
                            UserDefaults.standard.setValue(self.usersDetailsDict.value(forKey: "profile_pic_thumb")!, forKey: "profile_pic_thumb")
                            UserDefaults.standard.synchronize()
                            
                            completion(true)
                        }
                        else
                        {
                            completion(true)
                        }
                    }
                })
            }
        }
    }
    
    func getUploadedMediaFiles()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
        
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getProfileAlbum(userID: userID, authToken: authToken, page: "1",other_user_id: userID,  completion: { (responseData) in
                     
                        self.getPhotoKeyArray = []
                        self.profileImagesArray = []
                        self.albumArray = []
                        self.videoArray = []
                        self.listingArray = []
                        
                        if (responseData.value(forKey: "status") as? String == "200") 
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            
                            self.listingArray = dataDict?.value(forKey: "list") as! NSArray
                            
                            if (self.listingArray.count <= 0)
                            {
                                self.view.isUserInteractionEnabled = true
                                self.ImagesActivityIndicator.isHidden = true
                                self.pageControl.isHidden = true
                                self.userImagesView.image = UIImage(named: "Default Icon")
                                self.userImagesView.contentMode = UIViewContentMode.scaleAspectFill
                                self.getNoMediaAlbumUploaded()
                            }
                            
                            for index in 0..<self.listingArray.count
                            {
                                var albumDict = NSDictionary()
                                albumDict = self.listingArray[index] as! NSDictionary
                                
                                if !(self.albumArray.contains(albumDict))
                                {
                                    self.albumArray.add(albumDict)
                                }
                                
                                let keysArray = albumDict.allKeys as! [String]
                                if keysArray.contains("type")
                                {
                                    if (albumDict.value(forKey: "type") as? String == "1")
                                    {
                                        if !(self.profileImagesArray.contains(albumDict.value(forKey: "media") as! String))
                                        {
                                            self.profileImagesArray.append(albumDict.value(forKey: "media") as! String)
                                        }
                                    }
                                    else
                                    {
                                        if !(self.videoArray.contains(albumDict.value(forKey: "media") as! String))
                                        {
                                            self.videoArray.append(albumDict.value(forKey: "media") as! String)
                                        }
                                    }
                                }
                                
                                
                                if self.albumArray.count == 0 {
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.view.isUserInteractionEnabled = true
                                    self.noDataFoundLbl.isHidden = true
                                    self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                        self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                    }, completion: { (finished) in
                                    })
                                }
                                else
                                {
                                    self.noDataFoundLbl.isHidden = true
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                }
                                
                                self.imagesScrollView.frame = CGRect(x:0, y: self.imagesScrollView.frame.origin.y, width:self.view.frame.width, height:self.imagesScrollView.frame.height)
                                
                                var widthMultipliier = CGFloat()
                                var x_pos = CGFloat()
                                x_pos=0;
                                
                                if self.profileImagesArray.count == 0{
                                    self.view.isUserInteractionEnabled = true
                                    self.ImagesActivityIndicator.isHidden = true
                                    self.pageControl.isHidden = true
                                    self.userImagesView.image = UIImage(named: "Default Icon")
                                    self.userImagesView.contentMode = UIViewContentMode.scaleAspectFill
                                }
                                
                                for index in 0..<self.profileImagesArray.count
                                {
                                    widthMultipliier = CGFloat(index)
                                    
                                    self.newImageView = UIImageView(frame:CGRect(x:x_pos, y:0,width:self.view.frame.size.width, height:self.imagesScrollView.frame.size.height))
                                    
                                    self.newImageView.backgroundColor = UIColor.clear
                                    self.userImagesView.isHidden = true
                                    self.pageControl.isHidden = false
                                    
                                    self.newImageView.sd_setImage(with:  URL(string: self.profileImagesArray[index]), placeholderImage: UIImage(named: "Default Icon"))
                                    
                                    self.newImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
                                    
                                    self.newImageView.contentMode = UIViewContentMode.scaleAspectFit
                                    
                                    self.newImageView.clipsToBounds = true
                                self.imagesScrollView.addSubview(self.newImageView)
                                    
                                    x_pos = x_pos+self.imagesScrollView.frame.width;
                                    
                                    self.imagesScrollView.contentSize = CGSize(width:self.imagesScrollView.frame.width * widthMultipliier+self.imagesScrollView.frame.width, height:self.imagesScrollView.frame.height)
                                }
                            }
                            
                            self.photosCollectionView.isScrollEnabled = false
                            var collectionViewHeight = Int()
                            if self.albumArray.count % 2 == 0
                            {
                                collectionViewHeight = (self.albumArray.count/2)*150
                            }
                            else{
                                collectionViewHeight = ((self.albumArray.count/2 ) +  1)*150
                            }
                            
                            self.photosCollectionView.contentSize.height = CGFloat(collectionViewHeight)
                            self.photosCollectionView.frame = CGRect(x: self.photosCollectionView.frame.origin.x,y: self.photosCollectionView.frame.origin.y, width: self.photosCollectionView.frame.size.width, height: self.photosCollectionView.contentSize.height + 10)
                            
                            let value = self.imagesScrollView.frame.size as NSValue
                            let Data = NSKeyedArchiver.archivedData(withRootObject: value)
                            UserDefaults.standard.set(Data, forKey: "self.imagesScrollView.frame.size")
                            
                            self.imagesScrollView.delegate = self
                            self.pageControl.currentPage = 0
                            self.pageControl.numberOfPages = self.profileImagesArray.count
                            self.photosCollectionView.reloadData()
                            
                            if !self.timerForSlider.isValid
                            {
                                self.timerForSlider =  Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.moveToNextPage), userInfo: nil, repeats: true)
                            }
                            
                            let when = DispatchTime.now() + 2
                            DispatchQueue.main.asyncAfter(deadline: when) {
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            self.getNoMediaAlbumUploaded()
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            self.getNoMediaAlbumUploaded()
                        }
                    })
                }
            }
        }
    }
    
    // MARK: - ****** Method Called when no media record uploaded in database ******
    func getNoMediaAlbumUploaded()
    {
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
        self.pageControl.isHidden = true
        self.userImagesView.image = UIImage(named: "Default Icon")
        self.noDataFoundLbl.isHidden = true
        self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { (finished) in
        })
        
        self.userImagesView.contentMode = UIViewContentMode.scaleAspectFill
        self.lastPhotoUrl = ""
        self.timestampArray = []
        self.getPhotoKeyArray = []
        self.profileImagesArray = []
        self.albumArray = []
     
        self.ImagesActivityIndicator.isHidden = true
        self.pageControl.isHidden = true
        self.userImagesView.image = UIImage(named: "Default Icon")
        self.userImagesView.contentMode = UIViewContentMode.scaleAspectFill
        self.view.isUserInteractionEnabled = true
        
        let value = self.imagesScrollView.frame.size as NSValue
        let Data = NSKeyedArchiver.archivedData(withRootObject: value)
        UserDefaults.standard.set(Data, forKey: "self.imagesScrollView.frame.size")
        
        self.photosCollectionView.isScrollEnabled = false
        var collectionViewHeight = Int()
        if self.albumArray.count % 2 == 0
        {
            collectionViewHeight = (self.albumArray.count/2)*150
        }
        else
        {
            collectionViewHeight = ((self.albumArray.count/2 ) +  1)*150
        }
        
        self.photosCollectionView.contentSize.height = CGFloat(collectionViewHeight)
        self.photosCollectionView.frame = CGRect(x: self.photosCollectionView.frame.origin.x,y: self.photosCollectionView.frame.origin.y, width: self.photosCollectionView.frame.size.width, height: self.photosCollectionView.contentSize.height + 10)
    }
    
    // MARK: - ****** Update About Me Values ******
    func updateUsersDetails(notificationType: String, getVisibility: String, completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                ApiHandler.updateSettings(forUserID: userID, authToken: userToken, typeToBeUpdated: notificationType, isSetTo: getVisibility, completion: { (responseData) in
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
//                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        completion(true)
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        completion(false)
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        completion(false)
                    }
                })
            }
        }
    }
    
    
    // MARK: - ****** UIScrollView Delegates ******
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if (scrollView == self.wrapperScrollView)
        {
            if (UIScreen.main.bounds.size.height == 736)
            {
                if (scrollView.contentOffset.y >= 230) {
                    self.topViewButtons.isHidden = false
                    if (self.tappedBtnStr == "aboutBtn")
                    {
                        self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 350
                    }
                    else if (self.tappedBtnStr == "albumBtn")
                    {
                        if (self.photosCollectionView.contentSize.height < 1000)
                        {
                            self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                            self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                        }
                        else{
                            self.wrapperScrollView.contentSize.height = self.photosCollectionView.frame.origin.y + self.photosCollectionView.contentSize.height + 10
                        }
                    }
                    else if (self.tappedBtnStr == "postsBtn"){
                        if (self.myPosts_tableView.contentSize.height < 1000)
                        {
                            self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                            self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                        }
                        else
                        {
                            self.wrapperScrollView.contentSize.height = self.myPostsView.frame.origin.y + self.myPosts_tableView.contentSize.height
                        }
                    }
                }
                else
                {
                    self.topViewButtons.isHidden = true
                }
            }
            else if (UIScreen.main.bounds.size.height == 667)
            {
//                  if (scrollView.contentOffset.y >= 430) {
                if (scrollView.contentOffset.y >= 205) {
                    self.topViewButtons.isHidden = false
                    if (self.tappedBtnStr == "aboutBtn")
                    {
                        self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 350
                    }
                    else if (self.tappedBtnStr == "albumBtn")
                    {
                        if (self.photosCollectionView.contentSize.height < 1000)
                        {
                            self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                            self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                        }
                        else{
                            self.wrapperScrollView.contentSize.height = self.photosCollectionView.frame.origin.y + self.photosCollectionView.contentSize.height + 10
                        }
                    }
                    else if (self.tappedBtnStr == "postsBtn"){
                        if (self.myPosts_tableView.contentSize.height < 1000)
                        {
                            self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                            self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                        }
                        else{
                            self.wrapperScrollView.contentSize.height = self.myPostsView.frame.origin.y + self.myPosts_tableView.contentSize.height
                        }
                    }
                }
                else
                {
                    self.topViewButtons.isHidden = true
                }
            }
            else if (UIScreen.main.bounds.size.height == 568)
            {
                if (scrollView.contentOffset.y >= 180) {
                    self.topViewButtons.isHidden = false
                    if (self.tappedBtnStr == "aboutBtn")
                    {
                        self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 350
                    }
                    else if (self.tappedBtnStr == "albumBtn")
                    {
                        if (self.photosCollectionView.contentSize.height < 1000)
                        {
                            self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                            self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                        }
                        else{
                            self.wrapperScrollView.contentSize.height = self.photosCollectionView.frame.origin.y + self.photosCollectionView.contentSize.height + 10
                        }
                    }
                    else if (self.tappedBtnStr == "postsBtn"){
                        if (self.myPosts_tableView.contentSize.height < 1000)
                        {
                            self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                            self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                        }
                        else{
                            self.wrapperScrollView.contentSize.height = self.myPostsView.frame.origin.y + self.myPosts_tableView.contentSize.height
                        }
                    }
                }
                else
                {
                    self.topViewButtons.isHidden = true
                }
            }
        }
        else if (scrollView == self.about_wrapperScrollView)
        {
            if (scrollView.contentOffset.y <= 0){
                self.topViewButtons.isHidden = true
             }
        }
    }
    
  
    // MARK: - ****** Media captured From custom camera ******
    func mediaCapturedforProfileAlbums(value : Notification){
        
        let dic = value.object as! Dictionary<String, Any>
        
        media_uploadedFrom = dic["media_uploadedFrom"] as! String
        
        self.mediaType = dic["mediaType"] as! String
        
        if mediaType == "public.image" {
            
            if (dic["capturedImage"] != nil)
            {
                let chosenImage: UIImage = dic["capturedImage"] as! UIImage
                
                self.selectImageBool = true
                self.photoUploadView.isHidden = false
                self.addcaption_txtView.text = ""
                self.addCaptionLbl.isHidden = false
                self.addPhotoImageView.image = chosenImage
                
                self.addPhotoImageView.frame.size.width = self.view.frame.size.width
                self.addPhotoImageView.frame.size.height = self.view.frame.size.width
                
                self.addPhotoImageView.contentMode = UIViewContentMode.scaleAspectFit
            }
        }
        
        if mediaType == "public.movie" {
            
            if (dic["videoUrl"] != nil)
            {
                self.videoCaption_txtView.text = ""
                videoUploadbool = false
                self.videoCaption_lbl.isHidden = false
                
                videoURL = dic["videoUrl"] as! NSURL;
                self.uploadVideoView.isHidden = false
                self.videoController = MPMoviePlayerController()
                self.videoController.contentURL = self.videoURL as URL!
                if UIScreen.main.bounds.size.height == 568{
                    self.videoController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(60), width: CGFloat(self.view.frame.size.width), height: CGFloat(250))
                    self.videoCaption_txtView.frame.origin.y = videoController.view.frame.origin.y + videoController.view.frame.size.height + 20
                    self.videoCaption_lbl.frame.origin.y = videoController.view.frame.origin.y + videoController.view.frame.size.height + 20
                }
                else
                {
                    self.videoController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(80), width: CGFloat(self.view.frame.size.width), height: CGFloat(350))
                    self.videoCaption_txtView.frame.origin.y = videoController.view.frame.origin.y + videoController.view.frame.size.height + 20
                    self.videoCaption_lbl.frame.origin.y = videoController.view.frame.origin.y + videoController.view.frame.size.height + 20
                }
                
                self.wrapperScrollToUploadVideo.addSubview(self.videoController.view)
                self.videoController.play()
            }
        }
    }
    
    // MARK: - ****** Set done button on keyboard as input accessory ******
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
         let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(ProfileViewController.doneButtonTextView))
         let cancelBarButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(ProfileViewController.dismissKeyboard))
        keyboardToolbar.items = [cancelBarButton, flexBarButton, doneBarButton]
        
        self.videoCaption_txtView.inputAccessoryView = keyboardToolbar
        self.addcaption_txtView.inputAccessoryView = keyboardToolbar
        self.displaycaption_textView.inputAccessoryView = keyboardToolbar
        self.displayVideoCaption_txtView.inputAccessoryView = keyboardToolbar
        self.aboutMe_txtView.inputAccessoryView = keyboardToolbar
    }
    
    
    // MARK: - ****** Done btn action for keyboard ******
    func doneButtonTextView(textView: UITextView)
    {
        if (self.typeOftextView == "self.aboutMe_txtView")
        {
            view.endEditing(true)

                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                   
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    self.updateUsersDetails(notificationType: "about", getVisibility: self.aboutMe_txtView.text, completion: { (response) in
                        if (response == true)
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        }
                        self.viewUserDetails(completion: { (data) in
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        })
                    })
                }
        }
        else if (self.typeOftextView == "self.displayVideoCaption_txtView")
        {
            view.endEditing(true)
            
            self.displayVideoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.displayVideoCaption_txtView.frame.size.width
            self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displayVideoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displayVideoCaption_txtView.frame = newFrame;
            
            self.crossViewVideoAlbum.frame.origin.y = self.displayVideoCaption_txtView.frame.origin.y + self.displayVideoCaption_txtView.frame.height + 20
            
            self.wrapperScrollForVideoAlbum.isScrollEnabled=true
            self.wrapperScrollForVideoAlbum.contentSize.height = self.crossViewVideoAlbum.frame.origin.y + self.crossViewVideoAlbum.frame.size.height + 20
            
            
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                   
                    
                    if (self.displayVideoCaption_txtView.text != "")
                    {
                        var textMessage = String()
                        textMessage = self.displayVideoCaption_txtView.text!
                        if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false)
                        {
                            self.updateMediaCaption(mediaCaptionText: self.displayVideoCaption_txtView.text, mediaIDStr: self.deleteVideoURL)
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "", message: "Please enter a text.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                         self.updateMediaCaption(mediaCaptionText: self.displayVideoCaption_txtView.text, mediaIDStr: self.deleteVideoURL)
                    }
                    
                }
        }
        else if (self.typeOftextView == "self.displaycaption_textView")
        {
            view.endEditing(true)
            
            self.displaycaption_textView.isScrollEnabled = false
            
            let fixedWidth = self.displaycaption_textView.frame.size.width
            self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displaycaption_textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displaycaption_textView.frame = newFrame;
            
            self.crossViewPhotoAlbum.frame.origin.y = self.displaycaption_textView.frame.origin.y + self.displaycaption_textView.frame.height + 20
            
            self.wrapperScrollForPhotoAlbum.isScrollEnabled=true
            self.wrapperScrollForPhotoAlbum.contentSize.height = self.crossViewPhotoAlbum.frame.origin.y + self.crossViewPhotoAlbum.frame.size.height + 20

                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .online(.wwan) , .online(.wiFi):

                    if (self.displayVideoCaption_txtView.text != "")
                    {
                        var textMessage = String()
                        textMessage =  self.displaycaption_textView.text!
                        if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false)
                        {
                            self.updateMediaCaption(mediaCaptionText: self.displaycaption_textView.text, mediaIDStr: self.photoDeleteIndex)
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            
                            let alert = UIAlertController(title: "", message: "Please enter a text.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        self.updateMediaCaption(mediaCaptionText: self.displaycaption_textView.text, mediaIDStr: self.photoDeleteIndex)
                    }
                    
                }
        }
        else if (self.typeOftextView == "self.addcaption_txtView")
        {
            view.endEditing(true)
            
            self.addcaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.addcaption_txtView.frame.size.width
            self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.addcaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.addcaption_txtView.frame = newFrame;
            
            self.uploadPhotoBtn.frame.origin.y = self.addcaption_txtView.frame.origin.y + self.addcaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadPhoto.isScrollEnabled=true
            self.wrapperScrollToUploadPhoto.contentSize.height = self.uploadPhotoBtn.frame.origin.y + self.uploadPhotoBtn.frame.size.height + 20
        }
        else if (self.typeOftextView == "self.videoCaption_txtView")
        {
            view.endEditing(true)
            
            self.videoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.videoCaption_txtView.frame.size.width
            self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.videoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.videoCaption_txtView.frame = newFrame;
            
            self.uploadVideoBtn.frame.origin.y = self.videoCaption_txtView.frame.origin.y + self.videoCaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadVideo.isScrollEnabled=true
            self.wrapperScrollToUploadVideo.contentSize.height = self.uploadVideoBtn.frame.origin.y + self.uploadVideoBtn.frame.size.height + 20
        }
    }
    
    func updateMediaCaption(mediaCaptionText: String, mediaIDStr: String)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
      
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    let date = NSDate();
                    /// GET TIMESTAMP IN UNIX
                    let currentTimeStamp = Int64(date.timeIntervalSince1970)
                    
                    ApiHandler.updateProfileAlbum(userID: userID, authToken: authToken, type: self.mediaCaptionType, captionAdded: mediaCaptionText, mediaID: mediaIDStr, timestamp: Int(currentTimeStamp), completion: { (responseData) in
                        
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            
                            self.timestampArray = []
                            self.getPhotoKeyArray = []
                            self.videoArray = []
                            self.getPhotoKeyArray = []
                            self.profileImagesArray = []
                            self.albumArray = []
                            self.getUploadedMediaFiles()
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
            }
        }
    }
    
    
    // MARK: - ****** Action on resigning keyboard ******
    func dismissKeyboard()
    {
        self.typeOftextView = ""
        view.endEditing(true)
        
        if (self.typeOftextView == "self.displayVideoCaption_txtView")
        {
            self.displayVideoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.displayVideoCaption_txtView.frame.size.width
            self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displayVideoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displayVideoCaption_txtView.frame = newFrame;
            
            self.crossViewVideoAlbum.frame.origin.y = self.displayVideoCaption_txtView.frame.origin.y + self.displayVideoCaption_txtView.frame.height + 20
            
            self.wrapperScrollForVideoAlbum.isScrollEnabled=true
            self.wrapperScrollForVideoAlbum.contentSize.height = self.crossViewVideoAlbum.frame.origin.y + self.crossViewVideoAlbum.frame.size.height + 20
        }
        else if (self.typeOftextView == "self.displaycaption_textView")
        {
            self.displaycaption_textView.isScrollEnabled = false
            
            let fixedWidth = self.displaycaption_textView.frame.size.width
            self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displaycaption_textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displaycaption_textView.frame = newFrame;
            
            self.crossViewPhotoAlbum.frame.origin.y = self.displaycaption_textView.frame.origin.y + self.displaycaption_textView.frame.height + 20
            
            self.wrapperScrollForPhotoAlbum.isScrollEnabled=true
            self.wrapperScrollForPhotoAlbum.contentSize.height = self.crossViewPhotoAlbum.frame.origin.y + self.crossViewPhotoAlbum.frame.size.height + 20
        }
        else if (self.typeOftextView == "self.addcaption_txtView")
        {
            self.addcaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.addcaption_txtView.frame.size.width
            self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.addcaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.addcaption_txtView.frame = newFrame;
            
            self.uploadPhotoBtn.frame.origin.y = self.addcaption_txtView.frame.origin.y + self.addcaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadPhoto.isScrollEnabled=true
            self.wrapperScrollToUploadPhoto.contentSize.height = self.uploadPhotoBtn.frame.origin.y + self.uploadPhotoBtn.frame.size.height + 20
        }
        else if (self.typeOftextView == "self.videoCaption_txtView")
        {
            self.videoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.videoCaption_txtView.frame.size.width
            self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.videoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.videoCaption_txtView.frame = newFrame;
            
            self.uploadVideoBtn.frame.origin.y = self.videoCaption_txtView.frame.origin.y + self.videoCaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadVideo.isScrollEnabled=true
            self.wrapperScrollToUploadVideo.contentSize.height = self.uploadVideoBtn.frame.origin.y + self.uploadVideoBtn.frame.size.height + 20
        }
    }
    
    
    // MARK: - ****** Update Information on pop up delegate ******
    func popInformationOnPopMethod(returnFromView: String) {
        self.appDelegatePush = "fromWiReplies"
        self.typeOfAlbumsPush = "backFromReplies"
        self.viewAlbumBool = false
    }
    
    
    
    //MARK:- ****** confessionDropDown_BtnAction ******
    @IBAction func confessionDropDown_BtnAction(_ sender: UIButton) {
        
        if (self.myPostsArray.count > 0)
        {
            self.userIDToReportStr = self.myPostsArray[self.indexToClick].userIDStr
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
//                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
//                {
                    if (self.myPostsArray[self.indexToClick].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.myPostsArray[self.indexToClick].postedImage != "")
                        {
                            let imageURL = self.myPostsArray[self.indexToClick].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        print("typeOfCOnfession = \(typeOfCOnfession)")
  
                        self.showNewActionSheetForDropDownMenu_owner(postIdStr: self.myPostsArray[self.indexToClick].postIDStr, status: self.myPostsArray[self.indexToClick].statusAdded, backgroudImageStr: self.myPostsArray[self.indexToClick].postedImage, moodTypeImageStr: self.myPostsArray[self.indexToClick].moodTypeStr, timeFrameStr: self.myPostsArray[self.indexToClick].postHours, moodTypeStr: self.myPostsArray[self.indexToClick].moodTypeStr, moodTypeUrl: self.myPostsArray[self.indexToClick].moodTypeStr, photoTypeStr: typeOfCOnfession, getTotalLikeCount: self.myPostsArray[self.indexToClick].likesCount, getTotalReplyCount: self.myPostsArray[self.indexToClick].replyCount, gifHeight: self.myPostsArray[self.indexToClick].width, gifWidth: self.myPostsArray[self.indexToClick].height, postStatusStr: self.myPostsArray[self.indexToClick].statusAdded)
                    }
                    else
                    {
                        
                    }
//                }
            }
        }
    }
    
    
    // MARK: - ****** Show Action Sheet From select options. ******
    func showNewActionSheetForDropDownMenu_owner(postIdStr: String, status: String, backgroudImageStr : String, moodTypeImageStr: String, timeFrameStr: String, moodTypeStr: String, moodTypeUrl: String, photoTypeStr: String,getTotalLikeCount: String, getTotalReplyCount: String,gifHeight: Int, gifWidth: Int, postStatusStr: String) {
        
        let actionSheet = UIAlertController(title: "Please choose your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Edit", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let createConfession = storyboard.instantiateViewController(withIdentifier: "WiCreateConfessionViewController") as! WiCreateConfessionViewController
            createConfession.updateStr = "EditConfession"
            createConfession.updateMoodUrl = moodTypeUrl
            createConfession.updateTimeframe = timeFrameStr
            createConfession.updatePhotoTypeStr = photoTypeStr
            createConfession.updateStatusText = status
            createConfession.updateBackgroundImageUrl = backgroudImageStr
            createConfession.updateMoodTypeStr = moodTypeStr
            createConfession.updatePostID = postIdStr
            createConfession.postTypeStr = "CreateNewPost"
            createConfession.updateReplyToReplyID = ""
            createConfession.updateLikeCountStr = getTotalLikeCount
            createConfession.updateReplyCountStr = getTotalReplyCount
            createConfession.updateGifWidth = gifWidth
            createConfession.updateGifHeight = gifHeight
            self.navigationController?.pushViewController(createConfession, animated: true)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.deleteConfessionFromDatabase(postIDStr: postIdStr, replyID: "", replyOnReplyIDStr: "")
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    //MARK:- ****** confessionLikeBtnAction ******
    @IBAction func confessionLikeBtnAction(_ sender: UIButton) {
        
        self.getReloadIndexPath = self.indexToClick
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.myPostsArray.count > 0)
            {
                self.likeDislikeConfessions(confessionID: self.myPostsArray[self.indexToClick].postIDStr, completion: { (responseBool) in
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected = IndexPath()
                        self.indexPathSelected = IndexPath(row: self.indexToClick, section: 0)
                        
                        self.latestIntArr.add(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if (self.myPostsArray[self.indexToClick].postedImage != "")
                        {
                            let imageURL = self.myPostsArray[self.indexToClick].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.myPosts_tableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.myPostsArray[self.indexPathSelected.row].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text  = ""
                                    
                                    self.myPostsArray[self.indexToClick].likedByMeStatus = "yes"
                                    self.myPostsArray[self.indexToClick].likesCount = "1"
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikeLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.myPostsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.myPostsArray[self.indexToClick].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.myPostsArray[self.indexPathSelected.row].likesCount)! + 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.myPostsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.myPostsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.myPosts_tableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.myPostsArray[self.indexPathSelected.row].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.myPostsArray[self.indexToClick].likedByMeStatus = "yes"
                                    self.myPostsArray[self.indexToClick].likesCount = "1"
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikeLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.myPostsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.myPostsArray[self.indexToClick].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.myPostsArray[self.indexPathSelected.row].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.myPostsArray[self.indexToClick].likedByMeStatus = "yes"
                                        self.myPostsArray[self.indexToClick].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
                        
                        self.indexPathSelected = IndexPath()
                        self.indexPathSelected = IndexPath(row: self.indexToClick, section: 0)
                        
                        self.latestIntArr.remove(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if (self.myPostsArray[self.indexToClick].postedImage != "")
                        {
                            let imageURL = self.myPostsArray[self.indexToClick].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.myPosts_tableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.myPostsArray[self.indexPathSelected.row].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text = ""//String(totalLikes)
                                    
                                    self.myPostsArray[self.indexToClick].likedByMeStatus = "no"
                                    self.myPostsArray[self.indexToClick].likesCount = ""
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikeLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        onlineUsersCell.likeCountLbl.text = "" //String(totalLikes)
                                        
                                        self.myPostsArray[self.indexToClick].likedByMeStatus = "no"
                                        self.myPostsArray[self.indexToClick].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikeLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.myPostsArray[self.indexPathSelected.row].likesCount)! - 1
                                        
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            onlineUsersCell.likeCountLbl.text  = ""
                                            
                                            self.myPostsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.myPostsArray[self.indexToClick].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = ""
                                        }
                                        else
                                        {
                                            self.myPostsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.myPostsArray[self.indexToClick].likesCount = String(totalLikes)
                                            
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.myPosts_tableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.myPostsArray[self.indexPathSelected.row].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    creditsCell.totalLikesCountLbl.text = ""
                                    
                                    self.myPostsArray[self.indexToClick].likedByMeStatus = "no"
                                    self.myPostsArray[self.indexToClick].likesCount = ""
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikeLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                        
                                        self.myPostsArray[self.indexToClick].likedByMeStatus = "no"
                                        self.myPostsArray[self.indexToClick].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikeLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.myPostsArray[self.indexPathSelected.row].likesCount)! - 1
                                        
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            creditsCell.totalLikesCountLbl.text = ""
                                            
                                            self.myPostsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.myPostsArray[self.indexToClick].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = ""
                                        }
                                        else
                                        {
                                            self.myPostsArray[self.indexToClick].likedByMeStatus = "no"
                                            self.myPostsArray[self.indexToClick].likesCount = String(totalLikes)
                                            
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
    
    func likeDislikeConfessions(confessionID: String, completion: @escaping (String) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.likeUnlikeConfessions(userID: userID, userToken: authToken, confession_id: confessionID, completion: { (responseData) in
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            completion(message!)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(message!)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion("Error Occurs.")
                        }
                        
                    })
                }
            }
        }
    }
    
    //MARK:- ****** confessionReplyBtnAction ******
    @IBAction func confessionReplyBtnAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
        homeScreen.checkReplyType = "replyOnPost"
        homeScreen.popInfoDelegate = self
        if appDelegatePush == "fromAppDelegate"
        {
           if self.postID != ""
           {
            self.loadingView.isHidden = false
            self.view.isUserInteractionEnabled = false
            
            let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                homeScreen.postIdOwnerStr = self.ownerIdForConfession
                homeScreen.postIDStr = self.postID
                self.navigationController?.pushViewController(homeScreen, animated: true)
            }
          }
        }
        else{
            var dictForConfessions = [String: String]()
            dictForConfessions=["age": self.myPostsArray[self.indexToClick].age,
                                "gender": self.myPostsArray[self.indexToClick].gender,
                                "username": self.myPostsArray[self.indexToClick].name,
                                "location": self.myPostsArray[self.indexToClick].location,
                                "totalTime": String(self.myPostsArray[self.indexToClick].totalTime),
                                "typeOfConfession": self.myPostsArray[self.indexToClick].typeOfConfession,
                                "ownerProfilePic": self.myPostsArray[self.indexToClick].profilePic,
                                "statusAdded": self.myPostsArray[self.indexToClick].statusAdded
            ]
            homeScreen.ConfessionsDataDict = dictForConfessions
            homeScreen.postIdOwnerStr = self.myPostsArray[self.indexToClick].userIDStr
            homeScreen.postIDStr = self.myPostsArray[self.indexToClick].postIDStr
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
    }
    
    
    //MARK:- ****** closeFullSizeConfession_btn ******
    @IBAction func closeFullSizeConfession_btn(_ sender: Any) {
        self.viewFullSizePostView.isHidden = true
    }
    
    
    //MARK:- ****** collectionViewContentSize ******
    func collectionViewContentSize() -> CGSize
    {
        let collectionViewWidth = self.view.frame.size.width
        let collectionViewHeight = (self.myPostsArray.count/2)*150
        myPosts_collectionView.contentSize.height = CGFloat(collectionViewHeight)
        return CGSize(width: collectionViewWidth, height: myPosts_collectionView.contentSize.height);
    }
    
    
    func getSingleMediaDetailsFromDatabase(postIDStr: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
   
            if (postIDStr != "")
            {
                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                    {
                        ApiHandler.getSingleMediaDetails(user_id: userID, auth_token: authToken, mediaID: postIDStr, typeOfMedia: "album", completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                if (dataDict != nil)
                                {
                                    let mediaImageStr = dataDict?.value(forKey: "media") as! String
                                    let caption = dataDict?.value(forKey: "caption") as! String
                                    let likedbycurrent = dataDict?.value(forKey: "likedbycurrent") as! String
                                    let total_likes = dataDict?.value(forKey: "total_likes") as! String
                                    let created = dataDict?.value(forKey: "created") as! Int
                                    let type = dataDict?.value(forKey: "type") as! String
                                    let mediaid = dataDict?.value(forKey: "id") as! String
                                    
                                    self.photoDeleteIndex = mediaid
                                    self.mediaCaptionText = caption
                                    self.mediaCaptionType = "1"

                                    if type == "1"
                                    {
                                        if caption == ""
                                        {
                                            self.displaycaption_textView.text = ""
                                            self.displaycaption_textView.isHidden = true
                                            self.editPhotoCaption_lbl.isHidden = false
                                        }
                                        else
                                        {
                                            self.displaycaption_textView.isHidden = false
                                            self.editPhotoCaption_lbl.isHidden = true
                                            self.displaycaption_textView.text = caption
                                        }
                                        
                                        self.fullImage.sd_setImage(with: URL(string: mediaImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                        
                                        self.viewFullImage_view.isHidden=false
                                        
                                       /* self.fullImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                            self.fullImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                        }, completion: { (finished) in
                                        })*/
                                        
                                        self.fullImage.contentMode = UIViewContentMode.scaleAspectFill
                                        self.fullImage.clipsToBounds = true
                                        
                                        let likesCountStr = total_likes
                                        if (likesCountStr == "0")
                                        {
                                            self.imageLikeCount_lbl.text = ""
                                        }
                                        else
                                        {
                                            if (likesCountStr == "")
                                            {
                                                self.imageLikeCount_lbl.text = ""
                                            }
                                            else
                                            {
                                                self.imageLikeCount_lbl.text = String(likesCountStr)
                                            }
                                        }
                                        
                                        let timeStampInt = created
                                        let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampInt))
                                        let dateString = self.timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
                                        
                                        if dateString == ""
                                        {
                                            self.displayTime_lbl.text = ""
                                        }
                                        else
                                        {
                                            self.displayTime_lbl.text = dateString
                                        }
                                    }
                                    else
                                    {
                                        self.playVideoView.isHidden=false
                                        
                                        self.deleteVideoURL = mediaid
                                        self.mediaCaptionText = caption
                                        self.mediaCaptionType = "2"
                                        
                                        if caption == ""
                                        {
                                            self.displayVideoCaption_txtView.clipsToBounds = true
                                            self.displayVideoCaption_txtView.isHidden = true
                                            self.editVideoCaption_lbl.isHidden = false
                                            self.displayVideoCaption_txtView.text = ""
                                        }
                                        else
                                        {
                                            self.displayVideoCaption_txtView.clipsToBounds = true
                                            self.displayVideoCaption_txtView.isHidden = false
                                            self.editVideoCaption_lbl.isHidden = true
                                            self.displayVideoCaption_txtView.text = caption
                                        }
                                        
                                        let likesCountStr = total_likes
                                        if (likesCountStr == "0")
                                        {
                                            self.videoLikeCount_lbl.text = ""
                                        }
                                        else
                                        {
                                            if (likesCountStr == "")
                                            {
                                                self.videoLikeCount_lbl.text = ""
                                            }
                                            else
                                            {
                                                self.videoLikeCount_lbl.text =  String(likesCountStr)
                                            }
                                        }
                                        
                                        let timeStampInt = created
                                        let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampInt))
                                        let dateString = self.timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
                                        
                                        if dateString == ""
                                        {
                                           self.displayVideoTime_lbl.text = ""
                                        }
                                        else
                                        {
                                            self.displayVideoTime_lbl.text = dateString
                                        }
                                        
                                        let videoURL1 = URL(string: mediaImageStr)
                                        self.player = AVPlayer(url: videoURL1!)
                                        self.playerViewController.player = self.player
                                        do {
                                            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                                            try AVAudioSession.sharedInstance().setActive(true)
                                        } catch {
                                            print(error)
                                        }
                                        
                                        // using custom view for AVPlayerController()
                                        if UIScreen.main.bounds.size.height == 568{
                                            self.playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(50), width: CGFloat(self.view.frame.size.width), height: CGFloat(250))
                                            self.displayLikeView.frame.origin.y = self.playerViewController.view.frame.origin.y + self.playerViewController.view.frame.size.height + 10
                                            self.displayVideoCaption_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                                            self.editVideoCaption_lbl.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                                            self.edit_lblVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                                            self.edit_btnVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                                        }
                                        else{
                                            self.playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(60), width: CGFloat(self.view.frame.size.width), height: CGFloat(350))
                                            self.displayLikeView.frame.origin.y = self.playerViewController.view.frame.origin.y + self.playerViewController.view.frame.size.height + 10
                                            self.displayVideoCaption_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                                            self.editVideoCaption_lbl.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                                            self.edit_lblVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                                            self.edit_btnVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                                        }
                                        self.addChildViewController(self.playerViewController)
                                        self.playVideoView.addSubview(self.playerViewController.view)
                                        self.playerViewController.didMove(toParentViewController: self)
                                        self.playerViewController.player!.play()
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }
    }
    
    func getSingleConfessionDetailsFromDatabase(confessionID: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (confessionID != "")
            {
                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                    {
                        ApiHandler.getSingleMediaDetails(user_id: userID, auth_token: authToken, mediaID: confessionID, typeOfMedia: "confession", completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let userData = responseData.value(forKey: "data") as! NSDictionary
                                
                                if (userData != nil)
                                {
                                    let timestamp: String = userData.value(forKey: "created") as! String
                                    var loc = String()
                                    var age = String()
                                    var gender = String()
                                    var username = String()
                                    var userPic = String()
                                    var checkUserExists = String()
                                    
                                    let userID: String = userData.value(forKey: "user_id") as! String
                                    let postedImage: String = userData.value(forKey: "image") as! String
                                    
                                    let likeCountInt: Int = userData.value(forKey: "likes_count") as! Int
                                    let likeCountStr : String = String(likeCountInt)
                                    let likedby_current: String = userData.value(forKey: "likedby_current") as! String
                                    let replyCountStr : Int = userData.value(forKey: "comments_count") as! Int
                                   /* let total_duration: String = userData.value(forKey: "total_duration") as! String
                                    let statusAddedStr: String = userData.value(forKey: "text") as! String
                                    let postID: String = userData.value(forKey: "id") as! String
                                    let mood: String = userData.value(forKey: "mood") as! String
                                    let duration: String = userData.value(forKey: "duration") as! String*/
                                    
                                    let image_height: String = userData.value(forKey: "image_height") as! String
                                    let image_width: String = userData.value(forKey: "image_width") as! String
                                    
                                    var image_height_int = Int()
                                    var image_width_int = Int()
                                    
                                    if (image_height == "")
                                    {
                                        image_height_int = 250
                                    }
                                    else{
                                        image_height_int = Int(image_height)!
                                    }
                                    
                                    if (image_width == "")
                                    {
                                        image_width_int = 250
                                    }
                                    else{
                                        image_width_int = Int(image_width)!
                                    }
                                    
                                    var usersDetailsDict = NSDictionary()
                                    if (userData["user"] != nil)
                                    {
                                        usersDetailsDict = (userData.value(forKey: "user") as? NSDictionary)!
                                        
                                        checkUserExists = usersDetailsDict.value(forKey: "found") as! String
                                        
                                        if (checkUserExists == "no")
                                        {
                                            return
                                        }
                                        
                                        age = String(usersDetailsDict.value(forKey: "Age") as! Int)
                                        loc = usersDetailsDict.value(forKey: "Location") as! String
                                        gender = usersDetailsDict.value(forKey: "gender") as! String
                                        
                                        username = usersDetailsDict.value(forKey: "username") as! String
                                        userPic = usersDetailsDict.value(forKey: "profile_pic") as! String
                                    }
                                    else
                                    {
                                        age = ""
                                        loc = ""
                                        gender = ""
                                        username = ""
                                        userPic = ""
                                    }
                                    
                                   
                                    if (postedImage != "")
                                    {
                                        let imageURL = postedImage
                                        if (imageURL.contains("jpg"))
                                        {
                                            self.fullSizeImageConfession.sd_setImage(with: URL(string: postedImage), placeholderImage: UIImage(named: "Default Icon"), options: []) { (image, error, imageCacheType, imageUrl) in

                                                if ((image?.size.height) != nil) && ((image?.size.width) != nil)
                                                {
                                                    self.viewFullSizePostView.backgroundColor = UIColor.clear
                                                    self.viewFullSizePostView.isHidden = false
                                                    self.fullSizeImageConfession.backgroundColor = UIColor.clear
                                                    
                                                    self.fullSizeImageConfession.isHidden = false
                                                    self.fullSizeAnimatedImageConfession.isHidden = true
                                                    
                                                    if (image?.size.height)! <= (self.postGifWrapperView.frame.size.height)
                                                    {
                                                        var gifWidth = Int()
                                                        var screenRes = Int()
                                                        var newXPos =  Int()
                                                        
                                                        if (image?.size.width)! > (self.view.frame.size.width)
                                                        {
                                                            gifWidth = Int((image?.size.width)!/2)
                                                            screenRes = Int(self.view.frame.width/2)
                                                            newXPos =  0
                                                        }
                                                        else{
                                                            gifWidth = Int((image?.size.width)!/2)
                                                            screenRes = Int(self.view.frame.width/2)
                                                            newXPos =  screenRes - gifWidth
                                                        }
                                                        
                                                        let gifHeight = Int((image?.size.height)!/2)
                                                        let heightOfWrraperView = Int(self.postGifWrapperView.frame.height/2)
                                                        let newYPos: Int =  heightOfWrraperView - gifHeight
                                                        
                                                        self.postGifWrapperView.frame.origin.y = 0
                                                        self.fullSizeImageConfession.frame.origin.y = CGFloat(newYPos)
                                                        self.fullSizeImageConfession.frame.origin.x = CGFloat(newXPos)
                                                        
                                                        if (image?.size.width)! > (self.view.frame.size.width)
                                                        {
                                                            self.fullSizeImageConfession.frame.size.width = self.view.frame.size.width
                                                        }
                                                        else{
                                                            self.fullSizeImageConfession.frame.size.width = (image?.size.width)!
                                                        }
                                                        
                                                        if (image?.size.height)! <= (self.postGifWrapperView.frame.size.height)
                                                        {
                                                            self.fullSizeImageConfession.frame.size.height = (image?.size.height)!
                                                        }
                                                        else{
                                                            self.fullSizeImageConfession.frame.size.height = self.postGifWrapperView.frame.size.height
                                                        }
                                                        
                                                        self.imagesButtonWrapperView.frame.origin.y = self.fullSizeImageConfession.frame.origin.y + self.fullSizeImageConfession.frame.size.height + 10
                                                        self.postGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                                                    }
                                                    else{
                                                        self.fullSizeImageConfession.frame.origin.y = 21
                                                        self.fullSizeImageConfession.frame.size.height = (self.postGifWrapperView.frame.size.height - 21)
                                                        
                                                        self.imagesButtonWrapperView.frame.origin.y = self.postGifWrapperView.frame.origin.y + self.postGifWrapperView.frame.size.height + 10
                                                        self.postGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                                                    }
                                                    
                                                    self.fullSizeImageConfession.layer.borderWidth = 2.0
                                                    self.fullSizeImageConfession.layer.masksToBounds = false
                                                    self.fullSizeImageConfession.layer.borderColor = UIColor.lightGray.cgColor
                                                    self.fullSizeImageConfession.contentMode = UIViewContentMode.scaleAspectFit
                                                }
                                                else
                                                {
                                                    let alert = UIAlertController(title: "", message: "Please wait.", preferredStyle: UIAlertControllerStyle.alert)
                                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                                    self.present(alert, animated: true, completion: nil)
                                                }
                                            }
                                        }
                                        else
                                        {
                                            self.viewFullSizePostView.backgroundColor = UIColor.clear
                                            self.viewFullSizePostView.isHidden = false
                                            self.fullSizeImageConfession.backgroundColor = UIColor.clear
                                            
                                            self.fullSizeImageConfession.isHidden = true
                                            self.fullSizeAnimatedImageConfession.isHidden = false
                                            
                                            DispatchQueue.main.async {
                                                self.fullSizeAnimatedImageConfession.sd_setImage(with: URL(string: postedImage), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                                            }
                                            
                                            self.fullSizeAnimatedImageConfession.layer.borderWidth = 2.0
                                            self.fullSizeAnimatedImageConfession.layer.masksToBounds = false
                                            self.fullSizeAnimatedImageConfession.layer.borderColor = UIColor.lightGray.cgColor
                                            self.fullSizeAnimatedImageConfession.contentMode = UIViewContentMode.scaleAspectFit
                                            
                                            var gifWidth = Int()
                                            var screenRes = Int()
                                            var newXPos =  Int()
                                            if (image_width_int) > Int(self.view.frame.size.width)
                                            {
                                                gifWidth = Int(image_width_int/2)
                                                screenRes = Int(self.view.frame.width/2)
                                                newXPos =  0
                                            }
                                            else{
                                                gifWidth = Int(image_width_int/2)
                                                screenRes = Int(self.view.frame.width/2)
                                                newXPos =  screenRes - gifWidth
                                            }
                                            
                                            let gifHeight = Int(image_height_int/2)
                                            let heightOfWrraperView = Int(self.postGifWrapperView.frame.height/2)
                                            let newYPos: Int =  heightOfWrraperView - gifHeight
                                            
                                            self.postGifWrapperView.frame.origin.y = 0
                                            self.fullSizeAnimatedImageConfession.frame.origin.y = CGFloat(newYPos)
                                            self.fullSizeAnimatedImageConfession.frame.origin.x = CGFloat(newXPos)
                                            
                                            if (image_width_int) > Int(self.view.frame.size.width)
                                            {
                                                self.fullSizeAnimatedImageConfession.frame.size.width = self.view.frame.size.width
                                            }
                                            else{
                                                self.fullSizeAnimatedImageConfession.frame.size.width = CGFloat(image_width_int)
                                            }
                                            
                                            self.fullSizeAnimatedImageConfession.frame.size.height = CGFloat(image_height_int)
                                            
                                            self.imagesButtonWrapperView.frame.origin.y = self.fullSizeAnimatedImageConfession.frame.origin.y + self.fullSizeAnimatedImageConfession.frame.size.height + 10
                                            self.postGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                                        }
                                    }
                                    
                                    DispatchQueue.main.async {
                                      
                                            if (likedby_current == "yes")
                                            {
                                                self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            }
                                            else
                                            {
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            }
                                    }
                                    
                                    if likeCountStr == "0"
                                    {
                                        self.confessionLikeLbl.text = ""
                                    }
                                    else //if self.myPostsArray[sender.tag].likesCount > 0
                                    {
                                        self.confessionLikeLbl.text = likeCountStr
                                    }
                                    
                                    
                                    
                                    if replyCountStr == 0
                                    {
                                        self.confessionReplyLbl.text = ""
                                    }
                                    else if replyCountStr > 0
                                    {
                                        self.confessionReplyLbl.text = String(replyCountStr)
                                    }
                                    else
                                    {
                                        self.confessionReplyLbl.text = ""
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }
    }
    
 
    //MARK: ****** contentsize method ******
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?){
        if(keyPath == "contentSize"){
            
            if let newvalue = change?[.newKey]{
                
                if self.myPostsArray.count > 0
                {
                    if self.postLikedStr == false
                    {
                        self.myPostsView.isHidden = false
                        self.myPosts_tableView.isHidden = false
                        self.noPostsView.isHidden = true
                        self.noPostsView.frame.size.height = 60
                  
                        var newsize  = newvalue as! CGSize
                        if (newsize.height < 1000)
                        {
                            newsize.height = 1000
                        }
                        
                        self.myPostsView.frame.size.height = newsize.height
                        self.myPosts_tableView.frame.size.height = newsize.height
                        
                        self.wrapperScrollView.isScrollEnabled = true
                        self.wrapperScrollView.contentSize.height = self.myPostsView.frame.origin.y + newsize.height
                    }
                }
                else{
                    
                    if self.cameFromHome == "cameFromHome" {
                        self.wrapperScrollView.isScrollEnabled = true
                        self.about_wrapperScrollView.isScrollEnabled = false
                        self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                        
                        self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height + 50
                        
                        self.noPostsView.frame.size.height = 60
                        
                        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                        self.aboutBtn.setTitleColor(UIColor.black, for: .normal)
                        self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                        self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                        
                        self.aboutMeTop.setTitleColor(UIColor.black, for: .normal)
                        self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
                        self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
                    }
                    else{
                        self.myPostsView.frame =  CGRect(x: self.myPostsView.frame.origin.x , y: self.myPostsView.frame.origin.y, width: self.myPostsView.frame.size.width, height: self.myPostsView.frame.size.height)
                        
                        self.myPosts_tableView.frame =  CGRect(x: self.myPosts_tableView.frame.origin.x , y:0, width: self.myPosts_tableView.frame.size.width, height: self.myPostsView.frame.size.height)
                        
                        self.noPostsView.frame.size.height = 60
                        
                        self.wrapperScrollView.isScrollEnabled = true
                        self.wrapperScrollView.contentSize.height = self.myPosts_tableView.frame.origin.y + self.myPosts_tableView.frame.height
                    }
                }
            }
        }
    }
    
    
    // MARK: - ****** ViewDidDisAppear ******
    override func viewDidDisappear(_ animated: Bool)
    {
        self.isClickedPostsbtn = false
        
        //prab
        self.myPosts_tableView.removeObserver(self, forKeyPath: "contentSize")
    
        self.getPostBool = false
    }
    
    
    // MARK: - ****** Drag Chat Button Action. ******
    func wasDraggedBtn(_ recognizer: UIPanGestureRecognizer)
    {
        let button: UIButton? = (recognizer.view as? UIButton)
        let location: CGPoint = recognizer.translation(in: button)
        
        if (((self.chatBtn?.center.x)! + location.x)  > (self.chatBtn.frame.size.width)/2 && ((self.chatBtn?.center.y)! + location.y) > (self.chatBtn.frame.size.height)/2 && ((self.chatBtn?.center.x)! + location.x)  < (self.view.frame.size.width-(self.chatBtn.frame.size.width)/2) && ((self.chatBtn?.center.y)! + location.y) < (self.view.frame.size.height-(self.chatBtn.frame.size.height)/2))
        {
            self.chatBtn?.center = CGPoint(x: CGFloat((self.chatBtn?.center.x)! + location.x), y: CGFloat((self.chatBtn?.center.y)! + location.y))
        }
        recognizer.setTranslation(CGPoint.zero, in: button)
    }
    
    
    // MARK: - ****** Drag Chat Button Action. ******
    func wasDragged(_ button: UIButton, with event: UIEvent) {
        // get the touch
        let touch: UITouch? = event.touches(for: self.chatBtn)?.first
        // get delta
        
        let location: CGPoint? = touch?.location(in: self.view)
  
        // move button
        if ((location?.x)!  > (self.chatBtn.frame.size.width)/2 && (location?.y)! > (self.chatBtn.frame.size.height)/2 && (location?.x)!  < (self.view.frame.size.width-(self.chatBtn.frame.size.width)/2) && (location?.y)! < (self.view.frame.size.height-(self.chatBtn.frame.size.height)/2))
        {
            button.center = CGPoint(x: CGFloat((location?.x)!), y: CGFloat((location?.y)!))
        }
    }
    
    
        // MARK: - ****** ViewWillAppear. ******
       override func viewWillAppear(_ animated: Bool) {
        
        self.myPosts_tableView.isScrollEnabled = false
        self.ImagesActivityIndicator.isHidden = true
        self.displaycaption_textView.clipsToBounds = true
        self.displayVideoCaption_txtView.clipsToBounds = true
        
        //prabh
        self.myPosts_tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)

        UIApplication.shared.isStatusBarHidden=false
        self.navigationController?.isNavigationBarHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        Reach().monitorReachabilityChanges()
        
        self.totalInterestsSaved = []
        self.interestsArray = []
        self.showInterestsArray = []
        
        if self.appDelegatePush == "fromHomePage"
        {
            if self.typeOfAlbumsPush == "Pictures"
            {
                self.about_wrapperScrollView.isHidden = true
                self.photosCollectionView.isHidden = false
                self.videosView.isHidden = true
                self.myPostsView.isHidden = true
                self.myPosts_collectionView.isHidden = true
                self.noDataFoundLbl.isHidden = true
                
                self.tappedBtnStr = "albumBtn"
                
                self.albumBtn.setTitleColor(UIColor.black, for: .normal)
                self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
                
                self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
                self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
            }
            else if self.typeOfAlbumsPush == "Videos"
            {
                self.about_wrapperScrollView.isHidden = true
                self.photosCollectionView.isHidden = false
                self.videosView.isHidden = true
                self.myPostsView.isHidden = true
                self.myPosts_collectionView.isHidden = true
                self.noDataFoundLbl.isHidden = true
                
                self.tappedBtnStr = "albumBtn"
                
                self.albumBtn.setTitleColor(UIColor.black, for: .normal)
                self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
                
                self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
                self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
            }
            else if (self.typeOfAlbumsPush == "ConfessionLikes") || (self.typeOfAlbumsPush == "Comments")
            {
                if (self.typeOfPostStr == "photo/gif")
                {
                    self.getFullDataStr = "fetchAllData"
                    self.postLikedStr = false
                    
                    self.wrapperScrollView.isScrollEnabled = true
                    self.wrapperScrollView.contentSize.height = self.myPosts_tableView.frame.origin.y + self.myPosts_tableView.frame.size.height
                    
                    self.cameFromHome = "postsClicked"
                    self.myPosts_collectionView.isHidden = true
                    self.about_wrapperScrollView.isHidden = true
                    self.myPostsView.isHidden = false
                    self.videosView.isHidden = true
                    self.photosCollectionView.isHidden = true
                    
                    self.tappedBtnStr = "postsBtn"
                    
                    self.postsBtn.setTitleColor(UIColor.black, for: .normal)
                    self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
                    self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                    self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                    self.albumBtn.setTitleColor(UIColor.gray, for: .normal)
                    
                    self.postsBtnTop.setTitleColor(UIColor.black, for: .normal)
                    self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                    self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
                    
                    self.viewFullSizePostView.backgroundColor = UIColor.clear
                    self.viewFullSizePostView.isHidden = false
                    self.fullSizeImageConfession.backgroundColor = UIColor.clear
                }
                else{
                    self.viewFullSizePostView.isHidden = true
                }
            }
            else if (self.typeOfAlbumsPush == "backFromReplies")
            {
                self.isClickedPostsbtn = false
                if (self.typeOfPostStr == "photo/gif")
                {
                    self.getFullDataStr = "fetchAllData"
                    self.postLikedStr = false
                    
                    self.wrapperScrollView.isScrollEnabled = true
                    self.wrapperScrollView.contentSize.height = self.myPosts_tableView.frame.origin.y + self.myPosts_tableView.frame.size.height
                    
                    self.cameFromHome = "postsClicked"
                    self.myPosts_collectionView.isHidden = true
                    self.about_wrapperScrollView.isHidden = true
                    self.viewFullSizePostView.isHidden = true
                    self.myPostsView.isHidden = false
                    self.isClickedPostsbtn = false
                    self.videosView.isHidden = true
                    self.photosCollectionView.isHidden = true
                    
                    self.tappedBtnStr = "postsBtn"
                    
                    self.postsBtn.setTitleColor(UIColor.black, for: .normal)
                    self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
                    self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                    self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                    self.albumBtn.setTitleColor(UIColor.gray, for: .normal)
                    
                    self.postsBtnTop.setTitleColor(UIColor.black, for: .normal)
                    self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                    self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
                }
            }
            else{
                self.viewAlbumBool = false
                
                self.myPostsView.isHidden = true
                self.about_wrapperScrollView.isHidden = false
                self.interestsTxtView.isHidden = false
                self.videosView.isHidden = true
                self.noDataFoundLbl.isHidden = true
                self.photosCollectionView.isHidden = true
                self.myPosts_collectionView.isHidden = true
                self.viewFullSizePostView.isHidden = true
                
                self.tappedBtnStr = "aboutBtn"
                
                self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                self.aboutBtn.setTitleColor(UIColor.black, for: .normal)
                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.albumBtn.setTitleColor(UIColor.gray, for: .normal)
                
                self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
                self.aboutMeTop.setTitleColor(UIColor.black, for: .normal)
                self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
            }
        }
        else if self.appDelegatePush == "fromWiReplies"
        {
            if (self.typeOfPostStr == "photo/gif")
            {
                self.getFullDataStr = "fetchAllData"
                self.postLikedStr = false
                
                self.wrapperScrollView.isScrollEnabled = true
                self.wrapperScrollView.contentSize.height = self.myPosts_tableView.frame.origin.y + self.myPosts_tableView.frame.size.height
                
                self.cameFromHome = "postsClicked"
                self.myPosts_collectionView.isHidden = true
                self.about_wrapperScrollView.isHidden = true
                self.viewFullSizePostView.isHidden = true
                self.myPostsView.isHidden = false
                self.isClickedPostsbtn = false
                self.videosView.isHidden = true
                self.photosCollectionView.isHidden = true
                
                self.tappedBtnStr = "postsBtn"
                
                self.postsBtn.setTitleColor(UIColor.black, for: .normal)
                self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.albumBtn.setTitleColor(UIColor.gray, for: .normal)
                
                self.postsBtnTop.setTitleColor(UIColor.black, for: .normal)
                self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
            }
        }
        else
        {
            if self.viewAlbumBool == true
            {
                self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.albumBtn.setTitleColor(UIColor.black, for: .normal)
                
                self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
                self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
                
                self.tappedBtnStr = "albumBtn"
                
                self.about_wrapperScrollView.isHidden = true
                self.photosCollectionView.isHidden = false
                self.videosView.isHidden = true
                self.myPostsView.isHidden = true
                self.myPosts_collectionView.isHidden = true
                self.viewFullSizePostView.isHidden = true
                self.viewAlbumBool = false
            }
            else{
                self.viewAlbumBool = false
                
                self.myPostsView.isHidden = true
                self.about_wrapperScrollView.isHidden = false
                self.interestsTxtView.isHidden = false
                self.videosView.isHidden = true
                self.noDataFoundLbl.isHidden = true
                self.photosCollectionView.isHidden = true
                self.myPosts_collectionView.isHidden = true
                self.viewFullSizePostView.isHidden = true
                
                self.tappedBtnStr = "aboutBtn"
                
                self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                self.aboutBtn.setTitleColor(UIColor.black, for: .normal)
                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                self.albumBtn.setTitleColor(UIColor.gray, for: .normal)
                
                self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
                self.aboutMeTop.setTitleColor(UIColor.black, for: .normal)
                self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
            }
        }
        
        self.wrapperScrollView.isScrollEnabled = true
        self.about_wrapperScrollView.isScrollEnabled = false
        self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
        self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height + 50
    }
  
    // MARK: - ****** Close viewing particular post btn action. ******
    @IBAction func closeMyPostParticularView(_ sender: Any) {
        self.viewParticularPostView.isHidden = true
    }
    
    
    // MARK: - ****** Like my own post btn action. ******
    @IBAction func LikeMyPost_btnAction(_ sender: Any) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WiFi")
        }
    }

    
    // MARK: - ****** Reply on my own post btn action. ******
    @IBAction func replyMyPost_BtnAction(_ sender: Any) {
        print("replyMyPost_BtnAction")
    }
    
    
    
    // MARK: - ****** Get Network Status Reachibility Method ******
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        let getNetworkStatus = (userInfo?["Status"] as? String)!
        if getNetworkStatus == "Offline"
        {

        }
    }
    
    
    // MARK: - ****** Fetch Countries and States From DB. ******
    func populateCustomers() {
        let db = FMDBDataAccess()
        self.countryNameArray = db.getCustomers()
        var country = String()
        var countryID = String()
        var countryObj = Country()
        for i in 0..<self.countryNameArray.count {
            countryObj = self.countryNameArray[i] as! Country
            country = "\(countryObj.firstName!)"
            countryID = "\(countryObj.customerId)"
            testArray.add(country)
            CountryIDArray.add(countryID)
        }
        
        for i in 0..<testArray.count {
            var dict1 = [AnyHashable: Any]()
            dict1["c_name"] = testArray[i]
            dict1["c_id"] = CountryIDArray[i]
            let country_id: String = "\(CountryIDArray[i])"
            if !nameIDArray.contains(country_id) {
                nameIDArray.insert(dict1, at: i)
            }
        }
    }
    
    
    //MARK:- ****** playerDidFinishPlaying ******
    func playerDidFinishPlaying(){
        //now use seek to make current playback time to the specified time in this case (O)
        let duration : Int64 = 0 //(can be Int32 also)
        let preferredTimeScale : Int32 = 1
        let seekTime : CMTime = CMTimeMake(duration, preferredTimeScale)
        self.player?.seek(to: seekTime)
        self.player?.play()
    }
    
    // MARK: - ****** Scroll Images Automatically. ******
    func moveToNextPage (){
        let pageWidth:CGFloat = self.imagesScrollView.frame.width
        
        let totalCount: Int = self.profileImagesArray.count
        let maxWidth:CGFloat = pageWidth * CGFloat(totalCount)
        let contentOffset:CGFloat = self.imagesScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        
        self.imagesScrollView.scrollRectToVisible(CGRect(x:slideToX, y:self.imagesScrollView.frame.origin.y, width:pageWidth, height:self.imagesScrollView.frame.height), animated: true)
    }
    
    
    //MARK: ****** UIScrollView Delegate ******
      func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
}

    
    // MARK: - ****** UICollectionView Delegates and Datasource Methods. ******
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.myPosts_collectionView
        {
            return self.myPostsArray.count
        }
        else if collectionView == self.repliesCollectionView
        {
            return self.allRepliesArray.count
        }
        else{
            return self.albumArray.count
        }
    }
    

     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if  collectionView == self.myPosts_collectionView
        {
            let favouriteSearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "favouriteSearchCell", for: indexPath as IndexPath) as! FavoriteListPeopleCollectionViewCell
       
            return favouriteSearchCell
        }
        else if collectionView == self.repliesCollectionView
        {
            let favouriteSearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "favouriteSearchCell", for: indexPath as IndexPath) as! FavoriteListPeopleCollectionViewCell
        
            return favouriteSearchCell
        }
        else
        {
            // get a reference to our storyboard cell
            let photosCell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath as IndexPath) as! PhotosCollectionViewCell
            
            photosCell.clearCellData()
            
            photosCell.deletePhotoBtn.tag = indexPath.row
            photosCell.deletePhotoBtn.addTarget(self, action: #selector(ProfileViewController.DeletePhotoBtn(_:)), for: .touchUpInside)
            photosCell.layer.borderColor = UIColor.white.cgColor
            photosCell.layer.borderWidth = 1
            
           if ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "type") as? String == "1"
            {
                photosCell.videoPlayIcon.isHidden = true
                
                if ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") != nil
                {
                        let urlStr = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") as? String)!
                        DispatchQueue.main.async {
                            photosCell.photoImageView.sd_setImage(with: URL(string:urlStr), placeholderImage: UIImage(named: "Default Icon"))
                        }
                }
             
                photosCell.photoImageView.contentMode = UIViewContentMode.scaleAspectFill
            }
            else if ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "type") as? String == "2"
            {
                photosCell.videoPlayIcon.isHidden = false
                
                if ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") != nil
                {
//                    DispatchQueue.global(qos: .default).async{
                        let videoStr = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") as? String)!
                        self.videoUrlAtIndex = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") as? String)!
                        DispatchQueue.main.async {
                            photosCell.photoImageView.image = self.createThumbnailOfVideoFromFileURL(videoStr)
                        }
//                    }
                }

                photosCell.photoImageView.contentMode = UIViewContentMode.scaleAspectFill
            }
            
            return photosCell
        }
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == self.photosCollectionView{
            self.viewAlbumBool = true
            if ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "type") as? String == "1"
            {
                photoDeleteIndex = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "id") as? String)!
                self.mediaCaptionText = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String)!
                self.mediaCaptionType = "1"
  
                let captionStr =  ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String
                if captionStr == ""
                {
                    self.displaycaption_textView.clipsToBounds = true
                    self.displaycaption_textView.text = ""
                    self.displaycaption_textView.isHidden = true
                    self.editPhotoCaption_lbl.isHidden = false
                }
                else
                {
                    self.displaycaption_textView.clipsToBounds = true
                    self.displaycaption_textView.isHidden = false
                    self.editPhotoCaption_lbl.isHidden = true
                    self.displaycaption_textView.text = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String
                }
                
                let likesCountStr = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "total_likes") as? String)!
                if (likesCountStr == "0")
                {
                    self.imageLikeCount_lbl.text = ""
                }
                else
                {
                    if (likesCountStr == "")
                    {
                        self.imageLikeCount_lbl.text = ""
                    }
                    else
                    {
                        self.imageLikeCount_lbl.text = String(likesCountStr)
                    }
                }
                
                self.displaycaption_textView.isScrollEnabled = false
                
                let fixedWidth = self.displaycaption_textView.frame.size.width
                self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize = self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame = self.displaycaption_textView.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                self.displaycaption_textView.frame = newFrame;
                
                self.fullImage.frame.size.width = self.view.frame.size.width
                self.fullImage.frame.size.height = self.view.frame.size.width
                self.fullImageViewBtn.frame.size.width = self.view.frame.size.width
                self.fullImageViewBtn.frame.size.height = self.view.frame.size.width
                self.fullImage.contentMode = UIViewContentMode.scaleAspectFit
                
                self.wrapperViewBtns.frame.origin.y = self.fullImage.frame.origin.y +  self.fullImage.frame.size.height + 20
                self.displaycaption_textView.frame.origin.y = self.wrapperViewBtns.frame.origin.y +  self.wrapperViewBtns.frame.size.height + 10
                self.editPhotoCaption_lbl.frame.origin.y = self.wrapperViewBtns.frame.origin.y +  self.wrapperViewBtns.frame.size.height + 10
                self.editPhotoUploadedBtn.frame.origin.y = self.wrapperViewBtns.frame.origin.y + self.wrapperViewBtns.frame.size.height + 10
                self.editPhotoUploadedLbl.frame.origin.y = self.wrapperViewBtns.frame.origin.y + self.wrapperViewBtns.frame.size.height + 10
                
                self.crossViewPhotoAlbum.frame.origin.y = self.displaycaption_textView.frame.origin.y + self.displaycaption_textView.frame.height + 20
                
                self.wrapperScrollForPhotoAlbum.isScrollEnabled=true
                self.wrapperScrollForPhotoAlbum.contentSize.height = self.crossViewPhotoAlbum.frame.origin.y + self.crossViewPhotoAlbum.frame.size.height + 20
                
                self.wrapperScrollForPhotoAlbum.contentOffset = CGPoint(x:0, y: 0)
                
                self.viewAlbumBool = true
                let photosCell = collectionView.cellForItem(at: indexPath) as! PhotosCollectionViewCell
                self.fullImage.image = photosCell.photoImageView.image
                self.fullImage.contentMode = UIViewContentMode.scaleAspectFill
                self.fullImage.clipsToBounds = true
                self.viewFullImage_view.isHidden=false
                
               /* self.fullImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.fullImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                  }, completion: { (finished) in
                    
                })*/
                
                let timeStampInt = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "created") as! Int
                let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampInt))
                let dateString = timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
            
                if dateString == ""
                {
                    self.displayTime_lbl.text = ""
                }
                else
                {
                    self.displayTime_lbl.text = dateString
                }
            }
            else
            {
                self.deleteVideoURL = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "id") as? String)!
              
                self.mediaCaptionText = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String)!
                self.mediaCaptionType = "2"
                self.playVideoView.isHidden=false
                
                let timeStampInt = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "created") as! Int
                let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampInt))
                let dateString = timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
                
                    if dateString == ""
                    {
                        self.displayVideoTime_lbl.text = ""
                    }
                    else
                    {
                        self.displayVideoTime_lbl.text = dateString
                    }
                
                let captionStr = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String
                if captionStr == ""
                {
                    self.displayVideoCaption_txtView.clipsToBounds = true
                    self.displayVideoCaption_txtView.isHidden = true
                    self.editVideoCaption_lbl.isHidden = false
                    self.displayVideoCaption_txtView.text = ""
                }
                else
                {
                    self.displayVideoCaption_txtView.clipsToBounds = true
                    self.displayVideoCaption_txtView.isHidden = false
                    self.editVideoCaption_lbl.isHidden = true
                    self.displayVideoCaption_txtView.text = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String
                }
                
                let likesCountStr = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "total_likes") as? String)!
                if (likesCountStr == "0")
                {
                    self.videoLikeCount_lbl.text = ""
                }
                else
                {
                    if (likesCountStr == "")
                    {
                        self.videoLikeCount_lbl.text = ""
                    }
                    else
                    {
                        self.videoLikeCount_lbl.text = String(likesCountStr)
                    }
                }
                
                self.displayVideoCaption_txtView.isScrollEnabled = false
                
                let fixedWidth = self.displayVideoCaption_txtView.frame.size.width
                self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize = self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame = self.displayVideoCaption_txtView.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                self.displayVideoCaption_txtView.frame = newFrame;
                
                self.crossViewVideoAlbum.frame.origin.y = self.displayVideoCaption_txtView.frame.origin.y + self.displayVideoCaption_txtView.frame.height + 20
                
                self.wrapperScrollForVideoAlbum.isScrollEnabled=true
                self.wrapperScrollForVideoAlbum.contentSize.height = self.crossViewVideoAlbum.frame.origin.y + self.crossViewVideoAlbum.frame.size.height + 20
                
                self.wrapperScrollForPhotoAlbum.contentOffset = CGPoint(x:0, y: 0)
                
                let videoURL1 = URL(string: (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") as? String)!)
                player = AVPlayer(url: videoURL1!)
                playerViewController.player = player
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                    try AVAudioSession.sharedInstance().setActive(true)
                } catch {
//                    print(error)
                }
                self.viewAlbumBool = true
                // using custom view for AVPlayerController()
                if UIScreen.main.bounds.size.height == 568{
                    playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(50), width: CGFloat(self.view.frame.size.width), height: CGFloat(250))
                    self.displayLikeView.frame.origin.y = playerViewController.view.frame.origin.y + playerViewController.view.frame.size.height + 10
                    self.displayVideoCaption_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                    self.editVideoCaption_lbl.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                    self.edit_lblVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                    self.edit_btnVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                }
                else{
                    playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(60), width: CGFloat(self.view.frame.size.width), height: CGFloat(350))
                    self.displayLikeView.frame.origin.y = playerViewController.view.frame.origin.y + playerViewController.view.frame.size.height + 10
                    self.displayVideoCaption_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                    self.editVideoCaption_lbl.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                    self.edit_lblVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                    self.edit_btnVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                }
                self.addChildViewController(playerViewController)
                self.wrapperScrollForVideoAlbum.addSubview(playerViewController.view)
                playerViewController.didMove(toParentViewController: self)
                playerViewController.player!.play()
            }
        }
        else if collectionView == self.myPosts_collectionView
        {
            let favCell = collectionView.cellForItem(at: indexPath) as! FavoriteListPeopleCollectionViewCell
            
            self.postedImage.image = favCell.feedBackgroundImage.image
            self.timeFrame_lbl.text = favCell.feedTime_lbl.text
            self.postLiked_lbl.text = favCell.feedLikes_lbl.text
            self.postReplied_Lbl.text = favCell.totalReply_Lbl.text
            self.postedImage.contentMode = UIViewContentMode.scaleAspectFill
            self.postedImage.clipsToBounds = true
            
            self.postedStatus_lbl.text = (favCell.feedsStatus_lbl.text)?.uppercased()
            self.postedStatus_lbl.textColor = favCell.feedsStatus_lbl.textColor
            self.postedStatus_lbl.numberOfLines = 100
            self.postedStatus_lbl.adjustsFontSizeToFitWidth = true
          
            self.viewParticularPostView.isHidden=false
           /* self.postedImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.postedImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
               }, completion: { (finished) in
            })*/
            
            self.getPostBool = true
//            self.postID = self.myPostsArray[indexPath.row].postIDStr
            
//            self.fetchAllReplyForParticularFeed(pID: self.myPostsArray[indexPath.row].postIDStr)
//            self.fetchAllUsersLikedPost()
        }
}

    
    
    // MARK: - ****** Generate Thumbnail from video Url. ******
    func createThumbnailOfVideoFromFileURL(_ strVideoURL: String) -> UIImage?{
        let asset = AVAsset(url: URL(string: strVideoURL)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), 100)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            /* error handling here */

            if self.videoUrlAtIndex != ""
            {
//                thumbnailImage = self.createThumbnailOfVideoFromFileURL(self.videoUrlAtIndex)!
                return UIImage(named: "Default Icon") //thumbnailImage //UIImage(named: "Default Icon - 3")
            }
            
            return nil
        }
    } 
    
    
    
    // MARK: - ****** Delete Post from Uploads button action. ******
    func DeleteParticularPostBtn(_ sender: UIButton!)
    {
        
        self.deletePostID = self.myPostsArray[sender.tag].postIDStr
        self.deletePostImageStr = self.myPostsArray[sender.tag].profilePic
        
        let actionSheetController = UIAlertController(title: "", message: "Are you sure you want to delete the post?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelActionButton = UIAlertAction(title: "No", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Yes", style: .default) { action -> Void in
        
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }

    
 
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    // MARK: - ****** Delete Photo from Uploads button action. ******
    func DeletePhotoBtn(_ sender: UIButton!) {
        
        if ((self.albumArray[sender.tag]) as AnyObject).value(forKey: "type") as? String == "1"
        {
            photoDeleteIndex = (((self.albumArray[sender.tag]) as AnyObject).value(forKey: "id") as? String)!
            getURLPhotoStr = (((self.albumArray[sender.tag]) as AnyObject).value(forKey: "type") as? String)!

            if self.profileImagesArray.count >= 2
            {
                let actionSheetController = UIAlertController(title: "", message: "Are you sure that you want to delete it?", preferredStyle: UIAlertControllerStyle.alert)
                let cancelActionButton = UIAlertAction(title: "No", style: .cancel) { action -> Void in
                    
                }
                actionSheetController.addAction(cancelActionButton)
                
                let deleteActionButton = UIAlertAction(title: "Yes", style: .default) { action -> Void in
                    
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                
                        self.loadingView.isHidden = false
                        self.view.isUserInteractionEnabled = false
                        
                        self.deleteMediaAlbum(mediaIDStr: self.photoDeleteIndex, completion: { (responseString) in
                            
                            if (responseString == "Media deleted.")
                            {
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.newImageView.image = UIImage(named: "Default Icon")
                                self.photoUploadView.isHidden = true
                                self.photosCollectionView.reloadData()
                                self.timestampArray = []
                                self.getPhotoKeyArray = []
                                self.videoArray = []
                                self.getPhotoKeyArray = []
                                self.profileImagesArray = []
                                self.albumArray = []
                                self.viewAlbumBool = false
                                self.getUploadedMediaFiles()
                            }
                        })
                    }
                }
                
                actionSheetController.addAction(deleteActionButton)
                 self.present(actionSheetController, animated: true, completion: nil)
            }
            else{
                print("array count is 1 or 0")
                let alert = UIAlertController(title: "", message: "One picture is mandatory for album so you can't delete it. Thanks!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            photoDeleteIndex = (((self.albumArray[sender.tag]) as AnyObject).value(forKey: "id") as? String)!
            getURLPhotoStr = (((self.albumArray[sender.tag]) as AnyObject).value(forKey: "type") as? String)!
            
            let actionSheetController = UIAlertController(title: "", message: "Are you sure that you want to delete it?", preferredStyle: UIAlertControllerStyle.alert)
            let cancelActionButton = UIAlertAction(title: "No", style: .cancel) { action -> Void in
                
            }
            actionSheetController.addAction(cancelActionButton)
            
            let deleteActionButton = UIAlertAction(title: "Yes", style: .default) { action -> Void in
                
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                self.deleteMediaAlbum(mediaIDStr: self.photoDeleteIndex, completion: { (responseString) in
                    
                    if (responseString == "Media deleted.")
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.uploadVideoView.isHidden = true
                        self.likesCountArray = []
                        self.superLikesCountArray = []
                        self.timestampVideoArray = []
                        self.getVideoURLArray = []
                        self.videoArray = []
                        self.thumbNailArray = []
                        self.getPhotoKeyArray = []
                        self.profileImagesArray = []
                        self.albumArray = []
                        self.captionVideoArray = []
                        self.videoTableView.reloadData()
                        self.viewAlbumBool = false
                        self.getUploadedMediaFiles()
                    }
                })
            }
            actionSheetController.addAction(deleteActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
    
    func deleteMediaAlbum(mediaIDStr: String,completion: @escaping (String) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
         
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    let date = NSDate();
                    /// GET TIMESTAMP IN UNIX
                    let currentTimeStamp = Int64(date.timeIntervalSince1970)
                    
                    ApiHandler.deleteProfileAlbum(userID: userID, authToken: authToken, mediaID: mediaIDStr, timestamp: Int(currentTimeStamp), completion: { (responseData) in
                        
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            completion("Media deleted.")
                            
                            self.timestampArray = []
                            self.getPhotoKeyArray = []
                            self.profileImagesArray = []
                            self.albumArray = []
                            self.videoArray = []
                            self.getUploadedMediaFiles()
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            completion("\(String(describing: message))")
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            completion("Error Occurs.")
                        }
                    })
                }
            }
        }
    }
    
    // MARK: - ****** Delete Video from Uploads button Action. ******
    func DeleteVideoBtn(_ sender: UIButton!) {
        deleteVideoURL = self.getVideoURLArray[sender.tag]
        getVideoURLStr = self.videoArray[sender.tag]
    }
    
    // MARK: - ****** Play Particular Video Button Action. ******
    func playVideoBtn(_ sender: UIButton!) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
           
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                try AVAudioSession.sharedInstance().setActive(true)
            } catch {
                print(error)
            }
            
            // using custom view for AVPlayerController()
            if UIScreen.main.bounds.size.height == 568{
                playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(50), width: CGFloat(self.view.frame.size.width), height: CGFloat(250))
                self.displayLikeView.frame.origin.y = playerViewController.view.frame.origin.y + playerViewController.view.frame.size.height + 10
                self.displayVideoCaption_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                 self.editVideoCaption_lbl.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                self.edit_lblVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                self.edit_btnVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
            }
            else{
                playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(60), width: CGFloat(self.view.frame.size.width), height: CGFloat(350))
                self.displayLikeView.frame.origin.y = playerViewController.view.frame.origin.y + playerViewController.view.frame.size.height + 10
                self.displayVideoCaption_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                self.editVideoCaption_lbl.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                self.edit_lblVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                self.edit_btnVideo.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
            }
            self.addChildViewController(playerViewController)
            self.playVideoView.addSubview(playerViewController.view)
            playerViewController.didMove(toParentViewController: self)
            playerViewController.player!.play()

        }
    }
    
    
    // MARK: - ****** Edit My Interests Button Action. ******
    @IBAction func editInterests_btnAction(_ sender: Any) {
        let interestsScreen = self.storyboard?.instantiateViewController(withIdentifier: "AddInterestsViewController") as! AddInterestsViewController
        interestsScreen.checkViewType = "fromProfileScreen"
        interestsScreen.saveInterestsArray = self.getUpdatedInterestsArray as NSMutableArray
        self.navigationController?.pushViewController(interestsScreen, animated: true)
    }
    
    
    // MARK: - ****** Edit Bio Button Action. ******
    @IBAction func editBio_btnAction(_ sender: Any) {
        self.aboutMe_txtView.isEditable = true
        self.aboutMe_txtView.becomeFirstResponder()
    }
    

    // MARK: - ****** Close Full Imageview Button Action. ******
    @IBAction func crossFullImage_btn(_ sender: Any) {
        
        self.viewFullImage_view.isHidden = true
        self.typeOftextView = ""
        self.displaycaption_textView.resignFirstResponder()
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
        self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
        self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
        self.albumBtn.setTitleColor(UIColor.black, for: .normal)
        
        self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
        self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
        
        self.tappedBtnStr = "albumBtn"
        
        self.about_wrapperScrollView.isHidden = true
        self.photosCollectionView.isHidden = false
        self.videosView.isHidden = true
        self.myPostsView.isHidden = true
        self.myPosts_collectionView.isHidden = true
        
        self.viewAlbumBool = true
    }
    
    
    // MARK: - ****** Close Playing Video Button Action. ******
    @IBAction func closePlayVideo_btnAction(_ sender: Any) {
        self.playVideoView.isHidden = true
        self.typeOftextView = ""
        self.displayVideoCaption_txtView.resignFirstResponder()
         playerViewController.player!.pause()
        if let play = player {
            
            play.pause()
            player = nil
            
        } else {
            print("player was already deallocated")
        }
        
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
        self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
        self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
        self.albumBtn.setTitleColor(UIColor.black, for: .normal)
        
        self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
        self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
        
        self.tappedBtnStr = "albumBtn"
        
        self.about_wrapperScrollView.isHidden = true
        self.photosCollectionView.isHidden = false
        self.videosView.isHidden = true
        self.myPostsView.isHidden = true
        self.myPosts_collectionView.isHidden = true
        
        self.viewAlbumBool = true
        
        if self.videoLikedBool == true
        {
            self.likesCountArray = []
            self.superLikesCountArray = []
            self.getVideoURLArray = []
            self.timestampVideoArray = []
            self.videoArray = []
            self.thumbNailArray = []
            self.captionVideoArray = []
            self.getPhotoKeyArray = []
            self.profileImagesArray = []
            self.albumArray = []
            self.videoTableView.reloadData()
            self.videoLikedBool = false
//            self.getUploadedAlbum()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateVideoCountForOtherUsers"), object: nil, userInfo: nil)
        }

    }
    
    
    // MARK: - ****** Back Button Video View Action. ******
    @IBAction func backBtn_videoView(_ sender: Any) {
        self.uploadVideoView.isHidden = true
        self.videoController.stop()
        self.videoController.view.removeFromSuperview()
    }
    
    // MARK: - ****** Cancel Photo Upload Button Action. ******
    @IBAction func cancelPhotoUpload_btnAction(_ sender: Any) {
        self.photoUploadView.isHidden = true
    }
    
    
    // MARK: - ****** Upload Photo Button Action. ******
    @IBAction func photoUpload_btnAction(_ sender: Any) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    var image = UIImage()
                    image = self.rotateImageFix(image: self.addPhotoImageView.image!)
//                    image = ApiHandler.resizeImage(image: self.addPhotoImageView.image!, newWidth: self.view.frame.width)
                    let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
                    let newData = UIImageJPEGRepresentation(UIImage(data: imageData as Data)!, 1)
                    
                    let date = NSDate();
                    /// GET TIMESTAMP IN UNIX
                    let currentTimeStamp = Int64(date.timeIntervalSince1970)
   

                    // create dateFormatter with UTC time format
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                    let dateUTC = dateFormatter.string(from: date as Date)
                    let getUTCDate = dateFormatter.date(from: dateUTC)
//                    let currentTimeStampUTC = Int64((getUTCDate?.timeIntervalSince1970)!)
                    
                    // set upload path
                    let filePath = "\(currentTimeStamp).jpg"
                    
                    
                    if (self.addcaption_txtView.text != "")
                    {
                        var textMessage = String()
                        textMessage = self.addcaption_txtView.text!
                        if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false)
                        {
                            print("self.addcaption_txtView.text! is not empty string")
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "", message: "Please enter a text.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            return
                        }
                    }
                    
                    ApiHandler.addProfileAlbum(userID: userID, authToken: userToken, type: "1", captionAdded: self.addcaption_txtView.text, mediaData: newData!, nameOfFile: filePath, typeOfFile: "image/jpg", timestamp: Int(currentTimeStamp), completion: { (responseData) in
                        
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            
                            let alert = UIAlertController(title: "", message: "Photo uploaded successfully.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                                
                                self.cameFromHome = "cameFromHome"
                                
                                self.tappedBtnStr = "aboutBtn"
                                self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                                self.aboutBtn.setTitleColor(UIColor.black, for: .normal)
                                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                                self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                                
                                self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
                                self.aboutMeTop.setTitleColor(UIColor.black, for: .normal)
                                self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
                                
                                self.photoUploadView.isHidden = true
                                self.photosCollectionView.reloadData()
                                self.timestampArray = []
                                self.getPhotoKeyArray = []
                                self.videoArray = []
                                self.getPhotoKeyArray = []
                                self.profileImagesArray = []
                                self.albumArray = []
                                self.getUploadedMediaFiles()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
            }
        }
    }
    
    
    // MARK: - ****** Rotate Image with Fixed Orientation. ******
    func rotateImageFix(image: UIImage) -> UIImage {
        
        if (image.imageOrientation == UIImageOrientation.up ) {
            return image
        }
        
        UIGraphicsBeginImageContext(image.size)
        
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return copy!
    }
    
    
    // MARK: - ****** Upload Video Button Action. ******
    @IBAction func uploadVideo_btnAction(_ sender: Any) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
         
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false

               
                    var videoData =  Data()
                   /* do {
                        videoData =  try NSData(contentsOfFile: (self.videoURL as NSURL ), options: NSData.ReadingOptions.alwaysMapped) as! Data
                    } catch {
                        return
                    }*/
                    
                    if let video = NSData.init(contentsOf: (self.videoURL as NSURL) as URL) {
                        videoData = video as Data
                    }
                    else{
                         return
                    }
                    
                    let date = NSDate();
                    /// GET TIMESTAMP IN UNIX
                    let currentTimeStamp = Int64(date.timeIntervalSince1970)
                    // set upload path
                    let filePath = "\(currentTimeStamp).mp4"
                    
                    if (self.videoCaption_txtView.text != "")
                    {
                        var textMessage = String()
                        textMessage = self.videoCaption_txtView.text!
                        if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false)
                        {
                            print("self.videoCaption_txtView.text! is not empty string")
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "", message: "Please enter a text.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            return
                        }
                    }
                   
                    
                    ApiHandler.addProfileAlbum(userID: userID, authToken: userToken, type: "2", captionAdded: self.videoCaption_txtView.text, mediaData: videoData, nameOfFile: filePath, typeOfFile: "video/mp4", timestamp: Int(currentTimeStamp), completion: { (responseData) in
                        
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            
                            let alert = UIAlertController(title: "", message: "Video uploaded successfully.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                                self.uploadVideoView.isHidden = true
                                self.cameFromHome = "cameFromHome"
                                self.tappedBtnStr = "aboutBtn"
                                self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                                self.aboutBtn.setTitleColor(UIColor.black, for: .normal)
                                self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
                                self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
                                self.videoUploadbool = true
                                self.likesCountArray = []
                                self.superLikesCountArray = []
                                self.timestampVideoArray = []
                                self.getVideoURLArray = []
                                self.videoArray = []
                                self.thumbNailArray = []
                                self.captionVideoArray = []
                                self.getPhotoKeyArray = []
                                self.profileImagesArray = []
                                self.albumArray = []
                                self.getUploadedMediaFiles()
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
            }
        }
    }

    
    // MARK: - ****** Upload Photo/Video Button Action. ******
    @IBAction func clickProfileImage_btnAction(_ sender: Any) {
        uploadPhotoVideo()
    }
    
    // MARK: - ****** Open Camera Method. ******
    func camera()
    {
        let microPhoneStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio)
        switch microPhoneStatus {
        case .authorized:
            accessVideoCapture()
            break
       case .notDetermined:
             accessVideoCapture()
             break
        // Has access
        case .denied:
                     showAlertForMicrophone()
                      break
        case .restricted:
            showAlertForMicrophone()
            break
       
        }
    }
    
    // MARK: - ****** Capture Video Method. ******
    func accessVideoCapture()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.camera
            if self.useCameraStr == "Upload a photo using camera"
            {
                myPickerController.mediaTypes = [kUTTypeImage as String]
                myPickerController.allowsEditing = false
            }
            else if self.useCameraStr == "Upload a video"
            {
                myPickerController.mediaTypes = [kUTTypeMovie as String]
                myPickerController.allowsEditing = false
                myPickerController.videoMaximumDuration = 60.0
                
            }
            
            self.present(myPickerController, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "", message: "Sorry,Camera is not available to take picture.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - ****** Check Microphone Access Method. ******
    func showAlertForMicrophone()
    {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "\("WI Match") would like to access the microphones to record video.",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Access", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - ****** Open Photo Library Method. ******
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        myPickerController.allowsEditing = false
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** ActionSheet For Choosing upload option. ******
    func uploadPhotoVideo() {
        
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Upload a video", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            if (self.videoArray.count >= 3)
            {
                let alert = UIAlertController(title: "", message: "You cannot upload more than 3 videos.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if #available(iOS 10.0, *) {
                    let homeScreen = storyboard.instantiateViewController(withIdentifier: "CameraOverlayViewController") as! CameraOverlay
                    homeScreen.parentClass = self
                    homeScreen.differentiateStr = "ProfileViewController"
                    homeScreen.differentiateMediaStr = "Upload a video"
                    self.navigationController?.present(homeScreen, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Upload a photo", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if (self.profileImagesArray.count >= 8)
            {
                let alert = UIAlertController(title: "", message: "You cannot upload more than 8 photos.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if #available(iOS 10.0, *) {
                    let homeScreen = storyboard.instantiateViewController(withIdentifier: "CameraOverlayViewController") as! CameraOverlay
                    homeScreen.parentClass = self
                    homeScreen.differentiateStr = "ProfileViewController"
                    homeScreen.differentiateMediaStr = "Upload a photo"
                    self.navigationController?.present(homeScreen, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }

    
    // MARK: - ****** ActionSheet For Video uploading. ******
    func showActionSheet_forVideo() {
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.useCameraStr = "Upload a video"
            
            if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                    DispatchQueue.main.async() {
                        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                        switch authStatus {
                        case .authorized: self.camera() // Do your stuff here i.e. callCameraMethod()
                        case .denied: self.alertToEncourageCameraAccessInitially()
                        case .notDetermined: self.alertPromptToAllowCameraAccessViaSetting()
                        default: self.alertToEncourageCameraAccessInitially()
                        }
                    }
                }
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.useCameraStr = "Upload a video"
            
            let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
            switch authStatus {
            case .authorized: self.videoLibrary() // Do your stuff here i.e. callCameraMethod()
            case .denied: self.alertToEncourageCameraAccessInitially()
            case .notDetermined: self.alertPromptToAllowCameraAccessViaSetting()
            default: self.alertToEncourageCameraAccessInitially()
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    
    // MARK: - ****** Select Video From Library. ******
    func videoLibrary()
    {
        let microPhoneStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio)
        switch microPhoneStatus
        {
        case .authorized:
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = true
            myPickerController.videoMaximumDuration = 60.0
            myPickerController.mediaTypes = [kUTTypeMovie as String]
            self.present(myPickerController, animated: true, completion: nil)
            break
        case .notDetermined:
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            myPickerController.allowsEditing = true
            myPickerController.videoMaximumDuration = 60.0
            myPickerController.mediaTypes = [kUTTypeMovie as String]
            self.present(myPickerController, animated: true, completion: nil)
            break
        // Has access
        case .denied:
            showAlertForMicrophone()
            break
        case .restricted:
            showAlertForMicrophone()
            break
            
        }
    }
    
    
    // MARK: - ****** Access of Camera For Capturing Videos. ******
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "\("WI Match") Would like to access the Camera for capturing videos",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {

            if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                    DispatchQueue.main.async() {
                        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                        switch authStatus {
                        case .authorized: self.videoLibrary() // Do your stuff here i.e. callCameraMethod()
                        case .denied: self.alertToEncourageCameraAccessInitially()
                        case .notDetermined: self.alertPromptToAllowCameraAccessViaSetting()
                        default: self.alertToEncourageCameraAccessInitially()
                        }
                    }
                }
            }
    }
    
    
    
    
    // MARK: - ****** ActionSheet For choosing Photos. ******
    func showActionSheet() {
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.useCameraStr = "Upload a photo using camera"
            
            let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            if status == .denied {
                PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                    let alert = UIAlertController(
                        title: "IMPORTANT",
                        message: "\("WI Match") Would like to capture the Photos",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else if status == .authorized  || status == .notDetermined
            {
                self.camera()
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.useCameraStr = "Upload a photo using gallery"
            
            let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            if  status == .denied {
                PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                    let alert = UIAlertController(
                        title: "IMPORTANT",
                        message: "\("WI Match") Would like to access the Photos",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else if status == .authorized || status == .notDetermined
            {
                 self.photoLibrary()
            }

           
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    
    // MARK: - ****** UIImagePickerView Delegates. ******
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
       selectImageBool = true
        if self.useCameraStr == "Upload a photo using camera" || self.useCameraStr == "Upload a photo using gallery"
        {
            let pickedImage : UIImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!

            self.dismiss(animated: true, completion: { () -> Void in
            // Use view controller
                
                self.navigationController?.isNavigationBarHidden = false
//                if pickedImage != nil {
                    let controller = ImageCropViewController(image: pickedImage)
                    controller?.delegate = self
                    controller?.blurredBackground = true
                    self.navigationController?.pushViewController(controller!, animated: true)
//                self.navigationController?.present(controller!, animated: true, completion: nil)
//                }
            })
        }
        else if self.useCameraStr == "Upload a video"
        {
            self.videoCaption_txtView.text = ""
            videoUploadbool = false
            self.videoCaption_lbl.isHidden = false
            videoURL = info[UIImagePickerControllerMediaURL] as! NSURL;
            self.uploadVideoView.isHidden = false
            self.videoController = MPMoviePlayerController()
            self.videoController.contentURL = self.videoURL as URL!
            if UIScreen.main.bounds.size.height == 568{
                self.videoController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(60), width: CGFloat(self.view.frame.size.width), height: CGFloat(250))
                 self.videoCaption_txtView.frame.origin.y = videoController.view.frame.origin.y + videoController.view.frame.size.height + 20
                  self.videoCaption_lbl.frame.origin.y = videoController.view.frame.origin.y + videoController.view.frame.size.height + 20
            }
            else{
                self.videoController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(80), width: CGFloat(self.view.frame.size.width), height: CGFloat(350))
                 self.videoCaption_txtView.frame.origin.y = videoController.view.frame.origin.y + videoController.view.frame.size.height + 20
                 self.videoCaption_lbl.frame.origin.y = videoController.view.frame.origin.y + videoController.view.frame.size.height + 20
            }
            
          self.wrapperScrollToUploadVideo.addSubview(self.videoController.view)
            self.videoController.play()
            
             self.dismiss(animated: true, completion: nil)
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        selectImageBool = true
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - ****** Back Button Action. ******
    @IBAction func back_btnAction(_ sender: Any) {
        if (appDelegatePush == "fromAppDelegate") || (appDelegatePush == "onBtnTap")
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        else
        {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    // MARK: - ****** Search Wi People Button Action. ******
    @IBAction func search_btnAction(_ sender: Any) {
        let searchScreen = self.storyboard?.instantiateViewController(withIdentifier: "SearchPeopleViewController") as! SearchPeopleViewController
        self.navigationController?.pushViewController(searchScreen, animated: true)
    }
    
    
    //MARK:- ****** block_btnAction ******
    @IBAction func block_btnAction(_ sender: Any) {
        
    }

  
    // MARK: - ****** Edit Users Profile Button Action. ******
    @IBAction func editUsersProfile_btnAction(_ sender: Any) {
        let editProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(editProfileScreen, animated: true)

    }
    
    
    // MARK: - ****** In App Pruchases / Credits Button Action. ******
    @IBAction func usersCredits_btnAction(_ sender: Any) {
        let messagesScreen = storyboard?.instantiateViewController(withIdentifier: "CreditsPackageViewController") as! CreditsPackageViewController
        messagesScreen.typeOfCreditStr = "FromProfileScreen"
        self.navigationController?.pushViewController(messagesScreen, animated: true)
    }

    // MARK: - ****** About Button Action. ******
    @IBAction func about_btnAction(_ sender: Any) {
        self.appDelegatePush = "fromOtherView"
        if self.interestsArray.count > 0{
            self.noDataFoundLbl.isHidden = true
        }
        else{
             self.noDataFoundLbl.isHidden = true
        }
        
        self.wrapperScrollView.isScrollEnabled = true
        self.about_wrapperScrollView.isScrollEnabled = false
        
        if (self.topViewButtons.isHidden == false)
        {
            self.wrapperScrollView.contentOffset.y =  self.aboutPhotosVideosView.frame.origin.y + self.aboutPhotosVideosView.frame.size.height - 25
        }
        
        self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
        self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height + 50

        self.about_wrapperScrollView.isHidden = false
        self.interestsTxtView.isHidden = false
        self.videosView.isHidden = true
        self.myPostsView.isHidden = true
        self.photosCollectionView.isHidden = true
        self.myPosts_collectionView.isHidden = true
        self.tappedBtnStr = "aboutBtn"
        self.albumBtn.setTitleColor(UIColor.gray, for: .normal)
        self.aboutBtn.setTitleColor(UIColor.black, for: .normal)
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
        self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
        
        self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeTop.setTitleColor(UIColor.black, for: .normal)
        self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
    }
    
    
    // MARK: - ****** My Albums Button Action. ******
    @IBAction func album_btnAction(_ sender: Any) {
        self.cameFromHome = "cameFromHome"
        self.appDelegatePush = "fromOtherView"
        self.tappedBtnStr = "albumBtn"
        self.albumBtn.setTitleColor(UIColor.black, for: .normal)
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
        self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
        
        self.postsBtnTop.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
        self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
        
        if videoUploadbool == true{
            self.timestampVideoArray = []
            self.likesCountArray = []
            self.superLikesCountArray = []
            self.getVideoURLArray = []
            self.videoArray = []
            self.thumbNailArray = []
            self.captionVideoArray = []
            self.videoUploadbool = false
        }
        else
        {
            if self.albumArray.count == 0{
                self.noDataFoundLbl.isHidden = false
                self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: { (finished) in
                })
            }
            else
            {
                self.noDataFoundLbl.isHidden = true
            }
        }
        
        self.wrapperScrollView.isScrollEnabled = true

        
        if (self.topViewButtons.isHidden == false)
        {
            self.wrapperScrollView.contentOffset.y =  self.aboutPhotosVideosView.frame.origin.y + self.aboutPhotosVideosView.frame.size.height - 25
        }
        
        
        if (self.photosCollectionView.contentSize.height < 1000)
        {
            self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
            self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height + 50
        }
        else{
            self.wrapperScrollView.contentSize.height = self.photosCollectionView.frame.origin.y + self.photosCollectionView.contentSize.height + 10
        }
        
      
        self.about_wrapperScrollView.isHidden = true
        self.photosCollectionView.isHidden = false
        self.videosView.isHidden = true
        self.myPostsView.isHidden = true
        self.myPosts_collectionView.isHidden = true
    }
    
    
    
    // MARK: - ****** Photos Button Action. ******
    @IBAction func photos_btnAction(_ sender: Any) {
        
        self.appDelegatePush = "fromOtherView"
        
        if self.profileImagesArray.count > 0{
            self.noDataFoundLbl.isHidden = true
        }
        else{
             self.noDataFoundLbl.isHidden = true
        }
        self.about_wrapperScrollView.isHidden = true
        self.photosCollectionView.isHidden = false
        self.videosView.isHidden = true
        self.tappedBtnStr = "photoBtn"
        self.photoBtn.setTitleColor(UIColor.black, for: .normal)
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
        self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
    }
    
    
    // MARK: - ****** Load More Interests Button Action. ******
    @IBAction func loadMoreInterests_btnAction(_ sender: Any) {
        
        if loadMoreInterestsBtn.titleLabel?.text == "Load More Interests"
        {
            loadMoreInterestsBtn.setTitle("Show Less Interests", for: UIControlState.normal)
            isLoadMore = true
        }
        else{
             loadMoreInterestsBtn.setTitle("Load More Interests", for: UIControlState.normal)
             isLoadMore = false
        }
        

        self.interestsTxtView.isHidden = false
        
        if self.isLoadMore == false
        {
            for index in 0..<self.showInterestsArray.count
            {
                print("index = \(index)")
                var string = self.showInterestsArray.joined(separator: ", ")
                if (string.range(of: "@") != nil)
                {
                    let fullNameArr = string.components(separatedBy: "@")
                    if fullNameArr.count > 0
                    {
                        if fullNameArr.count == 1
                        {
                            let firstName: String = fullNameArr[0]
                            string = firstName
                        }
                        else
                        {
                            let firstName: String = fullNameArr[0]
//                            let lastName: String = fullNameArr[1]
                            string = firstName
                        }
                    }
                }
                
                self.interestsTxtView.text = string
            }
        }
        else
        {
            for index in 0..<self.interestsArray.count
            {
                print("index = \(index)")
                var string = self.interestsArray.joined(separator: ", ")
                if (string.range(of: "@") != nil)
                {
                    let fullNameArr = string.components(separatedBy: "@")
                    if fullNameArr.count > 0
                    {
                        if fullNameArr.count == 1
                        {
                            let firstName: String = fullNameArr[0]
                            string = firstName
                        }
                        else
                        {
                            let firstName: String = fullNameArr[0]
//                            let lastName: String = fullNameArr[1]
                            string = firstName
                        }
                    }
                }

                self.interestsTxtView.text = string
            }
        }
    }
    
    // MARK: - ****** Chat button Action. ******
     func Chat_tbnAction(_ button: UIButton, with event: UIEvent) {
        DispatchQueue.main.asyncAfter(deadline: .now() ){
            let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "ChatListingViewController") as! ChatListingViewController
            self.navigationController?.pushViewController(messagesScreen, animated: true)
        }
    }
    
    // MARK: - ****** Videos Button Action. ******
    @IBAction func videos_btnAction(_ sender: Any) {
     
        if videoUploadbool == true{
            self.timestampVideoArray = []
            self.getVideoURLArray = []
            self.likesCountArray = []
            self.superLikesCountArray = []
            self.videoArray = []
            self.thumbNailArray = []
            self.captionVideoArray = []
            self.videoTableView.reloadData()
             videoUploadbool = false
        }
        self.videosView.isHidden = false
        self.about_wrapperScrollView.isHidden = true
        self.tappedBtnStr = "videoBtn"
        self.videoBtn.setTitleColor(UIColor.black, for: .normal)
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
        self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
    }
    
    
    
    // MARK: - ****** Update Post/Status button Action. ******
    @IBAction func updateNewPost_btnAction(_ sender: Any)
    {
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Post a WiPost", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiCreateConfessionViewController") as! WiCreateConfessionViewController
            homeScreen.postTypeStr = "CreateNewPost"
            self.navigationController?.pushViewController(homeScreen, animated: true)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Post in Album", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
           
            self.uploadPhotoVideo()
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
   }
    
    
    // MARK: - ****** My Posts Button Action. ******
    @IBAction func myPosts_btnAction(_ sender: Any) {

        self.getFullDataStr = "fetchAllData"
        self.postLikedStr = false
        
        self.myPosts_tableView.isScrollEnabled = false
        self.wrapperScrollView.isScrollEnabled = true
        
        if (self.topViewButtons.isHidden == false)
        {
            self.wrapperScrollView.contentOffset.y =  self.aboutPhotosVideosView.frame.origin.y + self.aboutPhotosVideosView.frame.size.height - 25
        }

        if (self.myPosts_tableView.contentSize.height < 1000)
        {
            self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
            self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height + 50
        }
        else
        {
            self.wrapperScrollView.contentSize.height = self.myPostsView.frame.origin.y + self.myPosts_tableView.contentSize.height
        }

        self.cameFromHome = "postsClicked"
        self.myPosts_collectionView.isHidden = true
        self.about_wrapperScrollView.isHidden = true
        self.myPostsView.isHidden = false
        self.isClickedPostsbtn = true
        self.noDataFoundLbl.isHidden = true
        
        self.getAllMyConfessions(paginationStr: "1") { (responseBool) in
            if (responseBool == true)
            {
                if (self.myPostsArray.count > 0)
                {
                    DispatchQueue.main.async {
                        self.noPostsView.isHidden = true
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.myPosts_tableView.reloadData()
                    }
                }
                else
                {
                    self.myPostsArray = []
                    self.loadingView.isHidden = true
                    self.noDataFoundLbl.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.myPosts_collectionView.isHidden = true
                    self.noPostsView.isHidden = false
                    
                    self.noPostsView.frame.size.height = 60
                    
                    self.myPostsView.frame = CGRect(x: self.myPostsView.frame.origin.x , y: self.myPostsView.frame.origin.y, width: self.myPostsView.frame.size.width, height: self.about_wrapperScrollView.contentSize.height)
                    
                    self.myPosts_tableView.frame = CGRect(x: self.myPosts_tableView.frame.origin.x , y:0, width: self.myPosts_tableView.frame.size.width, height: self.myPostsView.frame.size.height)
                    
                    self.wrapperScrollView.isScrollEnabled = true
                    self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                    
                    self.myPostsView.isHidden = false
                    self.myPosts_tableView.isHidden = true
                    self.noPostsView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.noPostsView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                        
                    })
                }
            }
            else
            {
                self.myPostsArray = []
                self.loadingView.isHidden = true
                self.noDataFoundLbl.isHidden = true
                self.view.isUserInteractionEnabled = true
                self.myPosts_collectionView.isHidden = true
                self.noPostsView.isHidden = false
                
                self.noPostsView.frame.size.height = 60
                
                self.myPostsView.frame = CGRect(x: self.myPostsView.frame.origin.x , y: self.myPostsView.frame.origin.y, width: self.myPostsView.frame.size.width, height: self.about_wrapperScrollView.contentSize.height)
                
                self.myPosts_tableView.frame = CGRect(x: self.myPosts_tableView.frame.origin.x , y:0, width: self.myPosts_tableView.frame.size.width, height: self.myPostsView.frame.size.height)
                
                self.wrapperScrollView.isScrollEnabled = true
                self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                
                self.myPostsView.isHidden = false
                self.myPosts_tableView.isHidden = true
                self.noPostsView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.noPostsView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: { (finished) in
                    
                })
            }
        }
        
        self.videosView.isHidden = true
        self.photosCollectionView.isHidden = true
        self.tappedBtnStr = "postsBtn"
        
        self.postsBtn.setTitleColor(UIColor.black, for: .normal)
        self.aboutBtn.setTitleColor(UIColor.gray, for: .normal)
        self.photoBtn.setTitleColor(UIColor.gray, for: .normal)
        self.videoBtn.setTitleColor(UIColor.gray, for: .normal)
        self.albumBtn.setTitleColor(UIColor.gray, for: .normal)
        
        self.postsBtnTop.setTitleColor(UIColor.black, for: .normal)
        self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
        self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
    }
    
    
    
    func getAllMyConfessions(paginationStr: String,completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                ApiHandler.getMyOWnConfessions(user_id: userID, auth_token: userToken, page: "1", completion: { (responseData) in
                    
                    self.myPostsArray = []
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        
                        if (dataDict!["list"] != nil)
                        {
                            let listingArray = dataDict?.value(forKey: "list") as! NSArray
                            
                            if (listingArray.count == 0)
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                self.myPostsArray = []
                                completion(false)
                            }
                            
                            for index in 0..<listingArray.count
                            {
                                var userData = NSDictionary()
                                userData = listingArray[index] as! NSDictionary
                                
                                let timestamp: String = userData.value(forKey: "created") as! String
                                
                                var loc = String()
                                var age = String()
                                var gender = String()
                                var checkUserExists = String()
                                var username = String()
                                var userPic = String()
                                
                                let userID: String = userData.value(forKey: "user_id") as! String
                                let postedImage: String = userData.value(forKey: "image") as! String
                                
                                let likeCountInt: Int = userData.value(forKey: "likes_count") as! Int
                                let likeCountStr : String = String(likeCountInt)
                                let likedby_current: String = userData.value(forKey: "likedby_current") as! String
                                
                                let replyCountStr : Int = userData.value(forKey: "comments_count") as! Int
                                let total_duration: String = userData.value(forKey: "created") as! String
                                let statusAddedStr: String = userData.value(forKey: "text") as! String
                                let postID: String = userData.value(forKey: "id") as! String
                                let mood: String = userData.value(forKey: "mood") as! String
                                let duration: String = userData.value(forKey: "duration") as! String
                                
                                let image_height: String = userData.value(forKey: "image_height") as! String
                                let image_width: String = userData.value(forKey: "image_width") as! String
                                
                                let type: String = userData.value(forKey: "type") as! String
                                
                                var image_height_int = Int()
                                var image_width_int = Int()
                                
                                if (image_height == "")
                                {
                                    image_height_int = 250
                                }
                                else{
                                    image_height_int = Int(image_height)!
                                }
                                
                                if (image_width == "")
                                {
                                    image_width_int = 250
                                }
                                else{
                                    image_width_int = Int(image_width)!
                                }
                                
                                var usersDetailsDict = NSDictionary()
                                if (userData["user"] != nil)
                                {
                                    usersDetailsDict = (userData.value(forKey: "user") as? NSDictionary)!
                                    
                                    checkUserExists = usersDetailsDict.value(forKey: "found") as! String
                                    
                                    if (checkUserExists == "no")
                                    {
                                        return
                                    }
                                    
                                    age = String(usersDetailsDict.value(forKey: "Age") as! Int)
                                    
                                    loc = usersDetailsDict.value(forKey: "Location") as! String
                                    gender = usersDetailsDict.value(forKey: "gender") as! String
                                    username = usersDetailsDict.value(forKey: "username") as! String
                                    userPic = usersDetailsDict.value(forKey: "profile_pic") as! String
                                }
                                else
                                {
                                    age = ""
                                    loc = ""
                                    gender = ""
                                    username = ""
                                    userPic = ""
                                    checkUserExists = "no"
                                }
                                
                                if (checkUserExists == "yes")
                                {
                                    
                                    let userConfession = ConfessionDetailsForPosts.init(location: loc, age: age, createdAt: timestamp, userID: userID, postedImage: postedImage, gender: gender, likesCount: likeCountStr, replyCount: String(replyCountStr), totalTime: total_duration, Status: statusAddedStr, postID: postID, moodType: mood, photoTypeAlbum: "", width: image_width_int, height: image_height_int, postHoursStr: duration,likedByMeStr: likedby_current,username: username,profileStr: userPic, typeOfConfession: type)
                                    
                                    self.myPostsArray.append(userConfession)
                                }
                            }
                            completion(true)
                        }
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.myPostsArray = []
                        completion(false)
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.myPostsArray = []
                        completion(false)
                    }
                })
            }
        }
    }
    
    // MARK: - ****** Edit Description Button Action. ******
    @IBAction func editDescription_btnAction(_ sender: UIButton) {
        
        if sender.tag == 400{
            
            self.typeOftextView = "self.displaycaption_textView"
            
            self.displaycaption_textView.isEditable = true
            self.displaycaption_textView.clipsToBounds = true
            self.displaycaption_textView.becomeFirstResponder()
            
            self.displaycaption_textView.isScrollEnabled = false
            
            let fixedWidth = self.displaycaption_textView.frame.size.width
            self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displaycaption_textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displaycaption_textView.frame = newFrame;
            
            self.crossViewPhotoAlbum.frame.origin.y = self.displaycaption_textView.frame.origin.y + self.displaycaption_textView.frame.height + 20
            
            self.wrapperScrollForPhotoAlbum.isScrollEnabled=true
            self.wrapperScrollForPhotoAlbum.contentSize.height = self.crossViewPhotoAlbum.frame.origin.y + self.crossViewPhotoAlbum.frame.size.height + (self.view.frame.size.height/2)
        }
        else if sender.tag == 401
        {
            
            self.typeOftextView = "self.displayVideoCaption_txtView"
            
            playerViewController.player!.pause()
            if let play = player {
                play.pause()
                player = nil
            } else {
                print("player was already deallocated")
            }
            self.displayVideoCaption_txtView.isEditable = true
            self.displayVideoCaption_txtView.clipsToBounds = true
            self.displayVideoCaption_txtView.becomeFirstResponder()
            
            self.displayVideoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.displayVideoCaption_txtView.frame.size.width
            self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displayVideoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displayVideoCaption_txtView.frame = newFrame;
            
            self.crossViewVideoAlbum.frame.origin.y = self.displayVideoCaption_txtView.frame.origin.y + self.displayVideoCaption_txtView.frame.height + 20
            
            self.wrapperScrollForVideoAlbum.isScrollEnabled=true
            self.wrapperScrollForVideoAlbum.contentSize.height = self.crossViewVideoAlbum.frame.origin.y + self.crossViewVideoAlbum.frame.size.height + (self.view.frame.size.height/2)
        }
    }
    
   // MARK: - ****** Edit Questions Button Action. ******
    @IBAction func EditQuestions_btnAction(_ sender: UIButton) {
        
        if sender.tag == 100
        {
            typeOfStr = "bodyType_txtField"
            self.wrapperQuestionsView.isHidden = false
            
            self.newBodyTypeArray.removeAll()
            
            if let body_type_Array =  appDelegate.getAllDefaultValuesDict.value(forKey: "body_type") as? NSArray
            {
                for index in 0..<body_type_Array.count
                {
                    let dict = body_type_Array[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Slimz","Average","Big Boned","Athletic (Ah Have ah Lil Muscle)","Fluffy (More meat than rice)","Petite (Smallie)","Curvy (In All the right places)","Ask meh"]
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        else if sender.tag == 101
        {
            typeOfStr = "hairColor_txtField"
            self.wrapperQuestionsView.isHidden = false
            
            self.newBodyTypeArray.removeAll()
            
            if let haircolor =  appDelegate.getAllDefaultValuesDict.value(forKey: "haircolor") as? NSArray
            {
                for index in 0..<haircolor.count
                {
                    let dict = haircolor[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Red","Black","Dark Brown","Weave (Any color I choose for it to be)","Bald","Light Brown","Ask meh"]
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        else if sender.tag == 102
        {
            typeOfStr = "relationshipStatus_txtField"
            self.wrapperQuestionsView.isHidden = false
            
            self.newBodyTypeArray.removeAll()
            
            if let relationshipstatus =  appDelegate.getAllDefaultValuesDict.value(forKey: "relationshipstatus") as? NSArray
            {
                for index in 0..<relationshipstatus.count
                {
                    let dict = relationshipstatus[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Single (Like ah dolla)","Married (Happily)","In a relationship","Open relationship","Not looking"]
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)
            
        }
        else if sender.tag == 103
        {
            typeOfStr = "smoking_txtField"
            self.wrapperQuestionsView.isHidden = false
            
            self.newBodyTypeArray.removeAll()
            
            if let smoking = appDelegate.getAllDefaultValuesDict.value(forKey: "smoking") as? NSArray
            {
                for index in 0..<smoking.count
                {
                    let dict = smoking[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Always","Sometimes","Never","Trying to quit","Ask meh"]
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)

        }
        else if sender.tag == 104
        {
            typeOfStr = "drinking_txtField"
            self.wrapperQuestionsView.isHidden = false
            
            self.newBodyTypeArray.removeAll()
            
            if let drinking =  appDelegate.getAllDefaultValuesDict.value(forKey: "drinking") as? NSArray
            {
                for index in 0..<drinking.count
                {
                    let dict = drinking[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Not a drinker","Social Drinker","Frequent drinker","Communion wine (if that counts)","Ask meh"]
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        else if sender.tag == 105
        {
            typeOfStr = "education_txtField"
            self.wrapperQuestionsView.isHidden = false
            
            self.newBodyTypeArray.removeAll()
            
            if let education =  appDelegate.getAllDefaultValuesDict.value(forKey: "education") as? NSArray
            {
                for index in 0..<education.count
                {
                    let dict = education[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["High school (Yes I went to school)","Some College ( I wanted to be smart)","Associates degree (Realize a lil bright)","Bachelors Degree ( Wanted a bess wuk)","Masters ( I rel bright inno)","PhD ( Yah dun kno)","Trade (I'm good with my hands)","Ask meh"]
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        else if sender.tag == 106
        {
            typeOfStr = "genderSelection"
            self.wrapperQuestionsView.isHidden = false
            gender_type = ""
            
            self.newBodyTypeArray.removeAll()
            
            bodyTypeArray = ["Male","Female"]
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        else if sender.tag == 107
        {
            typeOfStr = "meetUp_textField"
            self.wrapperQuestionsView.isHidden = false
            
            self.newBodyTypeArray.removeAll()
            
            if let meetuppreferences =  appDelegate.getAllDefaultValuesDict.value(forKey: "meetuppreferences") as? NSArray
            {
                for index in 0..<meetuppreferences.count
                {
                    let dict = meetuppreferences[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
           bodyTypeArray = ["Looking For A Potential Relationship","Not Looking For A Relationship","Maybe Dinner & Conversation","Meet Up For Drinks","Meet Up For A Fete","Meet Up To Make A New Friendship"]
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        else if sender.tag == 108
        {
            typeOfStr = "interestedIn_txtField"
            self.wrapperQuestionsView.isHidden = false
            
            self.newBodyTypeArray.removeAll()
            
            if let interestesin =  appDelegate.getAllDefaultValuesDict.value(forKey: "interestesin") as? NSArray
            {
                for index in 0..<interestesin.count
                {
                    let dict = interestesin[index] as! NSDictionary
                    
                    let nameStr =  dict.value(forKey: "name") as! String
                    let idStr = dict.value(forKey: "id") as! String
                    
                    let details = values.init(name: nameStr, id: idStr)
                    self.newBodyTypeArray.append(details)
                }
            }
            
            bodyTypeArray = ["Women","Men","Both","Interested in just socializing"]
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        else if sender.tag == 109
        {
            typeOfStr = "island_txtField"
            self.wrapperQuestionsView.isHidden = false
            self.questionsPickerView.reloadAllComponents()
            //use this line for going at the top of the label index 0:
            self.questionsPickerView.selectRow(0, inComponent: 0, animated: false)
        }
    }
    
    
    
    // MARK: - ****** Close PickerView Button Action. ******
    @IBAction func cancel_pickerViewBtn(_ sender: Any) {
        self.wrapperQuestionsView.isHidden = true
    }
    
    
    // MARK: - ****** Done PickerView Button Action. ******
    @IBAction func done_pickerViewBtn(_ sender: Any) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.loadingView.isHidden = false
            self.view.isUserInteractionEnabled = false
            
            if typeOfStr == "bodyType_txtField"
            {
                self.wrapperQuestionsView.isHidden = true
                if bodyTypeSelectedStr == ""
                {
                    self.bodyType_lbl.text = bodyTypeArray[0]
                }
                else{
                    self.bodyType_lbl.text = bodyTypeSelectedStr
                }
                
                self.updateUsersDetails(notificationType: "body_type", getVisibility: self.bodyTypeSelectedID, completion: { (response) in
                    if (response == true)
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    self.viewUserDetails(completion: { (data) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    })
                })
            }
            else if  typeOfStr == "hairColor_txtField"
            {
                self.wrapperQuestionsView.isHidden = true
                if hairColorSelectedStr == ""
                {
                    self.hairColor_lbl.text = bodyTypeArray[0]
                }
                else{
                    self.hairColor_lbl.text = hairColorSelectedStr
                }
                
                self.updateUsersDetails(notificationType: "hair_color", getVisibility: self.hairColorSelectedID, completion: { (response) in
                    if (response == true)
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    self.viewUserDetails(completion: { (data) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    })
                })
                
            }
            else if  typeOfStr == "relationshipStatus_txtField"
            {
                self.wrapperQuestionsView.isHidden = true
                if relationshipSelectedStr == ""
                {
                    self.relationship_lbl.text = bodyTypeArray[0]
                }
                else{
                    self.relationship_lbl.text = relationshipSelectedStr
                }
                
                self.updateUsersDetails(notificationType: "relationship_status", getVisibility: self.relationshipSelectedID, completion: { (response) in
                    if (response == true)
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    self.viewUserDetails(completion: { (data) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    })
                })
                
            }
            else if  typeOfStr == "smoking_txtField"
            {
                self.wrapperQuestionsView.isHidden = true
                if smokingSelectedStr == ""
                {
                    self.smoking_lbl.text = bodyTypeArray[0]
                }
                else{
                    self.smoking_lbl.text = smokingSelectedStr
                }
             
                self.updateUsersDetails(notificationType: "smoking", getVisibility: self.smokingSelectedID, completion: { (response) in
                    if (response == true)
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    self.viewUserDetails(completion: { (data) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    })
                })
                
            }
            else if typeOfStr == "drinking_txtField"
            {
                self.wrapperQuestionsView.isHidden = true
                if drinkingSelectedStr == ""
                {
                    self.drinking_lbl.text = bodyTypeArray[0]
                }
                else{
                    self.drinking_lbl.text = drinkingSelectedStr
                }
                
                self.updateUsersDetails(notificationType: "drinking", getVisibility: self.drinkingSelectedID, completion: { (response) in
                    if (response == true)
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    self.viewUserDetails(completion: { (data) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    })
                })
                
            }
            else if  typeOfStr == "education_txtField"
            {
                self.wrapperQuestionsView.isHidden = true
                if educationSelectedStr == ""
                {
                    self.education_lbl.text = bodyTypeArray[0]
                }
                else{
                    self.education_lbl.text = educationSelectedStr
                }
             
                self.updateUsersDetails(notificationType: "education", getVisibility: self.educationSelectedID, completion: { (response) in
                    if (response == true)
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    self.viewUserDetails(completion: { (data) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    })
                })
            }
            else if typeOfStr == "genderSelection"
            {
                self.wrapperQuestionsView.isHidden = true
                bodyTypeArray = ["Male","Female"]
                
                var selectedGender = String()
                if gender_type == ""
                {
                    gender_type = bodyTypeArray[0]
                    if gender_type == "Male"{
                        
                        gender_type = "male"
                    }else{
                        gender_type = "female"
                    }
                    
                    selectedGender = gender_type
                    self.myGender_lbl.text = bodyTypeArray[0]
                }
                else{
                    if gender_type == "Male"{
                        
                        gender_type = "male"
                    }else{
                        gender_type = "female"
                    }

                    selectedGender = gender_type
                    self.myGender_lbl.text = gender_type
                }
                
                var genderID = String()
                if (self.gender_type == "male")
                {
                    genderID = "1"
                }
                else if (self.gender_type == "female")
                {
                    genderID = "2"
                }
                else
                {
                    genderID = "1"
                }
                
                print("selectedGender = \(selectedGender)")
                
                self.updateUsersDetails(notificationType: "gender", getVisibility: genderID, completion: { (response) in
                    if (response == true)
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    
                    self.viewUserDetails(completion: { (data) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    })
                })
            }
        else if  typeOfStr == "meetUp_textField"
        {
            self.wrapperQuestionsView.isHidden = true
            if meetUpStr == ""
            {
                self.meetup_lbl.text = bodyTypeArray[0]
            }
            else{
                self.meetup_lbl.text = meetUpStr
            }
          
            self.updateUsersDetails(notificationType: "meet_up_preferences", getVisibility: self.meetUpSelectedID, completion: { (response) in
                if (response == true)
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                }
                self.viewUserDetails(completion: { (data) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                })
            })
            
        }
        else if  typeOfStr == "interestedIn_txtField"
        {
            self.wrapperQuestionsView.isHidden = true
            if interestedInStr == ""
            {
                self.interestedIn_lbl.text = bodyTypeArray[0]
            }
            else{
                self.interestedIn_lbl.text = interestedInStr
            }
            
            if (self.interestedInSelectedID == "")
            {
                self.interestedInSelectedID = "1"
            }
          
            self.updateUsersDetails(notificationType: "intrested_in_meeting", getVisibility: self.interestedInSelectedID, completion: { (response) in
                if (response == true)
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                }
                self.viewUserDetails(completion: { (data) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                })
            })
            
        }
        else if  typeOfStr == "island_txtField"
        {
                self.wrapperQuestionsView.isHidden = true
                if countrySelectedStr == ""
                {
                    self.representCountry_lbl.text = ((nameIDArray[0] as! NSDictionary).value(forKey: "c_name") as? String)
                    self.countryApiSelectedID = "1"
                }
                else{
                    self.representCountry_lbl.text = countrySelectedStr
                }
            
            self.updateUsersDetails(notificationType: "country", getVisibility: self.countryApiSelectedID, completion: { (response) in
                if (response == true)
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                }
                self.viewUserDetails(completion: { (data) in
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                })
            })
          }
        }
    }

    // MARK: - ****** UIPickerView Delegate and Datasource method. ******
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if typeOfStr == "island_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                return appDelegate.newNameIDArray.count;
            }
            else
            {
                return nameIDArray.count
            }
        }
        else
        {
            if (self.newBodyTypeArray.count > 0)
            {
                return newBodyTypeArray.count;
            }
            else{
                return bodyTypeArray.count;
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        var str = String()
        if typeOfStr == "island_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                str = (appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "country_name") as! String
            }
            else{
                str = (nameIDArray[row] as! NSDictionary).value(forKey: "c_name") as! String
            }
        }
        else
        {
            if (self.newBodyTypeArray.count > 0)
            {
                str = newBodyTypeArray[row].name
            }
            else{
                str = bodyTypeArray[row]
            }
        }
        return str
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if typeOfStr == "island_txtField"
        {
            if (appDelegate.newNameIDArray.count > 0)
            {
                countrySelectedStr = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "country_name") as? String)!
                
                self.countryApiSelectedID = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "id") as? String)!
                
                if countrySelectedStr == "United States"
                {
                    getCountryID = ((appDelegate.newNameIDArray[row] as! NSDictionary).value(forKey: "id") as? String)!
                    let db = FMDBDataAccess()
                    self.countryNameArray = db.getStatesForCountrySelected(getCountryID)
                    
                    var state: String = ""
                    var countryy = Country()
                    
                    testArray.removeAllObjects()
                    for i in 0..<countryNameArray.count {
                        countryy = countryNameArray[i] as! Country
                        state = "\(countryy.lastName!)"
                        testArray.add(state)
                    }
                    
                    self.countryApiSelectedID = String(describing: row)
                }
            }
            else
            {
                countrySelectedStr = ((nameIDArray[row] as! NSDictionary).value(forKey: "c_name") as? String)!
                
                self.countryApiSelectedID = String(describing: row + 1)
                
                if countrySelectedStr == "United States"
                {
                    getCountryID = ((nameIDArray[row] as! NSDictionary).value(forKey: "c_id") as? String)!
                    let db = FMDBDataAccess()
                    self.countryNameArray = db.getStatesForCountrySelected(getCountryID)
                    
                    var state: String = ""
                    var countryy = Country()
                    
                    testArray.removeAllObjects()
                    for i in 0..<countryNameArray.count {
                        countryy = countryNameArray[i] as! Country
                        state = "\(countryy.lastName!)"
                        testArray.add(state)
                    }
                    
                    self.countryApiSelectedID = String(describing: row)
                }
            }
        }
        else if typeOfStr == "bodyType_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                bodyTypeSelectedStr = newBodyTypeArray[row].name
                self.bodyTypeSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                bodyTypeSelectedStr = bodyTypeArray[row]
                self.bodyTypeSelectedID = String(describing: row + 1)
            }
        }
        else if  typeOfStr == "hairColor_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                hairColorSelectedStr = newBodyTypeArray[row].name
                self.hairColorSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                hairColorSelectedStr = bodyTypeArray[row]
                self.hairColorSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "relationshipStatus_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                relationshipSelectedStr = newBodyTypeArray[row].name
                self.relationshipSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                relationshipSelectedStr = bodyTypeArray[row]
                self.relationshipSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "smoking_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                smokingSelectedStr = newBodyTypeArray[row].name
                self.smokingSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                smokingSelectedStr = bodyTypeArray[row]
                self.smokingSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "drinking_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                drinkingSelectedStr = newBodyTypeArray[row].name
                self.drinkingSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                drinkingSelectedStr = bodyTypeArray[row]
                self.drinkingSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "education_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                educationSelectedStr = newBodyTypeArray[row].name
                self.educationSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                educationSelectedStr = bodyTypeArray[row]
                self.educationSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "meetUp_textField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                meetUpStr = newBodyTypeArray[row].name
                self.meetUpSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                meetUpStr = bodyTypeArray[row]
                self.meetUpSelectedID = String(describing: row + 1)
            }
            
        }
        else if typeOfStr == "interestedIn_txtField"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                interestedInStr = newBodyTypeArray[row].name
                self.interestedInSelectedID = String(describing: newBodyTypeArray[row].id)
            }
            else
            {
                interestedInStr = bodyTypeArray[row]
                self.interestedInSelectedID = String(describing: row + 1)
            }
        }
        else if typeOfStr == "genderSelection"
        {
            if (self.newBodyTypeArray.count > 0)
            {
                gender_type = newBodyTypeArray[row].name
            }
            else
            {
                gender_type = bodyTypeArray[row]
            }
        }
    }

    // MARK: - ****** UIScrollView Delegate. ******
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.wrapperScrollView.isScrollEnabled = true
        self.about_wrapperScrollView.isScrollEnabled = false
        self.about_wrapperScrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
        self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height + 50
        self.buttonsScrollView.isScrollEnabled=true
        self.buttonsScrollView.contentSize.width = self.postsBtn.frame.origin.x + self.postsBtn.frame.size.width 
    }
    
    
    //MARK:-  ****** UITEXT-VIEW DELEGATES ******
    public func textViewDidChange(_ textView: UITextView)
    {
        if (textView == self.addcaption_txtView)
        {
            if (textView.text.count > 0)
            {
                addCaptionLbl.isHidden = true
            }
            else
            {
                addCaptionLbl.isHidden = false
            }
            
            self.addcaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.addcaption_txtView.frame.size.width
            self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.addcaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.addcaption_txtView.frame = newFrame;
            
            self.uploadPhotoBtn.frame.origin.y = self.addcaption_txtView.frame.origin.y + self.addcaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadPhoto.isScrollEnabled=true
            self.wrapperScrollToUploadPhoto.contentSize.height = self.uploadPhotoBtn.frame.origin.y + self.uploadPhotoBtn.frame.size.height + (self.view.frame.size.height/2)
        
        }
        else if (textView == self.videoCaption_txtView)
        {
            if (textView.text.count > 0)
            {
                videoCaption_lbl.isHidden = true
            }
            else
            {
                videoCaption_lbl.isHidden = false
            }
            
            self.videoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.videoCaption_txtView.frame.size.width
            self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.videoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.videoCaption_txtView.frame = newFrame;
            
            self.uploadVideoBtn.frame.origin.y = self.videoCaption_txtView.frame.origin.y + self.videoCaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadVideo.isScrollEnabled=true
            self.wrapperScrollToUploadVideo.contentSize.height = self.uploadVideoBtn.frame.origin.y + self.uploadVideoBtn.frame.size.height + (self.view.frame.size.height/2)
        }
        else if (textView == self.displayVideoCaption_txtView)
        {
            playerViewController.player!.pause()
            if let play = player {
                play.pause()
                player = nil
            } else {
                print("player was already deallocated")
            }
            if (textView.text.count > 0)
            {
                self.editVideoCaption_lbl.isHidden = true
            }
            else
            {
                self.editVideoCaption_lbl.isHidden = false
            }
            
            self.displayVideoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.displayVideoCaption_txtView.frame.size.width
            self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displayVideoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displayVideoCaption_txtView.frame = newFrame;
            
            self.crossViewVideoAlbum.frame.origin.y = self.displayVideoCaption_txtView.frame.origin.y + self.displayVideoCaption_txtView.frame.height + 20
            
            self.wrapperScrollForVideoAlbum.isScrollEnabled=true
            self.wrapperScrollForVideoAlbum.contentSize.height = self.crossViewVideoAlbum.frame.origin.y + self.crossViewVideoAlbum.frame.size.height + (self.view.frame.size.height/2)
        
        }
        else if (textView == self.displaycaption_textView)
        {
            if (textView.text.count > 0)
            {
                self.editPhotoCaption_lbl.isHidden = true
            }
            else
            {
                self.editPhotoCaption_lbl.isHidden = false
            }
            
            self.displaycaption_textView.isScrollEnabled = false
            
            let fixedWidth = self.displaycaption_textView.frame.size.width
            self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displaycaption_textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displaycaption_textView.frame = newFrame;
            
            self.crossViewPhotoAlbum.frame.origin.y = self.displaycaption_textView.frame.origin.y + self.displaycaption_textView.frame.height + 20
            
            self.wrapperScrollForPhotoAlbum.isScrollEnabled=true
            self.wrapperScrollForPhotoAlbum.contentSize.height = self.crossViewPhotoAlbum.frame.origin.y + self.crossViewPhotoAlbum.frame.size.height + (self.view.frame.size.height/2)
        }
        else if (textView == self.aboutMe_txtView)
        {
            if aboutMe_txtView.text.count > 0
            {
                self.aboutMe_placeholderLbl.isHidden = true
            }
            else{
                self.aboutMe_placeholderLbl.isHidden = false
            }
        }

    }

    // MARK: - ****** UITextView Delegates. ******
    public func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView == self.addcaption_txtView)
        {
            self.view.frame.origin.y -= 120
            self.typeOftextView = "self.addcaption_txtView"
            
            self.addcaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.addcaption_txtView.frame.size.width
            self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.addcaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.addcaption_txtView.frame = newFrame;
            
            self.uploadPhotoBtn.frame.origin.y = self.addcaption_txtView.frame.origin.y + self.addcaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadPhoto.isScrollEnabled=true
            self.wrapperScrollToUploadPhoto.contentSize.height = self.uploadPhotoBtn.frame.origin.y + self.uploadPhotoBtn.frame.size.height + (self.view.frame.size.height/2)
        }
        else
        {
             self.typeOftextView = ""
             self.view.frame.origin.y -= 180
        }
        
        if (textView == self.videoCaption_txtView)
        {
            self.typeOftextView = "self.videoCaption_txtView"
            
            self.videoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.videoCaption_txtView.frame.size.width
            self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.videoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.videoCaption_txtView.frame = newFrame;
            
            self.uploadVideoBtn.frame.origin.y = self.videoCaption_txtView.frame.origin.y + self.videoCaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadVideo.isScrollEnabled=true
            self.wrapperScrollToUploadVideo.contentSize.height = self.uploadVideoBtn.frame.origin.y + self.uploadVideoBtn.frame.size.height + (self.view.frame.size.height/2)
        }
        
        
        if (textView == self.displaycaption_textView)
        {
            self.typeOftextView = "self.displaycaption_textView"
            self.displaycaption_textView.isHidden = false
            self.editPhotoCaption_lbl.isHidden = true
            
            self.displaycaption_textView.isScrollEnabled = false
            
            let fixedWidth = self.displaycaption_textView.frame.size.width
            self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displaycaption_textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displaycaption_textView.frame = newFrame;
            
            self.crossViewPhotoAlbum.frame.origin.y = self.displaycaption_textView.frame.origin.y + self.displaycaption_textView.frame.height + 20
            
            self.wrapperScrollForPhotoAlbum.isScrollEnabled=true
            self.wrapperScrollForPhotoAlbum.contentSize.height = self.crossViewPhotoAlbum.frame.origin.y + self.crossViewPhotoAlbum.frame.size.height + (self.view.frame.size.height/2)
        }
        else if (textView == self.displayVideoCaption_txtView)
        {
            self.typeOftextView = "self.displayVideoCaption_txtView"
            
            self.displayVideoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.displayVideoCaption_txtView.frame.size.width
            self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displayVideoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displayVideoCaption_txtView.frame = newFrame;
            
            self.crossViewVideoAlbum.frame.origin.y = self.displayVideoCaption_txtView.frame.origin.y + self.displayVideoCaption_txtView.frame.height + 20
            
            self.wrapperScrollForVideoAlbum.isScrollEnabled=true
            self.wrapperScrollForVideoAlbum.contentSize.height = self.crossViewVideoAlbum.frame.origin.y + self.crossViewVideoAlbum.frame.size.height + (self.view.frame.size.height/2)
            
            playerViewController.player!.pause()
            if let play = player {
                play.pause()
                player = nil
            } else {
                print("player was already deallocated")
            }
            self.displayVideoCaption_txtView.isHidden = false
            self.editVideoCaption_lbl.isHidden = true
        }
    }
    
    
    public func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView == self.addcaption_txtView)
        {
            self.view.frame.origin.y += 120
        }
        else
        {
            self.view.frame.origin.y += 180
        }
        
        if (textView == self.displaycaption_textView)
        {

        }
        else if (textView == self.displayVideoCaption_txtView)
        {
            playerViewController.player!.pause()
            if let play = player {
                play.pause()
                player = nil
            } else {
                print("player was already deallocated")
            }

        }
        
        self.typeOftextView = ""
        
        if (self.typeOftextView == "self.displayVideoCaption_txtView")
        {
            self.displayVideoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.displayVideoCaption_txtView.frame.size.width
            self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displayVideoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displayVideoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displayVideoCaption_txtView.frame = newFrame;
            
            self.crossViewVideoAlbum.frame.origin.y = self.displayVideoCaption_txtView.frame.origin.y + self.displayVideoCaption_txtView.frame.height + 20
            
            self.wrapperScrollForVideoAlbum.isScrollEnabled=true
            self.wrapperScrollForVideoAlbum.contentSize.height = self.crossViewVideoAlbum.frame.origin.y + self.crossViewVideoAlbum.frame.size.height + 20
        }
        else if (self.typeOftextView == "self.displaycaption_textView")
        {
            self.displaycaption_textView.isScrollEnabled = false
            
            let fixedWidth = self.displaycaption_textView.frame.size.width
            self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.displaycaption_textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.displaycaption_textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.displaycaption_textView.frame = newFrame;
            
            self.crossViewPhotoAlbum.frame.origin.y = self.displaycaption_textView.frame.origin.y + self.displaycaption_textView.frame.height + 20
            
            self.wrapperScrollForPhotoAlbum.isScrollEnabled=true
            self.wrapperScrollForPhotoAlbum.contentSize.height = self.crossViewPhotoAlbum.frame.origin.y + self.crossViewPhotoAlbum.frame.size.height + 20
        }
        else if (self.typeOftextView == "self.addcaption_txtView")
        {
            self.addcaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.addcaption_txtView.frame.size.width
            self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.addcaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.addcaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.addcaption_txtView.frame = newFrame;
            
            self.uploadPhotoBtn.frame.origin.y = self.addcaption_txtView.frame.origin.y + self.addcaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadPhoto.isScrollEnabled=true
            self.wrapperScrollToUploadPhoto.contentSize.height = self.uploadPhotoBtn.frame.origin.y + self.uploadPhotoBtn.frame.size.height + 20
        }
        else if (self.typeOftextView == "self.videoCaption_txtView")
        {
            self.videoCaption_txtView.isScrollEnabled = false
            
            let fixedWidth = self.videoCaption_txtView.frame.size.width
            self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.videoCaption_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = self.videoCaption_txtView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.videoCaption_txtView.frame = newFrame;
            
            self.uploadVideoBtn.frame.origin.y = self.videoCaption_txtView.frame.origin.y + self.videoCaption_txtView.frame.height + 20
            
            self.wrapperScrollToUploadVideo.isScrollEnabled=true
            self.wrapperScrollToUploadVideo.contentSize.height = self.uploadVideoBtn.frame.origin.y + self.uploadVideoBtn.frame.size.height + 20
        }

    }
    
    
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
//         let maxCharacter: Int = 140
        var maxCharacter = Int()
        if textView ==  self.aboutMe_txtView
        {
            maxCharacter = 500
            self.typeOftextView = "self.aboutMe_txtView"
          
        }
         else if (textView == self.displayVideoCaption_txtView)
         {
//            maxCharacter = 140
            maxCharacter = 500
            self.typeOftextView = "self.displayVideoCaption_txtView"
          
        }
        else if (textView == self.displaycaption_textView)
        {
//            maxCharacter = 140
            maxCharacter = 500
            self.typeOftextView = "self.displaycaption_textView"
      
        }
        else if (textView == self.addcaption_txtView) || (textView == self.videoCaption_txtView)
         {
            maxCharacter = 500
            if (textView == self.addcaption_txtView)
            {
                self.typeOftextView = "self.addcaption_txtView"
            }
            else{
                self.typeOftextView = "self.videoCaption_txtView"
            }
          
        }
        
        return (textView.text?.utf16.count ?? 0) + text.utf16.count - range.length <= maxCharacter
        
    }
    
    
    //MARK:- ****** TIME DIFFERENCE FROM TIMESTAMP AND CURRENT TIME ******
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        
        /// GET TIMESTAMP IN UNIX
        let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
        
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        
        // Convert Timestamp to NSDate
        let date22:NSDate = NSDate(timeIntervalSince1970: (Double(dateString)!)/1000)
        
        var dateString = String()
        
        let nYears = timeDiff / (1000*60*60*24*365)
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        var timeMsg = ""
        if nYears > 0
        {
            //var yearWord = "years"
            if nYears == 1
            {
               // yearWord = "year"
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd, MMMM yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
            dateString = dateFormatter.string(from: date22 as Date)
            
            timeMsg = dateString //"\(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
           // var monthWord = "months"
            if nMonths == 1
            {
                //monthWord = "month"
            }
            
            //Date formatting
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM, dd"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
            dateString = dateFormatter.string(from: date22 as Date)

            timeMsg = dateString  //" \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            //Date formatting
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM, dd"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
            let dateString = dateFormatter.string(from: date22 as Date)
            
            if nDays > 7
            {
                timeMsg = dateString
            }
            else
            {
                timeMsg = " \(nDays) \(dayWord) ago"
            }
            
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = " \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = " \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }
    
    
    
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) mon ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 mon ago"
            } else {
                return "Last mon"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) min ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) sec ago"
        } else {
            return "just now"
        }
    }
    

    
    
    
    
    
     // MARK: - ****** Like My Images Btn Action. ******
    @IBAction func myImagesLike_btnAction(_ sender: Any) {
       
    }
   
    
    // MARK: - ****** Super Like My Images Btn Action. ******
    @IBAction func myImagesSuperLike_btnAction(_ sender: Any) {
   
    }
  
    
    // MARK: -  ****** Like My Video Btn Action. ******
    @IBAction func likeVideo_btnAction(_ sender: Any) {
     
    }
  
    
    // MARK: - ****** Super Like My Video Btn Action. ******
    @IBAction func superLikeVideo_btnAction(_ sender: Any) {
        
    }
 
    
    //MARK:- ****** didFinishCroppingImage ******
    func imageCropViewControllerSuccess(_ controller: UIViewController, didFinishCroppingImage croppedImage: UIImage)
    {
        self.navigationController!.popViewController(animated: true)
        selectImageBool = true
//        self.navigationController?.dismiss(animated: true, completion: nil)
        self.photoUploadView.isHidden = false
        self.addcaption_txtView.text = ""
        self.addCaptionLbl.isHidden = false
        self.addPhotoImageView.image = croppedImage
        self.addPhotoImageView.contentMode = UIViewContentMode.scaleAspectFit
    }

    //MARK:- ****** imageCropViewControllerDidCancel ******
    func imageCropViewControllerDidCancel(_ controller: UIViewController)
    {
        self.navigationController!.popViewController(animated: true)
        selectImageBool = true
        self.photoUploadView.isHidden = true
    }


    // MARK: - ****** viewAlbumsImage_btnAction ******
    @IBAction func viewAlbumsImage_btnAction(_ sender: Any) {
        self.currentPostedImageView.image = self.fullImage.image
        self.currentPostedImageView.contentMode = UIViewContentMode.scaleAspectFit
        self.currentPostedImageView.clipsToBounds = true
        
        self.viewImage.isHidden=false
       /* self.currentPostedImageView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.currentPostedImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { (finished) in
        })*/
    }
    
    // MARK: - ****** ViewPostedImage_btnAction ******
    @IBAction func ViewPostedImage_btnAction(_ sender: Any) {
        self.currentPostedImageView.image = self.postedImage.image
        self.currentPostedImageView.contentMode = UIViewContentMode.scaleAspectFit
        self.currentPostedImageView.clipsToBounds = true
        self.postPostStatus_lbl.isHidden = false
        self.postPostStatus_lbl.text = (self.postedStatus_lbl.text)?.uppercased()
        self.postPostStatus_lbl.textColor = self.postedStatus_lbl.textColor
        self.postPostStatus_lbl.numberOfLines = 100
        self.postPostStatus_lbl.adjustsFontSizeToFitWidth = true
        
        self.viewImage.isHidden=false
       /* self.currentPostedImageView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.currentPostedImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { (finished) in
        })*/
    }
    
    
    // MARK: - ****** Close ImageView Btn Action. ******
    @IBAction func closeViewImage_btnAction(_ sender: Any) {
        self.viewImage.isHidden = true
        
        if self.player != nil
        {
            playerViewController.player!.pause()
            if let play = player {
                play.pause()
                player = nil
            } else {
                print("player was already deallocated")
            }
        }
    }
    
    
    // MARK: - ****** View Profile Image Full Size Btn Action. ******
    @IBAction func viewProfileImage_btnAction(_ sender: Any) {
        self.currentPostedImageView.image = self.usersProfileImage.image
        self.currentPostedImageView.contentMode = UIViewContentMode.scaleAspectFit
        self.currentPostedImageView.clipsToBounds = true
        
        self.viewImage.isHidden=false
        /* self.currentPostedImageView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
         UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
         self.currentPostedImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
         }, completion: { (finished) in
         })*/
    
    }
    
    // MARK: - ****** View Album Photos Directly Btn Action. ******
    @IBAction func albumsView_btnAction(_ sender: UIButton) {

    }
    
    // MARK: - ****** View Album Photos Directly Btn Action. ******
    func viewAlbumPhoto_btnAction(_ sender: UIButton)
    {
       
    }
    
    
    
    // MARK: - ****** UITableView Delegate & Data Source Methods. ******
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == self.myPosts_tableView{
                return self.myPostsArray.count
        }
        else{
            return 10
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.myPosts_tableView
        {
            self.myPosts_tableView.separatorColor = UIColor.clear
            if (self.myPostsArray[indexPath.row].postedImage != "")
            {
                let imageURL = self.myPostsArray[indexPath.row].postedImage
                if (imageURL.contains("jpg"))
                {
                    let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath) as! CreditPackagesTableViewCell
                    
                    if (self.myPostsArray.count <= 0)
                    {
                        return creditsCell
                    }
                    
                    creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                    creditsCell.tag = indexPath.row
                    creditsCell.confessionLikeIcon.tag = indexPath.row
                    creditsCell.animatedGifImage.tag = indexPath.row
                    creditsCell.animatedGifImage.isHidden = true
                    creditsCell.activityIndicator_nearBy.tag = indexPath.row
                    creditsCell.moodTypeIcon.tag = indexPath.row
                    
                    DispatchQueue.main.async {
                        
                        if self.getFullDataStr == "fetchAllData"
                        {
                            if (self.myPostsArray[indexPath.row].likedByMeStatus == "yes")
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                        else
                        {
                         
                        }
                    }
                    
                    var endDateForConfession = String(self.myPostsArray[indexPath.row].totalTime)
                    endDateForConfession = endDateForConfession! + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    let datee = dateFormatter.date(from: endDateForConfession!)!
                    
                    creditsCell.decreaseTimerLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                    creditsCell.decreaseTimerLbl.adjustsFontSizeToFitWidth = true
                    
                    let genderStr = self.myPostsArray[indexPath.row].gender
                    var gender = String()
                    if genderStr == "1"
                    {
                        gender = "Male"
                    }
                    else
                    {
                        gender = "Female"
                    }
                    
                    creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                    creditsCell.moodTypeIcon.clipsToBounds = true
                    creditsCell.moodTypeIcon.layer.masksToBounds = true
                    creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                    
                    if (self.myPostsArray[indexPath.row].typeOfConfession == "anonymous")
                    {
                        creditsCell.userGenderAgeLbl.text = gender + ", " + self.myPostsArray[indexPath.row].age
                        
                         creditsCell.moodTypeIcon.sd_setImage(with: URL(string: self.myPostsArray[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                        creditsCell.moodTypeIcon.addSubview(blurEffectView)
                    }
                    else
                    {
                        creditsCell.userGenderAgeLbl.text = self.myPostsArray[indexPath.row].name
                        
                        for subview in creditsCell.moodTypeIcon.subviews
                        {
                            if subview.isKind(of: UIVisualEffectView.self)
                            {
                                subview.removeFromSuperview()
                            }
                        }
                        
                        creditsCell.moodTypeIcon.sd_setImage(with: URL(string: self.myPostsArray[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                    }
                    
                    
                    
//                    creditsCell.userStatusLbl.text = self.myPostsArray[indexPath.row].statusAdded
                    
                    let inputString: NSString = self.myPostsArray[indexPath.row].statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    
                    creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                    creditsCell.usersStatus_TxtView.text = modifiedString
                    
                   /* DispatchQueue.main.async {
                        if (creditsCell.usersStatus_TxtView.text != "")
                        {
                            let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                            creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            var newFrame = creditsCell.usersStatus_TxtView.frame
                            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                            
                            creditsCell.animatedImageHeight.constant = newSize.height
                        }
                        else{
                            creditsCell.animatedImageHeight.constant = 0
                        }
                    }*/
                    
                    creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.myPostsArray[indexPath.row].postedImage), placeholderImage: UIImage(named: "Default Icon"), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell.activityIndicator_nearBy.isHidden = true
                        
                        creditsCell.animatedGifImage.isHidden = false
//                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
//                        creditsCell.animatedGifImage.layer.masksToBounds = false
//                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                        creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell.animatedGifImage.clipsToBounds = true
                    }
                    
//                    let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGestureDetected(_:)))
//                    pinchGestureRecognizer.delegate = self
//                    creditsCell.animatedGifImage.addGestureRecognizer(pinchGestureRecognizer)
//                    creditsCell.animatedGifImage.isUserInteractionEnabled = true
                   
                    if self.myPostsArray[indexPath.row].likesCount == "0"
                    {
                        creditsCell.totalLikesCountLbl.text = ""
                    }
                    else
                    {
                        creditsCell.totalLikesCountLbl.text = String(self.myPostsArray[indexPath.row].likesCount)
                    }
                    
                    if (self.myPostsArray[indexPath.row].location != "")
                    {
                        creditsCell.locationIcon.isHidden = false
                        creditsCell.userNameLbl_Y_Constraint.constant = 5
                    }else{
                        creditsCell.locationIcon.isHidden = true
                        creditsCell.userNameLbl_Y_Constraint.constant = (creditsCell.moodTypeIcon.frame.origin.y + creditsCell.moodTypeIcon.frame.size.height)/3
                    }
                    
                    creditsCell.userLocationLbl.text = self.myPostsArray[indexPath.row].location
                    creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                    
                    if  self.myPostsArray[indexPath.row].replyCount == "0"
                    {
                        creditsCell.totalReplyCountLbl.text = ""
                    }
                    else
                    {
                        creditsCell.totalReplyCountLbl.text = self.myPostsArray[indexPath.row].replyCount
                    }
                    
                    creditsCell.confessionLikesBtn.tag = indexPath.row
                    creditsCell.confessionLikesBtn.addTarget(self, action: #selector(ProfileViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionReplyBtn.tag = indexPath.row
                    creditsCell.confessionReplyBtn.addTarget(self, action: #selector(ProfileViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)

                    creditsCell.dropDownBtn.tag = indexPath.row
                    creditsCell.dropDownBtn.addTarget(self, action: #selector(ProfileViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewFullImageBtn.tag = indexPath.row
                    creditsCell.viewFullImageBtn.addTarget(self, action: #selector(ProfileViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                    
                    return creditsCell
                }
                else
                {
                    let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell1", for: indexPath) as! CreditPackagesTableViewCell
                    
                    if (self.myPostsArray.count <= 0)
                    {
                        return creditsCell
                    }
                    
                    creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                    creditsCell.tag = indexPath.row
                    creditsCell.confessionLikeIcon.tag = indexPath.row
                    creditsCell.animatedGifImage.tag = indexPath.row
                    creditsCell.animatedGifImage.isHidden = true
                    creditsCell.activityIndicatorGif_nearBy.tag = indexPath.row
                    creditsCell.moodTypeIcon.tag = indexPath.row
                    
                    DispatchQueue.main.async {
                        
                        if self.getFullDataStr == "fetchAllData"
                        {
                            if (self.myPostsArray[indexPath.row].likedByMeStatus == "yes")
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                        else
                        {
                          
                        }
                    }
                    
                    var endDateForConfession = String(self.myPostsArray[indexPath.row].totalTime)
                    endDateForConfession = endDateForConfession! + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    let datee = dateFormatter.date(from: endDateForConfession!)!
                    
                    creditsCell.decreaseTimerLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                    creditsCell.decreaseTimerLbl.adjustsFontSizeToFitWidth = true
                    
                    let genderStr = self.myPostsArray[indexPath.row].gender
                    var gender = String()
                    if genderStr == "1"
                    {
                        gender = "Male"
                    }
                    else
                    {
                        gender = "Female"
                    }
                    
                    creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                    creditsCell.moodTypeIcon.clipsToBounds = true
                    creditsCell.moodTypeIcon.layer.masksToBounds = true
                    creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                    
                    if (self.myPostsArray[indexPath.row].typeOfConfession == "anonymous")
                    {
                        creditsCell.userGenderAgeLbl.text = gender + ", " + self.myPostsArray[indexPath.row].age
                        
                         creditsCell.moodTypeIcon.sd_setImage(with: URL(string: self.myPostsArray[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                        creditsCell.moodTypeIcon.addSubview(blurEffectView)
                    }
                    else
                    {
                        creditsCell.userGenderAgeLbl.text = self.myPostsArray[indexPath.row].name
                        
                        for subview in creditsCell.moodTypeIcon.subviews
                        {
                            if subview.isKind(of: UIVisualEffectView.self)
                            {
                                subview.removeFromSuperview()
                            }
                        }
                        
                        creditsCell.moodTypeIcon.sd_setImage(with: URL(string: self.myPostsArray[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                    }
                   
                    let inputString: NSString = self.myPostsArray[indexPath.row].statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    
                  
                    creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                    creditsCell.usersStatus_TxtView.text = modifiedString
                    
                  /*  DispatchQueue.main.async {
                        if (creditsCell.usersStatus_TxtView.text != "")
                        {
                            let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                            creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                            var newFrame = creditsCell.usersStatus_TxtView.frame
                            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                            
                            creditsCell.animatedGifHeight.constant = newSize.height
                        }
                        else{
                            creditsCell.animatedGifHeight.constant = 0
                        }
                    }*/
                    
                    creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.myPostsArray[indexPath.row].postedImage), placeholderImage: UIImage(named: "Default Icon"), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell.activityIndicatorGif_nearBy.isHidden = true
                        
                        creditsCell.animatedGifImage.isHidden = false
//                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
//                        creditsCell.animatedGifImage.layer.masksToBounds = false
//                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                        creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell.animatedGifImage.clipsToBounds = true
                    }
                    
//                    let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGestureDetected(_:)))
//                    pinchGestureRecognizer.delegate = self
//                    creditsCell.animatedGifImage.addGestureRecognizer(pinchGestureRecognizer)
//                    creditsCell.animatedGifImage.isUserInteractionEnabled = true
                    
                    if self.myPostsArray[indexPath.row].likesCount == "0"
                    {
                        creditsCell.totalLikesCountLbl.text = ""
                    }
                    else
                    {
                        creditsCell.totalLikesCountLbl.text = String(self.myPostsArray[indexPath.row].likesCount)
                    }
                    
                    if (self.myPostsArray[indexPath.row].location != "")
                    {
                        creditsCell.locationIcon.isHidden = false
                        creditsCell.userNameLbl_Y_Constraint.constant = 5
                    }else{
                        creditsCell.locationIcon.isHidden = true
                        creditsCell.userNameLbl_Y_Constraint.constant = (creditsCell.moodTypeIcon.frame.origin.y + creditsCell.moodTypeIcon.frame.size.height)/3
                    }
                    
                    creditsCell.userLocationLbl.text = self.myPostsArray[indexPath.row].location
                    creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                    
                    if  self.myPostsArray[indexPath.row].replyCount == "0"
                    {
                        creditsCell.totalReplyCountLbl.text = ""
                    }
                    else
                    {
                        creditsCell.totalReplyCountLbl.text = self.myPostsArray[indexPath.row].replyCount
                    }
                    
                    creditsCell.confessionLikesBtn.tag = indexPath.row
                    creditsCell.confessionLikesBtn.addTarget(self, action: #selector(ProfileViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionReplyBtn.tag = indexPath.row
                    creditsCell.confessionReplyBtn.addTarget(self, action: #selector(ProfileViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)
 
                    creditsCell.dropDownBtn.tag = indexPath.row
                    creditsCell.dropDownBtn.addTarget(self, action: #selector(ProfileViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewFullImageBtn.tag = indexPath.row
                    creditsCell.viewFullImageBtn.addTarget(self, action: #selector(ProfileViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                    
                    return creditsCell
                }
            }
            else
            {
                let onlineUsersCell = tableView.dequeueReusableCell(withIdentifier: "statusOnlyCell", for: indexPath) as! ConfessionStatusTableViewCell
                
                if (self.myPostsArray.count <= 0)
                {
                    return onlineUsersCell
                }
                
                onlineUsersCell.selectionStyle = UITableViewCellSelectionStyle.none
                onlineUsersCell.confessionLikeIcon.tag = indexPath.row
                onlineUsersCell.moodIcon.tag = indexPath.row
                
                DispatchQueue.main.async {
                    
                    if self.getFullDataStr == "fetchAllData"
                    {
                        if (self.myPostsArray[indexPath.row].likedByMeStatus == "yes")
                        {
                            onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            //confessionLikeIcon
                            onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                    else
                    {
                       
                    }
                }
                
                let genderStr = self.myPostsArray[indexPath.row].gender
                var gender = String()
                if genderStr == "1"
                {
                    gender = "Male"
                }
                else
                {
                    gender = "Female"
                }
                
                onlineUsersCell.moodIcon.layer.cornerRadius = onlineUsersCell.moodIcon.frame.size.height/2
                onlineUsersCell.moodIcon.clipsToBounds = true
                onlineUsersCell.moodIcon.layer.masksToBounds = true
                onlineUsersCell.moodIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.myPostsArray[indexPath.row].typeOfConfession == "anonymous")
                {
                    onlineUsersCell.userDetailsLbl.text = gender + ", " + self.myPostsArray[indexPath.row].age
                    
                     onlineUsersCell.moodIcon.sd_setImage(with: URL(string: self.myPostsArray[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                    
                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = onlineUsersCell.moodIcon.bounds
                    onlineUsersCell.moodIcon.addSubview(blurEffectView)
                }
                else
                {
                    onlineUsersCell.userDetailsLbl.text = self.myPostsArray[indexPath.row].name
                    
                    for subview in onlineUsersCell.moodIcon.subviews
                    {
                        if subview.isKind(of: UIVisualEffectView.self)
                        {
                            subview.removeFromSuperview()
                        }
                    }
                    
                     onlineUsersCell.moodIcon.sd_setImage(with: URL(string: self.myPostsArray[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                }
                
                var endDateForConfession = String(self.myPostsArray[indexPath.row].totalTime)
                endDateForConfession = endDateForConfession! + " UTC"
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone.current
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                let datee = dateFormatter.date(from: endDateForConfession!)!
                
                onlineUsersCell.decreasePostTimeLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                onlineUsersCell.decreasePostTimeLbl.adjustsFontSizeToFitWidth = true
                
                if self.myPostsArray[indexPath.row].likesCount == "0"
                {
                    onlineUsersCell.likeCountLbl.text = ""
                }
                else
                {
                    onlineUsersCell.likeCountLbl.text = String(self.myPostsArray[indexPath.row].likesCount)
                }
                
                if self.myPostsArray[indexPath.row].replyCount == "0"
                {
                    onlineUsersCell.replyCountLbl.text = ""
                }
                else
                {
                    onlineUsersCell.replyCountLbl.text = self.myPostsArray[indexPath.row].replyCount
                }
                
                if (self.myPostsArray[indexPath.row].location != "")
                {
                    onlineUsersCell.locationIcon.isHidden = false
                    onlineUsersCell.userDetailsLbl_Y_Frame.constant = 5
                }else{
                    onlineUsersCell.locationIcon.isHidden = true
                    onlineUsersCell.userDetailsLbl_Y_Frame.constant = (onlineUsersCell.moodIcon.frame.origin.y + onlineUsersCell.moodIcon.frame.size.height)/3
                }
                
                onlineUsersCell.userLocationLbl.text = self.myPostsArray[indexPath.row].location
                onlineUsersCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                
//                onlineUsersCell.userPostedStatusLbl.text = self.myPostsArray[indexPath.row].statusAdded
                
                let inputString: NSString = self.myPostsArray[indexPath.row].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                
                onlineUsersCell.usersStatusTextView.text = modifiedString
                
                onlineUsersCell.postLikesBtn.tag = indexPath.row
                onlineUsersCell.postLikesBtn.addTarget(self, action: #selector(ProfileViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postReplyBtn.tag = indexPath.row
                onlineUsersCell.postReplyBtn.addTarget(self, action: #selector(ProfileViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)

                onlineUsersCell.postDropDownBtn.tag = indexPath.row
                onlineUsersCell.postDropDownBtn.addTarget(self, action: #selector(ProfileViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return onlineUsersCell
            }
        }
        else
        {
            let videosCell = tableView.dequeueReusableCell(withIdentifier: "videosCell", for: indexPath as IndexPath) as! VideosTableViewCell
            
            return videosCell
        }
    }
    
    
    
    
    // MARK: - ****** confessionReply_btn ******
    func confessionReply_btn(sender: UIButton!) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
        
        var dictForConfessions = [String: String]()
        dictForConfessions=["age": self.myPostsArray[sender.tag].age,
                            "gender": self.myPostsArray[sender.tag].gender,
                            "username": self.myPostsArray[sender.tag].name,
                            "location": self.myPostsArray[sender.tag].location,
                            "totalTime": String(self.myPostsArray[sender.tag].totalTime),
                            "typeOfConfession": self.myPostsArray[sender.tag].typeOfConfession,
                            "ownerProfilePic": self.myPostsArray[sender.tag].profilePic,
                            "statusAdded": self.myPostsArray[sender.tag].statusAdded
        ]
        homeScreen.ConfessionsDataDict = dictForConfessions
        homeScreen.checkReplyType = "replyOnPost"
        homeScreen.popInfoDelegate = self
        homeScreen.postIdOwnerStr = self.myPostsArray[sender.tag].userIDStr
        homeScreen.postIDStr = self.myPostsArray[sender.tag].postIDStr
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }

    
    
    // MARK: - ****** confessionLikes_btnAction ******
    func confessionLikes_btnAction(sender: UIButton!) {

        self.getReloadIndexPath = sender.tag
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
           if (self.myPostsArray.count > 0)
            {
                self.likeDislikeConfessions(confessionID: self.myPostsArray[sender.tag].postIDStr, completion: { (responseBool) in
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected = IndexPath()
                        self.indexPathSelected = IndexPath(row: sender.tag, section: 0)
                       
                        self.latestIntArr.add(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if (self.myPostsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.myPostsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.myPosts_tableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.myPostsArray[sender.tag].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text  = ""
                                    
                                    self.myPostsArray[sender.tag].likedByMeStatus = "yes"
                                    self.myPostsArray[sender.tag].likesCount = "1"
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikeLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.myPostsArray[sender.tag].likedByMeStatus = "yes"
                                        self.myPostsArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.myPostsArray[sender.tag].likesCount)! + 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.myPostsArray[sender.tag].likedByMeStatus = "yes"
                                        self.myPostsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.myPosts_tableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.myPostsArray[sender.tag].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.myPostsArray[sender.tag].likedByMeStatus = "yes"
                                    self.myPostsArray[sender.tag].likesCount = "1"
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikeLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.myPostsArray[sender.tag].likedByMeStatus = "yes"
                                        self.myPostsArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.myPostsArray[sender.tag].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.myPostsArray[sender.tag].likedByMeStatus = "yes"
                                        self.myPostsArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikeLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
                    
                        self.indexPathSelected = IndexPath()
                        self.indexPathSelected = IndexPath(row: sender.tag, section: 0)
                        
                        self.latestIntArr.remove(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if (self.myPostsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.myPostsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.myPosts_tableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.myPostsArray[sender.tag].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text = ""//String(totalLikes)
                                    
                                    self.myPostsArray[sender.tag].likedByMeStatus = "no"
                                    self.myPostsArray[sender.tag].likesCount = ""
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikeLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        onlineUsersCell.likeCountLbl.text = "" //String(totalLikes)
                                        
                                        self.myPostsArray[sender.tag].likedByMeStatus = "no"
                                        self.myPostsArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikeLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.myPostsArray[sender.tag].likesCount)! - 1
                                        
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            onlineUsersCell.likeCountLbl.text  = ""
                                            
                                            self.myPostsArray[sender.tag].likedByMeStatus = "no"
                                            self.myPostsArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = ""
                                        }
                                        else
                                        {
                                            self.myPostsArray[sender.tag].likedByMeStatus = "no"
                                            self.myPostsArray[sender.tag].likesCount = String(totalLikes)
                                            
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.myPosts_tableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.myPostsArray[sender.tag].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    creditsCell.totalLikesCountLbl.text = ""
                                    
                                    self.myPostsArray[sender.tag].likedByMeStatus = "no"
                                    self.myPostsArray[sender.tag].likesCount = ""
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikeLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                        
                                        self.myPostsArray[sender.tag].likedByMeStatus = "no"
                                        self.myPostsArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikeLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.myPostsArray[sender.tag].likesCount)! - 1
                                        
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            creditsCell.totalLikesCountLbl.text = ""
                                            
                                            self.myPostsArray[sender.tag].likedByMeStatus = "no"
                                            self.myPostsArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = ""
                                        }
                                        else
                                        {
                                            self.myPostsArray[sender.tag].likedByMeStatus = "no"
                                            self.myPostsArray[sender.tag].likesCount = String(totalLikes)
                                            
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikeLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
    
    // MARK: - ****** getRemainingTime ******
    func getRemainingTime(timestamp: Date, timeframe: String)->  String
    {
        let date22 = timestamp
        let formatter22 = DateFormatter()
        formatter22.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter22.timeZone = TimeZone.current
//        let postCreatedDate = formatter22.string(from: date22 as Date)
        
        // *** Create date ***
//        let date = NSDate()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let now = formatter.string(from: date as Date) //prints 12.21 AM
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current
        var TimeFrameAdded = String()
        if (timeframe.range(of: " ") != nil)
        {
            let fullUserID = timeframe.components(separatedBy: " ")
            let beforeAtTheRateStr: String = fullUserID[0]
            
            TimeFrameAdded = beforeAtTheRateStr
        }
        
//        var now3 = String()
        var dateStr = Date()
        if timeframe == "24 hrs"
        {
            dateStr = calendar.date(byAdding: .day, value: 1, to: date22 as Date)!
            let formatter3 = DateFormatter()
            formatter3.timeZone = TimeZone.current
            formatter3.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            now3 = formatter.string(from: dateStr as Date)
        }
        else
        {
            dateStr = calendar.date(byAdding: .hour, value: Int(TimeFrameAdded)!, to: date22 as Date)!
            let formatter3 = DateFormatter()
            formatter3.timeZone = TimeZone.current
            formatter3.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            now3 = formatter.string(from: dateStr as Date)
        }
        
        let nowDate = NSDate()

        let secondsFromNowToFinish = dateStr.timeIntervalSince(nowDate as Date)
        let hours = Int(secondsFromNowToFinish / 3600)
//        let minutes = Int((secondsFromNowToFinish - Double(hours) * 3600) / 60)
//        let seconds = Int(secondsFromNowToFinish - Double(hours) * 3600 - Double(minutes) * 60 + 0.5)
        
//        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        
        return String(format: "%02d", hours)
    }
    
    
    
    // MARK: - ****** DropDown_btnAction ******
    func DropDown_btnAction(sender: UIButton!) {
        
        if (self.myPostsArray.count > 0)
        {
            self.userIDToReportStr = self.myPostsArray[sender.tag].userIDStr
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
//                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
//                {
                    if (self.myPostsArray[sender.tag].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.myPostsArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.myPostsArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }

                        self.showNewActionSheetForDropDownMenu_owner(postIdStr: self.myPostsArray[sender.tag].postIDStr, status: self.myPostsArray[sender.tag].statusAdded, backgroudImageStr: self.myPostsArray[sender.tag].postedImage, moodTypeImageStr: self.myPostsArray[sender.tag].moodTypeStr, timeFrameStr: self.myPostsArray[sender.tag].postHours, moodTypeStr: self.myPostsArray[sender.tag].moodTypeStr, moodTypeUrl: self.myPostsArray[sender.tag].moodTypeStr, photoTypeStr: typeOfCOnfession, getTotalLikeCount: self.myPostsArray[sender.tag].likesCount, getTotalReplyCount: self.myPostsArray[sender.tag].replyCount, gifHeight: self.myPostsArray[sender.tag].width, gifWidth: self.myPostsArray[sender.tag].height, postStatusStr: self.myPostsArray[sender.tag].statusAdded)
                    }
                    else
                    {
                       
                    }
//                }
            }
        }
    }
    
    //MARK:- ****** viewFullSizeImageACtion  ******
    func viewFullSizeImageACtion(sender: UIButton!)
    {
        self.indexToClick = sender.tag
        self.appDelegatePush = "onBtnTap"
        
        if (self.myPostsArray.count <= 0)
        {
            return
        }
        
        if (self.myPostsArray[self.indexToClick].postedImage != "")
        {
            let imageURL = self.myPostsArray[self.indexToClick].postedImage
            if (imageURL.contains("jpg"))
            {
                self.fullSizeImageConfession.sd_setImage(with: URL(string: self.myPostsArray[self.indexToClick].postedImage), placeholderImage: UIImage(named: "Default Icon"), options: []) { (image, error, imageCacheType, imageUrl) in

                    if ((image?.size.height) != nil) && ((image?.size.width) != nil)
                    {
                        self.viewFullSizePostView.backgroundColor = UIColor.clear
                        self.viewFullSizePostView.isHidden = false
                        self.fullSizeImageConfession.backgroundColor = UIColor.clear
                        
                        self.fullSizeImageConfession.isHidden = false
                        self.fullSizeAnimatedImageConfession.isHidden = true
                        
                        if (image?.size.height)! <= (self.postGifWrapperView.frame.size.height)
                        {
                            var gifWidth = Int()
                            var screenRes = Int()
                            var newXPos =  Int()
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  0
                            }
                            else{
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  screenRes - gifWidth
                            }
                            
                            let gifHeight = Int((image?.size.height)!/2)
                            let heightOfWrraperView = Int(self.postGifWrapperView.frame.height/2)
                            let newYPos: Int =  heightOfWrraperView - gifHeight
                            
                            self.postGifWrapperView.frame.origin.y = 0
                            self.fullSizeImageConfession.frame.origin.y = CGFloat(newYPos)
                            self.fullSizeImageConfession.frame.origin.x = CGFloat(newXPos)
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                self.fullSizeImageConfession.frame.size.width = self.view.frame.size.width
                            }
                            else{
                                self.fullSizeImageConfession.frame.size.width = (image?.size.width)!
                            }
                            
                            if (image?.size.height)! <= (self.postGifWrapperView.frame.size.height)
                            {
                                self.fullSizeImageConfession.frame.size.height = (image?.size.height)!
                            }
                            else{
                                self.fullSizeImageConfession.frame.size.height = self.postGifWrapperView.frame.size.height
                            }
                            
                            self.imagesButtonWrapperView.frame.origin.y = self.fullSizeImageConfession.frame.origin.y + self.fullSizeImageConfession.frame.size.height + 10
                            self.postGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                        }
                        else{
                            self.fullSizeImageConfession.frame.origin.y = 21
                            self.fullSizeImageConfession.frame.size.height = (self.postGifWrapperView.frame.size.height - 21)
                            
                            self.imagesButtonWrapperView.frame.origin.y = self.postGifWrapperView.frame.origin.y + self.postGifWrapperView.frame.size.height + 10
                            self.postGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                        }
                        
                        self.fullSizeImageConfession.layer.borderWidth = 2.0
                        self.fullSizeImageConfession.layer.masksToBounds = false
                        self.fullSizeImageConfession.layer.borderColor = UIColor.lightGray.cgColor
                        self.fullSizeImageConfession.contentMode = UIViewContentMode.scaleAspectFit
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Please wait.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                self.viewFullSizePostView.backgroundColor = UIColor.clear
                self.viewFullSizePostView.isHidden = false
                self.fullSizeImageConfession.backgroundColor = UIColor.clear
                
                self.fullSizeImageConfession.isHidden = true
                self.fullSizeAnimatedImageConfession.isHidden = false
                
                DispatchQueue.main.async {
                    self.fullSizeAnimatedImageConfession.sd_setImage(with: URL(string: self.myPostsArray[self.indexToClick].postedImage), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                }
                
                self.fullSizeAnimatedImageConfession.layer.borderWidth = 2.0
                self.fullSizeAnimatedImageConfession.layer.masksToBounds = false
                self.fullSizeAnimatedImageConfession.layer.borderColor = UIColor.lightGray.cgColor
                self.fullSizeAnimatedImageConfession.contentMode = UIViewContentMode.scaleAspectFit

                var gifWidth = Int()
                var screenRes = Int()
                var newXPos =  Int()
                if (self.myPostsArray[self.indexToClick].width) > Int(self.view.frame.size.width)
                {
                    gifWidth = Int(self.myPostsArray[self.indexToClick].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  0
                }
                else{
                    gifWidth = Int(self.myPostsArray[self.indexToClick].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  screenRes - gifWidth
                }
                
                let gifHeight = Int(self.myPostsArray[self.indexToClick].height/2)
                let heightOfWrraperView = Int(self.postGifWrapperView.frame.height/2)
                let newYPos: Int =  heightOfWrraperView - gifHeight
                
                self.postGifWrapperView.frame.origin.y = 0
                self.fullSizeAnimatedImageConfession.frame.origin.y = CGFloat(newYPos)
                self.fullSizeAnimatedImageConfession.frame.origin.x = CGFloat(newXPos)
                
                if (self.myPostsArray[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    self.fullSizeAnimatedImageConfession.frame.size.width = self.view.frame.size.width
                }
                else{
                    self.fullSizeAnimatedImageConfession.frame.size.width = CGFloat(self.myPostsArray[self.indexToClick].width)
                }
                
                self.fullSizeAnimatedImageConfession.frame.size.height = CGFloat(self.myPostsArray[self.indexToClick].height)
                
                self.imagesButtonWrapperView.frame.origin.y = self.fullSizeAnimatedImageConfession.frame.origin.y + self.fullSizeAnimatedImageConfession.frame.size.height + 10
                self.postGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
            }
        }
        
        DispatchQueue.main.async {
            
            if self.getFullDataStr == "fetchAllData"
            {
                if (self.myPostsArray[sender.tag].likedByMeStatus == "yes")
                {
                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
            else{
                if self.latestIntArr.contains(sender.tag)
                {
                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
        }
        
        if self.myPostsArray[sender.tag].likesCount == "0"
        {
            self.confessionLikeLbl.text = ""
        }
        else //if self.myPostsArray[sender.tag].likesCount > 0
        {
            self.confessionLikeLbl.text = String(self.myPostsArray[sender.tag].likesCount)
        }
        
        
        
        if Int(self.myPostsArray[sender.tag].replyCount)! == 0
        {
            self.confessionReplyLbl.text = ""
        }
        else if Int(self.myPostsArray[sender.tag].replyCount)! > 0
        {
            self.confessionReplyLbl.text = self.myPostsArray[sender.tag].replyCount
        }
        else
        {
            self.confessionReplyLbl.text = ""
        }
    }
    
    // MARK: - ****** deleteConfession ******
    func deleteConfessionFromDatabase(postIDStr: String, replyID: String,replyOnReplyIDStr: String)
    {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "Delete Wi Post", message: "Do you really want to delete post?", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }
        actionSheetController.addAction(cancelActionButton)
        
        let reportActionButton = UIAlertAction(title: "Delete", style: .destructive) { action -> Void in
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
               
                self.deleteParticularConfession(postIDToDelete: postIDStr, completion: { (result) in
                    
                    if result == true
                    {
                        self.getAllMyConfessions(paginationStr: "1") { (responseBool) in
                            if (responseBool == true)
                            {
                                if (self.myPostsArray.count > 0)
                                {
                                    DispatchQueue.main.async {
                                        self.noDataFoundLbl.isHidden = true
                                        self.noPostsView.isHidden = true
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.myPosts_tableView.reloadData()
                                    }
                                }
                                else
                                {
                                    self.myPostsArray = []
                                    self.loadingView.isHidden = true
                                    self.noDataFoundLbl.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.myPosts_collectionView.isHidden = true
                                    self.noPostsView.isHidden = false
                                    self.noDataFoundLbl.isHidden = true
                                    self.noPostsView.frame.size.height = 60
                                    
                                    self.myPostsView.frame = CGRect(x: self.myPostsView.frame.origin.x , y: self.myPostsView.frame.origin.y, width: self.myPostsView.frame.size.width, height: self.about_wrapperScrollView.contentSize.height)
                                    
                                    self.myPosts_tableView.frame = CGRect(x: self.myPosts_tableView.frame.origin.x , y:0, width: self.myPosts_tableView.frame.size.width, height: self.myPostsView.frame.size.height)
                                    
                                    self.wrapperScrollView.isScrollEnabled = true
                                    self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                                    
                                    self.myPostsView.isHidden = false
                                    self.myPosts_tableView.isHidden = true
                                    self.noPostsView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                        self.noPostsView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                    }, completion: { (finished) in
                                        
                                    })
                                }
                            }
                            else
                            {
                                self.myPostsArray = []
                                self.loadingView.isHidden = true
                                self.noDataFoundLbl.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.myPosts_collectionView.isHidden = true
                                self.noPostsView.isHidden = false
                                
                                self.noPostsView.frame.size.height = 60
                                
                                self.myPostsView.frame = CGRect(x: self.myPostsView.frame.origin.x , y: self.myPostsView.frame.origin.y, width: self.myPostsView.frame.size.width, height: self.about_wrapperScrollView.contentSize.height)
                                
                                self.myPosts_tableView.frame = CGRect(x: self.myPosts_tableView.frame.origin.x , y:0, width: self.myPosts_tableView.frame.size.width, height: self.myPostsView.frame.size.height)
                                
                                self.wrapperScrollView.isScrollEnabled = true
                                //                    self.wrapperScrollView.contentSize.height = self.myPostsView.frame.origin.y + self.myPosts_tableView.frame.height
                                self.wrapperScrollView.contentSize.height = self.about_wrapperScrollView.frame.origin.y + self.about_wrapperScrollView.contentSize.height
                                
                                self.myPostsView.isHidden = false
                                self.myPosts_tableView.isHidden = true
                                self.noPostsView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                    self.noPostsView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                }, completion: { (finished) in
                                    
                                })
                            }
                        }
                    }
                })
               
            }
        }
        
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
        actionSheetController.addAction(reportActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func deleteParticularConfession(postIDToDelete: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    ApiHandler.deleteConfessions(userID: userID, userToken: authToken, confession_id: postIDToDelete, completion: { (responseData) in
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    
    
    // MARK: - ****** Show Action Sheet From select options. ******
    func showActionSheetForDropDownMenu_owner(postIdStr: String, status: String, backgroudImageStr : String, moodTypeImageStr: String, timeFrameStr: String, moodTypeStr: String, moodTypeUrl: String, photoTypeStr: String,replyIDStr: String,getTotalLikeCount: String, getTotalReplyCount: String,gifWidth: Int,gifHeight: Int, postStatusStr: String) {
        
        let actionSheet = UIAlertController(title: "Please choose your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Edit", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let createConfession = storyboard.instantiateViewController(withIdentifier: "WiCreateConfessionViewController") as! WiCreateConfessionViewController
            createConfession.updateStr = "EditConfession"
            createConfession.updateMoodUrl = moodTypeUrl
            createConfession.updateTimeframe = timeFrameStr
            createConfession.updatePhotoTypeStr = photoTypeStr
            createConfession.updateStatusText = status
            createConfession.updateBackgroundImageUrl = backgroudImageStr
            createConfession.updateMoodTypeStr = moodTypeStr
            createConfession.updatePostID = postIdStr
            createConfession.updateReplyID = replyIDStr
            createConfession.postTypeStr = "CreateNewPost"
            createConfession.updateReplyToReplyID = ""
            createConfession.updateLikeCountStr = getTotalLikeCount
            createConfession.updateReplyCountStr = getTotalReplyCount
            createConfession.updateGifWidth = gifWidth
            createConfession.updateGifHeight = gifHeight
            self.navigationController?.pushViewController(createConfession, animated: true)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            if postIdStr != ""
            {
               self.deleteConfession(postIDStr: postIdStr)
            }
            
        }))
     
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
 
    //MARK:- ****** deleteConfession ******
    func deleteConfession(postIDStr: String)
    {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "Delete Wi Post", message: "Do you really want to delete post?", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }
        actionSheetController.addAction(cancelActionButton)
        
        let reportActionButton = UIAlertAction(title: "Delete", style: .destructive) { action -> Void in
           
          
        }
        
        actionSheetController.addAction(reportActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    
    // MARK: - ****** DidReceiveMemoryWarning ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
