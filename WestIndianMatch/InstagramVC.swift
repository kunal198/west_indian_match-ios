//
//  InstagramVC.swift
//  ZumeSpot
//
//  Created by mrinal khullar on 9/1/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class InstagramVC: UIViewController, UIWebViewDelegate,CLLocationManagerDelegate
{
    // MARK: - ****** Outlets ******
    var JsonResponseDict = NSMutableDictionary()
    var dataModel = NSData()
    var instaUserBioDict = NSDictionary()
    var fullNameInstagram = String()
    var instagramUserID = String()
    var instaUserProfilePic = String()
    var instaUserName = String()
    var instaUserWebSite = String()
    var locationManager: CLLocationManager!
    var currentLatitude = Double()
    var currentLongitude = Double()
    var countryStr = String()
    var currentLocationCountry = String()
    var city = String()
    var state = String()
    var localityStr = String()
    var subLocalityStr = String()
    
    
     var namearray=NSMutableArray()
     let appdele=AppDelegate()
    
    var getConversationStr = String()
    var getConversationAnonymousStr = String()
    
    /*var kBaseURL = "https://instagram.com/"
    var kAuthenticationURL="oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=token&scope=likes+comments+basic"
    var kClientID = "bf7c3d28453a411e85e1458413f2d80f"
    var kRedirectURI="http://www.brihaspatiinfotech.com"
    var kAccessToken="access_token"
    var typeOfAuthentication = ""*/
    
    
    var kBaseURL = "https://instagram.com/"
    var kAuthenticationURL="oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=token&scope=likes+comments+basic"
    var kClientID = "27bd360f951a4a83938dbe7aa0627db1"
    var kRedirectURI="http://example.com"//"https://wimatchapp.com"
    var kAccessToken="access_token"
    var typeOfAuthentication = ""
    
    var sorteddistance=[AnyObject]()
    
    // MARK: - ****** Outlets ******
    @IBOutlet weak var web_view: UIWebView!
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var loadingImage: UIImageView!
    
    
    // MARK: - ****** ViewDidLoad ******
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.web_view.delegate=self
        deleteChache()
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(InstagramVC.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    //MARK:- ****** didChangeAuthorization ******
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined , .denied , .restricted :
            
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
            }
            
            break
            
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
      /*  default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
    }
    
    // MARK: - ****** didUpdateLocations ******
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.locationManager.location != nil
        {
            if let getCoordinates = manager.location?.coordinate
            {
                print("getCoordinates = \(getCoordinates)")
                
                let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.locationManager.stopUpdatingLocation()
                
                if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                    let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in // this is the last line that is being called
                        var placemark : CLPlacemark!
                        placemark = placemarks?[0]
                        if placemark != nil
                        {
                            var city = String()
                            if (placemark.addressDictionary?["City"] != nil)
                            {
                                city = (placemark.addressDictionary?["City"] as! String)
                            }
                            
                            if (placemark.addressDictionary?["Country"] != nil)
                            {
                                self.currentLocationCountry = (placemark.addressDictionary?["Country"] as! String)
                            }
                            
                            var state = String()
                            if (placemark.addressDictionary?["State"] != nil)
                            {
                                state = (placemark.addressDictionary?["State"] as! String)
                            }
                            
                            self.city = city
                            self.state = state
                            
                            self.countryStr = city + "," + state
                            
                            var vv =  String()
                            if (placemark.subLocality != nil)
                            {
                                vv =  placemark.subLocality!
                            }
                            
                            var countWhiteSpaces = Int()
                            if (vv.range(of: " ") != nil)
                            {
                                countWhiteSpaces = (vv.components(separatedBy: " ").count) - 1
                            }
                            else{
                                countWhiteSpaces = 0
                            }
                            
                            var objAtIndexBeforeLast = String()
                            var objAtIndexLast = String()
                            
                            if (countWhiteSpaces > 0)
                            {
                                var fullNameArr = vv.components(separatedBy: " ")
                                objAtIndexBeforeLast = (fullNameArr[(fullNameArr.count)-2])
                                objAtIndexLast = (fullNameArr.last)!
                                self.subLocalityStr = objAtIndexBeforeLast + " " + objAtIndexLast
                            }
                            else
                            {
                                self.subLocalityStr = vv
                            }
                            
                            if (placemark.locality != nil)
                            {
                                self.localityStr = placemark.locality!
                            }
                        }
                    }
                }
            }
        }
    }
    
    

    // MARK: - ****** Delete Chache Method ******
    func deleteChache() {
        let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! as [HTTPCookie]{
            NSLog("cookie.domain = %@", cookie.domain)
            
            if cookie.domain == "www.instagram.com" ||
                cookie.domain == "api.instagram.com"{
                
                cookieJar.deleteCookie(cookie)
            }
        }
    }
    
    // MARK: - ****** DidReceiveMemoryWarning ******
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
     }
    
    
    
    // MARK: - ****** ViewWillAppear ******
     override func viewWillAppear(_ animated: Bool)
     {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
         
            var authURL: String? = nil
            
            if (typeOfAuthentication == "UNSIGNED")
            {
                authURL = "\(appdele.INSTAGRAM_AUTHURL)?client_id=\(appdele.INSTAGRAM_CLIENT_ID)&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&response_type=token&scope=\(appdele.INSTAGRAM_SCOPE)&DEBUG=True"
            }
            else
            {
                authURL = "\(appdele.INSTAGRAM_AUTHURL)?client_id=\(appdele.INSTAGRAM_CLIENT_ID)&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&response_type=code&scope=\(appdele.INSTAGRAM_SCOPE)&DEBUG=True"
            }
            web_view.loadRequest(NSURLRequest(url: NSURL(string: authURL!)! as URL) as URLRequest)
            web_view.delegate = self

        }
    }
    
    
    // MARK: - ****** Get Network Status Reachibility Method ******
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        let getNetworkStatus = (userInfo?["Status"] as? String)!
        if getNetworkStatus == "Offline"
        {

        }
    }
    
    
    
    // MARK: - ****** Back Button Action ******
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    
    //MARK:- ****** UIWEBVIEW DELEGATES ******
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
       self.loadingView.isHidden = false
       self.view.isUserInteractionEnabled = false
        return self.checkRequestForCallbackURL(request: request as NSURLRequest)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
         self.loadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
        web_view.layer.removeAllAnimations()
        web_view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.1, animations: {() -> Void in
            //  loginWebView.alpha = 0.2;
        })
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
        web_view.layer.removeAllAnimations()
        web_view.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.1, animations: {() -> Void in
            //loginWebView.alpha = 1.0;
        })
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
        self.webViewDidFinishLoad(webView)
    }
    
    
    //MARK:- ****** CHECK REQUEST FOR CALLBACK URL ******
    func checkRequestForCallbackURL(request: NSURLRequest) -> Bool
    {
        let urlString = request.url!.absoluteString
        if (typeOfAuthentication == "UNSIGNED")
        {
            // check, if auth was succesfull (check for redirect URL)
            if urlString.hasPrefix(appdele.INSTAGRAM_REDIRECT_URI)
            {
                // extract and handle access token
                let range = (urlString as NSString).range(of: "#access_token=")
                self.handleAuth(authToken: ((urlString as NSString).substring(from: range.location + range.length)))
                return false
            }
        }
        else
        {
            if urlString.hasPrefix(appdele.INSTAGRAM_REDIRECT_URI)
            {
                // extract and handle access token
                let range = (urlString as NSString).range(of: "code=")
                self.makePostRequest(code: (urlString as NSString).substring(from: range.location + range.length))
                return false
            }
        }
        return true
    }
    
    
    
    
    //MARK:- ****** MAKE POST REQUEST ******
    func makePostRequest(code: String)
    {
        let post = "client_id=\(appdele.INSTAGRAM_CLIENT_ID)&client_secret=\(appdele.INSTAGRAM_CLIENTSERCRET)&grant_type=authorization_code&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&code=\(code)"
        let postData = post.data(using: String.Encoding.ascii, allowLossyConversion: true)!
        let postLength = "\(UInt(postData.count))"
        let requestData = NSMutableURLRequest(url: NSURL(string: "https://api.instagram.com/oauth/access_token")! as URL)
        requestData.httpMethod = "POST"
        requestData.setValue(postLength, forHTTPHeaderField: "Content-Length")
        requestData.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        requestData.httpBody = postData
  
        var urldata=NSData()
        let requestError: NSError? = nil
        let response : AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do{
            let urlData = try NSURLConnection.sendSynchronousRequest(requestData as URLRequest, returning: response)
            urldata=urlData as NSData
        }
        catch (let e) {
        }
        
        let dict = try! JSONSerialization.jsonObject(with: urldata as Data, options: JSONSerialization.ReadingOptions.allowFragments)

        instaUserBioDict = (dict as AnyObject).value(forKey:"user") as! NSDictionary
        fullNameInstagram = instaUserBioDict.value(forKey: "full_name") as! String
        instagramUserID = instaUserBioDict.value(forKey: "id") as! String
        instaUserProfilePic = instaUserBioDict.value(forKey: "profile_picture") as! String
        instaUserName = instaUserBioDict.value(forKey: "username") as! String
        instaUserWebSite = instaUserBioDict.value(forKey: "website") as! String
        var passwordStr = String()
        if fullNameInstagram.characters.count <= 6
        {
            passwordStr = self.fullNameInstagram + self.fullNameInstagram
        }
        else
        {
            passwordStr = self.fullNameInstagram
        }
        
        var deviceID = String()
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            deviceID = uuid
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            appDelegate.myDeviceToken = refreshedToken
        }
        
        let emailStr = self.instaUserName + "@gmail.com"
        ApiHandler.loginUser(withEmail: emailStr, password: "wima@123", myDeviceToken: appDelegate.myDeviceToken, deviceID: deviceID, device_type: "ios", completion: { (responseData) in
            
            if (responseData.value(forKey: "status") as? String == "200")
            {
                let dataDict = responseData.value(forKey: "data") as? NSDictionary
                let keysArray = dataDict?.allKeys as! [String]
                if keysArray.contains("purchased_package")
                {
                    let purchased_package_Array = dataDict?.value(forKey: "purchased_package") as! NSArray
                    if (purchased_package_Array.count <= 0)
                    {
                        UserDefaults.standard.setValue("packageNotPurchased", forKey: "checkPurchasedPackage")
                        UserDefaults.standard.synchronize()
                    }
                    else
                    {
                        for index in 0..<purchased_package_Array.count
                        {
                            var packagePurchasedDict = NSDictionary()
                            packagePurchasedDict = purchased_package_Array[index] as! NSDictionary
                            
                            if (packagePurchasedDict["packages"] != nil)
                            {
                                var packageDict = NSDictionary()
                                packageDict = packagePurchasedDict.value(forKey: "packages") as! NSDictionary
                                
                                if (packageDict["description"] != nil)
                                {
                                    let packageDetails = packageDict.value(forKey: "description") as! String
                                    
                                    UserDefaults.standard.setValue(packageDetails, forKey: "checkPurchasedPackageDetails")
                                    UserDefaults.standard.synchronize()
                                }
                            }
                        }
                        
                        UserDefaults.standard.setValue("packagePurchased", forKey: "checkPurchasedPackage")
                        UserDefaults.standard.synchronize()
                    }
                }
           
                UserDefaults.standard.set(dataDict, forKey: "userDetailsDict")
                UserDefaults.standard.set("12345", forKey: "passwordSaved")
                UserDefaults.standard.set("facebookUser", forKey: "userType")
                UserDefaults.standard.setValue(dataDict?.value(forKey: "auth_token")!, forKey: "auth_token")
                UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic")!, forKey: "userProfilePicture")
                UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic_thumb")!, forKey: "profile_pic_thumb")
                UserDefaults.standard.set(dataDict?.value(forKey: "username")!, forKey: "userName")
                UserDefaults.standard.set(dataDict?.value(forKey: "id")!, forKey: "UserAuthID")
                
                UserDefaults.standard.setValue(dataDict?.value(forKey: "firebase_user_id"), forKey: "firebase_user_id")
                
                UserDefaults.standard.set(dataDict?.value(forKey: "age") as? String, forKey: "Age")
                UserDefaults.standard.set(dataDict?.value(forKey: "location") as? String, forKey: "Location")
                UserDefaults.standard.set(dataDict?.value(forKey: "state") as? String, forKey: "state")
                UserDefaults.standard.synchronize()
                
                UserDefaults.standard.set(dataDict?.value(forKey: "location_sharing") as? String, forKey: "location_sharing")
                UserDefaults.standard.set(dataDict?.value(forKey: "wi_chat_room") as? String, forKey: "wi_chat_room")
                UserDefaults.standard.set(dataDict?.value(forKey: "post_likes") as? String, forKey: "post_likes")
                UserDefaults.standard.set(dataDict?.value(forKey: "new_matches") as? String, forKey: "new_matches")
                UserDefaults.standard.set(dataDict?.value(forKey: "messages") as? String, forKey: "messages")
                UserDefaults.standard.set(dataDict?.value(forKey: "comments") as? String, forKey: "comments")
                UserDefaults.standard.set(dataDict?.value(forKey: "profile_visitors") as? String, forKey: "profile_visitors")
                UserDefaults.standard.set(dataDict?.value(forKey: "profile_visibility") as? String, forKey: "profile_visibility")
                UserDefaults.standard.synchronize()
                
                let usersFirebaseID = dataDict?.value(forKey: "firebase_user_id") as! String
                
                let ref = FIRDatabase.database().reference()
                var userDetails = [String: Any]()
                userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                
                if let refreshedToken = FIRInstanceID.instanceID().token()
                {
                    userDetails["myDeviceToken"] = refreshedToken
                }
                
                ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                
                
                var dataDictForGeneralCategory = [String : Any]()
                let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                if (profile_visibility == "on")
                {
                    dataDictForGeneralCategory["Profile_Visibility"] = "On"
                }
                else{
                    dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                }
                ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                
                var settingsDict = [String : Any]()
                let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                if (chatRoomValue == "on")
                {
                    settingsDict["Wi_Chat_Room"] = "On"
                }
                else{
                    settingsDict["Wi_Chat_Room"] = "Off"
                }
                
                let messages = dataDict?.value(forKey: "messages") as? String
                if (messages == "on")
                {
                    settingsDict["Messages"] = "On"
                }
                else
                {
                    settingsDict["Messages"] = "Off"
                }
                
                ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                
                if (dataDict!["is_firsttime_login"] != nil)
                {
                    let is_firsttime_login = dataDict?.value(forKey: "is_firsttime_login") as? String
                    
                    if (is_firsttime_login == "yes")
                    {
                        self.getConversation(usersFirebaseID: usersFirebaseID, completion: { (resultConversations) in
                            
                            self.getAnonymousConversations(usersFirebaseID: usersFirebaseID, completion: { (resultAnonymousConversations) in
                                
                                let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                                UserDefaults.standard.set("success", forKey: "userRegistered")
                                UserDefaults.standard.set("instagramUser", forKey: "userType")
                                self.navigationController?.pushViewController(homeScreen, animated: true)
                            })
                        })
                        
                    }
                    else
                    {
                        let ref = FIRDatabase.database().reference()
                        var userDetails = [String: Any]()
                        userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                        userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                        userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                        userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                        userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                        userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                        userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                        
                        if let refreshedToken = FIRInstanceID.instanceID().token()
                        {
                            userDetails["myDeviceToken"] = refreshedToken
                        }
                        
                        ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                        
                        
                        var dataDictForGeneralCategory = [String : Any]()
                        let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                        if (profile_visibility == "on")
                        {
                            dataDictForGeneralCategory["Profile_Visibility"] = "On"
                        }
                        else{
                            dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                        }
                        ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                        
                        var settingsDict = [String : Any]()
                        let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                        if (chatRoomValue == "on")
                        {
                            settingsDict["Wi_Chat_Room"] = "On"
                        }
                        else{
                            settingsDict["Wi_Chat_Room"] = "Off"
                        }
                        
                        let messages = dataDict?.value(forKey: "messages") as? String
                        if (messages == "on")
                        {
                            settingsDict["Messages"] = "On"
                        }
                        else
                        {
                            settingsDict["Messages"] = "Off"
                        }
                        
                        ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                        
                        
                        let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                        UserDefaults.standard.set("success", forKey: "userRegistered")
                        UserDefaults.standard.set("instagramUser", forKey: "userType")
                        self.navigationController?.pushViewController(homeScreen, animated: true)
                    }
                }
                else
                {
                    let ref = FIRDatabase.database().reference()
                    var userDetails = [String: Any]()
                    userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                    userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                    userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                    userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                    userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                    userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                    userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                    
                    if let refreshedToken = FIRInstanceID.instanceID().token()
                    {
                        userDetails["myDeviceToken"] = refreshedToken
                    }
                    
                    ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                    
                    
                    var dataDictForGeneralCategory = [String : Any]()
                    let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                    if (profile_visibility == "on")
                    {
                        dataDictForGeneralCategory["Profile_Visibility"] = "On"
                    }
                    else{
                        dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                    }
                    ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                    
                    var settingsDict = [String : Any]()
                    let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                    if (chatRoomValue == "on")
                    {
                        settingsDict["Wi_Chat_Room"] = "On"
                    }
                    else{
                        settingsDict["Wi_Chat_Room"] = "Off"
                    }
                    
                    let messages = dataDict?.value(forKey: "messages") as? String
                    if (messages == "on")
                    {
                        settingsDict["Messages"] = "On"
                    }
                    else
                    {
                        settingsDict["Messages"] = "Off"
                    }
                    
                    ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                    
                    let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                    UserDefaults.standard.set("success", forKey: "userRegistered")
                    UserDefaults.standard.set("instagramUser", forKey: "userType")
                    self.navigationController?.pushViewController(homeScreen, animated: true)
                }
            }
            else if (responseData.value(forKey: "status") as? String == "405")
            {
//                let message = responseData.value(forKey: "message") as? String
                
                let registerScreen = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                registerScreen.typeOfUser = "instagramUser"
                registerScreen.getDisplayNameStr = self.fullNameInstagram
                registerScreen.getEmailStr = self.instaUserName + "@gmail.com"
                registerScreen.instagramUserIdAsPassword = self.instagramUserID
                registerScreen.userNameStr = self.instaUserName
                
                registerScreen.imageStr = self.instaUserProfilePic
                registerScreen.faceBookID = self.instagramUserID
                self.navigationController?.pushViewController(registerScreen, animated: true)
            }
            else if (responseData.value(forKey: "status") as? String == "400")
            {
                let message = responseData.value(forKey: "message") as? String
                
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    
    func getConversation(usersFirebaseID: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let databaseRef = FIRDatabase.database().reference()
        databaseRef.child("Users").child(usersFirebaseID).child("conversations").observe(FIRDataEventType.value, with: { (snapshot) in
            
            if (snapshot.exists())
            {
                self.getConversationStr = "conversationsExists"
                
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot
                {
                    let conversationDict = rest.value as! NSDictionary
                    
                    let typeOfUser = conversationDict.value(forKey: "userType") as! String
                    let opponentStatus = conversationDict.value(forKey: "opponentProfileVisibilityStatus") as! String
                    let myStatus = conversationDict.value(forKey: "myProfileVisibilityStatus") as! String
                    let location = conversationDict.value(forKey: "location") as! String
                    
                    let data = ["location": location, "userType": typeOfUser,"myProfileVisibilityStatus": myStatus, "opponentProfileVisibilityStatus": opponentStatus]
                    databaseRef.child("Users").child(usersFirebaseID).child("conversations").child(rest.key).updateChildValues(data)
                }
                
                completion(true)
            }
            else
            {
                self.getConversationStr = "conversationsDoesNotExists"
                completion(true)
            }
        })
    }
    
    
    func getAnonymousConversations(usersFirebaseID: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let databaseRef = FIRDatabase.database().reference()
        databaseRef.child("Users").child(usersFirebaseID).child("conversations_anonymous").observe(FIRDataEventType.value, with: { (snapshotAnonymousConvo) in
            
            if (snapshotAnonymousConvo.exists())
            {
                self.getConversationAnonymousStr = "conversationsExists"
                
                let enumerator = snapshotAnonymousConvo.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot
                {
                    let conversationDict = rest.value as! NSDictionary
                    
                    let typeOfUser = conversationDict.value(forKey: "userType") as! String
                    
                    let location = conversationDict.value(forKey: "location") as! String
                    
                    if conversationDict["checkRevealProfile"] != nil
                    {
                        let checkRevealProfile = conversationDict.value(forKey: "checkRevealProfile") as! String
                        
                        let data = ["location": location, "userType": typeOfUser,"checkRevealProfile": checkRevealProfile]
                        databaseRef.child("Users").child(usersFirebaseID).child("conversations_anonymous").child(rest.key).updateChildValues(data)
                    }
                    else
                    {
                        let data = ["location": location, "userType": typeOfUser]
                        databaseRef.child("Users").child(usersFirebaseID).child("conversations_anonymous").child(rest.key).updateChildValues(data)
                    }
                }
                
                completion(true)
            }
            else
            {
                self.getConversationAnonymousStr = "conversationsDoesNotExists"
                completion(true)
            }
        })
    }
    
    
    
    //MARK:- ****** HANDLE AUTH ******
    func handleAuth(authToken: String)
    {
//        print("successfully logged in with Token == \(authToken)")
    }
    
    
    
    //MARK:- ****** FETCH USER'S POST LISTING API ******
    func json(accessToken:NSString)
    {
        
        let jsonUrlPath = "https://api.instagram.com/v1/users/\(self.instagramUserID)/media/recent/?access_token=\(accessToken)"
        let url:NSURL = NSURL(string: jsonUrlPath)!
        let session  = URLSession .shared
        let task = session.dataTask(with: url as URL, completionHandler: {data,response,error -> Void in
            if error != nil
            {
                
            }
            
            var err:NSError?
            var itemsdict = NSMutableDictionary()
            do
            {
                itemsdict = try JSONSerialization.jsonObject(with: data! , options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            } catch _
            {
                
            }
            
            if err != nil
            {
//                print("Json Error\(err!.localizedDescription)")
            }
        })
        task.resume()
    }
}
