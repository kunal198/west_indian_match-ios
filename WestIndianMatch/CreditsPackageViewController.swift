//
//  CreditsPackageViewController.swift
//  WestIndianMatch
//
//  Created by brst on 23/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import StoreKit
import Firebase

class CreditsPackageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,SKProductsRequestDelegate,SKPaymentTransactionObserver,UIScrollViewDelegate,UIWebViewDelegate {
    
    //MARK:- ****** VARIABLES DECLARATION  ******
    var webViewBool = Bool()
    var termsBool = Bool()
    var selectBtnOnIndex = Int()
    var totalNumberOfTokensPurchased = Int()
    var profileTitleArray = [String]()
    var profileDescriptionArray = [String]()
    var profileImagesArray = [String]()
    var usersDetailsDict = NSMutableDictionary()
    var checkBuyProfileVisitor = String()
    var checkBuyLinkUp = String()
    var checkRelLikes = String()
    var checkProfileVisibility = String()
    var checkLinkupVisibility = String()
    
    var generalPackageStr = String()
    var statusCreditsPackagesStr = String()
    var vipPackageStr = String()
    var vvipPackageStr = String()
    var typeOfCreditStr = String()
    var headingLbl = UILabel()
    var descriptionLbl = UILabel()
    var titleImage = UIImageView()
    
    var getPackagesArray = NSArray()
    var idofSelectedPackage = String()
    
    var titleScrollImageArray = [String]()
    var packageArray = [String]()
    var totalCostArray = [String]()
    var titleArray = [String]()
    var scrollHeadingTitleArray = [String]()
    var scrollDescriptionArray = [String]()
    var timerForSlider = Timer()
    var totalCostArrayInfo = [String]()
    /* Variables */
    

    let GENERAL_PRODUCT_ID = "com.generall.wimatch"
    let VIP_PRODUCT_ID = "com.vip.wimatch"
    let VVIP_PRODUCT_ID = "com.vvip.wimatch"
    
    let SPOTLIGHT_PRODUCT_ID = "com.wimatch.spotlight"
    let PREMIUM_PRODUCT_ID = "com.wimatch.profileVisitors"
    let LINKUP_PRODUCT_ID = "com.wimatch.linkUp"
    let PROFILE_VISIBILITY_PRODUCT_ID = "com.wimatch.profileVisibility"
    let REL_LIKES_PODUCT_ID = "com.wimatch.relLikes"
    var productID = ""
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    var nonConsumablePurchaseMade = UserDefaults.standard.bool(forKey: "nonConsumablePurchaseMade")
    
    //MARK:- ****** OUTLETS DECLARATION  ******
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var packagesScrollView: UIScrollView!
    @IBOutlet weak var packageUpgradeIcon: UIImageView!
    @IBOutlet weak var unlockHeading_lbl: UILabel!
    @IBOutlet weak var totalTokensAvailable_Lbl: UILabel!
    @IBOutlet weak var wrapperViewRefill: UIView!
    @IBOutlet weak var credits_tableView: UITableView!
    @IBOutlet weak var creditsTitle_lbl: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var termsUnderPackagesView: UIView!
    @IBOutlet weak var informationTextView: UITextView!
    @IBOutlet weak var termsOnPackagesLbl: FRHyperLabel!
    @IBOutlet weak var wrapperScrollPackages: UIScrollView!
    @IBOutlet weak var AgreementLbl: FRHyperLabel!
    @IBOutlet weak var termsWrapperView: UIView!
    @IBOutlet weak var termsWebView: UIWebView!
    @IBOutlet weak var privacyLbl: UILabel!
    @IBOutlet weak var termsImage: UIImageView!
    @IBOutlet weak var termsView: UIView!
    @IBOutlet weak var purchasePackageBtn: UIButton!
    @IBOutlet weak var closeInAppViewBtn: UIButton!
    @IBOutlet weak var inAppInfo_txtView: UITextView!
    @IBOutlet weak var planTypeLbl: UILabel!
    @IBOutlet weak var packageTitleLbl: UILabel!
    @IBOutlet weak var inAppInfoView: UIView!
    @IBOutlet weak var inAppInfo_scroll: UIScrollView!
    
    
    // MARK: - ****** ViewDidLoad. ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webViewBool = false
        termsBool = false
        self.unlockHeading_lbl.isHidden = true
        self.creditsTitle_lbl.isHidden = true
        self.packageUpgradeIcon.isHidden = true
        
        self.totalTokensAvailable_Lbl.isHidden = true
        
        self.profileTitleArray = ["General","VIP","VVIP"]
        self.profileDescriptionArray = ["1 Month For $10.00","6 Months For $50.00 save $10.00","12 Months For $100.00 save $20.00"]
        self.profileImagesArray = ["Credit Icon 1","Credit Icon 2","Credit Icon 3"]
        
        self.scrollHeadingTitleArray = ["Rel Vibes","Link up","Invisibility","Wi Visitors"]
        self.scrollDescriptionArray = ["Stand out from the crowd and get noticed! \n Send users Rel Vibes and be automatically seen.","See who said YES and may want to link up for drinks, fete or more with you!","Shhhhhh!\nGo unnoticed and view profiles and chats privately, when you’re invisible.","See who visited your profile and may be interesed."]
        self.titleScrollImageArray = ["Upgrade 2","Upgrade 3","Upgrade 4","Upgrade 1"]
        
        self.packageArray = ["12","6","1"]
        self.titleArray = ["VVIP","VIP","General"]
        self.totalCostArray = ["Save $20.00 \n$99.99/12 Months","Save $10.00 \n$49.99/6 Months","$9.99/Month"]
        self.totalCostArrayInfo = ["@ $99.99/12 Months (Save $20.00)","@ $49.99/6 Months (Save $10.00)","$9.99/Month"]
        
        if UserDefaults.standard.bool(forKey: "spotlightPurchased") == true{
            print("spotlight already purchased")
        }

        let normalText = " "
        let boldText = "terms of use and privacy policy"
        let attributedString = NSMutableAttributedString(string:normalText)
        let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15)]
        let boldString = NSMutableAttributedString(string:boldText, attributes:attrs)
        attributedString.append(boldString)
        
        self.AgreementLbl.attributedText = attributedString
        self.termsOnPackagesLbl.attributedText = attributedString
        
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            if substring == "terms of use"
            {
                self.webViewBool = true
                
                UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                    self.termsWrapperView.isHidden = false
                    self.privacyLbl.text = "Terms of use"
                }, completion: nil)
                
                 let setURL=URL(string:"https://wimatchapp.com/terms-of-service")
                 let setURLRequest=URLRequest(url:setURL!)
                 self.termsWebView.loadRequest(setURLRequest)
            }
            else if substring == "privacy policy"
            {
                self.webViewBool = true
   
                UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                    self.termsWrapperView.isHidden = false
                    self.privacyLbl.text = "Privacy Policy"
                }, completion: nil)
    
                 let setURL=URL(string:"https://wimatchapp.com/privacy-policy")
                 let setURLRequest=URLRequest(url:setURL!)
                 self.termsWebView.loadRequest(setURLRequest)
            }
        }
        
        self.AgreementLbl.setLinksForSubstrings(["terms of use" ,"privacy policy"], withLinkHandler: handler)
        self.termsOnPackagesLbl.setLinksForSubstrings(["terms of use" ,"privacy policy"], withLinkHandler: handler)
        
        
        self.getScrollElements()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.view.isUserInteractionEnabled = false
            self.loadingView.isHidden = false
            
            ApiHandler.getDefaultTokenPackages(completion: { (responseData) in
                
                self.fetchAvailableProducts()
                
                if (responseData.value(forKey: "status") as? String == "200")
                {
                    let dataDict = responseData.value(forKey: "data") as? NSDictionary
                    if (dataDict!["packages"] != nil)
                    {
                        self.getPackagesArray = (dataDict?.value(forKey: "packages") as? NSArray)!
                    }
                    
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                }
                else if (responseData.value(forKey: "status") as? String == "400")
                {
                    let message = responseData.value(forKey: "message") as? String
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    self.getPackagesArray = []
                    let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    self.getPackagesArray = []
                    let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
            })
        }
        
        if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
        {
            if (checkPackageStatus == "packagePurchased")
            {
                if let descriptionPackage = UserDefaults.standard.value(forKey: "checkPurchasedPackageDetails") as?  String
                {
                    if (descriptionPackage == "General")
                    {
                        self.statusCreditsPackagesStr = "generalCreditsPurchased"
                    }
                    else if (descriptionPackage == "VIP")
                    {
                        self.statusCreditsPackagesStr = "vipCreditsPurchased"
                    }
                    else if (descriptionPackage == "VVIP")
                    {
                        self.statusCreditsPackagesStr = "vvipCreditsPurchased"
                    }
                }
            }
        }
    }
    
    
    // MARK: - ****** getScrollElements. ******
    func getScrollElements()
    {
        self.packagesScrollView.frame = CGRect(x:0, y: self.packagesScrollView.frame.origin.y, width:self.view.frame.width, height:self.packagesScrollView.frame.height)
        
        var widthMultipliier = CGFloat()
        var x_pos = CGFloat()
        x_pos=0;
        
        var x_pos_Image = CGFloat()
        x_pos_Image=self.packageUpgradeIcon.frame.origin.x;
        
        
        for index in 0..<self.scrollHeadingTitleArray.count
        {
            widthMultipliier = CGFloat(index)
            
            self.headingLbl = UILabel(frame:CGRect(x:x_pos, y:self.unlockHeading_lbl.frame.origin.y,width:self.unlockHeading_lbl.frame.size.width, height:self.unlockHeading_lbl.frame.size.height))
            
            self.descriptionLbl = UILabel(frame:CGRect(x:x_pos, y:self.creditsTitle_lbl.frame.origin.y,width:self.creditsTitle_lbl.frame.size.width, height:self.creditsTitle_lbl.frame.size.height))

            self.titleImage = UIImageView(frame:CGRect(x:x_pos_Image, y:self.packageUpgradeIcon.frame.origin.y,width:self.packageUpgradeIcon.frame.size.width, height:self.packageUpgradeIcon.frame.size.height))

            self.titleImage.image = UIImage(named: self.titleScrollImageArray[index])
            
            self.titleImage.layer.borderWidth = 3.0
            self.titleImage.layer.masksToBounds = false
            self.titleImage.clipsToBounds = true
            self.titleImage.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
            self.titleImage.layer.cornerRadius = 13
            self.titleImage.layer.cornerRadius = self.titleImage.frame.size.height/2
            
            self.titleImage.contentMode = UIViewContentMode.scaleAspectFit
            
            self.pageControl.isHidden = false
            
            self.headingLbl.text = self.scrollHeadingTitleArray[index]
            self.headingLbl.font = UIFont.boldSystemFont(ofSize: 16)
            self.headingLbl.textColor = UIColor.white
            self.headingLbl.textAlignment = NSTextAlignment.center
            
            self.descriptionLbl.text = self.scrollDescriptionArray[index]
            self.descriptionLbl.font = UIFont.systemFont(ofSize: 14)
            self.descriptionLbl.textColor = UIColor.white
            self.descriptionLbl.textAlignment = NSTextAlignment.center
            self.descriptionLbl.numberOfLines = 10

            self.packagesScrollView.addSubview(self.headingLbl)
            self.packagesScrollView.addSubview(self.descriptionLbl)
            self.packagesScrollView.addSubview(self.titleImage)
   
            x_pos = x_pos+self.packagesScrollView.frame.width;
            x_pos_Image = x_pos_Image + self.packagesScrollView.frame.width;
            
            self.packagesScrollView.contentSize = CGSize(width:self.packagesScrollView.frame.width * widthMultipliier+self.packagesScrollView.frame.width, height:self.packagesScrollView.frame.height)
        }
        
        self.packagesScrollView.delegate = self
        self.pageControl.currentPage = 0
        self.pageControl.numberOfPages = self.scrollHeadingTitleArray.count
        
        if !timerForSlider.isValid
        {
            timerForSlider =  Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.moveToNextPage), userInfo: nil, repeats: true)
        }
    }
    
    // MARK: - ****** UIWebView Delegates ******
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        self.loadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
    }

    
    // MARK: - ****** Scroll Images Automatically. ******
    func moveToNextPage (){
        let pageWidth:CGFloat = self.packagesScrollView.frame.width
        
        let totalCount: Int = self.scrollDescriptionArray.count
        let maxWidth:CGFloat = pageWidth * CGFloat(totalCount)
        let contentOffset:CGFloat = self.packagesScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        
        self.packagesScrollView.scrollRectToVisible(CGRect(x:slideToX, y:self.packagesScrollView.frame.origin.y, width:pageWidth, height:self.packagesScrollView.frame.height), animated: true)
    }
    
    //MARK: ****** UIScrollView Delegate ******
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = packagesScrollView.frame.width
        let currentPage:CGFloat = floor((packagesScrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        self.pageControl.currentPage = Int(currentPage);
    }


    
    @IBAction func closeInAppInfoView(_ sender: Any) {
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromRight, animations: {_ in
             self.inAppInfoView.isHidden = true
        }, completion: nil)
    }
    
    
    
    @IBAction func termsInAppBtn(_ sender: Any) {
        if termsBool == false
        {
            self.termsImage.image = UIImage(named: "Terms Agree Icon")
            termsBool = true
        }
        else
        {
            self.termsImage.image = UIImage(named: "Terms Disagree Icon")
            termsBool = false
        }
    }
    
    
    @IBAction func termsBackBtn(_ sender: Any) {
        if self.webViewBool == true
        {
            self.webViewBool = false
            UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                self.termsWrapperView.isHidden = true
                self.privacyLbl.text = " "
            }, completion: nil)
        }
    }
    
    @IBAction func purchaseInAppBtn(_ sender: Any) {
            if selectBtnOnIndex == 2{
                if self.statusCreditsPackagesStr == "generalCreditsPurchased"
                {
                    let alert = UIAlertController(title: "In-app purchases", message: "Already Purchased.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.statusCreditsPackagesStr == "vipCreditsPurchased"
                {
                    let alert = UIAlertController(title: "In-app purchases", message: "VIP Package is Already Purchased.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.statusCreditsPackagesStr == "vvipCreditsPurchased"
                {
                    let alert = UIAlertController(title: "In-app purchases", message: "VVIP Package is Already Purchased.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    if iapProducts.count >= 0
                    {
                        productID = GENERAL_PRODUCT_ID
                        for index in 0..<self.iapProducts.count
                        {
                            let firstProduct = iapProducts[index].productIdentifier
                            if firstProduct == GENERAL_PRODUCT_ID
                            {
                                self.loadingView.isHidden = false
                                self.view.isUserInteractionEnabled = false
                                self.credits_tableView.isUserInteractionEnabled = false
                                DispatchQueue.main.async {
                                    self.purchaseMyProduct(product: self.iapProducts[index])
                                }
                            }
                        }
                    }
                    else{
                        let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
            else if selectBtnOnIndex == 1
            {
                if self.statusCreditsPackagesStr == "generalCreditsPurchased"
                {
                    let alert = UIAlertController(title: "In-app purchases", message: "General Package is Already Purchased.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.statusCreditsPackagesStr == "vipCreditsPurchased"
                {
                    let alert = UIAlertController(title: "In-app purchases", message: "Already Purchased.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.statusCreditsPackagesStr == "vvipCreditsPurchased"
                {
                    let alert = UIAlertController(title: "In-app purchases", message: "VVIP Package is Already Purchased.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    if iapProducts.count >= 0
                    {
                        productID = VIP_PRODUCT_ID
                        for index in 0..<self.iapProducts.count
                        {
                            let firstProduct = iapProducts[index].productIdentifier
                            if firstProduct == VIP_PRODUCT_ID
                            {
                                self.loadingView.isHidden = false
                                self.view.isUserInteractionEnabled = false
                                self.credits_tableView.isUserInteractionEnabled = false
                                DispatchQueue.main.async {
                                    self.purchaseMyProduct(product: self.iapProducts[index])
                                }
                            }
                        }
                    }
                    else{
                        let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
            else if selectBtnOnIndex == 0
            {
                if self.statusCreditsPackagesStr == "generalCreditsPurchased"
                {
                    let alert = UIAlertController(title: "In-app purchases", message: "General Package is Already Purchased.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.statusCreditsPackagesStr == "vipCreditsPurchased"
                {
                    let alert = UIAlertController(title: "In-app purchases", message: "VIP Package is Already Purchased.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.statusCreditsPackagesStr == "vvipCreditsPurchased"
                {
                    let alert = UIAlertController(title: "In-app purchases", message: "Already Purchased.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    if iapProducts.count >= 0
                    {
                        productID = VVIP_PRODUCT_ID
                        for index in 0..<self.iapProducts.count
                        {
                            let firstProduct = iapProducts[index].productIdentifier
                            if firstProduct == VVIP_PRODUCT_ID
                            {
                                self.loadingView.isHidden = false
                                self.view.isUserInteractionEnabled = false
                                self.credits_tableView.isUserInteractionEnabled = false
                                DispatchQueue.main.async {
                                    self.purchaseMyProduct(product: self.iapProducts[index])
                                }
                            }
                        }
                    }
                    else{
                        let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else{
                let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
    }
    
    
    // MARK: - ****** ViewDidLayoutSubViews. ******
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.inAppInfo_scroll.isScrollEnabled=true
        self.wrapperScrollPackages.isScrollEnabled=true
        
        self.credits_tableView.frame =  CGRect(x: self.credits_tableView.frame.origin.x , y:0, width: self.credits_tableView.frame.size.width, height: self.credits_tableView.contentSize.height)
        
        self.informationTextView.frame.origin.y = self.credits_tableView.frame.origin.y + self.credits_tableView.frame.size.height
        
        let fixedWidth_info = self.informationTextView.frame.size.width
        self.informationTextView.sizeThatFits(CGSize(width: fixedWidth_info, height: CGFloat.greatestFiniteMagnitude))
        let newSize_info = self.informationTextView.sizeThatFits(CGSize(width: fixedWidth_info, height: CGFloat.greatestFiniteMagnitude))
        var newFrame_info = self.informationTextView.frame
        newFrame_info.size = CGSize(width: max(newSize_info.width, fixedWidth_info), height: newSize_info.height)
        self.informationTextView.frame = newFrame_info;
        
        if (UIScreen.main.bounds.width == 320)
        {
            self.termsUnderPackagesView.frame.origin.y = self.informationTextView.frame.origin.y + self.inAppInfo_txtView.frame.size.height - 10
        }
        else if (UIScreen.main.bounds.width == 375){
            self.termsUnderPackagesView.frame.origin.y = self.informationTextView.frame.origin.y + self.informationTextView.frame.size.height - 20
        }
        else{
            self.termsUnderPackagesView.frame.origin.y = self.informationTextView.frame.origin.y + self.informationTextView.frame.size.height - 30
        }
        
        self.wrapperScrollPackages.contentSize.height = self.termsUnderPackagesView.frame.origin.y + self.termsUnderPackagesView.frame.size.height + 50
        
        let fixedWidth = self.inAppInfo_txtView.frame.size.width
        self.inAppInfo_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = self.inAppInfo_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = self.inAppInfo_txtView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        self.inAppInfo_txtView.frame = newFrame;
        
        if (UIScreen.main.bounds.width == 320)
        {
            self.termsView.frame.origin.y = self.inAppInfo_txtView.frame.origin.y + self.inAppInfo_txtView.frame.size.height - 10
        }
        else if (UIScreen.main.bounds.width == 375){
            self.termsView.frame.origin.y = self.inAppInfo_txtView.frame.origin.y + self.inAppInfo_txtView.frame.size.height - 20
        }
        else{
             self.termsView.frame.origin.y = self.inAppInfo_txtView.frame.origin.y + self.inAppInfo_txtView.frame.size.height - 30
        }
        self.purchasePackageBtn.frame.origin.y = self.termsView.frame.origin.y + termsView.frame.size.height + 20
        self.closeInAppViewBtn.frame.origin.y = self.purchasePackageBtn.frame.origin.y + self.purchasePackageBtn.frame.size.height + 20
        self.inAppInfo_scroll.contentSize.height = self.closeInAppViewBtn.frame.origin.y + self.closeInAppViewBtn.frame.size.height + 50
    }
    
    
   
    
    // MARK: - ****** FETCH AVAILABLE IAP PRODUCTS ******
    func fetchAvailableProducts()  {
        // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects: GENERAL_PRODUCT_ID,VIP_PRODUCT_ID,VVIP_PRODUCT_ID)
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        productsRequest.delegate = self
        productsRequest.start()
        SKPaymentQueue.default().add(self)
        self.loadingView.isHidden = false
        self.credits_tableView.isUserInteractionEnabled = false
    }
    
    
    // MARK: - ****** REQUEST IAP PRODUCTS ******
   public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if (response.products.count > 0)
        {
            iapProducts = response.products
            self.loadingView.isHidden = true
            self.credits_tableView.isUserInteractionEnabled = true
            for index in 0..<self.iapProducts.count
            {
                print("index = \(index)")
//                let firstProduct = iapProducts[index].productIdentifier
            }
        }
        else {
            self.loadingView.isHidden = true
            self.credits_tableView.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
         }
    }

    
    // MARK: - ****** MAKE PURCHASE OF A PRODUCT ******
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    func purchaseMyProduct(product: SKProduct) {
        
        if self.canMakePurchases() {
            DispatchQueue.main.async {
                let payment = SKPayment(product: product)
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().add(payment)
            }
        } else {
            let alert = UIAlertController(title: "In-app purchases", message: "Purchases are disabled in your device!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - ****** ViewWillDisAppear. ******
    override func viewWillDisappear(_ animated: Bool) {
        SKPaymentQueue.default().remove(self)
    }
    
    // MARK:-  ****** IAP PAYMENT QUEUE ******
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchasing:
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    self.credits_tableView.isUserInteractionEnabled = false
                    break
                    
                case .purchased:
                    
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.credits_tableView.isUserInteractionEnabled = true
                    
                    if productID == GENERAL_PRODUCT_ID
                    {
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        case .online(.wwan) , .online(.wiFi):
                      
//                            let date1 = NSDate()
//                            let currentTimeStamp = Int64(date1.timeIntervalSince1970 * 1000)
                            let dateFormatter : DateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let date = Date()
                            let dateString = dateFormatter.string(from: date)
                            
                            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                            {
                                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                                {
                                    self.loadingView.isHidden = false
                                    self.view.isUserInteractionEnabled = false
                                    
                                    ApiHandler.purchaseUpgradePackages(userID: userID, authToken: userToken, tokenID: self.idofSelectedPackage, dateCreated: dateString, completion: { (responseData) in
                                        
                                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                                        
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.credits_tableView.isUserInteractionEnabled = true
                                        
                                        if (responseData.value(forKey: "status") as? String == "200")
                                        {
//                                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                            
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            
                                            let message = responseData.value(forKey: "message") as? String
                                            
                                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                                action in
                                                
                                                self.statusCreditsPackagesStr = "generalCreditsPurchased"
                                            UserDefaults.standard.setValue("packagePurchased", forKey: "checkPurchasedPackage")
                                                UserDefaults.standard.synchronize()
                                                
                                                self.loadingView.isHidden = true
                                                self.view.isUserInteractionEnabled = true
                                            self.credits_tableView.isUserInteractionEnabled = true
                                                self.credits_tableView.reloadData()
                                                
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
                                                
                                            }))
                                            
                                            self.present(alert, animated: true, completion: nil)
                                           
                                        }
                                        else if (responseData.value(forKey: "status") as? String == "400")
                                        {
                                            let message = responseData.value(forKey: "message") as? String
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    })
                                }
                            }
                        }
                    }
                     else if productID == VIP_PRODUCT_ID
                     {
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        case .online(.wwan) , .online(.wiFi):
                          
//                            let date1 = NSDate()
//                            let currentTimeStamp = Int64(date1.timeIntervalSince1970 * 1000)
                            
                            let dateFormatter : DateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let date = Date()
                            let dateString = dateFormatter.string(from: date)
                            
                            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                            {
                                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                                {
                                    self.loadingView.isHidden = false
                                    self.view.isUserInteractionEnabled = false
                                    
                                    ApiHandler.purchaseUpgradePackages(userID: userID, authToken: userToken, tokenID: self.idofSelectedPackage, dateCreated: dateString, completion: { (responseData) in
                                        
                                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.credits_tableView.isUserInteractionEnabled = true
                                        
                                        if (responseData.value(forKey: "status") as? String == "200")
                                        {
//                                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            
                                            let message = responseData.value(forKey: "message") as? String
                                            
                                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                                action in
                                                
                                                self.statusCreditsPackagesStr = "vipCreditsPurchased"
                                            UserDefaults.standard.setValue("packagePurchased", forKey: "checkPurchasedPackage")
                                                UserDefaults.standard.synchronize()
                                                
                                                self.loadingView.isHidden = true
                                                self.view.isUserInteractionEnabled = true
                                                self.credits_tableView.isUserInteractionEnabled = true
                                                self.credits_tableView.reloadData()
                                                
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
                                                
                                            }))
                                            self.present(alert, animated: true, completion: nil)
                                           
                                        }
                                        else if (responseData.value(forKey: "status") as? String == "400")
                                        {
                                            let message = responseData.value(forKey: "message") as? String
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    })
                                }
                            }
                        }
                    }
                     else if productID == VVIP_PRODUCT_ID
                     {
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        case .online(.wwan) , .online(.wiFi):
//                            let date1 = NSDate()
//                            let currentTimeStamp = Int64(date1.timeIntervalSince1970 * 1000)
                            
                            let dateFormatter : DateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let date = Date()
                            let dateString = dateFormatter.string(from: date)
                            
                            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                            {
                                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                                {
                                    self.loadingView.isHidden = false
                                    self.view.isUserInteractionEnabled = false
                                    
                                    ApiHandler.purchaseUpgradePackages(userID: userID, authToken: userToken, tokenID: self.idofSelectedPackage, dateCreated: dateString, completion: { (responseData) in
                                        
                                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.credits_tableView.isUserInteractionEnabled = true
                                        
                                        if (responseData.value(forKey: "status") as? String == "200") 
                                        {
//                                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                            
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            
                                            let message = responseData.value(forKey: "message") as? String
                                            
                                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                                action in
                                                
                                                self.statusCreditsPackagesStr = "vvipCreditsPurchased"
                                                
                                                UserDefaults.standard.setValue("packagePurchased", forKey: "checkPurchasedPackage")
                                                UserDefaults.standard.synchronize()
                                                
                                                self.loadingView.isHidden = true
                                                self.view.isUserInteractionEnabled = true
                                                self.credits_tableView.isUserInteractionEnabled = true
                                                self.credits_tableView.reloadData()
                                                
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
                                                
                                            }))
                                            self.present(alert, animated: true, completion: nil)
                                          
                                        }
                                        else if (responseData.value(forKey: "status") as? String == "400")
                                        {
                                            let message = responseData.value(forKey: "message") as? String
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    })
                                }
                            }
                        }
                    }
                    
                    break
                    
                case .failed:
                    
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.credits_tableView.isUserInteractionEnabled = true

                    let alert = UIAlertController(title: "Transaction Cancelled", message: "Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    break
                    
                case .restored:
                    
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.credits_tableView.isUserInteractionEnabled = true
                    break
                    
                default:
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.credits_tableView.isUserInteractionEnabled = true
                    break
                }}}
    }

    
    // MARK: - ****** Back Button Action. ******
    @IBAction func backBtn_action(_ sender: Any) {
        if self.typeOfCreditStr == "FromProfileScreen"
        {
            print("from profile screen itself.")
        }
        else{
            if self.typeOfCreditStr == "FromMeetUpScreen"
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FromMeetUpScreen"), object: nil, userInfo: nil)
            }
            else if self.typeOfCreditStr == "FromSettingsScreen"
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FromSettingsScreen"), object: nil, userInfo: nil)
            }
            else if self.typeOfCreditStr == "FromVisitorsScreen"
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FromVisitorsScreen"), object: nil, userInfo: nil)
            }
        }
        
        self.navigationController!.popViewController(animated: true)
    }
    
    
    // MARK: - ****** Cancel/Close RefillView Button Action. ******
    @IBAction func cancelRefillView_btnAction(_ sender: Any) {
        self.wrapperViewRefill.isHidden = true
    }
    
    
    // MARK: - ****** Refill Button Action. ******
    @IBAction func refill_btnAction(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Refill Coming Soon.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** Search Button Action. ******
    @IBAction func searchBth_Action(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Search Coming Soon.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - ****** UITableView Delegate & Data Source Methods. ******
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.packageArray.count
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath)as! CreditPackagesTableViewCell
        
        credits_tableView.separatorColor = UIColor.clear
        creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let cost = self.totalCostArray[indexPath.row]
        if let range = cost.range(of: "\n")
        {
            let firstPart = cost.substring(to: range.lowerBound)
            let secondStr = cost.substring(to: range.upperBound)
            let range = (cost as NSString).range(of: firstPart)
            
            let attribute = NSMutableAttributedString.init(string: cost)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0) , range: range)
            
            creditsCell.priceOfToken_lbl.attributedText = attribute
        }
        else
        {
            creditsCell.priceOfToken_lbl.text = self.totalCostArray[indexPath.row]
        }
        
        creditsCell.noOfToken_lbl.text = self.packageArray[indexPath.row]
        creditsCell.title_lbl.text = self.titleArray[indexPath.row]
        creditsCell.selectTypeOfTokenBtn.layer.cornerRadius = creditsCell.selectTypeOfTokenBtn.frame.size.height / 2
        creditsCell.selectTypeOfTokenBtn.tag = indexPath.row
        
        creditsCell.selectTypeOfTokenBtn.addTarget(self, action: #selector(CreditsPackageViewController.buyNow(sender:)), for: UIControlEvents.touchUpInside)
        
        creditsCell.getInformatinBtn.tag = indexPath.row
        creditsCell.getInformatinBtn.addTarget(self, action: #selector(CreditsPackageViewController.getInformationForInApp(sender:)), for: UIControlEvents.touchUpInside)
        
        if indexPath.row == 2
        {
            if self.statusCreditsPackagesStr == "generalCreditsPurchased"
            {
                creditsCell.selectTypeOfTokenBtn.setTitle("Bought", for: .normal)
            }
            else
            {
                creditsCell.selectTypeOfTokenBtn.setTitle("Select", for: .normal)
            }
        }
        
        if indexPath.row == 1
        {
            if self.statusCreditsPackagesStr == "vipCreditsPurchased"
            {
                creditsCell.selectTypeOfTokenBtn.setTitle("Bought", for: .normal)
            }
            else
            {
                creditsCell.selectTypeOfTokenBtn.setTitle("Select", for: .normal)
            }
        }
        
        if indexPath.row == 0
        {
            if self.statusCreditsPackagesStr == "vvipCreditsPurchased"
            {
                creditsCell.selectTypeOfTokenBtn.setTitle("Bought", for: .normal)
            }
            else
            {
                creditsCell.selectTypeOfTokenBtn.setTitle("Select", for: .normal)
            }
        }
 
        return creditsCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func getInformationForInApp(sender: UIButton!) {
        
        self.selectBtnOnIndex = sender.tag
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { _ in
            self.inAppInfoView.isHidden = false
        }, completion: nil)
        
        if (sender.tag == 0)
        {
            self.packageTitleLbl.text = "VVIP PACKAGE"
            
            if (self.getPackagesArray.count > 0)
            {
                let packageDict = self.getPackagesArray[sender.tag] as! NSDictionary
                if (packageDict["id"] != nil)
                {
                    self.idofSelectedPackage = packageDict.value(forKey: "id") as! String
                }
                else{
                    self.idofSelectedPackage = "1"
                }
            }
            else{
                self.idofSelectedPackage = "1"
            }
        }
        else if (sender.tag == 1)
        {
            self.packageTitleLbl.text = "VIP PACKAGE"
            
            if (self.getPackagesArray.count > 0)
            {
                let packageDict = self.getPackagesArray[sender.tag] as! NSDictionary
                if (packageDict["id"] != nil)
                {
                    self.idofSelectedPackage = packageDict.value(forKey: "id") as! String
                }
                else{
                    self.idofSelectedPackage = "2"
                }
            }
            else{
                self.idofSelectedPackage = "2"
            }
        }
        else
        {
            self.packageTitleLbl.text = "GENERAL PACKAGE"
            
            if (self.getPackagesArray.count > 0)
            {
                let packageDict = self.getPackagesArray[sender.tag] as! NSDictionary
                if (packageDict["id"] != nil)
                {
                    self.idofSelectedPackage = packageDict.value(forKey: "id") as! String
                }
                else
                {
                    self.idofSelectedPackage = "5"
                }
            }
            else{
                self.idofSelectedPackage = "5"
            }
        }
        
        let cost = self.totalCostArrayInfo[sender.tag]
        if let range = cost.range(of: "(")
        {
            let firstPart = cost.substring(to: range.lowerBound)
            let secondStr = cost.substring(to: range.upperBound)
            let range = (cost as NSString).range(of: firstPart)
            
            let attribute = NSMutableAttributedString.init(string: cost)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0) , range: range)
            
            self.planTypeLbl.attributedText = attribute
        }
        else
        {
            if (sender.tag == 0)
            {
                self.planTypeLbl.text = "@ $99.99/12 Months (Save $20.00)"
            }
            else if (sender.tag == 1)
            {
                self.planTypeLbl.text = "@ $49.99/6 Months (Save $10.00)"
            }
            else{
                self.planTypeLbl.text = "$9.99/Month"
            }
        }
    }
    
    // MARK: - ****** Buy Now In App Purchase Button Action. ******
    func buyNow(sender: UIButton!) {
        self.selectBtnOnIndex = sender.tag
        
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { _ in
            self.inAppInfoView.isHidden = false
        }, completion: nil)
        
        
        if (sender.tag == 0)
        {
            self.packageTitleLbl.text = "VVIP PACKAGE"
            
            if (self.getPackagesArray.count > 0)
            {
                let packageDict = self.getPackagesArray[sender.tag] as! NSDictionary
                if (packageDict["id"] != nil)
                {
                    self.idofSelectedPackage = packageDict.value(forKey: "id") as! String
                }
                else{
                    self.idofSelectedPackage = "1"
                }
            }
            else{
                self.idofSelectedPackage = "1"
            }
        }
        else if (sender.tag == 1)
        {
            self.packageTitleLbl.text = "VIP PACKAGE"
            
            if (self.getPackagesArray.count > 0)
            {
                let packageDict = self.getPackagesArray[sender.tag] as! NSDictionary
                if (packageDict["id"] != nil)
                {
                    self.idofSelectedPackage = packageDict.value(forKey: "id") as! String
                }
                else{
                    self.idofSelectedPackage = "2"
                }
            }
            else{
                self.idofSelectedPackage = "2"
            }
        }
        else{
            self.packageTitleLbl.text = "GENERAL PACKAGE"
            
            if (self.getPackagesArray.count > 0)
            {
                let packageDict = self.getPackagesArray[sender.tag] as! NSDictionary
                if (packageDict["id"] != nil)
                {
                    self.idofSelectedPackage = packageDict.value(forKey: "id") as! String
                }
                else{
                    self.idofSelectedPackage = "5"
                }
            }
            else{
                self.idofSelectedPackage = "5"
            }
        }
        
        let cost = self.totalCostArrayInfo[sender.tag]
        if let range = cost.range(of: "(")
        {
            let firstPart = cost.substring(to: range.lowerBound)
            let secondStr = cost.substring(to: range.upperBound)
            let range = (cost as NSString).range(of: firstPart)
            
            let attribute = NSMutableAttributedString.init(string: cost)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0) , range: range)
            
            self.planTypeLbl.attributedText = attribute
        }
        else
        {
            if (sender.tag == 0)
            {
                self.planTypeLbl.text = "@ $99.99/12 Months (Save $20.00)"
            }
            else if (sender.tag == 1)
            {
                self.planTypeLbl.text = "@ $49.99/6 Months (Save $10.00)"
            }
            else{
                self.planTypeLbl.text = "$9.99/Month"
            }
        }
    }
    
    
    
    // MARK: - ****** UITextField Delegate Method. ******
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }


    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
