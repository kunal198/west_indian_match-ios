//
//  MessagesViewController.swift
//  WestIndianMatch
//
//  Created by brst on 27/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Photos
import Firebase
import AVKit
import AVFoundation
import MediaPlayer
import SDWebImage
import FLAnimatedImage

class MessagesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate {

    //MARK:- ***** VARIABLES DECLARATION *****
    var totalMessagesInt = Int()
    var checkAnonymousPostBool = Bool()
    var ownerDetailsDict = NSMutableDictionary()
    var checkRevealBool = Bool()
    var myProfileVisibilityStatus = String()
    var opponentProfileVisibilityStatus = String()
    var checkVisibiltyStr = String()
    var showAlertOnce = Bool()
    var mySettingProfileInvisibilityStr = String()
    var opponentSettingProfileInvisibilityStr = String()
    var myProfileVisibilityStr = String()
    var MessagesCount = String()
    var badgeCount = Int()
    var chatUserDifferentiateStr = String()
    var currentUserId = String()
    var opponentFirebaseUserID = String()
    var opponentUserID = String()
    var opponentName = String()
    var opponentProfilePicType = String()
    var opponentImage = String()
    var items = [Message]()
    var currentUser: User?
    var usersDetailsDict = NSMutableDictionary()
    var opponentProfilePic = UIImage()
    var viewOpponentProfile = String()
    var readMsgBool = Bool()
    var reportedUsersStr = String()
    var typeOfUser = String()
    var confessionTypeUser = String()
    var typeOfUserCopy = String()
    var opponentMessagesVisibilityStr = String()
    var opponentUserDeviceId = String()
    var appDelegatePush = String()
    var myID = String()
    var myFirebaseID = String()
    var myProfileImage = String()
    var myName = String()
    var profileRevealedBool = Bool()
    
    var opponentAuthToken = String()
    
    var opponentImageStrFromAppDel = String()
    
    var getRevealBool = Bool()
    var opponentProfileStatus = String()
    var myProfileStatus = String()
    var postID = String()
    

    var getUserStatus = String()
    
    var myVisiblityFromChatStr = String()
    var opponentVisibilityFromChatStr = String()
    
    var myProfileVisibilityArray = [String]()
    var opponentProfileVisibilityArray = [String]()

    
    var mainConfessionCheck = String()
    var userPostedConfessionImage = FLAnimatedImageView()
    var userPostedConfessionImageURL = String()
    var postedConfessionImageType = String()
    var postedConfessionStatus = String()
    var postedConfessionData = String()
    
    var reportedUsersByMeArray = NSMutableArray()
    var  userWhoReportedMeArray = NSMutableArray()
    
    var statusOfContinueStr = Bool()
    
    var myProfilePicLink = String()
    var blockedByOpponentStatus = String()
    var blockedByMeStatus = String()
    var reportedByMeStatus = String()
    var reportedByMe_ContinueChat = String()
    
    //MARK:- ***** OUTLETS DECLARATION *****
    @IBOutlet weak var playAudioBtn: UIButton!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var finishAudioBtn: UIButton!
    @IBOutlet weak var startAudioBtn: UIButton!
    @IBOutlet weak var sendAudioBtn: UIButton!
    @IBOutlet weak var audioUpdate_lbl: UILabel!
    @IBOutlet weak var opponentName_lbl: UILabel!
    @IBOutlet weak var opponentOnlineIcon: UIImageView!
    @IBOutlet weak var opponentProfileImage: UIImageView!
    @IBOutlet weak var particularImage: FLAnimatedImageView!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var selectedImageView: UIView!
    @IBOutlet weak var usersVew: UIView!
    @IBOutlet weak var clickCameraImageView: UIImageView!
    @IBOutlet weak var writeText_txtView: UITextView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var messages_tableView: UITableView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var sendMessageView: UIView!
    @IBOutlet weak var viewImageVIew: UIView!
    @IBOutlet weak var audioRecordingView: UIView!
    
    @IBOutlet weak var blockUserTitleLbl: UILabel!
    @IBOutlet weak var blockUserView: UIView!
    
    @IBOutlet weak var dropDownOptionsBtn: UIButton!
    
    
    
    // MARK: - ****** ViewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkAnonymousPostBool = false
        self.dropDownOptionsBtn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            self.myFirebaseID = currentUserID
        }
        
        print("self.myFirebaseID = \(self.myFirebaseID)")
        print("self.postID = \(self.postID)")
        print("self.userPostedConfessionImage = \(self.userPostedConfessionImage)")
        print("self.postedConfessionData = \(self.postedConfessionData)")
        print("self.postedConfessionStatus = \(self.postedConfessionStatus)")
        print("self.postedConfessionImageType = \(self.postedConfessionImageType)")
        print("self.userPostedConfessionImageURL = \(self.userPostedConfessionImageURL)")
        
        self.dropDownOptionsBtn.isHidden = true
        self.blockUserView.isHidden = true
        self.checkRevealBool = false
        self.showAlertOnce = false
        self.mySettingProfileInvisibilityStr = ""
        self.messages_tableView.rowHeight = UITableViewAutomaticDimension
        self.messages_tableView.estimatedRowHeight = 90.0
        self.getRevealBool = false
        self.profileRevealedBool = false
   
        UIApplication.shared.isStatusBarHidden=false
        readMsgBool = false
        
        self.chatView.frame.size.width = self.view.frame.size.width
        self.sendMessageView.frame.size.width = self.chatView.frame.size.width
        self.optionView.frame.size.width = self.chatView.frame.size.width
        self.sendMessageView.frame.size.height = self.chatView.frame.size.height
        self.optionView.frame.size.height = self.chatView.frame.size.height
        self.opponentProfileImage.layer.borderWidth = 2.0
        self.opponentProfileImage.layer.masksToBounds = true
        self.opponentProfileImage.layer.borderColor = UIColor.clear.cgColor
        self.opponentProfileImage.layer.cornerRadius = 13
        self.opponentProfileImage.layer.cornerRadius = self.opponentProfileImage.frame.size.height/2
        self.usersVew.isHidden = true
        
        if let name = UserDefaults.standard.value(forKey: "userName") as? String
        {
            self.myName = name
        }
        else{
            self.myName = ""
        }
        
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            self.myID = id
        }
        else
        {
            self.myID = ""
        }
        
        self.messages_tableView.separatorColor = UIColor.clear
        self.writeText_txtView.delegate=self
        self.writeText_txtView.layer.borderColor = UIColor.lightGray.cgColor
        self.writeText_txtView.layer.borderWidth = 1
        self.writeText_txtView.layer.cornerRadius = 5
        
        self.chatView.clipsToBounds = true
        print("self.opponentUserID = \(self.opponentUserID)")
        print("self.opponentFirebaseUserID = \(self.opponentFirebaseUserID)")

        self.selectedImageView.isHidden = true
        self.optionView.isHidden = false
        self.animateExtraButtons(toHide: true)
        self.getUserDetails()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        print("appDelegate.messagesCount = \(appDelegate.messagesCount)")
        appDelegate.messagesCount = 0
        UserDefaults.standard.set(0, forKey: "messagesCount")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMessageBadge"), object: nil, userInfo: nil)
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(MessagesViewController.deleteChatNotification), name: NSNotification.Name(rawValue: "deleteChatNotification"), object: nil)
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
          
            
            self.getStatusOfOpponent(completion: { (responseBool) in
                
                if (self.typeOfUser == "AnonymousUser")
                {
                    if (self.confessionTypeUser == "AnonymousUser")
                    {
                        self.dropDownOptionsBtn.isHidden = false
                        self.typeOfUserCopy = "AnonymousUser"
                        self.blockUserView.isHidden = true
                        self.userPostedConfessionImage.frame.size.width = self.selectedImage.frame.size.width
                        self.userPostedConfessionImage.frame.size.height = self.selectedImage.frame.size.height
                        
                        self.items = []
                        
                        self.opponentProfileStatus = "profileNotRevealed"
                        self.opponentProfileImage.image = nil
                        self.opponentProfileImage.backgroundColor = UIColor.clear
                    }
                    else
                    {
                        self.dropDownOptionsBtn.isHidden = false
                        self.blockUserView.isHidden = true
                        self.myProfileStatus = "profileRevealed"
                        self.opponentProfileStatus = "profileRevealed"
                        self.opponentProfileImage.image = nil
                        
                        self.opponentName_lbl.text = self.opponentName
                        self.opponentName_lbl.textColor = UIColor.white
                        self.opponentProfileImage.image = self.opponentProfilePic
                    }
                    
                    
                    self.fetchData_AnonymousUser()
                    self.fetchMyProfileVisibilityStatus()
                    self.getMyProfileStatus()
                    self.getOpponentProfileStatus()
                    self.updateChatDeleted_AnonymousUser()
                }
                else if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
                {
                    self.dropDownOptionsBtn.isHidden = false
                    self.blockUserView.isHidden = true
                    self.myProfileStatus = "profileRevealed"
                    self.opponentProfileStatus = "profileRevealed"
                    self.opponentProfileImage.image = nil
                    
                    self.opponentName_lbl.text = self.opponentName
                    self.opponentName_lbl.textColor = UIColor.white
                    
                    if self.appDelegatePush == "fromAppDelegate"
                    {
                       
                    }
                    else
                    {
                        self.opponentProfileImage.image = self.opponentProfilePic
                    }
                    
                    self.updateChatDeleted()
                }
                else
                {
                    self.dropDownOptionsBtn.isHidden = false
                    self.blockUserView.isHidden = true
                    self.myProfileStatus = "profileRevealed"
                    self.opponentProfileStatus = "profileRevealed"
                    self.updateChatDeleted()
                }
                
                self.getOpponentPicture(completion: { (opponentImageStr) -> Void in
                    self.opponentSettingsForProfileInvisibility(oppoImageStr: opponentImageStr)
                    self.mySettingsForProfileInvisibility()
                })
                
                self.getBadgeCount(ownerID: self.opponentFirebaseUserID)
                self.getOnlineStatus()
                self.fetchOwnerMessagesVisibilitySettings()
                self.fetchMyProfilePicture()
                self.fetchMyMessagesCount()
            })
        
        }
  }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        self.view.endEditing(true)
        self.writeText_txtView.resignFirstResponder()
    }
    
    func getStatusOfOpponent(completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
           
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getStatusOfBlockedUsersForMessages(userID: userID, authToken: userToken, other_user_id: self.opponentUserID, completion: { (responseData) in
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            
                            self.blockedByMeStatus = dataDict?.value(forKey: "blocked_byme") as! String
                            self.blockedByOpponentStatus = dataDict?.value(forKey: "blocked_byhim") as! String
                            self.reportedByMeStatus = dataDict?.value(forKey: "reported_byme") as! String
                            self.reportedByMe_ContinueChat = dataDict?.value(forKey: "reported_bymecontinuechat") as! String
                            
                            self.opponentAuthToken = dataDict?.value(forKey: "auth_token") as! String
                            
                            self.opponentImageStrFromAppDel = dataDict?.value(forKey: "profile_pic_thumb") as! String
                            
                            let result = dataDict?.value(forKey: "online_status") as! String
                            if result == "1"
                            {
                                if (self.blockedByOpponentStatus == "no")
                                {
                                    self.opponentOnlineIcon.isHidden = false
                                }
                                else
                                {
                                    self.opponentOnlineIcon.isHidden = true
                                }
                            }
                            else
                            {
                                self.opponentOnlineIcon.isHidden = true
                            }
                            
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                             completion(false)
                            //                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    
    // MARK: - ****** Fetch My Profile Visibility Status from Firebase ******
    func fetchMyProfileVisibilityStatus()
    {
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            let ref = FIRDatabase.database().reference()
            ref.child("Users").child(id).child("statusCreditsPackages")
            ref.child("Users").child(id).child("statusCreditsPackages").observeSingleEvent(of:FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull
                {
                    self.myProfileVisibilityStatus = "profileVisibilityNotPurchased"
                    self.fetchOpponentProfileVisibilityStatus()
                }
                else
                {
                    self.myProfileVisibilityStatus = snapshot.value as! String
                    
                    if (self.myProfileVisibilityStatus == "generalCreditsPurchased") || (self.myProfileVisibilityStatus == "vipCreditsPurchased") || (self.myProfileVisibilityStatus == "vvipCreditsPurchased")
                    {
                        self.myProfileVisibilityStatus = "profileVisibilityPurchased"
                    }
                    else{
                        self.myProfileVisibilityStatus = "profileVisibilityNotPurchased"
                    }
                    
                    self.fetchOpponentProfileVisibilityStatus()
                }
            })
        }
    }
    
    
    
    // MARK: - ****** Fetch Opponent Profile Visibility Status from Firebase ******
    func fetchOpponentProfileVisibilityStatus()
    {
        if (self.opponentFirebaseUserID != "")
        {
            let ref = FIRDatabase.database().reference()
            ref.child("Users").child(self.opponentFirebaseUserID).child("statusCreditsPackages")
        ref.child("Users").child(self.opponentFirebaseUserID).child("statusCreditsPackages").observeSingleEvent(of:FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull{
                    self.opponentProfileVisibilityStatus = "profileVisibilityNotPurchased"
                    self.getOpponentPicture(completion: { (opponentImageStr) -> Void in
                        self.opponentSettingsForProfileInvisibility(oppoImageStr: opponentImageStr)
                    })
                }
                else
                {
                    self.opponentProfileVisibilityStatus = snapshot.value as! String
                    
                    if (self.opponentProfileVisibilityStatus == "generalCreditsPurchased") || (self.opponentProfileVisibilityStatus == "vipCreditsPurchased") || (self.opponentProfileVisibilityStatus == "vvipCreditsPurchased")
                    {
                        self.opponentProfileVisibilityStatus = "profileVisibilityPurchased"
                    }
                    else{
                        self.opponentProfileVisibilityStatus = "profileVisibilityNotPurchased"
                    }
                    
                    self.getOpponentPicture(completion: { (opponentImageStr) -> Void in
                        self.opponentSettingsForProfileInvisibility(oppoImageStr: opponentImageStr)
                    })
                }
            })
        }
    }
    
    
    // MARK: - ****** opponentSettingsForProfileInvisibility. ******
    func opponentSettingsForProfileInvisibility(oppoImageStr: String)
    {
        if (self.opponentFirebaseUserID != "")
        {
          FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).child("Settings").child("General").child("Profile_Visibility").observe(FIRDataEventType.value, with: { (snapshot) in
                
                if snapshot.value is NSNull
                {
                    if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
                    {
                        self.opponentSettingProfileInvisibilityStr = "On"
                    }
                    else{
                        self.opponentSettingProfileInvisibilityStr = "Off"
                    }
                }
                else{
                    self.opponentSettingProfileInvisibilityStr = snapshot.value as! String
                }
                
                print("self.opponentSettingProfileInvisibilityStr = \(self.opponentSettingProfileInvisibilityStr)")
            
            
            if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
            {
                if (self.mainConfessionCheck == "comingFromChatListing")
                {
                    if (self.opponentVisibilityFromChatStr.range(of: "@") != nil)
                    {
                        let fullUserID = self.opponentVisibilityFromChatStr.components(separatedBy: "@")
                        let AfterAtTheRateStr: String = fullUserID[1]
                        self.opponentSettingProfileInvisibilityStr = AfterAtTheRateStr
                    }
                    else
                    {
                        print("messageStr doesnot contains the word `@`")
                        self.opponentSettingProfileInvisibilityStr = "On"
                    }
                }
            }

                if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
                {
                    if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                    {
                        DispatchQueue.main.async {
                            self.opponentProfileImage.image = nil
                            self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                            self.opponentName_lbl.text = self.opponentName
                            self.opponentName_lbl.textColor = UIColor.white
                            self.opponentProfilePic = self.opponentProfileImage.image!
                            
                            self.messages_tableView.reloadData()
                        }
                    }
                    else
                    {
                        if (self.confessionTypeUser == "WithProfileConfessionUser")
                        {
                            DispatchQueue.main.async {
                                
                                for subview in self.opponentProfileImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                if (self.opponentImageStrFromAppDel == "")
                                {
                                    self.opponentProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                }
                                else{
                                    self.opponentProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                
                                self.opponentProfilePic = self.opponentProfileImage.image!
                                self.opponentName_lbl.text = self.opponentName
                                self.opponentName_lbl.textColor = UIColor.white
                                self.opponentProfileImage.backgroundColor = UIColor.clear
                                self.typeOfUserCopy = "WithProfileUser"
                                self.messages_tableView.reloadData()
                            }
                        }
                        else
                        {
                            if (self.confessionTypeUser == "WithProfileConfessionUser")
                            {
                                DispatchQueue.main.async {
                                    
                                    for subview in self.opponentProfileImage.subviews
                                    {
                                        if subview.isKind(of: UIVisualEffectView.self)
                                        {
                                            subview.removeFromSuperview()
                                        }
                                    }
                                    
                                    if (self.opponentImageStrFromAppDel == "")
                                    {
                                        self.opponentProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                    }
                                    else{
                                        self.opponentProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                    
                                    self.opponentProfilePic = self.opponentProfileImage.image!
                                    self.opponentName_lbl.text = self.opponentName
                                    self.opponentName_lbl.textColor = UIColor.white
                                    self.opponentProfileImage.backgroundColor = UIColor.clear
                                    self.typeOfUserCopy = "WithProfileUser"
                                    self.messages_tableView.reloadData()
                                }
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                    self.opponentName_lbl.text = "VIP Member"
                                    self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                    self.opponentProfileImage.backgroundColor = UIColor.clear
                                    self.opponentProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                    self.opponentProfilePic = self.opponentProfileImage.image!
                                    self.messages_tableView.reloadData()
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                    {
                        if (self.typeOfUserCopy != "AnonymousUser")
                        {
                            let status = Reach().connectionStatus()
                            switch status
                            {
                            case .unknown, .offline:
                                DispatchQueue.main.async {

                                    if (self.confessionTypeUser == "AnonymousUser")
                                    {
                                        self.opponentName_lbl.text = "Anonymous User"
                                        
                                        self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                        self.opponentProfileImage.backgroundColor = UIColor.clear
                                        self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                        
                                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                        blurEffectView.frame = self.opponentProfileImage.bounds
                                        self.opponentProfileImage.addSubview(blurEffectView)
                                        
                                        self.opponentProfilePic = self.opponentProfileImage.image!
                                    }
                                    else
                                    {
                                        self.opponentProfileImage.image = nil
                                        self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                        self.opponentName_lbl.text = self.opponentName
                                        self.opponentName_lbl.textColor = UIColor.white
                                        self.opponentProfilePic = self.opponentProfileImage.image!
                                        
                                        self.messages_tableView.reloadData()
                                    }
                                    
                                }
                                
                            case .online(.wwan) , .online(.wiFi):
                                
                                let databaseRef = FIRDatabase.database().reference()
                                databaseRef.child("Users").child(self.opponentFirebaseUserID).child("ProfilePicture").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                                    
                                    if snapshot.value is NSNull
                                    {
//                                        if (self.opponentProfileVisibilityStatus == "profileVisibilityNotPurchased")
//                                        {
                                            self.opponentName_lbl.text = "Anonymous User"
//                                        }
//                                        else{
//                                            self.opponentName_lbl.text = "VIP Member"
//                                        }
                                        self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                        self.opponentProfileImage.backgroundColor = UIColor.clear
                                        self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                        
                                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                        blurEffectView.frame = self.opponentProfileImage.bounds
                                        self.opponentProfileImage.addSubview(blurEffectView)
                                        
                                        self.opponentProfilePic = self.opponentProfileImage.image!
                                        
                                        self.messages_tableView.reloadData()
                                    }
                                    else
                                    {
                                        if (self.opponentProfileStatus == "profileRevealed")
                                        {
                                            DispatchQueue.main.async {
                                                
                                                self.opponentProfileImage.image = nil
                                                
                                                for subview in self.opponentProfileImage.subviews
                                                {
                                                    if subview.isKind(of: UIVisualEffectView.self)
                                                    {
                                                        subview.removeFromSuperview()
                                                    }
                                                }
                                                
                                                self.opponentProfileImage.sd_setImage(with: URL(string: snapshot.value as! String), placeholderImage: UIImage(named: "Default Icon"))
                                                self.opponentName_lbl.text = self.opponentName
                                                self.opponentName_lbl.textColor = UIColor.white
                                                self.opponentProfilePic = self.opponentProfileImage.image!
                                                
                                                self.messages_tableView.reloadData()
                                            }
                                        }
                                        else
                                        {
                                            DispatchQueue.main.async {
                                                if (self.confessionTypeUser == "AnonymousUser")
                                                {
                                                    self.opponentName_lbl.text = "Anonymous User"
                                                    
                                                    self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                                    self.opponentProfileImage.backgroundColor = UIColor.clear
                                                    self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                                    
                                                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                                    blurEffectView.frame = self.opponentProfileImage.bounds
                                                    self.opponentProfileImage.addSubview(blurEffectView)
                                                    
                                                    self.opponentProfilePic = self.opponentProfileImage.image!
                                                }
                                                else
                                                {
                                                    self.opponentProfileImage.image = nil
                                                    self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                                    self.opponentName_lbl.text = self.opponentName
                                                    self.opponentName_lbl.textColor = UIColor.white
                                                    self.opponentProfilePic = self.opponentProfileImage.image!
                                                    
                                                }
                                            }
                                        }
                                  
                                    }
                                })
                            }
                        }
                        else
                        {
                            if (self.opponentProfileStatus == "profileRevealed")
                            {
                                if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                                {
                                    for subview in self.opponentProfileImage.subviews
                                    {
                                        if subview.isKind(of: UIVisualEffectView.self)
                                        {
                                            subview.removeFromSuperview()
                                        }
                                    }
                                    
                                    self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                    
                                    self.opponentProfilePic = self.opponentProfileImage.image!
                                    
                                    self.opponentName_lbl.text = self.opponentName
                                    self.opponentName_lbl.textColor = UIColor.white
                                    self.opponentProfileImage.backgroundColor = UIColor.clear
                                    
                                    self.typeOfUserCopy = "WithProfileUser"
                                    
                                    self.messages_tableView.reloadData()
                                }
                                else
                                {
                                    DispatchQueue.main.async {
                                        
                                        if (self.confessionTypeUser == "AnonymousUser")
                                        {
                                            self.opponentName_lbl.text = "Anonymous User"
                                            
                                            self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                            self.opponentProfileImage.backgroundColor = UIColor.clear
                                            self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                            
                                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                            blurEffectView.frame = self.opponentProfileImage.bounds
                                            self.opponentProfileImage.addSubview(blurEffectView)
                                            
                                            self.opponentProfilePic = self.opponentProfileImage.image!
                                            
                                            self.messages_tableView.reloadData()
                                        }
                                        else
                                        {
                                            self.opponentProfileImage.image = nil
                                            self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                            self.opponentName_lbl.text = self.opponentName
                                            self.opponentName_lbl.textColor = UIColor.white
                                            self.opponentProfilePic = self.opponentProfileImage.image!
                                            
                                            self.messages_tableView.reloadData()
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (self.opponentProfileStatus == "profileRevealed")
                                {
                                    DispatchQueue.main.async {
                                        self.opponentProfileImage.image = nil
                                        
                                        for subview in self.opponentProfileImage.subviews
                                        {
                                            if subview.isKind(of: UIVisualEffectView.self)
                                            {
                                                subview.removeFromSuperview()
                                            }
                                        }
                                        
                                        self.opponentProfileImage.sd_setImage(with: URL(string: snapshot.value as! String), placeholderImage: UIImage(named: "Default Icon"))
                                        self.opponentName_lbl.text = self.opponentName
                                        self.opponentName_lbl.textColor = UIColor.white
                                        self.opponentProfilePic = self.opponentProfileImage.image!
                                        
                                        self.messages_tableView.reloadData()
                                    }
                                }
                                else
                                {
                                    DispatchQueue.main.async {
                                        if (self.confessionTypeUser == "AnonymousUser")
                                        {
                                            self.opponentName_lbl.text = "Anonymous User"
                                            
                                            self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                            self.opponentProfileImage.backgroundColor = UIColor.clear
                                            self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                            
                                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                            blurEffectView.frame = self.opponentProfileImage.bounds
                                            self.opponentProfileImage.addSubview(blurEffectView)
                                            
                                            self.opponentProfilePic = self.opponentProfileImage.image!
                                            
                                            self.messages_tableView.reloadData()
                                        }
                                        else
                                        {
                                            self.opponentProfileImage.image = nil
                                            self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                            self.opponentName_lbl.text = self.opponentName
                                            self.opponentName_lbl.textColor = UIColor.white
                                            self.opponentProfilePic = self.opponentProfileImage.image!
                                            
                                            self.messages_tableView.reloadData()
                                        }
                                    }
                                }
                            }
                        }
                        
                        self.mySettingsForProfileInvisibility()
                    }
                    else
                    {
                        self.writeText_txtView.resignFirstResponder()
                        
                        if (self.opponentProfileStatus == "profileRevealed")
                        {
                            DispatchQueue.main.async {
                                self.opponentProfileImage.image = nil
                                
                                for subview in self.opponentProfileImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                if (self.opponentImageStrFromAppDel == "")
                                {
                                    self.opponentProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                }
                                else{
                                    self.opponentProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                self.opponentName_lbl.text = self.opponentName
                                self.opponentName_lbl.textColor = UIColor.white
                                self.opponentProfilePic = self.opponentProfileImage.image!
                                
                                self.messages_tableView.reloadData()
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                if (self.confessionTypeUser == "AnonymousUser")
                                {
                                    self.opponentName_lbl.text = "Anonymous User"
                                    
                                    self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                    self.opponentProfileImage.backgroundColor = UIColor.clear
                                    self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                    
                                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                    blurEffectView.frame = self.opponentProfileImage.bounds
                                    self.opponentProfileImage.addSubview(blurEffectView)
                                    
                                    self.opponentProfilePic = self.opponentProfileImage.image!
                                    
                                    self.messages_tableView.reloadData()
                                }
                                else
                                {
                                    self.opponentProfileImage.image = nil
                                    self.opponentProfileImage.sd_setImage(with: URL(string: oppoImageStr), placeholderImage: UIImage(named: "Default Icon"))
                                    self.opponentName_lbl.text = self.opponentName
                                    self.opponentName_lbl.textColor = UIColor.white
                                    self.opponentProfilePic = self.opponentProfileImage.image!
                                    
                                    self.messages_tableView.reloadData()
                                }
                            }
                        }
                  
                        self.mySettingsForProfileInvisibility()
                    }
                }
            })
        }
    }
    
    
    //MARK:- ****** Fetch My Profile Invisibility Settings ******
    func mySettingsForProfileInvisibility()
    {
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            FIRDatabase.database().reference().child("Users").child(id).child("Settings").child("General").child("Profile_Visibility").keepSynced(true)
            FIRDatabase.database().reference().child("Users").child(id).child("Settings").child("General").child("Profile_Visibility").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                
                 if snapshot.value is NSNull{
                    self.mySettingProfileInvisibilityStr = "On"
                }
                 else
                 {
                    self.mySettingProfileInvisibilityStr = snapshot.value as! String
                }
                
                print("self.mySettingProfileInvisibilityStr = \(self.mySettingProfileInvisibilityStr)")
                
                if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
                {
                    if (self.mainConfessionCheck == "comingFromChatListing")
                    {
                        if (self.myVisiblityFromChatStr.range(of: "@") != nil)
                        {
                            let fullUserID = self.myVisiblityFromChatStr.components(separatedBy: "@")
                            let AfterAtTheRateStr: String = fullUserID[1]
                            self.mySettingProfileInvisibilityStr = AfterAtTheRateStr
                        }
                        else
                        {
                            self.mySettingProfileInvisibilityStr = "On"
                        }
                    }
                }
                
                if self.typeOfUser == "AnonymousUser"
                {
                    if (self.mainConfessionCheck == "comingFromconfession")
                    {
                        self.checkForAnonymousChat(postID: self.postID, oppoID: self.opponentFirebaseUserID)
                    }
                }
                else if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
                {
                    if (self.mainConfessionCheck == "comingFromconfession")
                    {
                        self.checkForProfileChat(forUserID: self.opponentFirebaseUserID, opponentProfileVisibilityStr: self.opponentSettingProfileInvisibilityStr, myProfileVisibilityStr: self.mySettingProfileInvisibilityStr, completion: { (responseData) in
                            

                        })
                    }
                    
                    self.fetchData()
                    self.updateChatDeleted()
                }
                else
                {
                    if (self.mainConfessionCheck == "comingFromconfession")
                    {
                        self.checkForProfileChat(forUserID: self.opponentFirebaseUserID, opponentProfileVisibilityStr: self.opponentSettingProfileInvisibilityStr, myProfileVisibilityStr: self.mySettingProfileInvisibilityStr, completion: { (responseData) in
                            
                            
                            
                        })
                    }
                    
                    self.fetchData()
                    self.updateChatDeleted()
                }
            })
        }
    }
    
    //MARK:- ****** Drop Down Options button ******
    @IBAction func dropDownOptions_btnAction(_ sender: Any) {
        
        let actionSheetController = UIAlertController(title: "", message: "Please select your option here", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }
        actionSheetController.addAction(cancelActionButton)
        
        let revealActionButton = UIAlertAction(title: "Reveal My Profile", style: .default) { action -> Void in
            
            
            if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                if self.blockedByOpponentStatus == "yes"
                {
                    let alert = UIAlertController(title: "", message: "You’ve Been Blocked For Being Outta Timin.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.blockedByMeStatus == "yes"
                {
                    let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if self.reportedByMeStatus == "yes"
                {
                    let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    
                    
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            print("Not connected")
                            
                        case .online(.wwan) , .online(.wiFi):
                            
                            self.updateContinueChatWithReportedUser()
                            
                        }
                        
                      
                        self.sendRevealMyProfileStatus(userIDStr: id)
                        
                    }))
                        
                    if (self.reportedByMe_ContinueChat == "0")
                    {
                        self.present(alert, animated: true, completion: nil)
                    }
                    else{
                        self.sendRevealMyProfileStatus(userIDStr: id)
                    }
                }
                else if self.reportedUsersStr == "userAlreadyReportedMe"
                {

                }
                else
                {
                    self.sendRevealMyProfileStatus(userIDStr: id)
                }
            }
        }
        
        
        var blockActionButton = UIAlertAction()
        if (self.blockedByMeStatus == "yes")
        {
            blockActionButton = UIAlertAction(title: "User Blocked", style: .destructive) { action -> Void in
                
            }
        }
        else{
            blockActionButton = UIAlertAction(title: "Block User", style: .destructive) { action -> Void in
               
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                   
                    if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                    {
                        if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                        {
                            if self.opponentUserID != ""
                            {
                                self.loadingView.isHidden = false
                                self.view.isUserInteractionEnabled = false
                                
                                var userTypeStr = String()
                                
                               if (self.opponentName_lbl.text == "Anonymous User")
                               {
                                  userTypeStr = "confessions"
                               }
                                else{
                                    userTypeStr = "profile"
                                }
                                
                                ApiHandler.blockUnblockUser(userID: userID, authToken: userToken, other_user_id: self.opponentUserID,typeOfBlockUser: userTypeStr, completion: { (responseData) in
                                    
                                    if (responseData.value(forKey: "status") as? String == "200")
                                    {
//                                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                        
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        
                                        let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                            action in
                                            
                                              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userBlockedFromMessages"), object: nil)
                                            
                                            self.getStatusOfOpponent(completion: { (result) in
                                                
                                            })
                                            
                                        }))
                                        
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else if (responseData.value(forKey: "status") as? String == "400")
                                    {
                                        let message = responseData.value(forKey: "message") as? String
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                })
                            }
                        }
                    }
                   
                }
                
            }
        }
        
        
        var reportActionButton = UIAlertAction()
        if  (self.reportedUsersByMeArray.contains(self.opponentUserID))
        {
            reportActionButton = UIAlertAction(title: "User Reported", style: .destructive) { action -> Void in
                
                
            }
        }
        else
        {
            reportActionButton = UIAlertAction(title: "Report User", style: .destructive) { action -> Void in
                
                let alert = UIAlertController(title: "", message: "Do you really want to report the user?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                       
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                      
                        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                        {
                            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                            {
                                if self.opponentUserID != ""
                                {
                                    self.loadingView.isHidden = false
                                    self.view.isUserInteractionEnabled = false
                                    
                                    ApiHandler.reportContentOrUser(userID: userID, authToken: userToken, typeStr: "1", content_id: self.opponentUserID, completion: { (responseData) in
                                        
                                        if (responseData.value(forKey: "status") as? String == "200")
                                        {
//                                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                            
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            
                                            let alert = UIAlertController(title: "", message: "User Reported.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                                action in
                                                
                                                self.getStatusOfOpponent(completion: { (result) in
                                                    
                                                })
                                                
                                            }))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else if (responseData.value(forKey: "status") as? String == "400")
                                        {
                                            let message = responseData.value(forKey: "message") as? String
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    })
                                }
                            }
                        }
                    }
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
        
        
        if (self.myProfileStatus != "profileRevealed")
        {
            actionSheetController.addAction(revealActionButton)
        }
        
//        if (self.typeOfUser == "AnonymousUser"){
            actionSheetController.addAction(blockActionButton)
            actionSheetController.addAction(reportActionButton)
//        }
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    // MARK: - ***** Block opponent user and update in firebase. *****
    func ReportUser(userID : String , userName : String , Age : String , location : String , ProfilePic : String,Gender: String)
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            FIRDatabase.database().reference().child("ReportedUsersByMe").child(currentUserID).child(userID).updateChildValues(["UserID": userID, "UserName": userName, "Age": Age, "ProfilePicture": ProfilePic, "Location":location, "blockedStatus" : "reported","Gender" : Gender])
            FIRDatabase.database().reference().child("Users").child(userID).child("usersWhoReportedMe").child(currentUserID).updateChildValues(["UserID": currentUserID])
            
            FIRDatabase.database().reference().child("ReportedUsersByMe").child(currentUserID).child(userID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull{
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    return
                }
                
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                
                let alert = UIAlertController(title: "", message: "User Reported.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                    action in
                    
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                }))
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    func sendRevealMyProfileStatus(userIDStr: String)
    {
        FIRDatabase.database().reference().child("Users").child(userIDStr).child("Settings").child("General").child("Profile_Visibility").keepSynced(true)
        
        FIRDatabase.database().reference().child("Users").child(userIDStr).child("Settings").child("General").child("Profile_Visibility").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            
            if snapshot.value is NSNull{
                self.myProfileVisibilityStr = "On"
            }
            else
            {
                self.myProfileVisibilityStr = snapshot.value as! String
            }
            
            self.RevealMyProfile()
        })
    }
    
    
    func getOpponentPicture(completion: @escaping (String) -> Swift.Void)
    {
        completion(self.opponentImageStrFromAppDel)
    }
    
    //MARK:- ****** Check For Anonymous Chat ******
    func checkForProfileChat(forUserID: String,opponentProfileVisibilityStr: String,myProfileVisibilityStr: String, completion: @escaping (Bool) -> Swift.Void)
    {
        if (forUserID != "")
        {
            if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                var opponentVisibilityStr = String()
                opponentVisibilityStr = forUserID + "@" + opponentProfileVisibilityStr
                
                let myAndOppoStatusStr = opponentVisibilityStr + "@" + myProfileVisibilityStr
                FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(myAndOppoStatusStr).observe(.value, with: { (snapshot) in
                    
                    if snapshot.exists() {
                        
                        completion(true)
                    }
                    else
                    {
                        if (self.checkAnonymousPostBool == false)
                        {
                            self.checkAnonymousPostBool = true
                            self.composeMessage(type: .postedConfession, content: self.userPostedConfessionImageURL, confessionStatusText: self.postedConfessionStatus, confessionImageType: self.postedConfessionImageType, confessionCheck: self.postedConfessionData)
                        }
                        
                        completion(true)
                    }
                })
            }
            else
            {
                 completion(true)
            }
        }
        else{
            completion(true)
        }
    }
    
    
    //MARK:- ****** Check For Anonymous Chat ******
    func checkForAnonymousChat(postID: String, oppoID: String)
    {
        if (postID != "") && (oppoID != "")
        {
            if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                let opponentWithPostId = oppoID + "@" + postID
                FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(opponentWithPostId).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if snapshot.exists()
                    {
//                        let data = snapshot.value as! [String: String]
                    }
                    else
                    {
                        if (self.checkAnonymousPostBool == false)
                        {
                            self.checkAnonymousPostBool = true
                            self.composeMessage(type: .postedConfession, content: self.userPostedConfessionImageURL, confessionStatusText: self.postedConfessionStatus, confessionImageType: self.postedConfessionImageType, confessionCheck: self.postedConfessionData)
                        }
                    }
                })
            }
        }
    }
    
    
    // MARK: - ****** Fetch fetchMyMessagesCount from Firebase ******
    func fetchMyMessagesCount()
    {
        if self.opponentFirebaseUserID != ""
        {
            FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).child("MessagesCount").observe(FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull
                {
                    self.MessagesCount = "0"
                }
                else
                {
                    self.MessagesCount = snapshot.value as! String
                }
            })
        }
    }

    
    // MARK: - ****** deleteChatNotification ******
    func deleteChatNotification(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        
        if (self.typeOfUser == "AnonymousUser")
        {
            let oppoIDWithMyStr =  self.opponentFirebaseUserID + "@" + self.postID
            
            if ((userInfo?["Status"] as? String) == oppoIDWithMyStr)
            {
                //        "Oops! Current Chat is no longer available."
                if (self.items.count > 0)
                {
                    let alert = UIAlertController(title: "", message: "Message deleted.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        self.navigationController!.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else
        {
            let oppoIDWithMyStr =  self.opponentFirebaseUserID + "@" + self.opponentSettingProfileInvisibilityStr + "@" + self.mySettingProfileInvisibilityStr
      
            if ((userInfo?["Status"] as? String) == oppoIDWithMyStr)
            {
                //        "Oops! Current Chat is no longer available."
                if (self.items.count > 0)
                {
                    let alert = UIAlertController(title: "", message: "Message deleted.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        self.navigationController!.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - ****** getOnlineStatus ******
    func getOnlineStatus()
    {
        if self.opponentFirebaseUserID != ""
        {
            FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).child("userStatus").observe(FIRDataEventType.value, with:  { (snapshot) in
                if snapshot.exists()
                {
                    self.getUserStatus = snapshot.value as! String
                }
                else
                {
                    self.getUserStatus = "online"
                }
            })
        }
    }

 
    // MARK: - ****** getBadgeCount ******
    func getBadgeCount(ownerID:String)
    {
        let ref = FIRDatabase.database().reference()
        if ownerID != ""
        {
        ref.child("Users").child(ownerID).child("AppBadgeCount").observe(FIRDataEventType.value, with:  { (snapshot) in
                if snapshot.exists()
                {
                    self.badgeCount = snapshot.value as! Int
                }
                else
                {
                    var dataDict = [String: Int]()
                    dataDict["AppBadgeCount"] = 0
                    self.badgeCount = 0
                    ref.child("Users").child(ownerID).updateChildValues(dataDict)
                }
            })
        }
        else
        {
            self.badgeCount = 0
        }
    }
    
    
    // MARK: - ****** getMyProfileStatus ******
    func getMyProfileStatus()
    {
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {

            let opponentWithPostId = self.opponentFirebaseUserID + "@" + self.postID
            
           FIRDatabase.database().reference().child("Users").child(id).child("conversations_anonymous").child(opponentWithPostId).observe(.value, with: { (snapshot) in
                    if snapshot.exists()
                    {
                        FIRDatabase.database().reference().child("Users").child(id).child("conversations_anonymous").child(opponentWithPostId).observe(.childAdded, with: { (snap) in
                            
                            if snap.exists()
                            {
                                self.myProfileStatus = snap.value as! String
                                if self.getRevealBool == false
                                {
                                    if snap.key == "checkRevealProfile"
                                    {
                                        if self.myProfileStatus == "profileRevealed"
                                        {
                                            self.myProfileStatus = "profileRevealed"
                                            self.getRevealBool = true
                                            self.usersVew.isHidden = true
                                        }
                                        else
                                        {
                                            self.myProfileStatus = "profileNotRevealed"
                                            self.usersVew.isHidden = true
//                                            self.usersVew.isHidden = false
                                        }
                                        //self.messages_tableView.reloadData()
                                    }
                                    else
                                    {
                                        self.myProfileStatus = "profileNotRevealed"
                                        self.usersVew.isHidden = true
//                                        self.usersVew.isHidden = false
                                    }
                                    //self.messages_tableView.reloadData()
                                }
                                else
                                {
                                    self.myProfileStatus = "profileRevealed"
                                    self.getRevealBool = true
                                    self.usersVew.isHidden = true
                                    //self.messages_tableView.reloadData()
                                }
                            }
                            else
                            {
                                self.myProfileStatus = "profileNotRevealed"
                                self.usersVew.isHidden = true
//                                self.usersVew.isHidden = false
                               // self.messages_tableView.reloadData()
                            }
                        })
                    }
                    else
                    {
                        self.myProfileStatus = "profileNotRevealed"
                        self.usersVew.isHidden = true
//                        self.usersVew.isHidden = false
                       // self.messages_tableView.reloadData()
                   }
            })
        }
    }
    
    
    
    // MARK: - ****** getOpponentProfileStatus ******
    func getOpponentProfileStatus()
    {
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            let currentWithPostId = id + "@" + self.postID
            FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).child("conversations_anonymous").child(currentWithPostId).observe(.value, with: { (snapshot) in
            
                if snapshot.exists()
                {
                    FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).child("conversations_anonymous").child(currentWithPostId).observe(.childAdded, with: { (snap) in
                        if snap.exists()
                        {
                            if snap.key == "checkRevealProfile"
                            {
                                self.checkRevealBool = true
                                self.opponentProfileStatus = snap.value as! String
                            }
                        }
                        else
                        {
                            self.checkRevealBool = false
                            self.opponentProfileStatus = "profileNotRevealed"
                            self.opponentProfileImage.image = nil
                            self.opponentProfileImage.backgroundColor = UIColor.clear
                            self.opponentProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = self.opponentProfileImage.bounds
                            self.opponentProfileImage.addSubview(blurEffectView)
                            self.opponentProfilePic = self.opponentProfileImage.image!
                        }
                        
                        if (self.checkRevealBool == true)
                        {
                            self.checkRevealBool = false
                            self.opponentProfileStatus = "profileRevealed"
                            
                            if (self.opponentProfileStatus == "profileRevealed")
                            {
                                if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                                {
                                    DispatchQueue.main.async {
                                        
                                        for subview in self.opponentProfileImage.subviews
                                        {
                                            if subview.isKind(of: UIVisualEffectView.self)
                                            {
                                                subview.removeFromSuperview()
                                            }
                                        }
                                        
                                        
                                        
                                        if (self.opponentImageStrFromAppDel == "")
                                        {
                                            self.opponentProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                        }
                                        else{
                                            self.opponentProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                        }
                                        
                                        self.opponentProfilePic = self.opponentProfileImage.image!
                                        
                                        self.opponentName_lbl.text = self.opponentName
                                        self.opponentName_lbl.textColor = UIColor.white
                                        
                                    self.opponentProfileImage.backgroundColor = UIColor.clear
                                        
                                        self.typeOfUserCopy = "WithProfileUser"
                                        
//                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateListingOnRevealProfile"), object: nil, userInfo: nil)
                                        
                                        self.messages_tableView.reloadData()
                                    }
                                }
                                else
                                {
                                    
                                    if (self.confessionTypeUser == "WithProfileConfessionUser")
                                    {
                                        DispatchQueue.main.async {
                                            
                                            for subview in self.opponentProfileImage.subviews
                                            {
                                                if subview.isKind(of: UIVisualEffectView.self)
                                                {
                                                    subview.removeFromSuperview()
                                                }
                                            }
                                            
                                            if (self.opponentImageStrFromAppDel == "")
                                            {
                                                self.opponentProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                            }
                                            else{
                                                self.opponentProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                            }
                                            
                                            self.opponentProfilePic = self.opponentProfileImage.image!
                                            
                                            self.opponentName_lbl.text = self.opponentName
                                            self.opponentName_lbl.textColor = UIColor.white
                                            
                                            self.opponentProfileImage.backgroundColor = UIColor.clear
                                            
                                            self.typeOfUserCopy = "WithProfileUser"
                                            
                                            self.messages_tableView.reloadData()
                                        }
                                    }
                                    else
                                    {
                                        DispatchQueue.main.async {
                                            
                                            self.opponentName_lbl.text = "VIP Member"
                                            self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                            self.opponentProfileImage.backgroundColor = UIColor.clear
                                            self.opponentProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                            self.opponentProfilePic = self.opponentProfileImage.image!
                                            
                                            //                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateListingOnRevealProfile"), object: nil, userInfo: nil)
                                            self.messages_tableView.reloadData()
                                        }
                                    }
                                }
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                    self.opponentProfileImage.image = nil
                           
                                    self.opponentName_lbl.text = "Anonymous User"
                                    
                                    self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                                    self.opponentProfileImage.backgroundColor = UIColor.clear
                                    self.opponentProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                    
                                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                    blurEffectView.frame = self.opponentProfileImage.bounds
                                    self.opponentProfileImage.addSubview(blurEffectView)
                                    
                                    self.opponentProfilePic = self.opponentProfileImage.image!
                                }
                            }
                        }
                    })
                }
                else
                {
                    if (self.opponentProfileStatus == "profileRevealed")
                    {
                        DispatchQueue.main.async {
                            self.opponentProfileImage.image = nil
                            
                            for subview in self.opponentProfileImage.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            if (self.opponentImageStrFromAppDel == "")
                            {
                              self.opponentProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                            }
                            else
                            {
                              self.opponentProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            self.opponentName_lbl.text = self.opponentName
                            self.opponentName_lbl.textColor = UIColor.white
                            if self.opponentProfileImage.image != nil
                            {
                                self.opponentProfilePic = self.opponentProfileImage.image!
                            }
                            
                            
                            self.messages_tableView.reloadData()
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.opponentProfileImage.image = nil
                            self.opponentName_lbl.text = "Anonymous User"
                            self.opponentName_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                            self.opponentProfileImage.backgroundColor = UIColor.clear
                            self.opponentProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = self.opponentProfileImage.bounds
                            self.opponentProfileImage.addSubview(blurEffectView)
                            
                            self.opponentProfilePic = self.opponentProfileImage.image!
                        }
                    }
                }
          })
    }
}

    
    // MARK: - ****** Fetch My Profile Picture from Firebase ******
    func fetchMyProfilePicture()
    {
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            FIRDatabase.database().reference().child("Users").child(id).child("ProfilePicture").observe(FIRDataEventType.value, with: { snapshot in
                if snapshot.value is NSNull{
                    self.myProfileImage = (UserDefaults.standard.value(forKey: "userProfilePicture") as? String)!
                    return
                }
                self.myProfileImage = snapshot.value as! String
            })
        }
    }

    
    
    // MARK: - ****** Fetch Opponent's Messages Visibility Settings For Notification. ******
    func fetchOwnerMessagesVisibilitySettings()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            let ref =  FIRDatabase.database().reference()
            
                if self.opponentFirebaseUserID != ""
                {
                    ref.child("Users").child(self.opponentFirebaseUserID).observe(FIRDataEventType.value, with: { (snapshot) in
                        if snapshot.value is NSNull{
                            return
                        }
                        
                        self.ownerDetailsDict = snapshot.value as! NSMutableDictionary
                        let keysArray = self.ownerDetailsDict.allKeys as! [String]
                        if keysArray.contains("myDeviceToken")
                        {
                            self.opponentUserDeviceId = (self.ownerDetailsDict.value(forKey: "myDeviceToken") as? String)!
                        }
                        else
                        {
                            self.opponentUserDeviceId = ""
                        }
                        print("self.opponentUserDeviceId = \(self.opponentUserDeviceId)")
                        
                        var settingsDict = NSMutableDictionary()
                        var getNotificationDict = NSMutableDictionary()
                        if keysArray.contains("Settings")
                        {
                            settingsDict = self.ownerDetailsDict.value(forKey: "Settings") as! NSMutableDictionary
                            
                            if (settingsDict["Notification"] != nil)
                            {
                                getNotificationDict = settingsDict.value(forKey: "Notification") as! NSMutableDictionary
                                
                                if (getNotificationDict["Messages"] != nil)
                                {
                                    self.opponentMessagesVisibilityStr = getNotificationDict.value(forKey: "Messages") as! String
                                }
                                else
                                {
                                    self.opponentMessagesVisibilityStr = "On"
                                }
                            }
                            else{
                                self.opponentMessagesVisibilityStr = "On"
                            }
                         
                        }
                        else
                        {
                            self.opponentMessagesVisibilityStr = "On"
                        }
                })
            }
        }
    }

    
    
    // MARK: - ****** Method For Sending Push Notification to the Post's Owner. ******
//    func sendNotifications(_ message: String, andToken token: String)
    func sendNotifications(message: String, token: String, title: String)
    {
//        let titleStr = "Wi Match"
        let typeOfPush = "Messages"
//        if let refreshedToken = FIRInstanceID.instanceID().token()
//        {
            if self.getUserStatus == "offline"
            {
                self.badgeCount = self.badgeCount + 1
            }
        
            var typeUSers = String()
            var post = NSString()
        
        if (self.typeOfUser == "AnonymousUser")
        {
            if (self.confessionTypeUser == "AnonymousUser")
            {
                typeUSers = "withAnonymousChat"
            }else{
                typeUSers = "WithProfileConfessionUser"
            }
        }else{
            typeUSers = "withProfileChat"
        }
        
        
            if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
            {
                if (self.opponentVisibilityFromChatStr == "")
                {
                    self.opponentVisibilityFromChatStr = self.opponentFirebaseUserID + "@" + self.opponentSettingProfileInvisibilityStr
                }
                
                if (self.myVisiblityFromChatStr == "")
                {
                    if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                    {
                        self.myVisiblityFromChatStr = id + "@" + self.mySettingProfileInvisibilityStr
                    }
                }
                
               
               
               post = NSString(format:"{\"to\":\"\(token)\",\"notification\":{\"badge\":\"\(self.badgeCount)\",\"body\":\"\(message)\", \"title\":\"\(title)\", \"type\":\"\(typeOfPush)\", \"opponentSettingProfileInvisibilityStr\":\"\(self.opponentVisibilityFromChatStr)\", \"mySettingProfileInvisibilityStr\":\"\(self.myVisiblityFromChatStr)\", \"checkVisibiltyStr\":\"\(self.checkVisibiltyStr)\", \"OpponentUserID\":\"\(self.myID)\", \"OpponentFirebaseUserID\":\"\(self.myFirebaseID)\",\"OpponentProfileImage\":\"\(self.myProfileImage)\" , \"OpponentName\":\"\(self.myName)\", \"postIDStr\":\"\(self.postID)\", \"UsersType\":\"\(typeUSers)\"},\"priority\":10}" as NSString)
            }
            else
            {
                post = NSString(format:"{\"to\":\"\(token)\",\"notification\":{\"badge\":\"\(self.badgeCount)\",\"body\":\"\(message)\", \"title\":\"\(title)\", \"type\":\"\(typeOfPush)\" , \"checkVisibiltyStr\":\"\(self.checkVisibiltyStr)\", \"OpponentUserID\":\"\(self.myID)\", \"OpponentFirebaseUserID\":\"\(self.myFirebaseID)\",  \"OpponentProfileImage\":\"\(self.myProfileImage)\" , \"OpponentName\":\"\(self.myName)\", \"postIDStr\":\"\(self.postID)\", \"UsersType\":\"\(typeUSers)\"},\"priority\":10}" as NSString)
            }
            
            print("post = \(post)")
            
            var dataModel = post.data(using:  String.Encoding.nonLossyASCII.rawValue, allowLossyConversion: true)!
            let postLength = String(dataModel.count)
            let url = NSURL(string: "https://fcm.googleapis.com/fcm/send")
            let urlRequest = NSMutableURLRequest(url: url! as URL)
            urlRequest.httpMethod = "POST"
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("key=AAAA3VkUSMY:APA91bEnZp6IjfNV1ejbgr0coBzQeQoTLB3vZxq4OhXkbBhbruPfugX6IKXM69Zht9voYGOnPCU4IgLtvIzAUlqq1nrJMIrpi44YG2t0Ngl-8detdPjYF8Dr1wFlH3bnLibe-54NM6OQ", forHTTPHeaderField: "Authorization")
            urlRequest.httpBody = dataModel
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            let task = URLSession.shared.dataTask(with: urlRequest as URLRequest) { (data, response, error) -> Void in
                if let urlContent = data {
                    do {
                        if (self.opponentFirebaseUserID != "")
                        {
                            var dataDict = [String: Int]()
                            dataDict["AppBadgeCount"] = self.badgeCount
                            let ref = FIRDatabase.database().reference()
                        ref.child("Users").child(self.opponentFirebaseUserID).updateChildValues(dataDict)
                            
                            
                            var totalCount = Int()
                            if self.MessagesCount != ""
                            {
                                totalCount = Int(self.MessagesCount)!
                                totalCount = totalCount + 1
                            }
                            
                            var dataDict1 = [String : Any]()
                            dataDict1["MessagesCount"] = "\(totalCount)"
                        ref.child("Users").child(self.opponentFirebaseUserID).updateChildValues(dataDict1)
                            
//                            self.updateBadgeValueForVisitors(totalBadgeCount: self.badgeCount, completion: { (result) in
//
//                            })
                            
                            self.updateBadgeValueForVisitors(totalBadgeCount: totalCount, completion: { (result) in
                                
                            })
                            
                        }
                    } catch {
                        print("JSON serialization failed = \(error.localizedDescription)")
                    }
                    
                } else {
                    print("ERROR FOUND HERE = \(String(describing: error?.localizedDescription))")
                }
            }
            task.resume()
//        }
    }
    
    func updateBadgeValueForVisitors(totalBadgeCount: Int ,completion: @escaping (Bool) -> Swift.Void)
    {
        if self.opponentUserID != ""
        {
            if self.opponentAuthToken != ""
            {
                ApiHandler.updateBadgeCountValue(user_id: self.opponentUserID, auth_token: self.opponentAuthToken, typeofBadgeCountToBeUpdated: "messages_badgecount", totalBadgeCount: totalBadgeCount, completion: { (responseData) in
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
//                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        completion(true)
                    }
                    else
                    {
                        completion(false)
                    }
                })
            }
        }
    }
    
    // MARK: - ****** viewDidDisappear. ******
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self)
        
        let statusInternet = Reach().connectionStatus()
        switch statusInternet
        {
        case .unknown, .offline:
            print("Not connected")
            
        case .online(.wwan) , .online(.wiFi):
    
            if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).child("badgeCount").removeAllObservers()
                FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(self.opponentFirebaseUserID).removeAllObservers()
                FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).removeAllObservers()
                FIRDatabase.database().reference().child("conversations").removeAllObservers()
                FIRDatabase.database().reference().child("Users").child(currentUserID).child("ProfilePicture").removeAllObservers()
                FIRDatabase.database().reference().child("Users").child(currentUserID).removeAllObservers()
                FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(self.opponentFirebaseUserID).child("userType").removeAllObservers()
                FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).child("conversations").child(currentUserID).child("userType").removeAllObservers()
                FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).child("MessagesCount").removeAllObservers()
                FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).child("userStatus").removeAllObservers()
                
                let ref = FIRDatabase.database().reference()
                ref.child("BlockedUsers").child(currentUserID).removeAllObservers()
             ref.child("Users").child(currentUserID).child("blockedByUser").removeAllObservers()
            }
        }
    }
    
    

    
    // MARK: - ****** Identify user profile revealed or not. ******
    /*func checkProfileRevealed()
    {
        if let currentUserID = FIRAuth.auth()?.currentUser?.uid
        {
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(self.opponentUserID).observe(.value, with: { (snapshot) in
                
                print("snap.value = \(snapshot.value)")
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    FIRDatabase.database().reference().child("conversations_anonymous").child(location).observe(.childChanged, with: { (snap) in
                        print("snap.value = \(snap.value)")
                        if snap.exists()
                        {
                                let receivedMessage = snap.value as! [String: Any]
                                print(receivedMessage)
                                if receivedMessage["content"] != nil
                                {
                                    let toID = receivedMessage["toID"] as! String
                                    let typeOfUser = receivedMessage["typeOfUser"] as! String
                                    let fromID = receivedMessage["fromID"] as! String
                                    
                                    if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                                    {
                                        if ((fromID == id) && (typeOfUser == "WithProfileUser"))
                                        {
                                            self.usersVew.isHidden = true
                                        }
                                        else if  ((toID == id) || (typeOfUser == "WithProfileUser"))
                                        {
                                            self.opponentProfileImage.image = self.opponentProfilePic
                                            self.opponentName_lbl.text = self.opponentName
                                        }
                                    }
                                }
                           }
                    })
                }
            })
        }
    } */
    
    
    
    // MARK: - ****** Reveal My Profile Method. ******
    func RevealMyProfile()
    {

            if (self.myProfileVisibilityStr == "on") || (self.myProfileVisibilityStr == "On")
            {
                if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                {
                    let opponentWithPostId = self.opponentFirebaseUserID + "@" + self.postID
                    FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(opponentWithPostId).observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        if snapshot.exists() {
                            let data = snapshot.value as! [String: String]
                            
                            if (data["location"] != nil) && (data["userType"] != nil)
                            {
                                let location = data["location"]!
                                FIRDatabase.database().reference().child("conversations_anonymous").child(location).observeSingleEvent(of: .value, with: { (snap) in
                                    if snap.exists()
                                    {
                                        for snap in snap.children
                                        {
                                            let particularChildKey = (snap as! FIRDataSnapshot).key
                                            
                                            print("particularChildKey = \(particularChildKey)")
                                            
                                            let receivedMessage = (snap as! FIRDataSnapshot).value as! [String: Any]
                                            print(receivedMessage)
                                            if receivedMessage["content"] != nil
                                            {
//                                                let fromID = receivedMessage["UserID"] as! String
//                                                if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
//                                                {

                                                        if self.profileRevealedBool == false
                                                        {
                                                            self.profileRevealedBool = true
                                                            
                                                            let data = ["checkRevealProfile": "profileRevealed"]
                                                            
                                                            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(opponentWithPostId).updateChildValues(data)
                                                            
                                                            self.items = []
                                                            
                                                            self.getOpponentProfileStatus()
                                                            
                                                        }
//                                                    }
//                                                }
                                            }
                                        }
                                    }
                                })
                            }
                        }
                        else
                        {
                            let alert = UIAlertController(title: "", message: "To reveal your profile you must need to start conversation.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
                else{
                    print("currentUserID is not fetched")
                }
            }
            else
            {
                let alert = UIAlertController(title: "", message: "You can't reveal your profile until Turn ON your profile visibility from settings.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                }))
                self.present(alert, animated: true, completion: nil)
            }
        
    }
    
    
    // MARK: - ****** ViewWillAppear. ******
    override func viewWillAppear(_ animated: Bool)
    {
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
        fetchOpponentDetails()
        
        if let profile_pic_thumb = UserDefaults.standard.value(forKey: "profile_pic_thumb") as? String
        {
            if profile_pic_thumb != ""
            {
                self.myProfilePicLink = profile_pic_thumb
            }
            else
            {
                if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                {
                    self.myProfilePicLink = userProfilePicture
                }
            }
        }
        else
        {
            if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
            {
                self.myProfilePicLink = userProfilePicture
            }
//            self.myProfilePicLink = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
        }
    }
 
    
    // MARK: - ***** Block opponent user Btn Action *****
    @IBAction func blockUser_BtnAction(_ sender: Any) {
        if (self.blockedByMeStatus == "yes")
        {
            self.blockUserTitleLbl.text = "User Blocked"
        }
        else{
            self.blockUserTitleLbl.text = "Block User"
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
         
                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                    {
                        if self.opponentUserID != ""
                        {
                            self.loadingView.isHidden = false
                            self.view.isUserInteractionEnabled = false
                            
                            var userTypeStr = String()
                            
                            if (self.opponentName_lbl.text == "Anonymous User")
                            {
                                userTypeStr = "confessions"
                            }
                            else{
                                userTypeStr = "profile"
                            }
                            
                            ApiHandler.blockUnblockUser(userID: userID, authToken: userToken, other_user_id: self.opponentUserID,typeOfBlockUser: userTypeStr, completion: { (responseData) in
                                
                                if (responseData.value(forKey: "status") as? String == "200") 
                                {
//                                    let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                    
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    
                                    let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                        action in
                                        
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userBlockedFromMessages"), object: nil)

                                        self.getStatusOfOpponent(completion: { (result) in
                                           
                                        })
                                        
                                    }))
                                    
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else if (responseData.value(forKey: "status") as? String == "400")
                                {
                                    let message = responseData.value(forKey: "message") as? String
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else
                                {
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            })
                        }
                    }
                }
                
            }
        }
    }

    
    
    // MARK: - ****** Update Chat Messages After Deleting message by the owner message. ******
    func updateChatDeleted_AnonymousUser()
    {
//        if let currentUserID = FIRAuth.auth()?.currentUser?.uid
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            let postIDStr = self.opponentFirebaseUserID + "@" + self.postID
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(postIDStr).observe(.value, with: { (snapshot) in
                if snapshot.exists()
                {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    FIRDatabase.database().reference().child("conversations_anonymous").child(location).observe(.childRemoved, with: { (snap) in
                        if snap.exists()
                        {
                            let receivedMessage = snap.value as! [String: Any]
                    
                            for index in 0..<self.items.count
                            {
                                var deleteMessageTimeStamp = String()
                                deleteMessageTimeStamp = String(receivedMessage["timestamp"] as! Double)
                                
                                self.readMsgBool = true
                                
                                var getTimeStamp = String()
                                getTimeStamp = String(self.items[index].timestamp)
                                
                                if getTimeStamp == deleteMessageTimeStamp
                                {
                                    print("==== timestamp matched ====")
                                    self.items.remove(at: index)
                                    self.messages_tableView.reloadData()
                                    
                                    break
                                }
                                else
                                {
                                    print("==== timestamp doesn't matched ====")
                                }
                                
                            }
                        }
                        else
                        {
                            print("child doesnot exists")
                        }
                    })
                }
                else
                {
                    print("child doesnot exists")
                }
            })
        }
    }
    
    
    // MARK: - ****** Update Chat Messages After Deleting message by the owner message. ******
    func updateChatDeleted()
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            var opponentIDWithVisibilityStr = String()
            opponentIDWithVisibilityStr = self.opponentFirebaseUserID + "@" +  self.opponentSettingProfileInvisibilityStr + "@" + self.mySettingProfileInvisibilityStr
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(opponentIDWithVisibilityStr).observe(.value, with: { (snapshot) in
                if snapshot.exists()
                {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    FIRDatabase.database().reference().child("conversations").child(location).observe(.childRemoved, with: { (snap) in
                        if snap.exists()
                        {
                            let receivedMessage = snap.value as! [String: Any]
                            

                            for index in 0..<self.items.count
                            {
                                var deleteMessageTimeStamp = String()
                                if let timeStamp = receivedMessage["timestamp"] as? Double
                                {
                                    print("timeStamp = \(timeStamp)")
                                    deleteMessageTimeStamp = String(receivedMessage["timestamp"] as! Double)
                                    
                                    self.readMsgBool = true
                                    
                                    var getTimeStamp = String()
                                    getTimeStamp = String(self.items[index].timestamp)
                                    
                                    if getTimeStamp == deleteMessageTimeStamp
                                    {
                                        print("==== timestamp matched ====")
                                        self.items.remove(at: index)
                                        self.messages_tableView.reloadData()
                                        
                                        break
                                    }
                                    else
                                    {
                                        print("==== timestamp doesn't matched ====")
                                    }
                                }
                            }
                        }
                        else
                        {
                            print("child doesnot exists")
                        }
                    })
                }
                else
                {
                    print("child doesnot exists")
                }
            })
        }
    }
 
    
    // MARK: - ****** Fetch Current Users Details From Firebase. ******
    func getUserDetails()
    {
        if (UserDefaults.standard.value(forKey: "userDetailsDict") != nil)
        {
            usersDetailsDict = UserDefaults.standard.value(forKey: "userDetailsDict") as! NSMutableDictionary
        }
    }
    
    
    // MARK: - ****** Download Chat Messages After Child Removed. ******
    func fetchDataAfterChildRemoved() {
        Message.downloadAllMessagesAfterChildRemoved(forUserID: self.opponentFirebaseUserID, completion: {[weak weakSelf = self] (message) in
            weakSelf?.items.append(message)
            weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
            DispatchQueue.main.async {
                if let state = weakSelf?.items.isEmpty, state == false {
                    weakSelf?.messages_tableView.reloadData()
                    
                    if self.items.count > 1
                    {
                         weakSelf?.messages_tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                    }
                }
            }
        })
        Message.markMessagesRead(forUserID: self.opponentFirebaseUserID, opponentVisibility: self.opponentSettingProfileInvisibilityStr, myVisibility: self.mySettingProfileInvisibilityStr)
    }
    
    
    // MARK: - ****** Download All Chat Messages For particular conversation. ******
    func fetchData_AnonymousUser() {
       if self.postID != ""
       {
        
        Message.downloadAllMessages_anonymousUser(forUserID: self.opponentFirebaseUserID, postId: self.postID, completion: {[weak weakSelf = self] (message) in
            
            weakSelf?.items.append(message)
            weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
            
            if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                if (message.userID) == id
                {
                    self.myProfileVisibilityArray.append(message.myProfileInvisibilitySaved!)
                }
                else{
                    self.opponentProfileVisibilityArray.append(message.myProfileInvisibilitySaved!)
                }
            }
            
            DispatchQueue.main.async {
                if let state = weakSelf?.items.isEmpty, state == false {
                    weakSelf?.messages_tableView.reloadData()
                    
                    if self.items.count > 1
                    {
                         weakSelf?.messages_tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                    }
                   
                    if let viewControllers = self.navigationController?.viewControllers {
                        if let vc = viewControllers.last
                        {
                            if vc.isKind(of: MessagesViewController.self)
                            {
                                print("current opened screen is MessagesViewController")
                                if self.postID != ""
                                {
                                    let postIdStr = self.opponentFirebaseUserID + "@" + self.postID
                                    Message.markMessagesRead_AnonymousUser(forUserID: postIdStr)
                                }
                                
                            }
                            else
                            {
                                print("current opened screen is not MessagesViewController")
                            }
                        }
                    }
                }
            }
        })
        
        }
    }

    
    func getTotalMessagesCountInIndividualChat(completion: @escaping (Bool) -> Swift.Void)
    {
       
    }

    
    // MARK: - ****** Download All Chat Messages For particular conversation. ******
    func fetchData()
    {
        var paginationMessagesCount = Int()
        if (self.totalMessagesInt < 10)
        {
            paginationMessagesCount = self.totalMessagesInt
        }
        else
        {
            if (self.totalMessagesInt % 10 == 0)
            {
                paginationMessagesCount = 10
            }
            else
            {
                paginationMessagesCount = self.totalMessagesInt
            }
        }
        
        Message.downloadAllMessages(forUserID: self.opponentFirebaseUserID, opponentProfileVisibilityStr: self.opponentSettingProfileInvisibilityStr, myProfileVisibilityStr: self.mySettingProfileInvisibilityStr, getPaginationCount: paginationMessagesCount, completion: {[weak weakSelf = self] (message) in
            
                weakSelf?.items.append(message)
                weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
       
                DispatchQueue.main.async {
                    if let state = weakSelf?.items.isEmpty, state == false {
                        weakSelf?.messages_tableView.reloadData()
                        
                        if self.items.count > 1
                        {
                            weakSelf?.messages_tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                        }
                        
                        if let viewControllers = self.navigationController?.viewControllers {
                            if let vc = viewControllers.last
                            {
                                if vc.isKind(of: MessagesViewController.self)
                                {
                                    print("current opened screen is MessagesViewController")
                                    Message.markMessagesRead(forUserID: self.opponentFirebaseUserID, opponentVisibility: self.opponentSettingProfileInvisibilityStr, myVisibility: self.mySettingProfileInvisibilityStr)
                                }
                                else
                                {
                                    print("current opened screen is not MessagesViewController")
                                }
                            }
                        }
                    }
                }
            })
//        }
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    
    // MARK: - ****** Animate Options Button Action. ******
    func animateExtraButtons(toHide: Bool)  {
        
       /* if (self.mySettingProfileInvisibilityStr == "On") && (self.opponentSettingProfileInvisibilityStr == "On")
        {
            if self.typeOfUser == "AnonymousUser"
            {
                if (self.mainConfessionCheck == "comingFromconfession")
                {
                    self.checkForAnonymousChat(postID: self.postID, oppoID: self.opponentUserID)
                }
            }
        }
        else if (self.mySettingProfileInvisibilityStr == "Off") && (self.opponentSettingProfileInvisibilityStr == "Off")
        {
            self.writeText_txtView.resignFirstResponder()
            let alert = UIAlertController(title: "", message: "You can't send more messages until you and other user can Turn ON profile visibility from settings.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                self.writeText_txtView.resignFirstResponder()
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else if (self.mySettingProfileInvisibilityStr == "Off")
        {
            self.writeText_txtView.resignFirstResponder()
            let alert = UIAlertController(title: "", message: "You can't send more messages until Turn ON your profile visibility from settings.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                self.writeText_txtView.resignFirstResponder()
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else if (self.opponentSettingProfileInvisibilityStr == "Off")
        {
            self.writeText_txtView.resignFirstResponder()
            let alert = UIAlertController(title: "", message: "You can't send more messages until opponent Turn ON his/her profile visibility from settings.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                self.writeText_txtView.resignFirstResponder()
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else{*/
            switch toHide {
            case true:
                var frameOfOptionView = self.sendMessageView.frame as CGRect
                UIView.animate(withDuration: 0.1) {
                    
                    self.sendMessageView.frame = CGRect.init(x: frameOfOptionView.origin.x, y: 0, width: frameOfOptionView.size.width, height: frameOfOptionView.size.height)
                    
                    frameOfOptionView = self.optionView.frame as CGRect
                    self.optionView.frame = CGRect.init(x: frameOfOptionView.origin.x, y: 0-frameOfOptionView.size.height, width: frameOfOptionView.size.width, height: frameOfOptionView.size.height)
                }
            default:
                
                var frameOfOptionView = self.optionView.frame as CGRect
                
                UIView.animate(withDuration: 0.1) {
                    
                    self.optionView.frame = CGRect.init(x: frameOfOptionView.origin.x, y: 0, width: frameOfOptionView.size.width, height: frameOfOptionView.size.height)
                    
                    frameOfOptionView = self.sendMessageView.frame as CGRect
                    
                    self.sendMessageView.frame = CGRect.init(x: frameOfOptionView.origin.x, y: self.optionView.frame.origin.y + self.optionView.frame.size.height, width: frameOfOptionView.size.width, height: frameOfOptionView.size.height)
                }
            }
//        }
    }
    
    // MARK: - ****** ViewWillDisAppear. ******
    override func viewWillDisappear(_ animated: Bool) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
         
            if readMsgBool == false
            {
                 Message.markMessagesRead(forUserID: self.opponentFirebaseUserID, opponentVisibility: self.opponentSettingProfileInvisibilityStr, myVisibility: self.mySettingProfileInvisibilityStr)
            }
            else
            {
                self.readMsgBool = false
            }
            
        }
       
    }
    
    // MARK: - ****** ViewDidAppear ******
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        NotificationCenter.default.addObserver(self, selector: #selector(MessagesViewController.showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
          NotificationCenter.default.addObserver(self, selector: #selector(MessagesViewController.hideKeyboard(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    //MARK: ****** NotificationCenter handlers ******
    func hideKeyboard(notification: Notification) {
            self.chatView.frame.origin.y = self.messages_tableView.frame.origin.y + self.messages_tableView.frame.size.height
            self.messages_tableView.contentInset = UIEdgeInsets.zero
            self.messages_tableView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue
        {
                let height = frame.cgRectValue.height
                self.chatView.frame.origin.y = frame.cgRectValue.origin.y - self.chatView.frame.size.height
                self.messages_tableView.contentInset.bottom = height
                self.messages_tableView.scrollIndicatorInsets.bottom = height
                if self.items.count > 1
                {
                    self.messages_tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
                }
         }
    }
    
    // MARK: - ****** Close ImageView button Action. ******
    @IBAction func closeViewImage_btnAction(_ sender: Any) {
        self.viewImageVIew.isHidden = true
    }
    
    
    // MARK: - ****** Fetch Opponent Users Details From Firebase. ******
    func fetchOpponentDetails()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
         
            print("online")
            
           /* let childRef = FIRDatabase.database().reference()
            childRef.child("Users").child(self.opponentUserID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                let detailDict = snapshot.value as! NSMutableDictionary
                UserDefaults.standard.set(detailDict, forKey: "otherUsersDict")
                UserDefaults.standard.synchronize()
            })*/
        }
       
    }
    
    // MARK: - ****** View opponent's Profile button Action. ******
    @IBAction func opponentProfileView_btnAction(_ sender: Any) {
        
        self.view.endEditing(true)
        self.writeText_txtView.resignFirstResponder()

        var checkUserNameStr = String()
        checkUserNameStr = self.opponentName_lbl.text!
        
        if self.blockedByOpponentStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "You’ve Been Blocked For Being Outta Timin.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.blockedByMeStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.reportedByMeStatus == "yes"
        {
            
            let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            
            
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    print("Not connected")
                    
                case .online(.wwan) , .online(.wiFi):
                 
                    self.updateContinueChatWithReportedUser()
                }
                
                self.viewOpponentProfileStatus()
                
            }))
            
            if (self.reportedByMe_ContinueChat == "0")
            {
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.viewOpponentProfileStatus()
            }
            
        }
        else if self.reportedUsersStr == "userAlreadyReportedMe"
        {
//            let alert = UIAlertController(title: "", message: "User Reported You.", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
        }
        else if (checkUserNameStr.range(of: "VIP Member") != nil)
        {
//            let alert = UIAlertController(title: "", message: "VIP Member", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
        }
        else if (checkUserNameStr.range(of: "Anonymous User") != nil)
        {
//            let alert = UIAlertController(title: "", message: "Anonymous User", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
        }
        else
        {
           self.viewOpponentProfileStatus()
        }
    }

    func viewOpponentProfileStatus()
    {
        if (self.opponentFirebaseUserID != "")
        {
            var profileVisibilityStr = String()
            FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).keepSynced(true)
            FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull{
                    print("snapshot.value is NSNull")
                }
                else
                {
                    let userDict = snapshot.value as? NSDictionary
                    print("userDict = \(String(describing: userDict))")
                    
                    var auth_token = String()
                    if userDict!["auth_token"] != nil
                    {
                        auth_token = userDict?.value(forKey: "auth_token") as! String
                    }
                    else{
                        auth_token = ""
                    }
                    print("auth_token = \(String(describing: auth_token))")
                    
                    var profile_visibility = String()
                    if userDict!["profile_visibility"] != nil
                    {
                        profile_visibility = userDict?.value(forKey: "profile_visibility") as! String
                    }
                    else{
                        profile_visibility = "On"
                    }
                    print("profile_visibility = \(String(describing: profile_visibility))")
                    
                    if (profile_visibility == "on")  || (profile_visibility == "On")
                    {
                        if self.viewOpponentProfile == "fromParticularUserScreen"
                        {
                            self.navigationController!.popViewController(animated: true)
                        }
                        else
                        {
                            if self.typeOfUser == "AnonymousUser"
                            {
                                print("AnonymousUser AnonymousUser AnonymousUser AnonymousUser")
                            }
                            else
                            {
                                let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                                viewProfileScreen.ownerID = self.opponentUserID
                                viewProfileScreen.ownerAuthToken = auth_token
                                
                                viewProfileScreen.isKindOfScreen = "MessagesScreen"
                                self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                            }
                        }
                    }
                    else
                    {
                        print("opponent's profile visibility is off")
                    }
                }
            })
        }
    }
    
    
    // MARK: - ****** Close Image Btn Action. ******
    @IBAction func imageBack_btnAction(_ sender: Any) {
        self.selectedImageView.isHidden = true
    }
    
    // MARK: - ****** Upload Image Btn Action. ******
    @IBAction func sendImage_btnAction(_ sender: Any) {
        
        if self.blockedByOpponentStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "You’ve Been Blocked For Being Outta Timin.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.blockedByMeStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.reportedUsersStr == "userAlreadyReportedByMe"
        {
            let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            
            
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    print("Not connected")
                    
                case .online(.wwan) , .online(.wiFi):
                  
                     self.updateContinueChatWithReportedUser()
                    
                }
                
                self.sendImageToServer()
            }))
            
            if (self.statusOfContinueStr == false)
            {
                self.present(alert, animated: true, completion: nil)
            }
            else{
                self.sendImageToServer()
            }
        }
        else if self.reportedUsersStr == "userAlreadyReportedMe"
        {
            let alert = UIAlertController(title: "", message: "User Reported You.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
           self.sendImageToServer()
        }
    }
    
    
    func sendImageToServer()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
           
            let name = UserDefaults.standard.value(forKey: "userName") as! String
            
            if (self.opponentUserDeviceId != "") && ((self.opponentMessagesVisibilityStr == "on") || (self.opponentMessagesVisibilityStr == "On"))
            {
                if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
                {
                    if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                    {
//                        self.sendNotifications("\(name) sent a photo to you.", andToken: self.opponentUserDeviceId)
                        self.sendNotifications(message: "\(name) sent a photo to you.", token: self.opponentUserDeviceId, title: "Wi Match")
                    }
                    else
                    {
//                        self.sendNotifications("VIP Member sent a photo to you.", andToken: self.opponentUserDeviceId)
                        self.sendNotifications(message: "VIP Member sent a photo to you.", token: self.opponentUserDeviceId, title: "Wi Match")
                    }
                }
                else
                {
                    if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                    {
                        if (self.myProfileStatus == "profileRevealed") //&& (self.myProfileVisibilityStatus == "profileVisibilityPurchased")
                        {
//                            self.sendNotifications("\(name) sent a photo to you.", andToken: self.opponentUserDeviceId)
                             self.sendNotifications(message: "\(name) sent a photo to you.", token: self.opponentUserDeviceId, title: "Wi Match")
                        }
                        else
                        {
//                            self.sendNotifications("Anonymous User sent a photo to you.", andToken: self.opponentUserDeviceId)
                             self.sendNotifications(message: "Anonymous User sent a photo to you.", token: self.opponentUserDeviceId, title: "Wi Match")
                        }
                        
                    }
                    else
                    {
                        if (self.myProfileStatus == "profileRevealed") //&& (self.myProfileVisibilityStatus == "profileVisibilityPurchased")
                        {
//                            self.sendNotifications("\(name) sent a photo to you.", andToken: self.opponentUserDeviceId)
                             self.sendNotifications(message: "\(name) sent a photo to you.", token: self.opponentUserDeviceId, title: "Wi Match")
                        }
                        else
                        {
//                            self.sendNotifications("Anonymous User sent a photo to you.", andToken: self.opponentUserDeviceId)
                             self.sendNotifications(message: "Anonymous User sent a photo to you.", token: self.opponentUserDeviceId, title: "Wi Match")
                        }
                    }
                }
                
            }
            self.loadingView.isHidden = false
            self.view.isUserInteractionEnabled = false
            self.composeMessage(type: .photo, content: self.selectedImage.image!, confessionStatusText: "", confessionImageType: "", confessionCheck: "")
        }
    }
    
    
    // MARK: - ****** Users Button Action. ******
    @IBAction func Users_btnAction(_ sender: Any) {
        if let id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            if self.blockedByOpponentStatus == "yes"
            {
                let alert = UIAlertController(title: "", message: "You’ve Been Blocked For Being Outta Timin.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.blockedByMeStatus == "yes"
            {
                let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if self.reportedByMeStatus == "yes"
            {
                let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                
                
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        print("Not connected")
                        
                    case .online(.wwan) , .online(.wiFi):
                      
                        self.updateContinueChatWithReportedUser()
                        
                    }
                }))
                
                if (self.reportedByMe_ContinueChat == "0")
                {
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else if self.reportedUsersStr == "userAlreadyReportedMe"
            {
//                let alert = UIAlertController(title: "", message: "User Reported You.", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
            }
            else if self.reportedByMeStatus == "yes"
            {
                let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                
                
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        print("Not connected")
                        
                    case .online(.wwan) , .online(.wiFi):
                       
                       self.updateContinueChatWithReportedUser()
                    }
                    
                }))
                
                if (self.reportedByMe_ContinueChat == "0")
                {
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else if self.reportedUsersStr == "userAlreadyReportedMe"
            {
//                let alert = UIAlertController(title: "", message: "User Reported You.", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                FIRDatabase.database().reference().child("Users").child(id).child("Settings").child("General").child("Profile_Visibility").keepSynced(true)
                
                FIRDatabase.database().reference().child("Users").child(id).child("Settings").child("General").child("Profile_Visibility").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                    
                    if snapshot.value is NSNull{
                        self.myProfileVisibilityStr = "On"
                    }
                    else
                    {
                        self.myProfileVisibilityStr = snapshot.value as! String
                    }
                    
                    self.RevealMyProfile()
                })
            }
        }
    }
    
    // MARK: - ****** Back Button Action. ******
    @IBAction func back_btnAction(_ sender: Any) {
        if appDelegatePush == "fromAppDelegate"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        else
        {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    
    // MARK: - ****** Open Text Message View Button Action. ******
    @IBAction func sendTextMessage_btnAction(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
    }

    // MARK: - ****** Choose Other Options Button Action. ******
    @IBAction func chooseOptions_btnAction(_ sender: Any) {
        self.animateExtraButtons(toHide: false)
    }
    
    // MARK: - ****** Upload Text Message Button Action. ******
    @IBAction func sendMessage_btnAction(_ sender: Any) {
        
        if self.blockedByOpponentStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "You’ve Been Blocked For Being Outta Timin.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.blockedByMeStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.reportedByMeStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            
            
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    print("Not connected")
                    
                case .online(.wwan) , .online(.wiFi):
                  
                    self.updateContinueChatWithReportedUser()
                    
                }
                
                self.sendTextToServer()
            }))
            
            if (self.reportedByMe_ContinueChat == "0")
            {
                self.present(alert, animated: true, completion: nil)
            }
            else{
                self.sendTextToServer()
            }
        }
        else if self.reportedUsersStr == "userAlreadyReportedMe"
        {
//            let alert = UIAlertController(title: "", message: "User Reported You.", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.sendTextToServer()
        }
    }
    
    func sendTextToServer()
    {
        if let text = self.writeText_txtView.text {
            if text.count > 0 {
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                 
                    let name = UserDefaults.standard.value(forKey: "userName") as! String
                    
                    var textMessage = String()
                    textMessage = self.writeText_txtView.text!
                    if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false)
                    {
                        var search_txt: String = self.writeText_txtView.text
                        search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                        
                        self.composeMessage(type: .text, content: search_txt, confessionStatusText: "", confessionImageType: "", confessionCheck: "")
                        
                        if (self.opponentUserDeviceId != "") && ((self.opponentMessagesVisibilityStr == "on") || (self.opponentMessagesVisibilityStr == "On"))
                        {
                            if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
                            {
                                if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                                {
//                                    self.sendNotifications("\(name) sent a message to you.", andToken: self.opponentUserDeviceId)
                                    self.sendNotifications(message: search_txt, token: self.opponentUserDeviceId, title: "\(name)")
                                }
                                else
                                {
//                                    self.sendNotifications("VIP Member sent a message to you.", andToken: self.opponentUserDeviceId)
                                     self.sendNotifications(message: search_txt, token: self.opponentUserDeviceId, title: "VIP Member")
                                }
                            }
                            else
                            {
                                if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                                {
                                    if (self.myProfileStatus == "profileRevealed")
                                    {
//                                        self.sendNotifications("\(name) sent a message to you.", andToken: self.opponentUserDeviceId)
                                        self.sendNotifications(message: search_txt, token: self.opponentUserDeviceId, title: "\(name)")
                                    }
                                    else{
//                                        self.sendNotifications("Anonymous User sent a message to you.", andToken: self.opponentUserDeviceId)
                                        self.sendNotifications(message: search_txt, token: self.opponentUserDeviceId, title: "Anonymous User")
                                    }
                                }
                                else
                                {
                                    if (self.myProfileStatus == "profileRevealed")
                                    {
//                                        self.sendNotifications("\(name) sent a message to you.", andToken: self.opponentUserDeviceId)
                                        
                                         self.sendNotifications(message: search_txt, token: self.opponentUserDeviceId, title: "\(name)")
                                        
                                    }
                                    else{
//                                        self.sendNotifications("Anonymous User sent a message to you.", andToken: self.opponentUserDeviceId)
                                        
                                         self.sendNotifications(message: search_txt, token: self.opponentUserDeviceId, title: "Anonymous User")
                                    }
                                }
                            }
                        }
                    }
                    else{
                        print("you have entered empty string")
                    }
                    
                    
                    self.writeText_txtView.text = ""
                }
            }
        }
    }
    
    
    // MARK: - ****** Upload Message In Firebase Conversation Method. ******
    func composeMessage(type: MessageType, content: Any, confessionStatusText: String, confessionImageType: String, confessionCheck: String)
    {
      FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).keepSynced(true)
      FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
        
            if snapshot.value is NSNull
            {
                let alert = UIAlertController(title: "", message: "Wi User no longer available", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let name = UserDefaults.standard.value(forKey: "userName") as! String
                let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as! String
                
                var picture = String()
                if let profile_pic_thumb = UserDefaults.standard.value(forKey: "profile_pic_thumb") as? String
                {
                    if profile_pic_thumb != ""
                    {
                        picture = profile_pic_thumb
                    }
                    else{
                        picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
                    }
                }
                else{
                    picture = UserDefaults.standard.value(forKey: "userProfilePicture") as! String
                }
                
                
                var myAUthToken = String()
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    myAUthToken = userToken
                }
                
                if (self.typeOfUser == "AnonymousUser")
                {
                    if (self.confessionTypeUser == "AnonymousUser")
                    {
                       let message = Message.init(type: type, content: content, owner: .sender, timestamp: Double(NSDate().timeIntervalSince1970), isRead: false, profilePicture: picture, userName: name,userID: userID,typeOfUser: self.confessionTypeUser,checkRevealProfile: self.myProfileStatus + "@" + self.opponentProfileVisibilityStatus + "@" + self.myProfileVisibilityStatus,postID: self.postID, postedText: confessionStatusText, postedImageType: confessionImageType, postedConfessionCheck: confessionCheck, myProfileInvisibilitySavedStr: self.mySettingProfileInvisibilityStr, myConvoversationTypeStr: "withAnonymousChat", myRevealProfileStatusStr: self.myProfileStatus,opponentRevealProfileStatusStr: self.opponentProfileStatus, myPackagesStatusStr: self.myProfileVisibilityStatus, opponentPackageStatusStr: self.opponentProfileVisibilityStatus, auth_Token: myAUthToken, serverID: "")
                        
                        if ((self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")) && ((self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On"))
                        {
                            Message.sendMessage_AnonymousUser(postID: self.postID,message: message, typeOfUser: self.confessionTypeUser, toID: self.opponentFirebaseUserID,opponentProfileVisibilityStatus: "On", myProfileVisibilityStatus: "On" , completion:  {(_) in
                                
                                if let viewControllers = self.navigationController?.viewControllers {
                                    if let vc = viewControllers.last
                                    {
                                        if vc.isKind(of: MessagesViewController.self)
                                        {
                                            print("current opened screen is MessagesViewController")
                                            if self.postID != ""
                                            {
                                                //                                            let postIdStr = self.opponentUserID + "@" + self.postID
                                            }
                                        }
                                        else
                                        {
                                            print("current opened screen is not MessagesViewController")
                                        }
                                    }
                                }
                            })
                        }
                        else
                        {
                            Message.sendMessage_AnonymousUser(postID: self.postID,message: message, typeOfUser: self.confessionTypeUser, toID: self.opponentFirebaseUserID,opponentProfileVisibilityStatus: "Off", myProfileVisibilityStatus: "Off" , completion:  {(_) in
                                
                                if let viewControllers = self.navigationController?.viewControllers {
                                    if let vc = viewControllers.last
                                    {
                                        if vc.isKind(of: MessagesViewController.self)
                                        {
                                            print("current opened screen is MessagesViewController")
                                            if self.postID != ""
                                            {
                                                //                                            let postIdStr = self.opponentUserID + "@" + self.postID
                                            }
                                        }
                                        else
                                        {
                                            print("current opened screen is not MessagesViewController")
                                        }
                                    }
                                }
                            })
                        }
                    }
                    else
                    {
                        
                        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Double(NSDate().timeIntervalSince1970), isRead: false, profilePicture: picture, userName: name,userID: userID,typeOfUser: self.confessionTypeUser,checkRevealProfile: self.myProfileStatus + "@" + self.opponentProfileVisibilityStatus + "@" + self.myProfileVisibilityStatus,postID: self.postID, postedText: confessionStatusText, postedImageType: confessionImageType, postedConfessionCheck: confessionCheck, myProfileInvisibilitySavedStr: self.mySettingProfileInvisibilityStr, myConvoversationTypeStr: "WithProfileConfessionUser", myRevealProfileStatusStr: self.myProfileStatus,opponentRevealProfileStatusStr: self.opponentProfileStatus, myPackagesStatusStr: self.myProfileVisibilityStatus, opponentPackageStatusStr: self.opponentProfileVisibilityStatus, auth_Token: myAUthToken, serverID: "")
                        
                        if ((self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")) && ((self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On"))
                        {
                            Message.sendMessage_AnonymousUser(postID: self.postID,message: message, typeOfUser: self.confessionTypeUser, toID: self.opponentFirebaseUserID,opponentProfileVisibilityStatus: "On", myProfileVisibilityStatus: "On" , completion:  {(_) in
                                
                                if let viewControllers = self.navigationController?.viewControllers {
                                    if let vc = viewControllers.last
                                    {
                                        if vc.isKind(of: MessagesViewController.self)
                                        {
                                            if self.postID != ""
                                            {
                                                
                                            }
                                        }
                                        else
                                        {
                                            
                                        }
                                    }
                                }
                            })
                        }
                        else
                        {
                            Message.sendMessage_AnonymousUser(postID: self.postID,message: message, typeOfUser: self.confessionTypeUser, toID: self.opponentFirebaseUserID,opponentProfileVisibilityStatus: "Off", myProfileVisibilityStatus: "Off" , completion:  {(_) in
                                
                                if let viewControllers = self.navigationController?.viewControllers {
                                    if let vc = viewControllers.last
                                    {
                                        if vc.isKind(of: MessagesViewController.self)
                                        {
                                            if self.postID != ""
                                            {
                                                
                                            }
                                        }
                                        else
                                        {
                                            
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
                else
                {
                    var myAUthToken = String()
                    if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                    {
                        myAUthToken = userToken
                    }
                    
                    let message = Message.init(type: type, content: content, owner: .sender, timestamp: Double(NSDate().timeIntervalSince1970), isRead: false, profilePicture: picture, userName: name,userID: userID,typeOfUser: self.typeOfUser,checkRevealProfile: "withProfileChat",postID: self.postID, postedText: confessionStatusText, postedImageType: confessionImageType, postedConfessionCheck: confessionCheck, myProfileInvisibilitySavedStr: self.mySettingProfileInvisibilityStr, myConvoversationTypeStr: "withProfileChat", myRevealProfileStatusStr: self.myProfileStatus,opponentRevealProfileStatusStr: self.opponentProfileStatus, myPackagesStatusStr: self.myProfileVisibilityStatus, opponentPackageStatusStr: self.opponentProfileVisibilityStatus,auth_Token: myAUthToken, serverID: "")
                    
                    if ((self.opponentSettingProfileInvisibilityStr == "on")  || (self.opponentSettingProfileInvisibilityStr == "On")) && ((self.mySettingProfileInvisibilityStr == "on")  || (self.mySettingProfileInvisibilityStr == "On"))
                    {
                        Message.send(postID: self.postID, message: message, typeOfUser: self.typeOfUser, opponentProfileVisibilityStatus: "On", myProfileVisibilityStatus: "On", toID: self.opponentFirebaseUserID, completion: {(_) in
                            
                            if let viewControllers = self.navigationController?.viewControllers {
                                if let vc = viewControllers.last
                                    
                                {
                                    if vc.isKind(of: MessagesViewController.self)
                                    {
                                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateChatOnReceive"), object: nil, userInfo: nil)
                                    }
                                    else
                                    {
                                       
                                    }
                                }
                            }
                        })
                   
                    }
                    else
                    {
                        Message.send(postID: self.postID, message: message, typeOfUser: self.typeOfUser, opponentProfileVisibilityStatus: self.opponentSettingProfileInvisibilityStr, myProfileVisibilityStatus: self.mySettingProfileInvisibilityStr, toID: self.opponentFirebaseUserID, completion: {(_) in
                            
                            if let viewControllers = self.navigationController?.viewControllers {
                                if let vc = viewControllers.last
                                {
                                    if vc.isKind(of: MessagesViewController.self)
                                    {
                                        
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                            }
                        })
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: { () -> Void in
                    if type == .photo{
                        self.selectedImageView.isHidden = true
                    }
                    else if type == .audio
                    {
                        self.audioRecordingView.isHidden = true
                    }
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                })
            }
        })
    }
    
    
    // MARK: - ****** Audio Button Action. ******
    @IBAction func audio_btnAction(_ sender: Any) {
//        self.audioUpdate_lbl.isHidden = true
//        self.audioRecordingView.isHidden = false
//        self.finishAudioBtn.isUserInteractionEnabled = true
    }
    
    // MARK: - ****** Camera Button Action. ******
    @IBAction func camera_btnAction(_ sender: Any) {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if (status == .denied) {
            PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                let alert = UIAlertController(
                    title: "IMPORTANT",
                    message: "\("WI Match") Would like to capture the Photos",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                }))
                
                self.present(alert, animated: true, completion: nil)
            })
        }
        else if (status == .authorized)  || (status == .notDetermined)
        {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self;
                myPickerController.sourceType = UIImagePickerControllerSourceType.camera
//                myPickerController.mediaTypes = [kUTTypeImage as String]
                myPickerController.allowsEditing = false
                self.present(myPickerController, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "", message:  "Sorry,Camera is not available to take picture.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: - ****** Open Gallary Button Action. ******
    @IBAction func gallary_btnAction(_ sender: Any) {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if  status == .denied {
            PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                let alert = UIAlertController(
                    title: "IMPORTANT",
                    message: "\("WI Match") Would like to access the Photos",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Allow Photos", style: .cancel, handler: { (alert) -> Void in
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                }))
                self.present(alert, animated: true, completion: nil)
            })
        }
        else if status == .authorized || status == .notDetermined
        {
            self.photoLibrary()
        }
    }
    
    // MARK: - ****** Open Photo Library Method. ******
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        myPickerController.allowsEditing = false
        self.present(myPickerController, animated: true, completion: nil)
    }

    
    // MARK: -  ****** Open Camera Button Action. ******
    @IBAction func openCamera_btnAction(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.camera;
//            imag.mediaTypes = [kUTTypeImage]
            imag.allowsEditing = false
            
            self.present(imag, animated: true, completion: nil)
        }
    }
    
    // MARK: - ****** UIImagePicker Delegate Method. ******
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            let pickedImage : UIImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            self.selectedImageView.isHidden = false
            self.selectedImage.image = pickedImage//self.resizeImage(image: pickedImage, targetSize: CGSize(width: self.selectedImage.frame.size.width, height: self.selectedImage.frame.size.height))
            self.selectedImage.contentMode = UIViewContentMode.scaleAspectFit
            self.dismiss(animated: true, completion: nil)
    }
  
    
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    

    
    // MARK: - ****** addAudio_btnAction. ******
    @IBAction func addAudio_btnAction(_ sender: Any) {
    }
    
    
    // MARK: - ****** UITableView Delegate and DataSource Methods. ******
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.items.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.messages_tableView.separatorColor = UIColor.clear
        /*var ageStr = ""
        var genderAgeStr = ""
        var genderStr = "" */
      
        switch self.items[indexPath.row].owner {
            
        case .receiver:
            
            if self.items[indexPath.row].type == .postedConfession
            {
                if self.items[indexPath.row].postedConfessionCheck == "textWithImage"
                {
                   let messagesCell = tableView.dequeueReusableCell(withIdentifier: "ReceiverPostStatusCell", for: indexPath) as! MessagesTableViewCell
                    
                    if (self.items.count <= 0)
                    {
                        return messagesCell
                    }
                    
                    messagesCell.clearCellData()
                    messagesCell.userProfileImage.layer.cornerRadius = messagesCell.userProfileImage.frame.size.height/2
                    messagesCell.userProfileImage.layer.masksToBounds = true
                    messagesCell.userProfileImage.clipsToBounds = true
                    messagesCell.userProfileImage.contentMode = UIViewContentMode.scaleAspectFill
                    
                    messagesCell.selectionStyle = UITableViewCellSelectionStyle.none
                    messagesCell.message_txtView.font = UIFont.systemFont(ofSize: 16)
                    
                    /*  let inputString: NSString = self.items[indexPath.row].content as! NSString
                     let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                     let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                     cell.senderMsg_txtView.dataDetectorTypes = UIDataDetectorTypes.link
                     cell.senderMsg_txtView.text = modifiedString*/
                    
                    messagesCell.message_txtView.text = self.items[indexPath.row].postedText
                    
                    messagesCell.confessionReceiverImage.isHidden = false
                    messagesCell.receiverViewImage_btn.isHidden = true
                    
                    if let image = self.items[indexPath.row].image {
                        messagesCell.confessionReceiverImage.image = image
                        
                        messagesCell.confessionReceiverImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "Default Icon"))
                    } else {
                        
                        DispatchQueue.main.async {
                            messagesCell.confessionReceiverImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "loading"))
                        }
                        
                    }
                    
                    messagesCell.confessionReceiverImage.contentMode = UIViewContentMode.scaleAspectFill
                    
                    messagesCell.confessionReceiverImage.clipsToBounds = true
                    
                    let timeStampMessage = String(self.items[indexPath.row].timestamp)
                    //Convert to Date
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    
                    
                    if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                    {
                        if (self.myProfileStatus == "profileRevealed") 
                        {
                            let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth  = true
                            
                            for subview in messagesCell.userProfileImage.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            messagesCell.userProfileImage.image = nil
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                        }
                        else
                        {
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                            messagesCell.userProfileImage.image = nil
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = messagesCell.userProfileImage.bounds
                            messagesCell.userProfileImage.addSubview(blurEffectView)
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth  = true
                        }
                    }
                    else
                    {
                        if (self.myProfileStatus == "profileRevealed")
                        {
                            let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            
                            
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth  = true
                            
                            for subview in messagesCell.userProfileImage.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            messagesCell.userProfileImage.image = nil
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                        }
                        else
                        {
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                            messagesCell.userProfileImage.image = nil
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                               messagesCell.userProfileImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = messagesCell.userProfileImage.bounds
                            messagesCell.userProfileImage.addSubview(blurEffectView)
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth  = true
                        }
                    }
                    messagesCell.isRead_lbl.isHidden = true
                    return messagesCell
                }
                else
                {
                    let messagesCell = tableView.dequeueReusableCell(withIdentifier: "messagesCell", for: indexPath) as! MessagesTableViewCell
                    
                    if (self.items.count <= 0)
                    {
                        return messagesCell
                    }
                    
                    messagesCell.clearCellData()
                    
                    messagesCell.message_txtView.font = UIFont.systemFont(ofSize: 16)
                    
                    /*  let inputString: NSString = self.items[indexPath.row].content as! NSString
                     let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                     let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                     cell.senderMsg_txtView.dataDetectorTypes = UIDataDetectorTypes.link
                     cell.senderMsg_txtView.text = modifiedString*/
                    
                    messagesCell.message_txtView.text = self.items[indexPath.row].postedText!
                    messagesCell.message_txtView.textAlignment = .left
                    
                    messagesCell.receiverBackImage.isHidden = true
                    messagesCell.receiverViewImage_btn.isHidden = true
//                    messagesCell.receiverSlider.isHidden = true
//                    messagesCell.receiverAudioImage.isHidden = true
                    
                    let timeStampMessage = String(self.items[indexPath.row].timestamp)
                    //Convert to Date
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    
                    
                    if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                    {
                        if (self.myProfileStatus == "profileRevealed")
                        {
                            let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            
                            
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth  = true
                            
                            for subview in messagesCell.userProfileImage.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            messagesCell.userProfileImage.image = nil
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                        }
                        else
                        {
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                            messagesCell.userProfileImage.image = nil
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = messagesCell.userProfileImage.bounds
                            messagesCell.userProfileImage.addSubview(blurEffectView)
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth  = true
                        }
                    }
                    else
                    {
                        if (self.myProfileStatus == "profileRevealed")
                        {
                            let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth  = true
                            
                            for subview in messagesCell.userProfileImage.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            messagesCell.userProfileImage.image = nil
                            DispatchQueue.main.async {
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                        }
                        else
                        {
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                            messagesCell.userProfileImage.image = nil
                            messagesCell.userProfileImage.backgroundColor = UIColor.clear
                            DispatchQueue.main.async {
                               messagesCell.userProfileImage.sd_setImage(with: URL(string: self.myProfilePicLink), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = messagesCell.userProfileImage.bounds
                            messagesCell.userProfileImage.addSubview(blurEffectView)
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            messagesCell.userName_lbl.attributedText = attributedStr
                            messagesCell.userName_lbl.adjustsFontSizeToFitWidth  = true
                        }
                    }
                    messagesCell.isRead_lbl.isHidden = true
                    return messagesCell
                }
            }
            else
            {
                let messagesCell = tableView.dequeueReusableCell(withIdentifier: "messagesCell", for: indexPath) as! MessagesTableViewCell
                
                if (self.items.count <= 0)
                {
                    return messagesCell
                }
                
                messagesCell.clearCellData()
                messagesCell.selectionStyle = UITableViewCellSelectionStyle.none
                
                messagesCell.usersProfileViewBtn.tag = indexPath.row
                messagesCell.usersProfileViewBtn.addTarget(self, action: #selector(MessagesViewController.ViewOpponentProfileBtn(_:)), for: .touchUpInside)
                
                if (messagesCell.userProfileImage.frame.size.width) < (messagesCell.userProfileImage.frame.size.height)
                {
                    
                }
                else if (messagesCell.userProfileImage.frame.size.height) < (messagesCell.userProfileImage.frame.size.width)
                {
                    
                }
                
                messagesCell.userProfileImage.layer.cornerRadius = messagesCell.userProfileImage.frame.size.height/2
                messagesCell.userProfileImage.layer.masksToBounds = true
                messagesCell.userProfileImage.clipsToBounds = true
                messagesCell.userProfileImage.contentMode = UIViewContentMode.scaleAspectFill
          
                
                switch self.items[indexPath.row].type {
                    
                case .postedConfession:
                    print("postedConfession by receiver")
                    
                case .text:
                    
                    messagesCell.message_txtView.font = UIFont.systemFont(ofSize: 16)
                    
                    /*  let inputString: NSString = self.items[indexPath.row].content as! NSString
                     let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                     let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                     cell.senderMsg_txtView.dataDetectorTypes = UIDataDetectorTypes.link
                     cell.senderMsg_txtView.text = modifiedString*/
                    
                    messagesCell.message_txtView.text = self.items[indexPath.row].content as! String
                    messagesCell.message_txtView.textAlignment = .left
                    
                    messagesCell.receiverBackImage.isHidden = true
                    messagesCell.receiverViewImage_btn.isHidden = true

                    
                    let timeStampMessage = String(self.items[indexPath.row].timestamp)
                    //Convert to Date
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    
                    if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
                    {
                            if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                            {
                                messagesCell.userProfileImage.image = nil
                                
                                DispatchQueue.main.async {
//                                    messagesCell.userProfileImage.image = self.opponentProfileImage.image;
                                    if (self.opponentImageStrFromAppDel == "")
                                    {
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                      
                                    }
                                    else{
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: ""))
                                    }
                                }
                                
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                
                                let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                            else
                            {
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                messagesCell.userProfileImage.image = nil
//                                messagesCell.userProfileImage.image = UIImage(named: "moodIcon-1")
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                
                                var attributedStr = NSMutableAttributedString()
                                attributedStr = NSMutableAttributedString(string: "VIP Member" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:10))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                    }
                    else
                    {
                        if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                        {
                            if (self.opponentProfileStatus == "profileRevealed")
                            {
                                messagesCell.userProfileImage.image = nil
                                
                                for subview in messagesCell.userProfileImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                DispatchQueue.main.async {
//                                    messagesCell.userProfileImage.image = self.opponentProfileImage.image;
                                    if (self.opponentImageStrFromAppDel == "")
                                    {
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                        
                                        /*self.opponentProfileImage.sd_setImage(with: URL(string: self.items[indexPath.row].profilePicture!), placeholderImage: UIImage(named: "moodIcon-1"))
                                        self.opponentProfilePic = self.opponentProfileImage.image!*/
                                    }
                                    else{
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                }
                                
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                
                                let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                            else
                            {
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                messagesCell.userProfileImage.image = nil
                                DispatchQueue.main.async {
                                    messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = messagesCell.userProfileImage.bounds
                                messagesCell.userProfileImage.addSubview(blurEffectView)
                                
                                var attributedStr = NSMutableAttributedString()
                                attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                        }
                        else
                        {
                            if (self.opponentProfileStatus == "profileRevealed")
                            {
                                messagesCell.userProfileImage.image = nil
                                for subview in messagesCell.userProfileImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                DispatchQueue.main.async {
//                                    messagesCell.userProfileImage.image = self.opponentProfileImage.image;
                                    if (self.opponentImageStrFromAppDel == "")
                                    {
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                        
                                       /* self.opponentProfileImage.sd_setImage(with: URL(string: self.items[indexPath.row].profilePicture!), placeholderImage: UIImage(named: "moodIcon-1"))
                                        
                                        self.opponentProfilePic = self.opponentProfileImage.image!*/
                                    }
                                    else{
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                }
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                
                                let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                            else
                            {
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                messagesCell.userProfileImage.image = nil
                                DispatchQueue.main.async {
                                    messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = messagesCell.userProfileImage.bounds
                                messagesCell.userProfileImage.addSubview(blurEffectView)
                                
                                var attributedStr = NSMutableAttributedString()
                                attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                        }
                    }
                    
                    messagesCell.isRead_lbl.isHidden = true
                    
                case .photo:
                    
                    messagesCell.message_txtView.font = UIFont.systemFont(ofSize: 16)
                    messagesCell.receiverBackImage.isHidden = false
                    messagesCell.receiverViewImage_btn.isHidden = true

                    if (UIScreen.main.bounds.height <= 568)
                    {
                        messagesCell.message_txtView.text = "ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
                    }
                    else
                    {
                        messagesCell.message_txtView.text = "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
                    }
                    
                    if let image = self.items[indexPath.row].image {
                        messagesCell.receiverBackImage.image = image
                        messagesCell.message_txtView.isHidden = true
                    }
                    else {
                        messagesCell.message_txtView.isHidden = true
                        DispatchQueue.main.async {
                             messagesCell.receiverBackImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "loading"))
                        }
                    }
                    
                    messagesCell.receiverBackImage.contentMode  = UIViewContentMode.scaleAspectFill
                    messagesCell.receiverBackImage.clipsToBounds = true
                    
                    let timeStampMessage = String(self.items[indexPath.row].timestamp)
                    //Convert to Date
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    
                    if (self.checkVisibiltyStr == "FromViewProfile") || (self.checkVisibiltyStr == "withProfileChat")
                    {
                            if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                            {
                                messagesCell.userProfileImage.image = nil
                                
                                DispatchQueue.main.async {
                                    if (self.opponentImageStrFromAppDel == "")
                                    {
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                    }
                                    else{
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: ""))
                                    }
                                }
                                
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                
                                let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                            else
                            {
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                messagesCell.userProfileImage.image = nil
                                messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                
                                var attributedStr = NSMutableAttributedString()
                                attributedStr = NSMutableAttributedString(string: "VIP Member" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:10))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                    }
                    else
                    {
                        if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                        {
                            if (self.opponentProfileStatus == "profileRevealed")
                            {
                                messagesCell.userProfileImage.image = nil
                                
                                for subview in messagesCell.userProfileImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    if (self.opponentImageStrFromAppDel == "")
                                    {
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                    }
                                    else{
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                }
                                
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                
                                let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                            else
                            {
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                messagesCell.userProfileImage.image = nil
                                DispatchQueue.main.async {
                                    messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = messagesCell.userProfileImage.bounds
                                messagesCell.userProfileImage.addSubview(blurEffectView)
                                
                                var attributedStr = NSMutableAttributedString()
                                attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                        }
                        else
                        {
                            if (self.opponentProfileStatus == "profileRevealed")
                            {
                                messagesCell.userProfileImage.image = nil
                                
                                for subview in messagesCell.userProfileImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                DispatchQueue.main.async {
//                                    messagesCell.userProfileImage.image = self.opponentProfileImage.image;
                                    if (self.opponentImageStrFromAppDel == "")
                                    {
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "User Deleted Default Icon"))
                                        
                                      /*  self.opponentProfileImage.sd_setImage(with: URL(string: self.items[indexPath.row].profilePicture!), placeholderImage: UIImage(named: "moodIcon-1"))
                                        
                                        self.opponentProfilePic = self.opponentProfileImage.image!*/
                                    }
                                    else{
                                        messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                }
                                
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                
                                let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                            else
                            {
                                messagesCell.userProfileImage.backgroundColor = UIColor.clear
                                messagesCell.userProfileImage.image = nil
                                DispatchQueue.main.async {
                                    messagesCell.userProfileImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = messagesCell.userProfileImage.bounds
                                messagesCell.userProfileImage.addSubview(blurEffectView)
                                
                                var attributedStr = NSMutableAttributedString()
                                attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                messagesCell.userName_lbl.attributedText = attributedStr
                                messagesCell.userName_lbl.adjustsFontSizeToFitWidth = true
                                messagesCell.usersProfileViewBtn.isUserInteractionEnabled = true
                            }
                        }
                    }
                    
                    messagesCell.isRead_lbl.isHidden = true
                    
                case .audio:
                    print("audio case in messages")
              
                }
                return messagesCell
            }
            
            
            
        case .sender:
           
            if self.items[indexPath.row].type == .postedConfession
            {
                if self.items[indexPath.row].postedConfessionCheck == "textWithImage"
                {
                    let cell =  tableView.dequeueReusableCell(withIdentifier: "SenderPostStatusCell", for: indexPath) as! SenderCellTableViewCell
                    
                    if (self.items.count <= 0)
                    {
                        return cell
                    }
                    
                    print("postedConfession by sender")
                    
                    cell.senderMsg_txtView.font = UIFont.systemFont(ofSize: 16)
                    
                  //  cell.clearCellData()
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    
                    cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    cell.senderImage.layer.masksToBounds = true
                    cell.senderImage.clipsToBounds = true
                    cell.senderImage.contentMode = UIViewContentMode.scaleAspectFill
                    
                    cell.senderImage.layer.masksToBounds = true
                    cell.senderImage.clipsToBounds = true
                    cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    cell.senderIsRead_lbl.isHidden = true
                    
                    cell.senderMsg_txtView.isHidden = false
                    cell.confessionImage.isHidden = false
                    cell.senderViewImage_btn.isHidden = true
                    
                    if (self.items[indexPath.row].content as? String != "") {
                        
                        DispatchQueue.main.async {
                             cell.confessionImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "Default Icon"))
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            cell.confessionImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "loading"))
                        }
                    }
                    
                    cell.confessionImage.contentMode = UIViewContentMode.scaleAspectFill
                    cell.confessionImage.clipsToBounds = true
                    
                    cell.senderMsg_txtView.font = UIFont.systemFont(ofSize: 16)
                    
                    /*  let inputString: NSString = self.items[indexPath.row].content as! NSString
                     let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                     let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                     cell.senderMsg_txtView.dataDetectorTypes = UIDataDetectorTypes.link
                     cell.senderMsg_txtView.text = modifiedString*/
                    
                    cell.senderMsg_txtView.text = self.items[indexPath.row].postedText!
                    cell.senderMsg_txtView.textAlignment = .right
                    
                    let isReadMessage = self.items[indexPath.row].isRead
                    if isReadMessage == true{
                        cell.senderIsRead_lbl.text = "Sent"
                        cell.senderIsRead_lbl.isHidden = false
                    }
                    else{
                        cell.senderIsRead_lbl.isHidden = true
                        cell.senderIsRead_lbl.text = ""
                    }
                    
                    
                    let timeStampMessage = String(self.items[indexPath.row].timestamp)
                    //Convert to Date
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
              
                    if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                    {
                        if (self.opponentProfileStatus == "profileRevealed")
                        {
                            
                            for subview in cell.senderImage.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
//                                cell.senderImage.image = self.opponentProfileImage.image;
                                cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            cell.senderImage.backgroundColor = UIColor.clear
                            
                            let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                        else
                        {
                            cell.senderImage.backgroundColor = UIColor.clear
                            cell.senderImage.image = nil
                            
                            
                            DispatchQueue.main.async {
                                cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.senderImage.bounds
                            cell.senderImage.addSubview(blurEffectView)
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                    }
                    else
                    {
                        if (self.opponentProfileStatus == "profileRevealed")
                        {
                            cell.senderImage.image = nil
                            for subview in cell.senderImage.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            DispatchQueue.main.async {
//                                cell.senderImage.image = self.opponentProfileImage.image;
                                cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            cell.senderImage.backgroundColor = UIColor.clear
                            
                            let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                        else
                        {
                            cell.senderImage.backgroundColor = UIColor.clear
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
                                cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.senderImage.bounds
                            cell.senderImage.addSubview(blurEffectView)
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                    }
                   return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath) as! SenderCellTableViewCell
                    
                    if (self.items.count <= 0)
                    {
                        return cell
                    }
                    
                    cell.clearCellData()
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    
                    cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    cell.senderImage.layer.masksToBounds = true
                    cell.senderImage.clipsToBounds = true
                    cell.senderImage.contentMode = UIViewContentMode.scaleAspectFill
                    
                    cell.senderImage.layer.masksToBounds = true
                    cell.senderImage.clipsToBounds = true
                    cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    cell.senderIsRead_lbl.isHidden = true
                    
                    cell.senderMsg_txtView.font = UIFont.systemFont(ofSize: 16)
                    
                    /*  let inputString: NSString = self.items[indexPath.row].content as! NSString
                     let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                     let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                     cell.senderMsg_txtView.dataDetectorTypes = UIDataDetectorTypes.link
                     cell.senderMsg_txtView.text = modifiedString*/
                    
                    cell.senderMsg_txtView.text = self.items[indexPath.row].postedText
                    cell.senderMsg_txtView.textAlignment = .right
                    cell.senderBackImage.isHidden = true
                    cell.senderViewImage_btn.isHidden = true
//                    cell.senderAudioCell.isHidden = true
//                    cell.audioImage.isHidden = true
                    
                    let isReadMessage = self.items[indexPath.row].isRead
                    if isReadMessage == true
                    {
                        cell.senderIsRead_lbl.text = "Sent"
                        cell.senderIsRead_lbl.isHidden = false
                    }
                    else
                    {
                        cell.senderIsRead_lbl.isHidden = true
                        cell.senderIsRead_lbl.text = ""
                    }
                    
                    let timeStampMessage = String(self.items[indexPath.row].timestamp)
                    //Convert to Date
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    
                    if (self.opponentSettingProfileInvisibilityStr == "on") || (self.opponentSettingProfileInvisibilityStr == "On")
                    {
                        if (self.opponentProfileStatus == "profileRevealed")
                        {
                            cell.senderImage.image = nil
                            
                            for subview in cell.senderImage.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            DispatchQueue.main.async {
//                                cell.senderImage.image = self.opponentProfileImage.image;
                                cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            cell.senderImage.backgroundColor = UIColor.clear
                            
                            let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                        else
                        {
                            cell.senderImage.backgroundColor = UIColor.clear
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
                                cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.senderImage.bounds
                            cell.senderImage.addSubview(blurEffectView)
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                    }
                    else
                    {
                        if (self.opponentProfileStatus == "profileRevealed")                         {
                            cell.senderImage.image = nil
                            
                            for subview in cell.senderImage.subviews
                            {
                                if subview.isKind(of: UIVisualEffectView.self)
                                {
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            DispatchQueue.main.async {
//                                cell.senderImage.image = self.opponentProfileImage.image;
                                cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            cell.senderImage.backgroundColor = UIColor.clear
                            
                            let attributedStr = NSMutableAttributedString(string: self.opponentName + " " + "-" + " ")
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                        else
                        {
                            cell.senderImage.backgroundColor = UIColor.clear
                            cell.senderImage.image = nil
                            DispatchQueue.main.async {
                                cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                            }
                            
                            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                            blurEffectView.frame = cell.senderImage.bounds
                            cell.senderImage.addSubview(blurEffectView)
                            
                            var attributedStr = NSMutableAttributedString()
                            attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                            attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                            
                            let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                            let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                            attributedStr.append(normalString)
                            cell.senderName_lbl.attributedText = attributedStr
                            cell.senderName_lbl.adjustsFontSizeToFitWidth = true
                        }
                    }
                    return cell
                }
            }
            else
            {
                var cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath) as! SenderCellTableViewCell
                
                if (self.items.count <= 0)
                {
                    return cell
                }
                
                switch self.items[indexPath.row].type {
                    
                case .postedConfession:
                    print("postedConfession")
                    
                case .text:
                    
                    cell.clearCellData()
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    
                    cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    cell.senderImage.layer.masksToBounds = true
                    cell.senderImage.clipsToBounds = true
                    cell.senderImage.contentMode = UIViewContentMode.scaleAspectFill
                    
                    cell.senderImage.layer.masksToBounds = true
                    cell.senderImage.clipsToBounds = true
                    cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    cell.senderIsRead_lbl.isHidden = true
                    
                    cell.senderMsg_txtView.font = UIFont.systemFont(ofSize: 16)
                    
                  /*  let inputString: NSString = self.items[indexPath.row].content as! NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    cell.senderMsg_txtView.dataDetectorTypes = UIDataDetectorTypes.link
                    cell.senderMsg_txtView.text = modifiedString*/
                    
                    cell.senderMsg_txtView.text = self.items[indexPath.row].content as! String

                    cell.senderMsg_txtView.textAlignment = .right
                    cell.senderBackImage.isHidden = true
                    cell.senderViewImage_btn.isHidden = true
//                    cell.senderAudioCell.isHidden = true
//                    cell.audioImage.isHidden = true
                    
                    let isReadMessage = self.items[indexPath.row].isRead
                    if isReadMessage == true
                    {
                        cell.senderIsRead_lbl.text = "Sent"
                        cell.senderIsRead_lbl.isHidden = false
                    }
                    else
                    {
                        cell.senderIsRead_lbl.isHidden = true
                        cell.senderIsRead_lbl.text = ""
                    }
                    
                    let timeStampMessage = String(self.items[indexPath.row].timestamp)
                    //Convert to Date
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    
               
                    if (checkVisibiltyStr == "FromViewProfile") || (checkVisibiltyStr == "withProfileChat")
                    {
                            if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                            {
                                let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                                
                                DispatchQueue.main.async {
                                    
                                    if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                                    {
                                        cell.senderImage.sd_setImage(with: URL(string: userProfilePicture), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                    else{
                                        cell.senderImage.image = UIImage(named: "Default Icon")
                                    }
                                }
                                
                                cell.senderImage.backgroundColor = UIColor.clear
                            }
                            else
                            {
                                cell.senderImage.backgroundColor = UIColor.clear
                                cell.senderImage.image = nil
                                cell.senderImage.backgroundColor = UIColor.clear
                                DispatchQueue.main.async {
                                    cell.senderImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                }
                                
                                var attributedStr = NSMutableAttributedString()
                                attributedStr = NSMutableAttributedString(string: "VIP Member" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:10))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                            }
                    }
                    else
                    {
                        if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                        {
                            if (self.myProfileStatus == "profileRevealed")
                            {
                                let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                                
                                for subview in cell.senderImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    
                                    if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                                    {
                                        cell.senderImage.sd_setImage(with: URL(string: userProfilePicture), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                    else{
                                        cell.senderImage.image = UIImage(named: "Default Icon")
                                    }
                                }
                                
                                cell.senderImage.backgroundColor = UIColor.clear
                            }
                            else
                            {
                                cell.senderImage.backgroundColor = UIColor.clear
                                cell.senderImage.image = nil
                                cell.senderImage.backgroundColor = UIColor.clear
                                DispatchQueue.main.async {
                                    cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = cell.senderImage.bounds
                                cell.senderImage.addSubview(blurEffectView)
                                
                                var attributedStr = NSMutableAttributedString()
                           
                                attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                         
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                            }
                        }
                        else
                        {
                            if (self.myProfileStatus == "profileRevealed")
                            {
                                let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                                
                                for subview in cell.senderImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    
                                    if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                                    {
                                        cell.senderImage.sd_setImage(with: URL(string: userProfilePicture), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                    else{
                                        cell.senderImage.image = UIImage(named: "Default Icon")
                                    }
                                }
                                
                                cell.senderImage.backgroundColor = UIColor.clear
                            }
                            else
                            {
                                cell.senderImage.backgroundColor = UIColor.clear
                                cell.senderImage.image = nil
                                cell.senderImage.backgroundColor = UIColor.clear
                                DispatchQueue.main.async {
                                    cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = cell.senderImage.bounds
                                cell.senderImage.addSubview(blurEffectView)
                                
                                var attributedStr = NSMutableAttributedString()
                                
                                attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                            }
                        }
                    }

                    
                case .photo:
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath) as! SenderCellTableViewCell
                    
                    cell.clearCellData()
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    
                    cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    cell.senderImage.layer.masksToBounds = true
                    cell.senderImage.clipsToBounds = true
                    cell.senderImage.contentMode = UIViewContentMode.scaleAspectFill
                    
                    cell.senderImage.layer.masksToBounds = true
                    cell.senderImage.clipsToBounds = true
                    cell.senderImage.layer.cornerRadius =  cell.senderImage.frame.size.height/2
                    cell.senderIsRead_lbl.isHidden = true
                    
                    cell.senderBackImage.isHidden = false
                    cell.senderViewImage_btn.isHidden = true
                    
                    if (UIScreen.main.bounds.height <= 568)
                    {
                        cell.senderMsg_txtView.text = "ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
                    }
                    else
                    {
                        cell.senderMsg_txtView.text = "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
                    }
                    
                    if let image = self.items[indexPath.row].image {
                        cell.senderBackImage.image = image
                        cell.senderMsg_txtView.isHidden = true
                    } else {
                        cell.senderMsg_txtView.isHidden = true
                        DispatchQueue.main.async {
                            cell.senderBackImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "loading"))
                        }
                    }
                    cell.senderBackImage.contentMode = UIViewContentMode.scaleAspectFill
                    cell.senderBackImage.clipsToBounds = true
                    
                    let isReadMessage = self.items[indexPath.row].isRead
                    if isReadMessage == true{
                        cell.senderIsRead_lbl.text = "Sent"
                        cell.senderIsRead_lbl.isHidden = false
                    }
                    else{
                        cell.senderIsRead_lbl.isHidden = true
                        cell.senderIsRead_lbl.text = ""
                    }
                    
                    let timeStampMessage = String(self.items[indexPath.row].timestamp)
                    //Convert to Date
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampMessage)!)
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                    let dateString = dayTimePeriodFormatter.string(from: date as Date)
                    
                    if (checkVisibiltyStr == "FromViewProfile") || (checkVisibiltyStr == "withProfileChat")
                    {
                            if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                            {
                                let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                                
                                DispatchQueue.main.async {
                                    
                                    if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                                    {
                                        cell.senderImage.sd_setImage(with: URL(string: userProfilePicture), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                    else{
                                        cell.senderImage.image = UIImage(named: "Default Icon")
                                    }
                                }
                                
                                cell.senderImage.backgroundColor = UIColor.clear
                            }
                            else
                            {
                                cell.senderImage.backgroundColor = UIColor.clear
                                cell.senderImage.image = nil
                                cell.senderImage.backgroundColor = UIColor.clear
                                DispatchQueue.main.async {
                                    cell.senderImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "VIP Member"))
                                }
                                
                                var attributedStr = NSMutableAttributedString()
                                attributedStr = NSMutableAttributedString(string: "VIP Member" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:10))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                            }
                    }
                    else
                    {
                        if (self.mySettingProfileInvisibilityStr == "on") || (self.mySettingProfileInvisibilityStr == "On")
                        {
                            if (self.myProfileStatus == "profileRevealed")
                            {
                                let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                                for subview in cell.senderImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    
                                    if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                                    {
                                        cell.senderImage.sd_setImage(with: URL(string: userProfilePicture), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                    else{
                                        cell.senderImage.image = UIImage(named: "Default Icon")
                                    }
                                }
                                
                                cell.senderImage.backgroundColor = UIColor.clear
                            }
                            else
                            {
                                cell.senderImage.backgroundColor = UIColor.clear
                                cell.senderImage.image = nil
                                cell.senderImage.backgroundColor = UIColor.clear
                                DispatchQueue.main.async {
                                    cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = cell.senderImage.bounds
                                cell.senderImage.addSubview(blurEffectView)
                                
                                var attributedStr = NSMutableAttributedString()
                                
                                attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                            }
                        }
                        else
                        {
                            if (self.myProfileStatus == "profileRevealed")
                            {
                                let attributedStr = NSMutableAttributedString(string: (self.usersDetailsDict.value(forKey: "username") as? String)! + " " + "-" + " ")
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                                
                                for subview in cell.senderImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    
                                    if let userProfilePicture = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                                    {
                                        cell.senderImage.sd_setImage(with: URL(string: userProfilePicture), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                    else{
                                        cell.senderImage.image = UIImage(named: "Default Icon")
                                    }
                                }
                                
                                cell.senderImage.backgroundColor = UIColor.clear
                            }
                            else
                            {
                                cell.senderImage.backgroundColor = UIColor.clear
                                cell.senderImage.image = nil
                                cell.senderImage.backgroundColor = UIColor.clear
                                DispatchQueue.main.async {
                                    cell.senderImage.sd_setImage(with: URL(string: self.opponentImageStrFromAppDel), placeholderImage: UIImage(named: "Default Icon"))
                                }
                                
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = cell.senderImage.bounds
                                cell.senderImage.addSubview(blurEffectView)
                                
                                var attributedStr = NSMutableAttributedString()
                                
                                attributedStr = NSMutableAttributedString(string: "Anonymous User" + " " + "-" + " ")
                                attributedStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0), range: NSRange(location:0,length:15))
                                
                                let attrsize = [NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                                let normalString = NSMutableAttributedString(string:dateString, attributes:attrsize)
                                attributedStr.append(normalString)
                                cell.senderName_lbl.attributedText = attributedStr
                                cell.senderName_lbl.adjustsFontSizeToFitWidth  = true
                            }
                        }
                    }
                    
                case .audio:
                    
                    print("audio case in messages")
                    
                }
                return cell
            }
        }
    }
 
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.view.endEditing(true)
        self.writeText_txtView.resignFirstResponder()
        
        switch self.items[indexPath.row].owner {
        case .receiver:
            switch self.items[indexPath.row].type {
            case .postedConfession :
                if self.items[indexPath.row].postedConfessionCheck == "textWithImage"
                {
                    if (self.items[indexPath.row].content as? String != "") { //let photo = self.items[indexPath.row].image {
                        
                        self.particularImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "Default Icon"))
                        
                        //self.particularImage.image = photo
                        self.particularImage.contentMode = UIViewContentMode.scaleAspectFit
                        self.viewImageVIew.isHidden = false
                      /*  self.particularImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.particularImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })*/
                    }
                }
                
            case .photo:
                if (self.items[indexPath.row].content as? String != "") { //let photo = self.items[indexPath.row].image {
                    //self.particularImage.image = photo
                    self.particularImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "Default Icon"))
                    self.particularImage.contentMode = UIViewContentMode.scaleAspectFit
                    self.viewImageVIew.isHidden = false
                   /* self.particularImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.particularImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                      }, completion: { (finished) in
                    })*/
                }
                
            case .audio:
                print("audio case")
              
            default: break
            }
            
        case .sender:
            switch self.items[indexPath.row].type {
                
            case .postedConfession :
                if self.items[indexPath.row].postedConfessionCheck == "textWithImage"
                {
                    if (self.items[indexPath.row].content as? String != "") { //let photo = self.items[indexPath.row].image {
                        //self.particularImage.image = photo
                        
                        self.particularImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "Default Icon"))
                        self.particularImage.contentMode = UIViewContentMode.scaleAspectFit
                        self.viewImageVIew.isHidden = false
                       /* self.particularImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.particularImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })*/
                    }
                }
                
            case .photo:
                if (self.items[indexPath.row].content as? String != "") { //let photo = self.items[indexPath.row].image {
                   // self.particularImage.image = photo
                    
                    self.particularImage.sd_setImage(with: URL(string: self.items[indexPath.row].content as! String), placeholderImage: UIImage(named: "Default Icon"))
                    self.particularImage.contentMode = UIViewContentMode.scaleAspectFit
                    self.viewImageVIew.isHidden = false
                    /*self.particularImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.particularImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                    })*/
                }
            case .audio:
               print("audio case")
                
            default: break
            }
        }
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        switch self.items[indexPath.row].owner
        {
        case .receiver:
             return false
        case .sender:
            
            if self.items[indexPath.row].type == .postedConfession
            {
                return false
            }
            else{
                return true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        
    switch self.items[indexPath.row].owner
    {
        case .receiver: break
        case .sender:
            
            if editingStyle == .delete
            {
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                 
                    if self.typeOfUser == "AnonymousUser"
                    {
                        deleteParticularMessage_AnonymousUser(content: self.items[indexPath.row].content, timestamp: self.items[indexPath.row].timestamp)
                    }
                    else
                    {
                         deleteParticularMessage(content: self.items[indexPath.row].content, timestamp: self.items[indexPath.row].timestamp)
                    }

                    self.items.remove(at: indexPath.row)
                }
            }
        }
    }
    
    
    // MARK: - ****** Delete Particular Message For Anonymous User From Firebase. ******
    func deleteParticularMessage_AnonymousUser(content : Any , timestamp: Double)
    {
//        if let currentUserID = FIRAuth.auth()?.currentUser?.uid {
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String {
            let postIDStr = self.opponentFirebaseUserID + "@" + self.postID
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations_anonymous").child(postIDStr).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    FIRDatabase.database().reference().child("conversations_anonymous").child(location).observe(.value, with: { (snap) in
                        if snap.exists()
                        {
                            let receivedMessage = snap.value as! [String: Any]
                            print("receivedMessage = \(receivedMessage)")
                            for item in snap.children
                            {
                                let receivedMessage = (item as! FIRDataSnapshot).value as! [String: Any]
                                let contentReceived = receivedMessage["content"] as! String
                                let timestampReceived = receivedMessage["timestamp"] as! Double
                                if (content as! String == contentReceived) && (timestamp == timestampReceived)
                                {
                                    FIRDatabase.database().reference().child("conversations_anonymous").child(location).child((item as! FIRDataSnapshot).key).setValue(nil)
                                    self.messages_tableView.reloadData()
                                }
                            }
                        }
                    })
                }
            })
        }
    }

    
    // MARK: - ****** Delete Particular Message From Firebase. ******
    func deleteParticularMessage(content : Any , timestamp: Double)
    {
//        if let currentUserID = FIRAuth.auth()?.currentUser?.uid
        if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
        {
            var opponentIDWithVisibilityStr = String()
            opponentIDWithVisibilityStr = self.opponentFirebaseUserID + "@" +  self.opponentSettingProfileInvisibilityStr + "@" + self.mySettingProfileInvisibilityStr
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("conversations").child(opponentIDWithVisibilityStr).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    FIRDatabase.database().reference().child("conversations").child(location).observe(.value, with: { (snap) in
                        if snap.exists()
                        {
//                            let receivedMessage = snap.value as! [String: Any]
                            for item in snap.children
                            {
                                let receivedMessage = (item as! FIRDataSnapshot).value as! [String: Any]
                                let contentReceived = receivedMessage["content"] as! String
                                let timestampReceived = receivedMessage["timestamp"] as! Double
                                if (content as! String == contentReceived) && (timestamp == timestampReceived)
                                {
                                    FIRDatabase.database().reference().child("conversations").child(location).child((item as! FIRDataSnapshot).key).setValue(nil)
                                    self.messages_tableView.reloadData()
                                }
                            }
                        }
                    })
                }
            })
        }
    }
    
    
    // MARK: - ****** UITextView Delegate. ******
    // MARK: - ****** UITextView Delegate. ******
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let maxCharacter: Int = 200
        return (textView.text?.utf16.count ?? 0) + text.utf16.count - range.length <= maxCharacter
    }
    
   /*  public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
     {
        if(text == "\n")
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }*/
  
    // MARK: - ****** UITouch began Method. ******
     override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
     {
        // Hiding the Keyboard when the User Taps the Background
        if touches.first != nil
        {
            self.writeText_txtView.resignFirstResponder();
        }
       super.touchesBegan(touches, with: event)
    }
    
    func updateContinueChatWithReportedUser()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
           
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.view.isUserInteractionEnabled = false
                    self.loadingView.isHidden = false
                    
                    ApiHandler.updateReportContentToContinueChat(userID: userID, authToken: userToken, content_id: self.opponentUserID, continue_chating: "1", completion: { (responseData) in
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            self.reportedByMe_ContinueChat = "1"
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
            }
        }
    }
    
    // MARK: - ****** View Opponent Users Profile Button. ******
    func ViewOpponentProfileBtn(_ sender: UIButton!) {
        
        let indexPathRow = IndexPath(row: sender.tag, section: 0)
        let messagesCell = self.messages_tableView.cellForRow(at: indexPathRow) as! MessagesTableViewCell
        
        var checkUserNameStr = String()
        checkUserNameStr = messagesCell.userName_lbl.text!
        
        if self.blockedByOpponentStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "You’ve Been Blocked For Being Outta Timin.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.blockedByMeStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.reportedByMeStatus == "yes"
        {
            let alert = UIAlertController(title: "", message: "You’ve reported this user, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            
            
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    print("Not connected")
                    
                case .online(.wwan) , .online(.wiFi):
                  
                    self.updateContinueChatWithReportedUser()
//                    self.viewUsersProfile()
                }
            }))
            
            if (self.reportedByMe_ContinueChat == "0")
            {
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.viewUsersProfile()
            }
        }
        else if self.reportedUsersStr == "userAlreadyReportedMe"
        {
//            let alert = UIAlertController(title: "", message: "User Reported You.", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
        }
        else if (checkUserNameStr.range(of: "VIP Member") != nil)
        {
//            let alert = UIAlertController(title: "", message: "VIP Member", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
        }
        else if (checkUserNameStr.range(of: "Anonymous User") != nil)
        {
//            let alert = UIAlertController(title: "", message: "Anonymous User", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
        }
        else
        {
           self.viewUsersProfile()
        }
    }
    
    
    
    func viewUsersProfile()
    {
        if viewOpponentProfile == "fromParticularUserScreen"
        {
            self.navigationController!.popViewController(animated: true)
        }
        else
        {
            FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).keepSynced(true)
            FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                
                if snapshot.value is NSNull
                {
                    let alert = UIAlertController(title: "", message: "Wi User no longer available", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else
                {
//                    var profileVisibilityStr = String()
                    FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).keepSynced(true)
                    FIRDatabase.database().reference().child("Users").child(self.opponentFirebaseUserID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                        if snapshot.value is NSNull{
//                            profileVisibilityStr = "on"
                        }
                        else
                        {
//                            profileVisibilityStr = snapshot.value as! String
                        }
                        
                        let userDict = snapshot.value as? NSDictionary
                        
                        var auth_token = String()
                        if userDict!["auth_token"] != nil
                        {
                            auth_token = userDict?.value(forKey: "auth_token") as! String
                        }
                        else{
                            auth_token = ""
                        }
                        
                        var profile_visibility = String()
                        if userDict!["profile_visibility"] != nil
                        {
                            profile_visibility = userDict?.value(forKey: "profile_visibility") as! String
                        }
                        else{
                            profile_visibility = "On"
                        }
                        
                        if profile_visibility == "on" || (profile_visibility == "On")
                        {
                            let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
//                            viewProfileScreen.currentUserID = self.opponentUserID
                            viewProfileScreen.ownerID = self.opponentUserID
                            viewProfileScreen.ownerAuthToken = auth_token
                            viewProfileScreen.isKindOfScreen = "MessagesScreen"
                            self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                        }
                        else{
                            print("profile visibility is off ")
                        }
                    })
                }
            })
        }
    }
    
    
    // MARK: - ****** ViewPhotoBtn ******
    func ViewPhotoBtn(_ sender: UIButton!) {
        

    }

    // MARK: - ****** SenderViewPhotoBtn ******
    func SenderViewPhotoBtn(_ sender: UIButton!) {
        

    }

     // MARK: - ****** Close Audio recording view. ******
    @IBAction func closeAudioRecordingView_btnAction(_ sender: Any) {
       
    }
    
     // MARK: - ****** Start Audio Recording Btn. ******
    @IBAction func startAudioRecording_btnAction(_ sender: Any) {
        
    }
    
     // MARK: - ****** Stop Recording Btn. ******
    @IBAction func finishAudio_btnAction(_ sender: Any) {
        
    }
    
    
     // MARK: - ****** Play recorded audio btn action. ******
    @IBAction func playAudio_btnAction(_ sender: Any) {
       
    }
   
    
    
     // MARK: - ****** Upload Audio to Firebase. ******
    @IBAction func uploadAudioToFirebase_btnAction(_ sender: Any) {
       
    }
    
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
