//
//  VideosTableViewCell.swift
//  Wi Match
//
//  Created by brst on 09/03/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class VideosTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var videoSuperLikeCountLbl: UILabel!
    @IBOutlet weak var videoLikeCount_Lbl: UILabel!
    @IBOutlet weak var videoDeleteIcon: UIImageView!
    @IBOutlet weak var videoPlayBtn: UIButton!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoDeleteBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
