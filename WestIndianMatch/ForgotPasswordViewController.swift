//
//  ForgotPasswordViewController.swift
//  WestIndianMatch
//
//  Created by brst on 22/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {

    // MARK: - ****** Outlets ******
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var emailAddress_txtField: UITextField!
    @IBOutlet weak var ImputEmailView: UIView!
    @IBOutlet weak var sendPasswordBtn: UIButton!
    @IBOutlet weak var sendPasswordView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingImage: UIImageView!
    
    // MARK: - ****** ViewDidLoad. ******
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
    }

    
    // MARK: - ****** Back Button Action. ******
    @IBAction func backBtn_NavBar(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    // MARK: - ****** Send Forgot Password Button Action. ******
    @IBAction func sendPassword_btnAction(_ sender: Any) {
        if self.emailAddress_txtField.text == ""
        {
            let alert = UIAlertController(title: "", message: "Please enter your email", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if !isValidEmail(testStr: self.emailAddress_txtField.text!)
        {
            let alert = UIAlertController(title: "", message: "Please enter valid email ID.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.loadingView.isHidden = false
            self.view.isUserInteractionEnabled = false
            
            ApiHandler.ForgotPassword(withEmail: self.emailAddress_txtField.text!, completion: { (responseData) in
                
                if (responseData.value(forKey: "status") as? String == "200")
                {
//                    let dataDict = responseData.value(forKey: "data") as? NSDictionary
                    let message = responseData.value(forKey: "message") as? String
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else if (responseData.value(forKey: "status") as? String == "400")
                {
                    let message = responseData.value(forKey: "message") as? String
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    
                    let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
    }
    
    //MARK:- ****** EMAIL VALIDATION ******
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    // MARK: - ****** UITextField Delegate. ******
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
