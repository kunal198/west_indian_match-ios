//
//  WiMatch_MeetUpViewController.swift
//  Wi Match
//
//  Created by brst on 17/05/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import StoreKit
import AVKit
import AVFoundation

class compareTimestamp
{
    var name: String
    var location: String
    var age: String
    var profilePic: String
    var timestamp:String
    var userIDStr: String
    var profilePicType: String
    var swipeType: String
    var interestedIn: String
    var onlineStatus : String
    var authToken: String
    var firebase_user_id: String
    
    init(name: String, location: String, age: String, pic: String,timestamp: String,u_id: String,picType: String,swipeType: String, interestedInMeetingWith: String, userStatus: String, authToken: String, firebase_user_id: String)
    {
        self.name = name
        self.location = location
        self.age = age
        self.profilePic = pic
        self.timestamp = timestamp
        self.userIDStr = u_id
        self.profilePicType = picType
        self.swipeType = swipeType
        self.interestedIn = interestedInMeetingWith
        self.onlineStatus = userStatus
        self.authToken = authToken
        self.firebase_user_id = firebase_user_id
    }
}

class WiMatch_MeetUpViewController: UIViewController,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,updateOpponentDetails {
    
    //MARK:- ***** VARIABLES DECLARATION *****
    var details = [compareTimestamp]()
    var userID = String()
    var handle: UInt = 0
    var getLastKeyValue = String()
    var draggableBackground: DraggableViewBackground!
    var filterArray = NSArray()
    var getLastKeyValueForpage = String()
    var InterestedInMeetingUpWithStr = String()
    var meetUpUserLiked = NSMutableArray()
    var blockedUsersArray = NSMutableArray()
    var blockedByUserArray = NSMutableArray()
    var usersDetailsDict = NSMutableDictionary()
    var myMeetUpStr = String()
    var linkUpUser_likedMeArray = NSMutableArray()
    var myDeviceTokenStr = String()
    var appDelegatePush = String()
    var typeOfNotification = String()
    var getStatusPackages = String()
    var myIdFromPush = String()
    
    var opponentIDOnMatch = String()
    var opponentFirebaseIDOnMatch = String()
    var opponentIMage = String()
    var opponentName = String()
    var opponentNameOnMatch = String()
    var opponentImageOnMatch = String()
    var opponentImageTypeOnMatch = String()
    
    var meetUpUserArray_Vibes = NSMutableArray()
    var meetUpUserLikedArray = NSMutableArray()
    var meetUpUserArray = NSMutableArray()
    var meetUpUserCopyArray = NSMutableArray()
    var myInterestesdInStr = String()
    var OpponentInterestedInMeetingUpWithStr = String()

    var profileVisitorBool = Bool()
    var relVibesBool = Bool()
     var timer = Timer()
    var linkUpCount = Int()
    var linkUpUserIDArray = [String]()
    var LinkUpCountStr = String()
    var NewMatchesCountStr = String()
    var reportedUsersByMeArray = NSMutableArray()
    var viewProfileBool = Bool()
    
    var vibesCount = Int()
    var vibesUserIDArray = [String]()
    var profileImageURLForVideo = String()
    
    var newLinkUpBadge = Int()
    var newVibesBadge = Int()
    

    var getTypeOfMatchStr = String()
    var blockedIdStr = String()
    
    var matchResultDict = NSDictionary()
    
    var currentPageLinkUp = Int()
    var currentPageVibes = Int()
    var TotalPagesLinkUp = Int()
    var TotalPagesVibes = Int()
    var currentPageForListing_allUsers = Int()
    var totalPagesForSwipeUsers = Int()
    
    var refreshBool = Bool()
    var refreshControl_LinkUp: UIRefreshControl!
    var refreshControl_Vibes: UIRefreshControl!
    
    var lastLocation:CGPoint = CGPoint()
    
    //MARK:- ***** OUTLETS DECLARATION *****
    @IBOutlet weak var keepSwipingBtn: UIButton!
    @IBOutlet weak var sendMessageBtn: UIButton!
    @IBOutlet weak var opponentPictureImage: UIImageView!
    @IBOutlet weak var myPictureImage: UIImageView!
    @IBOutlet weak var matchedUsersName_lbl: UILabel!
    @IBOutlet weak var wiMatchInnerView: UIView!
    @IBOutlet weak var wiMatchView: UIView!
    @IBOutlet weak var buyItsVibesView: UIView!
    @IBOutlet weak var buyItsVibesInnerView: UIView!
    @IBOutlet weak var meetUpHeading: UILabel!
    @IBOutlet weak var vibesUnderline: UILabel!
    @IBOutlet weak var vibesTitle: UILabel!
    @IBOutlet weak var linkUpUnderline: UILabel!
    @IBOutlet weak var linkUpTitle: UILabel!
    @IBOutlet weak var meetUpUnderline: UILabel!
    @IBOutlet weak var linkUp_nodataView: UIView!
    @IBOutlet weak var vibes_noDataView: UIView!
    @IBOutlet weak var meetUpView: UIView!
    @IBOutlet weak var linkUpView: UIView!
    @IBOutlet weak var vibesCountLbl: UILabel!
    @IBOutlet weak var vibesCountView: UIView!
    @IBOutlet weak var linkUpCountLbl: UILabel!
    @IBOutlet weak var linkUpCountView: UIView!
    @IBOutlet weak var wrapperViewCards: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var noDataFoundView: UIView!
    @IBOutlet weak var mainScrollViewSections: UIScrollView!
    @IBOutlet weak var wiMatch_tableView: UITableView!
    @IBOutlet weak var vibesView: UIView!
    @IBOutlet weak var buybtnView: UIView!
    @IBOutlet weak var linkUp_tableView: UITableView!
    @IBOutlet weak var buyProfileVisibilityView: UIView!
    @IBOutlet weak var loadingImage: UIImageView!
    @IBOutlet weak var relVibesView: UIView!
    @IBOutlet weak var refreshLoadingView: UIView!
    @IBOutlet weak var superLikeIcon: UIImageView!
    
     // MARK: - ****** ViewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.opponentNameOnMatch = ""
        self.opponentImageOnMatch = ""
        self.opponentImageTypeOnMatch = ""
        self.blockedIdStr = ""
        self.viewProfileBool = false
        
        let updateDetailsForOpponent = AppDelegate()
        updateDetailsForOpponent.customDelegateUpdate = self
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.matchesCount = 0
        
         UIApplication.shared.isStatusBarHidden=false
         self.linkUpCount = 0
        
        self.profileVisitorBool = false
        self.buyItsVibesView.isHidden = true
        self.buyProfileVisibilityView.isHidden = true
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        self.linkUpCountView.isHidden = true
        self.vibesCountView.isHidden = true
        
        self.linkUpCountView.layer.masksToBounds = false
        self.linkUpCountView.layer.cornerRadius = self.vibesCountView.frame.size.height/2
    
        self.vibesCountView.layer.masksToBounds = false
        self.vibesCountView.layer.cornerRadius = self.vibesCountView.frame.size.height/2
        
        self.wiMatch_tableView.separatorColor = UIColor.clear
        self.linkUp_tableView.separatorColor = UIColor.clear
        
        self.mainScrollViewSections.delegate = self
        mainScrollViewSections.contentSize = CGSize.init(width: self.view.frame.size.width*3, height: mainScrollViewSections.contentSize.height)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.NoUserAlert), name: NSNotification.Name(rawValue: "showAlert"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getPaginationDataMethod), name: NSNotification.Name(rawValue: "getPaginationData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.stopRefreshingLoader), name: NSNotification.Name(rawValue: "stopRefreshingLoader"), object: nil)
        
      NotificationCenter.default.addObserver(self, selector: #selector(self.viewDidDisappear(_:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.likeProfileBtnClicked), name: NSNotification.Name(rawValue: "profileLikeBtn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.FromMeetUpScreen), name: NSNotification.Name(rawValue: "FromMeetUpScreen"), object: nil)
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserProfile), name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
        
        if appDelegatePush == "fromHomePage"
        {
            if self.typeOfNotification == "Link Up"
            {
                self.wiMatchView.isHidden = true
                
                var frame: CGPoint = self.mainScrollViewSections.contentOffset
                frame.x = self.mainScrollViewSections.frame.size.width * CGFloat(1);
                self.mainScrollViewSections.setContentOffset(frame, animated: true)
                
                self.linkUpTitle.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.linkUpUnderline.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.vibesUnderline.backgroundColor = UIColor.lightGray
                self.vibesTitle.textColor = UIColor.lightGray
                self.meetUpHeading.textColor = UIColor.lightGray
                self.meetUpUnderline.backgroundColor = UIColor.lightGray
                UserDefaults.standard.set("linkupuserViewedOnce", forKey: "LinkUpUsers")
                UserDefaults.standard.synchronize()
                self.linkUpCountView.isHidden = true
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    self.updateBadgeValueForVisitors(typeOfBadge: "linkup_badgecount", completion: { (result) in
                        
                    })
                }
            }
            else
            {
                var frame: CGPoint = self.mainScrollViewSections.contentOffset
                frame.x = self.mainScrollViewSections.frame.size.width * CGFloat(2);
                
                self.vibesTitle.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.vibesUnderline.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                self.linkUpUnderline.backgroundColor = UIColor.lightGray
                self.linkUpTitle.textColor = UIColor.lightGray
                self.meetUpHeading.textColor = UIColor.lightGray
                self.meetUpUnderline.backgroundColor = UIColor.lightGray
                UserDefaults.standard.set("userViewedOnce", forKey: "VibesUsers")
                UserDefaults.standard.synchronize()
                self.vibesCountView.isHidden = true
                
                self.mainScrollViewSections.setContentOffset(frame, animated: true)
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    print("Not connected")
                case .online(.wwan) , .online(.wiFi):
                    
                    self.updateBadgeValueForVisitors(typeOfBadge: "vibes_badgecount", completion: { (result) in
                        
                    })
                }
            }
        }
        
        if (self.typeOfNotification != "New Matches")
        {
            if self.appDelegatePush == "fromHomePage"
            {
                if (self.typeOfNotification != "Link Up")
                {
                    self.wiMatchView.isHidden = true
                    self.refreshLoadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    self.showAlertForRelVibesFromAppDelegate(matchResultDict: self.matchResultDict)
                }
                else{
                    self.wiMatchView.isHidden = true
                    self.opponentIDOnMatch = ""
                    self.opponentNameOnMatch = ""
                    self.opponentImageOnMatch = ""
                    self.opponentImageTypeOnMatch = ""
                }
            }
            else
            {
                self.wiMatchView.isHidden = true
                self.opponentIDOnMatch = ""
                self.opponentNameOnMatch = ""
                self.opponentImageOnMatch = ""
                self.opponentImageTypeOnMatch = ""
            }
        }
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
                if (self.draggableBackground == nil)
                {
                    self.draggableBackground = DraggableViewBackground(frame: self.wrapperViewCards.frame)
                    self.meetUpView.addSubview(self.draggableBackground)
                }
            
            self.filterArray = []
            self.currentPageForListing_allUsers = 1
            
            DispatchQueue.main.async {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                self.refreshLoadingView.isHidden = false
            }
         
                self.getUsersListingForMatch(pageCount: String(self.currentPageForListing_allUsers), completion: { (responseBool) in
                    if (responseBool == true)
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendAllUsers"), object: nil, userInfo: ["userInfo" : self.filterArray])
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        self.filterArray = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.noDataFoundView.isHidden = false
                        self.refreshLoadingView.isHidden = true
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlert"), object: nil, userInfo: nil)
                    }
                })
    
            DispatchQueue.global(qos: .userInitiated).sync {
                
                self.meetUpUserArray = []
                self.currentPageLinkUp = 1
                self.getLinkUpUsersListing(pageCountStr: self.currentPageLinkUp,completion: { (responseBool) in
                
                    if (responseBool == true)
                    {
                        self.view.isUserInteractionEnabled = true
//                        self.loadingView.isHidden = true
//                        self.refreshLoadingView.isHidden = true
                        self.linkUp_nodataView.isHidden = true
                        self.linkUp_tableView.reloadData()
                        self.refreshControl_LinkUp.endRefreshing()
                    }
                    else
                    {
//                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
//                        self.refreshLoadingView.isHidden = true
                        self.meetUpUserArray = []
                        self.view.isUserInteractionEnabled = true
                        self.linkUp_tableView.isHidden = false
                        self.linkUp_nodataView.isHidden = false
                        self.linkUp_tableView.reloadData()
                        self.refreshControl_LinkUp.endRefreshing()
                    }
                    
                    
                    self.details = []
                    self.currentPageVibes = 1
                    self.getVibesUsersListing(pageCountStr: self.currentPageVibes, completion: { (responseBool) in
                        
                        if (responseBool == true)
                        {
                            self.view.isUserInteractionEnabled = true
//                            self.loadingView.isHidden = true
//                            self.refreshLoadingView.isHidden = true
                            DispatchQueue.main.async {
                                self.wiMatch_tableView.reloadData()
                                self.vibes_noDataView.isHidden = true
                                self.refreshControl_Vibes.endRefreshing()
                            }
                        }
                        else
                        {
//                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
//                            self.refreshLoadingView.isHidden = true
                            self.details = []
                            self.view.isUserInteractionEnabled = true
                            self.wiMatch_tableView.isHidden = false
                            self.vibes_noDataView.isHidden = false
                            DispatchQueue.main.async {
                                self.wiMatch_tableView.reloadData()
                                self.refreshControl_Vibes.endRefreshing()
                            }
                        }
                    })
                })
            }
        }
        
        if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
        {
            if (checkPackageStatus == "packagePurchased")
            {
                self.profileVisitorBool = false
                
//                if let descriptionPackage = UserDefaults.standard.value(forKey: "checkPurchasedPackageDetails") as?  String
//                {
//
//                }
            }
            else
            {
                self.profileVisitorBool = true
            }
        }
        else
        {
            self.profileVisitorBool = true
        }
    }
    
    func updateUserProfile()
    {
        if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
        {
            if (checkPackageStatus == "packagePurchased")
            {
                self.profileVisitorBool = false
                
//                if let descriptionPackage = UserDefaults.standard.value(forKey: "checkPurchasedPackageDetails") as?  String
//                {
////                    print("descriptionPackage = \(descriptionPackage)")
//                }
            }
            else
            {
                self.profileVisitorBool = true
            }
        }
        else
        {
            self.profileVisitorBool = true
        }
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            self.meetUpUserArray = []
            self.currentPageLinkUp = 1
            self.getLinkUpUsersListing(pageCountStr: self.currentPageLinkUp,completion: { (responseBool) in
                if (responseBool == true)
                {
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    self.refreshLoadingView.isHidden = true
                    self.linkUp_tableView.reloadData()
                    self.refreshControl_LinkUp.endRefreshing()
                    self.linkUp_nodataView.isHidden = true
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.refreshLoadingView.isHidden = true
                    self.meetUpUserArray = []
                    self.view.isUserInteractionEnabled = true
                    self.linkUp_tableView.isHidden = false
                    self.linkUp_nodataView.isHidden = false
                    self.linkUp_tableView.reloadData()
                    self.refreshControl_LinkUp.endRefreshing()
                }
            })
        }
    }
    
    
    func getUsersListingForMatch(pageCount: String ,completion: @escaping (Bool) ->  Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getUsersListingForMatch(userID: userID, authToken: userToken, pageStr: pageCount, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            
                            if (dataDict!["meta"] != nil)
                            {
                                let metaDict = dataDict?.value(forKey: "meta") as! NSDictionary
                                
                                if (metaDict["currentpage"] != nil)
                                {
                                    self.totalPagesForSwipeUsers = Int(metaDict.value(forKey: "totalpages") as! NSNumber)
                                }
                            }
                            
                            if (dataDict!["list"] != nil)
                            {
                                self.filterArray = dataDict?.value(forKey: "list") as! NSArray
                                
                                if (self.filterArray.count > 0)
                                {
                                    for index in 0..<self.filterArray.count
                                    {
                                        
                                        var albumDict = NSDictionary()
                                        albumDict = self.filterArray[index] as! NSDictionary
                                        
                                        if !(self.filterArray.contains(albumDict))
                                        {
                                            self.filterArray.adding(albumDict)
                                        }
                                    }
                                    completion(true)
                                }
                                else
                                {
                                    completion(false)
                                }
                            }
                            else
                            {
                                completion(false)
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                    
                }
            }
        }
    }
    
    func getLinkUpUsersListing(pageCountStr: Int ,completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                     ApiHandler.getLinkUpUsers(userID: userID, authToken: userToken, pageStr: String(pageCountStr), completion: { (responseData) in
                    
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                          
                            if (dataDict!["meta"] != nil)
                            {
                                let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                                self.TotalPagesLinkUp = (metaDict?.value(forKey: "totalpages") as? Int)!
                            }
                            
                            if (dataDict!["list"] != nil)
                            {
                                let linkUpListingArray = dataDict?.value(forKey: "list") as! NSArray
                                if (linkUpListingArray.count > 0)
                                {
                                    for index in 0..<linkUpListingArray.count
                                    {
                                        var albumDict = NSDictionary()
                                        albumDict = linkUpListingArray[index] as! NSDictionary
                                        
                                        if !(self.meetUpUserArray.contains(albumDict))
                                        {
                                            self.meetUpUserArray.add(albumDict)
                                        }
                                    }
                                    
                                    completion(true)
                                }
                                else
                                {
                                    completion(false)
                                }
                            }
                            else
                            {
                                completion(false)
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    func getVibesUsersListing(pageCountStr: Int ,completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    DispatchQueue.main.async {
//                        self.loadingView.isHidden = false
//                        self.view.isUserInteractionEnabled = false
//                        self.refreshLoadingView.isHidden = false
                    }
                    
                    ApiHandler.getRelVibesUsersListing(userID: userID, authToken: userToken, pageStr: String(pageCountStr), completion: { (responseData) in
                        
                        self.details = []
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            
                            if (dataDict!["meta"] != nil)
                            {
                                let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                                self.TotalPagesVibes = (metaDict?.value(forKey: "totalpages") as? Int)!
                            }
                            
                            if (dataDict!["list"] != nil)
                            {
                                let vibesListingArray = dataDict?.value(forKey: "list") as! NSArray
                                if (vibesListingArray.count > 0)
                                {
                                    for index in 0..<vibesListingArray.count
                                    {
                                        var albumDict = NSDictionary()
                                        albumDict = vibesListingArray[index] as! NSDictionary
                                        
                                        let u_id: String = albumDict.value(forKey: "id") as! String
                                        let u_age: String = albumDict.value(forKey: "age") as! String
                                        let u_name: String = albumDict.value(forKey: "username") as! String
                                        let u_location: String = albumDict.value(forKey: "location") as! String
                                        let u_pic: String = albumDict.value(forKey: "profile_pic") as! String
//                                        let u_gender: String = albumDict.value(forKey: "gender") as! String
                                        
                                        let u_interestedIn: String = albumDict.value(forKey: "meet_up_preferences") as! String
                                        
                                        let u_onlineStatus: String = albumDict.value(forKey: "online_status") as! String
                                        let u_Token: String = albumDict.value(forKey: "auth_token") as! String
                                        
                                        let firebase_user_id: String = albumDict.value(forKey: "firebase_user_id") as! String
                                        
                                        var u_swipeType = String()
                                        if (albumDict["swipeType"] != nil)
                                        {
                                            u_swipeType = albumDict.value(forKey: "swipeType") as! String
                                        }
                                        else
                                        {
                                            u_swipeType = ""
                                        }
                                        
                                        var swipe_updated_time = String()
                                        if (albumDict["swipe_updated_time"] != nil)
                                        {
                                            swipe_updated_time = albumDict.value(forKey: "swipe_updated_time") as! String
                                        }
                                        else
                                        {
                                            swipe_updated_time = "0"
                                        }
                                        
                                        
                                        let details = compareTimestamp.init(name: u_name, location: u_location, age: u_age, pic: u_pic, timestamp: swipe_updated_time, u_id: u_id, picType: "image", swipeType: u_swipeType, interestedInMeetingWith: u_interestedIn, userStatus: u_onlineStatus, authToken: u_Token, firebase_user_id: firebase_user_id)
                                        
                                        self.details.append(details)
                                    }
                                    
                                    completion(true)
                                }
                                else
                                {
                                    completion(false)
                                }
                            }
                            else
                            {
                                completion(false)
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                    
                }
            }
        }
    }
    
 
    // MARK: - ****** FromMeetUpScreen ******
    func FromMeetUpScreen()
    {
        self.viewDidLoad()
    }
    
    // MARK: - ****** FromMeetUpScreen ******
    func reloadDataFromUsersProfile(_ anote: Notification)
    {
        let userInfo = (anote as NSNotification).userInfo
        let swipeStatus = (userInfo?["userInfo"] as? String)!
        print("swipeStatus = \(swipeStatus)")
    }
    
    
    // MARK: - ****** refresh page ******
    @IBAction func refreshPage_btnAction(_ sender: Any) {
        self.refreshLoadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
        self.filterArray = []
        self.meetUpUserArray = []
        self.details = []
        self.draggableBackground.allCards.removeAll()
        self.draggableBackground.loadedCards.removeAll()
        self.draggableBackground.getUsersArray = []
        self.draggableBackground.removeFromSuperview()
        self.draggableBackground.removeFromSuperview()
        self.draggableBackground = nil
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.draggableBackground == nil)
            {
                self.draggableBackground = DraggableViewBackground(frame: self.wrapperViewCards.frame)
                self.meetUpView.addSubview(self.draggableBackground)
            }
            
            self.filterArray = []
            self.currentPageForListing_allUsers = 1
            
            DispatchQueue.main.async {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                self.refreshLoadingView.isHidden = false
            }
            
            self.getUsersListingForMatch(pageCount: String(self.currentPageForListing_allUsers), completion: { (responseBool) in
                if (responseBool == true)
                {
//                    self.view.isUserInteractionEnabled = true
//                    self.loadingView.isHidden = true
//                    self.refreshLoadingView.isHidden = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendAllUsers"), object: nil, userInfo: ["userInfo" : self.filterArray])
                }
                else
                {
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    self.filterArray = []
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.noDataFoundView.isHidden = false
                    self.refreshLoadingView.isHidden = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlert"), object: nil, userInfo: nil)
                }
            })
            
            DispatchQueue.global(qos: .userInitiated).sync {
                
                self.meetUpUserArray = []
                self.currentPageLinkUp = 1
                self.getLinkUpUsersListing(pageCountStr: self.currentPageLinkUp,completion: { (responseBool) in
                    
                    if (responseBool == true)
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        self.refreshLoadingView.isHidden = true
                        self.linkUp_nodataView.isHidden = true
                        self.linkUp_tableView.reloadData()
                        self.refreshControl_LinkUp.endRefreshing()
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.refreshLoadingView.isHidden = true
                        self.meetUpUserArray = []
                        self.view.isUserInteractionEnabled = true
                        self.linkUp_tableView.isHidden = false
                        self.linkUp_nodataView.isHidden = false
                        self.linkUp_tableView.reloadData()
                        self.refreshControl_LinkUp.endRefreshing()
                    }
                    
                    self.details = []
                    self.currentPageVibes = 1
                    self.getVibesUsersListing(pageCountStr: self.currentPageVibes, completion: { (responseBool) in
                        
                        if (responseBool == true)
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            DispatchQueue.main.async {
                                self.wiMatch_tableView.reloadData()
                                self.vibes_noDataView.isHidden = true
                                self.refreshControl_Vibes.endRefreshing()
                            }
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.refreshLoadingView.isHidden = true
                            self.details = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.wiMatch_tableView.isHidden = false
                            self.vibes_noDataView.isHidden = false
                            DispatchQueue.main.async {
                                self.wiMatch_tableView.reloadData()
                                self.refreshControl_Vibes.endRefreshing()
                            }
                        }
                    })
                })
            }
        }
    }
   
    
    
    // MARK: - ****** updateDetails ******
    func updateDetails(oppoId: String, oppoName: String, oppoImage: String, oppoImageType: String) {
        self.opponentNameOnMatch = oppoName
        self.opponentIDOnMatch = oppoId
        self.opponentImageOnMatch = oppoImage
        self.opponentImageTypeOnMatch = oppoImageType
    }
    

    
     // MARK: - ****** ViewWillAppear ******
    override func viewWillAppear(_ animated: Bool)
    {
        if (self.newLinkUpBadge <= 0)
        {
            self.linkUpCountLbl.text = ""
            self.linkUpCountView.isHidden = true
        }
        else
        {
            self.linkUpCountLbl.text = String(self.newLinkUpBadge)
            self.linkUpCountView.isHidden = false
        }
        
        if (self.newVibesBadge <= 0)
        {
            self.vibesCountLbl.text = ""
            self.vibesCountView.isHidden = true
        }
        else
        {
            self.vibesCountLbl.text = String(self.newVibesBadge)
            self.vibesCountView.isHidden = false
        }
        
        if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
        {
            if (checkPackageStatus == "packagePurchased")
            {
                self.profileVisitorBool = false
                
//                if let descriptionPackage = UserDefaults.standard.value(forKey: "checkPurchasedPackageDetails") as?  String
//                {
////                    print("descriptionPackage = \(descriptionPackage)")
//                }
            }
            else
            {
                self.profileVisitorBool = true
            }
        }
        else
        {
            self.profileVisitorBool = true
        }
        
        self.buyItsVibesView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.viewUserDetails(_:)), name: NSNotification.Name(rawValue: "viewProfileBtnNotificaton"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.RelVibesAlert), name: NSNotification.Name(rawValue: "relLikesNotPurchased"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showAlertForRelVibes(_:)), name: NSNotification.Name(rawValue: "showAlertForRelVibes"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataFromUsersProfile), name: NSNotification.Name(rawValue: "reloadDataOnWiMatch"), object: nil)
        
        self.refreshControl_Vibes = UIRefreshControl()
        self.refreshControl_Vibes.backgroundColor = UIColor.clear
        self.refreshControl_Vibes.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.refreshControl_Vibes.addTarget(self, action: #selector(refreshVibesData), for: .valueChanged)
        self.wiMatch_tableView.addSubview(self.refreshControl_Vibes)
        self.refreshControl_LinkUp = UIRefreshControl()

        self.refreshControl_LinkUp.backgroundColor = UIColor.clear
        self.refreshControl_LinkUp.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.refreshControl_LinkUp.addTarget(self, action: #selector(refreshLinkUpData), for: .valueChanged)
        self.linkUp_tableView.addSubview(self.refreshControl_LinkUp)
    }
    
    
    func refreshVibesData(_ sender: Any) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.details = []
            self.currentPageVibes = 1
            self.getVibesUsersListing(pageCountStr: self.currentPageVibes, completion: { (responseBool) in
                
                if (responseBool == true)
                {
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    self.refreshLoadingView.isHidden = true
                    DispatchQueue.main.async {
                        self.wiMatch_tableView.reloadData()
                        self.vibes_noDataView.isHidden = true
                        self.refreshControl_Vibes.endRefreshing()
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.refreshLoadingView.isHidden = true
                    self.details = []
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.wiMatch_tableView.isHidden = false
                    self.vibes_noDataView.isHidden = false
                    DispatchQueue.main.async {
                        self.wiMatch_tableView.reloadData()
                        self.refreshControl_Vibes.endRefreshing()
                    }
                }
            })
        }
    }

    
    
    
    func refreshLinkUpData(_ sender: Any) {

        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.meetUpUserArray = []
            self.currentPageLinkUp = 1
            self.getLinkUpUsersListing(pageCountStr: self.currentPageLinkUp,completion: { (responseBool) in
                if (responseBool == true)
                {
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    self.refreshLoadingView.isHidden = true
                    self.linkUp_tableView.reloadData()
                    self.refreshControl_LinkUp.endRefreshing()
                    self.linkUp_nodataView.isHidden = true
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.refreshLoadingView.isHidden = true
                    self.meetUpUserArray = []
                    self.view.isUserInteractionEnabled = true
                    self.linkUp_tableView.isHidden = false
                    self.linkUp_nodataView.isHidden = false
                    self.linkUp_tableView.reloadData()
                    self.refreshControl_LinkUp.endRefreshing()
                }
            })
        }
    }

    
    
     // MARK: - ****** Send Message To Opponent_btnAction ******
    @IBAction func sendMessageToOpponent_btnAction(_ sender: Any) {
        if self.opponentIDOnMatch != ""
        {
            if let currentUserID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
            {
                if currentUserID == self.opponentFirebaseIDOnMatch
                {
                    print("currentUserID and self.opponentFirebaseIDOnMatch are same")
                }
                else
                {
                    let profilePic = UIImageView()
                    profilePic.sd_setImage(with: URL(string: self.opponentIMage), placeholderImage: UIImage(named: "Default Icon"))
                    
                    let messagesScreen = storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                    messagesScreen.mainConfessionCheck = "comingFromViewProfile"
                    messagesScreen.chatUserDifferentiateStr = "Messages"
                    messagesScreen.viewOpponentProfile = "fromParticularUserScreen"
                    messagesScreen.opponentName = self.opponentName
                    messagesScreen.opponentImage = self.opponentIMage
                    messagesScreen.opponentUserID = self.opponentIDOnMatch
                    messagesScreen.opponentFirebaseUserID = self.opponentFirebaseIDOnMatch
                    messagesScreen.opponentProfilePic =  profilePic.image!
                    messagesScreen.typeOfUser = "WithProfileUser"
                    messagesScreen.checkVisibiltyStr = "FromViewProfile"
                    self.navigationController?.pushViewController(messagesScreen, animated: true)
                }
            }
        }
        else
        {
            let alert = UIAlertController(title: "", message: "Something went wrong.Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
     
    }
    
    
     // MARK: - ****** keep Swiping Btn Action ******
    @IBAction func keepSwipingBtnAction(_ sender: Any) {
        self.wiMatchView.isHidden = true
        var frame: CGPoint = self.mainScrollViewSections.contentOffset
        frame.x = self.mainScrollViewSections.frame.size.width * CGFloat(0);
        self.mainScrollViewSections.setContentOffset(frame, animated: true)
        
        meetUpHeading.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        meetUpUnderline.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        vibesUnderline.backgroundColor = UIColor.lightGray
        vibesTitle.textColor = UIColor.lightGray
        linkUpTitle.textColor = UIColor.lightGray
        linkUpUnderline.backgroundColor = UIColor.lightGray
    }
    
    // MARK: - ****** showAlertForRelVibesFromAppDelegate ******
    func showAlertForRelVibesFromAppDelegate(matchResultDict: NSDictionary)
    {
        let dict = matchResultDict //(userInfo?["Status"] as? NSDictionary)!
        self.myPictureImage.layer.borderWidth = 2.0
        self.myPictureImage.layer.masksToBounds = false
        self.myPictureImage.layer.borderColor = UIColor.white.cgColor
        self.myPictureImage.layer.cornerRadius = 13
        self.myPictureImage.layer.cornerRadius = self.myPictureImage.frame.size.height/2
        self.myPictureImage.clipsToBounds = true
        
        self.opponentPictureImage.layer.borderWidth = 2.0
        self.opponentPictureImage.layer.masksToBounds = false
        self.opponentPictureImage.layer.borderColor = UIColor.white.cgColor
        self.opponentPictureImage.layer.cornerRadius = 13
        self.opponentPictureImage.layer.cornerRadius = self.opponentPictureImage.frame.size.height/2
        self.opponentPictureImage.clipsToBounds = true
        self.sendMessageBtn.layer.cornerRadius = self.sendMessageBtn.frame.size.height/2
        self.keepSwipingBtn.layer.borderWidth = 2.0
        self.keepSwipingBtn.layer.masksToBounds = false
        self.keepSwipingBtn.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
        self.keepSwipingBtn.layer.cornerRadius = self.keepSwipingBtn.frame.size.height/2
        
        if (dict != nil)
        {
            let opponent_id: String = dict.value(forKey: "opponent_id") as! String
            
            self.wiMatchView.isHidden = false
            
            let my_id: String = dict.value(forKey: "my_id") as! String
            let opponent_username: String = (dict.value(forKey: "opponent_username") as? String)!
            let my_username: String = (dict.value(forKey: "my_username") as? String)!
            let opponent_image: String = (dict.value(forKey: "opponent_image") as? String)!
            let my_image: String = (dict.value(forKey: "my_image") as? String)!
            let match_type: String = dict.value(forKey: "match_type") as! String
            
            let opponent_firebase_id: String = dict.value(forKey: "opponent_firebase_id") as! String
            self.opponentFirebaseIDOnMatch = opponent_firebase_id
            
            if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if (id == my_id)
                {
                    self.opponentIDOnMatch = opponent_id
                    self.opponentIMage = opponent_image
                    self.opponentName = opponent_username
                    
                    DispatchQueue.main.async {
                        
                        self.myPictureImage.sd_setImage(with: URL(string: my_image), placeholderImage: UIImage(named: "Default Icon"))
                        
                        self.opponentPictureImage.sd_setImage(with: URL(string: opponent_image), placeholderImage: UIImage(named: "Default Icon"))
                    }
                    
                    if (match_type == "4")
                    {
                        self.matchedUsersName_lbl.text = "You and " +  opponent_username + " have rel vibes."
                        self.superLikeIcon.isHidden = true
                    }
                    else if (match_type == "3" && id != opponent_id)
                    {
                        self.matchedUsersName_lbl.text =  opponent_username + " sent you Rel Vibes."
                        self.superLikeIcon.isHidden = false
                    }
                    else
                    {
                        self.matchedUsersName_lbl.text = "You’ve just sent " +  opponent_username + " rel vibes."
                        self.superLikeIcon.isHidden = false
                    }
                }
                else
                {
                    self.opponentIDOnMatch = my_id
                    self.opponentIMage = my_image
                    self.opponentName = my_username
                    
                    DispatchQueue.main.async {
                        
                        self.myPictureImage.sd_setImage(with: URL(string: opponent_image), placeholderImage: UIImage(named: "Default Icon"))
                        
                        self.opponentPictureImage.sd_setImage(with: URL(string: my_image), placeholderImage: UIImage(named: "Default Icon"))
                    }
                    
                    if (match_type == "4")
                    {
                        self.matchedUsersName_lbl.text = "You and " +  my_username + " have rel vibes."
                        self.superLikeIcon.isHidden = true
                    }
                    else if (match_type == "3" && id != opponent_id)
                    {
                        self.matchedUsersName_lbl.text =  opponent_username + " sent you Rel Vibes."
                        self.superLikeIcon.isHidden = false
                    }
                    else
                    {
                        self.matchedUsersName_lbl.text = "You’ve just sent " +  my_username + " rel vibes."
                        self.superLikeIcon.isHidden = false
                    }
                    
                }
            }
            else
            {
                self.wiMatchView.isHidden = true
            }
        }
        else
        {
            self.wiMatchView.isHidden = true
        }
    }
    
    // MARK: - ****** showAlertForRelVibes ******
    func showAlertForRelVibes(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        let dict = (userInfo?["Status"] as? NSDictionary)!
        
        self.myPictureImage.layer.borderWidth = 2.0
        self.myPictureImage.layer.masksToBounds = false
        self.myPictureImage.layer.borderColor = UIColor.white.cgColor
        self.myPictureImage.layer.cornerRadius = 13
        self.myPictureImage.layer.cornerRadius = self.myPictureImage.frame.size.height/2
        self.myPictureImage.clipsToBounds = true
        
        self.opponentPictureImage.layer.borderWidth = 2.0
        self.opponentPictureImage.layer.masksToBounds = false
        self.opponentPictureImage.layer.borderColor = UIColor.white.cgColor
        self.opponentPictureImage.layer.cornerRadius = 13
        self.opponentPictureImage.layer.cornerRadius = self.opponentPictureImage.frame.size.height/2
        self.opponentPictureImage.clipsToBounds = true
        self.sendMessageBtn.layer.cornerRadius = self.sendMessageBtn.frame.size.height/2
        self.keepSwipingBtn.layer.borderWidth = 2.0
        self.keepSwipingBtn.layer.masksToBounds = false
        self.keepSwipingBtn.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
        self.keepSwipingBtn.layer.cornerRadius = self.keepSwipingBtn.frame.size.height/2
        
        if (dict != nil)
        {
            let opponent_id: String = dict.value(forKey: "opponent_id") as! String
            self.wiMatchView.isHidden = false
            let my_id: String = dict.value(forKey: "my_id") as! String
            let opponent_username: String = (dict.value(forKey: "opponent_username") as? String)!
            let my_username: String = (dict.value(forKey: "my_username") as? String)!
            let opponent_image: String = (dict.value(forKey: "opponent_image") as? String)!
            let my_image: String = (dict.value(forKey: "my_image") as? String)!
            let match_type: String = dict.value(forKey: "match_type") as! String
            
            let opponent_firebase_id: String = dict.value(forKey: "opponent_firebase_id") as! String
            self.opponentFirebaseIDOnMatch = opponent_firebase_id
            
            if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if (id == my_id)
                {
                    self.opponentIDOnMatch = opponent_id
                    self.opponentIMage = opponent_image
                    self.opponentName = opponent_username
                    
                  
                    DispatchQueue.main.async {
                        self.myPictureImage.sd_setImage(with: URL(string: my_image), placeholderImage: UIImage(named: "Default Icon"))
                        
                        self.opponentPictureImage.sd_setImage(with: URL(string: opponent_image), placeholderImage: UIImage(named: "Default Icon"))
                    }
                    
                    if (match_type == "4")
                    {
                        self.matchedUsersName_lbl.text = "You and " +  opponent_username + " have rel vibes."
                        self.superLikeIcon.isHidden = true
                    }
                    else if (match_type == "3" && id != opponent_id)
                    {
                        self.matchedUsersName_lbl.text =  opponent_username + " sent you Rel Vibes."
                        self.superLikeIcon.isHidden = false
                    }
                    else
                    {
                       self.matchedUsersName_lbl.text = "You’ve just sent " +  opponent_username + " rel vibes."
                        self.superLikeIcon.isHidden = false
                    }
                }
                else
                {
                    self.opponentIDOnMatch = my_id
                    self.opponentIMage = my_image
                    self.opponentName = my_username
                    
                    DispatchQueue.main.async {
                        self.myPictureImage.sd_setImage(with: URL(string: opponent_image), placeholderImage: UIImage(named: "Default Icon"))
                        
                        self.opponentPictureImage.sd_setImage(with: URL(string: my_image), placeholderImage: UIImage(named: "Default Icon"))
                    }
                    
                    if (match_type == "4")
                    {
                        self.matchedUsersName_lbl.text = "You and " +  my_username + " have rel vibes."
                        self.superLikeIcon.isHidden = true
                    }
                    else if (match_type == "3" && id != opponent_id)
                    {
                        self.matchedUsersName_lbl.text =  opponent_username + " sent you Rel Vibes."
                        self.superLikeIcon.isHidden = false
                    }
                    else
                    {
                        self.matchedUsersName_lbl.text = "You’ve just sent " +  my_username + " rel vibes."
                        self.superLikeIcon.isHidden = false
                    }
                }
            }
            else{
                self.wiMatchView.isHidden = true
            }
        }
        else{
            self.wiMatchView.isHidden = true
        }
    }
    
    
    // MARK: - ****** timerAction ******
    func timerAction()
    {
        self.relVibesView.isHidden = true
    }
  
     // MARK: - ****** ViewDidDisAppear ******
    override func viewDidDisappear(_ animated: Bool)
    {
        self.viewProfileBool = false
        
        if self.timer.isValid == true
        {
            self.timer.invalidate()
        }
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
        case .online(.wwan) , .online(.wiFi):
            UserDefaults.standard.set(nil, forKey: "pushSentOnceToMe")
            UserDefaults.standard.set(nil, forKey: "pushSentOnceToOpponent")
        }
    }
   
    
    
    // MARK: - ****** Method to show opponent profile ******
    func viewUserDetails(_ anote: Notification)
    {
        if (self.draggableBackground.getUsersArray.count > 0)
        {
            let userID = ((self.draggableBackground.getUsersArray[self.draggableBackground.userAtIndex]) as AnyObject).value(forKey: "id") as! String
            
             let authToken = ((self.draggableBackground.getUsersArray[self.draggableBackground.userAtIndex]) as AnyObject).value(forKey: "auth_token") as! String
            
            if userID != ""
            {
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    self.viewProfileBool = true
                    DispatchQueue.main.asyncAfter(deadline: .now())
                    {
                        if self.viewProfileBool == true
                        {
                            self.viewProfileBool = false
                            let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                            viewProfileScreen.ownerID = userID
                            viewProfileScreen.ownerAuthToken = authToken
                        self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        }
                    }
                }
            }
        }
    }
    
    
    // MARK: - ****** Method to show likeProfileBtnClicked ******
    func likeProfileBtnClicked(_ anote: Notification)
    {
        print("likeProfileBtnClicked")
    }
    
    
     // MARK: - ****** Method to show no users found ******
    func NoUserAlert(_ anote: Notification)
    {
        self.filterArray = []
        self.noDataFoundView.isHidden = false
        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { (finished) in
        })
    }
    
    func stopRefreshingLoader(_ anote: Notification)
    {
        self.view.isUserInteractionEnabled = true
        self.loadingView.isHidden = true
        self.refreshLoadingView.isHidden = true
    }
    
    func getPaginationDataMethod(_ anote: Notification)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            if (self.currentPageForListing_allUsers < self.totalPagesForSwipeUsers)
            {
                self.currentPageForListing_allUsers = self.currentPageForListing_allUsers + 1
                self.loadingView.isHidden = false
                self.refreshLoadingView.isHidden = false
                
                self.getUsersListingForMatch(pageCount: String(self.currentPageForListing_allUsers), completion: { (responseBool) in
                    
                    if (responseBool == true)
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendAllUsers"), object: nil, userInfo: ["userInfo" : self.filterArray])
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.noDataFoundView.isHidden = false
                        self.refreshLoadingView.isHidden = true
                    }
                })
            }
            else
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlert"), object: nil, userInfo: nil)
            }
        }
    }
    
    // MARK: - ****** RelVibesAlert ******
    func RelVibesAlert(_ anote: Notification)
    {
        self.buyItsVibesView.isHidden = false
        self.buyItsVibesInnerView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.buyItsVibesInnerView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { (finished) in
        })
    }
    
    
     // MARK: - ****** Back Button Action. ******
    @IBAction func back_btnAction(_ sender: Any)
    {
        self.opponentIDOnMatch = ""
        if appDelegatePush == "fromAppDelegate"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        else
        {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers
            {
                if aViewController is HomeViewController
                {
                    self.navigationController!.popToViewController(aViewController, animated: true)
                    break
                }
            }
        }
    }
    
     // MARK: - ****** Wi Match other option buttons. ******
    @IBAction func WiMatch_MeetUpViewController(_ sender: UIButton)
    {
        var frame: CGPoint = self.mainScrollViewSections.contentOffset
        frame.x = self.mainScrollViewSections.frame.size.width * CGFloat(sender.tag-100);
        if sender.tag == 100
        {
            meetUpHeading.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            meetUpUnderline.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            vibesUnderline.backgroundColor = UIColor.lightGray
            vibesTitle.textColor = UIColor.lightGray
            linkUpTitle.textColor = UIColor.lightGray
            linkUpUnderline.backgroundColor = UIColor.lightGray
        }
        else if sender.tag == 101
        {
            linkUpTitle.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            linkUpUnderline.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            vibesUnderline.backgroundColor = UIColor.lightGray
            vibesTitle.textColor = UIColor.lightGray
            meetUpHeading.textColor = UIColor.lightGray
            meetUpUnderline.backgroundColor = UIColor.lightGray
            UserDefaults.standard.set("linkupuserViewedOnce", forKey: "LinkUpUsers")
            UserDefaults.standard.synchronize()
            self.linkUpCountView.isHidden = true
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                
                self.meetUpUserArray = []
                self.currentPageLinkUp = 1
                self.getLinkUpUsersListing(pageCountStr: self.currentPageLinkUp,completion: { (responseBool) in
                    
                    self.updateBadgeValueForVisitors(typeOfBadge: "linkup_badgecount", completion: { (result) in
                        
                    })
                    
                    if (responseBool == true)
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        self.refreshLoadingView.isHidden = true
                        self.linkUp_tableView.reloadData()
                        self.refreshControl_LinkUp.endRefreshing()
                        self.linkUp_nodataView.isHidden = true
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.refreshLoadingView.isHidden = true
                        self.meetUpUserArray = []
                        self.view.isUserInteractionEnabled = true
                        self.linkUp_tableView.isHidden = false
                        self.linkUp_nodataView.isHidden = false
                        self.linkUp_tableView.reloadData()
                        self.refreshControl_LinkUp.endRefreshing()
                    }
                })
            }
        }
        else if sender.tag == 102
        {
            vibesTitle.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            vibesUnderline.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            linkUpUnderline.backgroundColor = UIColor.lightGray
            linkUpTitle.textColor = UIColor.lightGray
            meetUpHeading.textColor = UIColor.lightGray
            meetUpUnderline.backgroundColor = UIColor.lightGray
            UserDefaults.standard.set("userViewedOnce", forKey: "VibesUsers")
            UserDefaults.standard.synchronize()
            self.vibesCountView.isHidden = true
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                
                self.details = []
                self.currentPageVibes = 1
                self.getVibesUsersListing(pageCountStr: self.currentPageVibes, completion: { (responseBool) in
                    
                    self.updateBadgeValueForVisitors(typeOfBadge: "vibes_badgecount", completion: { (result) in
                        
                    })
                    
                    if (responseBool == true)
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        self.refreshLoadingView.isHidden = true
                        DispatchQueue.main.async {
                            self.wiMatch_tableView.reloadData()
                            self.vibes_noDataView.isHidden = true
                            self.refreshControl_Vibes.endRefreshing()
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.refreshLoadingView.isHidden = true
                        self.details = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.wiMatch_tableView.isHidden = false
                        self.vibes_noDataView.isHidden = false
                        DispatchQueue.main.async {
                            self.wiMatch_tableView.reloadData()
                            self.refreshControl_Vibes.endRefreshing()
                        }
                    }
                })
            }
        }
        self.mainScrollViewSections.setContentOffset(frame, animated: true)
    }
    
    // MARK: - ****** UIScrollView Delegate ******
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        self.lastLocation.x = self.mainScrollViewSections.contentOffset.x
        self.lastLocation.y = self.mainScrollViewSections.contentOffset.y
    }
    
    
    // MARK: - ****** scrollViewDidEndDecelerating ******
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let pageWidth:CGFloat = self.mainScrollViewSections.frame.width
        let currentPage:CGFloat = floor((self.mainScrollViewSections.contentOffset.x-pageWidth/2)/pageWidth)+1
        let translation = self.mainScrollViewSections.panGestureRecognizer.translation(in: self.mainScrollViewSections.superview)
        
        if translation.y > 0 {
            // swipes from top to bottom of screen -> down
            print("okkk")
        } else {
            // swipes from bottom to top of screen -> up
            print("okkk 111")
            
            if currentPage == 0.0
            {
               
            }
            else if currentPage == 1.0
            {
                if (self.linkUp_tableView.contentOffset.y >= (self.linkUp_tableView.contentSize.height - self.linkUp_tableView.frame.size.height) ) {
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                        if self.currentPageLinkUp < self.TotalPagesLinkUp {
                            
                            let bottomEdge: Float = Float(self.linkUp_tableView.contentOffset.y + linkUp_tableView.frame.size.height)
                            if bottomEdge >= Float(linkUp_tableView.contentSize.height)  {
                                
                                print("we are at end of link up table")
                                
                                self.currentPageLinkUp = self.currentPageLinkUp + 1
                                self.getLinkUpUsersListing(pageCountStr: self.currentPageLinkUp,completion: { (responseBool) in
                                    if (responseBool == true)
                                    {
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        self.refreshLoadingView.isHidden = true
                                        self.linkUp_tableView.reloadData()
                                        self.refreshControl_LinkUp.endRefreshing()
                                        self.linkUp_nodataView.isHidden = true
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.refreshLoadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.linkUp_tableView.isHidden = false
                                        self.linkUp_nodataView.isHidden = true
                                        self.linkUp_tableView.reloadData()
                                        self.refreshControl_LinkUp.endRefreshing()
                                    }
                                })
                            }
                        }
                    }
                }
            }
            else if currentPage == 2.0
            {
                if (self.wiMatch_tableView.contentOffset.y >= (self.wiMatch_tableView.contentSize.height - self.wiMatch_tableView.frame.size.height) ) {
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                        
                        if self.currentPageVibes < self.TotalPagesVibes {
                            let bottomEdge: Float = Float(self.wiMatch_tableView.contentOffset.y + wiMatch_tableView.frame.size.height)
                            if bottomEdge >= Float(wiMatch_tableView.contentSize.height)  {
                                
                                print("we are at end of it's vibes table")
                                
                                self.currentPageVibes = self.currentPageVibes  + 1
                                self.getVibesUsersListing(pageCountStr: self.currentPageVibes, completion: { (responseBool) in
                                    if (responseBool == true)
                                    {
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        self.refreshLoadingView.isHidden = true
                                        DispatchQueue.main.async {
                                            self.wiMatch_tableView.reloadData()
                                            self.vibes_noDataView.isHidden = true
                                            self.refreshControl_Vibes.endRefreshing()
                                        }
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.refreshLoadingView.isHidden = true
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.wiMatch_tableView.isHidden = false
                                        self.vibes_noDataView.isHidden = true
                                        DispatchQueue.main.async {
                                            self.wiMatch_tableView.reloadData()
                                            self.refreshControl_Vibes.endRefreshing()
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
        
        if currentPage == 0.0
        {
            meetUpHeading.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            meetUpUnderline.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            vibesUnderline.backgroundColor = UIColor.lightGray
            vibesTitle.textColor = UIColor.lightGray
            linkUpTitle.textColor = UIColor.lightGray
            linkUpUnderline.backgroundColor = UIColor.lightGray
        }
        else if currentPage == 1.0
        {
            linkUpTitle.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            linkUpUnderline.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            vibesUnderline.backgroundColor = UIColor.lightGray
            vibesTitle.textColor = UIColor.lightGray
            meetUpHeading.textColor = UIColor.lightGray
            meetUpUnderline.backgroundColor = UIColor.lightGray
            UserDefaults.standard.set("linkupuserViewedOnce", forKey: "LinkUpUsers")
            UserDefaults.standard.synchronize()
            self.linkUpCountView.isHidden = true
        }
        else if currentPage == 2.0
        {
            vibesTitle.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            vibesUnderline.backgroundColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            linkUpUnderline.backgroundColor = UIColor.lightGray
            linkUpTitle.textColor = UIColor.lightGray
            meetUpHeading.textColor = UIColor.lightGray
            meetUpUnderline.backgroundColor = UIColor.lightGray
            UserDefaults.standard.set("userViewedOnce", forKey: "VibesUsers")
            UserDefaults.standard.synchronize()
            self.vibesCountView.isHidden = true
        }
        
        if Int(self.lastLocation.x) <  Int(self.mainScrollViewSections.contentOffset.x) {
            // moved right
            if currentPage == 1.0
            {
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    
                    self.meetUpUserArray = []
                    self.currentPageLinkUp = 1
                    self.getLinkUpUsersListing(pageCountStr: self.currentPageLinkUp,completion: { (responseBool) in
                        
                        self.updateBadgeValueForVisitors(typeOfBadge: "linkup_badgecount", completion: { (result) in
                            
                        })
                        
                        if (responseBool == true)
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.linkUp_nodataView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            self.linkUp_tableView.reloadData()
                            self.refreshControl_LinkUp.endRefreshing()
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.refreshLoadingView.isHidden = true
                            self.meetUpUserArray = []
                            self.view.isUserInteractionEnabled = true
                            self.linkUp_tableView.isHidden = false
                            self.linkUp_nodataView.isHidden = false
                            self.linkUp_tableView.reloadData()
                            self.refreshControl_LinkUp.endRefreshing()
                        }
                    })
                }
            }
            else if currentPage == 2.0
            {
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    
                    self.details = []
                    self.currentPageVibes = 1
                    self.getVibesUsersListing(pageCountStr: self.currentPageVibes, completion: { (responseBool) in
                        
                        self.updateBadgeValueForVisitors(typeOfBadge: "vibes_badgecount", completion: { (result) in
                            
                        })
                        
                        if (responseBool == true)
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            DispatchQueue.main.async {
                                self.wiMatch_tableView.reloadData()
                                self.vibes_noDataView.isHidden = true
                                self.refreshControl_Vibes.endRefreshing()
                            }
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.refreshLoadingView.isHidden = true
                            self.details = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.wiMatch_tableView.isHidden = false
                            self.vibes_noDataView.isHidden = false
                            DispatchQueue.main.async {
                                self.wiMatch_tableView.reloadData()
                                self.refreshControl_Vibes.endRefreshing()
                            }
                        }
                    })
                }
            }
        }
        else if Int(self.lastLocation.x) > Int(self.mainScrollViewSections.contentOffset.x) {
            // moved left
           
            if currentPage == 1.0
            {
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    
                    self.meetUpUserArray = []
                    self.currentPageLinkUp = 1
                    self.getLinkUpUsersListing(pageCountStr: self.currentPageLinkUp,completion: { (responseBool) in
                        
                        self.updateBadgeValueForVisitors(typeOfBadge: "linkup_badgecount", completion: { (result) in
                            
                        })
                        
                        if (responseBool == true)
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.linkUp_nodataView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            self.linkUp_tableView.reloadData()
                            self.refreshControl_LinkUp.endRefreshing()
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.refreshLoadingView.isHidden = true
                            self.meetUpUserArray = []
                            self.view.isUserInteractionEnabled = true
                            self.linkUp_tableView.isHidden = false
                            self.linkUp_nodataView.isHidden = false
                            self.linkUp_tableView.reloadData()
                            self.refreshControl_LinkUp.endRefreshing()
                        }
                    })
                }
            }
            else if currentPage == 2.0
            {
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):
                    
                    self.details = []
                    self.currentPageVibes = 1
                    self.getVibesUsersListing(pageCountStr: self.currentPageVibes, completion: { (responseBool) in
                        
                        self.updateBadgeValueForVisitors(typeOfBadge: "vibes_badgecount", completion: { (result) in
                            
                        })
                        
                        if (responseBool == true)
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.refreshLoadingView.isHidden = true
                            DispatchQueue.main.async {
                                self.wiMatch_tableView.reloadData()
                                self.vibes_noDataView.isHidden = true
                                self.refreshControl_Vibes.endRefreshing()
                            }
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.refreshLoadingView.isHidden = true
                            self.details = []
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.wiMatch_tableView.isHidden = false
                            self.vibes_noDataView.isHidden = false
                            DispatchQueue.main.async {
                                self.wiMatch_tableView.reloadData()
                                self.refreshControl_Vibes.endRefreshing()
                            }
                        }
                    })
                }
            }
        }
        else if Int(self.lastLocation.y) < Int(self.mainScrollViewSections.contentOffset.y) {
            print("up")
        }
        else if Int(self.lastLocation.y) > Int(self.mainScrollViewSections.contentOffset.y) {
            print("down")
        }
    }
    
    func updateBadgeValueForVisitors(typeOfBadge: String,completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                ApiHandler.updateBadgeCountValue(user_id: userID, auth_token: userToken, typeofBadgeCountToBeUpdated: typeOfBadge, totalBadgeCount: 0, completion: { (responseData) in
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
//                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                        print("dataDict = \(String(describing: dataDict))")
//                        print("badge value for \(typeOfBadge) updated")
                    }
                    else
                    {
//                        print("badge value for \(typeOfBadge) not updated")
                    }
                })
            }
        }
    }
   
  
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {

    }
    
    
    
    // MARK: - ****** Close Buy In App Purchase View Button Action. ******
    @IBAction func crossBuyView_btnAction(_ sender: Any) {
         self.buyProfileVisibilityView.isHidden = true
    }
    
    
    
    
    // MARK: - ****** Buy In App Purchase Button Action. ******
    @IBAction func buyProfileVisibility_btnAction(_ sender: Any) {
        let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "CreditsPackageViewController") as! CreditsPackageViewController
        messagesScreen.typeOfCreditStr = "FromMeetUpScreen"
        self.navigationController?.pushViewController(messagesScreen, animated: true)
    }
    
    
    // MARK: - ****** UITableView Delegates. ******
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == self.linkUp_tableView
        {
            return self.meetUpUserArray.count
        }
        else
        {
            return self.details.count
        }
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let onlineUsersCell = tableView.dequeueReusableCell(withIdentifier: "onlineUsersCell", for: indexPath) as! OnlineUsersTableViewCell
        
        self.linkUp_tableView.separatorColor = UIColor.clear
        onlineUsersCell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if tableView == self.linkUp_tableView
        {
            if (self.meetUpUserArray.count <= 0)
            {
                return onlineUsersCell
            }
            else
            {
                if self.profileVisitorBool == true
                {
                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = onlineUsersCell.usersProfilePic.bounds
                    onlineUsersCell.usersProfilePic.addSubview(blurEffectView)
                }
                else{
                    DispatchQueue.main.async {
                        
                        for subview in onlineUsersCell.usersProfilePic.subviews
                        {
                            if subview.isKind(of: UIVisualEffectView.self)
                            {
                                subview.removeFromSuperview()
                            }
                        }
                        
                        onlineUsersCell.usersProfilePic.sd_setImage(with: URL(string:((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "profile_pic") as! String), placeholderImage: UIImage(named: "Default Icon"))
                    }
                }
                
                let dict = ((self.meetUpUserArray[indexPath.row]) as AnyObject)
                let keysArray = dict.allKeys as! [String]
                
                if (((self.meetUpUserArray[indexPath.row]) as AnyObject) != nil)
                {
                    if (keysArray.contains("profile_pic") && keysArray.contains("location"))
                    {
                        let urlStr = ((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "profile_pic") as! String
                        
                        if (urlStr != "")
                        {
                            onlineUsersCell.sendMessageBtn.tag = indexPath.row
                            onlineUsersCell.sendMessageBtn.addTarget(self, action: #selector(self.sendMessageUser), for: .touchUpInside)
                            
                            onlineUsersCell.usersProfilePic.sd_setImage(with: URL(string:((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "profile_pic") as! String), placeholderImage: UIImage(named: "Default Icon"))
                            
                            onlineUsersCell.usersProfilePic.contentMode = UIViewContentMode.scaleAspectFill
                            onlineUsersCell.usersProfilePic.clipsToBounds = true
                            
                            let userLocation = ((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "location") as? String
                            onlineUsersCell.usersLocation_lbl.text = userLocation
                            
                            let meet_up_preferences = ((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "meet_up_preferences") as? String
                            onlineUsersCell.usersNewMeetUpLbl.text = meet_up_preferences
                            
                            let userGender = ((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "gender") as? String
                            
                            let username = ((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "username") as? String
                            
                            var gender = String()
                            if (userGender == "1")
                            {
                                gender = "Male"
                                
                            }
                            else
                            {
                                gender = "Female"
                            }
                            
                            
                            var swipe_updated_time = ((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "swipe_updated_time") as? String
                            
                            if (swipe_updated_time == "")
                            {
                                onlineUsersCell.hoursAgoLinkUpLbl.text = ""
                            }
                            else
                            {
                                if (swipe_updated_time == "")
                                {
                                    onlineUsersCell.hoursAgoLinkUpLbl.text = ""
                                }
                                else
                                {
                                    swipe_updated_time = swipe_updated_time! + " UTC"
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeZone = TimeZone.current
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                                    
                                    if (swipe_updated_time != nil)
                                    {
                                        let datee = dateFormatter.date(from: swipe_updated_time!)!
                                        
                                        let dateString = timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
                                        
                                        if dateString == ""
                                        {
                                            onlineUsersCell.hoursAgoLinkUpLbl.text = ""
                                        }
                                        else
                                        {
                                            onlineUsersCell.hoursAgoLinkUpLbl.text = dateString
                                        }
                                    }
                                    else
                                    {
                                        onlineUsersCell.hoursAgoLinkUpLbl.text = ""
                                    }
                                }
                            }
                            
                            
                            let userAge = ((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "age") as? String
                            
                            if self.profileVisitorBool == true
                            {
                                onlineUsersCell.userName_lbl.text = userAge! + ", " +  gender //+ "\n" + userLocation!
                            }
                            else{
                                onlineUsersCell.userName_lbl.text  = username! + ", " + userAge! + ", " +  gender
                            }
                            
                            
                            return onlineUsersCell
                        }
                        else
                        {
                            return onlineUsersCell
                        }
                    }
                    else{
                        return onlineUsersCell
                    }
                }
                else
                {
                    return onlineUsersCell
                }
            }
        }
        else
        {
          if (self.details.count > 0)
          {
            if (self.details[indexPath.row].userIDStr != "")
            {
                onlineUsersCell.islookingFor_lbl.text = self.details[indexPath.row].interestedIn
                onlineUsersCell.islookingFor_lbl.numberOfLines = 5
                onlineUsersCell.islookingFor_lbl.adjustsFontSizeToFitWidth = true
                
                if self.details[indexPath.row].onlineStatus == "0"
                {
                    onlineUsersCell.usersOnlineIcon.isHidden = true
                }
                else
                {
                    onlineUsersCell.usersOnlineIcon.isHidden = false
                    onlineUsersCell.usersOnlineIcon.image = UIImage(named: "Online Icon")
                }
                
                onlineUsersCell.sendMessageBtn.tag = indexPath.row
                onlineUsersCell.sendMessageBtn.addTarget(self, action: #selector(self.sendMessageUser_ItVibes), for: .touchUpInside)
                
                DispatchQueue.main.async {
                    
                    if (self.details.count > 0)
                    {
                        if (self.details[indexPath.row].profilePic != "")
                        {
                            onlineUsersCell.usersProfilePic.sd_setImage(with: URL(string: self.details[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"))
                        }
                        else{
                            onlineUsersCell.usersProfilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "Default Icon"))
                        }
                    }
                    else{
                        onlineUsersCell.usersProfilePic.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "Default Icon"))
                    }
                }
                
                onlineUsersCell.usersProfilePic.contentMode = UIViewContentMode.scaleAspectFill
                onlineUsersCell.usersProfilePic.clipsToBounds = true
                
                let userName =  self.details[indexPath.row].name
                let userAge = self.details[indexPath.row].age
                onlineUsersCell.userName_lbl.text = userName + ", " +  userAge
                onlineUsersCell.usersLocation_lbl.text = self.details[indexPath.row].location
                
                var timeStampInt = self.details[indexPath.row].timestamp
                
                if (timeStampInt.count == 0)
                {
                    onlineUsersCell.hoursAgoVibesLbl.text = ""
                }
                else
                {
                    timeStampInt = timeStampInt + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    
                    if (timeStampInt != nil)
                    {
                        let datee = dateFormatter.date(from: timeStampInt)!
                        
                        let dateString = timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
                        
                        if dateString == ""
                        {
                            onlineUsersCell.hoursAgoVibesLbl.text = ""
                        }
                        else
                        {
                            onlineUsersCell.hoursAgoVibesLbl.text = dateString
                        }
                    }
                    else{
                         onlineUsersCell.hoursAgoVibesLbl.text = ""
                    }
                }
                
                
                if self.details[indexPath.row].swipeType == "RightSwipe"
                {
                    onlineUsersCell.superLikeIcon.isHidden = true
                }
                else if self.details[indexPath.row].swipeType == "SuperLikeForVibes"
                {
                    onlineUsersCell.superLikeIcon.isHidden = false
                }
                else
                {
                    onlineUsersCell.superLikeIcon.isHidden = true
                }
             }
          }
    
          return onlineUsersCell
            
        }
    }
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) mon ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 mon ago"
            } else {
                return "Last mon"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "an hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) min ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "a min ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) sec ago"
        } else {
            return "just now"
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == self.linkUp_tableView
        {
            if self.profileVisitorBool == true
            {
                self.buyProfileVisibilityView.isHidden = false
                self.buybtnView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.buybtnView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                })
            }
            else
            {
                if (self.meetUpUserArray.count <= 0)
                {
                   return
                }
                else
                {
                    let dict = ((self.meetUpUserArray[indexPath.row]) as AnyObject)
                    let keysArray = dict.allKeys as! [String]
                    
                    if (((self.meetUpUserArray[indexPath.row]) as AnyObject) != nil)
                    {
                        if (keysArray.contains("profile_pic") && keysArray.contains("location"))
                        {
                            let userID =  ((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "id") as! String
                            
                            self.viewProfileBool = true
                            
                            let authToken = ((self.meetUpUserArray[indexPath.row]) as AnyObject).value(forKey: "auth_token") as! String
                            
                            DispatchQueue.main.asyncAfter(deadline: .now())
                            {
                                if self.viewProfileBool == true
                                {
                                    self.viewProfileBool = false
                                    let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                                    viewProfileScreen.ownerID = userID
                                    viewProfileScreen.ownerAuthToken = authToken
                                    self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                                }
                                else
                                {
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                
                if (self.details.count <= 0)
                {
                    return
                }
                else
                {
                    let userID = self.details[indexPath.row].userIDStr
                    let authToken = self.details[indexPath.row].authToken
                    self.viewProfileBool = true
                    
                    DispatchQueue.main.asyncAfter(deadline: .now())
                    {
                        if self.viewProfileBool == true
                        {
                            self.viewProfileBool = false
                            let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                            viewProfileScreen.ownerID = userID
                            viewProfileScreen.ownerAuthToken = authToken
                            self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        }
                    }
                }
            }
        }
    }
   

    
    // MARK: - ****** Send Message to Opponent Users ******
    func sendMessageUser(_sender: UIButton!)
    {
        if self.profileVisitorBool == true
        {
            self.buyProfileVisibilityView.isHidden = false
            self.buybtnView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.buybtnView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })
        }
        else
        {
            if (self.meetUpUserArray.count > 0)
            {
                let usersDetailsDict = self.meetUpUserArray[_sender.tag] as! NSDictionary
                
                let profilePic = UIImageView()
                profilePic.sd_setImage(with: URL(string:((self.meetUpUserArray[_sender.tag]) as AnyObject).value(forKey: "profile_pic") as! String), placeholderImage: UIImage(named: "Default Icon"))
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                
                let userID =  usersDetailsDict.value(forKey: "id") as? String
                let firebase_user_id =  usersDetailsDict.value(forKey: "firebase_user_id") as? String
                
                messagesScreen.opponentName = (usersDetailsDict.value(forKey: "username") as? String)!
                messagesScreen.opponentImage = usersDetailsDict.value(forKey: "profile_pic") as! String
                messagesScreen.opponentUserID = userID!
                messagesScreen.opponentFirebaseUserID = firebase_user_id!
                messagesScreen.opponentProfilePic =  profilePic.image!
                messagesScreen.typeOfUser = "WithProfileUser"
                messagesScreen.mainConfessionCheck = "comingFromViewProfile"
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromListingScreen"
                messagesScreen.typeOfUser = "WithProfileUser"
                messagesScreen.checkVisibiltyStr = "FromViewProfile"
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }
        }
    }

 
    
    
    // MARK: - ****** Send Message to Opponent Users ******
    func sendMessageUser_ItVibes(_sender: UIButton)
    {
        if (self.details.count > 0)
        {
            let profilePic = UIImageView()
            profilePic.sd_setImage(with: URL(string: self.details[_sender.tag].profilePic), placeholderImage: UIImage(named: "Default Icon"))
            
            let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
            let userID =  self.details[_sender.tag].userIDStr
            messagesScreen.opponentName = self.details[_sender.tag].name
            messagesScreen.opponentImage = self.details[_sender.tag].profilePic
            messagesScreen.opponentUserID = userID
            messagesScreen.opponentFirebaseUserID = self.details[_sender.tag].firebase_user_id
            messagesScreen.opponentProfilePic =  profilePic.image!
            messagesScreen.typeOfUser = "WithProfileUser"
            messagesScreen.mainConfessionCheck = "comingFromViewProfile"
            messagesScreen.chatUserDifferentiateStr = "Messages"
            messagesScreen.viewOpponentProfile = "fromListingScreen"
            messagesScreen.checkVisibiltyStr = "FromViewProfile"
            self.navigationController?.pushViewController(messagesScreen, animated: true)
        }
    }
    
    // MARK: - ****** In App Purchase For It's Vibes ******
    @IBAction func buyItsVibes_btnAction(_ sender: Any) {
        let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "CreditsPackageViewController") as! CreditsPackageViewController
        messagesScreen.typeOfCreditStr = "FromMeetUpScreen"
        self.navigationController?.pushViewController(messagesScreen, animated: true)
    }
    
    // MARK: - ****** Close It's Vibes in App purchase View ******
    @IBAction func closeitsVibes_btnAction(_ sender: Any) {
        self.buyItsVibesView.isHidden = true
    }
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
