//
//  OverlayView.swift
//  TinderSwipeCardsSwift
//
//  Created by Gao Chao on 4/30/15.
//  Copyright (c) 2015 gcweb. All rights reserved.
//

import Foundation
import UIKit

enum GGOverlayViewMode {
    case GGOverlayViewModeLeft
    case GGOverlayViewModeRight
}

class OverlayView: UIView{
    var _mode: GGOverlayViewMode! = GGOverlayViewMode.GGOverlayViewModeLeft
    var imageView: UIImageView!
    var swipeType_lbl = UILabel()


    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        imageView = UIImageView(image: UIImage(named: "No Icon"))
         self.addSubview(imageView)
        if swipeType_lbl.text == nil
        {
//            print("swipeType_lblswipeType_lblswipeType_lblswipeType_lblswipeType_lblswipeType_lbl")
            return
        }
        else
        {
//            print(swipeType_lbl)
            swipeType_lbl.text = "Nah"
            swipeType_lbl.font = UIFont.boldSystemFont(ofSize: 60)
            swipeType_lbl.textColor = UIColor.red
            self.addSubview(swipeType_lbl)
        }
       
    }

    func setMode(mode: GGOverlayViewMode) -> Void
    {
        if _mode == mode
        {
            return
        }
        _mode = mode

        if _mode == GGOverlayViewMode.GGOverlayViewModeLeft
        {
            imageView.image = UIImage(named: "No Icon")
            swipeType_lbl.text = "Nah"
            swipeType_lbl.font = UIFont.boldSystemFont(ofSize: 60)
            swipeType_lbl.textColor = UIColor.red
        }
        else
        {
            imageView.image = UIImage(named: "Yes Icon")
            swipeType_lbl.text = "Vibes"
            swipeType_lbl.font = UIFont.boldSystemFont(ofSize: 60)
            swipeType_lbl.textColor = UIColor.green
        }
    }

    override func layoutSubviews()
    {
        super.layoutSubviews()
        imageView.frame = CGRect(x: 20, y: 50, width : 100, height: 100)
        swipeType_lbl.frame = CGRect(x: 50, y: imageView.frame.size.height + 50, width : 300, height: 100)
    }
}
