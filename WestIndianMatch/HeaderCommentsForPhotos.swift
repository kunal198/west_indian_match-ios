//
//  HeaderCommentsForPhotos.swift
//  Wi Match
//
//  Created by brst on 31/07/18.
//  Copyright © 2018 brst. All rights reserved.
//

import UIKit
import FLAnimatedImage

class HeaderCommentsForPhotos: UITableViewHeaderFooterView {

    
    
    @IBOutlet weak var statusTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var repliesIcon: UIImageView!
    @IBOutlet weak var likesIcon: UIImageView!
    @IBOutlet weak var usersNameLbl: UILabel!
    @IBOutlet weak var userProfileIcon: UIImageView!
    @IBOutlet weak var usersLocationLbl: UILabel!
    @IBOutlet weak var userViewProfileBtn: UIButton!
    @IBOutlet weak var statusAddedTextView: UITextView!
    @IBOutlet weak var dropDownOptionsBtn: UIButton!
    
    @IBOutlet weak var postedPhotoImage: FLAnimatedImageView!
    @IBOutlet weak var likesBtn: UIButton!
    @IBOutlet weak var replyBtn: UIButton!
    @IBOutlet weak var messageBtnLbl: UIButton!
    @IBOutlet weak var likesCountLbl: UILabel!
    @IBOutlet weak var repliesCountLbl: UILabel!
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
