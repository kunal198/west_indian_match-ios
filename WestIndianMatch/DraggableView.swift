//
//  DraggableView.swift
//  TinderSwipeCardsSwift
//
//  Created by Gao Chao on 4/30/15.
//  Copyright (c) 2015 gcweb. All rights reserved.
//

import Foundation
import UIKit

let ACTION_MARGIN: Float = 120      //%%% distance from center where the action applies. Higher = swipe further in order for the action to be called
let SCALE_STRENGTH: Float = 4       //%%% how quickly the card shrinks. Higher = slower shrinking
let SCALE_MAX:Float = 0.93          //%%% upper bar for how much the card shrinks. Higher = shrinks less
let ROTATION_MAX: Float = 1         //%%% the maximum rotation allowed in radians.  Higher = card can keep rotating longer
let ROTATION_STRENGTH: Float = 320  //%%% strength of rotation. Higher = weaker rotation
let ROTATION_ANGLE: Float = 3.14/8  //%%% Higher = stronger rotation angle

protocol DraggableViewDelegate {
    func cardSwipedLeft(card: UIView) -> Void
    func cardSwipedRight(card: UIView) -> Void
}

class DraggableView: UIView {
    var delegate: DraggableViewDelegate!
    var panGestureRecognizer: UIPanGestureRecognizer!
    var originPoint: CGPoint!
    var overlayView: OverlayView!
    var information: UILabel!
    var profilePicImage : UIImageView!
    var xFromCenter: Float!
    var yFromCenter: Float!
    var viewProfileButton: UIButton!
    var userDetailsLabel:UILabel!
    var swipeTitle_lbl: UILabel!
    var getDraggableView:DraggableViewBackground!
    var viewProfileBtnBool: Bool!

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupView()
        
        viewProfileBtnBool = false
        
        if UIScreen.main.bounds.size.width == 320
        {
             profilePicImage = UIImageView(frame: CGRect(x: 0 , y: 0 , width : 290 , height : 270))
             viewProfileButton = UIButton(frame: CGRect(x:  0, y:  0, width: 290, height: 300))
             userDetailsLabel = UILabel(frame: CGRect(x: 0 , y: profilePicImage.frame.size.height + 10 , width : 290 , height : 40))
             swipeTitle_lbl = UILabel(frame: CGRect(x: 0 , y: 120, width : 290 , height : 35))
        }
        else if UIScreen.main.bounds.size.width == 375
        {
            profilePicImage = UIImageView(frame: CGRect(x: 0 , y: 0 , width : 345 , height : 310))
            viewProfileButton = UIButton(frame: CGRect(x:  0, y:  0, width: 345, height: 300))
            userDetailsLabel = UILabel(frame: CGRect(x: 0 , y: profilePicImage.frame.size.height + 10 , width : 345 , height : 40))
             swipeTitle_lbl = UILabel(frame: CGRect(x: 0 , y: 120, width : 345 , height : 35))
        }
        else if UIScreen.main.bounds.size.width == 414
        {
            profilePicImage = UIImageView(frame: CGRect(x: 0 , y: 0 , width : 374 , height : 400))
            viewProfileButton = UIButton(frame: CGRect(x: 0 , y: 0 , width : 334 , height : 400))
            userDetailsLabel = UILabel(frame: CGRect(x: 0 , y: profilePicImage.frame.size.height + 10 , width : 374 , height : 40))
             swipeTitle_lbl = UILabel(frame: CGRect(x: 0 , y: 120, width : 374 , height : 35))
        }

    
        profilePicImage.image = UIImage(named: "Default Icon")
        
//        information = UILabel(frame: CGRect(x: 0, y : 50, width : self.frame.size.width, height: 100))
//        information.text = "no info given"
//        information.textAlignment = NSTextAlignment.center
//        information.textColor = UIColor.black

        self.backgroundColor = UIColor.clear

        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(DraggableView.beingDragged))
        
        viewProfileButton.addTarget(self, action: #selector(DraggableView.viewOpponentProfile), for: UIControlEvents.touchUpInside)
        
        self.addGestureRecognizer(panGestureRecognizer)
//        self.addSubview(information)
        self.addSubview(userDetailsLabel)
        self.addSubview(profilePicImage)
        self.addSubview(viewProfileButton)
        self.addSubview(swipeTitle_lbl)
        self.bringSubview(toFront: swipeTitle_lbl)
//        profilePicImage.addSubview(swipeTitle_lbl)
        overlayView = OverlayView(frame: CGRect(x: 50, y: 0, width: 100, height: 100))
        overlayView.alpha = 0
        self.addSubview(overlayView)

        xFromCenter = 0
        yFromCenter = 0
    }
    
    func viewOpponentProfile() -> Void
    {
//        print("++++++++++ viewOpponentProfile btn clicked ++++++++++")
//        if viewProfileBtnBool == false{
//            viewProfileBtnBool = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "viewProfileBtnNotificaton"), object: nil, userInfo: nil)
//        }
//        else{
//            viewProfileBtnBool = false
//        }
    }

    func setupView() -> Void {
        self.layer.cornerRadius = 4;
        self.layer.shadowRadius = 3;
        self.layer.shadowOpacity = 0.2;
        self.layer.shadowOffset = CGSize(width: 1, height:1);
    }

    func beingDragged(gestureRecognizer: UIPanGestureRecognizer) -> Void
    {
        xFromCenter = Float(gestureRecognizer.translation(in: self).x)
        yFromCenter = Float(gestureRecognizer.translation(in: self).y)
        
        switch gestureRecognizer.state
        {
        case UIGestureRecognizerState.began:
            self.originPoint = self.center
        case UIGestureRecognizerState.changed:
            let rotationStrength: Float = min(xFromCenter/ROTATION_STRENGTH, ROTATION_MAX)
            let rotationAngle = ROTATION_ANGLE * rotationStrength
            let scale = max(1 - fabsf(rotationStrength) / SCALE_STRENGTH, SCALE_MAX)
            self.center = CGPoint(x: self.originPoint.x + CGFloat(xFromCenter),y :self.originPoint.y + CGFloat(yFromCenter))
            let transform = CGAffineTransform(rotationAngle: CGFloat(rotationAngle))
            let scaleTransform = transform.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
            self.transform = scaleTransform
            self.updateOverlay(distance: CGFloat(xFromCenter))
        case UIGestureRecognizerState.ended:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwipeNotify"), object: nil, userInfo: ["userInfo" : "swipeEnded"])
            self.afterSwipeAction()
        case UIGestureRecognizerState.possible:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwipeNotify"), object: nil, userInfo: ["userInfo" : "swipeEnded"])
            fallthrough
        case UIGestureRecognizerState.cancelled:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwipeNotify"), object: nil, userInfo: ["userInfo" : "swipeEnded"])
            fallthrough
        case UIGestureRecognizerState.failed:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwipeNotify"), object: nil, userInfo: ["userInfo" : "swipeEnded"])
            fallthrough
        default:
            break
        }
    }

    func updateOverlay(distance: CGFloat) -> Void {
        if distance > 0
        {
            overlayView.setMode(mode: GGOverlayViewMode.GGOverlayViewModeRight)
            print("+++++++++++ swipe to right +++++++++++")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwipeNotify"), object: nil, userInfo: ["userInfo" : "swipedRight"])
        }
        else
        {
            overlayView.setMode(mode: GGOverlayViewMode.GGOverlayViewModeLeft)
            print("+++++++++++ swipe to left +++++++++++")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwipeNotify"), object: nil, userInfo: ["userInfo" : "swipedLeft"])
        }
        
        overlayView.alpha = CGFloat(min(fabsf(Float(distance))/100, 0.4))
    }

    func afterSwipeAction() -> Void
    {
        let floatXFromCenter = Float(xFromCenter)
        if floatXFromCenter > ACTION_MARGIN {
//            getDraggableView.typeOfButton = "RightSwipe"
            self.rightAction()
        } else if floatXFromCenter < -ACTION_MARGIN {
            self.leftAction()
        } else {
            UIView.animate(withDuration: 0.3, animations: {() -> Void in
                self.center = self.originPoint
                self.transform = CGAffineTransform(rotationAngle: 0)
                self.overlayView.alpha = 0
            })
        }
    }
    
    func rightAction() -> Void {
        let finishPoint: CGPoint = CGPoint(x: 500, y: 2 * CGFloat(yFromCenter) + self.originPoint.y)
        UIView.animate(withDuration: 0.3,
            animations: {
                self.center = finishPoint
            }, completion: {
                (value: Bool) in
                self.removeFromSuperview()

        })
        delegate.cardSwipedRight(card: self)
    }

    func leftAction() -> Void {
        let finishPoint: CGPoint = CGPoint(x: -500, y:  2 * CGFloat(yFromCenter) + self.originPoint.y)
        UIView.animate(withDuration: 0.3,
            animations: {
                self.center = finishPoint
            }, completion: {
                (value: Bool) in
                self.removeFromSuperview()

        })
        delegate.cardSwipedLeft(card: self)
    }

    func rightClickAction() -> Void
    {
        let finishPoint = CGPoint(x: 600, y: self.center.y)
        UIView.animate(withDuration: 0.3,
            animations: {
                self.center = finishPoint
                self.transform = CGAffineTransform(rotationAngle: 1)
            }, completion: {
                (value: Bool) in
                self.removeFromSuperview()
        })
        delegate.cardSwipedRight(card: self)
    }

    func leftClickAction() -> Void {
        let finishPoint: CGPoint = CGPoint(x: -600, y: self.center.y)
        UIView.animate(withDuration: 0.3,
            animations: {
                self.center = finishPoint
                self.transform = CGAffineTransform(rotationAngle: 1)
            }, completion: {
                (value: Bool) in
                self.removeFromSuperview()
        })
        delegate.cardSwipedLeft(card: self)
    }
}
