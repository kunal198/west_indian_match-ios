//
//  SettingsViewController.swift
//  WestIndianMatch
//
//  Created by brst on 22/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import FBSDKLoginKit
import FBSDKCoreKit
import AVFoundation
import AVKit

class SettingsViewController: UIViewController,UIScrollViewDelegate,UITextFieldDelegate,UIWebViewDelegate
{
    //MARK:- ***** VARIABLES DECLARATION *****
    var myProfileVisibilityStatus = String()
    var changePasswordBool = Bool()
    var savedMyPassword = String()
    var userID = String()
    var webViewBool = Bool()
    
     //MARK:- ***** OUTLETS DECLARATION *****
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var profileVisibilitySwitch: UISwitch!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var deleteProfileBtn: UIButton!
    @IBOutlet weak var messages_switch: UISwitch!
    @IBOutlet weak var events_swicth: UISwitch!
    @IBOutlet weak var newMatches_switch: UISwitch!
    @IBOutlet weak var profileVisitor_switch: UISwitch!
    @IBOutlet weak var profileVisibilityBtn: UIButton!
    @IBOutlet weak var profileWrapperView: UIView!
    @IBOutlet weak var notificationWrapperView: UIView!
    @IBOutlet weak var generalWrapperView: UIView!
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var wiChatRoomSwitch: UISwitch!
    @IBOutlet weak var commentsSwitch: UISwitch!
    @IBOutlet weak var wrapperScrollView: UIScrollView!
    @IBOutlet weak var navTitle_lbl: UILabel!
    @IBOutlet weak var confirmPassword_txtField: UITextField!
    @IBOutlet weak var oldPassword_txtField: UITextField!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var privacyPolicy_webView: UIWebView!
    @IBOutlet weak var otherDeleteView: UIView!
    @IBOutlet weak var otherChangePswrdView: UIView!
    @IBOutlet weak var newPassword_txtField: UITextField!
    @IBOutlet weak var loadingImage: UIImageView!
    
    @IBOutlet weak var locationServicesSwitch: UISwitch!
    
     // MARK: - ****** viewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = true
        self.changePasswordView.isHidden = true
        self.wrapperScrollView.delegate = self
        self.changePasswordBool = false
        self.privacyPolicy_webView.isHidden = true
        self.webViewBool = false
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.FromSettingsScreen), name: NSNotification.Name(rawValue: "FromSettingsScreen"), object: nil)
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserProfile), name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
        
      
        
        self.getSavedVisibility()
        
        if let savedPassword = UserDefaults.standard.value(forKey: "passwordSaved") as? String
        {
            self.savedMyPassword = savedPassword as String
        }
        
        if let loggedInUserType = UserDefaults.standard.value(forKey: "userType") as? String
        {
            if (loggedInUserType == "instagramUser") ||  (loggedInUserType == "facebookUser")
            {
                self.otherChangePswrdView.isHidden = true
            }
            else
            {
                self.otherChangePswrdView.isHidden = false
            }
        }
        
        if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
        {
            if (checkPackageStatus == "packagePurchased")
            {
               self.myProfileVisibilityStatus = "profileVisibilityPurchased"
                
                if let descriptionPackage = UserDefaults.standard.value(forKey: "checkPurchasedPackageDetails") as?  String
                {
                    print("descriptionPackage = \(descriptionPackage)")
                }
            }
            else
            {
                self.myProfileVisibilityStatus = "profileVisibilityNotPurchased"
            }
        }
        else
        {
           self.myProfileVisibilityStatus = "profileVisibilityNotPurchased"
        }
    }
    
    func updateUserProfile()
    {
        if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
        {
            if (checkPackageStatus == "packagePurchased")
            {
                self.myProfileVisibilityStatus = "profileVisibilityPurchased"
                
                if let descriptionPackage = UserDefaults.standard.value(forKey: "checkPurchasedPackageDetails") as?  String
                {
                    print("descriptionPackage = \(descriptionPackage)")
                }
            }
            else
            {
                self.myProfileVisibilityStatus = "profileVisibilityNotPurchased"
            }
        }
        else
        {
            self.myProfileVisibilityStatus = "profileVisibilityNotPurchased"
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func getSavedVisibility()
    {
//        UserDefaults.standard.set(dataDict?.value(forKey: "location_sharing") as? String, forKey: "location_sharing")
        
        if let location_sharing = UserDefaults.standard.value(forKey: "location_sharing") as? String
        {
            print("location_sharing = \(location_sharing)")
            if (location_sharing == "on")
            {
                self.locationServicesSwitch.setOn(true, animated: true)
            }
            else
            {
                self.locationServicesSwitch.setOn(false, animated: true)
            }
        }
        else
        {
            self.locationServicesSwitch.setOn(true, animated: true)
            UserDefaults.standard.setValue("on", forKey: "location_sharing")
            UserDefaults.standard.synchronize()
        }
        
        if let wi_chat_room = UserDefaults.standard.value(forKey: "wi_chat_room") as? String
        {
            if (wi_chat_room == "on")
            {
                self.wiChatRoomSwitch.setOn(true, animated: true)
            }
            else
            {
                self.wiChatRoomSwitch.setOn(false, animated: true)
            }
        }
        else
        {
            self.wiChatRoomSwitch.setOn(true, animated: true)
            UserDefaults.standard.setValue("on", forKey: "wi_chat_room")
            UserDefaults.standard.synchronize()
        }
        
        if let post_likes = UserDefaults.standard.value(forKey: "post_likes") as? String
        {
            if (post_likes == "on")
            {
                self.events_swicth.setOn(true, animated: true)
            }
            else{
                self.events_swicth.setOn(false, animated: true)
            }
        }
        else
        {
            self.events_swicth.setOn(true, animated: true)
            UserDefaults.standard.setValue("on", forKey: "post_likes")
            UserDefaults.standard.synchronize()
        }
        
        if let new_matches = UserDefaults.standard.value(forKey: "new_matches") as? String
        {
            if (new_matches == "on")
            {
                self.newMatches_switch.setOn(true, animated: true)
            }
            else{
                self.newMatches_switch.setOn(false, animated: true)
            }
        }
        else
        {
            self.newMatches_switch.setOn(true, animated: true)
            UserDefaults.standard.setValue("on", forKey: "new_matches")
            UserDefaults.standard.synchronize()
        }
        
        if let messages = UserDefaults.standard.value(forKey: "messages") as? String
        {
            if (messages == "on")
            {
                self.messages_switch.setOn(true, animated: true)
            }
            else{
                self.messages_switch.setOn(false, animated: true)
            }
        }
        else
        {
            self.messages_switch.setOn(true, animated: true)
            UserDefaults.standard.setValue("on", forKey: "messages")
            UserDefaults.standard.synchronize()
        }
        
        if let comments = UserDefaults.standard.value(forKey: "comments") as? String
        {
            if (comments == "on")
            {
                self.commentsSwitch.setOn(true, animated: true)
            }
            else{
                self.commentsSwitch.setOn(false, animated: true)
            }
        }
        else
        {
            self.commentsSwitch.setOn(true, animated: true)
            UserDefaults.standard.setValue("on", forKey: "comments")
            UserDefaults.standard.synchronize()
        }
       
        if let profile_visitors = UserDefaults.standard.value(forKey: "profile_visitors") as? String
        {
            if (profile_visitors == "on")
            {
                self.profileVisitor_switch.setOn(true, animated: true)
            }
            else{
                self.profileVisitor_switch.setOn(false, animated: true)
            }
        }
        else
        {
            self.profileVisitor_switch.setOn(true, animated: true)
            UserDefaults.standard.setValue("on", forKey: "profile_visitors")
            UserDefaults.standard.synchronize()
        }
        
        if let profile_visibility = UserDefaults.standard.value(forKey: "profile_visibility") as? String
        {
            if (profile_visibility == "on")
            {
                self.profileVisibilitySwitch.setOn(true, animated: true)
            }
            else{
                self.profileVisibilitySwitch.setOn(false, animated: true)
            }
        }
        else
        {
            self.profileVisibilitySwitch.setOn(true, animated: true)
            UserDefaults.standard.setValue("on", forKey: "profile_visibility")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    func updateSettings(notificationType: String, getVisibility: String, completion: @escaping (Bool) -> Swift.Void)
    {
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    ApiHandler.updateSettings(forUserID: userID, authToken: userToken, typeToBeUpdated: notificationType, isSetTo: getVisibility, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                            print("dataDict = \(String(describing: dataDict))")
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
//                            print("message = \(String(describing: message))")
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                             completion(false)
                        }
                    })
                }
            }
    }

    
    
    // MARK: - ****** FromSettingsScreen ******
    func FromSettingsScreen()
    {
        self.viewDidLoad()
    }
  
    
    // MARK: - ****** UnBlock Users Button Action. ******
    @IBAction func blockedUsers_btnAction(_ sender: Any) {
        let blockUsersScreen = self.storyboard?.instantiateViewController(withIdentifier: "BlockedUsersToUnblockViewController") as! BlockedUsersToUnblockViewController
        self.navigationController?.pushViewController(blockUsersScreen, animated: true)
    }
    
    
    // MARK: - ****** Logout Button Action. ******
    @IBAction func logout_BtnAction(_ sender: Any) {
        let actionSheetController = UIAlertController(title: "LogOut", message: "Do you really want to logout?", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Confirm Logout", style: .default) { action -> Void in
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
               
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            case .online(.wwan) , .online(.wiFi):
                
                self.view.isUserInteractionEnabled = false
                self.loadingView.isHidden = false
                
                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
//                    print("userID = \(userID)")
                    ApiHandler.logOutUser(userSavedID: userID, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
                            for cookie in cookieJar.cookies! as [HTTPCookie]
                            {
                                NSLog("cookie.domain = %@", cookie.domain)
                                if cookie.domain == "www.instagram.com" ||
                                    cookie.domain == "api.instagram.com"
                                {
                                    cookieJar.deleteCookie(cookie)
                                }
                            }
                            
                            FIRMessaging.messaging().unsubscribe(fromTopic: "chatroom")
                            UserDefaults.standard.set("off", forKey: "wi_chat_room")
                            UserDefaults.standard.synchronize()
                            
                            if let firebase_user_id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                            {
                                let childRef = FIRDatabase.database().reference()
                                var dataDict = [String : Any]()
                                dataDict["myDeviceToken"] = ""
                                dataDict["userStatus"] = "offline"
                                childRef.child("Users").child(firebase_user_id).updateChildValues(dataDict)
                            }
                            
                            UserDefaults.standard.set(nil, forKey: "auth_token")
                            UserDefaults.standard.set(nil, forKey: "userPassword")
                            UserDefaults.standard.set(nil, forKey: "VibesUsers")
                            UserDefaults.standard.set(nil, forKey: "LinkUpUsers")
                            UserDefaults.standard.set(nil, forKey: "checkVibesUser")
                            UserDefaults.standard.set(nil, forKey: "checkLinkUpUser")
                            UserDefaults.standard.set(nil, forKey: "UserName")
                            UserDefaults.standard.set(nil, forKey: "Age")
                            UserDefaults.standard.set(nil, forKey: "Location")
                            
                            UserDefaults.standard.set(nil, forKey: "profile_pic_thumb")
                            UserDefaults.standard.set(nil, forKey: "userProfilePicture")
                            UserDefaults.standard.set(nil, forKey: "Gender")
                            UserDefaults.standard.set(false, forKey: "spotlightPurchased")
                            UserDefaults.standard.set(nil, forKey: "userProfilePicture")
                            UserDefaults.standard.set("", forKey: "userType")
                            UserDefaults.standard.set(nil,forKey: "UserAuthID")
                            UserDefaults.standard.set(nil, forKey: "firebase_user_id")
                            UserDefaults.standard.set(nil,forKey: "userDetailsDict")
                            UserDefaults.standard.set(nil, forKey: "spotlightPurchased")
                            UserDefaults.standard.set(0, forKey: "messagesCount")
                            UserDefaults.standard.set(nil, forKey: "reauthenticateUserEmail")
                            UserDefaults.standard.set(nil, forKey: "reauthenticatePassword")
                            UserDefaults.standard.synchronize()
                            
                            let loginManager: FBSDKLoginManager = FBSDKLoginManager()
                            loginManager.logOut()
                            FBSDKAccessToken.setCurrent(nil)
                            FBSDKProfile.setCurrent(nil)
                            
                            let homeScreen = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.navigationController?.pushViewController(homeScreen, animated: true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
//                            print("message = \(String(describing: message))")
                            
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                action in
                                
                                if (message == "No user found")
                                {
                                    let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
                                    for cookie in cookieJar.cookies! as [HTTPCookie]
                                    {
                                        NSLog("cookie.domain = %@", cookie.domain)
                                        if cookie.domain == "www.instagram.com" ||
                                            cookie.domain == "api.instagram.com"
                                        {
                                            cookieJar.deleteCookie(cookie)
                                        }
                                    }
                                    
                                    FIRMessaging.messaging().unsubscribe(fromTopic: "chatroom")
                                    UserDefaults.standard.set("off", forKey: "wi_chat_room")
                                    UserDefaults.standard.synchronize()
                                    
                                    if let firebase_user_id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                                    {
                                        let childRef = FIRDatabase.database().reference()
                                        var dataDict = [String : Any]()
                                        dataDict["myDeviceToken"] = ""
                                        dataDict["userStatus"] = "offline"
                                        childRef.child("Users").child(firebase_user_id).updateChildValues(dataDict)
                                    }
                                    
                                    UserDefaults.standard.set(nil, forKey: "auth_token")
                                    UserDefaults.standard.set(nil, forKey: "userPassword")
                                    UserDefaults.standard.set(nil, forKey: "VibesUsers")
                                    UserDefaults.standard.set(nil, forKey: "LinkUpUsers")
                                    UserDefaults.standard.set(nil, forKey: "checkVibesUser")
                                    UserDefaults.standard.set(nil, forKey: "checkLinkUpUser")
                                    UserDefaults.standard.set(nil, forKey: "UserName")
                                    UserDefaults.standard.set(nil, forKey: "Age")
                                    UserDefaults.standard.set(nil, forKey: "Location")
                                    UserDefaults.standard.set(nil, forKey: "profile_pic_thumb")
                                    UserDefaults.standard.set(nil, forKey: "userProfilePicture")
                                    UserDefaults.standard.set(nil, forKey: "Gender")
                                    UserDefaults.standard.set(false, forKey: "spotlightPurchased")
                                    UserDefaults.standard.set(nil, forKey: "userProfilePicture")
                                    UserDefaults.standard.set("", forKey: "userType")
                                    UserDefaults.standard.set(nil,forKey: "UserAuthID")
                                    UserDefaults.standard.set(nil,forKey: "userDetailsDict")
                                    UserDefaults.standard.set(nil, forKey: "spotlightPurchased")
                                    UserDefaults.standard.set(0, forKey: "messagesCount")
                                    UserDefaults.standard.set(nil, forKey: "reauthenticateUserEmail")
                                    UserDefaults.standard.set(nil, forKey: "reauthenticatePassword")
                                    UserDefaults.standard.synchronize()
                                    
                                    let loginManager: FBSDKLoginManager = FBSDKLoginManager()
                                    loginManager.logOut()
                                    FBSDKAccessToken.setCurrent(nil)
                                    FBSDKProfile.setCurrent(nil)
                                    
                                    let homeScreen = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    self.navigationController?.pushViewController(homeScreen, animated: true)
                                }
                                
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
                else
                {
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                }
            }
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    
    // MARK: - ****** UIScrollView Delegate. ******
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.wrapperScrollView.isScrollEnabled=true
        self.wrapperScrollView.contentSize.height = self.profileWrapperView.frame.origin.y + self.profileWrapperView.frame.size.height + 100
    }
    
    
     // MARK: - ****** viewDidDisappear ******
    override func viewDidDisappear(_ animated: Bool) {
     
    }
    
    
     // MARK: - ****** Back Button Action ******
    @IBAction func backBtn_action(_ sender: Any)
    {
        if self.changePasswordBool == true
        {
            UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: { _ in
                self.changePasswordView.isHidden = true
                self.navTitle_lbl.text = "Settings"
                self.oldPassword_txtField.resignFirstResponder()
                self.newPassword_txtField.resignFirstResponder()
                self.confirmPassword_txtField.resignFirstResponder()
            }, completion: nil)
            self.changePasswordBool = false
        }
        else if self.webViewBool == true
        {
            UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                self.privacyPolicy_webView.isHidden = true
                self.navTitle_lbl.text = "Settings"
            }, completion: nil)
            self.webViewBool = false
        }
        else
        {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    
     // MARK: - ****** Change Password Option Button Action ******
    @IBAction func changePassword_btnAction(_ sender: Any) {
        self.changePasswordBool = true
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromTop, animations: { _ in
          
            self.changePasswordView.isHidden = false
            self.navTitle_lbl.text = "Change Password"
            
        }, completion: nil)
    }
    
    
    // MARK: - ****** Update Password Button Action ******
    @IBAction func updatePassword_btnAction(_ sender: Any) {
        
        if (self.oldPassword_txtField.text == "") && (self.newPassword_txtField.text == "") && (self.confirmPassword_txtField.text == "")
        {
            let alert = UIAlertController(title: "", message: "All Fields are mandatory.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.oldPassword_txtField.text == ""
        {
            let alert = UIAlertController(title: "", message: "Please enter your Old Password.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.newPassword_txtField.text == ""
        {
            let alert = UIAlertController(title: "", message: "Please enter your New Password.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.confirmPassword_txtField.text == ""
        {
            let alert = UIAlertController(title: "", message: "Please enter your Confirm Password.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if self.oldPassword_txtField.text != self.savedMyPassword
        {
            let alert = UIAlertController(title: "", message: "Please enter correct old password.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if (self.newPassword_txtField.text) != (self.confirmPassword_txtField.text)
        {
            let alert = UIAlertController(title: "", message: "Password doesn't Match.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            case .online(.wwan) , .online(.wiFi):
               
                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                    {
                        if let savedPassword = UserDefaults.standard.value(forKey: "passwordSaved") as? String
                        {
                            print("savedPassword = \(savedPassword)")
                            
                            self.view.isUserInteractionEnabled = false
                            self.loadingView.isHidden  = false
                            ApiHandler.ChangePassword(userID: userID, authToken: userToken, new_password: self.newPassword_txtField.text!, current_password: self.oldPassword_txtField.text!, completion: { (responseeData) in
//                                print("responseeData = \(responseeData)")
                               
                                if (responseeData.value(forKey: "status") as? String == "200")
                                {
                                    var title = ""
                                    var message = ""
                                    
                                    title = "Success!"
                                    message = "Your password has been changed"
                                    self.oldPassword_txtField.text = ""
                                    self.newPassword_txtField.text = ""
                                    self.confirmPassword_txtField.text = ""
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                        action in
                                        
                                        UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: { _ in
                                            self.changePasswordView.isHidden = true
                                            self.navTitle_lbl.text = "Settings"
                                            self.oldPassword_txtField.resignFirstResponder()
                                            self.newPassword_txtField.resignFirstResponder()
                                            self.confirmPassword_txtField.resignFirstResponder()
                                        }, completion: nil)
                                        self.changePasswordBool = false
                                        
                                        
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else if (responseeData.value(forKey: "status") as? String == "400")
                                {
                                    let message = responseeData.value(forKey: "message") as? String
//                                    print("message = \(String(describing: message))")
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else
                                {
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            })
                        }
                    }
                }
                
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    
    // MARK: - ****** Location Services Switch Button Action ******
    @IBAction func locationServicesSwitch_btnAction(_ sender: Any) {
        print("locationServicesSwitch_btnAction")
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            if self.locationServicesSwitch.isOn == false
            {
                print("locationServicesSwitch_btnAction is Off")
                
                self.updateSettings(notificationType: "location_sharing", getVisibility: "off", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("off", forKey: "location_sharing")
                        UserDefaults.standard.synchronize()
                        self.locationServicesSwitch.setOn(false, animated: true)
                        self.getSavedVisibility()
                    }
                })
            }
            else
            {
                print("locationServicesSwitch_btnAction is On")
                
                self.updateSettings(notificationType: "location_sharing", getVisibility: "on", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("on", forKey: "location_sharing")
                        UserDefaults.standard.synchronize()
                        self.locationServicesSwitch.setOn(true, animated: true)
                        self.getSavedVisibility()
                    }
                })
            }
        }
    }
    
    
    // MARK: - ****** Profile Visibility Switch Button Action ******
    @IBAction func profileVisibilitySwitch_btnAction(_ sender: Any) {
        
        if self.myProfileVisibilityStatus == "profileVisibilityNotPurchased"
        {
            self.profileVisibilitySwitch.setOn(false, animated: true)
            let alert = UIAlertController(title: "", message: "Control your visibility by purchasing an all inclusive package", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "CreditsPackageViewController") as! CreditsPackageViewController
                 messagesScreen.typeOfCreditStr = "FromSettingsScreen"
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
               
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            case .online(.wwan) , .online(.wiFi):
               
                if self.profileVisibilitySwitch.isOn == false
                {
                    self.updateSettings(notificationType: "profile_visibility", getVisibility: "off", completion: { (response) in
                        
                        if (response == true)
                        {
                            UserDefaults.standard.set("off", forKey: "profile_visibility")
                            UserDefaults.standard.synchronize()
                            self.profileVisibilitySwitch.setOn(false, animated: true)
                            
                            if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                            {
                                var dataDict = [String : Any]()
                                dataDict["Profile_Visibility"] = "Off"
                                FIRDatabase.database().reference().child("Users").child(userID).child("Settings").child("General").updateChildValues(dataDict)
                            }
                            
                            self.getSavedVisibility()
                        }
                    })
                }
                else
                {
                    self.updateSettings(notificationType: "profile_visibility", getVisibility: "on", completion: { (response) in
                        
                        if (response == true)
                        {
                            UserDefaults.standard.set("on", forKey: "profile_visibility")
                            UserDefaults.standard.synchronize()
                            self.profileVisibilitySwitch.setOn(true, animated: true)
                            
                            if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                            {
                                var dataDict = [String : Any]()
                                dataDict["Profile_Visibility"] = "On"
                                FIRDatabase.database().reference().child("Users").child(userID).child("Settings").child("General").updateChildValues(dataDict)
                            }
                            
                            self.getSavedVisibility()
                        }
                    })
                }
            }
        }
    }
    
    
    // MARK: - ****** Profile Visibility Button Action ******
    @IBAction func profileVisibility_action(_ sender: Any)
    {
        if self.myProfileVisibilityStatus == "profileVisibilityNotPurchased"
        {
            let alert = UIAlertController(title: "", message: "Control your visibility by purchasing an all inclusive package", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "CreditsPackageViewController") as! CreditsPackageViewController
                 messagesScreen.typeOfCreditStr = "FromSettingsScreen"
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            case .online(.wwan) , .online(.wiFi):
              
                if self.profileVisibilitySwitch.isOn == false
                {
                    self.updateSettings(notificationType: "profile_visibility", getVisibility: "off", completion: { (response) in
                        
                        if (response == true)
                        {
                            UserDefaults.standard.set("off", forKey: "profile_visibility")
                            UserDefaults.standard.synchronize()
                            self.profileVisibilitySwitch.setOn(false, animated: true)
                            
                            if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                            {
                                var dataDict = [String : Any]()
                                dataDict["Profile_Visibility"] = "Off"
                                FIRDatabase.database().reference().child("Users").child(userID).child("Settings").child("General").updateChildValues(dataDict)
                            }
                            
                            self.getSavedVisibility()
                        }
                    })
                }
                else
                {
                    self.updateSettings(notificationType: "profile_visibility", getVisibility: "on", completion: { (response) in
                        
                        if (response == true)
                        {
                            UserDefaults.standard.set("on", forKey: "profile_visibility")
                            UserDefaults.standard.synchronize()
                            self.profileVisibilitySwitch.setOn(true, animated: true)
                            
                            if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                            {
                                var dataDict = [String : Any]()
                                dataDict["Profile_Visibility"] = "On"
                                FIRDatabase.database().reference().child("Users").child(userID).child("Settings").child("General").updateChildValues(dataDict)
                            }
                            
                            self.getSavedVisibility()
                        }
                    })
                }
           }
        }
    }
    
    
    // MARK: - ****** Profile Visitors Switch Button Action ******
    @IBAction func profileVisitor_switchAction(_ sender: Any) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):

                if self.profileVisitor_switch.isOn == false
                {
                    self.updateSettings(notificationType: "profile_visitors", getVisibility: "off", completion: { (response) in
                        
                        if (response == true)
                        {
                            UserDefaults.standard.set("off", forKey: "profile_visitors")
                            UserDefaults.standard.synchronize()
                            self.profileVisitor_switch.setOn(false, animated: true)
                            self.getSavedVisibility()
                        }
                    })
                }
                else
                {
                    self.updateSettings(notificationType: "profile_visitors", getVisibility: "on", completion: { (response) in
                        
                        if (response == true)
                        {
                            UserDefaults.standard.set("on", forKey: "profile_visitors")
                            UserDefaults.standard.synchronize()
                            self.profileVisitor_switch.setOn(true, animated: true)
                            self.getSavedVisibility()
                        }
                    })
                }
        }
    }
    
    
    // MARK: - ****** New Matches Button Action ******
    @IBAction func newMessages_switchAction(_ sender: Any)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            if self.newMatches_switch.isOn == false
            {
                self.updateSettings(notificationType: "new_matches", getVisibility: "off", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("off", forKey: "new_matches")
                        UserDefaults.standard.synchronize()
                        self.newMatches_switch.setOn(false, animated: true)
                        self.getSavedVisibility()
                    }
                    
                })
            }
            else
            {
                self.updateSettings(notificationType: "new_matches", getVisibility: "on", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("on", forKey: "new_matches")
                        UserDefaults.standard.synchronize()
                        self.newMatches_switch.setOn(true, animated: true)
                        self.getSavedVisibility()
                    }
                })
            }
        }
    }
    
    
    // MARK: - ****** Events Switch Button Action ******
    @IBAction func events_switchAction(_ sender: Any)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            if self.events_swicth.isOn == false
            {
                self.updateSettings(notificationType: "post_likes", getVisibility: "off", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("off", forKey: "post_likes")
                        UserDefaults.standard.synchronize()
                        self.events_swicth.setOn(false, animated: true)
                        self.getSavedVisibility()
                    }
                })
            }
            else
            {
                self.updateSettings(notificationType: "post_likes", getVisibility: "on", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("on", forKey: "post_likes")
                        UserDefaults.standard.synchronize()
                        self.events_swicth.setOn(true, animated: true)
                        self.getSavedVisibility()
                    }
                })
            }
        }
    }
    
    
    // MARK: - ****** Messages Switch Button Action ******
    @IBAction func messages_switchAction(_ sender: Any)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
           
            if self.messages_switch.isOn == false
            {
                self.updateSettings(notificationType: "messages", getVisibility: "off", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("off", forKey: "messages")
                        UserDefaults.standard.synchronize()
                        self.messages_switch.setOn(false, animated: true)
                        
                        if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                        {
                            var dataDict = [String : Any]()
                            dataDict["Messages"] = "Off"
                            FIRDatabase.database().reference().child("Users").child(userID).child("Settings").child("Notification").updateChildValues(dataDict)
                        }
                        
                        self.getSavedVisibility()
                    }
                })
            }
            else
            {
                self.updateSettings(notificationType: "messages", getVisibility: "on", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("on", forKey: "messages")
                        UserDefaults.standard.synchronize()
                        self.messages_switch.setOn(true, animated: true)
                        
                        if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                        {
                            var dataDict = [String : Any]()
                            dataDict["Messages"] = "On"
                            FIRDatabase.database().reference().child("Users").child(userID).child("Settings").child("Notification").updateChildValues(dataDict)
                        }
                        
                        self.getSavedVisibility()
                    }
                })
            }
        }
    }
    
    
     // MARK: - ****** Comments Switch Button Action ******
    @IBAction func Comments_switchAction(_ sender: Any) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            if self.commentsSwitch.isOn == false
            {
                self.updateSettings(notificationType: "comments", getVisibility: "off", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("off", forKey: "comments")
                        UserDefaults.standard.synchronize()
                        self.commentsSwitch.setOn(false, animated: true)
                        self.getSavedVisibility()
                    }
                })
            }
            else
            {
                self.updateSettings(notificationType: "comments", getVisibility: "on", completion: { (response) in
                    
                    if (response == true)
                    {
                        UserDefaults.standard.set("on", forKey: "comments")
                        UserDefaults.standard.synchronize()
                        self.commentsSwitch.setOn(true, animated: true)
                        self.getSavedVisibility()
                    }
                })
            }
        }
    }
    
     // MARK: - ****** Wi Chat Room Switch Button Action ******
    @IBAction func wiChatRoom_switchAction(_ sender: Any) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            if self.wiChatRoomSwitch.isOn == false
            {
                self.updateSettings(notificationType: "wi_chat_room", getVisibility: "off", completion: { (response) in
                    
                    if (response == true)
                    {
                        FIRMessaging.messaging().unsubscribe(fromTopic: "chatroom")
                        UserDefaults.standard.set("off", forKey: "wi_chat_room")
                        UserDefaults.standard.synchronize()
                        self.wiChatRoomSwitch.setOn(false, animated: true)
                        
                        if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                        {
                            var dataDict = [String : Any]()
                            dataDict["Wi_Chat_Room"] = "Off"
                            FIRDatabase.database().reference().child("Users").child(userID).child("Settings").child("Notification").updateChildValues(dataDict)
                        }
                        
                        self.getSavedVisibility()
                    }
                })
            }
            else
            {
                self.updateSettings(notificationType: "wi_chat_room", getVisibility: "on", completion: { (response) in
                    
                    if (response == true)
                    {
                        FIRMessaging.messaging().subscribe(toTopic: "chatroom")
                        UserDefaults.standard.set("on", forKey: "wi_chat_room")
                        UserDefaults.standard.synchronize()
                        self.wiChatRoomSwitch.setOn(true, animated: true)
                        
                        if let userID = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                        {
                            var dataDict = [String : Any]()
                            dataDict["Wi_Chat_Room"] = "On"
                            FIRDatabase.database().reference().child("Users").child(userID).child("Settings").child("Notification").updateChildValues(dataDict)
                        }
                        
                        self.getSavedVisibility()
                    }
                })
            }
        }
    }
    
    
    // MARK: - ****** Delete Profile Button Action ******
    @IBAction func deleteProfile_btnAction(_ sender: Any)
    {
//        if let currentUserID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
//        {
                    let actionSheetController = UIAlertController(title: "Delete Profile", message: "Do you really want to delete your profile?", preferredStyle: .actionSheet)
                    
                    let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                    }
                    actionSheetController.addAction(cancelActionButton)
                    
                    let deleteActionButton = UIAlertAction(title: "Confirm Delete", style: .default) { action -> Void in
                        
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        case .online(.wwan) , .online(.wiFi):
                          
                            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                            {
                                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                                {
                                    self.view.isUserInteractionEnabled = false
                                    self.loadingView.isHidden  = false
                                    
                                    ApiHandler.DeleteProfile(userID: userID, authToken: userToken, completion: { (responseData) in
//                                        print("responseData = \(responseData)")
                                        
                                        if (responseData.value(forKey: "status") as? String == "200")
                                        {
                                            let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
                                            for cookie in cookieJar.cookies! as [HTTPCookie]
                                            {
                                                NSLog("cookie.domain = %@", cookie.domain)
                                                if cookie.domain == "www.instagram.com" ||
                                                    cookie.domain == "api.instagram.com"
                                                {
                                                    cookieJar.deleteCookie(cookie)
                                                }
                                            }
                                            FIRMessaging.messaging().unsubscribe(fromTopic: "chatroom")
                                            UserDefaults.standard.set("off", forKey: "wi_chat_room")
                                            UserDefaults.standard.synchronize()
                                            
                                            if let firebase_user_id = UserDefaults.standard.value(forKey: "firebase_user_id") as? String
                                            {
                                                let childRef = FIRDatabase.database().reference()
                                                childRef.child("Users").child(firebase_user_id).setValue(nil)
                                            }
                                            
                                           UserDefaults.standard.set(nil, forKey: "profile_pic_thumb")
                                            UserDefaults.standard.set(nil, forKey: "firebase_user_id")
                                            UserDefaults.standard.set(nil, forKey: "paginationID")
                                            UserDefaults.standard.set(nil, forKey: "userPassword")
                                            UserDefaults.standard.set(nil, forKey: "VibesUsers")
                                            UserDefaults.standard.set(nil, forKey: "LinkUpUsers")
                                            UserDefaults.standard.set(nil, forKey: "checkVibesUser")
                                            UserDefaults.standard.set(nil, forKey: "checkLinkUpUser")
                                            UserDefaults.standard.set(nil, forKey: "UserName")
                                            UserDefaults.standard.set(nil, forKey: "Age")
                                            UserDefaults.standard.set(nil, forKey: "Location")
                                            UserDefaults.standard.set(nil, forKey: "userProfilePicture")
                                            UserDefaults.standard.set(nil, forKey: "Gender")
                                            UserDefaults.standard.set(false, forKey: "spotlightPurchased")
                                            UserDefaults.standard.set(nil, forKey: "userProfilePicture")
                                            UserDefaults.standard.set("", forKey: "userType")
                                            UserDefaults.standard.set(nil, forKey: "firebase_user_id")
                                            UserDefaults.standard.set(nil,forKey: "UserAuthID")
                                            UserDefaults.standard.set(nil,forKey: "userDetailsDict")
                                            UserDefaults.standard.set(nil, forKey: "spotlightPurchased")
                                            UserDefaults.standard.set(nil, forKey: "reauthenticateUserEmail")
                                            UserDefaults.standard.set(nil, forKey: "reauthenticatePassword")
                                            UserDefaults.standard.synchronize()
                                            
                                            let loginManager: FBSDKLoginManager = FBSDKLoginManager()
                                            loginManager.logOut()
                                            FBSDKAccessToken.setCurrent(nil)
                                            FBSDKProfile.setCurrent(nil)
                                            
                                            let homeScreen = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                            self.navigationController?.pushViewController(homeScreen, animated: true)
                                        }
                                        else if (responseData.value(forKey: "status") as? String == "400")
                                        {
                                            let message = responseData.value(forKey: "message") as? String
//                                            print("message = \(String(describing: message))")
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    })
                                }
                            }
                        }
                    }
                    
                    actionSheetController.addAction(deleteActionButton)
                    self.present(actionSheetController, animated: true, completion: nil)
//        }
    }
    
    // MARK: - ****** TermsConditions_btnAction ******
    @IBAction func TermsConditions_btnAction(_ sender: Any) {
        self.webViewBool = true
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
            self.privacyPolicy_webView.isHidden = false
            self.navTitle_lbl.text = "Terms & Conditions"
        }, completion: nil)
        
        let setURL=URL(string:"https://wimatchapp.com/terms-of-service")
        let setURLRequest=URLRequest(url:setURL!)
        self.privacyPolicy_webView.loadRequest(setURLRequest)
    }
    
    
    // MARK: - ****** safetyRules_btnAction ******
    @IBAction func safetyRules_btnAction(_ sender: Any) {
        self.webViewBool = true
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
            self.privacyPolicy_webView.isHidden = false
            self.navTitle_lbl.text = "Safety Rules"
        }, completion: nil)
        
        let setURL=URL(string:"https://wimatchapp.com/safety-policy")
        let setURLRequest=URLRequest(url:setURL!)
        self.privacyPolicy_webView.loadRequest(setURLRequest)
    }
    
    
    // MARK: - ****** FAQs_ActionBtn. ******
    @IBAction func FAQs_ActionBtn(_ sender: Any) {
        self.webViewBool = true
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
            self.privacyPolicy_webView.isHidden = false
            self.navTitle_lbl.text = "FAQs"
        }, completion: nil)
        
        let setURL=URL(string:"https://wimatchapp.com/faq")
        let setURLRequest=URLRequest(url:setURL!)
        self.privacyPolicy_webView.loadRequest(setURLRequest)
    }
    
    // MARK: - ****** wimatchInstructions. ******
    @IBAction func wimatchInstructions(_ sender: Any) {
        self.webViewBool = true
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
            self.privacyPolicy_webView.isHidden = false
            self.navTitle_lbl.text = "Wi Match Instructional"
        }, completion: nil)
        
        let setURL=URL(string:"https://youtu.be/bAaKj_c2Zoc")
        let setURLRequest=URLRequest(url:setURL!)
        self.privacyPolicy_webView.loadRequest(setURLRequest)
        
        self.privacyPolicy_webView.mediaPlaybackRequiresUserAction = false
        self.privacyPolicy_webView.allowsInlineMediaPlayback = false
    }
    
    
    // MARK: - ****** Privacy Policy Btn Action. ******
    @IBAction func privacyPolicy_btnAction(_ sender: Any) {
         self.webViewBool = true
         UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
         self.privacyPolicy_webView.isHidden = false
         self.navTitle_lbl.text = "Privacy Policy"
         }, completion: nil)
         
         let setURL=URL(string:"https://wimatchapp.com/privacy-policy")
         let setURLRequest=URLRequest(url:setURL!)
         self.privacyPolicy_webView.loadRequest(setURLRequest)
    }
    
    
    // MARK: - ****** UIWebView Delegates ******
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        self.loadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
      self.loadingView.isHidden = true
      self.view.isUserInteractionEnabled = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
//        print("error occurs in webview = \(error)")
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
    }

    // MARK: - ****** UITextField Delegate ******
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    // MARK: - ****** didReceiveMemoryWarning ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
