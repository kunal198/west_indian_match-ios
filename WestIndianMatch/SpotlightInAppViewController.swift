//
//  SpotlightInAppViewController.swift
//  Wi Match
//
//  Created by brst on 18/09/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import SDWebImage
import StoreKit
import Firebase

class SpotlightInAppViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SKProductsRequestDelegate,SKPaymentTransactionObserver,UIScrollViewDelegate {
    
    //MARK:- ***** VARIABLES DECLARATION *****
    let tenToken_ID = "com.wimatch.tenTokens"
    let fiveToken_ID = "com.wimatch.fiveTokens"
    let oneToken_ID = "com.wimatch.oneToken"
    var productID = ""
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    
    var webViewBool = Bool()
    var selectBtnOnIndex = Int()
    
    
    var oneTokenStr = String()
    var fiveTokenStr = String()
    var tenTokenStr = String()
    var totalNumberOfTokensPurchased = Int()
    
    var tokenOnePurchasedCount = Int()
    var tokenFivePurchasedCount = Int()
    var tokenTenPurchasedCount = Int()
    
    var oneTokenPurchasedTime = String()
    var fiveTokenPurchasedTime = String()
    var tenTokenPurchasedTime = String()
    var placeMeHereTimeStr = String()
    var spotlightStatusStr = String()
    var tokenAvailableInt = Int()
    var userId = String()
    
    var checkSpotlightEndStr = String()
    
    var noOfTokenArray = [String]()
    var priceOfTokenArray = [String]()
    var tokenHeadingArray = [String]()
    
    var getTokensArray = NSArray()
    var idofSelectedPackage = String()
    
    
    //MARK:- ***** OUTLETS DECLARATION *****
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var totalTokensAvailable_Lbl: UILabel!
    @IBOutlet weak var spotight_tableView: UITableView!
    @IBOutlet weak var usersImage: UIImageView!
    @IBOutlet weak var termsAndPolicyLbl: FRHyperLabel!
    @IBOutlet weak var termsInAppView: UIView!
    @IBOutlet weak var InfoTokenTextView: UITextView!
    @IBOutlet weak var costTitleLbl: UILabel!
    @IBOutlet weak var tokenTitleLbl: UILabel!
    @IBOutlet weak var wrapperInAppScroll: UIScrollView!
    @IBOutlet weak var wrapperInAppInfo: UIView!
    @IBOutlet weak var termsWebView: UIWebView!
    @IBOutlet weak var titlePolicyLbl: UILabel!
    @IBOutlet weak var wrapperWebView: UIView!
    @IBOutlet weak var termsUnderPackageLbl: FRHyperLabel!
    @IBOutlet weak var termsViewInfo: UIView!
    @IBOutlet weak var tokenInfoTextView: UITextView!
    @IBOutlet weak var wrapperScrollPackage: UIScrollView!
    @IBOutlet weak var purchasePackageBtn: UIButton!
    @IBOutlet weak var closeInAppViewBtn: UIButton!
    
    
    //MARK:- ****** viewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let userId = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            self.userId = userId
        }
        
        if self.tokenAvailableInt > 0
        {
            self.totalTokensAvailable_Lbl.text = "Total Tokens Available : " +  "\(self.tokenAvailableInt)"
        }
        else{
            self.totalTokensAvailable_Lbl.text = "Total Tokens Available : " +  "\(0)"
        }
        
        webViewBool = false
        
        let normalText = " "
        let boldText = "terms of use and privacy policy"
        let attributedString = NSMutableAttributedString(string:normalText)
        let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15)]
        let boldString = NSMutableAttributedString(string:boldText, attributes:attrs)
        attributedString.append(boldString)
        
        self.termsUnderPackageLbl.attributedText = attributedString
        self.termsAndPolicyLbl.attributedText = attributedString
        
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            if substring == "terms of use"
            {
                self.webViewBool = true
                
                UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                    self.wrapperWebView.isHidden = false
                    self.titlePolicyLbl.text = "Terms of use"
                }, completion: nil)
                
                
                let setURL=URL(string:"https://wimatchapp.com/terms-of-service")
                let setURLRequest=URLRequest(url:setURL!)
                self.termsWebView.loadRequest(setURLRequest)
            }
            else if substring == "privacy policy"
            {
                self.webViewBool = true
                
                UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                    self.wrapperWebView.isHidden = false
                    self.titlePolicyLbl.text = "Privacy Policy"
                }, completion: nil)
     
                let setURL=URL(string:"https://wimatchapp.com/privacy-policy")
                let setURLRequest=URLRequest(url:setURL!)
                self.termsWebView.loadRequest(setURLRequest)
            }
        }
        
        self.termsAndPolicyLbl.setLinksForSubstrings(["terms of use" ,"privacy policy"], withLinkHandler: handler)
        self.termsUnderPackageLbl.setLinksForSubstrings(["terms of use" ,"privacy policy"], withLinkHandler: handler)
        
        
        self.oneTokenPurchasedTime = ""
        self.fiveTokenPurchasedTime = ""
        self.tenTokenPurchasedTime = ""
        self.placeMeHereTimeStr = ""
        self.spotlightStatusStr = ""
        self.checkSpotlightEndStr = ""
        
        self.noOfTokenArray = ["10","5","1"]
        self.priceOfTokenArray = ["$1.60 / Token","$1.80 / Token","$1.99 / Token"]
        self.tokenHeadingArray = ["tokens","tokens","token"]
        
        if ((UserDefaults.standard.value(forKey: "userType") == nil)  || (UserDefaults.standard.value(forKey: "userType") is NSNull) || (UserDefaults.standard.value(forKey: "userType") as! String == "" ))
        {
            self.usersImage.image = UIImage(named: "Default Icon")
        }
        else
        {
                if let profileImage = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                {
                    if let profileImageType = UserDefaults.standard.value(forKey: "profileImageType") as? String
                    {
                        if profileImageType == "image"
                        {
                            self.usersImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "Default Icon"))
                        }
                        else
                        {
      
                        }
                    }
                    else
                    {
                        self.usersImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "Default Icon"))
                    }
                    
                }
                else
                {
                    self.usersImage.image = UIImage(named: "Default Icon")
                }
        }
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            self.view.isUserInteractionEnabled = false
            self.loadingView.isHidden = false
            
            ApiHandler.getDefaultTokenPackages(completion: { (responseData) in
//                print("responseData = \(responseData)")
                
                self.fetchAvailableProducts()
                SKPaymentQueue.default().add(self)
                
                if (responseData.value(forKey: "status") as? String == "200")
                {
                    let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                    print("dataDict = \(String(describing: dataDict))")
                    
                    if (dataDict!["tokens"] != nil)
                    {
                        self.getTokensArray = (dataDict?.value(forKey: "tokens") as? NSArray)!
//                        print("self.getTokensArray = \(String(describing: self.getTokensArray))")
                    }
                    
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                }
                else if (responseData.value(forKey: "status") as? String == "400")
                {
                    let message = responseData.value(forKey: "message") as? String
//                    print("message = \(String(describing: message))")
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    self.getTokensArray = []
                    let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    self.getTokensArray = []
                    let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                self.view.isUserInteractionEnabled = true
                self.loadingView.isHidden = true
            })
        }
    }
    
    
    @IBAction func termsBackBtn(_ sender: Any) {
        
        if self.webViewBool == true
        {
            self.webViewBool = false
            UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { _ in
                self.wrapperWebView.isHidden = true
                self.titlePolicyLbl.text = " "
            }, completion: nil)
        }
    }
    
    @IBAction func closeInAppViewBtn(_ sender: Any) {
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromRight, animations: {_ in
            self.wrapperInAppInfo.isHidden = true
        }, completion: nil)
    }
    
    @IBAction func purchasePackBtn(_ sender: Any) {
        if self.selectBtnOnIndex == 0{
            
            if iapProducts.count >= 0
            {
                productID = tenToken_ID
                
                for index in 0..<self.iapProducts.count
                {
                    let firstProduct = iapProducts[index].productIdentifier
                    if firstProduct == tenToken_ID
                    {
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = false
                            self.spotight_tableView.isUserInteractionEnabled = false
                            self.purchaseMyProduct(product: self.iapProducts[index])
                        }
                    }
                }
            }
            else{
                let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else if self.selectBtnOnIndex == 1
        {
            if iapProducts.count >= 0
            {
                productID = fiveToken_ID
                for index in 0..<self.iapProducts.count
                {
                    let firstProduct = iapProducts[index].productIdentifier
                    if firstProduct == fiveToken_ID
                    {
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = false
                            self.spotight_tableView.isUserInteractionEnabled = false
                            self.purchaseMyProduct(product: self.iapProducts[index])
                        }
                    }
                }
            }
            else{
                let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else if self.selectBtnOnIndex == 2
        {
            if iapProducts.count >= 0
            {
                productID = oneToken_ID
                for index in 0..<self.iapProducts.count
                {
                    let firstProduct = iapProducts[index].productIdentifier
                    if firstProduct == oneToken_ID
                    {
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = false
                            self.spotight_tableView.isUserInteractionEnabled = false
                            self.purchaseMyProduct(product: self.iapProducts[index])
                        }
                    }
                }
            }
            else{
                let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else{
            let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.wrapperScrollPackage.isScrollEnabled=true
        self.spotight_tableView.isScrollEnabled = false
        
        self.spotight_tableView.frame =  CGRect(x: self.spotight_tableView.frame.origin.x , y:0, width: self.spotight_tableView.frame.size.width, height: self.spotight_tableView.contentSize.height)
        
        self.tokenInfoTextView.frame.origin.y = self.spotight_tableView.frame.origin.y + self.spotight_tableView.frame.size.height
        
        let fixedWidth_info = self.tokenInfoTextView.frame.size.width
        self.tokenInfoTextView.sizeThatFits(CGSize(width: fixedWidth_info, height: CGFloat.greatestFiniteMagnitude))
        let newSize_info = self.tokenInfoTextView.sizeThatFits(CGSize(width: fixedWidth_info, height: CGFloat.greatestFiniteMagnitude))
        var newFrame_info = self.tokenInfoTextView.frame
        newFrame_info.size = CGSize(width: max(newSize_info.width, fixedWidth_info), height: newSize_info.height)
        self.tokenInfoTextView.frame = newFrame_info;
        
        if (UIScreen.main.bounds.width == 320)
        {
            self.termsViewInfo.frame.origin.y = self.tokenInfoTextView.frame.origin.y + self.tokenInfoTextView.frame.size.height - 10
        }
        else if (UIScreen.main.bounds.width == 375){
            self.termsViewInfo.frame.origin.y = self.tokenInfoTextView.frame.origin.y + self.tokenInfoTextView.frame.size.height - 20
        }
        else{
            self.termsViewInfo.frame.origin.y = self.tokenInfoTextView.frame.origin.y + self.tokenInfoTextView.frame.size.height - 30
        }
        
        self.wrapperScrollPackage.contentSize.height = self.termsViewInfo.frame.origin.y + self.termsViewInfo.frame.size.height + 50

        let fixedWidth = self.InfoTokenTextView.frame.size.width
        self.InfoTokenTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = self.InfoTokenTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = self.InfoTokenTextView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        self.InfoTokenTextView.frame = newFrame;
        
        if (UIScreen.main.bounds.width == 320)
        {
            self.termsInAppView.frame.origin.y = self.InfoTokenTextView.frame.origin.y + self.InfoTokenTextView.frame.size.height - 10
        }
        else if (UIScreen.main.bounds.width == 375){
            self.termsInAppView.frame.origin.y = self.InfoTokenTextView.frame.origin.y + self.InfoTokenTextView.frame.size.height - 20
        }
        else{
            self.termsInAppView.frame.origin.y = self.InfoTokenTextView.frame.origin.y + self.InfoTokenTextView.frame.size.height - 30
        }
        
        self.purchasePackageBtn.frame.origin.y = self.termsInAppView.frame.origin.y + termsInAppView.frame.size.height + 20
        
        self.closeInAppViewBtn.frame.origin.y = self.purchasePackageBtn.frame.origin.y + self.purchasePackageBtn.frame.size.height + 20
        
        self.wrapperInAppScroll.contentSize.height = self.closeInAppViewBtn.frame.origin.y + self.closeInAppViewBtn.frame.size.height + 50
    }
    
    
    
    //MARK:- ****** viewWillAppear ******
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    
    //MARK:- ****** TIME DIFFERENCE FROM TIMESTAMP AND CURRENT TIME ******
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        /// GET TIMESTAMP IN UNIX
        let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
        
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        
        // Convert Timestamp to NSDate
//        let date22:NSDate = NSDate(timeIntervalSince1970: (Double(dateString)!)/1000)
//        var dateString = String()
        
        let nYears = timeDiff / (1000*60*60*24*365)
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        var timeMsg = ""
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd, MMMM yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
//            dateString = dateFormatter.string(from: date22 as Date)
//            print("formatted date is =  \(dateString)")
            timeMsg = "\(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            //Date formatting
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM, dd"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
//            dateString = dateFormatter.string(from: date22 as Date)
//            print("formatted date is =  \(dateString)")
            timeMsg = " \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            //Date formatting
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM, dd"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
//            let dateString = dateFormatter.string(from: date22 as Date)
//            print("formatted date is =  \(dateString)")
            
            timeMsg = " \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = " \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = " \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }

    
    // MARK: - ****** FETCH AVAILABLE IAP PRODUCTS ******
    func fetchAvailableProducts()  {
        // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects: oneToken_ID,fiveToken_ID,tenToken_ID)
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        productsRequest.delegate = self
        productsRequest.start()
        self.loadingView.isHidden = false
        self.spotight_tableView.isUserInteractionEnabled = false
    }
    
    // MARK: - ****** REQUEST IAP PRODUCTS ******
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        if (response.products.count > 0)
        {
//            print("response.products = \(response.products)")
            self.loadingView.isHidden = true
            self.spotight_tableView.isUserInteractionEnabled = true
            iapProducts = response.products
            for index in 0..<self.iapProducts.count
            {
                print("index = \(index)")
            }
        }
        else
        {
            self.loadingView.isHidden = true
            self.spotight_tableView.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "In-app purchases", message: "Products are not available yet.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - ****** MAKE PURCHASE OF A PRODUCT ******
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    func purchaseMyProduct(product: SKProduct) {
        
        if self.canMakePurchases() {
            
            DispatchQueue.main.async {
                let payment = SKPayment(product: product)
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().add(payment)
            }
            // IAP Purchases disabled on the Device
        } else {
            let alert = UIAlertController(title: "In-app purchases", message: "Purchases are disabled in your device!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - ****** ViewWillDisAppear. ******
    override func viewWillDisappear(_ animated: Bool) {
        SKPaymentQueue.default().remove(self)
    }
    
    // MARK:- ****** IAP PAYMENT QUEUE ******
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
    {
        for transaction:AnyObject in transactions
        {
            if let trans = transaction as? SKPaymentTransaction
            {
                switch trans.transactionState {
                    
                case .purchasing:
                    self.loadingView.isHidden = false
                    self.spotight_tableView.isUserInteractionEnabled = false
                    break
                    
                case .purchased:
                    
                    self.loadingView.isHidden = true
                    self.spotight_tableView.isUserInteractionEnabled = true
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
          
                    if productID == oneToken_ID
                    {
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                          
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        case .online(.wwan) , .online(.wiFi):
                          
//                            let date1 = NSDate()
//                            let currentTimeStamp = Int64(date1.timeIntervalSince1970 * 1000)
                            let dateFormatter : DateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let date = Date()
                            let dateString = dateFormatter.string(from: date)
//                            print("dateString = \(dateString)")
                        
                        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                        {
                            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                            {
                                self.loadingView.isHidden = false
                                self.view.isUserInteractionEnabled = false
                                
                                ApiHandler.purchaseTokens(userID: userID, authToken: userToken, tokenID: self.idofSelectedPackage, dateCreated: dateString, completion: { (responseData) in
                                    
//                                    print("responseData = \(responseData)")
                                    
                                    if (responseData.value(forKey: "status") as? String == "200")
                                    {
                                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                                        print("dataDict = \(String(describing: dataDict))")
                                        
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        
                                        let message = responseData.value(forKey: "message") as? String
//                                        print("message = \(String(describing: message))")
                                        
                                        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                            action in
                                            
                                            if (dataDict!["total_tokens"] != nil)
                                            {
                                                self.tokenAvailableInt = (dataDict!.value(forKey: "total_tokens") as? Int)!
                                            }
                                            
                                            if self.tokenAvailableInt > 0
                                            {
                                                self.totalTokensAvailable_Lbl.text = "Total Tokens Available : " +  "\(self.tokenAvailableInt)"
                                            }
                                            else{
                                                self.totalTokensAvailable_Lbl.text = "Total Tokens Available : " +  "\(0)"
                                            }
                                            
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userBlocked"), object: nil)
                                            
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            self.spotight_tableView.isUserInteractionEnabled = true
                                            self.spotight_tableView.reloadData()
                                            
                                        }))
                                        self.present(alert, animated: true, completion: nil)
                                        
                                    }
                                    else if (responseData.value(forKey: "status") as? String == "400")
                                    {
                                        let message = responseData.value(forKey: "message") as? String
//                                        print("message = \(String(describing: message))")
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                })
                             }
                           }
                        }
                    }
                    else if productID == fiveToken_ID
                    {
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        case .online(.wwan) , .online(.wiFi):
                          
//                            let date1 = NSDate()
//                            let currentTimeStamp = Int64(date1.timeIntervalSince1970 * 1000)
                            let dateFormatter : DateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let date = Date()
                            let dateString = dateFormatter.string(from: date)
                            
                            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                            {
                                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                                {
                                    self.loadingView.isHidden = false
                                    self.view.isUserInteractionEnabled = false
                                    
                                    ApiHandler.purchaseTokens(userID: userID, authToken: userToken, tokenID: self.idofSelectedPackage, dateCreated: dateString, completion: { (responseData) in
                                        
//                                        print("responseData = \(responseData)")
                                        
                                        if (responseData.value(forKey: "status") as? String == "200")
                                        {
                                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                                            print("dataDict = \(String(describing: dataDict))")
                                            
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            
                                            let message = responseData.value(forKey: "message") as? String
//                                            print("message = \(String(describing: message))")
                                            
                                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                                action in
                                                
                                                if (dataDict!["total_tokens"] != nil)
                                                {
                                                    self.tokenAvailableInt = (dataDict!.value(forKey: "total_tokens") as? Int)!
                                                }
                                                
                                                if self.tokenAvailableInt > 0
                                                {
                                                    self.totalTokensAvailable_Lbl.text = "Total Tokens Available : " +  "\(self.tokenAvailableInt)"
                                                }
                                                else{
                                                    self.totalTokensAvailable_Lbl.text = "Total Tokens Available : " +  "\(0)"
                                                }
                                                
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userBlocked"), object: nil)
                                                
                                                self.loadingView.isHidden = true
                                                self.view.isUserInteractionEnabled = true
                                                self.spotight_tableView.isUserInteractionEnabled = true
                                                self.spotight_tableView.reloadData()
                                                
                                            }))
                                            
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else if (responseData.value(forKey: "status") as? String == "400")
                                        {
                                            let message = responseData.value(forKey: "message") as? String
//                                            print("message = \(String(describing: message))")
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    })
                                }
                            }
                        }
                    }
                    else if productID == tenToken_ID
                    {
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        case .online(.wwan) , .online(.wiFi):
                           
//                            let date1 = NSDate()
//                            let currentTimeStamp = Int64(date1.timeIntervalSince1970 * 1000)
                            let dateFormatter : DateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let date = Date()
                            let dateString = dateFormatter.string(from: date)
                            
                            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                            {
                                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                                {
                                    self.loadingView.isHidden = false
                                    self.view.isUserInteractionEnabled = false
                                    
                                    ApiHandler.purchaseTokens(userID: userID, authToken: userToken, tokenID: self.idofSelectedPackage, dateCreated: dateString, completion: { (responseData) in
                                        
//                                        print("responseData = \(responseData)")
                                        
                                        if (responseData.value(forKey: "status") as? String == "200")
                                        {
                                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                                            print("dataDict = \(String(describing: dataDict))")
                                            
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            
                                            let message = responseData.value(forKey: "message") as? String
//                                            print("message = \(String(describing: message))")
                                            
                                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                                action in
                                                
                                                if (dataDict!["total_tokens"] != nil)
                                                {
                                                    self.tokenAvailableInt = (dataDict!.value(forKey: "total_tokens") as? Int)!
                                                }
                                                
                                                if self.tokenAvailableInt > 0
                                                {
                                                    self.totalTokensAvailable_Lbl.text = "Total Tokens Available : " +  "\(self.tokenAvailableInt)"
                                                }
                                                else
                                                {
                                                    self.totalTokensAvailable_Lbl.text = "Total Tokens Available : " +  "\(0)"
                                                }
                                                
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userBlocked"), object: nil)
                                                
                                                self.loadingView.isHidden = true
                                                self.view.isUserInteractionEnabled = true
                                                self.spotight_tableView.isUserInteractionEnabled = true
                                                self.spotight_tableView.reloadData()
                                            }))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else if (responseData.value(forKey: "status") as? String == "400")
                                        {
                                            let message = responseData.value(forKey: "message") as? String
//                                            print("message = \(String(describing: message))")
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    })
                                }
                            }
                        }
                    }
                    
                    break
                    
                case .failed:
                    
                    self.loadingView.isHidden = true
                    self.spotight_tableView.isUserInteractionEnabled = true
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
                    let alert = UIAlertController(title: "Transaction Cancelled", message: "Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    break
                    
                case .restored:
                    self.loadingView.isHidden = true
                    self.spotight_tableView.isUserInteractionEnabled = true
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                    
                case .deferred:
                    self.loadingView.isHidden = true
                    self.spotight_tableView.isUserInteractionEnabled = true
//                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                    
                default:
                    self.loadingView.isHidden = true
                    self.spotight_tableView.isUserInteractionEnabled = true
                    
                    break
                    
                }}}
    }
    
     public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue)
     {
        print("paymentQueueRestoreCompletedTransactionsFinished")
     }
    
    public func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error){
        print("restoreCompletedTransactionsFailedWithError")
    }
    
    
    // MARK: - ****** UITableView Delegate & Data Source Methods. ******
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.noOfTokenArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath)as! CreditPackagesTableViewCell
        
        spotight_tableView.separatorColor = UIColor.clear
        creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
        
        creditsCell.noOfToken_lbl.text = self.noOfTokenArray[indexPath.row]
        creditsCell.priceOfToken_lbl.text = self.priceOfTokenArray[indexPath.row]
        creditsCell.tokensHeading_lbl.text = self.tokenHeadingArray[indexPath.row]
        
        creditsCell.selectTypeOfTokenBtn.layer.cornerRadius = creditsCell.selectTypeOfTokenBtn.frame.size.height / 2
        creditsCell.selectTypeOfTokenBtn.tag = indexPath.row
        creditsCell.selectTypeOfTokenBtn.addTarget(self, action: #selector(SpotlightInAppViewController.buyNow(sender:)), for: UIControlEvents.touchUpInside)
        
        creditsCell.tokenInformationBtn.tag = indexPath.row
        creditsCell.tokenInformationBtn.addTarget(self, action: #selector(SpotlightInAppViewController.getInformationForInApp(sender:)), for: UIControlEvents.touchUpInside)
        
        if indexPath.row == 0
        {
            creditsCell.selectTypeOfTokenBtn.setTitle("Select", for: .normal)
        }
        
        if indexPath.row == 1
        {
            creditsCell.selectTypeOfTokenBtn.setTitle("Select", for: .normal)
        }
        
        if indexPath.row == 2
        {
                creditsCell.selectTypeOfTokenBtn.setTitle("Select", for: .normal)
        }
        
        return creditsCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    // MARK: - ****** Buy Now In App Purchase Button Action. ******
    func buyNow(sender: UIButton!) {
        
        self.selectBtnOnIndex = sender.tag
        
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { _ in
            self.wrapperInAppInfo.isHidden = false
        }, completion: nil)
        
        if (sender.tag == 0)
        {
            self.tokenTitleLbl.text = "10 Tokens"
            self.costTitleLbl.text = "10 Tokens for $15.99"
            
            if (self.getTokensArray.count > 0)
            {
                let packageDict = self.getTokensArray[sender.tag] as! NSDictionary
                if (packageDict["id"] != nil)
                {
                    self.idofSelectedPackage = packageDict.value(forKey: "id") as! String
                }
                else{
                    self.idofSelectedPackage = "1"
                }
            }
            else{
                self.idofSelectedPackage = "1"
            }
        }
        else if (sender.tag == 1)
        {
            self.tokenTitleLbl.text = "5 Tokens"
            self.costTitleLbl.text = "5 Tokens for $8.99"
            
            if (self.getTokensArray.count > 0)
            {
                let packageDict = self.getTokensArray[sender.tag] as! NSDictionary
                if (packageDict["id"] != nil)
                {
                    self.idofSelectedPackage = packageDict.value(forKey: "id") as! String
                }
                else{
                    self.idofSelectedPackage = "2"
                }
            }
            else{
                self.idofSelectedPackage = "2"
            }
        }
        else{
            self.tokenTitleLbl.text = "1 Token"
            self.costTitleLbl.text = "1 Token for $1.99"
            
            if (self.getTokensArray.count > 0)
            {
                let packageDict = self.getTokensArray[sender.tag] as! NSDictionary
                if (packageDict["id"] != nil)
                {
                    self.idofSelectedPackage = packageDict.value(forKey: "id") as! String
                }
                else{
                    self.idofSelectedPackage = "3"
                }
            }
            else{
                self.idofSelectedPackage = "3"
            }
        }
    }
    
    // MARK: - ****** Buy Now In App Purchase Button Action. ******
    func getInformationForInApp(sender: UIButton!) {
        self.selectBtnOnIndex = sender.tag
        
        UIView.transition(with: view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { _ in
            self.wrapperInAppInfo.isHidden = false
        }, completion: nil)
        
        if (sender.tag == 0)
        {
            self.tokenTitleLbl.text = "10 Tokens"
            self.costTitleLbl.text = "10 Tokens for $15.99"
        }
        else if (sender.tag == 1)
        {
            self.tokenTitleLbl.text = "5 Tokens"
            self.costTitleLbl.text = "5 Tokens for $8.99"
        }
        else{
            self.tokenTitleLbl.text = "1 Token"
            self.costTitleLbl.text = "1 Token for $1.99"
        }
    }
    
    
    //MARK:- ****** back_btnAction ******
    @IBAction func back_btnAction(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.getValueFromSpotlight = true
        self.navigationController!.popViewController(animated: true)
    }

    
    //MARK:- ****** didReceiveMemoryWarning ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
