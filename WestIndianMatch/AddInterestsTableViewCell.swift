//
//  AddInterestsTableViewCell.swift
//  WestIndianMatch
//
//  Created by brst on 28/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class AddInterestsTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var subCategory_btn: UIButton!
    @IBOutlet weak var subInterests_wwrapperView: UIView!
    @IBOutlet weak var dropDownImage: UIImageView!
    @IBOutlet weak var interests_titleLbl: UILabel!
    @IBOutlet weak var addInterests_titleImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
