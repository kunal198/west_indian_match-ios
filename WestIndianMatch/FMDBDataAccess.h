//
//  FMDBDataAccess.h
//  Chanda
//
//  Created by Mohammad Azam on 10/25/11.
//  Copyright (c) 2011 HighOnCoding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h" 
#import "FMResultSet.h" 
#import "Country.h"


@interface FMDBDataAccess : NSObject
{
    
}

@property (nonatomic,strong) NSString *databaseName;
@property (nonatomic,strong) NSString *databasePath;

-(NSMutableArray *) getCustomers;
-(NSMutableArray *) getStatesForCountrySelected:(NSString *)countryId;
//-(BOOL) insertCustomer:(Country *) customer;
//-(BOOL) updateCustomer:(Country *) customer; 

@end
