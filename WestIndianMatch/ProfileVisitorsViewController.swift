//
//  ProfileVisitorsViewController.swift
//  WestIndianMatch
//
//  Created by brst on 23/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import StoreKit
import Firebase
import SDWebImage

class profileVisitors
{
    var u_name: String
    var u_id: String
    var firebase_user_id: String
    var profilePic: String
    var location: String
    var gender: String
    var age: String
    var onlineStatus: String
    var timeStamp: Int
    var meetUpPreferences: String
    var auth_Token: String
    
    init(name: String, uID: String, profilePic: String, location: String, gender: String, age: String,onlineStatus: String, timeStamp: Int,meetUpPreferences: String,firebase_user_id: String, auth_Token: String) {
        self.u_name = name
        self.u_id = uID
        self.profilePic = profilePic
        self.location = location
        self.gender = gender
        self.age = age
        self.onlineStatus = onlineStatus
        self.timeStamp = timeStamp
        self.meetUpPreferences = meetUpPreferences
        self.firebase_user_id = firebase_user_id
        self.auth_Token = auth_Token
    }
}


class ProfileVisitorsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    //MARK:- ***** VARIABLES DECLARATION *****
    var getLastKeyValue = String()
    var profileVisitorsArray = NSMutableArray()
    var profileVisitorsCopyArray = NSMutableArray()
    var timeStampArray = [String]()
    var blockedUsersArray = NSMutableArray()
    var blockedByUserArray = NSMutableArray()
    var differentiateStr = String()
    var profileVisitorBool = Bool()
    var appDelegatePush = String()
    var viewProfileBool = Bool()
    let SPOTLIGHT_PRODUCT_ID = "com.wimatch.profileVisitors"
    var productID = ""
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    var reportedUsersByMeArray = NSMutableArray()
    
    var currentPage = Int()
    var TotalPages = Int()
    
    var newProfileVisitorsArray = [profileVisitors]()
    var newProfileVisitorsCopyArray = [profileVisitors]()
    var updateNotificationStr = String()
    var getTotalNumberVisitors = Int()
    
    //MARK:- ***** OUTLETS DECLARATION *****
    @IBOutlet weak var loadingImage: UIImageView!
    @IBOutlet weak var buybtnView: UIView!
    @IBOutlet weak var InnerViewForBlur: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var noDataFoundView: UIView!
    @IBOutlet weak var search_txtField: UITextField!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var buyProfileVisitorsView: UIView!
    @IBOutlet weak var profileVisitors_tableView: UITableView!
    
    var refreshControl: UIRefreshControl!
    

     // MARK: - ****** ViewDidLoad ******
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.viewProfileBool = false
        
        self.refreshControl = UIRefreshControl()
      /*  let attributes = [NSForegroundColorAttributeName: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)]
        let attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attributes)*/

        self.refreshControl.backgroundColor = UIColor.clear
        self.refreshControl.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.profileVisitors_tableView.addSubview(refreshControl)

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.visitorsCount = 0
        
        UIApplication.shared.isStatusBarHidden=false
        self.differentiateStr = "allProfileVisitors"
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.FromVisitorsScreen), name: NSNotification.Name(rawValue: "FromVisitorsScreen"), object: nil)
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileVisitorsViewController.updateUserProfile), name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileVisitorsViewController.userBlocked), name: NSNotification.Name(rawValue: "userBlockedFromMessages"), object: nil)

        self.buyProfileVisitorsView.isHidden = true
        self.profileVisitorBool = false
        self.searchView.isHidden = true
        self.profileVisitors_tableView.separatorColor = UIColor.clear
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        self.view.isUserInteractionEnabled = true
        self.noDataFoundView.isHidden = true
        self.search_txtField.attributedPlaceholder = NSAttributedString(string: "Search by name",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.white])
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
          
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            self.currentPage = 1
            self.newProfileVisitorsArray.removeAll()
            self.newProfileVisitorsArray = []
            self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: self.search_txtField.text!, completion: { (responseBool) in
//                print("responseBool = \(responseBool)")
                if (responseBool == true)
                {
                    if (self.newProfileVisitorsArray.count > 0)
                    {
                        DispatchQueue.main.async {
                            self.noDataFoundView.isHidden = true
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.profileVisitors_tableView.reloadData()
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.newProfileVisitorsArray = []
                        self.profileVisitors_tableView.isHidden = false
                        self.noDataFoundView.isHidden = false
                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })
                        self.profileVisitors_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.newProfileVisitorsArray = []
                    self.profileVisitors_tableView.isHidden = false
                    self.noDataFoundView.isHidden = false
                    self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                    })
                    self.profileVisitors_tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
        }
        
        if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
        {
            if (checkPackageStatus == "packagePurchased")
            {
                self.profileVisitorBool = false
                if let descriptionPackage = UserDefaults.standard.value(forKey: "checkPurchasedPackageDetails") as?  String
                {
                    print("descriptionPackage = \(descriptionPackage)")
                }
            }
            else
            {
                self.profileVisitorBool = true
            }
        }
        else
        {
            self.profileVisitorBool = true
        }
    }
    
    func userBlocked()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            self.currentPage = 1
            self.newProfileVisitorsArray = []
            self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: self.search_txtField.text!, completion: { (responseBool) in
//                print("responseBool = \(responseBool)")
                if (responseBool == true)
                {
                    if (self.newProfileVisitorsArray.count > 0)
                    {
                        DispatchQueue.main.async {
                            self.noDataFoundView.isHidden = true
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.profileVisitors_tableView.reloadData()
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.newProfileVisitorsArray = []
                        self.profileVisitors_tableView.isHidden = false
                        self.noDataFoundView.isHidden = false
                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })
                        self.profileVisitors_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.newProfileVisitorsArray = []
                    self.profileVisitors_tableView.isHidden = false
                    self.noDataFoundView.isHidden = false
                    self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                    })
                    
                    self.profileVisitors_tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
        }
    }
    
    func updateUserProfile()
    {
        if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
        {
            if (checkPackageStatus == "packagePurchased")
            {
                self.buyProfileVisitorsView.isHidden = true
                self.profileVisitorBool = false
                self.newProfileVisitorsArray = []
                self.updateNotificationStr = "updateNotificationStrCalled"
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .online(.wwan) , .online(.wiFi):
                    
                    self.currentPage = 1
                    self.newProfileVisitorsArray = []
                    self.buyProfileVisitorsView.isHidden = true
                    self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: self.search_txtField.text!, completion: { (responseBool) in
//                        print("responseBool = \(responseBool)")
                        if (responseBool == true)
                        {
                            if (self.newProfileVisitorsArray.count > 0)
                            {
                                DispatchQueue.main.async {
                                    self.noDataFoundView.isHidden = true
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.profileVisitors_tableView.reloadData()
                                }
                            }
                            else
                            {
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.newProfileVisitorsArray = []
                                self.profileVisitors_tableView.isHidden = false
                                self.noDataFoundView.isHidden = false
                                self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                    self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                }, completion: { (finished) in
                                })
                                self.profileVisitors_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.newProfileVisitorsArray = []
                            self.profileVisitors_tableView.isHidden = false
                            self.noDataFoundView.isHidden = false
                            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                            }, completion: { (finished) in
                            })
                            self.profileVisitors_tableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    })
                }
            }
            else
            {
                self.profileVisitorBool = true
            }
        }
        else
        {
            self.profileVisitorBool = true
        }
    }
    
    func updateBadgeValueForVisitors(completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                ApiHandler.updateBadgeCountValue(user_id: userID, auth_token: userToken, typeofBadgeCountToBeUpdated: "profilevisitor_badgecount", totalBadgeCount: 0, completion: { (responseData) in
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
//                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
   
                    }
                    else
                    {
                       print("badge value for wi visitors not updated")
                    }
                })
            }
        }
    }
    
    
    
    func getAllProfileVisitorsListing(getPagination: Int,searchTextForVisitors: String,completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                ApiHandler.getProfileVisitorsListing(userID: userID, authToken: userToken, search_text: searchTextForVisitors,paginationValue: getPagination, completion: { (responseData) in
                    
                    self.updateBadgeValueForVisitors(completion: { (result) in
                        
                    })
                    
                    if (self.updateNotificationStr == "updateNotificationStrCalled")
                    {
                        self.newProfileVisitorsArray = []
                    }
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        
                        if (dataDict!["meta"] != nil)
                        {
                            let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                            self.TotalPages = (metaDict?.value(forKey: "totalpages") as? Int)!
                        }
                        
                        if (dataDict!["list"] != nil)
                        {
                            let listingArray = dataDict?.value(forKey: "list") as! NSArray
                            
                            if (listingArray.count == 0)
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                completion(false)
                            }
                            
                            for index in 0..<listingArray.count
                            {
                                var albumDict = NSDictionary()
                                albumDict = listingArray[index] as! NSDictionary
                       
                                let u_id: String = albumDict.value(forKey: "id") as! String
                                let firebase_user_id: String = albumDict.value(forKey: "firebase_user_id") as! String
                                let u_age: String = albumDict.value(forKey: "age") as! String
                                let u_name: String = albumDict.value(forKey: "username") as! String
                                let u_location: String = albumDict.value(forKey: "location") as! String
                                let u_pic: String = albumDict.value(forKey: "profile_pic") as! String
                                let u_status: String = albumDict.value(forKey: "online_status") as! String
                                var u_gender: String = albumDict.value(forKey: "gender") as! String
                                let auth_Token: String = albumDict.value(forKey: "auth_token") as! String
                                
                                if (u_gender == "1")
                                {
                                    u_gender = "male"
                                }
                                else
                                {
                                    u_gender = "female"
                                }
                                
                                var meetUpPreferences = String()
                                if (albumDict["meet_up_text"] != nil)
                                {
                                  meetUpPreferences = albumDict.value(forKey: "meet_up_text") as! String
                                }
                                else{
                                    meetUpPreferences = ""
                                }
                                
                                var createdDate = String()
                                if (albumDict["created"] != nil)
                                {
                                    createdDate = albumDict.value(forKey: "created") as! String
                                }
                                else{
                                    createdDate = "1529476926"
                                }
                                
                                let userDetails = profileVisitors.init(name: u_name, uID: u_id, profilePic: u_pic, location: u_location, gender: u_gender, age: u_age, onlineStatus: u_status, timeStamp: Int(createdDate)!, meetUpPreferences: meetUpPreferences, firebase_user_id: firebase_user_id, auth_Token: auth_Token)
                                
                                self.newProfileVisitorsArray.append(userDetails)
                                
                            }
                            completion(true)
                        }
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
//                        print("message = \(String(describing: message))")
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.newProfileVisitorsArray = []
                        completion(false)
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.newProfileVisitorsArray = []
                        completion(false)
                    }
                })
            }
        }
    }
    

    func refresh(_ sender: Any) {
     
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            self.newProfileVisitorsArray = []
            self.currentPage = 1
            self.newProfileVisitorsArray.removeAll()
            self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: self.search_txtField.text!, completion: { (responseBool) in
//                print("responseBool = \(responseBool)")
                if (responseBool == true)
                {
                    if (self.newProfileVisitorsArray.count > 0)
                    {
                        DispatchQueue.main.async {
                            self.noDataFoundView.isHidden = true
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.profileVisitors_tableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.newProfileVisitorsArray = []
                        self.profileVisitors_tableView.isHidden = false
                        self.noDataFoundView.isHidden = false
                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })
                        self.profileVisitors_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.newProfileVisitorsArray = []
                    self.profileVisitors_tableView.isHidden = false
                    self.noDataFoundView.isHidden = false
                    self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                    })
                    self.profileVisitors_tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
        }
        
    }
    
    
    // MARK: - ****** FromVisitorsScreen ******
    func FromVisitorsScreen()
    {
//        self.viewDidLoad()
    }
  
    
     // MARK: - ****** Close Purchase View Button Action ******
    @IBAction func closePurchaseView_btnAction(_ sender: Any) {
            self.buyProfileVisitorsView.isHidden = true
    }

    
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        print("scrollView.contentOffset = \(scrollView.contentOffset.y)")
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if (self.profileVisitors_tableView.contentOffset.y >= (self.profileVisitors_tableView.contentSize.height - self.profileVisitors_tableView.frame.size.height) ) {
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                
                if self.currentPage < self.TotalPages {
                    let bottomEdge: Float = Float(self.profileVisitors_tableView.contentOffset.y + profileVisitors_tableView.frame.size.height)
                    if bottomEdge >= Float(profileVisitors_tableView.contentSize.height)  {
                        
                        print("we are at end of table")
                        
                        self.currentPage = self.currentPage + 1
                        
                        self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: self.search_txtField.text!, completion: { (responseBool) in
//                            print("responseBool = \(responseBool)")
                            if (responseBool == true)
                            {
                                if (self.newProfileVisitorsArray.count > 0)
                                {
                                    DispatchQueue.main.async {
                                        self.noDataFoundView.isHidden = true
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.profileVisitors_tableView.reloadData()
                                    }
                                }
                                else
                                {
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    /* self.newProfileVisitorsArray = []
                                     self.profileVisitors_tableView.isHidden = false
                                     self.noDataFoundView.isHidden = false
                                     self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                     UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                     self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                     }, completion: { (finished) in
                                     })*/
                                    self.profileVisitors_tableView.reloadData()
                                    self.refreshControl.endRefreshing()
                                }
                            }
                            else
                            {
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                /* self.newProfileVisitorsArray = []
                                 self.profileVisitors_tableView.isHidden = false
                                 self.noDataFoundView.isHidden = false
                                 self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                 UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                 self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                 }, completion: { (finished) in
                                 })*/
                                self.profileVisitors_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        })
                    }
                }
            }
        }
    }
 

     // MARK: - ****** Cancel Search Button Action ******
    @IBAction func searchCancel_btnAction(_ sender: Any)
    {
        self.searchView.isHidden = true
        self.search_txtField.resignFirstResponder()
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            self.currentPage = 1
            self.search_txtField.text = ""
            self.newProfileVisitorsArray.removeAll()
            self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: self.search_txtField.text!, completion: { (responseBool) in
//                print("responseBool = \(responseBool)")
                if (responseBool == true)
                {
                    if (self.newProfileVisitorsArray.count > 0)
                    {
                        DispatchQueue.main.async {
                            self.noDataFoundView.isHidden = true
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.profileVisitors_tableView.reloadData()
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.newProfileVisitorsArray = []
                        self.profileVisitors_tableView.isHidden = false
                        self.noDataFoundView.isHidden = false
                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })
                        self.profileVisitors_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.newProfileVisitorsArray = []
                    self.profileVisitors_tableView.isHidden = false
                    self.noDataFoundView.isHidden = false
                    self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                    })
                    self.profileVisitors_tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
        }
    }
    
    
     // MARK: - ****** In App Purchases Button Action ******
    @IBAction func buyNow_btnAction(_ sender: Any) {
        let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "CreditsPackageViewController") as! CreditsPackageViewController
        messagesScreen.typeOfCreditStr = "FromVisitorsScreen"
        self.navigationController?.pushViewController(messagesScreen, animated: true)
    }
    
    

     // MARK: - ****** Back button Action ******
    @IBAction func backBtn_action(_ sender: Any)
    {
        if appDelegatePush == "fromAppDelegate"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        else
        {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    
     // MARK: - ****** UITableView Delegates ******
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.newProfileVisitorsArray.count
    }
    

    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let visitorsCell = tableView.dequeueReusableCell(withIdentifier: "visitorsCell", for: indexPath)as! ProfileVisitorsTableViewCell
        
        profileVisitors_tableView.separatorColor = UIColor.clear
        visitorsCell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if (self.newProfileVisitorsArray.count <= 0)
        {
            return visitorsCell
        }
        else
        {
            if (self.newProfileVisitorsArray[indexPath.row].profilePic != "")
            {
                DispatchQueue.main.async {
                    visitorsCell.profileImage.image = nil
                    visitorsCell.profileImage.sd_setImage(with: URL(string: self.newProfileVisitorsArray[indexPath.row].profilePic), placeholderImage: UIImage(named: ""))
                }
            }
            else
            {
                DispatchQueue.main.async {
                    visitorsCell.profileImage.image = nil
                    visitorsCell.profileImage.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "Default Icon"))
                }
            }
            
            visitorsCell.profileImage.contentMode = UIViewContentMode.scaleAspectFill
            visitorsCell.profileImage.clipsToBounds = true
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            if self.profileVisitorBool == true
            {
                blurEffectView.frame = visitorsCell.profileImage.bounds
                visitorsCell.profileImage.addSubview(blurEffectView)
            }
            else{
                DispatchQueue.main.async {
                    for subview in visitorsCell.profileImage.subviews
                    {
                        if subview.isKind(of: UIVisualEffectView.self)
                        {
                            subview.removeFromSuperview()
                        }
                    }
                    
                    visitorsCell.profileImage.sd_setImage(with: URL(string: self.newProfileVisitorsArray[indexPath.row].profilePic), placeholderImage: UIImage(named: ""))
                }
            }
            
            let userName = self.newProfileVisitorsArray[indexPath.row].u_name
            let userAge = self.newProfileVisitorsArray[indexPath.row].age
            let userGender = self.newProfileVisitorsArray[indexPath.row].gender
            
            var genderStr = String()
            if userGender == "male"
            {
                genderStr = "Male"
            }
            else if userGender == "female"
            {
                genderStr = "Female"
            }
            
            if self.profileVisitorBool == true
            {
                visitorsCell.userName_lbl.text =  userAge + ", " +  genderStr
            }
            else{
                visitorsCell.userName_lbl.text = userName + ", " + userAge + ", " +  genderStr
            }
            
            visitorsCell.location_lbl.text = self.newProfileVisitorsArray[indexPath.row].location
            visitorsCell.location_lbl.adjustsFontSizeToFitWidth = true
       
            visitorsCell.visitorsMeetUp_Lbl.text = self.newProfileVisitorsArray[indexPath.row].meetUpPreferences
            
            let timeStampInt = self.newProfileVisitorsArray[indexPath.row].timeStamp
//            print("timeStampInt = \(String(describing: timeStampInt))")
            let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampInt))
//            print("datee = \(String(describing: datee))")
            let dateString = timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
            
            if dateString == ""
            {
                visitorsCell.daysAgo_lbl.text = ""
            }
            else
            {
                visitorsCell.daysAgo_lbl.text = dateString
            }
            
            visitorsCell.deleteVisitorBtn.tag = indexPath.row
            visitorsCell.deleteVisitorBtn.addTarget(self, action: #selector(ProfileVisitorsViewController.deleteVisitor), for: .touchUpInside)
            
            visitorsCell.blockVisitorBtn.tag = indexPath.row
            visitorsCell.blockVisitorBtn.addTarget(self, action: #selector(ProfileVisitorsViewController.blockVisitor), for: .touchUpInside)
            
            visitorsCell.MessageVisitorBtn.tag = indexPath.row
            visitorsCell.MessageVisitorBtn.addTarget(self, action: #selector(ProfileVisitorsViewController.messageVisitor(sender:)), for: .touchUpInside)
            
//            let userID = self.newProfileVisitorsArray[indexPath.row].u_id
            
            if (self.newProfileVisitorsArray[indexPath.row].onlineStatus == "1")
            {
                visitorsCell.onlineUserIcon.isHidden = false
                visitorsCell.onlineUserIcon.image = UIImage(named: "Online Icon")
            }
            else
            {
                visitorsCell.onlineUserIcon.isHidden = true
            }
 
            return visitorsCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.profileVisitorBool == true
        {
            self.buyProfileVisitorsView.isHidden = false
            self.buybtnView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.buybtnView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })
        }
        else
        {
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                
                self.viewProfileBool = true
                
                let userID = self.newProfileVisitorsArray[indexPath.row].u_id
               
                let authToken = self.newProfileVisitorsArray[indexPath.row].auth_Token
                
                self.viewProfileBool = true
                
                DispatchQueue.main.asyncAfter(deadline: .now())
                {
                    if self.viewProfileBool == true
                    {
                        self.viewProfileBool = false
                        let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                        viewProfileScreen.ownerID = userID
                        viewProfileScreen.ownerAuthToken = authToken
                        self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                }
            }
        }
    }
 
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) mon ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 mon ago"
            } else {
                return "Last mon"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "an hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) min ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "a min ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) sec ago"
        } else {
            return "just now"
        }
    }
    
    
    //MARK:- ****** TIME DIFFERENCE FROM TIMESTAMP AND CURRENT TIME ******
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        /// GET TIMESTAMP IN UNIX
        let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        
        let nYears = timeDiff / (1000*60*60*24*365)
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        var timeMsg = ""
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            timeMsg = " \(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            timeMsg = " \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            timeMsg = " \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = " \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = " \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

     // MARK: - ****** Delete Button Action ******
    func deleteVisitor(sender: UIButton!)
    {
        if self.profileVisitorBool == true
        {
            self.buyProfileVisitorsView.isHidden = false
            self.buybtnView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.buybtnView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })
        }
        else
        {
            let alert = UIAlertController(title: "", message: "Do you really want to delete a user?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .online(.wwan) , .online(.wiFi):
                    
                    if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                    {
                        if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                        {
                            self.loadingView.isHidden = false
                            self.view.isUserInteractionEnabled = false
                            
                            ApiHandler.deleteProfileVisiors(user_id: userID, auth_token: userToken, other_user_id: self.newProfileVisitorsArray[sender.tag].u_id, completion: { (responseData) in
                           
//                                print("responseData = \(responseData)")
                                
                                if (responseData.value(forKey: "status") as? String == "200")
                                {
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    
                                    let status = Reach().connectionStatus()
                                    switch status
                                    {
                                    case .unknown, .offline:
                                        
                                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    case .online(.wwan) , .online(.wiFi):
                                        
                                        self.currentPage =  1
                                        self.newProfileVisitorsArray.removeAll()
                                        self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: self.search_txtField.text!, completion: { (responseBool) in
//                                            print("responseBool = \(responseBool)")
                                            if (responseBool == true)
                                            {
                                                if (self.newProfileVisitorsArray.count > 0)
                                                {
                                                    DispatchQueue.main.async {
                                                        self.loadingView.isHidden = true
                                                        self.view.isUserInteractionEnabled = true
                                                        self.noDataFoundView.isHidden = true
                                                        self.profileVisitors_tableView.reloadData()
                                                    }
                                                }
                                                else
                                                {
                                                    self.loadingView.isHidden = true
                                                    self.view.isUserInteractionEnabled = true
                                                    self.newProfileVisitorsArray = []
                                                    self.profileVisitors_tableView.isHidden = false
                                                    self.noDataFoundView.isHidden = false
                                                    self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                                    }, completion: { (finished) in
                                                    })
                                                    self.profileVisitors_tableView.reloadData()
                                                    self.refreshControl.endRefreshing()
                                                }
                                            }
                                            else
                                            {
                                                self.loadingView.isHidden = true
                                                self.view.isUserInteractionEnabled = true
                                                self.newProfileVisitorsArray = []
                                                self.profileVisitors_tableView.isHidden = false
                                                self.noDataFoundView.isHidden = false
                                                self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                                    self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                                }, completion: { (finished) in
                                                })
                                                self.profileVisitors_tableView.reloadData()
                                                self.refreshControl.endRefreshing()
                                            }
                                        })
                                    }
                                }
                                else if (responseData.value(forKey: "status") as? String == "400")
                                {
                                    let message = responseData.value(forKey: "message") as? String
//                                    print("message = \(String(describing: message))")
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else
                                {
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            })
                        }
                    }
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
     // MARK: - ****** Block Button Action ******
    func blockVisitor(sender: UIButton!)
    {
        if self.profileVisitorBool == true
        {
            self.buyProfileVisitorsView.isHidden = false
            self.buybtnView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.buybtnView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })
            
        }
        else
        {
            let alert = UIAlertController(title: "", message: "Do you really want to block?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                   
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .online(.wwan) , .online(.wiFi):
                    
                    if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                    {
                        if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                        {
                            if self.newProfileVisitorsArray[sender.tag].u_id != ""
                            {
                                self.loadingView.isHidden = false
                                self.view.isUserInteractionEnabled = false
                                
                                ApiHandler.blockUnblockUser(userID: userID, authToken: userToken, other_user_id: self.newProfileVisitorsArray[sender.tag].u_id,typeOfBlockUser: "visitors", completion: { (responseData) in
                                    
//                                    print("responseData = \(responseData)")
                                    
                                    if (responseData.value(forKey: "status") as? String == "200")
                                    {
//                                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                                        print("dataDict = \(String(describing: dataDict))")
                                        
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        
                                        let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                            action in
                                            
                                            let status = Reach().connectionStatus()
                                            switch status
                                            {
                                            case .unknown, .offline:
                                                
                                                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                                self.present(alert, animated: true, completion: nil)
                                            case .online(.wwan) , .online(.wiFi):
                                                
                                                self.currentPage = 1
                                                self.newProfileVisitorsArray.removeAll()
                                                self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: self.search_txtField.text!, completion: { (responseBool) in
//                                                    print("responseBool = \(responseBool)")
                                                    if (responseBool == true)
                                                    {
                                                        if (self.newProfileVisitorsArray.count > 0)
                                                        {
                                                            DispatchQueue.main.async {
                                                                self.noDataFoundView.isHidden = true
                                                                self.loadingView.isHidden = true
                                                                self.view.isUserInteractionEnabled = true
                                                                self.profileVisitors_tableView.reloadData()
                                                            }
                                                        }
                                                        else
                                                        {
                                                            self.loadingView.isHidden = true
                                                            self.view.isUserInteractionEnabled = true
                                                            self.newProfileVisitorsArray = []
                                                            self.profileVisitors_tableView.isHidden = false
                                                            self.noDataFoundView.isHidden = false
                                                            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                                            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                                                self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                                            }, completion: { (finished) in
                                                            })
                                                            self.profileVisitors_tableView.reloadData()
                                                            self.refreshControl.endRefreshing()
                                                        }
                                                    }
                                                    else
                                                    {
                                                        self.loadingView.isHidden = true
                                                        self.view.isUserInteractionEnabled = true
                                                        self.newProfileVisitorsArray = []
                                                        self.profileVisitors_tableView.isHidden = false
                                                        self.noDataFoundView.isHidden = false
                                                        self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                                            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                                        }, completion: { (finished) in
                                                        })
                                                        self.profileVisitors_tableView.reloadData()
                                                        self.refreshControl.endRefreshing()
                                                    }
                                                })
                                            }
                                        }))
                                        
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else if (responseData.value(forKey: "status") as? String == "400")
                                    {
                                        let message = responseData.value(forKey: "message") as? String
//                                        print("message = \(String(describing: message))")
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                })
                            }
                        }
                    }
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }

    
    
     // MARK: - ****** Message Opponent Users ******
    func messageVisitor(sender: UIButton!)
    {
        if self.profileVisitorBool == true
        {
            self.buyProfileVisitorsView.isHidden = false
            self.buybtnView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.buybtnView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })
        }
        else
        {
            if (self.newProfileVisitorsArray.count > 0)
            {
                let userID =  self.newProfileVisitorsArray[sender.tag].u_id
                let firebase_user_id = self.newProfileVisitorsArray[sender.tag].firebase_user_id
                
                let profilePic = UIImageView()
                profilePic.sd_setImage(with: URL(string:self.newProfileVisitorsArray[sender.tag].profilePic), placeholderImage: UIImage(named: "Default Icon"))
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                messagesScreen.mainConfessionCheck = "comingFromViewProfile"
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromParticularUserScreen"
                messagesScreen.opponentName = self.newProfileVisitorsArray[sender.tag].u_name
                messagesScreen.opponentImage = self.newProfileVisitorsArray[sender.tag].profilePic
                messagesScreen.opponentUserID = userID
                messagesScreen.opponentFirebaseUserID = firebase_user_id
                messagesScreen.opponentProfilePic =  profilePic.image!
                messagesScreen.typeOfUser = "WithProfileUser"
                messagesScreen.checkVisibiltyStr = "FromViewProfile"
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }
        }
    }
    
    
     // MARK: - ****** Search Button Action ******
    @IBAction func search_BtnAction(_ sender: Any) {
        if self.profileVisitorBool == true
        {
            self.buyProfileVisitorsView.isHidden = false
            self.buybtnView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.buybtnView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })
        }
        else
        {
            self.search_txtField.text = ""
            self.searchView.isHidden = false
            self.search_txtField.becomeFirstResponder()
        }
    }
    
     // MARK: - ****** UITextField Delegate ******
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
           
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if self.search_txtField.text != ""
            {
                self.differentiateStr = "searchProfileVisitors"
//                print("text field is not blank")
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .online(.wwan) , .online(.wiFi):
                    
                    var search_txt = self.search_txtField.text
                    search_txt = search_txt?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
//                    print("search_txt = \(String(describing: search_txt))")
                    
                    self.currentPage = 1
                    self.newProfileVisitorsArray.removeAll()
                    self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: search_txt!, completion: { (responseBool) in
//                        print("responseBool = \(responseBool)")
                        if (responseBool == true)
                        {
                            if (self.newProfileVisitorsArray.count > 0)
                            {
                                DispatchQueue.main.async {
                                    self.noDataFoundView.isHidden = true
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.profileVisitors_tableView.reloadData()
                                }
                            }
                            else
                            {
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.newProfileVisitorsArray = []
                                self.profileVisitors_tableView.isHidden = false
                                self.noDataFoundView.isHidden = false
                                self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                    self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                }, completion: { (finished) in
                                })
                                self.profileVisitors_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.newProfileVisitorsArray = []
                            self.profileVisitors_tableView.isHidden = false
                            self.noDataFoundView.isHidden = false
                            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                            }, completion: { (finished) in
                            })
                            self.profileVisitors_tableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    })
                }
            }
            else
            {
                self.differentiateStr = "allProfileVisitors"
                self.searchView.isHidden = true
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .online(.wwan) , .online(.wiFi):
                    
                    self.currentPage = 1
                    self.newProfileVisitorsArray.removeAll()
                    self.getAllProfileVisitorsListing(getPagination: self.currentPage, searchTextForVisitors: self.search_txtField.text!, completion: { (responseBool) in
//                        print("responseBool = \(responseBool)")
                        if (responseBool == true)
                        {
                            if (self.newProfileVisitorsArray.count > 0)
                            {
                                DispatchQueue.main.async {
                                    self.noDataFoundView.isHidden = true
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.profileVisitors_tableView.reloadData()
                                }
                            }
                            else
                            {
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                self.newProfileVisitorsArray = []
                                self.profileVisitors_tableView.isHidden = false
                                self.noDataFoundView.isHidden = false
                                self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                    self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                }, completion: { (finished) in
                                })
                                self.profileVisitors_tableView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                        else
                        {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.newProfileVisitorsArray = []
                            self.profileVisitors_tableView.isHidden = false
                            self.noDataFoundView.isHidden = false
                            self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                self.noDataFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                            }, completion: { (finished) in
                            })
                            self.profileVisitors_tableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    })
                }
            }
        }
        
        return true
    }
    
    
     // MARK: - ****** Search with username ******
    func searchWith_UserName(_textFieldValue: String)
    {
        self.profileVisitorsArray = []
        self.newProfileVisitorsArray = []
        self.loadingView.isHidden = false
        self.view.isUserInteractionEnabled = false
        
        var lowercaseChars = _textFieldValue.lowercased()
        lowercaseChars = lowercaseChars.trimmingCharacters(in: CharacterSet.whitespaces)

      /*  var userID = String()
        if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            userID = id
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }*/
    }
    
    
     // MARK: - ****** Did Receive Memory Warning ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
