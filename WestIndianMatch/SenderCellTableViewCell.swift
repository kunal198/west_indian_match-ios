//
//  SenderCellTableViewCell.swift
//  Wi Match
//
//  Created by brst on 13/04/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import FLAnimatedImage

class SenderCellTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var bubbleSenderIcon: UIImageView!
    
    @IBOutlet weak var senderAudioCell: UISlider!
    @IBOutlet weak var senderIsRead_lbl: UILabel!
    @IBOutlet weak var senderViewImage_btn: UIButton!
    @IBOutlet weak var senderBackImage: UIImageView!
    @IBOutlet weak var senderDateTime_lbl: UILabel!
    @IBOutlet weak var senderMsg_txtView: UITextView!
    @IBOutlet weak var senderName_lbl: UILabel!
    @IBOutlet weak var senderImage: UIImageView!
    
    @IBOutlet weak var confessionImage: FLAnimatedImageView!
    @IBOutlet weak var audioImage: UIImageView!
    
    
    func clearCellData()  {
        self.senderMsg_txtView.text = nil
        self.senderMsg_txtView.isHidden = false
        self.senderBackImage.image = nil
        self.senderImage.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        changeImage("Bubble_Sent_Icon")
        if #available(iOS 11.0, *) {
            //            bubbleReceivedIcon.tintColor = UIColor(named: "chat_bubble_color_sent")
        } else {
            // Fallback on earlier versions
        }
    }
    
    func changeImage(_ name: String) {
        guard let image = UIImage(named: name) else { return }
        bubbleSenderIcon.image = image
            .resizableImage(withCapInsets:
                UIEdgeInsetsMake(17, 21, 17, 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
        bubbleSenderIcon.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0) // Blue Color Code Acc. to App's theme
        bubbleSenderIcon.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
