//
//  PhotosCollectionViewCell.swift
//  Wi Match
//
//  Created by brst on 09/03/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import FLAnimatedImage

class PhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoPlayIcon: UIImageView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var deletePhotoBtn: UIButton!
    
    
    @IBOutlet weak var giphyAnimatedImageView: FLAnimatedImageView!
    
    @IBOutlet weak var acitivityIndicator: UIActivityIndicatorView!
    
    func clearCellData()
    {
        self.photoImageView.image = nil
        self.videoPlayIcon.isHidden = true
    }
    
}
