//
//  CreditPackagesTableViewCell.swift
//  WestIndianMatch
//
//  Created by brst on 23/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import FLAnimatedImage

class CreditPackagesTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var userNameLbl_Y_Constraint: NSLayoutConstraint!
    
    @IBOutlet weak var locationIcon: UIImageView!
    
    @IBOutlet weak var tableWrapperView: UIView!
    @IBOutlet weak var getInformatinBtn: UIButton!
    @IBOutlet weak var buyNow_lbl: UILabel!
    @IBOutlet weak var buyNowBtn: UIButton!
    @IBOutlet weak var btnWrapperView: UIView!
    @IBOutlet weak var packageDesc_lbl: UILabel!
    @IBOutlet weak var creditPackage_lbl: UILabel!
    @IBOutlet weak var creditImage: UIImageView!
    @IBOutlet weak var imageWrapperView: UIView!
    
    @IBOutlet weak var tokenInformationBtn: UIButton!
    
    @IBOutlet weak var tokensHeading_lbl: UILabel!
    
    @IBOutlet weak var title_lbl: UILabel!
    
    @IBOutlet weak var noOfToken_lbl: UILabel!
    
    @IBOutlet weak var priceOfToken_lbl: UILabel!
    
    @IBOutlet weak var selectTypeOfTokenBtn: UIButton!
    
    @IBOutlet weak var activityIndicator_nearBy: UIActivityIndicatorView!
    
    @IBOutlet weak var activityIndicatorGif_nearBy: UIActivityIndicatorView!
    
    @IBOutlet weak var activityIndicator_popular: UIActivityIndicatorView!
    
    @IBOutlet weak var animatedGifHeight: NSLayoutConstraint!
    @IBOutlet weak var animatedImageHeight: NSLayoutConstraint!
    @IBOutlet weak var activityIndicatorGif_popular: UIActivityIndicatorView!
    
    @IBOutlet weak var activityIndicator_Latest: UIActivityIndicatorView!
    
    @IBOutlet weak var activityIndicatorGif_Latest: UIActivityIndicatorView!
    
    @IBOutlet weak var viewProfileBtn: UIButton!
    
    
    @IBOutlet weak var usersStatus_TxtView: UITextView!
    
    @IBOutlet weak var onlyGifImageAnimated: FLAnimatedImageView!
    // Wi Confessions Variables
    @IBOutlet weak var moodTypeIcon: UIImageView!
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var confessionMessageBtn: UIButton!
    @IBOutlet weak var confessionReplyBtn: UIButton!
    @IBOutlet weak var confessionLikesBtn: UIButton!
    @IBOutlet weak var userLocationLbl: UILabel!
    @IBOutlet weak var totalReplyCountLbl: UILabel!
    @IBOutlet weak var totalLikesCountLbl: UILabel!
//    @IBOutlet weak var userPostedPictureIcon: UIImageView!
    @IBOutlet weak var userStatusLbl: UILabel!
    @IBOutlet weak var userGenderAgeLbl: UILabel!
    
    @IBOutlet weak var imageViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var animatedGifImage: FLAnimatedImageView!
    @IBOutlet weak var viewFullImageBtn: UIButton!
    
    @IBOutlet weak var confessionReplyImage: UIImageView!
    @IBOutlet weak var confessionLikeIcon: UIImageView!
    @IBOutlet weak var decreaseTimerLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        moodTypeIcon.frame.size.width = 45
//        moodTypeIcon.frame.size.height = 45
//        moodTypeIcon.layer.cornerRadius = moodTypeIcon.frame.size.height/2
//        moodTypeIcon.clipsToBounds = true
//        moodTypeIcon.layer.masksToBounds = true
//        moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
//        moodTypeIcon.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
