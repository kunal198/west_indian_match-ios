//
//  OnlineUsersTableViewCell.swift
//  Wi Match
//
//  Created by brst on 05/05/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class OnlineUsersTableViewCell: UITableViewCell {

    @IBOutlet weak var usersProfilePic: UIImageView!
    @IBOutlet weak var sendMessageBtn: UIButton!
    @IBOutlet weak var usersLocation_lbl: UILabel!
    @IBOutlet weak var userName_lbl: UILabel!
    @IBOutlet weak var usersOnlineIcon: UIImageView!
    @IBOutlet weak var islookingFor_lbl: UILabel!
    @IBOutlet weak var superLikeIcon: UIImageView!
    
    @IBOutlet weak var usersNewMeetUpLbl: UILabel!
  
    @IBOutlet weak var hoursAgoVibesLbl: UILabel!
    @IBOutlet weak var hoursAgoLinkUpLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
