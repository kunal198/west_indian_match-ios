//
//  BlockedUsersToUnblockViewController.swift
//  Wi Match
//
//  Created by brst on 31/01/18.
//  Copyright © 2018 brst. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage


class BlockedUsersToUnblockViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var refreshControl: UIRefreshControl!
    var blockedUsersArray = NSMutableArray()
    var refreshBool = Bool()
    
    @IBOutlet weak var blockedUsers_tableView: UITableView!
    @IBOutlet weak var noUsersFoundView: UIView!
    @IBOutlet weak var loadingView: UIView!
    
    // MARK: - ****** viewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshBool = false
        self.blockedUsers_tableView.separatorColor = UIColor.clear
        
        self.refreshControl = UIRefreshControl()
        /*let attributes = [NSForegroundColorAttributeName: UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)]
        let attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attributes)*/
        self.refreshControl.backgroundColor = UIColor.clear
        self.refreshControl.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.blockedUsers_tableView.addSubview(refreshControl)

        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            self.blockUsersListing(completion: { (response) in
                if (response == true)
                {
                    if (self.blockedUsersArray.count > 0)
                    {
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.blockedUsers_tableView.reloadData()
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.blockedUsersArray = []
                        self.blockedUsers_tableView.isHidden = false
                        self.noUsersFoundView.isHidden = false
                        self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })
                        self.blockedUsers_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.blockedUsersArray = []
                    self.blockedUsers_tableView.isHidden = false
                    self.noUsersFoundView.isHidden = false
                    self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                    })
                    self.blockedUsers_tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
        }
    }
    
    func blockUsersListing(completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                ApiHandler.getBlockedUsersListing(userID: userID, authToken: userToken, completion: { (responseData) in
                    
//                    print("responseData = \(responseData)")
                    self.blockedUsersArray = []
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        
                        if (dataDict!["list"] != nil)
                        {
                            let listingArray = dataDict?.value(forKey: "list") as! NSArray
                            
                            for index in 0..<listingArray.count
                            {
//                                print("listingArray[index] = \(listingArray[index])")
                                
                                var albumDict = NSDictionary()
                                albumDict = listingArray[index] as! NSDictionary
                                
                                if (albumDict["blokedprofile"] != nil)
                                {
                                    if let blokedprofileDict = albumDict.value(forKey: "blokedprofile") as? NSDictionary
                                    {
                                        if !(self.blockedUsersArray.contains(blokedprofileDict))
                                        {
                                            self.blockedUsersArray.add(blokedprofileDict)
                                        }
                                    }
                                }
                            }
                        }
                       completion(true)
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
//                        print("message = \(String(describing: message))")
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.blockedUsersArray = []
                        completion(false)
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.blockedUsersArray = []
                        completion(false)
                    }
                })
            }
        }
    }
    
    func refresh(_ sender: Any) {
        
        self.refreshBool = true
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            self.blockUsersListing(completion: { (response) in
                if (response == true)
                {
                    if (self.blockedUsersArray.count > 0)
                    {
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            self.blockedUsers_tableView.reloadData()
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.blockedUsersArray = []
                        self.blockedUsers_tableView.isHidden = false
                        self.noUsersFoundView.isHidden = false
                        self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })
                        self.blockedUsers_tableView.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.blockedUsersArray = []
                    self.blockedUsers_tableView.isHidden = false
                    self.noUsersFoundView.isHidden = false
                    self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { (finished) in
                    })
                    self.blockedUsers_tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
        }
        
    }
    
    // MARK: - ****** viewDidDisappear ******
    override func viewDidDisappear(_ animated: Bool) {
       
        
    }
    
   
    // MARK: - ****** back_btnAction ******
    @IBAction func back_btnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - ****** UITableView Delegates ******
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.blockedUsersArray.count
    }
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let visitorsCell = tableView.dequeueReusableCell(withIdentifier: "unblockUsersCell", for: indexPath)as! ProfileVisitorsTableViewCell
        
        self.blockedUsers_tableView.separatorColor = UIColor.clear
        
        visitorsCell.selectionStyle = UITableViewCellSelectionStyle.none
        
        visitorsCell.unblockProfileImage.sd_setImage(with: URL(string:((self.blockedUsersArray[indexPath.row]) as AnyObject).value(forKey: "profile_pic") as! String), placeholderImage: UIImage(named: ""))
        visitorsCell.unblockProfileImage.contentMode = UIViewContentMode.scaleAspectFill
        visitorsCell.unblockProfileImage.clipsToBounds = true
  
        let userAge = ((self.blockedUsersArray[indexPath.row]) as AnyObject).value(forKey: "age") as? String
        let userGender = ((self.blockedUsersArray[indexPath.row]) as AnyObject).value(forKey: "gender") as? String
        
        var genderStr = String()
        if userGender == "1"
        {
            genderStr = "Male"
        }
        else if userGender == "2"
        {
            genderStr = "Female"
        }
        
        visitorsCell.unblockUserName.text = ((self.blockedUsersArray[indexPath.row]) as AnyObject).value(forKey: "username") as? String

        visitorsCell.unblockUsergender.text = userAge! + ", " +  genderStr
        
        visitorsCell.unblockUserLocation.text = ((self.blockedUsersArray[indexPath.row]) as AnyObject).value(forKey: "location") as? String
        visitorsCell.unblockUserLocation.adjustsFontSizeToFitWidth = true
        
        visitorsCell.unblockBtnAction.tag = indexPath.row
        visitorsCell.unblockBtnAction.addTarget(self, action: #selector(BlockedUsersToUnblockViewController.unblockBtnAction), for: .touchUpInside)

        return visitorsCell
    }
    
    // MARK: - ****** unblockBtnAction ******
    func unblockBtnAction(sender: UIButton!)
    {
//        print("unblockBtnAction = \(sender.tag)")
        
        if (self.blockedUsersArray.count <= 0)
        {
            return
        }
        else
        {
            var unblockUserID = String()
            unblockUserID = (((self.blockedUsersArray[sender.tag]) as AnyObject).value(forKey: "id") as? String)!
            
            var typeOfBlockedUser = String()
            typeOfBlockedUser = (((self.blockedUsersArray[sender.tag]) as AnyObject).value(forKey: "blocked_type") as? String)!
            
            if (typeOfBlockedUser == "")
            {
                typeOfBlockedUser = "profile"
            }
            
            let alert = UIAlertController(title: "", message: "Do you really want to unblock?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .online(.wwan) , .online(.wiFi):
                    
                    self.unblockBlockedUser(ownerIDToUnblock: unblockUserID,type: typeOfBlockedUser, completion: { (responseBool) in
                        if (responseBool == true)
                        {
                            DispatchQueue.main.async {
                                
                                self.blockedUsersArray = []
                                
                                self.blockUsersListing(completion: { (response) in
                                    if (response == true)
                                    {
                                        if (self.blockedUsersArray.count > 0)
                                        {
//                                            print("self.blockedUsersArray = \(self.blockedUsersArray)")
                                            DispatchQueue.main.async {
                                                self.loadingView.isHidden = true
                                                self.view.isUserInteractionEnabled = true
                                                self.blockedUsers_tableView.reloadData()
                                            }
                                        }
                                        else
                                        {
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            self.blockedUsersArray = []
                                            self.blockedUsers_tableView.isHidden = false
                                            self.noUsersFoundView.isHidden = false
                                            self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                                self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                            }, completion: { (finished) in
                                            })
                                            self.blockedUsers_tableView.reloadData()
                                            self.refreshControl.endRefreshing()
                                        }
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.blockedUsersArray = []
                                        self.blockedUsers_tableView.isHidden = false
                                        self.noUsersFoundView.isHidden = false
                                        self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                            self.noUsersFoundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                        }, completion: { (finished) in
                                        })
                                        self.blockedUsers_tableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                    }
                                })
                            }
                        }
                    })
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func unblockBlockedUser(ownerIDToUnblock: String,type: String,completion : @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                if ownerIDToUnblock != ""
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    ApiHandler.blockUnblockUser(userID: userID, authToken: userToken, other_user_id: ownerIDToUnblock, typeOfBlockUser: type, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
//                            print("dataDict = \(String(describing: dataDict))")
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
//                            print("message = \(String(describing: message))")
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    // MARK: - ****** didReceiveMemoryWarning ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
