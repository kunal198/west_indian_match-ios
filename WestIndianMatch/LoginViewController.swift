//
//  LoginViewController.swift
//  WestIndianMatch
//
//  Created by brst on 22/02/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import FBSDKCoreKit
import CoreLocation
import AVFoundation

class LoginViewController: UIViewController,CLLocationManagerDelegate//,FBSDKLoginButtonDelegate
{
    // MARK: - ****** Variables ******
    var facebookDetailsArray = NSMutableDictionary()
    var facebookImageURL = String()
    var facebookUserName = String()
    var facebookUserEmail = String()
    var facebookUserID = String()
    var locationManager: CLLocationManager!
    var currentLatitude = Double()
    var currentLongitude = Double()
    var countryStr = String()
    var currentLocationCountry = String()
    var city = String()
    var state = String()
    var localityStr = String()
    var subLocalityStr = String()
    
    var getConversationStr = String()
    var getConversationAnonymousStr = String()
    
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var paused: Bool = false

    // MARK: - ****** Outlets ******
    @IBOutlet weak var wrapperViewButtons: UIView!
    @IBOutlet weak var instagramBtnView: UIView!
    @IBOutlet weak var register_btn: UIButton!
    @IBOutlet weak var signIn_btn: UIButton!
    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var signInView: UIView!
    @IBOutlet weak var loginWithInsta_btn: UIButton!
    @IBOutlet weak var signIn_Lbl: UILabel!
    @IBOutlet weak var westIndianMatch_lbl: UILabel!
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var facelookloginBtn: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var loadingImage: UIImageView!
    
    // MARK: - ****** ViewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let theURL = Bundle.main.url(forResource:"loginVideo", withExtension: "mp4") else{
            print("loginVideo.m4v not found")
            return
        }
    
            self.avPlayer = AVPlayer(url: theURL)
            self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
            self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            self.avPlayer.actionAtItemEnd = .none
            
            self.avPlayerLayer.frame = self.view.layer.bounds
            self.view.backgroundColor = .clear
//        view.layer.insertSublayer(avPlayerLayer, at: 0)
            self.backgroundImage.layer.addSublayer(self.avPlayerLayer)
   
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer.currentItem)


        UIApplication.shared.isStatusBarHidden=false
        self.navigationController?.isNavigationBarHidden = true
        
        registerView.layer.borderWidth = 2
        registerView.layer.masksToBounds=false
        registerView.layer.borderColor = UIColor.init(red: 49.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
        
        signInView.layer.borderWidth = 2
        signInView.layer.masksToBounds=false
        signInView.layer.borderColor = UIColor.init(red: 49.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
        
        
        instagramBtnView.layer.borderWidth = 2.0
        instagramBtnView.layer.masksToBounds = false
        instagramBtnView.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
        instagramBtnView.layer.cornerRadius = 13
        instagramBtnView.layer.cornerRadius = instagramBtnView.frame.size.height/2
        
        loginWithInsta_btn.layer.cornerRadius = loginWithInsta_btn.frame.size.height/2
        
        facebookView.layer.borderWidth = 2.0
        facebookView.layer.masksToBounds = false
        facebookView.layer.borderColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0).cgColor
        facebookView.layer.cornerRadius = 13
        facebookView.layer.cornerRadius = facebookView.frame.size.height/2
        
        facelookloginBtn.layer.cornerRadius = facelookloginBtn.frame.size.height/2
        
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    
    func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: kCMTimeZero)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let theAudioPlayer =  self.avPlayer {
            theAudioPlayer.play()
            paused = false
        }
        
//        avPlayer.play()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if (paused == false)
        {
            if let theAudioPlayer =  self.avPlayer {
                if #available(iOS 10.0, *) {
                    if (theAudioPlayer.timeControlStatus == .playing) {
                        theAudioPlayer.pause()
                        paused = true
                    }
                } else {
                    // Fallback on earlier versions
                }
            }
            
            /*if #available(iOS 10.0, *) {
                if (avPlayer.timeControlStatus == .playing)
                {
                    avPlayer.pause()
                    paused = true
                }
            } else {
                // Fallback on earlier versions
            }*/
        }
    }
    
    //MARK:- ****** didChangeAuthorization ******
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
            
        case .notDetermined , .denied , .restricted :
            
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
            }
            
            break
            
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
       /* default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
    }
    
    
    // MARK: - ****** didUpdateLocations ******
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.locationManager.location != nil
        {
            if let getCoordinates = manager.location?.coordinate
            {
                print("getCoordinates = \(getCoordinates)")
                
                let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.locationManager.stopUpdatingLocation()
                
                if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                    let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in // this is the last line that is being called
                        var placemark : CLPlacemark!
                        placemark = placemarks?[0]
                        if placemark != nil
                        {
                            if (placemark.addressDictionary?["Country"] != nil)
                            {
                                self.currentLocationCountry = (placemark.addressDictionary?["Country"] as! String)
                            }
                            
                            var city = String()
                            if (placemark.addressDictionary?["City"] != nil)
                            {
                                city = (placemark.addressDictionary?["City"] as! String)
                            }
                            
                            var state = String()
                            if (placemark.addressDictionary?["State"] != nil)
                            {
                                state = (placemark.addressDictionary?["State"] as! String)
                            }
                            
                            self.city = city
                            self.state = state
                            
                            var vv = String()
                            if (placemark.subLocality != nil)
                            {
                                vv =  placemark.subLocality!
                            }
                            
                            var countWhiteSpaces = Int()
                            if (vv.range(of: " ") != nil)
                            {
                                countWhiteSpaces = (vv.components(separatedBy: " ").count) - 1
                            }
                            else
                            {
                                countWhiteSpaces = 0
                            }
                            
                            var objAtIndexBeforeLast = String()
                            var objAtIndexLast = String()
                            
                            if (countWhiteSpaces > 0)
                            {
                                var fullNameArr = vv.components(separatedBy: " ")
                                objAtIndexBeforeLast = (fullNameArr[(fullNameArr.count) - 2])
                                objAtIndexLast = (fullNameArr.last)!
                                
                                self.subLocalityStr = objAtIndexBeforeLast + " " + objAtIndexLast
                            }
                            else
                            {
                                self.subLocalityStr = vv
                            }
                            
                            if (placemark.locality != nil)
                            {
                                self.localityStr = placemark.locality!
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    // MARK: - ****** ViewWillAppear ******
    override func viewWillAppear(_ animated: Bool)
    {
        UIApplication.shared.isStatusBarHidden=false
        self.navigationController?.isNavigationBarHidden = true

    }
    
    
    // MARK: - ****** Get Network Status Reachibility Method ******
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        let getNetworkStatus = (userInfo?["Status"] as? String)!
        if getNetworkStatus == "Offline"
        {

        }
    }

    
    // MARK: - ****** LoginWithFacebook Button Action. ******
    @IBAction func loginWithFB_btnAction(_ sender: Any) {
        
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.loginBehavior = .web
        loginManager.logOut()
//        FBSDKAccessToken.setCurrent(nil)
//        FBSDKProfile.setCurrent(nil)
        let cookies = HTTPCookieStorage.shared
        let facebookCookies = cookies.cookies(for: URL(string: "https://facebook.com/")!)
        for cookie in facebookCookies! {
            cookies.deleteCookie(cookie )
        }
        
        UserDefaults.standard.synchronize()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):

            self.loadingView.isHidden = false
            let loginManager: FBSDKLoginManager = FBSDKLoginManager()
            loginManager.loginBehavior = .web
            loginManager.logIn(withReadPermissions: ["email"]) { (result, error) -> Void in
                if (error != nil)
                {
                    let alert = UIAlertController(title: "Alert!", message: "Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.loadingView.isHidden = true
                }
                else if (result?.isCancelled)!
                {
                    NSLog("Cancelled");
                    self.loadingView.isHidden = true
                }
                else
                {
                    NSLog("Logged in");
                    
                    self.getFaceBookDetails()
                }
            }
        }
    }
    
    //MARK:- ****** Structure For Facebook Permossions ******
    struct FacebookPermission
    {
        static let ID: String = "id"
        static let NAME: String = "name"
        static let EMAIL: String = "email"
        static let PROFILE_PIC: String = "picture"
        static let LAST_NAME: String = "last_name"
        static let FIRST_NAME: String = "first_name"
//        static let USER_FRIENDS: String = "user_friends"
//        static let PUBLIC_PROFILE: String = "public_profile"
    }
    
    
    //MARK:- ****** GET FACEBOOK DETAILS ******
    func getFaceBookDetails()
    {
//        print("FBSDKAccessToken.currentAccessToken().tokenString = \(FBSDKAccessToken.current().tokenString)")
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if(FBSDKAccessToken.current()?.tokenString != nil)
            {
                loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "\(FacebookPermission.NAME), \(FacebookPermission.FIRST_NAME), \(FacebookPermission.LAST_NAME), \(FacebookPermission.EMAIL), \(FacebookPermission.PROFILE_PIC).type(large)"])
                
                graphRequest.start { (connection: FBSDKGraphRequestConnection?, result: Any?, error: Error?) in
                    if error == nil
                    {
                        if let facebookData = result as? NSDictionary
                        {
                            if let profilePic = facebookData.value(forKey: FacebookPermission.PROFILE_PIC)
                            {
                                self.facebookImageURL = (((profilePic as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "url") as? String)!
                            }
                            else{
                                self.facebookImageURL = ""
                            }
                            
                            if let name = facebookData.value(forKey: FacebookPermission.NAME)
                            {
                                self.facebookUserName = name as! String
                            }
                            else{
                                self.facebookUserName = ""
                            }
                            
                            if let email = facebookData.value(forKey: FacebookPermission.EMAIL)
                            {
                                self.facebookUserEmail = email as! String
                            }
                            else{
                                self.facebookUserEmail = ""
                            }
                            
                            if let id = facebookData.value(forKey: FacebookPermission.ID)
                            {
                                self.facebookUserID = id as! String
                            }
                            else{
                                self.facebookUserID = ""
                            }
                            
                            var deviceID = String()
                            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                                deviceID = uuid
                            }
                       
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            
                            if let refreshedToken = FIRInstanceID.instanceID().token() {
                                appDelegate.myDeviceToken = refreshedToken
                            }
                            
                            ApiHandler.loginUser(withEmail: self.facebookUserEmail, password: "wima@123", myDeviceToken: appDelegate.myDeviceToken, deviceID: deviceID,device_type: "ios", completion: { (responseData) in
                                
                                if (responseData.value(forKey: "status") as? String == "200")
                                {
                                    let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                    
                                    let keysArray = dataDict?.allKeys as! [String]
                                    if keysArray.contains("purchased_package")
                                    {
                                        let purchased_package_Array = dataDict?.value(forKey: "purchased_package") as! NSArray
                                        
                                        if (purchased_package_Array.count <= 0)
                                        {
                                            UserDefaults.standard.setValue("packageNotPurchased", forKey: "checkPurchasedPackage")
                                            UserDefaults.standard.synchronize()
                                        }
                                        else
                                        {
                                            
                                            for index in 0..<purchased_package_Array.count
                                            {
                                                
                                                var packagePurchasedDict = NSDictionary()
                                                packagePurchasedDict = purchased_package_Array[index] as! NSDictionary
                                                
                                                if (packagePurchasedDict["packages"] != nil)
                                                {
                                                    var packageDict = NSDictionary()
                                                    packageDict = packagePurchasedDict.value(forKey: "packages") as! NSDictionary
                                                    
                                                    if (packageDict["description"] != nil)
                                                    {
                                                        let packageDetails = packageDict.value(forKey: "description") as! String
                                                        
                                                    UserDefaults.standard.setValue(packageDetails, forKey: "checkPurchasedPackageDetails")
                                                        UserDefaults.standard.synchronize()
                                                    }
                                                }
                                            }
                                            
                                           UserDefaults.standard.setValue("packagePurchased", forKey: "checkPurchasedPackage")
                                            UserDefaults.standard.synchronize()
                                        }
                                    }
                                    
//                                    var profile_pic_thumb = dataDict?.value(forKey: "profile_pic_thumb") as! String
                                    
                                    UserDefaults.standard.set(dataDict, forKey: "userDetailsDict")
                                    UserDefaults.standard.set("12345", forKey: "passwordSaved")
                                    UserDefaults.standard.set("facebookUser", forKey: "userType")
                                    UserDefaults.standard.setValue(dataDict?.value(forKey: "auth_token")!, forKey: "auth_token")
                                    UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic")!, forKey: "userProfilePicture")
                                    UserDefaults.standard.setValue(dataDict?.value(forKey: "profile_pic_thumb")!, forKey: "profile_pic_thumb")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "username")!, forKey: "userName")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "id")!, forKey: "UserAuthID")
                                    
                                    UserDefaults.standard.setValue(dataDict?.value(forKey: "firebase_user_id"), forKey: "firebase_user_id")
                                    
                                    UserDefaults.standard.set(dataDict?.value(forKey: "age") as? String, forKey: "Age")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "location") as? String, forKey: "Location")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "state") as? String, forKey: "state")
                                    UserDefaults.standard.synchronize()
                                    
                                    UserDefaults.standard.set(dataDict?.value(forKey: "location_sharing") as? String, forKey: "location_sharing")
                                    
                                    UserDefaults.standard.set(dataDict?.value(forKey: "wi_chat_room") as? String, forKey: "wi_chat_room")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "post_likes") as? String, forKey: "post_likes")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "new_matches") as? String, forKey: "new_matches")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "messages") as? String, forKey: "messages")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "comments") as? String, forKey: "comments")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "profile_visitors") as? String, forKey: "profile_visitors")
                                    UserDefaults.standard.set(dataDict?.value(forKey: "profile_visibility") as? String, forKey: "profile_visibility")
                                    UserDefaults.standard.synchronize()
                                    
                                    let usersFirebaseID = dataDict?.value(forKey: "firebase_user_id") as! String
                                    
                                    let ref = FIRDatabase.database().reference()
                                    var userDetails = [String: Any]()
                                    userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                                    userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                                    userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                                    userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                                    userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                                    userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                                    userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                                    
                                    if let refreshedToken = FIRInstanceID.instanceID().token()
                                    {
                                        userDetails["myDeviceToken"] = refreshedToken
                                    }
                                    
                                    ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                                    
                                    
                                    var dataDictForGeneralCategory = [String : Any]()
                                    let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                                    if (profile_visibility == "on")
                                    {
                                        dataDictForGeneralCategory["Profile_Visibility"] = "On"
                                    }
                                    else
                                    {
                                        dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                                    }
                                ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                                    
                                    var settingsDict = [String : Any]()
                                    let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                                    
                                    if (chatRoomValue == "on")
                                    {
                                        settingsDict["Wi_Chat_Room"] = "On"
                                    }
                                    else{
                                        settingsDict["Wi_Chat_Room"] = "Off"
                                    }
                                    
                                    let messages = dataDict?.value(forKey: "messages") as? String
                                    if (messages == "on")
                                    {
                                        settingsDict["Messages"] = "On"
                                    }
                                    else
                                    {
                                        settingsDict["Messages"] = "Off"
                                    }
                                    
                                ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                                    
                                    if (dataDict!["is_firsttime_login"] != nil)
                                    {
                                        let is_firsttime_login = dataDict?.value(forKey: "is_firsttime_login") as? String
                                        
                                        if (is_firsttime_login == "yes")
                                        {
                                            self.getConversation(usersFirebaseID: usersFirebaseID, completion: { (resultConversations) in
                                                
                                                self.getAnonymousConversations(usersFirebaseID: usersFirebaseID, completion: { (resultAnonymousConversations) in
                                                    
                                                    let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                                    self.loadingView.isHidden = true
                                                    self.view.isUserInteractionEnabled = true
                                                    
                                                UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                                                UserDefaults.standard.set("success", forKey: "userRegistered")
                                                UserDefaults.standard.set("facebookUser", forKey: "userType")
                                            self.navigationController?.pushViewController(homeScreen, animated: true)
                                                })
                                            })
                                        }
                                        else
                                        {
                                            let ref = FIRDatabase.database().reference()
                                            var userDetails = [String: Any]()
                                            userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                                            userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                                            userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                                            userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                                            userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                                            userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                                            userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                                            
                                            if let refreshedToken = FIRInstanceID.instanceID().token()
                                            {
                                                userDetails["myDeviceToken"] = refreshedToken
                                            }
                                            
                                            ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                                            
                                            
                                            var dataDictForGeneralCategory = [String : Any]()
                                            let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                                            if (profile_visibility == "on")
                                            {
                                                dataDictForGeneralCategory["Profile_Visibility"] = "On"
                                            }
                                            else{
                                                dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                                            }
                                            ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                                            
                                            var settingsDict = [String : Any]()
                                            let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                                            if (chatRoomValue == "on")
                                            {
                                                settingsDict["Wi_Chat_Room"] = "On"
                                            }
                                            else{
                                                settingsDict["Wi_Chat_Room"] = "Off"
                                            }
                                            
                                            let messages = dataDict?.value(forKey: "messages") as? String
                                            if (messages == "on")
                                            {
                                                settingsDict["Messages"] = "On"
                                            }
                                            else
                                            {
                                                settingsDict["Messages"] = "Off"
                                            }
                                            
                                            ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                                            
                                            
                                            let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                                            UserDefaults.standard.set("success", forKey: "userRegistered")
                                            UserDefaults.standard.set("emailUser", forKey: "userType")
                                            self.navigationController?.pushViewController(homeScreen, animated: true)
                                        }
                                    }
                                    else
                                    {
                                        let ref = FIRDatabase.database().reference()
                                        var userDetails = [String: Any]()
                                        userDetails["UserID"] = dataDict?.value(forKey: "id") as? String
                                        userDetails["Age"] = dataDict?.value(forKey: "age") as? String
                                        userDetails["UserName"] = dataDict?.value(forKey: "username") as? String
                                        userDetails["ProfilePicture"] = dataDict?.value(forKey: "profile_pic") as? String
                                        userDetails["Location"] = dataDict?.value(forKey: "location") as? String
                                        userDetails["auth_token"] = dataDict?.value(forKey: "auth_token") as? String
                                        userDetails["firebase_user_id"] = dataDict?.value(forKey: "firebase_user_id") as? String
                                        
                                        if let refreshedToken = FIRInstanceID.instanceID().token()
                                        {
                                            userDetails["myDeviceToken"] = refreshedToken
                                        }
                                        
                                        ref.child("Users").child(usersFirebaseID).updateChildValues(userDetails)
                                        
                                        
                                        var dataDictForGeneralCategory = [String : Any]()
                                        let profile_visibility = dataDict?.value(forKey: "profile_visibility") as? String
                                        if (profile_visibility == "on")
                                        {
                                            dataDictForGeneralCategory["Profile_Visibility"] = "On"
                                        }
                                        else{
                                            dataDictForGeneralCategory["Profile_Visibility"] = "Off"
                                        }
                                        ref.child("Users").child(usersFirebaseID).child("Settings").child("General").updateChildValues(dataDictForGeneralCategory)
                                        
                                        var settingsDict = [String : Any]()
                                        let chatRoomValue = dataDict?.value(forKey: "wi_chat_room") as? String
                                        if (chatRoomValue == "on")
                                        {
                                            settingsDict["Wi_Chat_Room"] = "On"
                                        }
                                        else{
                                            settingsDict["Wi_Chat_Room"] = "Off"
                                        }
                                        
                                        let messages = dataDict?.value(forKey: "messages") as? String
                                        if (messages == "on")
                                        {
                                            settingsDict["Messages"] = "On"
                                        }
                                        else
                                        {
                                            settingsDict["Messages"] = "Off"
                                        }
                                        
                                        ref.child("Users").child(usersFirebaseID).child("Settings").child("Notification").updateChildValues(settingsDict)
                                        
                                        
                                        let homeScreen = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        UserDefaults.standard.set("addedSuccess", forKey: "userInterests")
                                        UserDefaults.standard.set("success", forKey: "userRegistered")
                                        UserDefaults.standard.set("emailUser", forKey: "userType")
                                        self.navigationController?.pushViewController(homeScreen, animated: true)
                                    }
                                }
                                else if (responseData.value(forKey: "status") as? String == "405")
                                {
//                                    let message = responseData.value(forKey: "message") as? String
                                    
                                    let registerScreen = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                                    registerScreen.typeOfUser = "facebookUser"
                                    registerScreen.getDisplayNameStr = self.facebookUserName
                                    registerScreen.getEmailStr = self.facebookUserEmail
                                    registerScreen.imageStr = self.facebookImageURL
                                    registerScreen.faceBookID = self.facebookUserID
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                self.navigationController?.pushViewController(registerScreen, animated: true)
                                }
                                else if (responseData.value(forKey: "status") as? String == "400")
                                {
                                    let message = responseData.value(forKey: "message") as? String
                                    
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else
                                {
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            })
                        }
                    }
                }
            }
        }
    }
    
    func getConversation(usersFirebaseID: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let databaseRef = FIRDatabase.database().reference()
        databaseRef.child("Users").child(usersFirebaseID).child("conversations").observe(FIRDataEventType.value, with: { (snapshot) in
            
            if (snapshot.exists())
            {
                self.getConversationStr = "conversationsExists"
                
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot
                {
                    let conversationDict = rest.value as! NSDictionary
                    
                    let typeOfUser = conversationDict.value(forKey: "userType") as! String
                    let opponentStatus = conversationDict.value(forKey: "opponentProfileVisibilityStatus") as! String
                    let myStatus = conversationDict.value(forKey: "myProfileVisibilityStatus") as! String
                    let location = conversationDict.value(forKey: "location") as! String
                    
                    let data = ["location": location, "userType": typeOfUser,"myProfileVisibilityStatus": myStatus, "opponentProfileVisibilityStatus": opponentStatus]
                    databaseRef.child("Users").child(usersFirebaseID).child("conversations").child(rest.key).updateChildValues(data)
                }
                
                completion(true)
            }
            else
            {
                self.getConversationStr = "conversationsDoesNotExists"
                
                completion(true)
            }
        })
    }
    
    
    func getAnonymousConversations(usersFirebaseID: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let databaseRef = FIRDatabase.database().reference()
        databaseRef.child("Users").child(usersFirebaseID).child("conversations_anonymous").observe(FIRDataEventType.value, with: { (snapshotAnonymousConvo) in
            
            if (snapshotAnonymousConvo.exists())
            {
                self.getConversationAnonymousStr = "conversationsExists"
                
                let enumerator = snapshotAnonymousConvo.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot
                {
                    let conversationDict = rest.value as! NSDictionary
                    
                    let typeOfUser = conversationDict.value(forKey: "userType") as! String
                    
                    let location = conversationDict.value(forKey: "location") as! String
                    
                    if conversationDict["checkRevealProfile"] != nil
                    {
                        let checkRevealProfile = conversationDict.value(forKey: "checkRevealProfile") as! String
                        
                        let data = ["location": location, "userType": typeOfUser,"checkRevealProfile": checkRevealProfile]
                        databaseRef.child("Users").child(usersFirebaseID).child("conversations_anonymous").child(rest.key).updateChildValues(data)
                    }
                    else
                    {
                        let data = ["location": location, "userType": typeOfUser]
                        databaseRef.child("Users").child(usersFirebaseID).child("conversations_anonymous").child(rest.key).updateChildValues(data)
                    }
                }
                
                completion(true)
            }
            else
            {
                self.getConversationAnonymousStr = "conversationsDoesNotExists"
                completion(true)
            }
        })
    }


    // MARK: - ****** Login with facebook button delegates ******
     func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        if let error = error
        {
            return
        }
       
    }

    
    // MARK: - ****** Login With Instagram button Action. ******
    @IBAction func loginWithInsta_btnAction(_ sender: Any) {
            let homeScreen = storyboard?.instantiateViewController(withIdentifier: "InstagramVC") as! InstagramVC
            self.navigationController?.pushViewController(homeScreen, animated: true)
    }
    
    
    // MARK: - ****** Login with Email button Action. ******
    @IBAction func signIn_btnAction(_ sender: Any) {
        let loginScreen = storyboard?.instantiateViewController(withIdentifier: "LoginWithEmailViewController") as! LoginWithEmailViewController
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    
    // MARK: - ****** Register With Email button Action. ******
    @IBAction func register_btnAction(_ sender: Any) {
        let registrationScreen = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as!RegisterViewController
        self.navigationController?.pushViewController(registrationScreen, animated: true)
    }
    
    // MARK: - ****** DidReceiveMemoryWarning. ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}
