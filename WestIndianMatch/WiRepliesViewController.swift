//
//  WiRepliesViewController.swift
//  Wi Match
//
//  Created by brst on 08/11/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import SDWebImage
import FLAnimatedImage
import GiphyCoreSDK

protocol updatePopInfo {
    func popInformationOnPopMethod(returnFromView : String)
}

class ReplyDetails
{
    var location: String
    var age: String
    var createdAt:String
    var userIDStr: String
    var postedImage: String
    var gender: String
    var likesCount: String
    var replyCount: String
    var totalTime: String
    var statusAdded: String
    var postIDStr: String
    var moodTypeStr: String
    var photoAlbumTypeStr: String
    var width: Int
    var height: Int
    var postHours: String
    var replyID: String
    var furtherReplyID: String
    var  likedByMeStatus: String
    var name: String
    var profilePic: String
    var firebase_user_id: String
    var child_comments_array: NSArray
    
    init(location: String, age: String, createdAt: String, userID: String, postedImage: String, gender: String, likesCount: String, replyCount: String, totalTime: String, Status: String,postID: String, moodType: String, photoTypeAlbum: String, width: Int, height: Int, postHoursStr: String,replyID: String, furtherReplyIDStr: String, likedByMeStatus: String, username: String, profileStr: String,firebase_user_id: String,child_comments_array: NSArray)
    {
        self.location = location
        self.age = age
        self.createdAt = createdAt
        self.userIDStr = userID
        self.postedImage = postedImage
        self.gender = gender
        self.likesCount = likesCount
        self.replyCount = replyCount
        self.totalTime = totalTime
        self.statusAdded = Status
        self.postIDStr = postID
        self.moodTypeStr = moodType
        self.photoAlbumTypeStr = photoTypeAlbum
        self.width = width
        self.height = height
        self.postHours = postHoursStr
        self.replyID = replyID
        self.furtherReplyID = furtherReplyIDStr
        self.likedByMeStatus = likedByMeStatus
        self.name = username
        self.profilePic = profileStr
        self.firebase_user_id = firebase_user_id
        self.child_comments_array = child_comments_array
    }
}

class WiRepliesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UITextViewDelegate,UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //MARK:- ***** VARIABLES DECLARATION *****
    var popInfoDelegate: updatePopInfo?
    
    //gif variables
    var gifArray = NSMutableArray()
    var offset = Int()
    var searchOffset = Int()
    var moreScroll = Bool()
    var getGifURl = String()
    var searchText = String()
    var differentiateStrForGifs = String()
    var gifImageChangedStr = String()
    var mediaType = String()
    var media_uploadedFrom = String()
    
    var currentLatitudeFromHomePage = Double()
    var currentLongitudeFromHomePage = Double()
    var currentLocationFromHomePage = String()
    
    var photoUploadType = String()
    var widthOfGif = Int()
    var heightOfGif = Int()
    
    var postReportedByArray = NSMutableArray()
    var getFullDataStr = String()
    var latestIntArr = NSMutableArray()
    var indexAtClicked = Int()
    var checkLikeBtnBool = Bool()
    var userIDToReportStr = String()
    var statusArray = [String]()
    var appDelegatePush = String()
    var checkReplyType = String()
    var postIDStr = String()
    var replyIDStr = String()
    var replyonreplyStr = String()
    var postIdOwnerStr = String()
    
    var allRepliesArray = [ReplyDetails]()
    var getAllFurtherReplies = [ReplyDetails]()
    
    var userIDReplyOwner = String()
    var totalCountArray = NSMutableArray()
    
    var typeOfPostStr = String()
    var typeOfAlbumsPush = String()
    var postIDOpenPost = String()
    var replyONReplyIDStr = String()
    
    var likePostBool = Bool()
    var getUserStatus = String()
    var postIDOwner = String()
    var badgeCount = Int()
    var postIDLikesStr = String()
    var ownerProfileVisibility_postLikes = String()
    var ownerDeviceId = String()
    var postLikedByArray = NSMutableArray()
    var reportBool = Bool()
    var getLikesCountStr = String()
    var getReportTotalCount = String()
    var getReplyIDStr = String()
    
    var getReplyCountStr = String()
    var decreaseCountBool = Bool()
    
    var differentiateTypereplyStr = String()
    
    var getReloadIndexPath = Int()
    var indexPathSelected = IndexPath()
    var likedUserDict = NSMutableDictionary()
    
    var repliesBool = Bool()
    var userID = String()
    
    var refreshBool = Bool()
    var refreshControl: UIRefreshControl!
    
    var blockedUsersByMeArray = NSMutableArray()
    var blockedUsersArray = NSMutableArray()
    var blockedByUserArray = NSMutableArray()
    var reportedUsersByMeArray = NSMutableArray()
    
    var currentPageStr = Int()
    
    var totalPage = Int()
    var currentPage = Int()
    
    var typeOfConfession = String()
    
    var ConfessionsDataDict = [String: String]()
    var statusAdded = String()

    
    //MARK:- ***** OUTLETS DECLARATION *****
    
    
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var userName_Y_Constraint: NSLayoutConstraint!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeHolderTextView: UILabel!
    @IBOutlet weak var postRepltBtn: UIButton!
    @IBOutlet weak var confessionType_MyImage: FLAnimatedImageView!
    @IBOutlet weak var writeReply_txtView: UITextView!
    @IBOutlet weak var writeReplyView: UIView!
    @IBOutlet weak var repliesTableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var noRepliesFound: UIView!
    @IBOutlet weak var confessionLikeIcon: UIImageView!
    @IBOutlet weak var fullImageAnimatedGif: FLAnimatedImageView!
    @IBOutlet weak var fullImageView: UIImageView!
    @IBOutlet weak var showImagefullScreenView: UIView!
    @IBOutlet weak var confessionReply: UILabel!
    @IBOutlet weak var confessionLikesLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var wrapperViewForConfession: UIView!
    @IBOutlet weak var usersLocationLbl: UILabel!
    @IBOutlet weak var usersProfilePic: UIImageView!
    @IBOutlet weak var statusTextView: UITextView!
    @IBOutlet weak var pendingHoursLbl: UILabel!
    @IBOutlet weak var repliesGifWrapperView: UIView!
    @IBOutlet weak var imagesButtonWrapperView: UIView!
    
    
    //gif outlets
    
    @IBOutlet weak var searchMainView: UIView!
    @IBOutlet weak var noGifAvailableView: UIView!
    @IBOutlet weak var giphyCollectionView: UICollectionView!
    @IBOutlet weak var giphyImagesView: UIView!
    @IBOutlet weak var loadMoreBtn: UIButton!
    @IBOutlet weak var searchGifView: UIView!
    @IBOutlet weak var searchGif_TxtField: UITextField!
    
    
    var imagePickedBool = Bool()
    
    
    //MARK:- ****** viewDidLoad ******
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchMainView.layer.cornerRadius = 15
        self.searchMainView.layer.masksToBounds = false
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTap(_:)))
        tap.numberOfTapsRequired = 1
        self.repliesTableView.addGestureRecognizer(tap)
        
        self.imagePickedBool = false
        self.confessionType_MyImage.image = nil

      /*  repliesTableView.register(UINib(nibName: "HeaderComments", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderPhotos")
        
         repliesTableView.register(UINib(nibName: "HeaderCommentsForText", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderText")
        
         repliesTableView.register(UINib(nibName: "HeaderCommentsForGif", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderGif") */
        
        if (ConfessionsDataDict != nil)
        {
            if (ConfessionsDataDict["statusAdded"] != nil)
            {
                statusAdded = ConfessionsDataDict["statusAdded"]!
                
                if (statusAdded != "")
                {
                    let inputString: NSString = statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    self.statusTextView.dataDetectorTypes = UIDataDetectorTypes.link
                    
                    self.statusTextView.text = modifiedString
                    
                    if (self.statusTextView.text != "")
                    {
                        let fixedWidth = self.statusTextView.frame.size.width
                        self.statusTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                        let newSize = self.statusTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                        var newFrame = self.statusTextView.frame
                        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                        
                        self.textViewHeightConstraint.constant = newSize.height
                    }
                    else{
                        self.textViewHeightConstraint.constant = 0
                    }
                
                    self.usersProfilePic.layer.cornerRadius = self.usersProfilePic.frame.size.height/2
                    self.usersProfilePic.clipsToBounds = true
                    self.usersProfilePic.layer.masksToBounds = true
                    self.usersProfilePic.contentMode = UIViewContentMode.scaleAspectFill
                    
                    let typeofUser = ConfessionsDataDict["typeOfConfession"]
                    let location = ConfessionsDataDict["location"]
                    let ownerProfilePic = ConfessionsDataDict["ownerProfilePic"]
                    let age = ConfessionsDataDict["age"]
                    let gender = ConfessionsDataDict["gender"]
                    var genderStr = String()
                    if gender == "1"
                    {
                        genderStr = "Male"
                    }
                    else
                    {
                        genderStr = "Female"
                    }
                    
                    let totalTime = ConfessionsDataDict["totalTime"]
                    let username = ConfessionsDataDict["username"]
                    if (typeofUser == "withprofile")
                    {
                        self.userNameLbl.text = username
                        self.usersProfilePic.sd_setImage(with: URL(string: ownerProfilePic!), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                    }
                    else
                    {
                        self.userNameLbl.text = genderStr + ", " + age!
                        
                        self.usersProfilePic.sd_setImage(with: URL(string: ownerProfilePic!), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = self.usersProfilePic.bounds
                        self.usersProfilePic.addSubview(blurEffectView)
                    }
                    
                    var endDateForConfession = totalTime
                    endDateForConfession = endDateForConfession! + " UTC"
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    let datee = dateFormatter.date(from: endDateForConfession!)!
                    
                    self.pendingHoursLbl.text = self.getRemainingTime(timestamp: datee as Date, timeframe: "24 hrs") + " hrs"
                    
                    if (location != "")
                    {
                        self.locationIcon.isHidden = false
                        self.userName_Y_Constraint.constant = 5
                    }else{
                        self.locationIcon.isHidden = true
                        self.userName_Y_Constraint.constant = (self.usersProfilePic.frame.origin.y +  self.usersProfilePic.frame.size.height)/3
                    }
                    
                    self.usersLocationLbl.text = location
                }
                else{
                    self.wrapperViewForConfession.isHidden = true
                }
            }else{
                self.wrapperViewForConfession.isHidden = true
            }
        }
        else{
            self.wrapperViewForConfession.isHidden = true
        }
        
        
        
        self.refreshBool = false
        
        currentPageStr = 1
        
        self.photoUploadType = ""
        self.widthOfGif = 100
        self.heightOfGif = 100
        
        self.writeReply_txtView.delegate=self
        writeReply_txtView.layer.borderColor = UIColor.lightGray.cgColor
        writeReply_txtView.layer.borderWidth = 1
        writeReply_txtView.layer.cornerRadius = 5
        
        confessionType_MyImage.layer.borderWidth = 2.0
        confessionType_MyImage.layer.masksToBounds = true
        confessionType_MyImage.layer.borderColor = UIColor.clear.cgColor
        confessionType_MyImage.layer.cornerRadius = 13
        confessionType_MyImage.layer.cornerRadius = confessionType_MyImage.frame.size.height/2
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.repliesTableView.addSubview(refreshControl)
        
        self.repliesTableView.rowHeight = UITableViewAutomaticDimension
        self.repliesTableView.estimatedRowHeight = 109.0
        self.checkLikeBtnBool = false
        self.decreaseCountBool = false
        self.repliesBool = false
        self.reportBool = false
        self.userIDToReportStr = ""
        self.getReportTotalCount = ""
        self.getReplyIDStr = ""
        
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
//        print("self.postIdOwnerStr = \(self.postIdOwnerStr)")
//        print("self.postIDStr = \(self.postIDStr)")
//        print("replyIDStr = \(self.replyIDStr)")

        self.showImagefullScreenView.isHidden = true
        self.statusArray = ["withoutPicture","withPicture","withoutPicture","withoutPicture","withPicture","withoutPicture","withoutPicture","withoutPicture","withPicture","withoutPicture"]
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(WiRepliesViewController.notifyOnUpdate), name: NSNotification.Name(rawValue: "notifyOnUpdate"), object: nil)
        
       
        self.getGifURl = ""
        self.widthOfGif = 100
        self.heightOfGif = 100
        
        self.offset = 0
        self.searchOffset = 0
        self.gifImageChangedStr = ""
        self.giphyImagesView.isHidden = true
        self.differentiateStrForGifs = "allGifs"
        self.searchGifView.isHidden = true
        self.searchGif_TxtField.attributedPlaceholder = NSAttributedString(string: "Search by name",
                                                                           attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        let giphy = GPHClient.init(apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B")
        
        /// Gif Search
        self.getGifs(gifType: "applause+actions+awww+celebrities", apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25,pageOffset: self.offset)
        
        NotificationCenter.default.addObserver(self, selector: #selector(mediaCapturedforCreateCampaign), name: NSNotification.Name(rawValue: "mediaCapturedforCreateCampaign"), object: nil)
    }
    
    func handleTap(_ recognizer: UITapGestureRecognizer?) {
       
        self.view.endEditing(true)
        self.writeReply_txtView.resignFirstResponder();
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        
        let headerView = self.repliesTableView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        var height = CGFloat()
        if (ConfessionsDataDict != nil)
        {
            if (ConfessionsDataDict["statusAdded"] != nil)
            {
                if (statusAdded != "")
                {
                    height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
                }
                else{
                    height = 0
                }
            }else{
                height = 0
            }
        }else{
            height = 0
        }
        
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        self.repliesTableView.tableHeaderView = headerView
    }
    
    // MARK: - ****** UICollectionView Delegates and DataSource Methods. ******
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.gifArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = self.giphyCollectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotosCollectionViewCell
        
        let imagesDict: NSDictionary = ((self.gifArray[indexPath.row] as AnyObject).value(forKey: "images")) as! NSDictionary
        
        var preview_gif = NSDictionary()
        var gifURL = String()
        
        if imagesDict["preview_gif"] != nil
        {
            preview_gif = imagesDict.value(forKey: "preview_gif") as! NSDictionary
            gifURL = preview_gif.value(forKey: "url") as! String
        }
        else if imagesDict["fixed_height"] != nil{
            preview_gif = imagesDict.value(forKey: "fixed_height") as! NSDictionary
            gifURL = preview_gif.value(forKey: "url") as! String
        }
        
        DispatchQueue.main.async {
            
            cell.acitivityIndicator.isHidden = false
            cell.giphyAnimatedImageView.sd_setImage(with: URL(string: gifURL), placeholderImage: UIImage(named: ""),options: SDWebImageOptions(rawValue: 0), completed: { (img, err, cacheType, imgURL) in
                cell.acitivityIndicator.isHidden = true
            })
        }
        
        cell.giphyAnimatedImageView.contentMode = UIViewContentMode.scaleAspectFit
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        var frameOfCell = self.giphyCollectionView.frame as CGRect
        frameOfCell.origin.y = 0
        return CGSize.init(width: self.view.frame.size.width/2 - 1, height: self.view.frame.size.width/2 + 15)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.getGifURl = ""
        self.widthOfGif = 100
        self.heightOfGif = 100
        let photosCell = collectionView.cellForItem(at: indexPath) as! PhotosCollectionViewCell
        
        DispatchQueue.main.async {
            
            self.confessionType_MyImage.animatedImage = photosCell.giphyAnimatedImageView.animatedImage
            
            for subview in self.confessionType_MyImage.subviews
            {
                if subview.isKind(of: UIVisualEffectView.self)
                {
                    subview.removeFromSuperview()
                }
            }
            
            let imagesDict: NSDictionary = ((self.gifArray[indexPath.row] as AnyObject).value(forKey: "images")) as! NSDictionary
            
            var preview_gif = NSDictionary()
            
            if imagesDict["fixed_height"] != nil{
                preview_gif = imagesDict.value(forKey: "fixed_height") as! NSDictionary
            }
            else if imagesDict["preview_gif"] != nil{
                preview_gif = imagesDict.value(forKey: "preview_gif") as! NSDictionary
            }
            
            self.getGifURl = preview_gif.value(forKey: "url") as! String
            if (preview_gif["width"] != nil) && (preview_gif["height"] != nil)
            {
                self.widthOfGif = Int(preview_gif.value(forKey: "width") as! String)!
                self.heightOfGif = Int(preview_gif.value(forKey: "height") as! String)!
            }
            else
            {
                self.widthOfGif = 100
                self.heightOfGif = 100
            }

            self.imagePickedBool = true
            UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: { _ in
                
                self.giphyImagesView.isHidden = true

                
            }, completion: nil)
            
            self.photoUploadType = "gif"
            
            self.gifImageChangedStr = "gifChanged"
        }
    }
    
    
    // MARK: - ****** getRemainingTime ******
    func getRemainingTime(timestamp: Date, timeframe: String)->  String
    {
        let date22 = timestamp
        let formatter22 = DateFormatter()
        formatter22.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter22.timeZone = TimeZone.current
        
        // *** Create date ***
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let now = formatter.string(from: date as Date) //prints 12.21 AM
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current
        var TimeFrameAdded = String()
        if (timeframe.range(of: " ") != nil)
        {
            let fullUserID = timeframe.components(separatedBy: " ")
            let beforeAtTheRateStr: String = fullUserID[0]
            TimeFrameAdded = beforeAtTheRateStr
        }
        
//        var now3 = String()
        var dateStr = Date()
        if timeframe == "24 hrs"
        {
            dateStr = calendar.date(byAdding: .day, value: 1, to: date22 as Date)!
            let formatter3 = DateFormatter()
            formatter3.timeZone = TimeZone.current
            formatter3.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            now3 = formatter.string(from: dateStr as Date)
        }
        else
        {
            dateStr = calendar.date(byAdding: .hour, value: Int(TimeFrameAdded)!, to: date22 as Date)!
            let formatter3 = DateFormatter()
            formatter3.timeZone = TimeZone.current
            formatter3.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            now3 = formatter.string(from: dateStr as Date)
        }
        
        let nowDate = NSDate()
        
        let secondsFromNowToFinish = dateStr.timeIntervalSince(nowDate as Date)
        let hours = Int(secondsFromNowToFinish / 3600)
//        let minutes = Int((secondsFromNowToFinish - Double(hours) * 3600) / 60)
//        let seconds = Int(secondsFromNowToFinish - Double(hours) * 3600 - Double(minutes) * 60 + 0.5)
        
        return String(format: "%02d", hours)
    }
    
    func pinchGestureDetected(_ recognizer: UIPinchGestureRecognizer?) {
        let state: UIGestureRecognizerState? = recognizer?.state
        if state == .began || state == .changed {
            let scale: CGFloat? = recognizer?.scale
            recognizer?.view?.transform = (recognizer?.view?.transform.scaledBy(x: scale!, y: scale!))!
            recognizer?.scale = 1.0
        }
        else if state == .ended
        {
            print("gestureRecognizerShouldEnded")
        }
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    
    func notifyOnUpdate()
    {
        self.getRepliesListing { (responseBool) in
        }
    }
    
    
    
    func getRepliesListing(completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan) , .online(.wiFi):
            
            if self.checkReplyType == "replyOnPost"
            {
                self.getFullDataStr = "fetchAllData"
                if (self.refreshBool == false)
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                }
                
                self.currentPage = 1
                self.allRepliesArray = []
                self.getAllFurtherReplies = []
                self.repliesTableView.reloadData()
                self.getReplyOnConfessionListing(paginationValue: self.currentPage, replyID: self.postIDStr,pageID: currentPageStr, completion: { (response) in
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    if (response == true)
                    {
                        DispatchQueue.main.async {
                            self.repliesTableView.isHidden = false
                            self.noRepliesFound.isHidden = true
                            self.repliesTableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else
                    {
                        self.allRepliesArray = []
                        self.getAllFurtherReplies = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.repliesTableView.isHidden = false
                        self.repliesTableView.reloadData()
                        self.refreshControl.endRefreshing()
                        self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })
                    }
                })
            }
            else if self.checkReplyType == "replyOnReply"
            {
                self.getFullDataStr = "fetchAllData"
                if (self.refreshBool == false)
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                }
                
                self.allRepliesArray = []
                self.getAllFurtherReplies = []
                self.currentPage = 1
                self.repliesTableView.reloadData()

                self.getReplyOnReplyListing(paginationValue: self.currentPage, replyID: self.postIDStr, pageID: currentPageStr, completion:  { (response) in
                    
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    if (response == true)
                    {
                        DispatchQueue.main.async {
                            self.repliesTableView.isHidden = false
                            self.noRepliesFound.isHidden = true
                            self.repliesTableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else
                    {
                        self.allRepliesArray = []
                        self.getAllFurtherReplies = []
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        self.repliesTableView.isHidden = false
//                        self.noRepliesFound.isHidden = false
                        self.repliesTableView.reloadData()
                        self.refreshControl.endRefreshing()
                        self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                            self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        }, completion: { (finished) in
                        })
                    }
                })
            }
        }
    }
    
    
    func getReplyOnReplyListing(paginationValue: Int,replyID: String,pageID: Int,completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getReplyOnReplyListing(userID: userID, userToken: authToken, confession_id: replyID, parent_comment_id: self.replyIDStr, page: paginationValue, completion: { (responseData) in
                        
                        self.allRepliesArray = []
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            if (dataDict!["meta"] != nil)
                            {
                                let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                                self.totalPage = (metaDict?.value(forKey: "totalpages") as? Int)!
                            }
                            
                            if (dataDict!["confession_type"] != nil)
                            {
                                self.typeOfConfession = dataDict?.value(forKey: "confession_type") as! String
                            }
                            else{
                                self.typeOfConfession = "withprofile"
                            }
                            
                            if let profileImage = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                            {
                                if self.imagePickedBool == false
                                {
                                    DispatchQueue.main.async {
                                        self.confessionType_MyImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                }
                                
                            }
                            
                            self.confessionType_MyImage.contentMode = UIViewContentMode.scaleAspectFill
                            
                            if (self.typeOfConfession == "anonymous" && self.imagePickedBool == false)
                            {
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = self.confessionType_MyImage.bounds
                                self.confessionType_MyImage.addSubview(blurEffectView)
                            }
                            else
                            {
                                for subview in self.confessionType_MyImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                            }
                            
                            if (dataDict!["list"] != nil)
                            {
                                let replyListingArray = dataDict?.value(forKey: "list") as! NSArray
                                if (replyListingArray.count > 0)
                                {
                                    for index in 0..<replyListingArray.count
                                    {
                                        var userData = NSDictionary()
                                        userData = replyListingArray[index] as! NSDictionary
                                        
                                        let timestamp: String = userData.value(forKey: "created") as! String
                                        
                                        var loc = String()
                                        var age = String()
                                        var gender = String()
                                        var username = String()
                                        var userPic = String()
                                        var firebase_user_id = String()
                                        
                                        let userID: String = userData.value(forKey: "user_id") as! String
                                        let postedImage: String = userData.value(forKey: "image") as! String
                                        
                                        let likeCountInt: Int = userData.value(forKey: "likes_count") as! Int
                                        let likeCountStr : String = String(likeCountInt)
                                        let replyCountStr : Int = userData.value(forKey: "comments_count") as! Int
                                        let total_duration: String = ""
                                        let statusAddedStr: String = userData.value(forKey: "text") as! String
                                        let confession_id: String = userData.value(forKey: "confession_id") as! String
                                        let reply_id: String = userData.value(forKey: "id") as! String
                                        let parent_comment_id: String = userData.value(forKey: "parent_comment_id") as! String
                                        let mood: String = userData.value(forKey: "mood") as! String
                                        let duration: String = ""
                                        
                                        let likedby_current: String = userData.value(forKey: "likedby_current") as! String
                                        
                                        let image_height: String = userData.value(forKey: "image_height") as! String
                                        let image_width: String = userData.value(forKey: "image_width") as! String
                                        
                                        var image_height_int = Int()
                                        var image_width_int = Int()
                                        
                                        if (image_height == "")
                                        {
                                            image_height_int = 250
                                        }
                                        else{
                                            image_height_int = Int(image_height)!
                                        }
                                        
                                        if (image_width == "")
                                        {
                                            image_width_int = 250
                                        }
                                        else{
                                            image_width_int = Int(image_width)!
                                        }
                                        
                                        var usersDetailsDict = NSDictionary()
                                        if (userData["user"] != nil)
                                        {
                                            usersDetailsDict = (userData.value(forKey: "user") as? NSDictionary)!
                                            
                                            age = String(usersDetailsDict.value(forKey: "Age") as! Int)
                                            loc = usersDetailsDict.value(forKey: "Location") as! String
                                            gender = usersDetailsDict.value(forKey: "gender") as! String
                                            username = usersDetailsDict.value(forKey: "username") as! String
                                            userPic = usersDetailsDict.value(forKey: "profile_pic") as! String
                                            firebase_user_id = usersDetailsDict.value(forKey: "firebase_user_id") as! String
                                        }
                                        else
                                        {
                                            age = ""
                                            loc = ""
                                            gender = ""
                                            username = ""
                                            userPic = ""
                                            firebase_user_id = ""
                                        }
                                        
                                        let array = NSArray()
                                        let userConfession = ReplyDetails.init(location: loc, age: age, createdAt: timestamp, userID: userID, postedImage: postedImage, gender: gender, likesCount: likeCountStr, replyCount: String(replyCountStr), totalTime: total_duration, Status: statusAddedStr, postID: confession_id, moodType: mood, photoTypeAlbum: "", width: image_width_int, height: image_height_int, postHoursStr: duration, replyID: parent_comment_id, furtherReplyIDStr: reply_id, likedByMeStatus: likedby_current, username: username,profileStr: userPic, firebase_user_id: firebase_user_id, child_comments_array: array)
                                        
                                        self.allRepliesArray.append(userConfession)
                                    }
                                    
                                    completion(true)
                                }
                                else
                                {
                                    completion(false)
                                }
                            }
                            else
                            {
                                completion(false)
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    func getReplyOnConfessionListing(paginationValue: Int, replyID: String,pageID: Int,completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getReplyListing(userID: userID, userToken: authToken, confession_id: replyID, page: paginationValue, completion: { (responseData) in
                        
//                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            if (dataDict!["meta"] != nil)
                            {
                                let metaDict = dataDict?.value(forKey: "meta") as? NSDictionary
                                self.totalPage = (metaDict?.value(forKey: "totalpages") as? Int)!
                            }
                            
                            if (dataDict!["confession_type"] != nil)
                            {
                                self.typeOfConfession = dataDict?.value(forKey: "confession_type") as! String
                            }
                            else
                            {
                                self.typeOfConfession = "withprofile"
                            }
                            
                            if let profileImage = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                            {
                                if self.imagePickedBool == false
                                {
                                    DispatchQueue.main.async {
                                        self.confessionType_MyImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "Default Icon"))
                                    }
                                }
                            }
                            
                            self.confessionType_MyImage.contentMode = UIViewContentMode.scaleAspectFill
                            
                            if (self.typeOfConfession == "anonymous" && self.imagePickedBool == false)
                            {
                                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                blurEffectView.frame = self.confessionType_MyImage.bounds
                                self.confessionType_MyImage.addSubview(blurEffectView)
                            }
                            else
                            {
                                for subview in self.confessionType_MyImage.subviews
                                {
                                    if subview.isKind(of: UIVisualEffectView.self)
                                    {
                                        subview.removeFromSuperview()
                                    }
                                }
                            }
                            
                            if (dataDict!["list"] != nil)
                            {
                                let replyListingArray = dataDict?.value(forKey: "list") as! NSArray
                                
                                if (replyListingArray.count > 0)
                                {
                                    for index in 0..<replyListingArray.count
                                    {
                                        var userData = NSDictionary()
                                        userData = replyListingArray[index] as! NSDictionary
                                        
                                        let timestamp: String = userData.value(forKey: "created") as! String
                                        
                                        var loc = String()
                                        var age = String()
                                        var gender = String()
                                        var username = String()
                                        var userPic = String()
                                        var firebase_user_id = String()
                                        
                                        let userID: String = userData.value(forKey: "user_id") as! String
                                        let postedImage: String = userData.value(forKey: "image") as! String
                                        
                                        let likeCountInt: Int = userData.value(forKey: "likes_count") as! Int
                                        let likeCountStr : String = String(likeCountInt)
                                        let replyCountStr : Int = userData.value(forKey: "comments_count") as! Int
                                        let total_duration: String = ""
                                        let statusAddedStr: String = userData.value(forKey: "text") as! String
                                        let confession_id: String = userData.value(forKey: "confession_id") as! String
                                        let reply_id: String = userData.value(forKey: "id") as! String
                                        let parent_comment_id: String = userData.value(forKey: "parent_comment_id") as! String
                                        let mood: String = userData.value(forKey: "mood") as! String
                                        let duration: String = ""
                                        
                                        let image_height: String = userData.value(forKey: "image_height") as! String
                                        let image_width: String = userData.value(forKey: "image_width") as! String
                                        
                                        var image_height_int = Int()
                                        var image_width_int = Int()
                                        
                                        if (image_height == "")
                                        {
                                            image_height_int = 250
                                        }
                                        else{
                                            image_height_int = Int(image_height)!
                                        }
                                        
                                        if (image_width == "")
                                        {
                                            image_width_int = 250
                                        }
                                        else{
                                            image_width_int = Int(image_width)!
                                        }
                                        
                                        let likedby_current: String = userData.value(forKey: "likedby_current") as! String
                                        
                                        var usersDetailsDict = NSDictionary()
                                        if (userData["user"] != nil)
                                        {
                                            usersDetailsDict = (userData.value(forKey: "user") as? NSDictionary)!
                                            
                                            age = String(usersDetailsDict.value(forKey: "Age") as! Int)
                                            loc = usersDetailsDict.value(forKey: "Location") as! String
                                            gender = usersDetailsDict.value(forKey: "gender") as! String
                                            username = usersDetailsDict.value(forKey: "username") as! String
                                            userPic = usersDetailsDict.value(forKey: "profile_pic") as! String
                                            
                                            firebase_user_id = usersDetailsDict.value(forKey: "firebase_user_id") as! String
                                        }
                                        else
                                        {
                                            age = ""
                                            loc = ""
                                            gender = ""
                                            username = ""
                                            userPic = ""
                                            firebase_user_id = ""
                                        }
                                        
                                        let array = NSArray()
                                        let userConfession = ReplyDetails.init(location: loc, age: age, createdAt: timestamp, userID: userID, postedImage: postedImage, gender: gender, likesCount: likeCountStr, replyCount: String(replyCountStr), totalTime: total_duration, Status: statusAddedStr, postID: confession_id, moodType: mood, photoTypeAlbum: "", width: image_width_int, height: image_height_int, postHoursStr: duration, replyID: reply_id, furtherReplyIDStr: parent_comment_id, likedByMeStatus: likedby_current, username: username,profileStr: userPic, firebase_user_id: firebase_user_id, child_comments_array: array)
                                        
                                        self.allRepliesArray.append(userConfession)
                                    }
                                    
                                    completion(true)
                                }
                                else
                                {
                                    completion(false)
                                }
                            }
                            else
                            {
                                completion(false)
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
 
    
    func refresh(_ sender: Any) {
        
        self.showImagefullScreenView.isHidden = true
        self.refreshBool = true
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.getRepliesListing(completion: { (reponsebool) in
                
            })
        }
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if (self.repliesTableView.contentOffset.y >= (self.repliesTableView.contentSize.height - self.repliesTableView.frame.size.height) ) {
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                
                if self.currentPage < self.totalPage {
                    let bottomEdge: Float = Float(self.repliesTableView.contentOffset.y + repliesTableView.frame.size.height)
                    if bottomEdge >= Float(repliesTableView.contentSize.height)  {
                        
                            if self.checkReplyType == "replyOnPost"
                            {
                                self.getFullDataStr = "fetchAllData"
                                if (self.refreshBool == false)
                                {
                                    self.loadingView.isHidden = false
                                    self.view.isUserInteractionEnabled = false
                                }
                                
                                self.currentPage = self.currentPage + 1
                                self.getReplyOnConfessionListing(paginationValue: self.currentPage, replyID: self.postIDStr,pageID: currentPageStr, completion: { (response) in
              
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    if (response == true)
                                    {
                                        DispatchQueue.main.async {
                                            self.repliesTableView.isHidden = false
                                            self.noRepliesFound.isHidden = true
                                            self.repliesTableView.reloadData()
                                            self.refreshControl.endRefreshing()
                                        }
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.repliesTableView.isHidden = false
                                        self.noRepliesFound.isHidden = true
                                        self.repliesTableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                        self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                            self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                        }, completion: { (finished) in
                                        })
                                    }
                                })
                            }
                            else if self.checkReplyType == "replyOnReply"
                            {
                                self.getFullDataStr = "fetchAllData"
                                if (self.refreshBool == false)
                                {
                                    self.loadingView.isHidden = false
                                    self.view.isUserInteractionEnabled = false
                                }
                                
                                self.currentPage = self.currentPage + 1
                                self.getReplyOnReplyListing(paginationValue: self.currentPage, replyID: self.postIDStr, pageID: currentPageStr, completion:  { (response) in
                       
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    if (response == true)
                                    {
                                        DispatchQueue.main.async {
                                            self.repliesTableView.isHidden = false
                                            self.noRepliesFound.isHidden = true
                                            self.repliesTableView.reloadData()
                                            self.refreshControl.endRefreshing()
                                        }
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.repliesTableView.isHidden = false
                                        self.noRepliesFound.isHidden = true
                                        self.repliesTableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                        self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                            self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                        }, completion: { (finished) in
                                        })
                                    }
                                })
                            }
                    }
                }
            }
        }
    }
    
    func createreplieArray(childComments:NSArray) -> [ReplyDetails]
    {
        var repliesArray = [ReplyDetails]()

        if (childComments.count > 0)
        {
            let child_commentsArray = childComments
            
            if (child_commentsArray.count > 0)
            {
                for index in 0..<child_commentsArray.count
                {
                    var userData = NSDictionary()
                    userData = child_commentsArray[index] as! NSDictionary
                    
                    let timestamp: String = userData.value(forKey: "created") as! String
                    
                    var loc = String()
                    var age = String()
                    var gender = String()
                    var username = String()
                    var userPic = String()
                    var firebase_user_id = String()
                    
                    let userID: String = userData.value(forKey: "user_id") as! String
                    let postedImage: String = userData.value(forKey: "image") as! String
                    
                    let likeCountInt: Int = userData.value(forKey: "likes_count") as! Int
                    let likeCountStr : String = String(likeCountInt)
                    let replyCountStr : Int = userData.value(forKey: "comments_count") as! Int
                    let total_duration: String = ""
                    let statusAddedStr: String = userData.value(forKey: "text") as! String
                    let confession_id: String = userData.value(forKey: "confession_id") as! String
                    let reply_id: String = userData.value(forKey: "id") as! String
                    let parent_comment_id: String = userData.value(forKey: "parent_comment_id") as! String
                    let mood: String = userData.value(forKey: "mood") as! String
                    let duration: String = ""
                    
                    let image_height: String = userData.value(forKey: "image_height") as! String
                    let image_width: String = userData.value(forKey: "image_width") as! String
                    
                    var image_height_int = Int()
                    var image_width_int = Int()
                    
                    if (image_height == "")
                    {
                        image_height_int = 250
                    }
                    else{
                        image_height_int = Int(image_height)!
                    }
                    
                    if (image_width == "")
                    {
                        image_width_int = 250
                    }
                    else{
                        image_width_int = Int(image_width)!
                    }
                    
                    let likedby_current: String = userData.value(forKey: "likedby_current") as! String
                    
                    var furtherChild_commentsDict = NSDictionary()
                    var furtherChild_commentsArray = NSArray()
                    
                    if (userData["child_comments"] != nil)
                    {
                        furtherChild_commentsDict = (userData.value(forKey: "child_comments") as? NSDictionary)!
                        
                        furtherChild_commentsArray = furtherChild_commentsDict.value(forKey: "list") as! NSArray
                        
                        if (furtherChild_commentsArray.count > 0)
                        {
                            for index in 0..<furtherChild_commentsArray.count
                            {
                                print("furtherChild_commentsArray = \(furtherChild_commentsArray[index])")
                            }
                        }
                    }
                    
                    var usersDetailsDict = NSDictionary()
                    if (userData["user"] != nil)
                    {
                        usersDetailsDict = (userData.value(forKey: "user") as? NSDictionary)!
                        
                        age = String(usersDetailsDict.value(forKey: "Age") as! Int)
                        loc = usersDetailsDict.value(forKey: "Location") as! String
                        gender = usersDetailsDict.value(forKey: "gender") as! String
                        username = usersDetailsDict.value(forKey: "username") as! String
                        userPic = usersDetailsDict.value(forKey: "profile_pic") as! String
                        
                        firebase_user_id = usersDetailsDict.value(forKey: "firebase_user_id") as! String
                    }
                    else
                    {
                        age = ""
                        loc = ""
                        gender = ""
                        username = ""
                        userPic = ""
                        firebase_user_id = ""
                    }
                    
                    let userConfession = ReplyDetails.init(location: loc, age: age, createdAt: timestamp, userID: userID, postedImage: postedImage, gender: gender, likesCount: likeCountStr, replyCount: String(replyCountStr), totalTime: total_duration, Status: statusAddedStr, postID: confession_id, moodType: mood, photoTypeAlbum: "", width: image_width_int, height: image_height_int, postHoursStr: duration, replyID: reply_id, furtherReplyIDStr: parent_comment_id, likedByMeStatus: likedby_current, username: username,profileStr: userPic, firebase_user_id: firebase_user_id, child_comments_array: child_commentsArray)
                    
                    repliesArray.append(userConfession)
                }
                
            }
        }
        
        return repliesArray
    }
    
    
    // MARK: - ****** startSearch_btnAction ******
    @IBAction func startSearch_btnAction(_ sender: Any) {
        self.searchGif_TxtField.text = ""
        self.searchText = ""
        self.searchGifView.isHidden = false
        self.searchGif_TxtField.becomeFirstResponder()
    }
    
    
    func isValidInput(Input:String) -> Bool {
        let myCharSet=CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let output: String = Input.trimmingCharacters(in: myCharSet.inverted)
        let isValid: Bool = (Input == output)
        
        return isValid
    }
    
    // MARK: - ****** UITextField Delegate ******
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
      
            if self.searchGif_TxtField.text != ""
            {
                self.differentiateStrForGifs = "searchNewGifs"
                
                self.searchText = self.searchGif_TxtField.text!
                self.searchText = self.searchText.replacingOccurrences(of: " ", with: "")
                
                if isValidInput(Input: self.searchText) == false {
                    let alert = UIAlertController(title: "", message:"Search by name field accepts only alphabatics", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    self.gifArray = []
                    self.giphyCollectionView.isHidden = true
                    self.loadingView.isHidden = false
                    self.searchOffset = 0
                    
                    self.loadMoreBtn.isUserInteractionEnabled = false
                    searchText = removeSpecialCharsFromString(text: searchText) // "pre"
                    
                    self.getSearchGifs(gifType: searchText, apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25, pageOffset: self.searchOffset)
                }
            }
            else
            {
                self.differentiateStrForGifs = "allGifs"
                self.searchGifView.isHidden = true
                self.offset = 0
                self.gifArray = []
                self.giphyCollectionView.isHidden = true
                self.loadingView.isHidden = false
                self.getGifs(gifType: "applause+actions+awww+celebrities", apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25,pageOffset: self.offset)
            }
        }
        return true
    }
    
    //MARK:- ****** closeGiphyImageView_btnAction ******
    @IBAction func closeGiphyImageView_btnAction(_ sender: Any) {
        
        UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: { _ in
            
            self.getGifURl = ""
            self.widthOfGif = 100
            self.heightOfGif = 100
            self.giphyImagesView.isHidden = true
            
        }, completion: nil)
    }
    
    // MARK: - ****** cancelSearchGif_btnAction ******
    @IBAction func cancelSearcgGif_btnAction(_ sender: Any) {
        self.searchGifView.isHidden = true
        self.searchGif_TxtField.text = ""
        self.differentiateStrForGifs = "allGifs"
        self.searchText = ""
        self.searchGif_TxtField.resignFirstResponder()
        
        if (self.differentiateStrForGifs == "allGifs")
        {
            self.differentiateStrForGifs = "allGifs"
            self.searchGifView.isHidden = true
            self.offset = 0
            self.gifArray = []
            self.searchText = ""
            self.giphyCollectionView.isHidden = true
            self.loadingView.isHidden = false
            self.getGifs(gifType: "applause+actions+awww+celebrities", apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25,pageOffset: self.offset)
        }
        else{
            self.differentiateStrForGifs = "searchNewGifs"
            
            self.searchText = self.searchGif_TxtField.text!
            self.searchText = self.searchText.replacingOccurrences(of: " ", with: "")
            
            if isValidInput(Input: self.searchText) == false {
                let alert = UIAlertController(title: "", message:"Search by name field accepts only alphabatics", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
            else{
                self.gifArray = []
                self.giphyCollectionView.isHidden = true
                self.loadingView.isHidden = false
                self.searchOffset = 0
                
                self.loadMoreBtn.isUserInteractionEnabled = false
                searchText = removeSpecialCharsFromString(text: searchText) // "pre"
                
                self.getSearchGifs(gifType: searchText, apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25, pageOffset: self.searchOffset)
            }
        }
    }
    
    
    // MARK: - ****** Load More Gif's Button Action ******
    @IBAction func loadMoreGif_btnAction(_ sender: Any) {
        self.moreScroll = false
        DispatchQueue.main.async {
            //SVProgressHUD.show(withStatus: "Loading...")
        }
        
        
        if (self.differentiateStrForGifs == "allGifs")
        {
            //+reactions+stickers+nature
            self.getGifs(gifType: "applause+actions+awww+celebrities", apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25,pageOffset: self.offset)
        }
        else{
            
            if (self.searchText != "")
            {
                self.loadMoreBtn.isUserInteractionEnabled = false
           
                searchText = removeSpecialCharsFromString(text: searchText) // "pre"
      
                self.getSearchGifs(gifType: self.searchText, apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B", limit: 25, pageOffset: self.searchOffset)
            }
            else{
                print("self.searchText is blank")
            }
            
        }
        
    }
    
    
    
    // MARK: - ****** Method For Sending Push Notification to the Post's Owner. ******
    func getGifs(gifType: String, apiKey: String, limit: Int,pageOffset: Int)
    {
        let url = URL(string: "https://api.giphy.com/v1/gifs/search?q=\(gifType)&api_key=\(apiKey)&limit=\(limit)&offset=\(pageOffset)")
        
        if (url != nil){
            URLSession.shared.dataTask(with: url!, completionHandler: {
                (data, response, error) in
                if(error != nil){
                    self.gifArray = []
                    self.loadMoreBtn.isHidden = true
                    self.loadingView.isHidden = true
                    self.giphyCollectionView.isHidden = true
                    self.noGifAvailableView.isHidden = false
                    self.loadingView.isHidden = true
                }else{
                    do{
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! NSDictionary
                      
                        let data = json.value(forKey: "data") as! NSArray
                        self.gifArray.addObjects(from: data as! [Any])
                        let pageDict:NSDictionary = json.value(forKey: "pagination") as! NSDictionary
                        
                        self.offset = pageDict.value(forKey: "offset") as! Int
                        
                        if(self.offset == 119) {
                            self.moreScroll = false
                        } else
                        {
                            self.offset = self.offset + 25
                            self.moreScroll = true
                        }
                        
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                            self.giphyCollectionView.isHidden = false
                            self.noGifAvailableView.isHidden = true
                            
                            self.loadMoreBtn.isHidden = false
                            self.giphyCollectionView.isHidden = false
                            self.loadingView.isHidden = true
                            self.giphyCollectionView.reloadData()
                        }
                        
                    }catch let error as NSError {
                        self.gifArray = []
                        self.loadMoreBtn.isHidden = true
                        self.loadingView.isHidden = true
                        self.giphyCollectionView.isHidden = true
                        self.noGifAvailableView.isHidden = false
                        self.loadingView.isHidden = true
                    }
                }
            }).resume()
        }
    }
    
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_'".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    // MARK: - ****** Method For Sending Push Notification to the Post's Owner. ******
    func getSearchGifs(gifType: String, apiKey: String, limit: Int,pageOffset: Int)
    {
        if (pageOffset >= 0)
        {
            let url = URL(string: "https://api.giphy.com/v1/gifs/search?q=\(gifType)&api_key=\(apiKey)&limit=\(limit)&offset=\(pageOffset)")
    
            if (url != nil){
                URLSession.shared.dataTask(with: url!, completionHandler: {
                    (data, response, error) in
                    if(error != nil)
                    {
                        self.gifArray = []
                        self.loadMoreBtn.isHidden = true
                        self.loadingView.isHidden = true
                        self.giphyCollectionView.isHidden = true
                        self.noGifAvailableView.isHidden = false
                        self.giphyCollectionView.isHidden = true
                        self.loadingView.isHidden = true
                        
                        self.loadMoreBtn.isUserInteractionEnabled = true
                        
                    }
                    else
                    {
                        do{
                            let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! NSDictionary
         
                            if (json["data"] != nil)
                            {
                                let data = json.value(forKey: "data") as! NSArray
                                
                                if (data != [])
                                {
                                    self.gifArray.addObjects(from: data as! [Any])
                                    let pageDict:NSDictionary = json.value(forKey: "pagination") as! NSDictionary
                                    self.searchOffset = pageDict.value(forKey: "offset") as! Int
                                    
                                    if(self.searchOffset == 119) {
                                        self.moreScroll = false
                                    } else {
                                        self.searchOffset = self.searchOffset + 25
                                        self.moreScroll = true
                                    }
                                    
                                    
                                    /* OperationQueue.main.addOperation({
                                     self.loadMoreBtn.isHidden = false
                                     self.giphyCollectionView.isHidden = false
                                     self.loadingView.isHidden = true
                                     self.loadMoreBtn.isUserInteractionEnabled = true
                                     self.giphyCollectionView.reloadData()
                                     })*/
                                    
                                    DispatchQueue.main.async {
                                        self.loadMoreBtn.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        self.giphyCollectionView.isHidden = false
                                        self.noGifAvailableView.isHidden = true
                                        
                                        
                                        self.loadMoreBtn.isHidden = false
                                        self.giphyCollectionView.isHidden = false
                                        self.loadingView.isHidden = true
                                        self.loadMoreBtn.isUserInteractionEnabled = true
                                        self.giphyCollectionView.reloadData()
                                        
                                    }
                                }
                                else {
                                    self.gifArray = []
                                    self.loadMoreBtn.isHidden = true
                                    self.loadingView.isHidden = true
                                    self.giphyCollectionView.isHidden = true
                                    self.noGifAvailableView.isHidden = false
                                    self.loadMoreBtn.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                }
                            }
                            else{
                                self.gifArray = []
                                self.loadMoreBtn.isHidden = true
                                self.loadingView.isHidden = true
                                self.giphyCollectionView.isHidden = true
                                self.noGifAvailableView.isHidden = false
                                self.loadingView.isHidden = true
                                self.loadMoreBtn.isUserInteractionEnabled = true
                            }
                            
                        }catch let error as NSError {
                            self.gifArray = []
                            self.loadMoreBtn.isHidden = true
                            self.loadingView.isHidden = true
                            self.giphyCollectionView.isHidden = true
                            self.noGifAvailableView.isHidden = false
                            self.loadingView.isHidden = true
                            self.loadMoreBtn.isUserInteractionEnabled = true
                        }
                    }
                }).resume()
            }else{
               
            }
            
        }
    }
    
    
    
    
    // MARK: - ****** findAllGif ******
    func findAllGif(gifType: String, apiKey: String, limit: Int,pageOffset: Int)
    {
        let giphy = GPHClient.init(apiKey: "8bJM8hW7qX0H3Dc0AsnOPUYv9NZRAx6B")
        let op1 =  giphy.categoriesForGifs(self.offset, limit: 25, sort: "", completionHandler: { (response, error) in
            
            if let error = error as NSError? {
                // Do what you want with the error
            }
            
            if let response = response, let data = response.data, let pagination = response.pagination {
                
                for result in data {
//                    print("result = \(result)")
                }
            } else {
                print("No Top Categories Found")
            }
        })
    }
    
    func mediaCapturedforCreateCampaign(value : Notification)
    {
        let dic = value.object as! Dictionary<String, Any>
        
        media_uploadedFrom = dic["media_uploadedFrom"] as! String
        
        self.mediaType = dic["mediaType"] as! String
        
        if mediaType  == "public.image" {
            if (dic["capturedImage"] != nil)
            {
                let chosenImage: UIImage = dic["capturedImage"] as! UIImage
                self.confessionType_MyImage.image = chosenImage
                self.imagePickedBool = true
                self.photoUploadType = "photo"
            }
        }
    }
    
    @IBAction func selectMediaOption_BtnAction(_ sender: Any) {
        
        self.writeReply_txtView.resignFirstResponder()
        
        let actionSheet = UIAlertController(title: "Please select your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Add a gif in wi post", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.writeReply_txtView.resignFirstResponder()
            
            UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromTop, animations: { _ in
                
                self.giphyImagesView.isHidden = false
                
            }, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Add a photo in wi post", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.writeReply_txtView.resignFirstResponder()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if #available(iOS 10.0, *) {
                let homeScreen = storyboard.instantiateViewController(withIdentifier: "CameraOverlayViewController") as! CameraOverlay
                homeScreen.parentClass = self
                homeScreen.differentiateStr = "WiCreateConfessionViewController"
                self.navigationController?.present(homeScreen, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
   
    
    @IBAction func postReply_btnActuon(_ sender: Any) {
        
        self.writeReply_txtView.resignFirstResponder()
        
        if ((self.writeReply_txtView.text == "") && (self.imagePickedBool == false))
        {
            let alert = UIAlertController(title: "", message: "Please add status/select media to upload.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            var textMessage = String()
            
            if (self.writeReply_txtView.text != "")
            {
                textMessage = self.writeReply_txtView.text!
                if (textMessage.trimmingCharacters(in: .whitespaces).isEmpty == false)
                {
                    var search_txt: String = self.writeReply_txtView.text
                    search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                    
                    if self.checkReplyType == "replyOnPost"
                    {
                        self.createConfessionReplyAndSave(enteredText: search_txt, completion: { (result) in
                            if (result == true)
                            {
                                self.getRepliesListing(completion: { (reponsebool) in
                                    
                                })
                            }
                        })
                    }
                    else if self.checkReplyType == "replyOnReply"
                    {
                        self.createConfessionFurtherReplyAndSave(enteredText: search_txt, completion: { (result) in
                            if (result == true)
                            {
                                self.getRepliesListing(completion: { (reponsebool) in
                                    
                                })
                            }
                        })
                    }
                }
                else
                {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "", message: "Please enter a text to post a wipost.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                var search_txt: String = self.writeReply_txtView.text
                search_txt = search_txt.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
                
                if self.checkReplyType == "replyOnPost"
                {
                    self.createConfessionReplyAndSave(enteredText: search_txt, completion: { (result) in
                        if (result == true)
                        {
                            self.getRepliesListing(completion: { (reponsebool) in
                                
                            })
                        }
                    })
                }
                else if self.checkReplyType == "replyOnReply"
                {
                    self.createConfessionFurtherReplyAndSave(enteredText: search_txt, completion: { (result) in
                        if (result == true)
                        {
                            self.getRepliesListing(completion: { (reponsebool) in
                                
                            })
                        }
                    })
                }
            }
        }
    }
    
    // MARK: - ****** Rotate Image with Fixed Orientation. ******
    func rotateImageFix(image: UIImage) -> UIImage {
        
        if (image.imageOrientation == UIImageOrientation.up ) {
            return image
        }
        
        UIGraphicsBeginImageContext(image.size)
        
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return copy!
    }
    
    
    
    func createConfessionReplyAndSave(enteredText: String,completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    var typeOfFileofSave = String()
                    var typeOfConfessionStr = String()
                    var mediaExtension = String()
                    if self.photoUploadType == "photo"
                    {
                        typeOfFileofSave = "image/jpg"
                        typeOfConfessionStr = "photo"
                        mediaExtension = "jpg"
                    }
                    else if self.photoUploadType == "gif"
                    {
                        typeOfFileofSave = "image/gif"
                        typeOfConfessionStr = "gif"
                        mediaExtension = "gif"
                    }
                    else{
                        typeOfFileofSave = ""
                        typeOfConfessionStr = "text"
                        mediaExtension = ""
                    }
                    
                    let date = NSDate();
                    let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
                    // set upload path
                    let filePath = "\(currentTimeStamp).\(mediaExtension)"
                  
                   if (self.confessionType_MyImage.image != nil) && ((typeOfConfessionStr == "photo") || (typeOfConfessionStr == "gif"))
                    {
                        var image = UIImage()
//                        var animatedImage = FLAnimatedImage()
                        
                        if self.photoUploadType == "photo"
                        {
                            image = self.rotateImageFix(image: self.confessionType_MyImage.image!)
                        }
                        else if self.photoUploadType == "gif"
                        {
//                            animatedImage = self.confessionType_MyImage.animatedImage
                        }
                        
                        ApiHandler.createReplyConfession(userID: userID, authToken: authToken, mood: "", duration: "24", confessionImage: image, text: enteredText, typeOfConfession: typeOfConfessionStr, nameOfFile: filePath, typeOfFile: typeOfFileofSave, animatedGif: self.getGifURl, parent_comment_id: "", confession_id: self.postIDStr,image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
//                            print("responseData = \(responseData)")
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    self.writeReply_txtView.text = ""
                                    self.imagePickedBool = false
                                    self.confessionType_MyImage.image = nil
                                    
                                    if let profileImage = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                                    {
                                        DispatchQueue.main.async {
                                            self.confessionType_MyImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "Default Icon"))
                                        }
                                    }
                                    
                                    self.confessionType_MyImage.contentMode = UIViewContentMode.scaleAspectFill
                                    
                                    if (self.typeOfConfession == "anonymous")
                                    {
                                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                        blurEffectView.frame = self.confessionType_MyImage.bounds
                                    self.confessionType_MyImage.addSubview(blurEffectView)
                                    }
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                    else
                    {
                        let image = UIImage()
                        ApiHandler.createReplyConfession(userID: userID, authToken: authToken, mood: "", duration: "24", confessionImage: image, text: enteredText, typeOfConfession: typeOfConfessionStr, nameOfFile: "", typeOfFile: typeOfFileofSave, animatedGif: "", parent_comment_id: "", confession_id: self.postIDStr,image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
//                            print("responseData = \(responseData)")
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                
                                    self.writeReply_txtView.text = ""
                                    self.confessionType_MyImage.image = nil
                                    self.imagePickedBool = false
                                    
                                    if let profileImage = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                                    {
                                        DispatchQueue.main.async {
                                            self.confessionType_MyImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "Default Icon"))
                                        }
                                    }
                                    
                                    self.confessionType_MyImage.contentMode = UIViewContentMode.scaleAspectFill
                                    
                                    if (self.typeOfConfession == "anonymous")
                                    {
                                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                                        blurEffectView.frame = self.confessionType_MyImage.bounds
                                        self.confessionType_MyImage.addSubview(blurEffectView)
                                    }
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
            }
        }
    }
    
    
    func createConfessionFurtherReplyAndSave(enteredText: String,completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    var typeOfFileofSave = String()
                    var typeOfConfessionStr = String()
                    var mediaExtension = String()
                    if self.photoUploadType == "photo"
                    {
                        typeOfFileofSave = "image/jpg"
                        typeOfConfessionStr = "photo"
                        mediaExtension = "jpg"
                    }
                    else if self.photoUploadType == "gif"
                    {
                        typeOfFileofSave = "image/gif"
                        typeOfConfessionStr = "gif"
                        mediaExtension = "gif"
                    }
                    else{
                        typeOfFileofSave = ""
                        typeOfConfessionStr = "text"
                        mediaExtension = ""
                    }
                    
                    let date = NSDate();
                    let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
                    // set upload path
                    let filePath = "\(currentTimeStamp).\(mediaExtension)"
                    
                if (self.confessionType_MyImage.image != nil) && ((typeOfConfessionStr == "photo") || (typeOfConfessionStr == "gif"))
                {
                    var image = UIImage()
//                        var animatedImage = FLAnimatedImage()
                        if self.photoUploadType == "photo"
                        {
                            image = self.rotateImageFix(image: self.confessionType_MyImage.image!)
                        }
                        else if self.photoUploadType == "gif"
                        {
//                            animatedImage = self.confessionType_MyImage.animatedImage
                        }
                    
                        ApiHandler.createReplyConfession(userID: userID, authToken: authToken, mood: "", duration: "24", confessionImage: image, text: enteredText, typeOfConfession: typeOfConfessionStr, nameOfFile: filePath, typeOfFile: typeOfFileofSave, animatedGif: self.getGifURl, parent_comment_id: self.replyIDStr, confession_id: self.postIDStr,image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
//                            print("responseData = \(responseData)")
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                  
                                    self.writeReply_txtView.text = ""
                                    self.confessionType_MyImage.image = nil
//                                    self.confessionType_MyImage.image = UIImage(named: "Open Camera Icon")
                                    
                                    self.writeReply_txtView.text = ""
                                    self.imagePickedBool = false
                                    self.confessionType_MyImage.image = nil
                                    
                                    if let profileImage = UserDefaults.standard.value(forKey: "userProfilePicture") as? String
                                    {
                                        DispatchQueue.main.async {
                                            self.confessionType_MyImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "Default Icon"))
                                        }
                                    }
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                }
                else
                {
                    let image = UIImage()
                    
                    ApiHandler.createReplyConfession(userID: userID, authToken: authToken, mood: "", duration: "24", confessionImage: image, text: enteredText, typeOfConfession: typeOfConfessionStr, nameOfFile: "", typeOfFile: typeOfFileofSave, animatedGif: "", parent_comment_id: self.replyIDStr, confession_id: self.postIDStr,image_height: self.heightOfGif,image_width:self.widthOfGif, typeOfMedia: typeOfConfessionStr, completion: { (responseData) in
                            
//                            print("responseData = \(responseData)")
                        
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                                    
                                    self.writeReply_txtView.text = ""
                                    self.confessionType_MyImage.image = nil
                                    self.confessionType_MyImage.image = UIImage(named: "Open Camera Icon")
                                   
                                }))
                                self.present(alert, animated: true, completion: nil)
                                completion(true)
                            }
                            else if (responseData.value(forKey: "status") as? String == "400")
                            {
                                let message = responseData.value(forKey: "message") as? String
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                            else
                            {
                                self.view.isUserInteractionEnabled = true
                                self.loadingView.isHidden = true
                                let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
            }
        }
    }
    
    

    
    //MARK: ****** ViewDidAppear. ******
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(WiRepliesViewController.showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(WiRepliesViewController.hideKeyboard(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK: ****** NotificationCenter handlers ******
    func hideKeyboard(notification: Notification) {
        self.writeReplyView.frame.origin.y = self.repliesTableView.frame.origin.y + self.repliesTableView.frame.size.height
        self.repliesTableView.contentInset = UIEdgeInsets.zero
        self.repliesTableView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func showKeyboard(notification: Notification) {
        
        if (notification.userInfo != nil)
        {
            if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let height = frame.cgRectValue.height
                self.writeReplyView.frame.origin.y = frame.cgRectValue.origin.y - self.writeReplyView.frame.size.height
                self.repliesTableView.contentInset.bottom = height
                self.repliesTableView.scrollIndicatorInsets.bottom = height
            }
        }
    }
    
    // MARK: - ****** UITextView Delegate. ******
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let maxCharacter: Int = 500
        return (textView.text?.utf16.count ?? 0) + text.utf16.count - range.length <= maxCharacter
    }
    
    public func textViewDidChange(_ textView: UITextView)
    {
        if textView.text.count > 0
        {
            self.placeHolderTextView.isHidden = true
        }
        else{
            self.placeHolderTextView.isHidden = false
        }
    }
  
    
    //MARK:- ****** viewWillAppear ******
    override func viewWillAppear(_ animated: Bool) {
        
        self.showImagefullScreenView.isHidden = true
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
           
            self.getRepliesListing(completion: { (reponsebool) in
                
            })
            
        }
    }
    
    
    func getSingleReplyDetailsFromDatabase(typeOfReply: String,confessionID: String,replyIDStr: String,parentReplyID: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
           
            if (confessionID != "")
            {
                if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                    {
                        ApiHandler.getSingleMediaDetails(user_id: userID, auth_token: authToken, mediaID: confessionID, typeOfMedia: "reply", completion: { (responseData) in
                            
                            if (responseData.value(forKey: "status") as? String == "200")
                            {
                                let userData = responseData.value(forKey: "data") as! NSDictionary
                                
                                if (userData != nil)
                                {
                                    let timestamp: String = userData.value(forKey: "created") as! String
                                    var loc = String()
                                    var age = String()
                                    var gender = String()
                                    var username = String()
                                    var userPic = String()
                                    var checkUserExists = String()
                                    
//                                    let userID: String = userData.value(forKey: "user_id") as! String
                                    let postedImage: String = userData.value(forKey: "image") as! String
                                    
                                    let likeCountInt: Int = userData.value(forKey: "likes_count") as! Int
                                    let likeCountStr : String = String(likeCountInt)
                                    let likedby_current: String = userData.value(forKey: "likedby_current") as! String
                                    let replyCountStr : Int = userData.value(forKey: "comments_count") as! Int
                                  /*  let total_duration: String = userData.value(forKey: "total_duration") as! String
                                    let statusAddedStr: String = userData.value(forKey: "text") as! String
                                    let postID: String = userData.value(forKey: "id") as! String
                                    let mood: String = userData.value(forKey: "mood") as! String
                                    let duration: String = userData.value(forKey: "duration") as! String*/
                                    
                                    let image_height: String = userData.value(forKey: "image_height") as! String
                                    let image_width: String = userData.value(forKey: "image_width") as! String
                                    
                                    var image_height_int = Int()
                                    var image_width_int = Int()
                                    if (image_height == "")
                                    {
                                        image_height_int = 250
                                    }
                                    else{
                                        image_height_int = Int(image_height)!
                                    }
                                    
                                    if (image_width == "")
                                    {
                                        image_width_int = 250
                                    }
                                    else{
                                        image_width_int = Int(image_width)!
                                    }
                                    
                                    var usersDetailsDict = NSDictionary()
                                    if (userData["user"] != nil)
                                    {
                                        usersDetailsDict = (userData.value(forKey: "user") as? NSDictionary)!
                                        
                                        checkUserExists = usersDetailsDict.value(forKey: "found") as! String
                                        
                                        if (checkUserExists == "no")
                                        {
                                            return
                                        }
                                        
                                        age = String(usersDetailsDict.value(forKey: "Age") as! Int)
                                        loc = usersDetailsDict.value(forKey: "Location") as! String
                                        gender = usersDetailsDict.value(forKey: "gender") as! String
                                        
                                        username = usersDetailsDict.value(forKey: "username") as! String
                                        userPic = usersDetailsDict.value(forKey: "profile_pic") as! String
                                    }
                                    else
                                    {
                                        age = ""
                                        loc = ""
                                        gender = ""
                                        username = ""
                                        userPic = ""
                                    }
                                    
                                    
                                    if (postedImage != "")
                                    {
                                        let imageURL = postedImage
                                        print("imageURL = \(imageURL)")
                                        if (postedImage.contains("jpg"))
                                        {
                                            self.fullImageView.sd_setImage(with: URL(string: postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                                                
                                                if ((image?.size.height) != nil) && ((image?.size.width) != nil)
                                                {
                                                    self.showImagefullScreenView.isHidden = false
                                                    self.fullImageAnimatedGif.isHidden = true
                                                    self.fullImageView.isHidden = false
                                                    
                                                    if (image?.size.height)! <= (self.repliesGifWrapperView.frame.size.height)
                                                    {
                                                        var gifWidth = Int()
                                                        var screenRes = Int()
                                                        var newXPos =  Int()
                                                        
                                                        if (image?.size.width)! > (self.view.frame.size.width)
                                                        {
                                                            gifWidth = Int((image?.size.width)!/2)
                                                            screenRes = Int(self.view.frame.width/2)
                                                            newXPos =  0
                                                        }
                                                        else{
                                                            gifWidth = Int((image?.size.width)!/2)
                                                            screenRes = Int(self.view.frame.width/2)
                                                            newXPos =  screenRes - gifWidth
                                                        }
                                                        
                                                        
                                                        let gifHeight = Int((image?.size.height)!/2)
                                                        let heightOfWrraperView = Int(self.repliesGifWrapperView.frame.height/2)
                                                        let newYPos: Int =  heightOfWrraperView - gifHeight
                                                        
                                                        self.repliesGifWrapperView.frame.origin.y = 0
                                                        self.fullImageView.frame.origin.y = CGFloat(newYPos)
                                                        self.fullImageView.frame.origin.x = CGFloat(newXPos)
                                                        
                                                        if (image?.size.width)! > (self.view.frame.size.width)
                                                        {
                                                            self.fullImageView.frame.size.width = self.view.frame.size.width
                                                        }
                                                        else{
                                                            self.fullImageView.frame.size.width = (image?.size.width)!
                                                        }
                                                        
                                                        if (image?.size.height)! <= (self.repliesGifWrapperView.frame.size.height)
                                                        {
                                                            self.fullImageView.frame.size.height = (image?.size.height)!
                                                        }
                                                        else{
                                                            self.fullImageView.frame.size.height = self.repliesGifWrapperView.frame.size.height
                                                        }
                                                        
                                                        self.imagesButtonWrapperView.frame.origin.y = self.fullImageView.frame.origin.y + self.fullImageView.frame.size.height + 10
                                                        self.repliesGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                                                    }
                                                    else{
                                                        self.fullImageView.frame.origin.y = 21
                                                        self.fullImageView.frame.size.height = (self.repliesGifWrapperView.frame.size.height - 21)
                                                        
                                                        self.imagesButtonWrapperView.frame.origin.y = self.repliesGifWrapperView.frame.origin.y + self.repliesGifWrapperView.frame.size.height + 10
                                                        self.repliesGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                                                    }
                                                    
                                                    self.fullImageView.layer.borderWidth = 2.0
                                                    self.fullImageView.layer.masksToBounds = false
                                                    self.fullImageView.layer.borderColor = UIColor.lightGray.cgColor
                                                    self.fullImageView.contentMode = UIViewContentMode.scaleAspectFit
                                                }
                                                else
                                                {
                                                    let alert = UIAlertController(title: "", message: "Please wait.", preferredStyle: UIAlertControllerStyle.alert)
                                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                                    self.present(alert, animated: true, completion: nil)
                                                }
                                            }
                                        }
                                        else
                                        {
                                            self.showImagefullScreenView.isHidden = false
                                            
                                            self.fullImageView.isHidden = true
                                            self.fullImageAnimatedGif.isHidden = false
                                            
                                            DispatchQueue.main.async {
                                                self.fullImageAnimatedGif.sd_setImage(with: URL(string: postedImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions.refreshCached)
                                            }
                                            
                                            self.fullImageAnimatedGif.layer.borderWidth = 2.0
                                            self.fullImageAnimatedGif.layer.masksToBounds = false
                                            self.fullImageAnimatedGif.layer.borderColor = UIColor.lightGray.cgColor
                                            self.fullImageAnimatedGif.contentMode = UIViewContentMode.scaleAspectFit
                                            
                                            var gifWidth = Int()
                                            var screenRes = Int()
                                            var newXPos =  Int()
                                            if (image_width_int) > Int(self.view.frame.size.width)
                                            {
                                                gifWidth = Int(image_width_int/2)
                                                screenRes = Int(self.view.frame.width/2)
                                                newXPos =  0
                                            }
                                            else{
                                                gifWidth = Int(image_width_int/2)
                                                screenRes = Int(self.view.frame.width/2)
                                                newXPos =  screenRes - gifWidth
                                            }
                                            
                                            let gifHeight = Int(image_height_int/2)
                                            let heightOfWrraperView = Int(self.repliesGifWrapperView.frame.height/2)
                                            let newYPos: Int =  heightOfWrraperView - gifHeight
                                            
                                            self.repliesGifWrapperView.frame.origin.y = 0
                                            self.fullImageAnimatedGif.frame.origin.y = CGFloat(newYPos)
                                            self.fullImageAnimatedGif.frame.origin.x = CGFloat(newXPos)
                                            
                                            if (image_width_int) > Int(self.view.frame.size.width)
                                            {
                                                self.fullImageAnimatedGif.frame.size.width = self.view.frame.size.width
                                            }
                                            else{
                                                self.fullImageAnimatedGif.frame.size.width = CGFloat(image_width_int)
                                            }
                                            
                                            self.fullImageAnimatedGif.frame.size.height = CGFloat(image_height_int)
                                            
                                            self.imagesButtonWrapperView.frame.origin.y = self.fullImageAnimatedGif.frame.origin.y + self.fullImageAnimatedGif.frame.size.height + 10
                                            self.repliesGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                                        }
                                    }
                                    
                                    
                                    DispatchQueue.main.async {
                                            if (likedby_current == "yes")
                                            {
                                                self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            }
                                            else
                                            {
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            }
                                    }
                                    
                                    if likeCountStr == "0"
                                    {
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        self.confessionLikesLbl.text = likeCountStr
                                    }
                                    
                                    if replyCountStr == 0
                                    {
                                        self.confessionReply.text = ""
                                    }
                                    else if replyCountStr > 0
                                    {
                                        self.confessionReply.text = "\(replyCountStr)"
                                    }
                                    else
                                    {
                                        self.confessionReply.text = ""
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }
    }
  
    //MARK:- ****** viewDidDisappear ******
    override func viewDidDisappear(_ animated: Bool) {
        
    }
  
    
    //MARK:- ****** closeFullSize_btnAction ******
    @IBAction func closeFullSize_btnAction(_ sender: Any) {
        self.showImagefullScreenView.isHidden = true
    }
    
   
    //MARK:- ****** confessionsDropDown_Btn ******
    @IBAction func confessionsDropDown_Btn(_ sender: UIButton) {
        
        if (self.allRepliesArray.count > 0)
        {
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    print("authToken = \(authToken)")
                    if (self.allRepliesArray[self.indexAtClicked].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.allRepliesArray[self.indexAtClicked].postedImage != "")
                        {
                            let imageURL = self.allRepliesArray[self.indexAtClicked].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        print("typeOfCOnfession = \(typeOfCOnfession)")
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: self.allRepliesArray[self.indexAtClicked].postIDStr, status: self.allRepliesArray[self.indexAtClicked].statusAdded, backgroudImageStr: self.allRepliesArray[self.indexAtClicked].postedImage, moodTypeImageStr: self.allRepliesArray[self.indexAtClicked].moodTypeStr, timeFrameStr: self.allRepliesArray[self.indexAtClicked].postHours, moodTypeStr: self.allRepliesArray[self.indexAtClicked].moodTypeStr, moodTypeUrl: self.allRepliesArray[self.indexAtClicked].moodTypeStr, photoTypeStr: typeOfCOnfession, replyIDStr: self.allRepliesArray[self.indexAtClicked].replyID, replyToReplyIDStr: self.allRepliesArray[self.indexAtClicked].furtherReplyID, getTotalLikeCount: self.allRepliesArray[self.indexAtClicked].likesCount, getTotalReplyCount: self.allRepliesArray[self.indexAtClicked].replyCount, gifWidth: self.allRepliesArray[self.indexAtClicked].width, gifHeight: self.allRepliesArray[self.indexAtClicked].height)
                    }
                    else
                    {
                        self.showActionSheetForDropDownMenu_otherUser(postID: self.allRepliesArray[self.indexAtClicked].postIDStr, ownerIDStr: self.allRepliesArray[self.indexAtClicked].userIDStr, replyID: self.allRepliesArray[self.indexAtClicked].replyID, furtherReplyID: self.allRepliesArray[self.indexAtClicked].furtherReplyID, myID: userID)
                    }
                }
            }
        }
 
    }
    
    
    //MARK:- ****** confessionMessage_btn ******
    @IBAction func confessionMessage_btn(_ sender: UIButton)
    {
        let userOwnerIDStr =  self.allRepliesArray[self.indexAtClicked].userIDStr
        if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if id == userOwnerIDStr
            {
                print("owner of the post and current user is same")
            }
            else
            {
                let ownerProfileImage = UIImageView()
                ownerProfileImage.sd_setImage(with: URL(string: self.allRepliesArray[self.indexAtClicked].postedImage), placeholderImage: UIImage(named: "Default Icon"))
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                
                messagesScreen.mainConfessionCheck = "comingFromReplies"
                
                messagesScreen.chatUserDifferentiateStr = "Messages"
                
                messagesScreen.viewOpponentProfile = "fromStatusScreen"
                
                let userIDStr =  self.allRepliesArray[self.indexAtClicked].userIDStr
                
                messagesScreen.opponentName = self.allRepliesArray[self.indexAtClicked].name
                messagesScreen.opponentImage = self.allRepliesArray[self.indexAtClicked].profilePic
                messagesScreen.opponentUserID = userIDStr
                messagesScreen.opponentFirebaseUserID = self.allRepliesArray[self.indexAtClicked].firebase_user_id
                messagesScreen.postID = self.allRepliesArray[self.indexAtClicked].postIDStr
                messagesScreen.opponentProfilePic =  ownerProfileImage.image!
                
                if (self.typeOfConfession == "anonymous")
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                }
                else
                {
                    messagesScreen.typeOfUser = "WithProfileUser"
                    messagesScreen.checkVisibiltyStr = "withProfileChat"
                }
                
                messagesScreen.mainConfessionCheck = "comingFromReplies"
                
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }
        }
      
    }
    
    
    //MARK:- ****** confessionLikeReply_btnAction ******
    @IBAction func confessionLikeReply_btnAction(_ sender: UIButton) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            
            if self.checkReplyType == "replyOnPost"
            {
                if (self.allRepliesArray.count > 0)
                {
                    self.likeDislikeConfessions(confessionID: self.allRepliesArray[self.indexAtClicked].replyID, completion: { (responseBool) in
                        
                        if (responseBool == "Liked successfully.")
                        {
                            self.indexPathSelected = IndexPath()
                            self.indexPathSelected = IndexPath(row: self.indexAtClicked, section: 0)
                            
                            self.latestIntArr.add(self.indexPathSelected.row)
                            
                            var typeOfCOnfession = String()
                            if (self.allRepliesArray[self.indexAtClicked].postedImage != "")
                            {
                                let imageURL = self.allRepliesArray[self.indexAtClicked].postedImage
                                if (imageURL.contains("jpg"))
                                {
                                    typeOfCOnfession = "photo"
                                }
                                else
                                {
                                    typeOfCOnfession = "gif"
                                }
                            }
                            else
                            {
                                typeOfCOnfession = "text"
                            }
                            
                            if (typeOfCOnfession == "text")
                            {
                                let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                                
                                DispatchQueue.main.async {
                                    
                                    onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    
                                    if  (likesCountStr == "0")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                        self.allRepliesArray[self.indexAtClicked].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            let totalLikes = 1
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = "1"
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = "1"
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! + 1
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                            else
                            {
                                let creditsCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                                
                                DispatchQueue.main.async {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    
                                    if (likesCountStr == "0")
                                    {
                                        creditsCell.totalLikesCountLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            
                                        }
                                        else
                                        {
                                            
                                        }
                                    }
                                    
                                    if  (likesCountStr == "0")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                        self.allRepliesArray[self.indexAtClicked].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            let totalLikes = 1
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = "1"
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = "1"
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! + 1
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                        else if (responseBool == "Unliked successfully.")
                        {
                            self.indexPathSelected = IndexPath()
                            self.indexPathSelected = IndexPath(row: self.indexAtClicked, section: 0)
                            
                            self.latestIntArr.remove(self.indexPathSelected.row)
                            
                            var typeOfCOnfession = String()
                            if (self.allRepliesArray[self.indexAtClicked].postedImage != "")
                            {
                                let imageURL = self.allRepliesArray[self.indexAtClicked].postedImage
                                if (imageURL.contains("jpg"))
                                {
                                    typeOfCOnfession = "photo"
                                }
                                else
                                {
                                    typeOfCOnfession = "gif"
                                }
                            }
                            else
                            {
                                typeOfCOnfession = "text"
                            }
                            
                            
                            if (typeOfCOnfession == "text")
                            {
                                let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                                
                                DispatchQueue.main.async {
                                    onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    
                                    if (likesCountStr == "0")
                                    {
                                        onlineUsersCell.likeCountLbl.text = ""//String(totalLikes)
                                        self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                        self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            onlineUsersCell.likeCountLbl.text = ""// String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                onlineUsersCell.likeCountLbl.text  = ""
                                                
                                                self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                                self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = ""
                                            }
                                            else
                                            {
                                                onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                                
                                                self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                                self.allRepliesArray[self.indexAtClicked].likesCount = String(totalLikes)
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = String(totalLikes)
                                            }
                                            
                                        }
                                    }
                                }
                            }
                            else
                            {
                                let creditsCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                                
                                DispatchQueue.main.async {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    
                                    if (likesCountStr == "0")
                                    {
                                        creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                        
                                        self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                        self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                creditsCell.totalLikesCountLbl.text  = ""
                                                
                                                self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                                self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = ""
                                            }
                                            else
                                            {
                                                creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                                
                                                self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                                self.allRepliesArray[self.indexAtClicked].likesCount = String(totalLikes)
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            
                        }
                    })
                }
            }
            else if self.checkReplyType == "replyOnReply"
            {
                if (self.allRepliesArray.count > 0)
                {
                    self.likeDislikeConfessions(confessionID: self.allRepliesArray[self.indexAtClicked].furtherReplyID, completion: { (responseBool) in
                        
                        if (responseBool == "Liked successfully.")
                        {
                            self.indexPathSelected = IndexPath()
                            self.indexPathSelected = IndexPath(row: self.indexAtClicked, section: 0)
                            
                            self.latestIntArr.add(self.indexPathSelected.row)
                            
                            var typeOfCOnfession = String()
                            if (self.allRepliesArray[self.indexAtClicked].postedImage != "")
                            {
                                let imageURL = self.allRepliesArray[self.indexAtClicked].postedImage
                                if (imageURL.contains("jpg"))
                                {
                                    typeOfCOnfession = "photo"
                                }
                                else
                                {
                                    typeOfCOnfession = "gif"
                                }
                            }
                            else
                            {
                                typeOfCOnfession = "text"
                            }
                            
                            if (typeOfCOnfession == "text")
                            {
                                let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                                
                                DispatchQueue.main.async {
                                    
                                    onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    
                                    if  (likesCountStr == "0")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                        self.allRepliesArray[self.indexAtClicked].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            let totalLikes = 1
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = "1"
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = "1"
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! + 1
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                            else
                            {
                                let creditsCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                                
                                DispatchQueue.main.async {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    
                                    if (likesCountStr == "0")
                                    {
                                        creditsCell.totalLikesCountLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            
                                        }
                                        else
                                        {
                                            
                                        }
                                    }
                                    
                                    if  (likesCountStr == "0")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                        self.allRepliesArray[self.indexAtClicked].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            let totalLikes = 1
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = "1"
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = "1"
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! + 1
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "yes"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                        else if (responseBool == "Unliked successfully.")
                        {
                            self.indexPathSelected = IndexPath()
                            self.indexPathSelected = IndexPath(row: self.indexAtClicked, section: 0)
                            
                            self.latestIntArr.remove(self.indexPathSelected.row)
                            
                            var typeOfCOnfession = String()
                            if (self.allRepliesArray[self.indexAtClicked].postedImage != "")
                            {
                                let imageURL = self.allRepliesArray[self.indexAtClicked].postedImage
                                if (imageURL.contains("jpg"))
                                {
                                    typeOfCOnfession = "photo"
                                }
                                else
                                {
                                    typeOfCOnfession = "gif"
                                }
                            }
                            else
                            {
                                typeOfCOnfession = "text"
                            }
                            
                            
                            if (typeOfCOnfession == "text")
                            {
                                let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                                
                                DispatchQueue.main.async {
                                    onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    
                                    if (likesCountStr == "0")
                                    {
                                        onlineUsersCell.likeCountLbl.text = ""//String(totalLikes)
                                        self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                        self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            onlineUsersCell.likeCountLbl.text = ""// String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                onlineUsersCell.likeCountLbl.text  = ""
                                                
                                                self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                                self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = ""
                                            }
                                            else
                                            {
                                                onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                                
                                                self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                                self.allRepliesArray[self.indexAtClicked].likesCount = String(totalLikes)
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = String(totalLikes)
                                            }
                                            
                                        }
                                    }
                                }
                            }
                            else
                            {
                                let creditsCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                                
                                DispatchQueue.main.async {
                                    creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    
                                    if (likesCountStr == "0")
                                    {
                                        creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                        
                                        self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                        self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                            
                                            self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                            self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                creditsCell.totalLikesCountLbl.text  = ""
                                                
                                                self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                                self.allRepliesArray[self.indexAtClicked].likesCount = ""
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = ""
                                            }
                                            else
                                            {
                                                creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                                
                                                self.allRepliesArray[self.indexAtClicked].likedByMeStatus = "no"
                                                self.allRepliesArray[self.indexAtClicked].likesCount = String(totalLikes)
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            
                        }
                    })
                }
            }
        }
    }
    
    
    //MARK:- ****** confessionReply_btn ******
    @IBAction func confessionReply_btn(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
        
        if self.checkReplyType == "replyOnPost"
        {
            homeScreen.postIdOwnerStr = self.allRepliesArray[self.indexAtClicked].userIDStr
            homeScreen.postIDStr = self.allRepliesArray[self.indexAtClicked].postIDStr
            homeScreen.replyIDStr = self.allRepliesArray[self.indexAtClicked].replyID
            homeScreen.checkReplyType = "replyOnReply"
            homeScreen.differentiateTypereplyStr = "firstReplyStr"
            
        }
        else if self.checkReplyType == "replyOnReply"
        {
            homeScreen.postIdOwnerStr = self.allRepliesArray[self.indexAtClicked].userIDStr
            homeScreen.postIDStr = self.allRepliesArray[self.indexAtClicked].replyID
            homeScreen.replyIDStr = self.allRepliesArray[self.indexAtClicked].furtherReplyID
            homeScreen.checkReplyType = "replyOnReply"
            homeScreen.differentiateTypereplyStr = "furtherReply"
        }
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }
    
    
    
    // MARK: - ****** UITableView Delegate & Data Source Methods. ******
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.allRepliesArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.repliesTableView.separatorColor = UIColor.clear
        if (self.allRepliesArray[indexPath.row].postedImage != "")
        {
            let imageURL = self.allRepliesArray[indexPath.row].postedImage
            if (imageURL.contains("jpg"))
            {
                let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath) as! CreditPackagesTableViewCell
                
                if (self.allRepliesArray.count <= 0)
                {
                    return creditsCell
                }
                
                creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                creditsCell.animatedGifImage.isHidden = true
                creditsCell.tag = indexPath.row
                creditsCell.confessionLikeIcon.tag = indexPath.row
                creditsCell.animatedGifImage.tag = indexPath.row
                creditsCell.activityIndicator_nearBy.tag = indexPath.row
                creditsCell.moodTypeIcon.tag = indexPath.row
                
                DispatchQueue.main.async {
                    if self.getFullDataStr == "fetchAllData"
                    {
                        if (self.allRepliesArray[indexPath.row].likedByMeStatus == "yes")
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                    else
                    {
                        if self.latestIntArr.contains(indexPath.row)
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                }
                
                let genderStr = self.allRepliesArray[indexPath.row].gender
                var gender = String()
                if genderStr == "1"
                {
                    gender = "Male"
                }
                else
                {
                    gender = "Female"
                }
                
                
                
                creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                creditsCell.moodTypeIcon.clipsToBounds = true
                creditsCell.moodTypeIcon.layer.masksToBounds = true
                creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.typeOfConfession == "anonymous")
                {
                    creditsCell.userGenderAgeLbl.text = gender + ", " + self.allRepliesArray[indexPath.row].age
         
                    creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.allRepliesArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                        } else {
                            creditsCell.moodTypeIcon.image = image
                        }
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                        creditsCell.moodTypeIcon.addSubview(blurEffectView)
                    }
                    
                }
                else
                {
                    creditsCell.userGenderAgeLbl.text = self.allRepliesArray[indexPath.row].name
         
                    creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.allRepliesArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                        } else {
                            creditsCell.moodTypeIcon.image = image
                        }
                        
                        for subview in creditsCell.moodTypeIcon.subviews
                        {
                            if subview.isKind(of: UIVisualEffectView.self)
                            {
                                subview.removeFromSuperview()
                            }
                        }
                    }
                }
                
                let inputString: NSString = self.allRepliesArray[indexPath.row].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                creditsCell.usersStatus_TxtView.text = modifiedString
                
              /*  DispatchQueue.main.async {
                    if (creditsCell.usersStatus_TxtView.text != "")
                    {
                        let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                        creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                        let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                        var newFrame = creditsCell.usersStatus_TxtView.frame
                        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                        
                        creditsCell.animatedImageHeight.constant = newSize.height
                    }
                    else{
                        creditsCell.animatedImageHeight.constant = 0
                    }
                }*/
                
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                    
                    creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.allRepliesArray[indexPath.row].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell.activityIndicator_nearBy.isHidden = true
                        
                        creditsCell.animatedGifImage.isHidden = false
                        //                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
                        //                        creditsCell.animatedGifImage.layer.masksToBounds = false
                        //                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                        creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell.animatedGifImage.clipsToBounds = true
                        
                    }
                })
                
                //                let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGestureDetected(_:)))
                //                pinchGestureRecognizer.delegate = self
                //                creditsCell.animatedGifImage.addGestureRecognizer(pinchGestureRecognizer)
                //                creditsCell.animatedGifImage.isUserInteractionEnabled = true
                
                if self.allRepliesArray[indexPath.row].likesCount == "0"
                {
                    creditsCell.totalLikesCountLbl.text = ""
                }
                else
                {
                    creditsCell.totalLikesCountLbl.text = String(self.allRepliesArray[indexPath.row].likesCount)
                }
                
                if (self.allRepliesArray[indexPath.row].location != "")
                {
                    creditsCell.locationIcon.isHidden = false
                    creditsCell.userNameLbl_Y_Constraint.constant = 5
                }else{
                    creditsCell.locationIcon.isHidden = true
                    creditsCell.userNameLbl_Y_Constraint.constant = (creditsCell.moodTypeIcon.frame.origin.y + creditsCell.moodTypeIcon.frame.size.height)/3
                }
                
                creditsCell.userLocationLbl.text = self.allRepliesArray[indexPath.row].location
                creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                
                if  self.allRepliesArray[indexPath.row].replyCount == "0"
                {
                    creditsCell.totalReplyCountLbl.text = ""
                }
                else //if totalRepliesCount > 0
                {
                    creditsCell.totalReplyCountLbl.text = self.allRepliesArray[indexPath.row].replyCount
                }
                
                creditsCell.confessionLikesBtn.tag = indexPath.row
                creditsCell.confessionLikesBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.confessionReplyBtn.tag = indexPath.row
                creditsCell.confessionReplyBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.confessionMessageBtn.tag = indexPath.row
                creditsCell.confessionMessageBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.dropDownBtn.tag = indexPath.row
                creditsCell.dropDownBtn.addTarget(self, action: #selector(WiRepliesViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.viewFullImageBtn.tag = indexPath.row
                creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiRepliesViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.viewProfileBtn.tag = indexPath.row
                creditsCell.viewProfileBtn.addTarget(self, action: #selector(WiRepliesViewController.viewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return creditsCell
            }
            else
            {
                let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell1", for: indexPath) as! CreditPackagesTableViewCell
                
                if (self.allRepliesArray.count <= 0)
                {
                    return creditsCell
                }
                
                creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                creditsCell.tag = indexPath.row
                creditsCell.confessionLikeIcon.tag = indexPath.row
                creditsCell.animatedGifImage.tag = indexPath.row
                creditsCell.animatedGifImage.isHidden = true
                creditsCell.activityIndicatorGif_nearBy.tag = indexPath.row
                creditsCell.moodTypeIcon.tag = indexPath.row
                
                DispatchQueue.main.async {
                    
                    if self.getFullDataStr == "fetchAllData"
                    {
                        if (self.allRepliesArray[indexPath.row].likedByMeStatus == "yes")
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                    else
                    {
                        if self.latestIntArr.contains(indexPath.row)
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                }
                
                let genderStr = self.allRepliesArray[indexPath.row].gender
                var gender = String()
                if genderStr == "1"
                {
                    gender = "Male"
                }
                else
                {
                    gender = "Female"
                }
                
                creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                creditsCell.moodTypeIcon.clipsToBounds = true
                creditsCell.moodTypeIcon.layer.masksToBounds = true
                creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.typeOfConfession == "anonymous")
                {
                    creditsCell.userGenderAgeLbl.text = gender + ", " + self.allRepliesArray[indexPath.row].age
                    
                    creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.allRepliesArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                        } else {
                            creditsCell.moodTypeIcon.image = image
                        }
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                        creditsCell.moodTypeIcon.addSubview(blurEffectView)
                    }
                    
                }
                else
                {
                    creditsCell.userGenderAgeLbl.text = self.allRepliesArray[indexPath.row].name
                    
                    creditsCell.moodTypeIcon?.sd_setImage(with: URL(string: self.allRepliesArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                        if (error != nil) {
                            creditsCell.moodTypeIcon.image = UIImage(named: "Default Icon")
                        } else {
                            creditsCell.moodTypeIcon.image = image
                        }
                        
                        for subview in creditsCell.moodTypeIcon.subviews
                        {
                            if subview.isKind(of: UIVisualEffectView.self)
                            {
                                subview.removeFromSuperview()
                            }
                        }
                    }
                }
                
                
                let inputString: NSString = self.allRepliesArray[indexPath.row].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                creditsCell.usersStatus_TxtView.text = modifiedString
                
              /*  DispatchQueue.main.async {
                    if (creditsCell.usersStatus_TxtView.text != "")
                    {
                        let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                        creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                        let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                        var newFrame = creditsCell.usersStatus_TxtView.frame
                        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                        
                        creditsCell.animatedGifHeight.constant = newSize.height
                    }
                    else{
                        creditsCell.animatedGifHeight.constant = 0
                    }
                }*/
                
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                    
                    creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.allRepliesArray[indexPath.row].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell.activityIndicatorGif_nearBy.isHidden = true
                        
                        creditsCell.animatedGifImage.isHidden = false
                        //                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
                        //                        creditsCell.animatedGifImage.layer.masksToBounds = false
                        //                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                        creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell.animatedGifImage.clipsToBounds = true
                        
                    }
                })
                
                //                let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGestureDetected(_:)))
                //                pinchGestureRecognizer.delegate = self
                //                creditsCell.animatedGifImage.addGestureRecognizer(pinchGestureRecognizer)
                //                creditsCell.animatedGifImage.isUserInteractionEnabled = true
                
                if self.allRepliesArray[indexPath.row].likesCount == "0"
                {
                    creditsCell.totalLikesCountLbl.text = ""
                }
                else
                {
                    creditsCell.totalLikesCountLbl.text = String(self.allRepliesArray[indexPath.row].likesCount)
                }
                
                if (self.allRepliesArray[indexPath.row].location != "")
                {
                    creditsCell.locationIcon.isHidden = false
                    creditsCell.userNameLbl_Y_Constraint.constant = 5
                }else{
                    creditsCell.locationIcon.isHidden = true
                    creditsCell.userNameLbl_Y_Constraint.constant = (creditsCell.moodTypeIcon.frame.origin.y + creditsCell.moodTypeIcon.frame.size.height)/3
                }
                
                creditsCell.userLocationLbl.text = self.allRepliesArray[indexPath.row].location
                creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                
                if  self.allRepliesArray[indexPath.row].replyCount == "0"
                {
                    creditsCell.totalReplyCountLbl.text = ""
                }
                else
                {
                    creditsCell.totalReplyCountLbl.text = self.allRepliesArray[indexPath.row].replyCount
                }
                
                creditsCell.confessionLikesBtn.tag = indexPath.row
                creditsCell.confessionLikesBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.confessionReplyBtn.tag = indexPath.row
                creditsCell.confessionReplyBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.confessionMessageBtn.tag = indexPath.row
                creditsCell.confessionMessageBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.dropDownBtn.tag = indexPath.row
                creditsCell.dropDownBtn.addTarget(self, action: #selector(WiRepliesViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.viewFullImageBtn.tag = indexPath.row
                creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiRepliesViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.viewProfileBtn.tag = indexPath.row
                creditsCell.viewProfileBtn.addTarget(self, action: #selector(WiRepliesViewController.viewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return creditsCell
            }
        }
        else
        {
            let onlineUsersCell = tableView.dequeueReusableCell(withIdentifier: "statusOnlyCell", for: indexPath) as! ConfessionStatusTableViewCell
            
            if (self.allRepliesArray.count <= 0)
            {
                return onlineUsersCell
            }
            
            onlineUsersCell.selectionStyle = UITableViewCellSelectionStyle.none
            onlineUsersCell.confessionLikeIcon.tag = indexPath.row
            
            
            DispatchQueue.main.async {
                
                if self.getFullDataStr == "fetchAllData"
                {
                    if (self.allRepliesArray[indexPath.row].likedByMeStatus == "yes")
                    {
                        onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                    }
                    else
                    {
                        onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                    }
                }
                else
                {
                    if self.latestIntArr.contains(indexPath.row)
                    {
                        onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                    }
                    else
                    {
                        onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                    }
                }
            }
            
            let genderStr = self.allRepliesArray[indexPath.row].gender
            var gender = String()
            if genderStr == "1"
            {
                gender = "Male"
            }
            else
            {
                gender = "Female"
            }
            
            onlineUsersCell.moodIcon.frame.size.width = 45
            onlineUsersCell.moodIcon.frame.size.height = 45
            onlineUsersCell.moodIcon.layer.cornerRadius =  onlineUsersCell.moodIcon.frame.size.height/2
            onlineUsersCell.moodIcon.clipsToBounds = true
            onlineUsersCell.moodIcon.layer.masksToBounds = true
            onlineUsersCell.moodIcon.contentMode = UIViewContentMode.scaleAspectFill
            
            if (self.typeOfConfession == "anonymous")
            {
                onlineUsersCell.userDetailsLbl.text = gender + ", " + self.allRepliesArray[indexPath.row].age
                
                onlineUsersCell.moodIcon?.sd_setImage(with: URL(string: self.allRepliesArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                    if (error != nil) {
                        onlineUsersCell.moodIcon.image = UIImage(named: "Default Icon")
                    } else {
                        onlineUsersCell.moodIcon.image = image
                    }
                    
                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = onlineUsersCell.moodIcon.bounds
                    onlineUsersCell.moodIcon.addSubview(blurEffectView)
                }
                
                
            }
            else
            {
                onlineUsersCell.userDetailsLbl.text = self.allRepliesArray[indexPath.row].name
                
                onlineUsersCell.moodIcon?.sd_setImage(with: URL(string: self.allRepliesArray[indexPath.row].profilePic)) { (image, error, cache, urls) in
                    if (error != nil) {
                        onlineUsersCell.moodIcon.image = UIImage(named: "Default Icon")
                    } else {
                        onlineUsersCell.moodIcon.image = image
                    }
                    
                    for subview in onlineUsersCell.moodIcon.subviews
                    {
                        if subview.isKind(of: UIVisualEffectView.self)
                        {
                            subview.removeFromSuperview()
                        }
                    }
                }
            }
            
            
            
            if self.allRepliesArray[indexPath.row].likesCount == "0"
            {
                onlineUsersCell.likeCountLbl.text = ""
            }
            else
            {
                onlineUsersCell.likeCountLbl.text = String(self.allRepliesArray[indexPath.row].likesCount)
            }
            
            if self.allRepliesArray[indexPath.row].replyCount == "0"
            {
                onlineUsersCell.replyCountLbl.text = ""
            }
            else
            {
                onlineUsersCell.replyCountLbl.text = self.allRepliesArray[indexPath.row].replyCount
            }
            
            if (self.allRepliesArray[indexPath.row].location != "")
            {
                onlineUsersCell.locationIcon.isHidden = false
                onlineUsersCell.userDetailsLbl_Y_Frame.constant = 5
            }else{
                onlineUsersCell.locationIcon.isHidden = true
                onlineUsersCell.userDetailsLbl_Y_Frame.constant = (onlineUsersCell.moodIcon.frame.origin.y + onlineUsersCell.moodIcon.frame.size.height)/3
            }
            
            onlineUsersCell.userLocationLbl.text = self.allRepliesArray[indexPath.row].location
            onlineUsersCell.userLocationLbl.adjustsFontSizeToFitWidth = true
            
            //            onlineUsersCell.userPostedStatusLbl.text = self.allRepliesArray[indexPath.row].statusAdded
            
            let inputString: NSString = self.allRepliesArray[indexPath.row].statusAdded as NSString
            let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
            let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
            
            onlineUsersCell.usersStatusTextView.text = modifiedString
            
            onlineUsersCell.postLikesBtn.tag = indexPath.row
            onlineUsersCell.postLikesBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            onlineUsersCell.postReplyBtn.tag = indexPath.row
            onlineUsersCell.postReplyBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)
            
            onlineUsersCell.postMessageBtn.tag = indexPath.row
            onlineUsersCell.postMessageBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            onlineUsersCell.postDropDownBtn.tag = indexPath.row
            onlineUsersCell.postDropDownBtn.addTarget(self, action: #selector(WiRepliesViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            onlineUsersCell.viewProfile_btn.tag = indexPath.row
            onlineUsersCell.viewProfile_btn.addTarget(self, action: #selector(WiRepliesViewController.viewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            return onlineUsersCell
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func viewProfileWithProfileUsers_btnAction(sender: UIButton!) {
        
        if (self.allRepliesArray.count > 0)
        {
            if (self.typeOfConfession == "anonymous")
            {
                print("post is anonymous")
            }
            else
            {
//                let indexPathRow = IndexPath(row: sender.tag, section: 0)
                let userOwnerIDStr = self.allRepliesArray[sender.tag].userIDStr
                if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if id == userOwnerIDStr
                    {
                        let profileView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                        self.navigationController?.pushViewController(profileView, animated: true)
                    }
                    else
                    {
                        let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                        viewProfileScreen.ownerID = self.allRepliesArray[sender.tag].userIDStr
                        self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                    }
                }
            }
        }
    }
    
    //MARK:- ****** viewFullSizeImageACtion ******
    func viewFullSizeImageACtion(sender: UIButton!)
    {
        self.indexAtClicked = sender.tag
        
        if (self.allRepliesArray.count <= 0)
        {
            return
        }
        
        if (self.allRepliesArray[sender.tag].postedImage != "")
        {
            let imageURL = self.allRepliesArray[sender.tag].postedImage
            if (imageURL.contains("jpg"))
            {
                self.fullImageView.sd_setImage(with: URL(string: self.allRepliesArray[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
             
                    if ((image?.size.height) != nil) && ((image?.size.width) != nil)
                    {
                        self.showImagefullScreenView.isHidden = false
                        
                        self.fullImageAnimatedGif.isHidden = true
                        self.fullImageView.isHidden = false
                        
                        if (image?.size.height)! <= (self.repliesGifWrapperView.frame.size.height)
                        {
                            var gifWidth = Int()
                            var screenRes = Int()
                            var newXPos =  Int()
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  0
                            }
                            else{
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  screenRes - gifWidth
                            }
                            
                            
                            let gifHeight = Int((image?.size.height)!/2)
                            let heightOfWrraperView = Int(self.repliesGifWrapperView.frame.height/2)
                            let newYPos: Int =  heightOfWrraperView - gifHeight
                            
                            self.repliesGifWrapperView.frame.origin.y = 0
                            self.fullImageView.frame.origin.y = CGFloat(newYPos)
                            self.fullImageView.frame.origin.x = CGFloat(newXPos)
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                self.fullImageView.frame.size.width = self.view.frame.size.width
                            }
                            else{
                                self.fullImageView.frame.size.width = (image?.size.width)!
                            }
                            
                            if (image?.size.height)! <= (self.repliesGifWrapperView.frame.size.height)
                            {
                                self.fullImageView.frame.size.height = (image?.size.height)!
                            }
                            else{
                                self.fullImageView.frame.size.height = self.repliesGifWrapperView.frame.size.height
                            }
                            
                            self.imagesButtonWrapperView.frame.origin.y = self.fullImageView.frame.origin.y + self.fullImageView.frame.size.height + 10
                            self.repliesGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                        }
                        else{
                            self.fullImageView.frame.origin.y = 21
                            self.fullImageView.frame.size.height = (self.repliesGifWrapperView.frame.size.height - 21)
                            
                            self.imagesButtonWrapperView.frame.origin.y = self.repliesGifWrapperView.frame.origin.y + self.repliesGifWrapperView.frame.size.height + 10
                            self.repliesGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                        }
                        
                        self.fullImageView.layer.borderWidth = 2.0
                        self.fullImageView.layer.masksToBounds = false
                        self.fullImageView.layer.borderColor = UIColor.lightGray.cgColor
                        self.fullImageView.contentMode = UIViewContentMode.scaleAspectFit
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Please wait.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                self.showImagefullScreenView.isHidden = false
                
                self.fullImageView.isHidden = true
                self.fullImageAnimatedGif.isHidden = false
                
                DispatchQueue.main.async {
                    self.fullImageAnimatedGif.sd_setImage(with: URL(string: self.allRepliesArray[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions.refreshCached)
                }
                
                self.fullImageAnimatedGif.layer.borderWidth = 2.0
                self.fullImageAnimatedGif.layer.masksToBounds = false
                self.fullImageAnimatedGif.layer.borderColor = UIColor.lightGray.cgColor
                self.fullImageAnimatedGif.contentMode = UIViewContentMode.scaleAspectFit

                var gifWidth = Int()
                var screenRes = Int()
                var newXPos =  Int()
                if (self.allRepliesArray[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    gifWidth = Int(self.allRepliesArray[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  0
                }
                else{
                    gifWidth = Int(self.allRepliesArray[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  screenRes - gifWidth
                }
                
                let gifHeight = Int(self.allRepliesArray[sender.tag].height/2)
                let heightOfWrraperView = Int(self.repliesGifWrapperView.frame.height/2)
                let newYPos: Int =  heightOfWrraperView - gifHeight
                
                self.repliesGifWrapperView.frame.origin.y = 0
                self.fullImageAnimatedGif.frame.origin.y = CGFloat(newYPos)
                self.fullImageAnimatedGif.frame.origin.x = CGFloat(newXPos)
                
                if (self.allRepliesArray[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    self.fullImageAnimatedGif.frame.size.width = self.view.frame.size.width
                }
                else{
                    self.fullImageAnimatedGif.frame.size.width = CGFloat(self.allRepliesArray[sender.tag].width)
                }
                
                self.fullImageAnimatedGif.frame.size.height = CGFloat(self.allRepliesArray[sender.tag].height)
                
                self.imagesButtonWrapperView.frame.origin.y = self.fullImageAnimatedGif.frame.origin.y + self.fullImageAnimatedGif.frame.size.height + 10
                self.repliesGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
            }
        }
        
        
        DispatchQueue.main.async {
            
            if self.getFullDataStr == "fetchAllData"
            {
                if (self.allRepliesArray[sender.tag].likedByMeStatus == "yes")
                {
                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
            else{
                if self.latestIntArr.contains(sender.tag)
                {
                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
        }
        
        if self.allRepliesArray[sender.tag].likesCount == "0"
        {
            self.confessionLikesLbl.text = ""
        }
        else //if self.allRepliesArray[sender.tag].likesCount > 0
        {
            self.confessionLikesLbl.text = String(self.allRepliesArray[sender.tag].likesCount)
        }
        
        if Int(self.allRepliesArray[sender.tag].replyCount)! == 0
        {
            self.confessionReply.text = ""
        }
        else if Int(self.allRepliesArray[sender.tag].replyCount)! > 0
        {
            self.confessionReply.text = self.allRepliesArray[sender.tag].replyCount
        }
        else
        {
            self.confessionReply.text = ""
        }
    }
    
    
    // MARK: - ****** confessionLikes_btnAction ******
    func confessionLikes_btnAction(sender: UIButton!) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if (self.allRepliesArray.count > 0)
            {
                var commentLikeID = String()
                
                if self.checkReplyType == "replyOnPost"
                {
                    commentLikeID = self.allRepliesArray[sender.tag].replyID
                }
                else{
                   commentLikeID = self.allRepliesArray[sender.tag].furtherReplyID
                }
                
                self.likeDislikeConfessions(confessionID: commentLikeID, completion: { (responseBool) in
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected = IndexPath()
                        self.indexPathSelected = IndexPath(row: sender.tag, section: 0)
                        
                        self.latestIntArr.add(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if (self.allRepliesArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.allRepliesArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.allRepliesArray[sender.tag].likesCount
                                
                                if  (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                    
                                    self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                    self.allRepliesArray[sender.tag].likesCount = "1"
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikesLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                        self.allRepliesArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.allRepliesArray[sender.tag].likesCount)! + 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                        self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.allRepliesArray[sender.tag].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    creditsCell.totalLikesCountLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                                
                                if  (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                    self.allRepliesArray[sender.tag].likesCount = "1"
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikesLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                        self.allRepliesArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.allRepliesArray[sender.tag].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                        self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
                        self.indexPathSelected = IndexPath()
                        self.indexPathSelected = IndexPath(row: sender.tag, section: 0)
                        
                        self.latestIntArr.remove(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if (self.allRepliesArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.allRepliesArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.allRepliesArray[sender.tag].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    onlineUsersCell.likeCountLbl.text = ""//String(totalLikes)
                                    self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                    self.allRepliesArray[sender.tag].likesCount = ""
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikesLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        onlineUsersCell.likeCountLbl.text = ""// String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                        self.allRepliesArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.allRepliesArray[sender.tag].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            onlineUsersCell.likeCountLbl.text  = ""
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                            self.allRepliesArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                            self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                        
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.allRepliesArray[sender.tag].likesCount
                                
                                if (likesCountStr == "0")
                                {
                                    
                                    creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                    
                                    self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                    self.allRepliesArray[sender.tag].likesCount = ""
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikesLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        
                                        creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                        self.allRepliesArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.allRepliesArray[sender.tag].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            creditsCell.totalLikesCountLbl.text  = ""
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                            self.allRepliesArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                            self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
    
    // MARK: - ****** confessionReply_btn ******
    func confessionReply_btn(sender: UIButton!) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
        if self.checkReplyType == "replyOnPost"
        {
            homeScreen.postIdOwnerStr = self.allRepliesArray[sender.tag].userIDStr
            homeScreen.postIDStr = self.allRepliesArray[sender.tag].postIDStr
            homeScreen.replyIDStr = self.allRepliesArray[sender.tag].replyID
            homeScreen.checkReplyType = "replyOnReply"
            homeScreen.differentiateTypereplyStr = "firstReplyStr"
        }
        else if self.checkReplyType == "replyOnReply"
        {
            homeScreen.postIdOwnerStr = self.allRepliesArray[sender.tag].userIDStr
//            homeScreen.postIDStr = self.allRepliesArray[sender.tag].replyID
            homeScreen.postIDStr = self.allRepliesArray[sender.tag].postIDStr
            homeScreen.replyIDStr = self.allRepliesArray[sender.tag].furtherReplyID
            homeScreen.checkReplyType = "replyOnReply"
            homeScreen.differentiateTypereplyStr = "furtherReply"
        }
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }
    
    
    // MARK: - ****** confessionMessage_btnAction ******
    func confessionMessage_btnAction(sender: UIButton!) {
        
        let userOwnerIDStr =  self.allRepliesArray[sender.tag].userIDStr
        if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if id == userOwnerIDStr
            {
                print("owner of the post and current user is same")
            }
            else
            {
                let ownerProfileImage = UIImageView()
                ownerProfileImage.sd_setImage(with: URL(string: self.allRepliesArray[sender.tag].postedImage), placeholderImage: UIImage(named: "Default Icon"))
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                messagesScreen.mainConfessionCheck = "comingFromReplies"
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromStatusScreen"
                let userIDStr =  self.allRepliesArray[sender.tag].userIDStr
                messagesScreen.opponentName = self.allRepliesArray[sender.tag].name
                messagesScreen.opponentImage = self.allRepliesArray[sender.tag].profilePic
                messagesScreen.opponentUserID = userIDStr
                messagesScreen.postID = self.allRepliesArray[sender.tag].postIDStr
                messagesScreen.opponentFirebaseUserID = self.allRepliesArray[self.indexAtClicked].firebase_user_id
                messagesScreen.opponentProfilePic =  ownerProfileImage.image!
                
                if (self.typeOfConfession == "anonymous")
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                }
                else
                {
                    messagesScreen.typeOfUser = "WithProfileUser"
                    messagesScreen.checkVisibiltyStr = "withProfileChat"
                }
                
                messagesScreen.mainConfessionCheck = "comingFromReplies"
                
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }
        }
        
    }
    
    
    // MARK: - ****** DropDown_btnAction ******
    func DropDown_btnAction(sender: UIButton!) {
        
        if (self.allRepliesArray.count > 0)
        {
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
//                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
//                {
                    if (self.allRepliesArray[sender.tag].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.allRepliesArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.allRepliesArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        print("typeOfCOnfession = \(typeOfCOnfession)")
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: self.allRepliesArray[sender.tag].postIDStr, status: self.allRepliesArray[sender.tag].statusAdded, backgroudImageStr: self.allRepliesArray[sender.tag].postedImage, moodTypeImageStr: self.allRepliesArray[sender.tag].moodTypeStr, timeFrameStr: self.allRepliesArray[sender.tag].postHours, moodTypeStr: self.allRepliesArray[sender.tag].moodTypeStr, moodTypeUrl: self.allRepliesArray[sender.tag].moodTypeStr, photoTypeStr: typeOfCOnfession, replyIDStr: self.allRepliesArray[sender.tag].replyID, replyToReplyIDStr: self.allRepliesArray[sender.tag].furtherReplyID, getTotalLikeCount: self.allRepliesArray[sender.tag].likesCount, getTotalReplyCount: self.allRepliesArray[sender.tag].replyCount, gifWidth: self.allRepliesArray[sender.tag].width, gifHeight: self.allRepliesArray[sender.tag].height)
                    }
                    else
                    {
                        self.showActionSheetForDropDownMenu_otherUser(postID: self.allRepliesArray[sender.tag].postIDStr, ownerIDStr: self.allRepliesArray[sender.tag].userIDStr, replyID: self.allRepliesArray[sender.tag].replyID, furtherReplyID: self.allRepliesArray[sender.tag].furtherReplyID, myID: userID)
                    }
//                }
            }
        }
    }
    
    /*
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        print("section = \(section)")
        print("self.allRepliesArray.count = (self.allRepliesArray)")
       return self.allRepliesArray[section].child_comments_array.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.allRepliesArray.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if (self.allRepliesArray[section].postedImage != "")
        {
            let imageURL = self.allRepliesArray[section].postedImage
            if (imageURL.contains("jpg"))
            {
                return 460
            }
            else{
                return 380
            }
        }
        else{
            return 145
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        self.repliesTableView.separatorColor = UIColor.clear
        if (self.allRepliesArray[section].postedImage != "")
        {
            let imageURL = self.allRepliesArray[section].postedImage
            if (imageURL.contains("jpg"))
            {
                let header:HeaderCommentsForPhotos = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderPhotos") as! HeaderCommentsForPhotos

                header.postedPhotoImage.isHidden = true
                header.tag = section
                header.likesBtn.tag = section
                header.likesCountLbl.tag = section
                header.replyBtn.tag = section
                header.repliesCountLbl.tag = section
                header.messageBtnLbl.tag = section
                header.postedPhotoImage.tag = section
                header.userViewProfileBtn.tag = section
                header.userProfileIcon.tag = section
                
                DispatchQueue.main.async {
                    if self.getFullDataStr == "fetchAllData"
                    {
                        if (self.allRepliesArray[section].likedByMeStatus == "yes")
                        {
                            header.likesIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            header.likesIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                    else
                    {
                        if self.latestIntArr.contains(section)
                        {
                            header.likesIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            header.likesIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                }
                
                let genderStr = self.allRepliesArray[section].gender
                var gender = String()
                if genderStr == "1"
                {
                    gender = "Male"
                }
                else
                {
                    gender = "Female"
                }
                
                header.userProfileIcon.layer.cornerRadius = header.userProfileIcon.frame.size.height/2
                header.userProfileIcon.clipsToBounds = true
                header.userProfileIcon.layer.masksToBounds = true
                header.userProfileIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.typeOfConfession == "anonymous")
                {
                    header.usersNameLbl.text = gender + ", " + self.allRepliesArray[section].age
                    
                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = header.userProfileIcon.bounds
                    header.userProfileIcon.addSubview(blurEffectView)
                }
                else
                {
                    header.usersNameLbl.text = self.allRepliesArray[section].name
                }
                
                header.userProfileIcon.sd_setImage(with: URL(string: self.allRepliesArray[section].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                
                let inputString: NSString = self.allRepliesArray[section].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                header.statusAddedTextView.dataDetectorTypes = UIDataDetectorTypes.link
                header.statusAddedTextView.text = modifiedString
                
                if (header.statusAddedTextView.text != "")
                {
                    let fixedWidth = header.statusAddedTextView.frame.size.width
                    header.statusAddedTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                    let newSize = header.statusAddedTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                    var newFrame = header.statusAddedTextView.frame
                    newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                    
                    header.statusTextViewHeight.constant = newSize.height
                }
                else
                {
                    header.statusTextViewHeight.constant = 0
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                    
                    header.postedPhotoImage.sd_setImage(with: URL(string: self.allRepliesArray[section].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
//                        creditsCell.activityIndicator_nearBy.isHidden = true
                        
                        header.postedPhotoImage.isHidden = false
                        header.postedPhotoImage.contentMode = .scaleAspectFit
                        header.postedPhotoImage.clipsToBounds = true
                        
                    }
                })
                
                if self.allRepliesArray[section].likesCount == "0"
                {
                    header.likesCountLbl.text = ""
                }
                else
                {
                    header.likesCountLbl.text = String(self.allRepliesArray[section].likesCount)
                }
                
                header.usersLocationLbl.text = self.allRepliesArray[section].location
                header.usersLocationLbl.adjustsFontSizeToFitWidth = true
                
                if  self.allRepliesArray[section].replyCount == "0"
                {
                    header.repliesCountLbl.text = ""
                }
                else 
                {
                    header.repliesCountLbl.text = self.allRepliesArray[section].replyCount
                }
                
                header.likesBtn.tag = section
                header.likesBtn.addTarget(self, action: #selector(WiRepliesViewController.singleReplyLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)

                header.replyBtn.tag = section
                header.replyBtn.addTarget(self, action: #selector(WiRepliesViewController.singleReply_btn(sender:)), for: UIControlEvents.touchUpInside)

                header.messageBtnLbl.tag = section
                header.messageBtnLbl.addTarget(self, action: #selector(WiRepliesViewController.singleMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)

                header.dropDownOptionsBtn.tag = section
                header.dropDownOptionsBtn.addTarget(self, action: #selector(WiRepliesViewController.singleDropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)

//                creditsCell.viewFullImageBtn.tag = section
//                creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiRepliesViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)

                header.userViewProfileBtn.tag = section
                header.userViewProfileBtn.addTarget(self, action: #selector(WiRepliesViewController.singleViewiewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return header
            }
            else{
                let header:HeaderCommentsForGif = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderGif") as! HeaderCommentsForGif
                
                header.postedGifImage.isHidden = true
                header.tag = section
                header.likesBtn.tag = section
                header.likesCountLbl.tag = section
                header.replyBtn.tag = section
                header.repliesCountLbl.tag = section
                header.messageBtn.tag = section
                header.postedGifImage.tag = section
                header.viewProfileBtn.tag = section
                header.usersProfileIcon.tag = section
                
                DispatchQueue.main.async {
                    if self.getFullDataStr == "fetchAllData"
                    {
                        if (self.allRepliesArray[section].likedByMeStatus == "yes")
                        {
                            header.confesionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            header.confesionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                    else
                    {
                        if self.latestIntArr.contains(section)
                        {
                            header.confesionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            header.confesionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                }
                
                let genderStr = self.allRepliesArray[section].gender
                var gender = String()
                if genderStr == "1"
                {
                    gender = "Male"
                }
                else
                {
                    gender = "Female"
                }
                
                header.usersProfileIcon.layer.cornerRadius = header.usersProfileIcon.frame.size.height/2
                header.usersProfileIcon.clipsToBounds = true
                header.usersProfileIcon.layer.masksToBounds = true
                header.usersProfileIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.typeOfConfession == "anonymous")
                {
                    header.usersNameLbl.text = gender + ", " + self.allRepliesArray[section].age
                    
                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = header.usersProfileIcon.bounds
                    header.usersProfileIcon.addSubview(blurEffectView)
                }
                else
                {
                    header.usersNameLbl.text = self.allRepliesArray[section].name
                }
                
                header.usersProfileIcon.sd_setImage(with: URL(string: self.allRepliesArray[section].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                
                let inputString: NSString = self.allRepliesArray[section].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                header.statusPostedTextView.dataDetectorTypes = UIDataDetectorTypes.link
                header.statusPostedTextView.text = modifiedString
                
                if (header.statusPostedTextView.text != "")
                {
                    let fixedWidth = header.statusPostedTextView.frame.size.width
                    header.statusPostedTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                    let newSize = header.statusPostedTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                    var newFrame = header.statusPostedTextView.frame
                    newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                    
                    header.statusTextViewHeight.constant = newSize.height
                }
                else
                {
                    header.statusTextViewHeight.constant = 0
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                    
                    header.postedGifImage.sd_setImage(with: URL(string: self.allRepliesArray[section].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        //                        creditsCell.activityIndicator_nearBy.isHidden = true
                        
                        header.postedGifImage.isHidden = false
                        header.postedGifImage.contentMode = .scaleAspectFit
                        header.postedGifImage.clipsToBounds = true
                        
                    }
                })
                
                if self.allRepliesArray[section].likesCount == "0"
                {
                    header.likesCountLbl.text = ""
                }
                else
                {
                    header.likesCountLbl.text = String(self.allRepliesArray[section].likesCount)
                }
                
                header.usersLocationLbl.text = self.allRepliesArray[section].location
                header.usersLocationLbl.adjustsFontSizeToFitWidth = true
                
                if  self.allRepliesArray[section].replyCount == "0"
                {
                    header.repliesCountLbl.text = ""
                }
                else
                {
                    header.repliesCountLbl.text = self.allRepliesArray[section].replyCount
                }
                
                
                header.likesBtn.tag = section
                header.likesBtn.addTarget(self, action: #selector(WiRepliesViewController.singleReplyLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                header.replyBtn.tag = section
                header.replyBtn.addTarget(self, action: #selector(WiRepliesViewController.singleReply_btn(sender:)), for: UIControlEvents.touchUpInside)
                
                header.messageBtn.tag = section
                header.messageBtn.addTarget(self, action: #selector(WiRepliesViewController.singleMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                header.dropDownOptionsBtn.tag = section
                header.dropDownOptionsBtn.addTarget(self, action: #selector(WiRepliesViewController.singleDropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
//                creditsCell.viewFullImageBtn.tag = section
//                creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiRepliesViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                
                header.viewProfileBtn.tag = section
                header.viewProfileBtn.addTarget(self, action: #selector(WiRepliesViewController.singleViewiewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return header
            }
        }
        else{
            let header:HeaderCommentsForText = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderText") as! HeaderCommentsForText
            
            header.tag = section
            header.likesBtn.tag = section
            header.likesCountLbl.tag = section
            header.commentBtn.tag = section
            header.repliesCountLbl.tag = section
            header.messageBtn.tag = section
            header.viewProfileBtn.tag = section
            header.usersProfilePic.tag = section
            
            DispatchQueue.main.async {
                if self.getFullDataStr == "fetchAllData"
                {
                    if (self.allRepliesArray[section].likedByMeStatus == "yes")
                    {
                        header.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                    }
                    else
                    {
                        header.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                    }
                }
                else
                {
                    if self.latestIntArr.contains(section)
                    {
                        header.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                    }
                    else
                    {
                        header.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                    }
                }
            }
            
            let genderStr = self.allRepliesArray[section].gender
            var gender = String()
            if genderStr == "1"
            {
                gender = "Male"
            }
            else
            {
                gender = "Female"
            }
            
            header.usersProfilePic.layer.cornerRadius = header.usersProfilePic.frame.size.height/2
            header.usersProfilePic.clipsToBounds = true
            header.usersProfilePic.layer.masksToBounds = true
            header.usersProfilePic.contentMode = UIViewContentMode.scaleAspectFill
            
            if (self.typeOfConfession == "anonymous")
            {
                header.usersNameLbl.text = gender + ", " + self.allRepliesArray[section].age
                
                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = header.usersProfilePic.bounds
                header.usersProfilePic.addSubview(blurEffectView)
            }
            else
            {
                header.usersNameLbl.text = self.allRepliesArray[section].name
            }
            
            header.usersProfilePic.sd_setImage(with: URL(string: self.allRepliesArray[section].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
            
            let inputString: NSString = self.allRepliesArray[section].statusAdded as NSString
            let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
            let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
            
            header.statusPostedTxtView.dataDetectorTypes = UIDataDetectorTypes.link
            header.statusPostedTxtView.text = modifiedString
            
            if (header.statusPostedTxtView.text != "")
            {
                let fixedWidth = header.statusPostedTxtView.frame.size.width
                header.statusPostedTxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize = header.statusPostedTxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame = header.statusPostedTxtView.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                
            }
            else{
            }
            
            
            if self.allRepliesArray[section].likesCount == "0"
            {
                header.likesCountLbl.text = ""
            }
            else
            {
                header.likesCountLbl.text = String(self.allRepliesArray[section].likesCount)
            }
            
            header.usersLocationLbl.text = self.allRepliesArray[section].location
            header.usersLocationLbl.adjustsFontSizeToFitWidth = true
            
            if  self.allRepliesArray[section].replyCount == "0"
            {
                header.repliesCountLbl.text = ""
            }
            else
            {
                header.repliesCountLbl.text = self.allRepliesArray[section].replyCount
            }
            
            header.likesBtn.tag = section
            header.likesBtn.addTarget(self, action: #selector(WiRepliesViewController.singleReplyLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            header.commentBtn.tag = section
            header.commentBtn.addTarget(self, action: #selector(WiRepliesViewController.singleReply_btn(sender:)), for: UIControlEvents.touchUpInside)
            
            header.messageBtn.tag = section
            header.messageBtn.addTarget(self, action: #selector(WiRepliesViewController.singleMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            header.dropDownOptionsBtn.tag = section
            header.dropDownOptionsBtn.addTarget(self, action: #selector(WiRepliesViewController.singleDropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            header.viewProfileBtn.tag = section
            header.viewProfileBtn.addTarget(self, action: #selector(WiRepliesViewController.singleViewiewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            return header
        }
    }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.repliesTableView.separatorColor = UIColor.clear
        
        let repliesArray = self.createreplieArray(childComments: self.allRepliesArray[indexPath.section].child_comments_array)
        
        if repliesArray.count > 0
        {
            if (repliesArray[indexPath.row].postedImage != "")
            {
                let imageURL = repliesArray[indexPath.row].postedImage
                if (imageURL.contains("jpg"))
                {
                    let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath) as! CreditPackagesTableViewCell
                    
                    creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                    creditsCell.animatedGifImage.isHidden = true
                    creditsCell.tag = indexPath.row
                    creditsCell.confessionLikeIcon.tag = indexPath.row
                    creditsCell.animatedGifImage.tag = indexPath.row
                    creditsCell.activityIndicator_nearBy.tag = indexPath.row
                    creditsCell.moodTypeIcon.tag = indexPath.row
                    
                    DispatchQueue.main.async {
                        if self.getFullDataStr == "fetchAllData"
                        {
                            if (repliesArray[indexPath.row].likedByMeStatus == "yes")
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                        else
                        {
                            if self.latestIntArr.contains(indexPath.row)
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                        }
                    }
                    
                    let genderStr = repliesArray[indexPath.row].gender
                    var gender = String()
                    if genderStr == "1"
                    {
                        gender = "Male"
                    }
                    else
                    {
                        gender = "Female"
                    }
                    
                    
                    
                    creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                    creditsCell.moodTypeIcon.clipsToBounds = true
                    creditsCell.moodTypeIcon.layer.masksToBounds = true
                    creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                    
                    if (self.typeOfConfession == "anonymous")
                    {
                        creditsCell.userGenderAgeLbl.text = gender + ", " + repliesArray[indexPath.row].age
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                        creditsCell.moodTypeIcon.addSubview(blurEffectView)
                    }
                    else
                    {
                        creditsCell.userGenderAgeLbl.text = repliesArray[indexPath.row].name
                    }
                    
                    print(repliesArray[indexPath.row].profilePic)
                    
                    creditsCell.moodTypeIcon.sd_setImage(with: URL(string: repliesArray[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                    
                    //                creditsCell.userStatusLbl.text = self.allRepliesArray[indexPath.row].statusAdded
                    
                    let inputString: NSString = repliesArray[indexPath.row].statusAdded as NSString
                    let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                    let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                    
                    creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                    creditsCell.usersStatus_TxtView.text = modifiedString
                    
                    if (creditsCell.usersStatus_TxtView.text != "")
                    {
                        let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                        creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                        let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                        var newFrame = creditsCell.usersStatus_TxtView.frame
                        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                        
                        creditsCell.animatedImageHeight.constant = newSize.height
                    }
                    else{
                        creditsCell.animatedImageHeight.constant = 0
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                        
                        creditsCell.animatedGifImage.sd_setImage(with: URL(string: repliesArray[indexPath.row].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                            
                            creditsCell.activityIndicator_nearBy.isHidden = true
                            
                            creditsCell.animatedGifImage.isHidden = false
                            //                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
                            //                        creditsCell.animatedGifImage.layer.masksToBounds = false
                            //                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                            
                            creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                            creditsCell.animatedGifImage.clipsToBounds = true
                            
                        }
                    })
                    
                    //                let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGestureDetected(_:)))
                    //                pinchGestureRecognizer.delegate = self
                    //                creditsCell.animatedGifImage.addGestureRecognizer(pinchGestureRecognizer)
                    //                creditsCell.animatedGifImage.isUserInteractionEnabled = true
                    
                    if repliesArray[indexPath.row].likesCount == "0"
                    {
                        creditsCell.totalLikesCountLbl.text = ""
                    }
                    else
                    {
                        creditsCell.totalLikesCountLbl.text = String(repliesArray[indexPath.row].likesCount)
                    }
                    
                    creditsCell.userLocationLbl.text = repliesArray[indexPath.row].location
                    creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                    
                    if  repliesArray[indexPath.row].replyCount == "0"
                    {
                        creditsCell.totalReplyCountLbl.text = ""
                    }
                    else //if totalRepliesCount > 0
                    {
                        creditsCell.totalReplyCountLbl.text = repliesArray[indexPath.row].replyCount
                    }
                    
                    creditsCell.confessionLikesBtn.tag = indexPath.row
                    creditsCell.confessionLikesBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionReplyBtn.tag = indexPath.row
                    creditsCell.confessionReplyBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.confessionMessageBtn.tag = indexPath.row
                    creditsCell.confessionMessageBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.dropDownBtn.tag = indexPath.row
                    creditsCell.dropDownBtn.addTarget(self, action: #selector(WiRepliesViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewFullImageBtn.tag = indexPath.row
                    creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiRepliesViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                    
                    creditsCell.viewProfileBtn.tag = indexPath.row
                    creditsCell.viewProfileBtn.addTarget(self, action: #selector(WiRepliesViewController.viewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                    
                    return creditsCell
            }
        }
            else
            {
                let onlineUsersCell = tableView.dequeueReusableCell(withIdentifier: "statusOnlyCell", for: indexPath) as! ConfessionStatusTableViewCell
                
                if (repliesArray.count <= 0)
                {
                    return onlineUsersCell
                }
                
                onlineUsersCell.selectionStyle = UITableViewCellSelectionStyle.none
                onlineUsersCell.confessionLikeIcon.tag = indexPath.row
                
                DispatchQueue.main.async {
                    
                    if self.getFullDataStr == "fetchAllData"
                    {
                        if (repliesArray[indexPath.row].likedByMeStatus == "yes")
                        {
                            onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                    else
                    {
                        if self.latestIntArr.contains(indexPath.row)
                        {
                            onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                }
                
                let genderStr = repliesArray[indexPath.row].gender
                var gender = String()
                if genderStr == "1"
                {
                    gender = "Male"
                }
                else
                {
                    gender = "Female"
                }
                
                onlineUsersCell.moodIcon.layer.cornerRadius =  onlineUsersCell.moodIcon.frame.size.height/2
                onlineUsersCell.moodIcon.clipsToBounds = true
                onlineUsersCell.moodIcon.layer.masksToBounds = true
                onlineUsersCell.moodIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.typeOfConfession == "anonymous")
                {
                    onlineUsersCell.userDetailsLbl.text = gender + ", " + repliesArray[indexPath.row].age
                    
                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame =  onlineUsersCell.moodIcon.bounds
                    onlineUsersCell.moodIcon.addSubview(blurEffectView)
                }
                else
                {
                    onlineUsersCell.userDetailsLbl.text = repliesArray[indexPath.row].name
                }
                
                print(repliesArray[indexPath.row].profilePic)
                
                onlineUsersCell.moodIcon.sd_setImage(with: URL(string: repliesArray[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                
                
                if repliesArray[indexPath.row].likesCount == "0"
                {
                    onlineUsersCell.likeCountLbl.text = ""
                }
                else
                {
                    onlineUsersCell.likeCountLbl.text = String(repliesArray[indexPath.row].likesCount)
                }
                
                if repliesArray[indexPath.row].replyCount == "0"
                {
                    onlineUsersCell.replyCountLbl.text = ""
                }
                else
                {
                    onlineUsersCell.replyCountLbl.text = repliesArray[indexPath.row].replyCount
                }
                
                onlineUsersCell.userLocationLbl.text = repliesArray[indexPath.row].location
                onlineUsersCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                
                
                let inputString: NSString = repliesArray[indexPath.row].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                onlineUsersCell.usersStatusTextView.text = modifiedString
                
                onlineUsersCell.postLikesBtn.tag = indexPath.row
                onlineUsersCell.postLikesBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postReplyBtn.tag = indexPath.row
                onlineUsersCell.postReplyBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postMessageBtn.tag = indexPath.row
                onlineUsersCell.postMessageBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.postDropDownBtn.tag = indexPath.row
                onlineUsersCell.postDropDownBtn.addTarget(self, action: #selector(WiRepliesViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                onlineUsersCell.viewProfile_btn.tag = indexPath.row
                onlineUsersCell.viewProfile_btn.addTarget(self, action: #selector(WiRepliesViewController.viewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return onlineUsersCell
            }
        }
        
        if (self.getAllFurtherReplies[indexPath.row].postedImage != "")
        {
            let imageURL = self.getAllFurtherReplies[indexPath.row].postedImage
            if (imageURL.contains("jpg"))
            {
                let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell", for: indexPath) as! CreditPackagesTableViewCell
                
                if (self.getAllFurtherReplies.count <= 0)
                {
                    return creditsCell
                }
                
                creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                creditsCell.animatedGifImage.isHidden = true
                creditsCell.tag = indexPath.row
                creditsCell.confessionLikeIcon.tag = indexPath.row
                creditsCell.animatedGifImage.tag = indexPath.row
                creditsCell.activityIndicator_nearBy.tag = indexPath.row
                creditsCell.moodTypeIcon.tag = indexPath.row
                
                DispatchQueue.main.async {
                    if self.getFullDataStr == "fetchAllData"
                    {
                            if (self.getAllFurtherReplies[indexPath.row].likedByMeStatus == "yes")
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                    }
                    else
                    {
                        if self.latestIntArr.contains(indexPath.row)
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                }
                
                let genderStr = self.getAllFurtherReplies[indexPath.row].gender
                var gender = String()
                if genderStr == "1"
                {
                    gender = "Male"
                }
                else
                {
                    gender = "Female"
                }
                
                
                
                creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                creditsCell.moodTypeIcon.clipsToBounds = true
                creditsCell.moodTypeIcon.layer.masksToBounds = true
                creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.typeOfConfession == "anonymous")
                {
                    creditsCell.userGenderAgeLbl.text = gender + ", " + self.getAllFurtherReplies[indexPath.row].age
                    
                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                    creditsCell.moodTypeIcon.addSubview(blurEffectView)
                }
                else
                {
                    creditsCell.userGenderAgeLbl.text = self.getAllFurtherReplies[indexPath.row].name
                }
                
                creditsCell.moodTypeIcon.sd_setImage(with: URL(string: self.getAllFurtherReplies[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                
//                creditsCell.userStatusLbl.text = self.allRepliesArray[indexPath.row].statusAdded
                
                let inputString: NSString = self.getAllFurtherReplies[indexPath.row].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                creditsCell.usersStatus_TxtView.text = modifiedString
                
                if (creditsCell.usersStatus_TxtView.text != "")
                {
                    let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                    creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                    let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                    var newFrame = creditsCell.usersStatus_TxtView.frame
                    newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                    
                    creditsCell.animatedImageHeight.constant = newSize.height
                }
                else{
                    creditsCell.animatedImageHeight.constant = 0
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                    
                    creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.getAllFurtherReplies[indexPath.row].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                        creditsCell.activityIndicator_nearBy.isHidden = true
                        
                        creditsCell.animatedGifImage.isHidden = false
//                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
//                        creditsCell.animatedGifImage.layer.masksToBounds = false
//                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                        creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell.animatedGifImage.clipsToBounds = true
                        
                    }
                })
                
//                let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGestureDetected(_:)))
//                pinchGestureRecognizer.delegate = self
//                creditsCell.animatedGifImage.addGestureRecognizer(pinchGestureRecognizer)
//                creditsCell.animatedGifImage.isUserInteractionEnabled = true
              
                if self.getAllFurtherReplies[indexPath.row].likesCount == "0"
                {
                    creditsCell.totalLikesCountLbl.text = ""
                }
                else
                {
                    creditsCell.totalLikesCountLbl.text = String(self.getAllFurtherReplies[indexPath.row].likesCount)
                }
                
                creditsCell.userLocationLbl.text = self.getAllFurtherReplies[indexPath.row].location
                creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
              
                if  self.getAllFurtherReplies[indexPath.row].replyCount == "0"
                {
                    creditsCell.totalReplyCountLbl.text = ""
                }
                else //if totalRepliesCount > 0
                {
                    creditsCell.totalReplyCountLbl.text = self.getAllFurtherReplies[indexPath.row].replyCount
                }
                
                creditsCell.confessionLikesBtn.tag = indexPath.row
                creditsCell.confessionLikesBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.confessionReplyBtn.tag = indexPath.row
                creditsCell.confessionReplyBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.confessionMessageBtn.tag = indexPath.row
                creditsCell.confessionMessageBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.dropDownBtn.tag = indexPath.row
                creditsCell.dropDownBtn.addTarget(self, action: #selector(WiRepliesViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.viewFullImageBtn.tag = indexPath.row
                creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiRepliesViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.viewProfileBtn.tag = indexPath.row
                creditsCell.viewProfileBtn.addTarget(self, action: #selector(WiRepliesViewController.viewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return creditsCell
            }
            else
            {
                let creditsCell = tableView.dequeueReusableCell(withIdentifier: "creditsCell1", for: indexPath) as! CreditPackagesTableViewCell
                
                if (self.getAllFurtherReplies.count <= 0)
                {
                    return creditsCell
                }
                
                creditsCell.selectionStyle = UITableViewCellSelectionStyle.none
                creditsCell.tag = indexPath.row
                creditsCell.confessionLikeIcon.tag = indexPath.row
                creditsCell.animatedGifImage.tag = indexPath.row
                creditsCell.animatedGifImage.isHidden = true
                creditsCell.activityIndicatorGif_nearBy.tag = indexPath.row
                creditsCell.moodTypeIcon.tag = indexPath.row
                
                DispatchQueue.main.async {
                    
                    if self.getFullDataStr == "fetchAllData"
                    {
                           if (self.getAllFurtherReplies[indexPath.row].likedByMeStatus == "yes")
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                            }
                            else
                            {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                            }
                    }
                    else
                    {
                        if self.latestIntArr.contains(indexPath.row)
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                    }
                }
      
                let genderStr = self.getAllFurtherReplies[indexPath.row].gender
                var gender = String()
                if genderStr == "1"
                {
                    gender = "Male"
                }
                else
                {
                    gender = "Female"
                }
                
                creditsCell.moodTypeIcon.layer.cornerRadius = creditsCell.moodTypeIcon.frame.size.height/2
                creditsCell.moodTypeIcon.clipsToBounds = true
                creditsCell.moodTypeIcon.layer.masksToBounds = true
                creditsCell.moodTypeIcon.contentMode = UIViewContentMode.scaleAspectFill
                
                if (self.typeOfConfession == "anonymous")
                {
                    creditsCell.userGenderAgeLbl.text = gender + ", " + self.getAllFurtherReplies[indexPath.row].age
                    
                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = creditsCell.moodTypeIcon.bounds
                    creditsCell.moodTypeIcon.addSubview(blurEffectView)
                }
                else
                {
                    creditsCell.userGenderAgeLbl.text = self.getAllFurtherReplies[indexPath.row].name
                }
                
                creditsCell.moodTypeIcon.sd_setImage(with: URL(string: self.getAllFurtherReplies[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
                
//                creditsCell.userStatusLbl.text = self.allRepliesArray[indexPath.row].statusAdded
                
                let inputString: NSString = self.getAllFurtherReplies[indexPath.row].statusAdded as NSString
                let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
                let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
                
                creditsCell.usersStatus_TxtView.dataDetectorTypes = UIDataDetectorTypes.link
                creditsCell.usersStatus_TxtView.text = modifiedString
                
                if (creditsCell.usersStatus_TxtView.text != "")
                {
                    let fixedWidth = creditsCell.usersStatus_TxtView.frame.size.width
                    creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                    let newSize = creditsCell.usersStatus_TxtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                    var newFrame = creditsCell.usersStatus_TxtView.frame
                    newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                    
                    creditsCell.animatedGifHeight.constant = newSize.height
                }
                else{
                    creditsCell.animatedGifHeight.constant = 0
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: { () -> Void in
                    
                    creditsCell.animatedGifImage.sd_setImage(with: URL(string: self.getAllFurtherReplies[indexPath.row].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                        
                         creditsCell.activityIndicatorGif_nearBy.isHidden = true
                        
                        creditsCell.animatedGifImage.isHidden = false
//                        creditsCell.animatedGifImage.layer.borderWidth = 2.0
//                        creditsCell.animatedGifImage.layer.masksToBounds = false
//                        creditsCell.animatedGifImage.layer.borderColor = UIColor.lightGray.cgColor
                        
                        creditsCell.animatedGifImage.contentMode = .scaleAspectFit
                        creditsCell.animatedGifImage.clipsToBounds = true
                        
                    }
                })
                
//                let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGestureDetected(_:)))
//                pinchGestureRecognizer.delegate = self
//                creditsCell.animatedGifImage.addGestureRecognizer(pinchGestureRecognizer)
//                creditsCell.animatedGifImage.isUserInteractionEnabled = true
           
                if self.getAllFurtherReplies[indexPath.row].likesCount == "0"
                {
                    creditsCell.totalLikesCountLbl.text = ""
                }
                else
                {
                    creditsCell.totalLikesCountLbl.text = String(self.getAllFurtherReplies[indexPath.row].likesCount)
                }
                
                creditsCell.userLocationLbl.text = self.getAllFurtherReplies[indexPath.row].location
                creditsCell.userLocationLbl.adjustsFontSizeToFitWidth = true
                
                if  self.getAllFurtherReplies[indexPath.row].replyCount == "0"
                {
                    creditsCell.totalReplyCountLbl.text = ""
                }
                else
                {
                    creditsCell.totalReplyCountLbl.text = self.getAllFurtherReplies[indexPath.row].replyCount
                }
                
                creditsCell.confessionLikesBtn.tag = indexPath.row
                creditsCell.confessionLikesBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.confessionReplyBtn.tag = indexPath.row
                creditsCell.confessionReplyBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.confessionMessageBtn.tag = indexPath.row
                creditsCell.confessionMessageBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.dropDownBtn.tag = indexPath.row
                creditsCell.dropDownBtn.addTarget(self, action: #selector(WiRepliesViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.viewFullImageBtn.tag = indexPath.row
                creditsCell.viewFullImageBtn.addTarget(self, action: #selector(WiRepliesViewController.viewFullSizeImageACtion(sender:)), for: UIControlEvents.touchUpInside)
                
                creditsCell.viewProfileBtn.tag = indexPath.row
                creditsCell.viewProfileBtn.addTarget(self, action: #selector(WiRepliesViewController.viewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
                
                return creditsCell
            }
        }
        else
        {
            let onlineUsersCell = tableView.dequeueReusableCell(withIdentifier: "statusOnlyCell", for: indexPath) as! ConfessionStatusTableViewCell
            
            if (self.getAllFurtherReplies.count <= 0)
            {
                return onlineUsersCell
            }
            
            onlineUsersCell.selectionStyle = UITableViewCellSelectionStyle.none
            onlineUsersCell.confessionLikeIcon.tag = indexPath.row
            
            DispatchQueue.main.async {
                
                if self.getFullDataStr == "fetchAllData"
                {
                        if (self.getAllFurtherReplies[indexPath.row].likedByMeStatus == "yes")
                        {
                            onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                        }
                        else
                        {
                            onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                        }
                }
                else
                {
                    if self.latestIntArr.contains(indexPath.row)
                    {
                        onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                    }
                    else
                    {
                        onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                    }
                }
            }
            
            let genderStr = self.getAllFurtherReplies[indexPath.row].gender
            var gender = String()
            if genderStr == "1"
            {
                gender = "Male"
            }
            else
            {
                gender = "Female"
            }
            
             onlineUsersCell.moodIcon.layer.cornerRadius =  onlineUsersCell.moodIcon.frame.size.height/2
             onlineUsersCell.moodIcon.clipsToBounds = true
             onlineUsersCell.moodIcon.layer.masksToBounds = true
             onlineUsersCell.moodIcon.contentMode = UIViewContentMode.scaleAspectFill
            
            if (self.typeOfConfession == "anonymous")
            {
                onlineUsersCell.userDetailsLbl.text = gender + ", " + self.getAllFurtherReplies[indexPath.row].age
                
                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame =  onlineUsersCell.moodIcon.bounds
                onlineUsersCell.moodIcon.addSubview(blurEffectView)
            }
            else
            {
                onlineUsersCell.userDetailsLbl.text = self.getAllFurtherReplies[indexPath.row].name
            }
            
             onlineUsersCell.moodIcon.sd_setImage(with: URL(string: self.getAllFurtherReplies[indexPath.row].profilePic), placeholderImage: UIImage(named: "Default Icon"), options: SDWebImageOptions.refreshCached)
            
            
            if self.getAllFurtherReplies[indexPath.row].likesCount == "0"
            {
                onlineUsersCell.likeCountLbl.text = ""
            }
            else
            {
                onlineUsersCell.likeCountLbl.text = String(self.getAllFurtherReplies[indexPath.row].likesCount)
            }
            
            if self.getAllFurtherReplies[indexPath.row].replyCount == "0"
            {
                onlineUsersCell.replyCountLbl.text = ""
            }
            else 
            {
                onlineUsersCell.replyCountLbl.text = self.getAllFurtherReplies[indexPath.row].replyCount
            }
            
            onlineUsersCell.userLocationLbl.text = self.getAllFurtherReplies[indexPath.row].location
            onlineUsersCell.userLocationLbl.adjustsFontSizeToFitWidth = true
          
            
            let inputString: NSString = self.getAllFurtherReplies[indexPath.row].statusAdded as NSString
            let regex = try? NSRegularExpression(pattern: "<a href=\"(.*?)\">.*?</a>", options: .caseInsensitive)
            let modifiedString = regex?.stringByReplacingMatches(in: inputString as String, options: [], range: NSRange(location: 0, length: inputString.length), withTemplate: "$1")
            
            onlineUsersCell.usersStatusTextView.text = modifiedString
            
            onlineUsersCell.postLikesBtn.tag = indexPath.row
            onlineUsersCell.postLikesBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionLikes_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            onlineUsersCell.postReplyBtn.tag = indexPath.row
            onlineUsersCell.postReplyBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionReply_btn(sender:)), for: UIControlEvents.touchUpInside)
            
            onlineUsersCell.postMessageBtn.tag = indexPath.row
            onlineUsersCell.postMessageBtn.addTarget(self, action: #selector(WiRepliesViewController.confessionMessage_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            onlineUsersCell.postDropDownBtn.tag = indexPath.row
            onlineUsersCell.postDropDownBtn.addTarget(self, action: #selector(WiRepliesViewController.DropDown_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            onlineUsersCell.viewProfile_btn.tag = indexPath.row
            onlineUsersCell.viewProfile_btn.addTarget(self, action: #selector(WiRepliesViewController.viewProfileWithProfileUsers_btnAction(sender:)), for: UIControlEvents.touchUpInside)
            
            return onlineUsersCell
        }
    }
    
    func singleViewiewProfileWithProfileUsers_btnAction(sender: UIButton!)
    {
        print("singleViewiewProfileWithProfileUsers_btnAction btn clicked")
        
        if (self.allRepliesArray.count > 0)
        {
            if (self.typeOfConfession == "anonymous")
            {
                print("post is anonymous")
            }
            else
            {
                print("post is with profile")
                
                let indexPathRow = IndexPath(row: sender.tag, section: 0)
                let userOwnerIDStr = self.allRepliesArray[sender.tag].userIDStr
                if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if id == userOwnerIDStr
                    {
                        print("profile owner")
                        let profileView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                        self.navigationController?.pushViewController(profileView, animated: true)
                    }
                    else
                    {
                        let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                        viewProfileScreen.ownerID = self.allRepliesArray[sender.tag].userIDStr
                        self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                    }
                }
            }
        }
    }
    
    
    func viewProfileWithProfileUsers_btnAction(sender: UIButton!)
    {
        print("viewProfileWithProfileUsers_btnAction btn clicked")
        
        if (self.getAllFurtherReplies.count > 0)
        {
            if (self.typeOfConfession == "anonymous")
            {
                print("post is anonymous")
            }
            else
            {
                print("post is with profile")
                
                let indexPathRow = IndexPath(row: sender.tag, section: 0)
                let userOwnerIDStr = self.getAllFurtherReplies[sender.tag].userIDStr
                if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                {
                    if id == userOwnerIDStr
                    {
                        print("profile owner")
                        let profileView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                        self.navigationController?.pushViewController(profileView, animated: true)
                    }
                    else
                    {
                        let viewProfileScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewOtherUsersProfileViewController") as! ViewOtherUsersProfileViewController
                        viewProfileScreen.ownerID = self.getAllFurtherReplies[sender.tag].userIDStr
                        self.navigationController?.pushViewController(viewProfileScreen, animated: true)
                    }
                }
            }
        }
    }
    
    //MARK:- ****** viewFullSizeImageACtion ******
    func viewFullSizeImageACtion(sender: UIButton!)
    {
        self.indexAtClicked = sender.tag
        
        if (self.getAllFurtherReplies.count <= 0)
        {
            return
        }
        
        if (self.getAllFurtherReplies[sender.tag].postedImage != "")
        {
            let imageURL = self.getAllFurtherReplies[sender.tag].postedImage
            if (imageURL.contains("jpg"))
            {
                self.fullImageView.sd_setImage(with: URL(string: self.getAllFurtherReplies[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: []) { (image, error, imageCacheType, imageUrl) in
                    print("image?.size.height = \(String(describing: image?.size.height))")
                    print("image?.size.width = \(String(describing: image?.size.width))")
                    
                    if ((image?.size.height) != nil) && ((image?.size.width) != nil)
                    {
                        self.showImagefullScreenView.isHidden = false
                        
                        self.fullImageAnimatedGif.isHidden = true
                        self.fullImageView.isHidden = false
                        
                        if (image?.size.height)! <= (self.repliesGifWrapperView.frame.size.height)
                        {
                            var gifWidth = Int()
                            var screenRes = Int()
                            var newXPos =  Int()
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  0
                            }
                            else{
                                gifWidth = Int((image?.size.width)!/2)
                                screenRes = Int(self.view.frame.width/2)
                                newXPos =  screenRes - gifWidth
                            }
                            
                            
                            let gifHeight = Int((image?.size.height)!/2)
                            let heightOfWrraperView = Int(self.repliesGifWrapperView.frame.height/2)
                            let newYPos: Int =  heightOfWrraperView - gifHeight
                            
                            self.repliesGifWrapperView.frame.origin.y = 0
                            self.fullImageView.frame.origin.y = CGFloat(newYPos)
                            self.fullImageView.frame.origin.x = CGFloat(newXPos)
                            
                            if (image?.size.width)! > (self.view.frame.size.width)
                            {
                                self.fullImageView.frame.size.width = self.view.frame.size.width
                            }
                            else{
                                self.fullImageView.frame.size.width = (image?.size.width)!
                            }
                            
                            if (image?.size.height)! <= (self.repliesGifWrapperView.frame.size.height)
                            {
                                self.fullImageView.frame.size.height = (image?.size.height)!
                            }
                            else{
                                self.fullImageView.frame.size.height = self.repliesGifWrapperView.frame.size.height
                            }
                            
                            self.imagesButtonWrapperView.frame.origin.y = self.fullImageView.frame.origin.y + self.fullImageView.frame.size.height + 10
                            self.repliesGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                        }
                        else{
                            self.fullImageView.frame.origin.y = 21
                            self.fullImageView.frame.size.height = (self.repliesGifWrapperView.frame.size.height - 21)
                            
                            self.imagesButtonWrapperView.frame.origin.y = self.repliesGifWrapperView.frame.origin.y + self.repliesGifWrapperView.frame.size.height + 10
                            self.repliesGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
                        }
                        
                        self.fullImageView.layer.borderWidth = 2.0
                        self.fullImageView.layer.masksToBounds = false
                        self.fullImageView.layer.borderColor = UIColor.lightGray.cgColor
                        self.fullImageView.contentMode = UIViewContentMode.scaleAspectFit
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Please wait.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                self.showImagefullScreenView.isHidden = false
                
                self.fullImageView.isHidden = true
                self.fullImageAnimatedGif.isHidden = false
                
                DispatchQueue.main.async {
                    self.fullImageAnimatedGif.sd_setImage(with: URL(string: self.getAllFurtherReplies[sender.tag].postedImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions.refreshCached)
                }
                
                self.fullImageAnimatedGif.layer.borderWidth = 2.0
                self.fullImageAnimatedGif.layer.masksToBounds = false
                self.fullImageAnimatedGif.layer.borderColor = UIColor.lightGray.cgColor
                self.fullImageAnimatedGif.contentMode = UIViewContentMode.scaleAspectFit
                
                print("self.getAllFurtherReplies[sender.tag].height = \(self.getAllFurtherReplies[sender.tag].height)")
                print("self.getAllFurtherReplies[sender.tag].width = \(self.getAllFurtherReplies[sender.tag].width)")
                
                /* let gifWidth = Int(self.allRepliesArray[sender.tag].width/2)
                 let screenRes = Int(self.view.frame.width/2)
                 let newXPos: Int =  screenRes - gifWidth
                 
                 let gifHeight = Int(self.allRepliesArray[sender.tag].height/2)
                 let heightOfWrraperView = Int(self.repliesGifWrapperView.frame.height/2)
                 let newYPos: Int =  heightOfWrraperView - gifHeight
                 
                 self.repliesGifWrapperView.frame.origin.y = 0
                 self.fullImageAnimatedGif.frame.origin.y = CGFloat(newYPos)
                 self.fullImageAnimatedGif.frame.origin.x = CGFloat(newXPos)
                 self.fullImageAnimatedGif.frame.size.width = CGFloat(self.allRepliesArray[sender.tag].width)
                 self.fullImageAnimatedGif.frame.size.height = CGFloat(self.allRepliesArray[sender.tag].height)*/
                
                
                var gifWidth = Int()
                var screenRes = Int()
                var newXPos =  Int()
                if (self.getAllFurtherReplies[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    gifWidth = Int(self.getAllFurtherReplies[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  0
                }
                else{
                    gifWidth = Int(self.getAllFurtherReplies[sender.tag].width/2)
                    screenRes = Int(self.view.frame.width/2)
                    newXPos =  screenRes - gifWidth
                }
                
                let gifHeight = Int(self.getAllFurtherReplies[sender.tag].height/2)
                let heightOfWrraperView = Int(self.repliesGifWrapperView.frame.height/2)
                let newYPos: Int =  heightOfWrraperView - gifHeight
                
                self.repliesGifWrapperView.frame.origin.y = 0
                self.fullImageAnimatedGif.frame.origin.y = CGFloat(newYPos)
                self.fullImageAnimatedGif.frame.origin.x = CGFloat(newXPos)
                
                if (self.getAllFurtherReplies[sender.tag].width) > Int(self.view.frame.size.width)
                {
                    self.fullImageAnimatedGif.frame.size.width = self.view.frame.size.width
                }
                else{
                    self.fullImageAnimatedGif.frame.size.width = CGFloat(self.getAllFurtherReplies[sender.tag].width)
                }
                
                self.fullImageAnimatedGif.frame.size.height = CGFloat(self.getAllFurtherReplies[sender.tag].height)
                
                self.imagesButtonWrapperView.frame.origin.y = self.fullImageAnimatedGif.frame.origin.y + self.fullImageAnimatedGif.frame.size.height + 10
                self.repliesGifWrapperView.bringSubview(toFront: self.imagesButtonWrapperView)
            }
        }
        
        
        DispatchQueue.main.async {
            
            if self.getFullDataStr == "fetchAllData"
            {
                if (self.getAllFurtherReplies[sender.tag].likedByMeStatus == "yes")
                {
                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
            else{
                if self.latestIntArr.contains(sender.tag)
                {
                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                }
                else
                {
                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                }
            }
        }
        
        if self.getAllFurtherReplies[sender.tag].likesCount == "0"
        {
            self.confessionLikesLbl.text = ""
        }
        else //if self.allRepliesArray[sender.tag].likesCount > 0
        {
            self.confessionLikesLbl.text = String(self.getAllFurtherReplies[sender.tag].likesCount)
        }
        
        if Int(self.getAllFurtherReplies[sender.tag].replyCount)! == 0
        {
            self.confessionReply.text = ""
        }
        else if Int(self.getAllFurtherReplies[sender.tag].replyCount)! > 0
        {
            self.confessionReply.text = self.getAllFurtherReplies[sender.tag].replyCount
        }
        else
        {
            self.confessionReply.text = ""
        }
    }
    
    
    // MARK: - ****** confessionLikes_btnAction ******
    func singleReplyLikes_btnAction(sender: UIButton!) {
        print("singleReplyLikes_btnAction btn clicked")
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WWAN")
            
            print("self.allRepliesArray[sender.tag].replyID = \(self.allRepliesArray[sender.tag].replyID)")
            
            if (self.allRepliesArray.count > 0)
            {
                self.likeDislikeConfessions(confessionID: self.allRepliesArray[sender.tag].replyID, completion: { (responseBool) in
                    print("responseBool = \(responseBool)")
                    
                    if (responseBool == "Liked successfully.")
                    {
                        self.indexPathSelected = IndexPath()
                        self.indexPathSelected = IndexPath(row: sender.tag, section: 0)
                        
                        self.latestIntArr.add(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if (self.allRepliesArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.allRepliesArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
//                            let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            let header:HeaderCommentsForText = self.repliesTableView.headerView(forSection: sender.tag) as! HeaderCommentsForText
                            
                            DispatchQueue.main.async {
                                
                                header.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                print("likesCountStr = \(likesCountStr)")
                                
                                if  (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    header.likesCountLbl.text = String(totalLikes)
                                    
                                    self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                    self.allRepliesArray[sender.tag].likesCount = "1"
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikesLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        header.likesCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                        self.allRepliesArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! + 1
                                        header.likesCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                        self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                       
                            if (typeOfCOnfession == "photo")
                            {
                                let header:HeaderCommentsForPhotos = self.repliesTableView.headerView(forSection: sender.tag) as! HeaderCommentsForPhotos
                                
                                DispatchQueue.main.async {
                                    header.likesIcon.image = UIImage(named: "LikeIconBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    print("likesCountStr = \(likesCountStr)")
                                    
                                    if (likesCountStr == "0")
                                    {
                                        header.likesCountLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            
                                        }
                                        else
                                        {
                                            
                                        }
                                    }
                                    
                                    if  (likesCountStr == "0")
                                    {
                                        let totalLikes = 1
                                        header.likesCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                        self.allRepliesArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            let totalLikes = 1
                                            header.likesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                            self.allRepliesArray[sender.tag].likesCount = "1"
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = "1"
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! + 1
                                            header.likesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                            self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                            else{
                                let header:HeaderCommentsForGif = self.repliesTableView.headerView(forSection: sender.tag) as! HeaderCommentsForGif
                                
                                DispatchQueue.main.async {
                                    header.confesionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                    print("likesCountStr = \(likesCountStr)")
                                    
                                    if (likesCountStr == "0")
                                    {
                                        header.likesCountLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            
                                        }
                                        else
                                        {
                                            
                                        }
                                    }
                                    
                                    if  (likesCountStr == "0")
                                    {
                                        let totalLikes = 1
                                        header.likesCountLbl.text = String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                        self.allRepliesArray[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            let totalLikes = 1
                                            header.likesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                            self.allRepliesArray[sender.tag].likesCount = "1"
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = "1"
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! + 1
                                            header.likesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "yes"
                                            self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
                        self.indexPathSelected = IndexPath()
                        self.indexPathSelected = IndexPath(row: sender.tag, section: 0)
                        
                        self.latestIntArr.remove(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if (self.allRepliesArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.allRepliesArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        
                        if (typeOfCOnfession == "text")
                        {
//                            let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            let header:HeaderCommentsForText = self.repliesTableView.headerView(forSection: sender.tag) as! HeaderCommentsForText
                            
                            DispatchQueue.main.async {
                                header.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                
                                var likesCountStr = String()
                                likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
                                print("likesCountStr = \(likesCountStr)")
                                
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 0
                                    header.likesCountLbl.text = ""//String(totalLikes)
                                    self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                    self.allRepliesArray[sender.tag].likesCount = ""
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikesLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 0
                                        header.likesCountLbl.text = ""// String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                        self.allRepliesArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            header.likesCountLbl.text  = ""
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                            self.allRepliesArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            header.likesCountLbl.text = String(totalLikes)
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                            self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                        
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (typeOfCOnfession == "photo")
                            {
                                 let header:HeaderCommentsForPhotos = self.repliesTableView.headerView(forSection: sender.tag) as! HeaderCommentsForPhotos
                                
                                DispatchQueue.main.async {
                                    header.likesIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
     
                                    if (likesCountStr == "0")
                                    {
                                        let totalLikes = 0
                                        header.likesCountLbl.text = "" //String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                        self.allRepliesArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            let totalLikes = 0
                                            header.likesCountLbl.text = "" //String(totalLikes)
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                            self.allRepliesArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                header.likesCountLbl.text  = ""
                                                
                                                self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                                self.allRepliesArray[sender.tag].likesCount = ""
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = ""
                                            }
                                            else
                                            {
                                                header.likesCountLbl.text = String(totalLikes)
                                                
                                                self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                                self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                let header:HeaderCommentsForGif = self.repliesTableView.headerView(forSection: sender.tag) as! HeaderCommentsForGif
                                
                                DispatchQueue.main.async {
                                    header.confesionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    
                                    var likesCountStr = String()
                                    likesCountStr = self.allRepliesArray[self.indexPathSelected.row].likesCount
     
                                    if (likesCountStr == "0")
                                    {
                                        let totalLikes = 0
                                        header.likesCountLbl.text = "" //String(totalLikes)
                                        
                                        self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                        self.allRepliesArray[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        if (likesCountStr == "")
                                        {
                                            let totalLikes = 0
                                            header.likesCountLbl.text = "" //String(totalLikes)
                                            
                                            self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                            self.allRepliesArray[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            let totalLikes = Int(self.allRepliesArray[self.indexPathSelected.row].likesCount)! - 1
                                            
                                            if (String(totalLikes) == "0")
                                            {
                                                header.likesCountLbl.text  = ""
                                                
                                                self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                                self.allRepliesArray[sender.tag].likesCount = ""
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = ""
                                            }
                                            else
                                            {
                                                header.likesCountLbl.text = String(totalLikes)
                                                
                                                self.allRepliesArray[sender.tag].likedByMeStatus = "no"
                                                self.allRepliesArray[sender.tag].likesCount = String(totalLikes)
                                                
                                                self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                                self.confessionLikesLbl.text = String(totalLikes)
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }
    
    
    // MARK: - ****** confessionLikes_btnAction ******
    func confessionLikes_btnAction(sender: UIButton!) {
     
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
     
            self.indexPathSelected = IndexPath()
            let buttonPosition = sender.convert(CGPoint.zero, to: self.repliesTableView)
            self.indexPathSelected = self.repliesTableView.indexPathForRow(at: buttonPosition)!
            
            
            let replyDict = self.allRepliesArray[self.indexPathSelected.section].child_comments_array[self.indexPathSelected.row] as! NSDictionary

            
            if (replyDict.count > 0)
            {
                self.likeDislikeConfessions(confessionID: (replyDict.value(forKey: "id") as! String), completion: { (responseBool) in
     
                    if (responseBool == "Liked successfully.")
                    {
                        self.latestIntArr.add(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if ((replyDict.value(forKey: "image") as! String) != "")
                        {
                            let imageURL = replyDict.value(forKey: "image") as! String
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = String(replyDict.value(forKey: "likes_count") as! Int)
     
                                if  (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                    
                                    self.getAllFurtherReplies[sender.tag].likedByMeStatus = "yes"
                                    self.getAllFurtherReplies[sender.tag].likesCount = "1"
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikesLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.getAllFurtherReplies[sender.tag].likedByMeStatus = "yes"
                                        self.getAllFurtherReplies[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.getAllFurtherReplies[self.indexPathSelected.row].likesCount)! + 1
                                        onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                        
                                        self.getAllFurtherReplies[sender.tag].likedByMeStatus = "yes"
                                        self.getAllFurtherReplies[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = String(replyDict.value(forKey: "likes_count") as! Int)
     
                                if (likesCountStr == "0")
                                {
                                    creditsCell.totalLikesCountLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        
                                    }
                                    else
                                    {
                                       
                                    }
                                }
                                
                                if  (likesCountStr == "0")
                                {
                                    let totalLikes = 1
                                    creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                    
                                    self.getAllFurtherReplies[sender.tag].likedByMeStatus = "yes"
                                    
                                    self.getAllFurtherReplies[sender.tag].likesCount = "1"
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                    self.confessionLikesLbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.getAllFurtherReplies[sender.tag].likedByMeStatus = "yes"
                                        self.getAllFurtherReplies[sender.tag].likesCount = "1"
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.getAllFurtherReplies[self.indexPathSelected.row].likesCount)! + 1
                                        creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                        
                                        self.getAllFurtherReplies[sender.tag].likedByMeStatus = "yes"
                                        self.getAllFurtherReplies[sender.tag].likesCount = String(totalLikes)
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "LikeIconBlue")
                                        self.confessionLikesLbl.text = String(totalLikes)
                                    }
                                }
                            }
                        }
                    }
                    else if (responseBool == "Unliked successfully.")
                    {
                        self.indexPathSelected = IndexPath()
                        let buttonPosition = sender.convert(CGPoint.zero, to: self.repliesTableView)
                        self.indexPathSelected = self.repliesTableView.indexPathForRow(at: buttonPosition)!
                        
                        self.latestIntArr.remove(self.indexPathSelected.row)
                        
                        var typeOfCOnfession = String()
                        if ((replyDict.value(forKey: "image") as! String) != "")
                        {
                            let imageURL = replyDict.value(forKey: "image") as! String
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        
                        if (typeOfCOnfession == "text")
                        {
                            let onlineUsersCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! ConfessionStatusTableViewCell
                            
                            DispatchQueue.main.async {
                                onlineUsersCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = String(replyDict.value(forKey: "likes_count") as! Int)
     
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 0
                                    onlineUsersCell.likeCountLbl.text = ""//String(totalLikes)
                                    self.getAllFurtherReplies[sender.tag].likedByMeStatus = "no"
                                    self.getAllFurtherReplies[sender.tag].likesCount = ""
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikesLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 0
                                        onlineUsersCell.likeCountLbl.text = ""// String(totalLikes)
                                        
                                        self.getAllFurtherReplies[sender.tag].likedByMeStatus = "no"
                                        self.getAllFurtherReplies[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.getAllFurtherReplies[self.indexPathSelected.row].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            onlineUsersCell.likeCountLbl.text  = ""
                                            
                                            self.getAllFurtherReplies[sender.tag].likedByMeStatus = "no"
                                            self.getAllFurtherReplies[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            onlineUsersCell.likeCountLbl.text = String(totalLikes)
                                            
                                            self.getAllFurtherReplies[sender.tag].likedByMeStatus = "no"
                                            self.getAllFurtherReplies[sender.tag].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                        
                                    }
                                }
                            }
                        }
                        else
                        {
                            let creditsCell = self.repliesTableView.cellForRow(at: self.indexPathSelected) as! CreditPackagesTableViewCell
                            
                            DispatchQueue.main.async {
                                creditsCell.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = String(replyDict.value(forKey: "likes_count") as! Int)
     
                                if (likesCountStr == "0")
                                {
                                    let totalLikes = 0
                                    creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                    
                                    self.getAllFurtherReplies[sender.tag].likedByMeStatus = "no"
                                    self.getAllFurtherReplies[sender.tag].likesCount = ""
                                    
                                    self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                    self.confessionLikesLbl.text = ""
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        let totalLikes = 0
                                        creditsCell.totalLikesCountLbl.text = "" //String(totalLikes)
                                        
                                        self.getAllFurtherReplies[sender.tag].likedByMeStatus = "no"
                                        self.getAllFurtherReplies[sender.tag].likesCount = ""
                                        
                                        self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                        self.confessionLikesLbl.text = ""
                                    }
                                    else
                                    {
                                        let totalLikes = Int(self.getAllFurtherReplies[self.indexPathSelected.row].likesCount)! - 1
                                        
                                        if (String(totalLikes) == "0")
                                        {
                                            creditsCell.totalLikesCountLbl.text  = ""
                                            
                                            self.getAllFurtherReplies[sender.tag].likedByMeStatus = "no"
                                            self.getAllFurtherReplies[sender.tag].likesCount = ""
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = ""
                                        }
                                        else
                                        {
                                            creditsCell.totalLikesCountLbl.text = String(totalLikes)
                                            
                                            self.getAllFurtherReplies[sender.tag].likedByMeStatus = "no"
                                            self.getAllFurtherReplies[sender.tag].likesCount = String(totalLikes)
                                            
                                            self.confessionLikeIcon.image = UIImage(named: "ConfessionLikeBlue")
                                            self.confessionLikesLbl.text = String(totalLikes)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
    }*/
    
    func likeDislikeConfessions(confessionID: String, completion: @escaping (String) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.likeUnlikeConfessions_Comments(userID: userID, userToken: authToken, comment_id: confessionID, completion:  { (responseData) in
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            let message = responseData.value(forKey: "message") as? String
                            
                            completion(message!)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(message!)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion("Error Occurs.")
                        }
                    })
                }
            }
        }
    }
    
    func deleteParticularConfession(replyToDelete: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    ApiHandler.deleteReplyForConfessions(userID: userID, userToken: authToken, confession_id: replyToDelete, completion: { (responseData) in
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                }
            }
        }
    }
    
    func reportParticularReplyOnConfession(replyIDToReport: String, completion: @escaping (Bool) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.view.isUserInteractionEnabled = false
                    self.loadingView.isHidden = false
                    
                    ApiHandler.reportContentOrUser(userID: userID, authToken: authToken, typeStr: "2", content_id: replyIDToReport, completion: { (responseData) in
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
//                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                           
                            completion(true)
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion(false)
                        }
                    })
                    
                }
            }
        }
    }

    
    
    /*
    // MARK: - ****** confessionReply_btn ******
    func singleReply_btn(sender: UIButton!) {
     
        if (self.allRepliesArray.count > 0)
        {
            self.postIDStr = self.allRepliesArray[sender.tag].postIDStr
            //self.allRepliesArray[sender.tag].replyID
            self.replyIDStr = self.allRepliesArray[sender.tag].replyID
            //self.allRepliesArray[sender.tag].furtherReplyID
   
            self.checkReplyType = "replyOnReply"
            self.writeReply_txtView.becomeFirstResponder()
        }
        
       
//        NotificationCenter.default.post(name: Notification.Name.UIKeyboardWillShow, object: nil)
        
       /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
        if self.checkReplyType == "replyOnPost"
        {
            homeScreen.postIdOwnerStr = self.allRepliesArray[sender.tag].userIDStr
            homeScreen.postIDStr = self.allRepliesArray[sender.tag].postIDStr
            homeScreen.replyIDStr = self.allRepliesArray[sender.tag].replyID
            homeScreen.checkReplyType = "replyOnReply"
            homeScreen.differentiateTypereplyStr = "firstReplyStr"
        }
        else if self.checkReplyType == "replyOnReply"
        {
            homeScreen.postIdOwnerStr = self.allRepliesArray[sender.tag].userIDStr
            homeScreen.postIDStr = self.allRepliesArray[sender.tag].replyID
            homeScreen.replyIDStr = self.allRepliesArray[sender.tag].furtherReplyID
            homeScreen.checkReplyType = "replyOnReply"
            homeScreen.differentiateTypereplyStr = "furtherReply"
        }
        self.navigationController?.pushViewController(homeScreen, animated: true)*/
    }
    
    // MARK: - ****** confessionReply_btn ******
    func confessionReply_btn(sender: UIButton!) {
     
        if (self.getAllFurtherReplies.count > 0)
        {
            var indexPathSelect = IndexPath()
            
            let buttonPosition = sender.convert(CGPoint.zero, to: self.repliesTableView)
            indexPathSelect = self.repliesTableView.indexPathForRow(at: buttonPosition)!
 
            self.postIDStr = (self.allRepliesArray[indexPathSelect.section].child_comments_array[indexPathSelect.row] as! NSDictionary).value(forKey: "confession_id") as! String
            self.replyIDStr = (self.allRepliesArray[indexPathSelect.section].child_comments_array[indexPathSelect.row] as! NSDictionary).value(forKey: "parent_comment_id") as! String
     
            self.checkReplyType = "replyOnReply"
            self.writeReply_txtView.becomeFirstResponder()
        }
        
        
       /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiRepliesViewController") as! WiRepliesViewController
        if self.checkReplyType == "replyOnPost"
        {
            homeScreen.postIdOwnerStr = self.getAllFurtherReplies[sender.tag].userIDStr
            homeScreen.postIDStr = self.getAllFurtherReplies[sender.tag].postIDStr
            homeScreen.replyIDStr = self.getAllFurtherReplies[sender.tag].replyID
            homeScreen.checkReplyType = "replyOnReply"
            homeScreen.differentiateTypereplyStr = "firstReplyStr"
        }
        else if self.checkReplyType == "replyOnReply"
        {
            homeScreen.postIdOwnerStr = self.getAllFurtherReplies[sender.tag].userIDStr
            homeScreen.postIDStr = self.getAllFurtherReplies[sender.tag].replyID
            homeScreen.replyIDStr = self.getAllFurtherReplies[sender.tag].furtherReplyID
            homeScreen.checkReplyType = "replyOnReply"
            homeScreen.differentiateTypereplyStr = "furtherReply"
        }
        self.navigationController?.pushViewController(homeScreen, animated: true)*/
    }
    
    
    // MARK: - ****** confessionMessage_btnAction ******
    func singleMessage_btnAction(sender: UIButton!) {
     
        let userOwnerIDStr =  self.allRepliesArray[sender.tag].userIDStr
        if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if id == userOwnerIDStr
            {
                print("owner of the post and current user is same")
            }
            else
            {
                let ownerProfileImage = UIImageView()
                ownerProfileImage.sd_setImage(with: URL(string: self.allRepliesArray[sender.tag].postedImage), placeholderImage: UIImage(named: "Default Icon"))
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                messagesScreen.mainConfessionCheck = "comingFromReplies"
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromStatusScreen" //"fromListingScreen"
                let userIDStr =  self.allRepliesArray[sender.tag].userIDStr
                messagesScreen.opponentName = self.allRepliesArray[sender.tag].name
                messagesScreen.opponentImage = self.allRepliesArray[sender.tag].profilePic
                messagesScreen.opponentUserID = userIDStr
                messagesScreen.postID = self.allRepliesArray[sender.tag].postIDStr
                messagesScreen.opponentFirebaseUserID = self.allRepliesArray[self.indexAtClicked].firebase_user_id
                messagesScreen.opponentProfilePic =  ownerProfileImage.image!
                
                if (self.typeOfConfession == "anonymous")
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                }
                else
                {
                    messagesScreen.typeOfUser = "WithProfileUser"
                    messagesScreen.checkVisibiltyStr = "withProfileChat"
                }
                
                messagesScreen.mainConfessionCheck = "comingFromReplies"
                
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }
        }
    }
    
    // MARK: - ****** confessionMessage_btnAction ******
    func confessionMessage_btnAction(sender: UIButton!) {
     
        var indexPathSelect = IndexPath()
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.repliesTableView)
        indexPathSelect = self.repliesTableView.indexPathForRow(at: buttonPosition)!
        
        let replyDict = self.allRepliesArray[indexPathSelect.section].child_comments_array[indexPathSelect.row] as! NSDictionary
        
        let userOwnerIDStr = replyDict.value(forKey: "user_id") as! String
        
        if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if id == userOwnerIDStr
            {
                print("owner of the post and current user is same")
            }
            else
            {
                let ownerProfileImage = UIImageView()
                
                let image = (replyDict.value(forKey: "user") as! NSDictionary).value(forKey: "profile_pic") as! String

                
                ownerProfileImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "Default Icon"))
                
                let messagesScreen = self.storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                messagesScreen.mainConfessionCheck = "comingFromReplies"
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromStatusScreen" //"fromListingScreen"
//                let userIDStr =  self.getAllFurtherReplies[sender.tag].userIDStr
                messagesScreen.opponentName = (replyDict.value(forKey: "user") as! NSDictionary).value(forKey: "username") as! String
                messagesScreen.opponentImage = image
                messagesScreen.opponentUserID = String((replyDict.value(forKey: "user") as! NSDictionary).value(forKey: "user_id") as! Int)
                messagesScreen.postID = (replyDict.value(forKey: "user") as! NSDictionary).value(forKey: "profile_pic") as! String
                messagesScreen.opponentFirebaseUserID = (replyDict.value(forKey: "user") as! NSDictionary).value(forKey: "firebase_user_id") as! String
//                messagesScreen.opponentProfilePic =  image
                
                if (self.typeOfConfession == "anonymous")
                {
                    messagesScreen.typeOfUser = "AnonymousUser"
                    messagesScreen.checkVisibiltyStr = "comingFromconfession"
                }
                else
                {
                    messagesScreen.typeOfUser = "WithProfileUser"
                    messagesScreen.checkVisibiltyStr = "withProfileChat"
                }
                
                messagesScreen.mainConfessionCheck = "comingFromReplies"
                
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }
        }
  
    }
    
    
    // MARK: - ****** DropDown_btnAction ******
    func singleDropDown_btnAction(sender: UIButton!) {
     
        if (self.allRepliesArray.count > 0)
        {
     
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    if (self.allRepliesArray[sender.tag].userIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                        if (self.allRepliesArray[sender.tag].postedImage != "")
                        {
                            let imageURL = self.allRepliesArray[sender.tag].postedImage
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: self.allRepliesArray[sender.tag].postIDStr, status: self.allRepliesArray[sender.tag].statusAdded, backgroudImageStr: self.allRepliesArray[sender.tag].postedImage, moodTypeImageStr: self.allRepliesArray[sender.tag].moodTypeStr, timeFrameStr: self.allRepliesArray[sender.tag].postHours, moodTypeStr: self.allRepliesArray[sender.tag].moodTypeStr, moodTypeUrl: self.allRepliesArray[sender.tag].moodTypeStr, photoTypeStr: typeOfCOnfession, replyIDStr: self.allRepliesArray[sender.tag].replyID, replyToReplyIDStr: self.allRepliesArray[sender.tag].furtherReplyID, getTotalLikeCount: self.allRepliesArray[sender.tag].likesCount, getTotalReplyCount: self.allRepliesArray[sender.tag].replyCount, gifWidth: self.allRepliesArray[sender.tag].width, gifHeight: self.allRepliesArray[sender.tag].height)
                    }
                    else
                    {
                        self.showActionSheetForDropDownMenu_otherUser(postID: self.allRepliesArray[sender.tag].postIDStr, ownerIDStr: self.allRepliesArray[sender.tag].userIDStr, replyID: self.allRepliesArray[sender.tag].replyID, furtherReplyID: self.allRepliesArray[sender.tag].furtherReplyID, myID: userID)
                    }
                }
            }
        }
    }
    
    
    // MARK: - ****** DropDown_btnAction ******
    func DropDown_btnAction(sender: UIButton!) {
     
        self.indexPathSelected = IndexPath()
        let buttonPosition = sender.convert(CGPoint.zero, to: self.repliesTableView)
        self.indexPathSelected = self.repliesTableView.indexPathForRow(at: buttonPosition)!
        
        
        let replyDict = self.allRepliesArray[self.indexPathSelected.section].child_comments_array[self.indexPathSelected.row] as! NSDictionary
     
        if (replyDict.count > 0)
        {
    
            
            
            let userOwnerIDStr = replyDict.value(forKey: "user_id") as! String
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let authToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    if (userOwnerIDStr == userID)
                    {
                        var typeOfCOnfession = String()
                       if ((replyDict.value(forKey: "image") as! String) != "")
                        {
                           let imageURL = replyDict.value(forKey: "image") as! String
                            if (imageURL.contains("jpg"))
                            {
                                typeOfCOnfession = "photo"
                            }
                            else
                            {
                                typeOfCOnfession = "gif"
                            }
                        }
                        else
                        {
                            typeOfCOnfession = "text"
                        }
                        
                        
                        let postID = replyDict.value(forKey: "confession_id") as! String
                        let statusAdded = replyDict.value(forKey: "text") as! String
                        let postedImage = replyDict.value(forKey: "image") as! String
                        let postHours = "24 Hrs"
                        let moodTypeStr = (replyDict.value(forKey: "user") as! NSDictionary).value(forKey: "profile_pic") as! String
                        let replyID = replyDict.value(forKey: "id") as! String
                        let furtherReplyID = replyDict.value(forKey: "parent_comment_id") as! String
                        let likesCount = String(replyDict.value(forKey: "likes_count") as! Int)
                        let replyCount = String(replyDict.value(forKey: "comments_count") as! Int)
                        let width = Int(replyDict.value(forKey: "image_width")  as! String)
                        let height = Int(replyDict.value(forKey: "image_height")  as! String)
                        
                        self.showActionSheetForDropDownMenu_owner(postIdStr: postID, status: statusAdded, backgroudImageStr: postedImage, moodTypeImageStr: moodTypeStr, timeFrameStr: postHours, moodTypeStr: moodTypeStr, moodTypeUrl: moodTypeStr, photoTypeStr: typeOfCOnfession, replyIDStr: replyID, replyToReplyIDStr: furtherReplyID, getTotalLikeCount: likesCount, getTotalReplyCount: replyCount, gifWidth: width!, gifHeight: height!)
                    }
                    else
                    {
                        let postID = replyDict.value(forKey: "confession_id") as! String
                        let replyID = replyDict.value(forKey: "id") as! String
                        let furtherReplyID = replyDict.value(forKey: "parent_comment_id") as! String
                        
                        self.showActionSheetForDropDownMenu_otherUser(postID: postID, ownerIDStr: userOwnerIDStr, replyID: replyID, furtherReplyID: furtherReplyID, myID: userID)
                    }
                }
            }
        }
    }*/
    
    // MARK: - ****** Show Action Sheet From select options. ******
    func showActionSheetForDropDownMenu_otherUser(postID: String, ownerIDStr: String, replyID: String, furtherReplyID: String,myID: String) {
        
        let actionSheet = UIAlertController(title: "Please choose your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Report", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.reportBool = true
            
            if self.checkReplyType == "replyOnPost"
            {
                self.reportParticularReplyOnConfession(replyIDToReport: replyID, completion: { (responseBool) in
                    
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    
                    if (responseBool == true)
                    {
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        case .online(.wwan) , .online(.wiFi):
                            
                            self.allRepliesArray = []
                            self.getAllFurtherReplies = []
                            self.currentPage = 1
                            self.repliesTableView.reloadData()

                            self.getReplyOnConfessionListing(paginationValue: self.currentPage, replyID: self.postIDStr, pageID: self.currentPageStr, completion: { (response) in
                                
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                if (response == true)
                                {
                                    DispatchQueue.main.async {
                                        self.repliesTableView.isHidden = false
                                        self.noRepliesFound.isHidden = true
                                        self.repliesTableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                    }
                                }
                                else
                                {
                                    self.allRepliesArray = []
                                    self.getAllFurtherReplies = []
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.repliesTableView.isHidden = false
                                    self.repliesTableView.reloadData()
                                    self.refreshControl.endRefreshing()
                                    self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                        self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                    }, completion: { (finished) in
                                    })
                                }
                            })
                        }
                    }
                })
            }
            else if self.checkReplyType == "replyOnReply"
            {
   
                self.reportParticularReplyOnConfession(replyIDToReport: furtherReplyID, completion: { (responseBool) in
                    
                    self.view.isUserInteractionEnabled = true
                    self.loadingView.isHidden = true
                    
                    if (responseBool == true)
                    {
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        case .online(.wwan) , .online(.wiFi):
                            
                            self.allRepliesArray = []
                            self.getAllFurtherReplies = []
                            self.currentPage = 1
                            self.repliesTableView.reloadData()

                            self.getReplyOnConfessionListing(paginationValue: self.currentPage, replyID: self.postIDStr, pageID: self.currentPageStr, completion: { (response) in
                                
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                                if (response == true)
                                {
                                    DispatchQueue.main.async {
                                        self.repliesTableView.isHidden = false
                                        self.noRepliesFound.isHidden = true
                                        self.repliesTableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                    }
                                }
                                else
                                {
                                    self.allRepliesArray = []
                                    self.getAllFurtherReplies = []
                                    self.loadingView.isHidden = true
                                    self.view.isUserInteractionEnabled = true
                                    self.repliesTableView.isHidden = false
                                    self.repliesTableView.reloadData()
                                    self.refreshControl.endRefreshing()
                                    self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                        self.noRepliesFound.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                    }, completion: { (finished) in
                                    })
                                }
                            })
                        }
                    }
                })
            }
        }))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
  
    
    // MARK: - ****** Show Action Sheet From select options. ******
    func showActionSheetForDropDownMenu_owner(postIdStr: String, status: String, backgroudImageStr : String, moodTypeImageStr: String, timeFrameStr: String, moodTypeStr: String, moodTypeUrl: String, photoTypeStr: String,replyIDStr: String, replyToReplyIDStr: String,getTotalLikeCount: String, getTotalReplyCount: String,gifWidth: Int,gifHeight: Int) {
        let actionSheet = UIAlertController(title: "Please choose your option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Edit", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let createConfession = storyboard.instantiateViewController(withIdentifier: "WiCreateConfessionViewController") as! WiCreateConfessionViewController
            createConfession.updateStr = "EditConfession"
            createConfession.updateMoodUrl = moodTypeUrl
            createConfession.updateTimeframe = timeFrameStr
            createConfession.updatePhotoTypeStr = photoTypeStr
            createConfession.updateStatusText = status
            createConfession.updateBackgroundImageUrl = backgroudImageStr
            createConfession.updateMoodTypeStr = moodTypeStr
            createConfession.updatePostID = postIdStr
            createConfession.updateReplyID = replyIDStr
            createConfession.updateReplyToReplyID = replyToReplyIDStr
            createConfession.updateLikeCountStr = getTotalLikeCount
            createConfession.updateReplyCountStr = getTotalReplyCount
            createConfession.updateGifWidth = gifWidth
            createConfession.updateGifHeight = gifHeight
            createConfession.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
            createConfession.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
            createConfession.currentLocationFromHomePage = self.currentLocationFromHomePage
            
            if self.checkReplyType == "replyOnPost"
            {
                createConfession.postTypeStr = "ForReplyOnPost"
            }
            else if self.checkReplyType == "replyOnReply"
            {
                createConfession.postTypeStr = "ForReplyOnReply"
            }
            
            self.navigationController?.pushViewController(createConfession, animated: true)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            
            if self.checkReplyType == "replyOnPost"
            {
                    if  (replyIDStr != "")
                    {
                        self.decreaseCountBool = true
                        self.deleteConfession(postIDStr: postIdStr, replyID: replyIDStr, replyOnReplyIDStr: replyToReplyIDStr)
                    }
            }
            else if self.checkReplyType == "replyOnReply"
            {
                    if (replyIDStr != "") //(replyIDStr != "") && (replyIDStr != "")
                    {
                        self.decreaseCountBool = true
                        
                        self.deleteConfession(postIDStr: postIdStr, replyID: replyIDStr, replyOnReplyIDStr: replyToReplyIDStr)
                    }
            }

        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }

    
    // MARK: - ****** deleteConfession ******
    func deleteConfession(postIDStr: String, replyID: String,replyOnReplyIDStr: String)
    {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "Delete Wi Post", message: "Do you really want to delete post?", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }
        actionSheetController.addAction(cancelActionButton)
        
        let reportActionButton = UIAlertAction(title: "Delete", style: .destructive) { action -> Void in
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                
                if self.checkReplyType == "replyOnPost"
                {
                    self.deleteParticularConfession(replyToDelete: replyID, completion: { (responseBool) in
                        
                        if (responseBool == true)
                        {
                            self.getRepliesListing { (responseBool) in
                            }
                        }
                    })
                }
                else  if self.checkReplyType == "replyOnReply"
                {
                    if self.differentiateTypereplyStr == "firstReplyStr"
                    {
                     
                    }
                    else if self.differentiateTypereplyStr == "furtherReply"
                    {
                       
                    }
                    
                    self.deleteParticularConfession(replyToDelete: replyOnReplyIDStr, completion: { (responseBool) in
                        
                        if (responseBool == true)
                        {
                            self.getRepliesListing { (responseBool) in
                            }
                        }
                    })
                }
            }
        }
        
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
        actionSheetController.addAction(reportActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }

    
    // MARK: - ****** back_btnAction ******
    @IBAction func back_btnAction(_ sender: Any) {
        if appDelegatePush == "fromAppDelegate"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeScreen = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(homeScreen, animated: true)
        }
        else
        {
            popInfoDelegate?.popInformationOnPopMethod(returnFromView: "fromWiReplies")
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    
    
    // MARK: - ****** reply_btnAction ******
    @IBAction func reply_btnAction(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = storyboard.instantiateViewController(withIdentifier: "WiCreateConfessionViewController") as! WiCreateConfessionViewController
        if self.checkReplyType == "replyOnPost"
        {
            homeScreen.postTypeStr = "ForReplyOnPost"
            homeScreen.differentiateString = "wiFeedsReply"
            homeScreen.postIdOwner = self.postIdOwnerStr
            homeScreen.currentPostIDStr = self.postIDStr
            homeScreen.currentReplyIDStr = self.replyIDStr
            homeScreen.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
            homeScreen.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
            homeScreen.currentLocationFromHomePage = self.currentLocationFromHomePage
        }
        else if self.checkReplyType == "replyOnReply"
        {
            homeScreen.postTypeStr = "ForReplyOnReply"
            if differentiateTypereplyStr  == "firstReplyStr"
            {
                homeScreen.differentiateString = "replyFeedsReply"
            }
            else if differentiateTypereplyStr == "furtherReply"
            {
                homeScreen.differentiateString = "replyFeedsFurtherReply"
            }
            homeScreen.postIdOwner = self.postIdOwnerStr
            homeScreen.currentPostIDStr = self.postIDStr
            homeScreen.currentReplyIDStr = self.replyIDStr
            homeScreen.currentLatitudeFromHomePage = self.currentLatitudeFromHomePage
            homeScreen.currentLongitudeFromHomePage = self.currentLongitudeFromHomePage
            homeScreen.currentLocationFromHomePage = self.currentLocationFromHomePage
        }
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }

    
    // MARK: - ****** didReceiveMemoryWarning ******
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
