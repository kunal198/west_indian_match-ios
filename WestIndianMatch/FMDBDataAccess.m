//
//  FMDBDataAccess.m
//  Chanda
//
//  Created by Mohammad Azam on 10/25/11.
//  Copyright (c) 2011 HighOnCoding. All rights reserved.
//

#import "FMDBDataAccess.h"

@implementation FMDBDataAccess


//-(BOOL) updateCustomer:(Country *)customer
//{
//    FMDatabase *db = [FMDatabase databaseWithPath:[Utility getDatabasePath]];
//    
//    [db open];
//    
//    BOOL success = [db executeUpdate:[NSString stringWithFormat:@"UPDATE customers SET firstname = '%@', lastname = '%@' where id = %d",customer.firstName,customer.lastName,customer.customerId]];
//    
//    [db close];
//    
//    return success; 
//}

//-(BOOL) insertCustomer:(Country *) customer
//{
//    // insert customer into database
//    
//    FMDatabase *db = [FMDatabase databaseWithPath:[Utility getDatabasePath]];
//    
//    [db open];
//    
//    BOOL success =  [db executeUpdate:@"INSERT INTO customers (firstname,lastname) VALUES (?,?);",
//                     customer.firstName,customer.lastName, nil];
//    
//    [db close];
//    
//    return success; 
//    
//    return YES; 
//}

-(NSMutableArray *) getCustomers
{
    NSMutableArray *customers = [[NSMutableArray alloc] init];
    
//    FMDatabase *db = [FMDatabase databaseWithPath:[Utility getDatabasePath]];
    
    NSBundle *myDbBundle  = [NSBundle bundleWithPath:[[NSBundle mainBundle]pathForResource:@"myDbBundle" ofType:@"bundle"]];
    NSLog(@"BundleIdentifier:%@",[myDbBundle bundleIdentifier]);
    self.databasePath = [myDbBundle pathForResource:@"Country" ofType:@"sqlite"];
    NSLog(@"DataBasePath:%@",_databasePath);
    
    FMDatabase *db = [FMDatabase databaseWithPath:_databasePath];
    [db open];
    
    FMResultSet *results = [db executeQuery:@"SELECT country_id,name FROM countries"];
    while([results next])
    {
        Country *customer = [[Country alloc] init];
        customer.customerId = [results intForColumn:@"country_id"];
        customer.firstName = [results stringForColumn:@"name"];
//        customer.lastName = [results stringForColumn:@"lastname"];
        [customers addObject:customer];
    }
    
    [db close];
  
    return customers; 
}



-(NSMutableArray *) getStatesForCountrySelected:(NSString *)countryId
{
    NSMutableArray *customers = [[NSMutableArray alloc] init];
//    FMDatabase *db = [FMDatabase databaseWithPath:[Utility getDatabasePath]];
    NSBundle *myDbBundle  = [NSBundle bundleWithPath:[[NSBundle mainBundle]pathForResource:@"myDbBundle" ofType:@"bundle"]];
    NSLog(@"BundleIdentifier:%@",[myDbBundle bundleIdentifier]);
    self.databasePath = [myDbBundle pathForResource:@"Country" ofType:@"sqlite"];
    NSLog(@"DataBasePath:%@",_databasePath);
    
    FMDatabase *db = [FMDatabase databaseWithPath:_databasePath];
    [db open];
    
    FMResultSet *results = [db executeQuery:@"SELECT name FROM zone where country_id = 223"];
    
    while([results next])
    {
        Country *customer = [[Country alloc] init];
//        customer.customerId = [results intForColumn:@"country_id"];
//        customer.firstName = [results stringForColumn:@"name"];
          customer.lastName = [results stringForColumn:@"name"];
        [customers addObject:customer];
    }
    
    [db close];
    
    return customers;
}






@end
