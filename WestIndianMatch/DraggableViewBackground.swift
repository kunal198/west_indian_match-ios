//
//  DraggableViewBackground.swift
//  TinderSwipeCardsSwift
//
//  Created by Gao Chao on 4/30/15.
//  Copyright (c) 2015 gcweb. All rights reserved.
//

import Foundation
import UIKit
//import Firebase
import SDWebImage
import FirebaseInstanceID

class DraggableViewBackground: UIView, DraggableViewDelegate {
    
    var exampleCardLabels: [String]!
    var allCards: [DraggableView]!

    let MAX_BUFFER_SIZE = 25
    let CARD_HEIGHT: CGFloat = 310
    let CARD_WIDTH: CGFloat = 290

    var relVibesStr: String!
    var typeOfButton: String!
    var profileOwnerID: String!
    var userID: String!
    var opponentUserId: String!
    var myInterestesdInStr: String!
    var myDeviceTokenStr:String!
    var myNewMatchesCount:String!
    var ownerNewMatchesCount:String!
    var ownerLinkUpCount:String!
    var opponentDeviceToken: String!
    var opponentNewMatchesStr:String!
    var opponentLinkUpStr:String!
    var newMatchesVisibilityStr:String!
    var meetUpUserArray_Vibes = NSMutableArray()
    var meetUpUserLikedArray = NSMutableArray()
    var badgeCount = Int()
    var opponentNameStr:String!
    var opponentPictureStr:String!
    
    var cardsLoadedIndex: Int!
    var userAtIndex = Int()
    var loadedCards: [DraggableView]!
    var menuButton: UIButton!
    var messageButton: UIButton!
    var checkButton: UIButton!
    var xButton: UIButton!
    var superLikeButton: UIButton!
    var swipeTypeStr: UILabel!
    
    var getUsersArray = NSMutableArray()

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        super.layoutSubviews()
        

        self.setupView()
//        typeOfButton = "RightSwipe"
        self.typeOfButton = ""
        self.userID = ""
        self.opponentUserId = ""
        xButton.isHidden = true
        superLikeButton.isHidden = true
        checkButton.isHidden = true
        swipeTypeStr.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.swipeInformation), name: NSNotification.Name(rawValue: "SwipeNotify"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.anyAction), name: NSNotification.Name(rawValue: "SendAllUsers"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.noUsersExists), name: NSNotification.Name(rawValue: "NoUsersExists"), object: nil)
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserProfile), name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshBtnNotify), name: NSNotification.Name(rawValue: "refreshBtnNotify"), object: nil)
    
        exampleCardLabels = ["Default Icon - 3", "Default Icon - 3", "Default Icon - 3", "Default Icon - 3", "Default Icon - 3","Default Icon - 3", "Default Icon - 3", "Default Icon - 3", "Default Icon - 3", "Default Icon - 3"]
        allCards = []
        loadedCards = []
        cardsLoadedIndex = 0
        loadedCards.removeAll()
        allCards.removeAll()
    }
    
    // MARK: - ****** Method to show no users found ******
    func refreshBtnNotify(_ anote: Notification)
    {
        allCards = []
        loadedCards = []
        cardsLoadedIndex = 0
        loadedCards.removeAll()
        allCards.removeAll()
    }
    
    func updateUserProfile()
    {
        if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
        {
            if (checkPackageStatus == "packagePurchased")
            {
//                print("checkPackageStatus = \(checkPackageStatus)")
            
            }
            else
            {
                self.relVibesStr = "relLikesNotPurchased"
            }
        }
        else
        {
            self.relVibesStr = "relLikesNotPurchased"
        }
    }
    
    func getUserDetails()
    {
        //print("getUserDetails called")
         var profileOwnerID = String()
        if cardsLoadedIndex < allCards.count
        {
            profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "UserID") as! String
        }
        else
        {
            profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "UserID") as! String
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    func swipeInformation(_ anote: Notification)
    {
        let draggableView = DraggableView()
        let userInfo = (anote as NSNotification).userInfo
        let swipeStatus = (userInfo?["userInfo"] as? String)!
//        //print("userInfo for swipeInformation string= \(swipeStatus)")
        if swipeStatus == "swipedLeft"
        {
            draggableView.swipeTitle_lbl.text = "Nah"
            draggableView.swipeTitle_lbl.textAlignment = NSTextAlignment.center
            draggableView.swipeTitle_lbl.font = UIFont.boldSystemFont(ofSize: 60)
            draggableView.swipeTitle_lbl.textColor = UIColor.red
            draggableView.swipeTitle_lbl.backgroundColor = UIColor.init(red: 37.0/255, green: 43.0/255, blue: 58.0/255, alpha: 1.0)
            draggableView.swipeTitle_lbl.layer.cornerRadius = 10
            draggableView.swipeTitle_lbl.layer.masksToBounds = true
            
            draggableView.swipeTitle_lbl.isHidden = false
            draggableView.swipeTitle_lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                     draggableView.swipeTitle_lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: { (finished) in
            })
        }
        else if swipeStatus == "swipeEnded"
        {
            draggableView.swipeTitle_lbl.isHidden = true
        }
        else
        {
            typeOfButton = "RightSwipe"
            draggableView.swipeTitle_lbl.text = "Vibes"
            draggableView.swipeTitle_lbl.textAlignment = NSTextAlignment.center
            draggableView.swipeTitle_lbl.font = UIFont.boldSystemFont(ofSize: 60)
            draggableView.swipeTitle_lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            draggableView.swipeTitle_lbl.backgroundColor = UIColor.init(red: 37.0/255, green: 43.0/255, blue: 58.0/255, alpha: 1.0)
            draggableView.swipeTitle_lbl.layer.cornerRadius = 10
            draggableView.swipeTitle_lbl.layer.masksToBounds = true
            
            draggableView.swipeTitle_lbl.isHidden = false
            draggableView.swipeTitle_lbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                draggableView.swipeTitle_lbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })
        }
    }
    
    func noUsersExists(_ anote: Notification)
    {
        xButton.isHidden = true
        superLikeButton.isHidden = true
        checkButton.isHidden = true
        swipeTypeStr.isHidden = true
    }
    
    func anyAction(_ anote: Notification)
    {
        self.isHidden = true
        self.getUsersArray = []
        self.loadedCards = []
        allCards = []

        loadedCards.removeAll()
        allCards.removeAll()
        cardsLoadedIndex = 0
        let dict: [AnyHashable: Any]? = anote.userInfo
        var newDataArray = NSArray()
        newDataArray = dict?["userInfo"] as! NSArray
        
        if newDataArray.count > 0
        {
            for index in 0..<newDataArray.count
            {
                let getUserDetailsDict = newDataArray[index] as! NSDictionary
                
                if !(self.getUsersArray.contains(getUserDetailsDict))
                {
                    self.getUsersArray.add(getUserDetailsDict)
                }
            }
            
            self.userAtIndex = 0
            self.loadCards()
        }
    }

    func setupView() -> Void
    {
        if UIScreen.main.bounds.size.width == 320
        {
             xButton = UIButton(frame: CGRect(x: 40, y:  CARD_HEIGHT + 50, width: 60, height: 60))
             superLikeButton = UIButton(frame: CGRect(x: 130, y:  CARD_HEIGHT + 50, width: 60, height: 60))
             checkButton = UIButton(frame: CGRect(x: 220, y:  CARD_HEIGHT + 50, width: 60, height: 60))
             swipeTypeStr = UILabel(frame: CGRect(x: 100, y:  CARD_HEIGHT + 2, width: 120, height: 35))
        }
        else if UIScreen.main.bounds.size.width == 375
        {
             xButton = UIButton(frame: CGRect(x: 40, y:  CARD_HEIGHT + 100, width: 60, height: 60))
            superLikeButton = UIButton(frame: CGRect(x: 150, y:  CARD_HEIGHT + 100, width: 60, height: 60))
            checkButton = UIButton(frame: CGRect(x: 280, y:  CARD_HEIGHT + 100, width: 60, height: 60))
            swipeTypeStr = UILabel(frame: CGRect(x: 100, y:  330 + 20, width: 150, height: 30))
        }
        else if UIScreen.main.bounds.size.width == 414
        {
             xButton = UIButton(frame: CGRect(x: 50, y:  CARD_HEIGHT + 170, width: 60, height: 60))
             superLikeButton = UIButton(frame: CGRect(x: 180, y:  CARD_HEIGHT + 170, width: 60, height: 60))
             checkButton = UIButton(frame: CGRect(x: 300, y:  CARD_HEIGHT + 170, width: 60, height: 60))
             swipeTypeStr = UILabel(frame: CGRect(x: 140, y:  110, width: 150, height: 40))
        }
        
        xButton.setImage(UIImage(named: "Swipe Left Icon"), for: UIControlState.normal)
        xButton.addTarget(self, action: #selector(DraggableViewBackground.swipeLeft), for: UIControlEvents.touchUpInside)
        
        superLikeButton.setImage(UIImage(named: "Super Like Green"), for: UIControlState.normal)
        superLikeButton.addTarget(self, action: #selector(DraggableViewBackground.superLikeForVibes), for: UIControlEvents.touchUpInside)
    
        checkButton.setImage(UIImage(named: "Swipe Right Icon"), for: UIControlState.normal)
        checkButton.addTarget(self, action: #selector(DraggableViewBackground.swipeRight), for: UIControlEvents.touchUpInside)
        
        self.addSubview(xButton)
        self.addSubview(superLikeButton)
        self.addSubview(checkButton)
    }
    
    

    func createDraggableViewWithDataAtIndex(index: NSInteger) -> DraggableView
    {
       var draggableView = DraggableView()
        if UIScreen.main.bounds.size.width == 320
        {
           draggableView = DraggableView(frame: CGRect(x:  15, y:  10, width: CARD_WIDTH, height: CARD_HEIGHT))
        }
        else if UIScreen.main.bounds.size.width == 375
        {
             draggableView = DraggableView(frame: CGRect(x:  15, y:  10, width: 345, height: 330))
        }
        else if UIScreen.main.bounds.size.width == 414
        {
           draggableView = DraggableView(frame: CGRect(x:  20, y:  10, width: 374, height: 450))
        }
        
        let dataDict = self.getUsersArray[index] as! NSDictionary
        let keysArray = dataDict.allKeys as! [String]
        
        draggableView.profilePicImage.isHidden = true
        
        if (keysArray.contains("profile_pic") && keysArray.contains("location") && keysArray.contains("age"))
        {
            let userLocation =  ((self.getUsersArray[index]) as AnyObject).value(forKey: "location") as? String
            let userAge = ((self.getUsersArray[index]) as AnyObject).value(forKey: "age") as? String
            draggableView.userDetailsLabel.text = userAge! + "" + "," + " " + userLocation!
            draggableView.userDetailsLabel.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
            draggableView.userDetailsLabel.backgroundColor = UIColor.init(red: 37.0/255, green: 43.0/255, blue: 58.0/255, alpha: 1.0)
            draggableView.userDetailsLabel.textAlignment = NSTextAlignment.right
            
                let urlStr = ((self.getUsersArray[index]) as AnyObject).value(forKey: "profile_pic") as! String
            
                DispatchQueue.main.async{
                    
                    draggableView.profilePicImage.sd_setImage(with: URL(string: urlStr), placeholderImage: UIImage(named: ""), completed: { (image, err, imageCacheType, imageUrl) in
                        
                        draggableView.profilePicImage.isHidden = false
                        draggableView.profilePicImage.contentMode = UIViewContentMode.scaleAspectFill
                        draggableView.profilePicImage.clipsToBounds = true
                        draggableView.contentMode = UIViewContentMode.center
                        
                        if index+1 == self.getUsersArray.count
                        {
                            DispatchQueue.main.async {
                                self.isHidden = false
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "stopRefreshingLoader"), object: nil, userInfo: nil)
                            }
                        }
                        
                    })
//                    draggableView.profilePicImage.sd_setImage(with: URL(string: urlStr), placeholderImage: UIImage(named: ""), options: SDWebImageOptions.cacheMemoryOnly)
                }
        }
        
        draggableView.delegate = self
        return draggableView
    }

    
    func loadCards() -> Void {
        
        if self.getUsersArray.count > 0
        {
            
            DispatchQueue.main.async {
                
                self.xButton.isHidden = false
                self.superLikeButton.isHidden = false
                self.checkButton.isHidden = false
                
                let numLoadedCardsCap = self.getUsersArray.count > self.MAX_BUFFER_SIZE ? self.MAX_BUFFER_SIZE : self.getUsersArray.count
                
                var newCard = DraggableView()
                
                for i in 0 ..< self.getUsersArray.count
                {
                    newCard = self.createDraggableViewWithDataAtIndex(index: i)
                    
                    if !(self.allCards.contains(newCard))
                    {
                        self.allCards.append(newCard)
                    }
                    
                    if i < numLoadedCardsCap
                    {
                        if !(self.loadedCards.contains(newCard))
                        {
                            self.loadedCards.append(newCard)
                        }
                    }
                }
                
                for i in 0 ..< self.loadedCards.count
                {
                    if i > 0
                    {
                        self.insertSubview(self.loadedCards[i], belowSubview: self.loadedCards[i - 1])
                    }
                    else
                    {
                        self.addSubview(self.loadedCards[i])
                    }
                    
                    self.cardsLoadedIndex = self.cardsLoadedIndex + 1
                }
                
            }
        }
    }

    func cardSwipedLeft(card: UIView) -> Void
    {
        if (loadedCards.count <= 0)
        {
            xButton.isHidden = true
            superLikeButton.isHidden = true
            checkButton.isHidden = true
            swipeTypeStr.isHidden = true
            
            allCards.removeAll()
            loadedCards.removeAll()
            getUsersArray.removeAllObjects()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPaginationData"), object: nil, userInfo: nil)
            
            return
        }
        else
        {
           loadedCards.remove(at: 0)
        }

        if cardsLoadedIndex < allCards.count
        {
            loadedCards.append(allCards[cardsLoadedIndex])

            self.userAtIndex = self.userAtIndex + 1
            cardsLoadedIndex = cardsLoadedIndex + 1
            self.insertSubview(loadedCards[MAX_BUFFER_SIZE - 1], belowSubview: loadedCards[MAX_BUFFER_SIZE - 2])
            
            let profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "id") as! String
            
            print("111111")
            self.swipeUser(otherUserID: profileOwnerID, swipeType: "2", completion: { (result) in
                //print("result = \(result)")
            })
        }
        else
        {
            let profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "id") as! String
            
            self.swipeUser(otherUserID: profileOwnerID, swipeType: "2", completion: { (result) in
                //print("result = \(result)")
            })
            
            if loadedCards.count <= 0
            {
                xButton.isHidden = true
                superLikeButton.isHidden = true
                checkButton.isHidden = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPaginationData"), object: nil, userInfo: nil)
            }
            else
            {
                let lastElement = self.getUsersArray.lastObject
                if ((lastElement) != nil)
                {
                    if (cardsLoadedIndex == (self.getUsersArray.count - 2))
                    {
                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPaginationData"), object: nil, userInfo: nil)
                    }
                   
                }
                
                self.userAtIndex = self.userAtIndex + 1
                cardsLoadedIndex = cardsLoadedIndex + 1
            }
        }
    }
    
    func cardSwipedRight(card: UIView) -> Void
    {
        if (loadedCards.count <= 0)
        {
            xButton.isHidden = true
            superLikeButton.isHidden = true
            checkButton.isHidden = true
            swipeTypeStr.isHidden = true
            
            allCards.removeAll()
            loadedCards.removeAll()
            getUsersArray.removeAllObjects()
            
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlert"), object: nil, userInfo: nil)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPaginationData"), object: nil, userInfo: nil)
            
            return
        }
        else
        {
            loadedCards.remove(at: 0)
        }
        
        if cardsLoadedIndex < allCards.count
        {
            loadedCards.append(allCards[cardsLoadedIndex])
            
            if typeOfButton == "RightSwipe"
            {
                typeOfButton = "RightSwipe"
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.getTypeOfMatch = "RightSwipe"
                
                let profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "id") as! String
                
                self.swipeUser(otherUserID: profileOwnerID, swipeType: "1", completion: { (result) in
                    
                    
                })
//                self.likeProfilePicture()
            }
            else if typeOfButton == "SuperLikeForVibes"
            {
                typeOfButton = "SuperLikeForVibes"
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.getTypeOfMatch = "SuperLikeForVibes"
                
                let profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "id") as! String
                
                self.swipeUser(otherUserID: profileOwnerID, swipeType: "3", completion: { (result) in
                    //print("result = \(result)")
                })
                
//                self.superLikeProfilePicture()
            }
            else
            {
                typeOfButton = "RightSwipe"
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.getTypeOfMatch = "RightSwipe"
                
                let profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "id") as! String
                
                self.swipeUser(otherUserID: profileOwnerID, swipeType: "1", completion: { (result) in
                    //print("result = \(result)")
                })
            }
            
            self.userAtIndex = self.userAtIndex + 1
            cardsLoadedIndex = cardsLoadedIndex + 1
            self.insertSubview(loadedCards[MAX_BUFFER_SIZE - 1], belowSubview: loadedCards[MAX_BUFFER_SIZE - 2])
        }
        else
        {
            if typeOfButton == "RightSwipe"
            {
                typeOfButton = "RightSwipe"
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.getTypeOfMatch = "RightSwipe"
                
                let profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "id") as! String
                
                self.swipeUser(otherUserID: profileOwnerID, swipeType: "1", completion: { (result) in
                    //print("result = \(result)")
                    
                })
//                 self.likeProfilePicture()
            }
            else if typeOfButton == "SuperLikeForVibes"
            {
                //print("superLikeForVibes button clicked")
                typeOfButton = "SuperLikeForVibes"
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.getTypeOfMatch = "SuperLikeForVibes"
                
                let profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "id") as! String
                
                self.swipeUser(otherUserID: profileOwnerID, swipeType: "3", completion: { (result) in
                    //print("result = \(result)")
                })
                
//                self.superLikeProfilePicture()
            }
            else
            {
                self.typeOfButton = "RightSwipe"
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.getTypeOfMatch = "RightSwipe"
                
                let profileOwnerID = ((self.getUsersArray[self.userAtIndex]) as AnyObject).value(forKey: "id") as! String
                
                self.swipeUser(otherUserID: profileOwnerID, swipeType: "1", completion: { (result) in
                    //print("result = \(result)")
                })
//                self.likeProfilePicture()
            }
                        
            self.userAtIndex = self.userAtIndex + 1
            
            let lastElement = self.getUsersArray.lastObject
            if ((lastElement) != nil)
            {
                print("cardsLoadedIndex = \(cardsLoadedIndex)")
                print("self.getUsersArray.count - 2 = \(self.getUsersArray.count - 2)")
                if (cardsLoadedIndex == (self.getUsersArray.count - 2))
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPaginationData"), object: nil, userInfo: nil)
                }
            }
            
            if loadedCards.count <= 0
            {
                xButton.isHidden = true
                superLikeButton.isHidden = true
                checkButton.isHidden = true
                swipeTypeStr.isHidden = true
               
                allCards.removeAll()
                loadedCards.removeAll()
                getUsersArray.removeAllObjects()

//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlert"), object: nil, userInfo: nil)
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPaginationData"), object: nil, userInfo: nil)
             }
        }
    }
    
    
    func swipeUser(otherUserID: String, swipeType: String, completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            print("22222")

            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                print("33333")

                ApiHandler.swipeUserForMatch(userID: userID, authToken: userToken, other_user_id: otherUserID, swipe_status: swipeType, completion: { (responseData) in
                    
                    print("responseData = \(String(describing: responseData.value(forKey: "status")))")
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        print("dataDict = \(String(describing: dataDict))")
                        
                        if (dataDict != nil)
                        {
                            let match_text = dataDict?.value(forKey: "match_text") as? String
                            
                            if (match_text == "new_match")
                            {
                                if (dataDict != nil)
                                {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAlertForRelVibes"), object: nil, userInfo:  ["Status": dataDict as! NSDictionary])
                               }
                          }
                      }
                        
                        completion(true)
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
                        //print("message = \(String(describing: message))")
                       
                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
                        completion(false)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
                        completion(false)
                    }
                })
            }
        }
    }
        
    
    
    func superLikeForVibes() -> Void
    {
        typeOfButton = "SuperLikeForVibes"
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.getTypeOfMatch = "SuperLikeForVibes"
        //print("superLikeForVibes btn clicked")
        
       let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
        case .online(.wwan) , .online(.wiFi):
            //print("Connected via WWAN")
            print("Connected via WiFi")
            
            if let checkPackageStatus = UserDefaults.standard.value(forKey: "checkPurchasedPackage") as?  String
            {
                if (checkPackageStatus == "packagePurchased")
                {
                    //print("checkPackageStatus = \(checkPackageStatus)")
                    
                    if self.loadedCards.count <= 0
                    {
                        return
                    }
                    
                    let dragView: DraggableView = self.loadedCards[0]
                    dragView.overlayView.setMode(mode: GGOverlayViewMode.GGOverlayViewModeRight)
                    UIView.animate(withDuration: 0.2, animations: {
                        () -> Void in
                        dragView.overlayView.alpha = 1
                    })
                    dragView.rightClickAction()
              
                }
                else
                {
                    self.relVibesStr = "relLikesNotPurchased"
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "relLikesNotPurchased"), object: nil, userInfo: nil)
                }
            }
            else
            {
                self.relVibesStr = "relLikesNotPurchased"
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "relLikesNotPurchased"), object: nil, userInfo: nil)
            }
      
        }
    }


    func swipeRight() -> Void
    {
        typeOfButton = "RightSwipe"
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.getTypeOfMatch = "RightSwipe"
        if loadedCards.count <= 0
        {
            return
        }
        
        let dragView: DraggableView = loadedCards[0]
        dragView.overlayView.setMode(mode: GGOverlayViewMode.GGOverlayViewModeRight)
        UIView.animate(withDuration: 0.2, animations: {
            () -> Void in
            dragView.overlayView.alpha = 1
        })
        dragView.rightClickAction()
    }

    func swipeLeft() -> Void
    {
        if loadedCards.count <= 0
        {
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.opponentNameForMatch = ""
        appDelegate.opponentIDForMatch = ""
        appDelegate.opponentImageForMatch = ""

        
        let dragView: DraggableView = loadedCards[0]
        dragView.overlayView.setMode(mode: GGOverlayViewMode.GGOverlayViewModeLeft)
        UIView.animate(withDuration: 0.2, animations: {
            () -> Void in
            dragView.overlayView.alpha = 1
        })
        dragView.leftClickAction()
    }
   

}
