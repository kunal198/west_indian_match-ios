//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import UIKit
import Firebase

class User: NSObject {
    
    //MARK: Properties
    let name: String
    let email: String
    let id: String
    let firebase_user_id: String
    var profilePic: UIImage
    var profilePicType: String
    var profilePicLink: String
    var userLocation: String
    var userAge: String
    
    
    //MARK: Methods
    class func registerUser(withName: String, email: String, password: String, profilePic: UIImage, completion: @escaping (Bool) -> Swift.Void) {
        FIRAuth.auth()?.createUser(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
                user?.sendEmailVerification(completion: nil)
                let storageRef = FIRStorage.storage().reference().child("usersProfilePics").child(user!.uid)
                let imageData = UIImageJPEGRepresentation(profilePic, 0.1)
                storageRef.put(imageData!, metadata: nil, completion: { (metadata, err) in
                    if err == nil {
                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["name": withName, "email": email, "profilePicLink": path!]
                        FIRDatabase.database().reference().child("Users").child((user?.uid)!).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                            if errr == nil {
                                let userInfo = ["email" : email, "password" : password]
                                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                                completion(true)
                            }
                        })
                    }
                })
            }
            else {
                completion(false)
            }
        })
    }
    
   class func loginUser(withEmail: String, password: String, completion: @escaping (Bool) -> Swift.Void) {
        FIRAuth.auth()?.signIn(withEmail: withEmail, password: password, completion: { (user, error) in
            if error == nil {
                let userInfo = ["email": withEmail, "password": password]
                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                completion(true)
            } else {
                completion(false)
            }
        })
    }
    
    class func logOutUser(completion: @escaping (Bool) -> Swift.Void) {
        do {
            try FIRAuth.auth()?.signOut()
            UserDefaults.standard.removeObject(forKey: "userInformation")
            completion(true)
        } catch _ {
            completion(false)
        }
    }
    
   class func info(forUserID: String, completion: @escaping (User) -> Swift.Void) {
    
    print("forUserID = \(forUserID)")
    var userID = ""
    if (forUserID.range(of: "@") != nil)
    {
        print("messageStr contains the word `@`")
        let fullUserID = forUserID.components(separatedBy: "@")
        let beforeAtTheRateStr: String = fullUserID[0]
        userID = beforeAtTheRateStr
    }
    else
    {
        print("messageStr doesnot contains the word `@`")
        userID = forUserID
    }
    
   
    FIRDatabase.database().reference().child("Users").child(userID).keepSynced(true)
    FIRDatabase.database().reference().child("Users").child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
        
        if snapshot.value is NSNull
        {
            let profilePic = UIImage(named : "Default Icon")
            let user = User.init(name: "", email: "", id: "", profilePic: profilePic!,profileType: "",profilePicLink: "",userLoc: "",userAge: "", firebase_user_id: "")
            
            completion(user)
        }
        else
        {
            print("snapshot.value = \(String(describing: snapshot.value))")
            
            let filterResultsDict = snapshot.value as! NSMutableDictionary
            let allKeys = filterResultsDict.allKeys as! [String]
            if allKeys.contains("UserName") ||  allKeys.contains("ProfilePicture")
            {
                let name = filterResultsDict.value(forKey: "UserName")
                let email = ""
                var profileImageType = String()
                var userLocation = String()
                var userAge = String()
                var firebase_user_id = String()
                var usersIDStr = String()
                
                if allKeys.contains("Location") && allKeys.contains("Age") && allKeys.contains("ProfilePicture") && allKeys.contains("UserID")
                {
                    if allKeys.contains("profileImageType")
                    {
                        profileImageType = filterResultsDict.value(forKey: "profileImageType") as! String
                    }
                    else{
                        profileImageType = "image"
                    }
                    
                    if allKeys.contains("Location")
                    {
                        userLocation = filterResultsDict.value(forKey: "Location") as! String
                    }
                    else{
                        userLocation = " "
                    }
                    
                    if allKeys.contains("Age")
                    {
                        userAge = filterResultsDict.value(forKey: "Age") as! String
                    }
                    else{
                        userAge = " "
                    }
                    
                    if allKeys.contains("UserID")
                    {
                        usersIDStr = filterResultsDict.value(forKey: "UserID") as! String
                    }
                    else{
                        usersIDStr = ""
                    }
                    
                    if allKeys.contains("firebase_user_id")
                    {
                        firebase_user_id = filterResultsDict.value(forKey: "firebase_user_id") as! String
                    }
                    else
                    {
                        firebase_user_id = usersIDStr
                    }
                    
                    if (firebase_user_id == "") && (usersIDStr == "")
                    {
                        firebase_user_id = userID
                    }
                    
                    getOpponentProfilePicURL(opponentID: usersIDStr, completion: { (picString) in
                        
                        print("picString = \(picString)")
                        
                        if (picString != "")
                        {
                            let link = URL.init(string: picString)
                            
                            if (link != nil)
                            {
                                URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                                    if error == nil
                                    {
                                        var profilePic = UIImage.init(data: data!)
                                        if profilePic == nil
                                        {
                                            profilePic = UIImage(named : "Default Icon")
                                        }
                                        
                                        let user = User.init(name: (name as? String)!, email: email, id: usersIDStr, profilePic: profilePic!,profileType: profileImageType,profilePicLink: (link?.absoluteString)!,userLoc: userLocation,userAge: userAge, firebase_user_id: firebase_user_id)
                                        
                                        completion(user)
                                    }
                                }).resume()
                            }
                        }
                        else
                        {
                            let link = URL.init(string: filterResultsDict.value(forKey: "ProfilePicture") as! String)
                            if (link != nil)
                            {
                                URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                                    if error == nil
                                    {
                                        var profilePic = UIImage.init(data: data!)
                                        if profilePic == nil
                                        {
                                            profilePic = UIImage(named : "Default Icon")
                                        }
                                        
                                        let user = User.init(name: (name as? String)!, email: email, id: usersIDStr, profilePic: profilePic!,profileType: profileImageType,profilePicLink: (link?.absoluteString)!,userLoc: userLocation,userAge: userAge, firebase_user_id: firebase_user_id)
                                        
                                        completion(user)
                                    }
                                }).resume()
                            }
                        }
                    })
                    
                   /* let link = URL.init(string: filterResultsDict.value(forKey: "ProfilePicture") as! String)
                    URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                        if error == nil
                        {
                            var profilePic = UIImage.init(data: data!)
                            if profilePic == nil
                            {
                                profilePic = UIImage(named : "Default Icon")
                            }
                            
                            let user = User.init(name: (name as? String)!, email: email, id: usersIDStr, profilePic: profilePic!,profileType: profileImageType,profilePicLink: (link?.absoluteString)!,userLoc: userLocation,userAge: userAge, firebase_user_id: firebase_user_id)
                            
                            completion(user)
                        }
                    }).resume()*/
                    
                    
                }
                else
                {
                    let profilePic = UIImage(named : "Default Icon")
                    let user = User.init(name: "", email: "", id: "", profilePic: profilePic!,profileType: "",profilePicLink: "",userLoc: "",userAge: "", firebase_user_id: "")
                    
                    completion(user)
                }
                
            }
            else
            {
                let profilePic = UIImage(named : "Default Icon")
                let user = User.init(name: "", email: "", id: "", profilePic: profilePic!,profileType: "",profilePicLink: "",userLoc: "",userAge: "", firebase_user_id: "")
                
                completion(user)
            }
        }
        
      })
    }
    
    class func getOpponentProfilePicURL(opponentID: String ,completion: @escaping (String) -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            completion("")
            
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    ApiHandler.getStatusOfBlockedUsersForMessages(userID: userID, authToken: userToken, other_user_id: opponentID, completion: { (responseData) in
                        
                        print("responseData = \(responseData)")
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                            
                            print("dataDict = \(String(describing: dataDict))")
                            var opponentProfilePic = String()
                            opponentProfilePic = dataDict?.value(forKey: "profile_pic_thumb") as! String
                            print("opponentProfilePic = \(opponentProfilePic)")
                          
                            completion(opponentProfilePic)
                            
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            print("message = \(String(describing: message))")
                            completion("")
                        }
                        else
                        {
                            completion("")
                        }
                    })
                }
                else
                {
                    completion("")
                }
            }
            else
            {
                completion("")
            }
        }
    }
    
    class func downloadAllUsers(exceptID: String, completion: @escaping (User) -> Swift.Void) {
        FIRDatabase.database().reference().child("Users").observe(.childAdded, with: { (snapshot) in
            let id = snapshot.key
            let data = snapshot.value as! [String: Any]
            let credentials = data["credentials"] as! [String: String]
            if id != exceptID {
                let name = credentials["name"]!
                let email = credentials["email"]!
                let link = URL.init(string: credentials["profilePicLink"]!)
                let profileImageType = credentials["profileImageType"]!
                URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                    if error == nil {
                        let profilePic = UIImage.init(data: data!)
                        let user = User.init(name: name, email: email, id: id, profilePic: profilePic!,profileType: profileImageType,profilePicLink: (link?.absoluteString)!,userLoc: "",userAge: "", firebase_user_id:  "")
                        completion(user)
                    }
                }).resume()
            }
        })
    }
    
    class func checkUserVerification(completion: @escaping (Bool) -> Swift.Void) {
        FIRAuth.auth()?.currentUser?.reload(completion: { (_) in
            let status = (FIRAuth.auth()?.currentUser?.isEmailVerified)!
            completion(status)
        })
    }

    
    //MARK: Inits
    init(name: String, email: String, id: String, profilePic: UIImage, profileType: String,profilePicLink: String,userLoc: String,userAge: String,firebase_user_id: String) {
        self.name = name
        self.email = email
        self.id = id
        self.profilePic = profilePic
        self.profilePicType = profileType
        self.profilePicLink = profilePicLink
        self.userLocation = userLoc
        self.userAge = userAge
        self.firebase_user_id = firebase_user_id
    }
}

