//
//  ViewOtherUsersProfileViewController.swift
//  Wi Match
//
//  Created by brst on 17/04/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
//import Firebase
import  MediaPlayer
import MobileCoreServices
import SDWebImage
import CoreLocation

extension Double
{
    func truncate(places : Int)-> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}

class ViewOtherUsersProfileViewController: UIViewController,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,CLLocationManagerDelegate  {
    
    // MARK: - ***** VARIABLES DECLARATION *****
    var cameFromMatch = String()
    var videosLikedByArray = NSMutableArray()
    var videosSuperLikedByArray = NSMutableArray()
    var picturesSuperLikedByArray = NSMutableArray()
    var picturesLikedByArray = NSMutableArray()
    var currentUserGender = String()
    var relLikesStatus = String()
    var photosUploadedArray = NSDictionary()
    var profileImagesArray = [String]()
    var interestsArray = [String]()
    var usersDetailsDict = NSDictionary()
    var photoArray = [String]()
    var imagebase64Str = String()
    var deleteInterestIndex = Int()
    var useCameraStr = String()
    var videoURL = NSURL()
    var videoController = MPMoviePlayerController()
    var videoArray = [String]()
    var videoUploadbool = Bool()
    var timerForSlider = Timer()
    var thumbNailArray = [UIImage]()
    var captionPhotoArray = [String]()
    var captionVideoArray = [String]()
    var dict1 = NSDictionary()
    var selectImageBool = Bool()
    var getPhotoKeyArray = [String]()
    var photoDeleteIndex = String()
    var newImageView = UIImageView()
    let playerViewController = AVPlayerViewController()
    var player:AVPlayer?
    var interestVal = Int()
    var isLoadMore = Bool()
    var showInterestsArray = [String]()
    var getVideoURLArray = [String]()
    var deleteVideoURL = String()
    var totalInterestsSaved = NSMutableArray()
    var timestampArray = [String]()
    var timestampVideoArray = [String]()
    var lastPhotoUrl = String()
    var lastVideoUrl = UIImage()
    var videoUrlAtIndex = String()
    var currentUserID = String()
    var opponentUserDeviceId = String()
    var opponentProfileVisibilityStr = String()
    var profileVisitorsArray = NSMutableArray()
    var likesCountArray = [String]()
    var superLikesCountArray = [String]()
    var videoLikedBool = Bool()
    var profileVisitorsCount = String()
    var albumArray = NSMutableArray()
    var ownerLatitude = Double()
    var ownerLongitude = Double()
    var myLatitude = Double()
    var myLongitude = Double()
    var badgeCount = Int()
    var getUserStatus = String()
    var profileImageType = String()
    var opponentCurrentLocation = String()
    var getUpdatedInterestsArray = NSMutableArray()
    
    var currentLatitude = Double()
    var currentLongitude = Double()
    var locationManager: CLLocationManager!
    
    private var lastContentOffset: CGFloat = 0
    var tappedBtnStr = String()
    
    var reportedUsersByMeArray = NSMutableArray()
    
    var isKindOfScreen = String()
    
    var checkMediaLikedByMe = String()
    var listingArray = NSArray()
    var ownerID = String()
    var ownerAuthToken = String()
    
    // MARK: - ***** OUTLETS DECLARATION *****
    
    @IBOutlet weak var backBtnLbl: UIButton!
    @IBOutlet weak var aboutPhotosVideosView: UIView!
    @IBOutlet weak var photoCloseBtn: UIButton!
    @IBOutlet weak var wrapperScrollPhoto: UIScrollView!
    @IBOutlet weak var videoClosebtn: UIButton!
    @IBOutlet weak var wrapperScrollVideo: UIScrollView!
    
    @IBOutlet weak var messageUserBtn: UIButton!
    @IBOutlet var userVideoLikeImage: UIImageView!
    @IBOutlet var userAlbumLikeImage: UIImageView!
    @IBOutlet weak var imageSuperLikeCount_Lbl: UILabel!
    @IBOutlet weak var imageLikeCount_lbl: UILabel!
    @IBOutlet weak var usersOnline_lbl: UIImageView!
    @IBOutlet weak var loadMoreInterestsBtn: UIButton!
    @IBOutlet weak var displayVideoTime_lbl: UILabel!
    @IBOutlet weak var displayLikeView: UIView!
    @IBOutlet weak var displayTime_lbl: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var usersImageView: UIImageView!
    @IBOutlet weak var imagesScrollView: UIScrollView!
    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var captionVideo_txtView: UITextView!
    @IBOutlet weak var viewVideoView: UIView!
    @IBOutlet weak var captionPhoto_txtView: UITextView!
    @IBOutlet weak var viewParcticularImage: UIImageView!
    @IBOutlet weak var ViewFullSize_ImageView: UIView!
    @IBOutlet weak var videos_tableView: UITableView!
    @IBOutlet weak var PhotosView: UIView!
    @IBOutlet weak var usersLocationLbl: UILabel!
    @IBOutlet weak var usersName_lbl: UILabel!
    @IBOutlet weak var usersProfileImage: UIImageView!
    @IBOutlet weak var noDataFoundLbl: UILabel!
    @IBOutlet weak var aboutMe_scrollView: UIScrollView!
    @IBOutlet weak var aboutMeView: UIView!
    @IBOutlet weak var gender_lbl: UILabel!
    @IBOutlet weak var country_lbl: UILabel!
    @IBOutlet weak var interests_txtView: UITextView!
    @IBOutlet weak var education_lbl: UILabel!
    @IBOutlet weak var aboutMe_textView: UITextView!
    @IBOutlet weak var drinking_lbl: UILabel!
    @IBOutlet weak var smoking_lbl: UILabel!
    @IBOutlet weak var hairColor_lbl: UILabel!
    @IBOutlet weak var bodyType_lbl: UILabel!
    @IBOutlet weak var interestedIn_lbl: UILabel!
    @IBOutlet weak var meetUp_lbl: UILabel!
    @IBOutlet weak var relationship_lbl: UILabel!
    @IBOutlet weak var videosView: UIView!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    @IBOutlet weak var postsBtnATop: UIButton!
    
    @IBOutlet weak var dropDownOptionsBtn: UIButton!
    @IBOutlet weak var albumBtnTop: UIButton!
    @IBOutlet weak var aboutMeTop: UIButton!
    
    @IBOutlet weak var postsBtn: UIButton!
    @IBOutlet weak var albumBnt: UIButton!
    @IBOutlet weak var aboutMeBtn: UIButton!
    @IBOutlet weak var videoSuperLikeCount_lbl: UILabel!
    @IBOutlet weak var videoLikeCount_lbl: UILabel!
    @IBOutlet weak var wrapper_ScrollView: UIScrollView!
    @IBOutlet weak var milesAway_Lbl: UILabel!
    @IBOutlet weak var aboutMe_lbl: UILabel!
    @IBOutlet weak var showCurrentPostedImage: UIImageView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var loadingImage: UIImageView!
    
    @IBOutlet weak var fullImageViewBtn: UIButton!
    
    @IBOutlet weak var topViewButtons: UIView!
    @IBOutlet weak var wrapperViewBtns: UIView!
    
    
    // MARK: - ***** ViewDidLoad. *****
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageControl.isHidden = true
        self.pageControl.numberOfPages = 0
        self.usersOnline_lbl.isHidden = true
        self.albumArray = []
        self.noDataFoundLbl.isHidden = true
        self.loadingView.isHidden = true
        self.loadingView.layer.cornerRadius = 15
        self.loadingView.layer.masksToBounds = false
        
        self.messageUserBtn.titleLabel?.textAlignment = NSTextAlignment.left
        self.messageUserBtn.contentHorizontalAlignment = .left

        self.videoLikedBool = false
        NotificationCenter.default.removeObserver(self)
        
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        var panRecognizer: UIPanGestureRecognizer?
        panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.wasDraggedBtn))
        // cancel touches so that touchUpInside touches are ignored
        panRecognizer?.cancelsTouchesInView = true
        self.chatBtn.addGestureRecognizer(panRecognizer!)
        self.chatBtn.addTarget(self, action:#selector(Chat_tbnAction(_:with:)), for:.touchUpInside)
        interestVal = 0
        videoUploadbool = false
        isLoadMore = false
        selectImageBool = false
        useCameraStr = ""
        self.aboutMeView.isHidden = true
        self.aboutMe_scrollView.isHidden = false
        self.PhotosView.isHidden = true
        self.videosView.isHidden = true
        self.noDataFoundLbl.isHidden = true
        self.photosCollectionView.isHidden = true
        self.tappedBtnStr = "aboutBtn"
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeBtn.setTitleColor(UIColor.black, for: .normal)
        
        self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeTop.setTitleColor(UIColor.black, for: .normal)
        self.postsBtnATop.setTitleColor(UIColor.gray, for: .normal)
        
        chatBtn.layer.cornerRadius = chatBtn.frame.size.height/2
        chatBtn.layer.shadowColor = UIColor.black.cgColor
        chatBtn.layer.shadowOpacity = 0.8;
        chatBtn.layer.shadowRadius = 12;
        chatBtn.layer.shadowOffset = CGSize.init(width: 4.0, height: 4.0);
    }
    
    //MARK:- ****** didChangeAuthorization ******
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
            
        case .notDetermined , .denied , .restricted :
            
            if let getAllowStr = UserDefaults.standard.value(forKey: "not_Now") as? String
            {
                if (getAllowStr != "notNowClickedOnce") //|| (getAllowStr != "alreadyAuthorized")
                {
                    let alert = UIAlertController(title: "", message: "Please allow location services from your iPhone settings to find nearby Caribbean people around you in the app.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                        UIApplication.shared.openURL(settingsURL!)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Not Now", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                        
                        UserDefaults.standard.set("notNowClickedOnce", forKey: "not_Now")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                UserDefaults.standard.set("notNowNotClickedOnce", forKey: "not_Now")
            }
            
            break
            
        case .authorizedWhenInUse:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
        case .authorizedAlways:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
            break
            
       /* default:
            
            UserDefaults.standard.set("alreadyAuthorized", forKey: "not_Now")
            self.locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()*/
        }
    }
    
    
    
    // MARK: - ****** Fetch Users Current Location. ******
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.locationManager.location != nil
        {
            if let getCoordinates = manager.location?.coordinate
            {
                print("getCoordinates = \(getCoordinates)")
                
                let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.locationManager.stopUpdatingLocation()
                
                if locValue.latitude != 0.0 && locValue.longitude != 0.0 {
                    let loca = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    CLGeocoder().reverseGeocodeLocation(loca) { (placemarks, error) in
                        // this is the last line that is being called
                        var placemark : CLPlacemark!
                        placemark = placemarks?[0]
                        if placemark != nil
                        {
                            
                        }
                    }
                }
            }
        }
    }
    
    func viewUserDetails(completion: @escaping (Bool) -> Swift.Void)
    {
        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
            {
                self.loadingView.isHidden = false
                self.view.isUserInteractionEnabled = false
                self.backBtnLbl.isUserInteractionEnabled = false
                
                let date = NSDate();
                /// GET TIMESTAMP IN UNIX
                let currentTimeStamp = Int64(date.timeIntervalSince1970)
//                let data = "\(userID) + \(userToken) + \(self.ownerID) + \(String(currentTimeStamp))"
                
                ApiHandler.getUserProfileDetails(forUserID: userID , authToken: userToken, other_user_id: self.ownerID,timestamp: Int(currentTimeStamp), completion: { (responseData) in
                    
                    if (responseData.value(forKey: "status") as? String == "200")
                    {
//                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                        
                        self.usersDetailsDict = responseData.value(forKey: "data") as! NSDictionary
                    
                        var userGenderTypeStr  = String()
                        userGenderTypeStr = self.usersDetailsDict.value(forKey: "gender") as! String
                        
                        if (userGenderTypeStr == "male") || (userGenderTypeStr == "Male")  || (userGenderTypeStr == "1")
                        {
                            self.gender_lbl.text = "Male"
                        }
                        else
                        {
                            self.gender_lbl.text = "Female"
                        }
                        
                        if (self.ownerAuthToken == "")
                        {
                            self.ownerAuthToken = (self.usersDetailsDict.value(forKey: "auth_token") as? String)!
                        }
                        
                        let userName = self.usersDetailsDict.value(forKey: "username") as? String
                        let userAge = self.usersDetailsDict.value(forKey: "age") as? String
                        self.usersName_lbl.text = userName! + ", " +  userAge!
                        
                        let keysArray = self.usersDetailsDict.allKeys as! [String]
                        if (self.usersDetailsDict["online_status"] != nil)
                        {
                            let usersStatus = self.usersDetailsDict.value(forKey: "online_status") as! String
                            if usersStatus == "1"
                            {
                                self.usersOnline_lbl.isHidden = false
                                self.usersOnline_lbl.image = UIImage(named: "Online Icon")
                            }
                            else
                            {
                                self.usersOnline_lbl.isHidden = true
                                self.usersOnline_lbl.image = UIImage(named: "Offline Icon")
                            }
                        }
                        else{
                            self.usersOnline_lbl.isHidden = true
                            self.usersOnline_lbl.image = UIImage(named: "Offline Icon")
                        }
                        
                        if keysArray.contains("about")
                        {
                            var descriptionStr = String()
                            descriptionStr = (self.usersDetailsDict.value(forKey: "about") as? String)!
                            if descriptionStr.count > 0
                            {
                                self.aboutMe_textView.text = self.usersDetailsDict.value(forKey: "about") as? String
                                self.aboutMe_lbl.isHidden = true
                            }
                            else{
                                self.aboutMe_textView.text = ""
                                self.aboutMe_lbl.isHidden = false
                            }
                        }else{
                            self.aboutMe_textView.text = ""
                            self.aboutMe_lbl.isHidden = false
                        }
                        
                        let location_sharing = self.usersDetailsDict.value(forKey: "location_sharing") as! String
                        print("location_sharing = \(String(describing: location_sharing))")
                        
                        if keysArray.contains("location") && (location_sharing == "on")
                        {
                            let locStr = self.usersDetailsDict.value(forKey: "location") as? String
                            
                            if (locStr == "")
                            {
                                self.milesAway_Lbl.frame.origin.y = self.usersLocationLbl.frame.origin.y
                            }
                            
                            self.usersLocationLbl.text = self.usersDetailsDict.value(forKey: "location") as? String
                        }
                        else{
                            self.usersLocationLbl.text = ""
                            self.milesAway_Lbl.frame.origin.y = self.usersLocationLbl.frame.origin.y
                        }
                        
                        if let countryStr = self.usersDetailsDict.value(forKey: "country") as? NSDictionary
                        {
                            if let country_name = countryStr.value(forKey: "country_name") as? String
                            {
                                self.country_lbl.text  = country_name
                            }
                            else{
                                self.country_lbl.text = ""
                            }
                        }
                        else{
                            self.country_lbl.text = ""
                        }
                        

                        if keysArray.contains("profileImageType")
                        {
                            self.profileImageType = (self.usersDetailsDict.value(forKey: "profileImageType") as? String)!
                        }
                        else{
                            self.profileImageType = "image"
                        }
                        
                        if let bodytype = self.usersDetailsDict.value(forKey: "bodytype") as? NSDictionary
                        {
                            if let bodytypeName = bodytype.value(forKey: "name") as? String
                            {
                                self.bodyType_lbl.text = bodytypeName
                            }
                        }
                        
                        if let drinkingtype = self.usersDetailsDict.value(forKey: "drinking") as? NSDictionary
                        {
                            if let drinkingName = drinkingtype.value(forKey: "name") as? String
                            {
                                self.drinking_lbl.text = drinkingName
                            }
                        }
                        
                        if let educationtype = self.usersDetailsDict.value(forKey: "education") as? NSDictionary
                        {
                            if let educationName = educationtype.value(forKey: "name") as? String
                            {
                                self.education_lbl.text = educationName
                            }
                        }
                        
                        if let haircolortype = self.usersDetailsDict.value(forKey: "haircolor") as? NSDictionary
                        {
                            if let haircolorName = haircolortype.value(forKey: "name") as? String
                            {
                                self.hairColor_lbl.text = haircolorName
                            }
                        }
                        
                        if let interestesintype = self.usersDetailsDict.value(forKey: "interestesin") as? NSDictionary
                        {
                            if let interestesinName = interestesintype.value(forKey: "name") as? String
                            {
                                self.interestedIn_lbl.text = interestesinName
                            }
                        }
                        
                        
                        if let meetuppreferences = self.usersDetailsDict.value(forKey: "meetuppreferences") as? NSDictionary
                        {
                            if let meetuppreferencesName = meetuppreferences.value(forKey: "name") as? String
                            {
                                self.meetUp_lbl.text = meetuppreferencesName
                            }
                        }
                        
                        if let relationshipstatus = self.usersDetailsDict.value(forKey: "relationshipstatus") as? NSDictionary
                        {
                            if let relationshipstatusName = relationshipstatus.value(forKey: "name") as? String
                            {
                                self.relationship_lbl.text = relationshipstatusName
                            }
                        }
                        
                        if let smoking = self.usersDetailsDict.value(forKey: "smoking") as? NSDictionary
                        {
                            if let smokingName = smoking.value(forKey: "name") as? String
                            {
                                self.smoking_lbl.text = smokingName
                            }
                        }
                        
                        let profileURL = (self.usersDetailsDict.value(forKey: "profile_pic") as? String)!
                        DispatchQueue.main.async {
                            self.usersProfileImage.sd_setImage(with: URL(string: profileURL), placeholderImage: UIImage(named: ""))
                        }
                        
                        if keysArray.contains("myDeviceToken")
                        {
                            self.opponentUserDeviceId = (self.usersDetailsDict.value(forKey: "myDeviceToken") as? String)!
                        }
                        else
                        {
                            self.opponentUserDeviceId = ""
                        }
                        
                        if keysArray.contains("longitude")
                        {
                            self.ownerLongitude = Double(self.usersDetailsDict.value(forKey: "longitude") as! String)!
                        }
                        else{
                            self.ownerLongitude = 0.0
                        }
                        if keysArray.contains("latitude")
                        {
                            self.ownerLatitude = Double(self.usersDetailsDict.value(forKey: "latitude") as! String)!
                        }
                        else
                        {
                            self.ownerLatitude = 0.0
                        }
                        
                        let ownersState = self.usersDetailsDict.value(forKey: "state") as? String
                        if let myState = UserDefaults.standard.value(forKey: "state") as? String
                        {
                            if (myState != ownersState)
                            {
                                if (ownersState == "")
                                {
                                    self.milesAway_Lbl.text = self.country_lbl.text
                                    self.milesAway_Lbl.textColor = UIColor.white
                                }
                                else
                                {
                                    self.milesAway_Lbl.text = ownersState//self.country_lbl.text
                                    self.milesAway_Lbl.textColor = UIColor.white
                                }
                            }
                            else
                            {
                                self.milesAway_Lbl.isHidden = false
                                
                                let ownerCoordinate = CLLocation(latitude: self.ownerLatitude, longitude: self.ownerLongitude)
                                let myCoordinate = CLLocation(latitude: self.currentLatitude, longitude: self.currentLongitude)
                                
                                let distanceInMeters = ownerCoordinate.distance(from: myCoordinate) // result is in meters
                   
                                let distanceInMiles = distanceInMeters/1609.344
                                print("distanceInMiles = \(distanceInMiles)")
                                
                                let milesDistance = String(distanceInMiles.truncate(places: 2))
                                
                                let numberComponent = milesDistance.components(separatedBy :".")
                                
                                if (numberComponent.count > 0)
                                {
                                    let integerNumber = numberComponent [0]
                                    let fractionalNumber = numberComponent [1]
                                    
                                    print("integerNumber = \(integerNumber)")
                                    print("fractionalNumber = \(fractionalNumber)")
                                    
                                    self.milesAway_Lbl.text = integerNumber + " miles away"
                                }
                                else{
                                    self.milesAway_Lbl.text = String(distanceInMiles.truncate(places: 2)) + " miles away"
                                }
                               
                                self.milesAway_Lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                            }
                        }
                        
                        
                        if let userfooddrinks = self.usersDetailsDict.value(forKey: "userfooddrinks") as? NSArray
                        {
                            for index in 0..<userfooddrinks.count
                            {
                                var userfooddrinksDict = NSDictionary()
                                userfooddrinksDict = userfooddrinks[index] as! NSDictionary
                                
                                if (userfooddrinksDict["fooddrinks"] != nil)
                                {
                                    if let fooddrinksDict = userfooddrinksDict.value(forKey: "fooddrinks") as? NSDictionary
                                    {
                                        if (fooddrinksDict["name"] != nil)
                                        {
                                            let name = fooddrinksDict.value(forKey: "name") as! String
                                            
                                            if !(self.getUpdatedInterestsArray.contains(name))
                                            {
                                                self.getUpdatedInterestsArray.add(name)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if let userhobbies = self.usersDetailsDict.value(forKey: "userhobbies") as? NSArray
                        {
                            for index in 0..<userhobbies.count
                            {
                                var userhobbiesDict = NSDictionary()
                                userhobbiesDict = userhobbies[index] as! NSDictionary
                                
                                if (userhobbiesDict["hobbies"] != nil)
                                {
                                    if let hobbiesDict = userhobbiesDict.value(forKey: "hobbies") as? NSDictionary
                                    {
                                        if (hobbiesDict["name"] != nil)
                                        {
                                            let name = hobbiesDict.value(forKey: "name") as! String
                                            
                                            if !(self.getUpdatedInterestsArray.contains(name))
                                            {
                                                self.getUpdatedInterestsArray.add(name)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if let usermusic = self.usersDetailsDict.value(forKey: "usermusic") as? NSArray
                        {
                            for index in 0..<usermusic.count
                            {
                                var usermusicDict = NSDictionary()
                                usermusicDict = usermusic[index] as! NSDictionary
                                if (usermusicDict["music"] != nil)
                                {
                                   if let musicDict = usermusicDict.value(forKey: "music") as? NSDictionary
                                   {
                                    if (musicDict["name"] != nil)
                                    {
                                        let name = musicDict.value(forKey: "name") as! String
                                        
                                        if !(self.getUpdatedInterestsArray.contains(name))
                                        {
                                            self.getUpdatedInterestsArray.add(name)
                                        }
                                    }
                                   }
                                }
                            }
                        }
                        
                        
                        if let userprofession = self.usersDetailsDict.value(forKey: "userprofession") as? NSArray
                        {
                            for index in 0..<userprofession.count
                            {
                                var userprofessionDict = NSDictionary()
                                userprofessionDict = userprofession[index] as! NSDictionary
                                
                                if (userprofessionDict["profession"] != nil)
                                {
                                    let professionDict = userprofessionDict.value(forKey: "profession") as? NSDictionary
                                    
                                    if (professionDict != nil)
                                    {
                                        let keyArray = professionDict?.allKeys as! [String]
                                        if (keyArray.contains("name"))
                                        {
                                            if (professionDict!["name"] != nil)
                                            {
                                                let name = professionDict?.value(forKey: "name") as! String
                                                
                                                if !(self.getUpdatedInterestsArray.contains(name))
                                                {
                                                    self.getUpdatedInterestsArray.add(name)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                        if let usersports = self.usersDetailsDict.value(forKey: "usersports") as? NSArray
                        {
                            for index in 0..<usersports.count
                            {
                                var usersportsDict = NSDictionary()
                                usersportsDict = usersports[index] as! NSDictionary
                                
                                if (usersportsDict["sports"] != nil)
                                {
                                    if let sportsDict = usersportsDict.value(forKey: "sports") as? NSDictionary
                                    {
                                        if (sportsDict["name"] != nil)
                                        {
                                            let name = sportsDict.value(forKey: "name") as! String
                                            
                                            if !(self.getUpdatedInterestsArray.contains(name))
                                            {
                                                self.getUpdatedInterestsArray.add(name)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if let religiousreviews = self.usersDetailsDict.value(forKey: "religiousreviews") as? String
                        {
                            if !(self.getUpdatedInterestsArray.contains(religiousreviews))
                            {
                                self.getUpdatedInterestsArray.add(religiousreviews)
                            }
                        }
                        
                        if let politicalreviews = self.usersDetailsDict.value(forKey: "politicalreviews") as? String
                        {
                            if !(self.getUpdatedInterestsArray.contains(politicalreviews))
                            {
                                self.getUpdatedInterestsArray.add(politicalreviews)
                            }
                        }
                        
                        for index in 0..<self.getUpdatedInterestsArray.count
                        {
                            if !(self.interestsArray.contains(self.getUpdatedInterestsArray[index] as! String))
                            {
                                self.interestsArray.append(self.getUpdatedInterestsArray[index] as! String)
                            }
                            
                            
                            if index <= 4
                            {
                                if !(self.showInterestsArray.contains(self.getUpdatedInterestsArray[index] as! String))
                                {
                                    self.showInterestsArray.append(self.getUpdatedInterestsArray[index] as! String)
                                }
                            }
                            else{
                                print("index greater than 4")
                            }
                        }
                        
                        self.interests_txtView.isHidden = false
                        
                        if self.isLoadMore == false{
                            for index in 0..<self.showInterestsArray.count
                            {
                                print("index = \(index)")
                                let string = self.showInterestsArray.joined(separator: ", ")
                                self.interests_txtView.text = string
                            }
                        }
                        else
                        {
                            for index in 0..<self.interestsArray.count
                            {
                                print("index = \(index)")
                                let string = self.interestsArray.joined(separator: ", ")
                                self.interests_txtView.text = string
                            }
                        }
               
                        completion(true)
                    }
                    else if (responseData.value(forKey: "status") as? String == "400")
                    {
                        let message = responseData.value(forKey: "message") as? String
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        self.backBtnLbl.isUserInteractionEnabled = true
                        
                         let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                            action in
                            
                            if (message == "No user found")
                            {
                                self.navigationController!.popViewController(animated: true)
                            }
                           
                            
                        }))
                       
                        self.present(alert, animated: true, completion: nil)
                        completion(true)
                    }
                    else
                    {
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
//                        let alert = UIAlertController(title: "Alert!", message: "\(responseData) + \(data)", preferredStyle: UIAlertControllerStyle.alert)
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. please try again later", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        completion(true)
                    }
                })
            }
        }
    }
    
    func getUploadedMediaFiles()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
          
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    if self.ownerID != ""
                    {
                        if self.ownerAuthToken != ""
                        {
                            ApiHandler.getProfileAlbum(userID: userID, authToken: userToken, page: "1", other_user_id: self.ownerID, completion: { (responseData) in
                                
                                self.getPhotoKeyArray = []
                                self.profileImagesArray = []
                                self.albumArray = []
                                self.videoArray = []
                                self.listingArray = []
                                
                                if (responseData.value(forKey: "status") as? String == "200")
                                {
                                    let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                    self.listingArray = dataDict?.value(forKey: "list") as! NSArray
                                    
                                    if (self.listingArray.count <= 0)
                                    {
                                        self.view.isUserInteractionEnabled = true
                                        self.backBtnLbl.isUserInteractionEnabled = true
                                        self.activityIndicator.isHidden = true
                                        self.pageControl.isHidden = true
                                        self.usersImageView.image = UIImage(named: "Default Icon")
                                        self.usersImageView.contentMode = UIViewContentMode.scaleAspectFill
                                        self.getNoMediaAlbumUploaded()
                                    }
                                    
                                    for index in 0..<self.listingArray.count
                                    {
                                        var albumDict = NSDictionary()
                                        albumDict = self.listingArray[index] as! NSDictionary
                                        
                                        if !(self.albumArray.contains(albumDict))
                                        {
                                            self.albumArray.add(albumDict)
                                        }
                                        
                                        let keysArray = albumDict.allKeys as! [String]
                                        if keysArray.contains("type")
                                        {
                                            if (albumDict.value(forKey: "type") as? String == "1")
                                            {
                                                if !(self.profileImagesArray.contains(albumDict.value(forKey: "media") as! String))
                                                {
                                                    self.profileImagesArray.append(albumDict.value(forKey: "media") as! String)
                                                }
                                            }
                                            else
                                            {
                                                if !(self.videoArray.contains(albumDict.value(forKey: "media") as! String))
                                                {
                                                    self.videoArray.append(albumDict.value(forKey: "media") as! String)
                                                }
                                            }
                                        }
                                        
                                        
                                        if self.albumArray.count == 0 {
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            self.view.isUserInteractionEnabled = true
                                            self.noDataFoundLbl.isHidden = true
                                            self.backBtnLbl.isUserInteractionEnabled = true
                                            self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                                                self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                                            }, completion: { (finished) in
                                            })
                                        }
                                        else
                                        {
                                            self.noDataFoundLbl.isHidden = true
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            self.backBtnLbl.isUserInteractionEnabled = true
                                        }
                                        
                                        
                                        self.imagesScrollView.frame = CGRect(x:0, y: self.imagesScrollView.frame.origin.y, width:self.view.frame.width, height:self.imagesScrollView.frame.height)
                                        
                                        var widthMultipliier = CGFloat()
                                        var x_pos = CGFloat()
                                        x_pos=0;
                                        
                                        if self.profileImagesArray.count == 0{
                                            self.view.isUserInteractionEnabled = true
                                            self.backBtnLbl.isUserInteractionEnabled = true
                                            self.activityIndicator.isHidden = true
                                            self.pageControl.isHidden = true
                                            self.usersImageView.image = UIImage(named: "Default Icon")
                                            self.usersImageView.contentMode = UIViewContentMode.scaleAspectFill
                                        }
                                        
                                        for index in 0..<self.profileImagesArray.count
                                        {
                                            widthMultipliier = CGFloat(index)
                                            
                                            self.newImageView = UIImageView(frame:CGRect(x:x_pos, y:0,width:self.view.frame.size.width, height:self.imagesScrollView.frame.size.height))
                                            
                                            self.newImageView.backgroundColor = UIColor.clear
                                            self.usersImageView.isHidden = true
//                                            self.pageControl.isHidden = false
                                            self.pageControl.isHidden = true
                                            
                                            self.newImageView.sd_setImage(with:  URL(string: self.profileImagesArray[index]), placeholderImage: UIImage(named: "Default Icon"))
                                            
                                            self.newImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
                                            
                                            self.newImageView.contentMode = UIViewContentMode.scaleAspectFit
                                            
                                            self.newImageView.clipsToBounds = true
                                            self.imagesScrollView.addSubview(self.newImageView)
                                            
                                            x_pos = x_pos+self.imagesScrollView.frame.width;
                                            
                                            self.imagesScrollView.contentSize = CGSize(width:self.imagesScrollView.frame.width * widthMultipliier+self.imagesScrollView.frame.width, height:self.imagesScrollView.frame.height)
                                        }
                                    }
 
                                    self.photosCollectionView.isScrollEnabled = false
                                    var collectionViewHeight = Int()
                                    if self.albumArray.count % 2 == 0
                                    {
                                        collectionViewHeight = (self.albumArray.count/2)*150
                                    }
                                    else{
                                        collectionViewHeight = ((self.albumArray.count/2 ) +  1)*150
                                    }
                                    
                                    self.photosCollectionView.contentSize.height = CGFloat(collectionViewHeight)
                                    self.photosCollectionView.frame = CGRect(x: self.photosCollectionView.frame.origin.x,y: self.photosCollectionView.frame.origin.y, width: self.photosCollectionView.frame.size.width, height: self.photosCollectionView.contentSize.height + 10)
                                    
                                    let value = self.imagesScrollView.frame.size as NSValue
                                    let Data = NSKeyedArchiver.archivedData(withRootObject: value)
                                    UserDefaults.standard.set(Data, forKey: "self.imagesScrollView.frame.size")
                                    
                                    self.imagesScrollView.delegate = self
                                    self.pageControl.currentPage = 0
                                    self.pageControl.numberOfPages = self.profileImagesArray.count
                                    self.photosCollectionView.reloadData()
                                    
                                    if !self.timerForSlider.isValid
                                    {
                                        self.timerForSlider =  Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.moveToNextPage), userInfo: nil, repeats: true)
                                    }
                                    
                                    let when = DispatchTime.now() + 2
                                    DispatchQueue.main.asyncAfter(deadline: when) {
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        self.backBtnLbl.isUserInteractionEnabled = true
                                        self.activityIndicator.isHidden = true
                                    }
                                }
                                else if (responseData.value(forKey: "status") as? String == "400")
                                {
                                    let message = responseData.value(forKey: "message") as? String
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    self.activityIndicator.isHidden = true
                                    self.backBtnLbl.isUserInteractionEnabled = true
                                    let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    self.getNoMediaAlbumUploaded()
                                }
                                else
                                {
                                    self.view.isUserInteractionEnabled = true
                                    self.loadingView.isHidden = true
                                    self.activityIndicator.isHidden = true
                                    self.backBtnLbl.isUserInteractionEnabled = true
                                    let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    self.getNoMediaAlbumUploaded()
                                }
                            })
                        }else{
                            DispatchQueue.main.async {
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    // MARK: - ****** Method Called when no media record uploaded in database ******
    func getNoMediaAlbumUploaded()
    {
        self.loadingView.isHidden = true
        self.view.isUserInteractionEnabled = true
        self.activityIndicator.isHidden = true
        self.activityIndicator.isHidden = true
        self.backBtnLbl.isUserInteractionEnabled = true
        self.pageControl.isHidden = true
        self.usersImageView.image = UIImage(named: "Default Icon")
        self.noDataFoundLbl.isHidden = true
        self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { (finished) in
        })
        self.usersImageView.contentMode = UIViewContentMode.scaleAspectFill
        self.lastPhotoUrl = ""
        self.timestampArray = []
        self.getPhotoKeyArray = []
        self.profileImagesArray = []
        self.albumArray = []
        
        self.activityIndicator.isHidden = true
        self.pageControl.isHidden = true
        self.usersImageView.image = UIImage(named: "Default Icon")
        self.usersImageView.contentMode = UIViewContentMode.scaleAspectFill
        self.view.isUserInteractionEnabled = true
        
        let value = self.imagesScrollView.frame.size as NSValue
        let Data = NSKeyedArchiver.archivedData(withRootObject: value)
        UserDefaults.standard.set(Data, forKey: "self.imagesScrollView.frame.size")
        
        self.photosCollectionView.isScrollEnabled = false
        var collectionViewHeight = Int()
        if self.albumArray.count % 2 == 0
        {
            collectionViewHeight = (self.albumArray.count/2)*150
        }
        else
        {
            collectionViewHeight = ((self.albumArray.count/2 ) +  1)*150
        }
        
        self.photosCollectionView.contentSize.height = CGFloat(collectionViewHeight)
        self.photosCollectionView.frame = CGRect(x: self.photosCollectionView.frame.origin.x,y: self.photosCollectionView.frame.origin.y, width: self.photosCollectionView.frame.size.width, height: self.photosCollectionView.contentSize.height + 10)
    }
    
    
    public func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) // called on finger up as we are moving
    {
        print("scrollViewWillBeginDecelerating")
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("scrollViewDidEndDecelerating")
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if (scrollView == self.wrapper_ScrollView)
        {
            if (UIScreen.main.bounds.size.height == 736)
            {
//                if (scrollView.contentOffset.y >= 440) {
                if (scrollView.contentOffset.y >= 190) {
                    self.topViewButtons.isHidden = false
                    if (self.tappedBtnStr == "aboutBtn")
                    {
                        self.aboutMe_scrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 350
                    }
                    else if (self.tappedBtnStr == "albumBtn")
                    {
                        if (self.photosCollectionView.contentSize.height < 1000)
                        {
                            self.aboutMe_scrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                            self.wrapper_ScrollView.contentSize.height = self.aboutMe_scrollView.frame.origin.y + self.aboutMe_scrollView.contentSize.height + 50
                        }
                        else {
                            self.wrapper_ScrollView.contentSize.height =  self.photosCollectionView.frame.origin.y  + self.photosCollectionView.contentSize.height + 10
                        }
                    }
                    else if (self.tappedBtnStr == "postsBtn") {
                        
                    }
                }
                else
                {
                    self.topViewButtons.isHidden = true
                }
            }
            else if (UIScreen.main.bounds.size.height == 667)
            {
//                if (scrollView.contentOffset.y >= 400) {
                 if (scrollView.contentOffset.y >= 162) {
                    self.topViewButtons.isHidden = false
                    if (self.tappedBtnStr == "aboutBtn")
                    {
                        self.aboutMe_scrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 350
                    }
                    else if (self.tappedBtnStr == "albumBtn")
                    {
                        if (self.photosCollectionView.contentSize.height < 1000)
                        {
                            self.aboutMe_scrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                            self.wrapper_ScrollView.contentSize.height = self.aboutMe_scrollView.frame.origin.y + self.aboutMe_scrollView.contentSize.height + 50
                        }
                        else
                        {
                            self.wrapper_ScrollView.contentSize.height =  self.photosCollectionView.frame.origin.y  + self.photosCollectionView.contentSize.height + 10
                        }
                    }
                    else if (self.tappedBtnStr == "postsBtn")
                    {
                        
                    }
                }
                else
                {
                    self.topViewButtons.isHidden = true
                }
            }
            else if (UIScreen.main.bounds.size.height == 568)
            {
//                if (scrollView.contentOffset.y >= 340) {
                if (scrollView.contentOffset.y >= 137) {
                    self.topViewButtons.isHidden = false
                    if (self.tappedBtnStr == "aboutBtn")
                    {
                        self.aboutMe_scrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 350
                    }
                    else if (self.tappedBtnStr == "albumBtn")
                    {
                        if (self.photosCollectionView.contentSize.height < 1000)
                        {
                            self.aboutMe_scrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
                            self.wrapper_ScrollView.contentSize.height = self.aboutMe_scrollView.frame.origin.y + self.aboutMe_scrollView.contentSize.height + 50
                        }
                        else{
                            self.wrapper_ScrollView.contentSize.height =  self.photosCollectionView.frame.origin.y  + self.photosCollectionView.contentSize.height + 10
                        }
                    }
                    else if (self.tappedBtnStr == "postsBtn")
                    {
                        
                    }
                }
                else
                {
                    self.topViewButtons.isHidden = true
                }
            }
        }
        else if (scrollView == self.aboutMe_scrollView)
        {
            if (scrollView.contentOffset.y <= 0)
            {
                self.topViewButtons.isHidden = true
            }
        }
    }
    
    
  
    @IBAction func messageUserBtnAction(_ sender: Any) {
        
        if (self.usersDetailsDict != nil)
        {
            if (usersDetailsDict["firebase_user_id"] != nil) && (usersDetailsDict["id"] != nil)
            {
                let messagesScreen = storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
                messagesScreen.mainConfessionCheck = "comingFromViewProfile"
                messagesScreen.chatUserDifferentiateStr = "Messages"
                messagesScreen.viewOpponentProfile = "fromParticularUserScreen"
                let userID =  usersDetailsDict.value(forKey: "id") as? String
                let firebase_user_id =  usersDetailsDict.value(forKey: "firebase_user_id") as? String
                messagesScreen.opponentName = (usersDetailsDict.value(forKey: "username") as? String)!
                messagesScreen.opponentImage = usersDetailsDict.value(forKey: "profile_pic") as! String
                messagesScreen.opponentUserID = userID!
                messagesScreen.opponentFirebaseUserID = firebase_user_id!
//                messagesScreen.opponentProfilePic =  self.usersProfileImage.image!
                messagesScreen.typeOfUser = "WithProfileUser"
                messagesScreen.checkVisibiltyStr = "FromViewProfile"
                self.navigationController?.pushViewController(messagesScreen, animated: true)
            }
        }
    }
    
    @IBAction func dropDown_btnAction(_ sender: Any) {
        
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "", message: "Please select your option here", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            self.loadingView.isHidden = true
            self.view.isUserInteractionEnabled = true
        }
        actionSheetController.addAction(cancelActionButton)
        
        
        var revealActionButton = UIAlertAction()
        if  (self.reportedUsersByMeArray.contains(self.ownerID))
        {
            revealActionButton = UIAlertAction(title: "User Reported", style: .destructive) { action -> Void in
                
            }
        }
        else
        {
            revealActionButton = UIAlertAction(title: "Report User", style: .destructive) { action -> Void in
                
                let alert = UIAlertController(title: "", message: "Do you really want to report the user?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                    
                    let status = Reach().connectionStatus()
                    switch status
                    {
                    case .unknown, .offline:
                        let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case .online(.wwan) , .online(.wiFi):
                      
                        if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                        {
                            if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                            {
                                if self.ownerID != ""
                                {
                                    self.loadingView.isHidden = false
                                    self.view.isUserInteractionEnabled = false
                                    
                                    ApiHandler.reportContentOrUser(userID: userID, authToken: userToken, typeStr: "1", content_id: self.ownerID, completion: { (responseData) in
                                        
                                        if (responseData.value(forKey: "status") as? String == "200")
                                        {
//                                            let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                            
                                            self.loadingView.isHidden = true
                                            self.view.isUserInteractionEnabled = true
                                            
                                            let alert = UIAlertController(title: "", message: "User Reported.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                                action in
                                                
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userBlocked"), object: nil)
                                                self.navigationController!.popViewController(animated: true)
                                            }))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else if (responseData.value(forKey: "status") as? String == "400")
                                        {
                                            let message = responseData.value(forKey: "message") as? String
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            self.backBtnLbl.isUserInteractionEnabled = true
                                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.view.isUserInteractionEnabled = true
                                            self.loadingView.isHidden = true
                                            self.backBtnLbl.isUserInteractionEnabled = true
                                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    })
                                }
                            }
                        }
                        
                       
                        
                        
                        
//                        self.ReportUser(userID: (self.usersDetailsDict.value(forKey: "UserID") as? String)!, userName: (self.usersDetailsDict.value(forKey: "UserName") as? String)!, Age: (self.usersDetailsDict.value(forKey: "Age") as? String)!, location: (self.usersDetailsDict.value(forKey: "Location") as? String)!, ProfilePic: (self.usersDetailsDict.value(forKey: "ProfilePicture") as? String)!, Gender: (self.usersDetailsDict.value(forKey: "Gender") as? String)!)
                    }
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
        var blockActionButton = UIAlertAction()
        blockActionButton = UIAlertAction(title: "Block User", style: .destructive) { action -> Void in
      
            let alert = UIAlertController(title: "", message: "Do you really want to block?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                
                let status = Reach().connectionStatus()
                switch status
                {
                case .unknown, .offline:
                    let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .online(.wwan) , .online(.wiFi):

                    if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                    {
                        if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                        {
                            if self.ownerID != ""
                            {
                                self.loadingView.isHidden = false
                                self.view.isUserInteractionEnabled = false
                                
                                ApiHandler.blockUnblockUser(userID: userID, authToken: userToken, other_user_id: self.ownerID,typeOfBlockUser: "profile", completion: { (responseData) in
                                    
                                    if (responseData.value(forKey: "status") as? String == "200")
                                    {
//                                        let dataDict = responseData.value(forKey: "data") as? NSDictionary
                                        
                                        self.loadingView.isHidden = true
                                        self.view.isUserInteractionEnabled = true
                                        
                                        let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                                            action in

                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userBlocked"), object: nil)
                                            self.navigationController!.popViewController(animated: true)
                                            
                                        }))
                                        
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else if (responseData.value(forKey: "status") as? String == "400")
                                    {
                                        let message = responseData.value(forKey: "message") as? String
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        self.backBtnLbl.isUserInteractionEnabled = true
                                        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        self.view.isUserInteractionEnabled = true
                                        self.loadingView.isHidden = true
                                        self.backBtnLbl.isUserInteractionEnabled = true
                                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                })
                            }
                        }
                    }
                    
//                    self.loadingView.isHidden = false
//                    self.view.isUserInteractionEnabled = false
//
//                    self.blockUser(userID: (self.usersDetailsDict.value(forKey: "UserID") as? String)!, userName: (self.usersDetailsDict.value(forKey: "UserName") as? String)!, Age: (self.usersDetailsDict.value(forKey: "Age") as? String)!, location: (self.usersDetailsDict.value(forKey: "Location") as? String)!, ProfilePic: (self.usersDetailsDict.value(forKey: "ProfilePicture") as? String)!, Gender: (self.usersDetailsDict.value(forKey: "Gender") as? String)!)
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        actionSheetController.addAction(revealActionButton)
        actionSheetController.addAction(blockActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func userReport_BtnAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "Do you really want to report the user?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                print("Connected via WWAN")
                
//                self.loadingView.isHidden = false
//                self.view.isUserInteractionEnabled = false
//
//                self.ReportUser(userID: (self.usersDetailsDict.value(forKey: "UserID") as? String)!, userName: (self.usersDetailsDict.value(forKey: "UserName") as? String)!, Age: (self.usersDetailsDict.value(forKey: "Age") as? String)!, location: (self.usersDetailsDict.value(forKey: "Location") as? String)!, ProfilePic: (self.usersDetailsDict.value(forKey: "ProfilePicture") as? String)!, Gender: (self.usersDetailsDict.value(forKey: "Gender") as? String)!)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    /*
    // MARK: - ***** Block opponent user and update in firebase. *****
    func ReportUser(userID : String , userName : String , Age : String , location : String , ProfilePic : String,Gender: String)
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            print("currentUserID = \(currentUserID)")
            
            FIRDatabase.database().reference().child("ReportedUsersByMe").child(currentUserID).child(userID).updateChildValues(["UserID": userID, "UserName": userName, "Age": Age, "ProfilePicture": ProfilePic, "Location":location, "blockedStatus" : "reported","Gender" : Gender])
            FIRDatabase.database().reference().child("Users").child(userID).child("usersWhoReportedMe").child(currentUserID).updateChildValues(["UserID": currentUserID])
            
            FIRDatabase.database().reference().child("ReportedUsersByMe").child(currentUserID).child(userID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull{
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    return
                }
                
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                
                let alert = UIAlertController(title: "", message: "User Reported.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                    action in
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userBlocked"), object: nil)
                    self.navigationController!.popViewController(animated: true)
                    
                    
                }))
                self.present(alert, animated: true, completion: nil)
            })
        }
    }*/
    
    
    // MARK: - ***** viewProfilePicture_btnAction *****
    @IBAction func viewProfilePicture_btnAction(_ sender: Any) {
        
        self.showCurrentPostedImage.image = self.usersProfileImage.image
        self.showCurrentPostedImage.contentMode = UIViewContentMode.scaleAspectFit
        self.showCurrentPostedImage.clipsToBounds = true
        
        self.viewImage.isHidden=false
        
       /* if self.profileImageType == "image"
        {
            self.showCurrentPostedImage.image = self.usersProfileImage.image
            self.showCurrentPostedImage.contentMode = UIViewContentMode.scaleAspectFit
            self.showCurrentPostedImage.clipsToBounds = true
            
            self.viewImage.isHidden=false
           /* self.showCurrentPostedImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.showCurrentPostedImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (finished) in
            })*/
        }
        else
        {
            if let url = URL(string:usersDetailsDict.value(forKey: "ProfilePicture") as! String){
                
                self.viewImage.isHidden=false
                
                self.player = AVPlayer(url: url)
                self.playerViewController.player=self.player
                self.playerViewController.view.frame = self.showCurrentPostedImage.frame
                self.viewImage.addSubview(self.playerViewController.view)
                self.addChildViewController(self.playerViewController)
                self.playerViewController.willMove(toParentViewController: self)
                self.playerViewController.didMove(toParentViewController: self)
                
                self.player?.play()
                
                NotificationCenter.default.addObserver(self, selector: #selector(ViewOtherUsersProfileViewController.playerDidFinishPlaying), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
            }
        }*/
    }
    
    // MARK: - ***** playerDidFinishPlaying *****
    func playerDidFinishPlaying(){
        //now use seek to make current playback time to the specified time in this case (O)
        let duration : Int64 = 0 //(can be Int32 also)
        let preferredTimeScale : Int32 = 1
        let seekTime : CMTime = CMTimeMake(duration, preferredTimeScale)
        self.player?.seek(to: seekTime)
        self.player?.play()
    }

    
    // MARK: - ***** Chat Button Action. *****
    func Chat_tbnAction(_ button: UIButton, with event: UIEvent)
    {
       /* if (self.usersDetailsDict != nil)
        {
            let messagesScreen = storyboard?.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
            messagesScreen.mainConfessionCheck = "comingFromViewProfile"
            messagesScreen.chatUserDifferentiateStr = "Messages"
            messagesScreen.viewOpponentProfile = "fromParticularUserScreen"
            let userID =  usersDetailsDict.value(forKey: "id") as? String
            let firebase_user_id =  usersDetailsDict.value(forKey: "firebase_user_id") as? String
            messagesScreen.opponentName = (usersDetailsDict.value(forKey: "username") as? String)!
            messagesScreen.opponentImage = usersDetailsDict.value(forKey: "profile_pic") as! String
            messagesScreen.opponentUserID = userID!
            messagesScreen.opponentFirebaseUserID = firebase_user_id!
            messagesScreen.opponentProfilePic =  self.usersProfileImage.image!
            messagesScreen.typeOfUser = "WithProfileUser"
            messagesScreen.checkVisibiltyStr = "FromViewProfile"
            self.navigationController?.pushViewController(messagesScreen, animated: true)
        }*/
    }
    
    
    // MARK: - ***** Drag Chat Button Action. *****
    func wasDraggedBtn(_ recognizer: UIPanGestureRecognizer) {
        let button: UIButton? = (recognizer.view as? UIButton)
        let location: CGPoint = recognizer.translation(in: button)
        
        if (((self.chatBtn?.center.x)! + location.x)  > (self.chatBtn.frame.size.width)/2 && ((self.chatBtn?.center.y)! + location.y) > (self.chatBtn.frame.size.height)/2 && ((self.chatBtn?.center.x)! + location.x)  < (self.view.frame.size.width-(self.chatBtn.frame.size.width)/2) && ((self.chatBtn?.center.y)! + location.y) < (self.view.frame.size.height-(self.chatBtn.frame.size.height)/2))
        {
            self.chatBtn?.center = CGPoint(x: CGFloat((self.chatBtn?.center.x)! + location.x), y: CGFloat((self.chatBtn?.center.y)! + location.y))
        }
        
        recognizer.setTranslation(CGPoint.zero, in: button)
    }
    
    
    // MARK: - ***** ViewWillAppear. *****
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.isStatusBarHidden=false
        self.navigationController?.isNavigationBarHidden = true
        
        self.noDataFoundLbl.isHidden = true
        self.totalInterestsSaved = []
        self.interestsArray = []
        self.showInterestsArray = []
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            self.viewUserDetails(completion: { (response) in
//                DispatchQueue.global(qos: .background).async {
                    self.getUploadedMediaFiles()
//                }
            })
        }
    }
    
    
    /*
    // MARK: - ***** Fetch Opponent's Interests. *****
    func getInterests()
    {
        self.loadingView.isHidden = false
//        self.view.isUserInteractionEnabled = false
        let databaseRef = FIRDatabase.database().reference()
        databaseRef.child("Users").child(self.currentUserID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if snapshot.hasChild("Interests"){
                print("key/user already exist")
                databaseRef.child("Users").child(self.currentUserID).child("Interests").observeSingleEvent(of: FIRDataEventType.value, with: { snapshot in
                    
                    let interestsDict = snapshot.value as! NSMutableDictionary
                    print("interestsDict = \(interestsDict)")
                    
                    let myInterestArr = interestsDict.value(forKey: "myInterests")  as! NSMutableArray
                    if myInterestArr.count == 0{
                        self.noDataFoundLbl.isHidden = true
                        print("self.noDataFoundLbl.isHidden is false in interests")
                    }
                    else{
                        self.noDataFoundLbl.isHidden = true
                    }
                    
                    for index in 0..<myInterestArr.count
                    {
                        if myInterestArr[index] is NSNull
                        {
                            print("interest at index is nsnull")
                        }
                        else
                        {
                            if !(self.getUpdatedInterestsArray.contains(myInterestArr[index] as! String))
                            {
                                self.getUpdatedInterestsArray.add(myInterestArr[index] as! String)
                            }
                            
                            if !(self.interestsArray.contains(myInterestArr[index] as! String))
                            {
                                var interestString = String()
                                interestString = myInterestArr[index] as! String
                                print("interestString = \(interestString)")
                                var textBefore = String()
                                var textAfter = String()
                                if interestString.contains("@")
                                {
                                    let fullNameArr = interestString.components(separatedBy: "@")
                                    textBefore = fullNameArr[0]
                                    print("textBefore = \(textBefore)")
                                    textAfter = fullNameArr[1]
                                    print("textAfter = \(textAfter)")
                                    
                                    if !(self.interestsArray.contains(textBefore))
                                    {
                                        self.interestsArray.append(textBefore)
                                    }
                                    
                                    if !(self.totalInterestsSaved.contains(textBefore))
                                    {
                                        self.totalInterestsSaved.add(textBefore)
                                    }
                                }
                                else
                                {
                                    self.interestsArray.append(myInterestArr[index] as! String)
                                    self.totalInterestsSaved.add(myInterestArr[index] as! String)
                                }
                            }
                        }
                        
                    }
                    
                    for index in 0..<self.interestsArray.count
                    {
                        if index <= 4
                        {
                            if !(self.showInterestsArray.contains(self.interestsArray[index]))
                            {
                                self.showInterestsArray.append(self.interestsArray[index])
                            }
                        }
                        else{
                            print("index greater than 4")
                        }
                    }
                    
                    self.interests_txtView.isHidden = false
                    
                    if self.isLoadMore == false{
                        for index in 0..<self.showInterestsArray.count
                        {
                            let string = self.showInterestsArray.joined(separator: ", ")
                            print(string)
                            self.interests_txtView.text = string
                        }
                    }
                    else
                    {
                        for index in 0..<self.interestsArray.count
                        {
                            let string = self.interestsArray.joined(separator: ", ")
                            print(string)
                            self.interests_txtView.text = string
                        }
                    }
                    self.noDataFoundLbl.isHidden = true
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                })
            }
            else
            {
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                self.noDataFoundLbl.isHidden = true
                print("self.noDataFoundLbl.isHidden is false in interests")
            }
        })
    }
    */
    

    
    // MARK: - ***** Create Thumbnail of the Video fetched from Firebase. *****
    func createThumbnailOfVideoFromFileURL(_ strVideoURL: String) -> UIImage?{
        let asset = AVAsset(url: URL(string: strVideoURL)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), 100)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch
        {
            /* error handling here */
            if self.videoUrlAtIndex != ""
            {
//                var thumbnailImage = UIImage()
//                thumbnailImage = self.createThumbnailOfVideoFromFileURL(self.videoUrlAtIndex)!
                return UIImage(named: "Default Icon") //thumbnailImage
            }
            return nil
        }
    }
    
    
    
    // MARK: - ***** viewDidDisappear. *****
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - ***** UIScrollViewDelegate. *****
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.wrapper_ScrollView.isScrollEnabled=true
        self.wrapper_ScrollView.contentSize.height = self.aboutMe_scrollView.frame.origin.y + self.aboutMe_scrollView.frame.size.height + 10
    }

    

     /*
     
    // MARK: - ***** Fetch Opponent's Profile Visibility Settings For Notification. *****
    func fetchOwnerProfileVisibilitySettings()
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WWAN")
            print("Connected via WiFi")
         
       FIRDatabase.database().reference().child("Users").child(usersDetailsDict.value(forKey: "UserID") as! String).child("Settings").child("Notification").child("Profile_Visitors").observe(FIRDataEventType.value, with: { (snapshot) in
        
                    if snapshot.value is NSNull{
                        self.opponentProfileVisibilityStr = "On"
//                        if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
//                        {
                            if (self.opponentProfileVisibilityStr == "On") && (self.opponentUserDeviceId != "") //&& !(self.profileVisitorsArray.contains(self.usersDetailsDict.value(forKey: "UserID") as! String))
                            {
//                                let name = UserDefaults.standard.value(forKey: "UserName") as! String
//                                self.sendNotifications("\(name) viewed your profile.", andToken: self.opponentUserDeviceId)
                                
                                var totalCount = Int(self.profileVisitorsCount)
                                totalCount = totalCount! + 1
                            FIRDatabase.database().reference().child("Users").child(self.usersDetailsDict.value(forKey: "UserID") as! String).updateChildValues(["profileVisitorsCount": "\(totalCount!)"])
                                
                            self.sendNotifications("someone viewed your profile.", andToken: self.opponentUserDeviceId)
                                
                            }
//                        }
                        return
                    }
        
                    self.opponentProfileVisibilityStr = snapshot.value as! String
        

                        if (self.opponentProfileVisibilityStr == "On") && (self.opponentUserDeviceId != "")
                        {
//                            let name = UserDefaults.standard.value(forKey: "UserName") as! String
                            var totalCount = Int(self.profileVisitorsCount)
                            totalCount = totalCount! + 1
                            FIRDatabase.database().reference().child("Users").child(self.usersDetailsDict.value(forKey: "UserID") as! String).updateChildValues(["profileVisitorsCount": "\(totalCount!)"])
                            
//                            self.sendNotifications("\(name) viewed your profile.", andToken: self.opponentUserDeviceId)
                             self.sendNotifications("someone viewed your profile.", andToken: self.opponentUserDeviceId)
                        }
                })
        }
    }

    
    
    
    // MARK: - ***** Fetch Opponent user other Information From firebase. *****
    func getUserDetails()
    {
        self.loadingView.isHidden = false
        self.noDataFoundLbl.isHidden = true
        
        if self.currentUserID != ""
        {
           let databaseRef = FIRDatabase.database().reference()
           databaseRef.child("Users").child(self.currentUserID).keepSynced(true)
           databaseRef.child("Users").child(self.currentUserID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
           
                if snapshot.value is NSNull
                {
                    print("snapshot.value is nsnull")
                    if (UserDefaults.standard.value(forKey: "otherUsersDict") != nil)
                    {
                        if let dict = UserDefaults.standard.value(forKey: "otherUsersDict") as? NSMutableDictionary
                        {
                            self.usersDetailsDict = dict
                            
                            self.currentUserID = self.usersDetailsDict.value(forKey: "UserID") as! String
                            var userGenderTypeStr  = String()
                            userGenderTypeStr = self.usersDetailsDict.value(forKey: "Gender") as! String
                            if userGenderTypeStr == "male" || userGenderTypeStr == "Male"
                            {
                                self.gender_lbl.text = "Male"
                            }
                            else
                            {
                                self.gender_lbl.text = "Female"
                            }
                            
                            if (self.usersDetailsDict["userStatus"] != nil)
                            {
                                let usersStatus = self.usersDetailsDict.value(forKey: "userStatus") as! String
                                if usersStatus == "online"
                                {
                                    self.usersOnline_lbl.isHidden = false
                                    self.usersOnline_lbl.image = UIImage(named: "Online Icon")
                                }
                                else
                                {
                                    self.usersOnline_lbl.isHidden = false
                                    self.usersOnline_lbl.image = UIImage(named: "Offline Icon")
                                }
                            }
                            else{
                                self.usersOnline_lbl.isHidden = false
                                self.usersOnline_lbl.image = UIImage(named: "Offline Icon")
                            }
                            
                            let keysArray = self.usersDetailsDict.allKeys as! [String]
                            if keysArray.contains("AboutMe_description")
                            {
                                var descriptionStr = String()
                                descriptionStr = (self.usersDetailsDict.value(forKey: "AboutMe_description") as? String)!
                                if  descriptionStr.count > 0
                                {
                                    self.aboutMe_lbl.isHidden = true
                                    self.aboutMe_textView.text = self.usersDetailsDict.value(forKey: "AboutMe_description") as? String
                                }
                                else
                                {
                                    self.aboutMe_lbl.isHidden = false
                                    self.aboutMe_textView.text = ""
                                }
                            }
                            else
                            {
                                self.aboutMe_lbl.isHidden = false
                                self.aboutMe_textView.text = ""
                            }
                            
                            
                            self.usersName_lbl.numberOfLines = 100
                            self.usersName_lbl.adjustsFontSizeToFitWidth = true
                            
                            let userName = self.usersDetailsDict.value(forKey: "UserName") as? String
                            let userAge = self.usersDetailsDict.value(forKey: "Age") as? String
                            self.usersName_lbl.text = userName! + ", " +  userAge!
                            //            self.userName_lbl.text = usersDetailsDict.value(forKey: "UserName") as? String
                            
                            
                            var userCurrentCity = String()
                            var userCurrentState = String()
                            
                            if keysArray.contains("currentCity")
                            {
                                userCurrentCity = self.usersDetailsDict.value(forKey: "currentCity") as! String
                            }
                            else{
                                userCurrentCity = ""
                            }
                            
                            
                            if keysArray.contains("currentState")
                            {
                                userCurrentState = (self.usersDetailsDict.value(forKey: "currentState") as? String)!
                            }
                            else{
                                userCurrentState = ""
                            }
                            
                            
                            if (userCurrentCity != "") && (userCurrentState != "")
                            {
                                self.usersLocationLbl.text = userCurrentCity + ", " + userCurrentState
                            }
                            else
                            {
                                self.usersLocationLbl.text = self.usersDetailsDict.value(forKey: "Location") as? String
                            }
                            
                            
                            if keysArray.contains("Meet Up Preferences")
                            {
                                self.meetUp_lbl.text = self.usersDetailsDict.value(forKey: "Meet Up Preferences") as? String
                            }
                            else{
                                self.meetUp_lbl.text = ""
                            }
                            
                            if keysArray.contains("Island")
                            {
                                self.country_lbl.text = self.usersDetailsDict.value(forKey: "Island") as? String
                            }
                            else{
                                self.country_lbl.text = ""
                            }
                            
                            if keysArray.contains("currentLocationCountry")
                            {
                                self.opponentCurrentLocation = (self.usersDetailsDict.value(forKey: "currentLocationCountry") as? String)!
                            }
                            else{
                                self.opponentCurrentLocation = ""
                            }
                            
                            
                            if keysArray.contains("myDeviceToken")
                            {
                                self.opponentUserDeviceId = (self.usersDetailsDict.value(forKey: "myDeviceToken") as? String)!
                            }
                            else{
                                self.opponentUserDeviceId = ""
                            }
                            
                            if keysArray.contains("Interesting In meeting up with")
                            {
                                self.interestedIn_lbl.text = self.usersDetailsDict.value(forKey: "Interesting In meeting up with") as? String
                            }
                            else{
                                self.interestedIn_lbl.text = ""
                            }
                            
                            
                            if keysArray.contains("CurrentLongitude")
                            {
                                self.ownerLongitude = (self.usersDetailsDict.value(forKey: "CurrentLongitude") as? Double)!
                            }
                            else{
                                self.ownerLongitude = 0.0
                            }
                            if keysArray.contains("CurrentLatitude")
                            {
                                self.ownerLatitude = (self.usersDetailsDict.value(forKey: "CurrentLatitude") as? Double)!
                            }
                            else{
                                self.ownerLatitude = 0.0
                            }
                            
                            self.bodyType_lbl.text = self.usersDetailsDict.value(forKey: "BodyType") as? String
                            self.hairColor_lbl.text = self.usersDetailsDict.value(forKey: "Hair Color") as? String
                            self.relationship_lbl.text = self.usersDetailsDict.value(forKey: "Relationship Status") as? String
                            self.smoking_lbl.text = self.usersDetailsDict.value(forKey: "Smoking") as? String
                            self.drinking_lbl.text = self.usersDetailsDict.value(forKey: "Drinking") as? String
                            self.education_lbl.text = self.usersDetailsDict.value(forKey: "Education") as? String
                            
                            if keysArray.contains("profileImageType")
                            {
                                self.profileImageType = (self.usersDetailsDict.value(forKey: "profileImageType") as? String)!
                            }
                            else{
                                self.profileImageType = "image"
                            }
                            
                            
                            if self.profileImageType == "image"
                            {
                                self.usersProfileImage.sd_setImage(with: URL(string: self.usersDetailsDict.value(forKey: "ProfilePicture") as! String), placeholderImage: UIImage(named: "Default Icon - 3"))
                            }
                            else
                            {
                                if let url = URL(string: self.usersDetailsDict.value(forKey: "ProfilePicture") as! String){
                                    self.videoUrlAtIndex = self.usersDetailsDict.value(forKey: "ProfilePicture") as! String
                                    self.usersProfileImage.image = self.createThumbnailOfVideoFromFileURL(String(describing: url))
                                }
                            }
                            
                            let status = Reach().connectionStatus()
                            switch status
                            {
                            case .unknown, .offline:
                                print("Not connected")
                                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            case .online(.wwan) , .online(.wiFi):
                                print("Connected via WWAN")
                                print("Connected via WiFi")
                                
                                self.fetchRelLikesStatus()
                                
                                self.fetchOwnerProfileVisitorsCount()
                            }
                        }
                        else
                        {
                            print("dictionary is nil")
                        }
                    }
                    else
                    {
                        self.loadingView.isHidden = true
                    }
                }
                else
                {
                    self.usersDetailsDict = snapshot.value as! NSMutableDictionary
                    
                    print("snapshot.value is not nsnull = \(self.usersDetailsDict)")
                    
                    let keysArray = self.usersDetailsDict.allKeys as! [String]
                    
                     if keysArray.contains("Age") && keysArray.contains("UserName")
                     {
                        self.currentUserID = self.usersDetailsDict.value(forKey: "UserID") as! String
                        var userGenderTypeStr  = String()
                        userGenderTypeStr = self.usersDetailsDict.value(forKey: "Gender") as! String
                        if userGenderTypeStr == "male" || userGenderTypeStr == "Male"
                        {
                            self.gender_lbl.text = "Male"
                        }
                        else
                        {
                            self.gender_lbl.text = "Female"
                        }
                        
                        if (self.usersDetailsDict["userStatus"] != nil)
                        {
                            let usersStatus = self.usersDetailsDict.value(forKey: "userStatus") as! String
                            if usersStatus == "online"
                            {
                                self.usersOnline_lbl.isHidden = false
                                self.usersOnline_lbl.image = UIImage(named: "Online Icon")
                            }
                            else
                            {
                                self.usersOnline_lbl.isHidden = false
                                self.usersOnline_lbl.image = UIImage(named: "Offline Icon")
                            }
                        }
                        else{
                            self.usersOnline_lbl.isHidden = false
                            self.usersOnline_lbl.image = UIImage(named: "Offline Icon")
                        }
                        
                        if keysArray.contains("AboutMe_description")
                        {
                            var descriptionStr = String()
                            descriptionStr = (self.usersDetailsDict.value(forKey: "AboutMe_description") as? String)!
                            if  descriptionStr.count > 0
                            {
                                self.aboutMe_lbl.isHidden = true
                                self.aboutMe_textView.text = self.usersDetailsDict.value(forKey: "AboutMe_description") as? String
                            }
                            else
                            {
                                self.aboutMe_lbl.isHidden = false
                                self.aboutMe_textView.text = ""
                            }
                        }
                        else
                        {
                            self.aboutMe_lbl.isHidden = false
                            self.aboutMe_textView.text = ""
                        }
                        
                        
                        self.usersName_lbl.numberOfLines = 100
                        self.usersName_lbl.adjustsFontSizeToFitWidth = true
                        
                        let userName = self.usersDetailsDict.value(forKey: "UserName") as? String
                        let userAge = self.usersDetailsDict.value(forKey: "Age") as? String
                        self.usersName_lbl.text = userName! + ", " +  userAge!
                        //            self.userName_lbl.text = usersDetailsDict.value(forKey: "UserName") as? String
                        
                        
                        var userCurrentCity = String()
                        var userCurrentState = String()
                        
                        if keysArray.contains("currentCity")
                        {
                            userCurrentCity = self.usersDetailsDict.value(forKey: "currentCity") as! String
                        }
                        else{
                            userCurrentCity = ""
                        }
                        
                        
                        if keysArray.contains("currentState")
                        {
                            userCurrentState = (self.usersDetailsDict.value(forKey: "currentState") as? String)!
                        }
                        else{
                            userCurrentState = ""
                        }
                        
                        
                        if (userCurrentCity != "") && (userCurrentState != "")
                        {
                            self.usersLocationLbl.text = userCurrentCity + ", " + userCurrentState
                        }
                        else
                        {
                            self.usersLocationLbl.text = self.usersDetailsDict.value(forKey: "Location") as? String
                        }
                        
                        
                        if keysArray.contains("Meet Up Preferences")
                        {
                            self.meetUp_lbl.text = self.usersDetailsDict.value(forKey: "Meet Up Preferences") as? String
                        }
                        else{
                            self.meetUp_lbl.text = ""
                        }
                        
                        if keysArray.contains("Island")
                        {
                            self.country_lbl.text = self.usersDetailsDict.value(forKey: "Island") as? String
                        }
                        else{
                            self.country_lbl.text = ""
                        }
                        
                        if keysArray.contains("currentLocationCountry")
                        {
                            self.opponentCurrentLocation = (self.usersDetailsDict.value(forKey: "currentLocationCountry") as? String)!
                        }
                        else{
                            self.opponentCurrentLocation = ""
                        }
                        
                        
                        if keysArray.contains("myDeviceToken")
                        {
                            self.opponentUserDeviceId = (self.usersDetailsDict.value(forKey: "myDeviceToken") as? String)!
                        }
                        else{
                            self.opponentUserDeviceId = ""
                        }
                        
                        if keysArray.contains("Interesting In meeting up with")
                        {
                            self.interestedIn_lbl.text = self.usersDetailsDict.value(forKey: "Interesting In meeting up with") as? String
                        }
                        else{
                            self.interestedIn_lbl.text = ""
                        }
                        
                        
                        if keysArray.contains("CurrentLongitude")
                        {
                            self.ownerLongitude = (self.usersDetailsDict.value(forKey: "CurrentLongitude") as? Double)!
                        }
                        else{
                            self.ownerLongitude = 0.0
                        }
                        if keysArray.contains("CurrentLatitude")
                        {
                            self.ownerLatitude = (self.usersDetailsDict.value(forKey: "CurrentLatitude") as? Double)!
                        }
                        else{
                            self.ownerLatitude = 0.0
                        }
                        
                        self.bodyType_lbl.text = self.usersDetailsDict.value(forKey: "BodyType") as? String
                        self.hairColor_lbl.text = self.usersDetailsDict.value(forKey: "Hair Color") as? String
                        self.relationship_lbl.text = self.usersDetailsDict.value(forKey: "Relationship Status") as? String
                        self.smoking_lbl.text = self.usersDetailsDict.value(forKey: "Smoking") as? String
                        self.drinking_lbl.text = self.usersDetailsDict.value(forKey: "Drinking") as? String
                        self.education_lbl.text = self.usersDetailsDict.value(forKey: "Education") as? String
                        
                        if keysArray.contains("profileImageType")
                        {
                            self.profileImageType = (self.usersDetailsDict.value(forKey: "profileImageType") as? String)!
                        }
                        else{
                            self.profileImageType = "image"
                        }
                        
                        
                        if self.profileImageType == "image"
                        {
                            self.usersProfileImage.sd_setImage(with: URL(string: self.usersDetailsDict.value(forKey: "ProfilePicture") as! String), placeholderImage: UIImage(named: "Default Icon - 3"))
                        }
                        else
                        {
                            if let url = URL(string: self.usersDetailsDict.value(forKey: "ProfilePicture") as! String){
                                self.videoUrlAtIndex = self.usersDetailsDict.value(forKey: "ProfilePicture") as! String
                                self.usersProfileImage.image = self.createThumbnailOfVideoFromFileURL(String(describing: url))
                            }
                        }
                        
                        let status = Reach().connectionStatus()
                        switch status
                        {
                        case .unknown, .offline:
                            print("Not connected")
                            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        case .online(.wwan) , .online(.wiFi):
                            print("Connected via WWAN")
                            print("Connected via WiFi")
                            
                            self.fetchRelLikesStatus()
                            
                            self.fetchOwnerProfileVisitorsCount()
                        }
                     }
                
                }
            })
        }
        else{
            self.loadingView.isHidden = true
        }
    }
    
    
    //MARK: ***** fetchMyIslandCountry *****
    func fetchMyIslandCountry()
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("currentLocationCountry").keepSynced(true)
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("currentLocationCountry").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull
                {
                    self.fetchMyLatitude()
                }
                else
                {
                   let countryStr = snapshot.value as! String
                    if (countryStr != self.opponentCurrentLocation)
                        //self.country_lbl.text
                    {
                        if (self.opponentCurrentLocation == "")
                        {
                            self.milesAway_Lbl.text = self.country_lbl.text
                            self.milesAway_Lbl.textColor = UIColor.white
                        }
                        else
                        {
                            self.milesAway_Lbl.text = self.opponentCurrentLocation//self.country_lbl.text
                            self.milesAway_Lbl.textColor = UIColor.white
                        }
                        
                         self.getInterests()
                    }
                    else
                    {
                         self.fetchMyLatitude()
                    }
                }
            })
        }
    }

    
    //MARK: ***** fetchMyLatitude *****
    func fetchMyLatitude()
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("CurrentLatitude").observe(FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull
                {
                    self.myLatitude  = 0.0
                    self.getInterests()
                }
                else
                {
                    self.myLatitude = snapshot.value as! Double
                    self.fetchMyLongitude()
                }
            })
        }
    }

    //MARK: ***** fetchMyLongitude *****
    func fetchMyLongitude()
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            FIRDatabase.database().reference().child("Users").child(currentUserID).child("CurrentLongitude").observe(FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull
                {
                    self.myLongitude  = 0.0
                    self.getInterests()
                }
                else
                {
                    self.myLongitude = snapshot.value as! Double
                }
                
                    self.milesAway_Lbl.isHidden = false
                    
                    let ownerCoordinate = CLLocation(latitude: self.ownerLatitude, longitude: self.ownerLongitude)
                    let myCoordinate = CLLocation(latitude: self.myLatitude, longitude: self.myLongitude)
                    
                    let distanceInMeters = ownerCoordinate.distance(from: myCoordinate) // result is in meters
                    let distanceInMiles = distanceInMeters/1609.344
  
                    self.milesAway_Lbl.text = String(distanceInMiles.truncate(places: 2)) + " miles away"
                    self.milesAway_Lbl.textColor = UIColor.init(red: 39.0/255, green: 230.0/255, blue: 227.0/255, alpha: 1.0)
                
                  self.getInterests()
            })
        }
    }*/

    
    
    // MARK: - ***** Automatic Scroll users pictures Method. *****
    func moveToNextPage ()
    {
        let pageWidth:CGFloat = self.imagesScrollView.frame.width
        let totalCount: Int = self.profileImagesArray.count
        let maxWidth:CGFloat = pageWidth * CGFloat(totalCount)
        let contentOffset:CGFloat = self.imagesScrollView.contentOffset.x
        var slideToX = contentOffset + pageWidth
        if  contentOffset + pageWidth == maxWidth
        {
            slideToX = 0
        }
        
        self.imagesScrollView.scrollRectToVisible(CGRect(x:slideToX, y:self.imagesScrollView.frame.origin.y, width:pageWidth, height:self.imagesScrollView.frame.height), animated: true)
    }
    
    
    //MARK: ***** UIScrollView Delegate *****
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
    }
    
    
    //MARK: ***** closeViewVideo_btnAction *****
    @IBAction func closeViewVideo_btnAction(_ sender: Any) {
        self.viewVideoView.isHidden = true
        playerViewController.player!.pause()
        if let play = player {
            play.pause()
            player = nil
        } else {
            print("player was already deallocated")
        }
        
        if self.videoLikedBool == true
        {
            self.likesCountArray = []
            self.superLikesCountArray = []
            self.getVideoURLArray = []
            self.timestampVideoArray = []
            self.videoArray = []
            self.thumbNailArray = []
            self.captionVideoArray = []
            self.videos_tableView.reloadData()
            self.videoLikedBool = false
            self.albumArray = []
//            self.getUploadedAlbum()
        }
    }
    
    
    // MARK: - ***** Close Image View Button Action. *****
    @IBAction func closeFullSizeImage_btnAction(_ sender: Any) {
        self.ViewFullSize_ImageView.isHidden = true
    }
    
    
    
    // MARK: - ***** Close Videos View Button Action. *****
    @IBAction func videos_BackBtn(_ sender: Any) {
        self.videosView.isHidden = true
        self.aboutMeView.isHidden = true
        self.aboutMe_scrollView.isHidden = true
        self.PhotosView.isHidden = true
        self.videosView.isHidden = true
        self.tappedBtnStr = "albumBtn"
        self.aboutMeBtn.setTitleColor(UIColor.gray, for: .normal)
        self.albumBnt.setTitleColor(UIColor.black, for: .normal)
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        self.videoController.stop()
        self.videoController.view.removeFromSuperview()
    }
    
    
    // MARK: - ***** Back Button Action. *****
    @IBAction func back_btnAction(_ sender: Any) {
        
        if (self.cameFromMatch == "ViewOtherUsersProfileViewController")
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadDataOnWiMatch"), object: nil, userInfo: ["userInfo": self.currentUserID])
        }
        
        if (isKindOfScreen == "GroupChat")
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshGroupChat"), object: nil, userInfo: ["userInfo": self.currentUserID])
        }
        else if (self.isKindOfScreen == "MessagesScreen")
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateListingOnRevealProfile"), object: nil, userInfo: nil)
        }
        
        self.navigationController!.popViewController(animated: true)
    }
    
    
    // MARK: - ***** Block opponent user button action. *****
    @IBAction func blockUser_btnAction(_ sender: Any) {
    
        let alert = UIAlertController(title: "", message: "Do you really want to block?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
            
            let status = Reach().connectionStatus()
            switch status
            {
            case .unknown, .offline:
                let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .online(.wwan) , .online(.wiFi):
                print("Connected via WWAN")
              
//                self.loadingView.isHidden = false
//                self.view.isUserInteractionEnabled = false
//
//               self.blockUser(userID: (self.usersDetailsDict.value(forKey: "UserID") as? String)!, userName: (self.usersDetailsDict.value(forKey: "UserName") as? String)!, Age: (self.usersDetailsDict.value(forKey: "Age") as? String)!, location: (self.usersDetailsDict.value(forKey: "Location") as? String)!, ProfilePic: (self.usersDetailsDict.value(forKey: "ProfilePicture") as? String)!, Gender: (self.usersDetailsDict.value(forKey: "Gender") as? String)!)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    /*
    
    // MARK: - ***** Block opponent user and update in firebase. *****
    func blockUser(userID : String , userName : String , Age : String , location : String , ProfilePic : String,Gender: String)
    {
        if let currentUserID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            print("currentUserID = \(currentUserID)")
            
            FIRDatabase.database().reference().child("BlockedUsers").child(currentUserID).child(userID).updateChildValues(["UserID": userID, "UserName": userName, "Age": Age, "ProfilePicture": ProfilePic, "Location":location, "blockedStatus" : "blocked","Gender" : Gender,"userType": "ProfileUser"])
        FIRDatabase.database().reference().child("Users").child(userID).child("blockedByUser").child(currentUserID).updateChildValues(["UserID": currentUserID])
            
            FIRDatabase.database().reference().child("BlockedUsers").child(currentUserID).child(userID).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                if snapshot.value is NSNull{
                    self.loadingView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    return
                }
                
                self.loadingView.isHidden = true
                self.view.isUserInteractionEnabled = true
                
                let alert = UIAlertController(title: "", message: "User Blocked.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                    action in
                    
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.getValueFromSpotlight = true
                    
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userBlocked"), object: nil)
                    self.navigationController!.popViewController(animated: true)
                    
                }))
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    */
    
    
    // MARK: - ***** Close Photos Button Action. *****
    @IBAction func photos_backBtn(_ sender: Any) {
        self.aboutMeView.isHidden = true
        self.aboutMe_scrollView.isHidden = true
        self.PhotosView.isHidden = true
        self.videosView.isHidden = true
        self.tappedBtnStr = "albumBtn"
        self.albumBnt.setTitleColor(UIColor.black, for: .normal)
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeBtn.setTitleColor(UIColor.gray, for: .normal)
    }
    
    
    // MARK: - ***** Load More Button Action. *****
    @IBAction func loadMoreInterests_btnAction(_ sender: Any) {
        if loadMoreInterestsBtn.titleLabel?.text == "Load More Interests"
        {
            loadMoreInterestsBtn.setTitle("Show Less Interests", for: UIControlState.normal)
            isLoadMore = true
        }
        else{
            loadMoreInterestsBtn.setTitle("Load More Interests", for: UIControlState.normal)
            isLoadMore = false
        }
        
        self.interests_txtView.isHidden = false
        
        if self.isLoadMore == false
        {
            for index in 0..<self.showInterestsArray.count
            {
                print("index = \(index)")
                let string = self.showInterestsArray.joined(separator: ", ")
                self.interests_txtView.text = string
            }
        }
        else
        {
            
            for index in 0..<self.interestsArray.count
            {
                print("index = \(index)")
                var string = String()
//                if (self.interestsArray.count == (index + 1))
//                {
//                    string = self.interestsArray[index]
//                    print(string)
//                }
//                else
//                {
                    string = self.interestsArray.joined(separator: ", ")
//                    print(string)
//                }
                
                self.interests_txtView.text = string
            }
        }
    }
    
    
    
    // MARK: - ***** About Me button Action. *****
    @IBAction func aboutMe_btnAction(_ sender: Any) {
        if self.interestsArray.count > 0{
            self.noDataFoundLbl.isHidden = true
        }
        else{
            self.noDataFoundLbl.isHidden = false
        }
        
        self.aboutMe_scrollView.isScrollEnabled = false
        
        self.wrapper_ScrollView.isScrollEnabled=true
        
        
        if (self.topViewButtons.isHidden == false)
        {
            self.wrapper_ScrollView.contentOffset.y =  self.aboutPhotosVideosView.frame.origin.y + self.aboutPhotosVideosView.frame.size.height - 25
        }
        self.wrapper_ScrollView.contentSize.height = self.aboutMe_scrollView.frame.origin.y + self.aboutMe_scrollView.frame.size.height + 10
        
        self.noDataFoundLbl.isHidden = true
        self.photosCollectionView.isHidden = true
        self.aboutMeView.isHidden = true
        self.aboutMe_scrollView.isHidden = false
        self.PhotosView.isHidden = true
        self.videosView.isHidden = true
        self.tappedBtnStr = "aboutBtn"
        self.albumBnt.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeBtn.setTitleColor(UIColor.black, for: .normal)
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        
        self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeTop.setTitleColor(UIColor.black, for: .normal)
        self.postsBtnATop.setTitleColor(UIColor.gray, for: .normal)
    }
    
    
    // MARK: - ***** View Albums Button Action. *****
    @IBAction func album_btnAction(_ sender: Any) {
        if videoUploadbool == true{
            self.timestampVideoArray = []
            self.getVideoURLArray = []
            self.videoArray = []
            self.superLikesCountArray = []
            self.likesCountArray = []
            self.thumbNailArray = []
            self.captionVideoArray = []
            self.albumArray = []
//            self.getUploadedAlbum()
            self.videoUploadbool = false
        }
        else
        {
            if self.albumArray.count == 0{
                self.noDataFoundLbl.isHidden = false
                self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.noDataFoundLbl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: { (finished) in
                })
            }
            else
            {
                self.noDataFoundLbl.isHidden = true
            }
        }
        
        self.wrapper_ScrollView.isScrollEnabled=true
        if (self.topViewButtons.isHidden == false)
        {
            self.wrapper_ScrollView.contentOffset.y =  self.aboutPhotosVideosView.frame.origin.y + self.aboutPhotosVideosView.frame.size.height - 25
        }
        
        if (self.photosCollectionView.contentSize.height < 1000)
        {
            self.aboutMe_scrollView.contentSize.height = self.loadMoreInterestsBtn.frame.origin.y + self.loadMoreInterestsBtn.frame.size.height + 20
            self.wrapper_ScrollView.contentSize.height = self.aboutMe_scrollView.frame.origin.y + self.aboutMe_scrollView.contentSize.height + 50
        }
        else{
            self.wrapper_ScrollView.contentSize.height =  self.photosCollectionView.frame.origin.y  + self.photosCollectionView.contentSize.height + 10
        }
        
        self.tappedBtnStr = "albumBtn"
        
        self.photosCollectionView.isHidden = false
        self.aboutMeView.isHidden = true
        self.aboutMe_scrollView.isHidden = true
        self.PhotosView.isHidden = true
        self.videosView.isHidden = true
        self.aboutMeBtn.setTitleColor(UIColor.gray, for: .normal)
        self.albumBnt.setTitleColor(UIColor.black, for: .normal)
        self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
        
        self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
        self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
        self.postsBtnATop.setTitleColor(UIColor.gray, for: .normal)
    }
    
    // MARK: - ***** View Posts Button Action. *****
    @IBAction func posts_btnAction(_ sender: Any) {
        self.aboutMeView.isHidden = true
        self.aboutMe_scrollView.isHidden = true
        self.noDataFoundLbl.isHidden = false
        self.PhotosView.isHidden = true
        self.videosView.isHidden = true
         self.photosCollectionView.isHidden = true
        self.tappedBtnStr = "postsBtn"
        self.postsBtn.setTitleColor(UIColor.black, for: .normal)
        self.aboutMeBtn.setTitleColor(UIColor.gray, for: .normal)
        self.albumBnt.setTitleColor(UIColor.gray, for: .normal)
        
        self.albumBtnTop.setTitleColor(UIColor.gray, for: .normal)
        self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
        self.postsBtnATop.setTitleColor(UIColor.black, for: .normal)
    }
    
    
    
    // MARK: - ***** UICollectionView Delegates & Datasource Methods. *****
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.albumArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
            // get a reference to our storyboard cell
            let photosCell = collectionView.dequeueReusableCell(withReuseIdentifier: "photosCell", for: indexPath as IndexPath) as! PhotosCollectionViewCell
        
            photosCell.clearCellData()
            photosCell.layer.borderColor = UIColor.white.cgColor
            photosCell.layer.borderWidth = 1
        
        if ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "type") as? String == "1"
        {
            photosCell.videoPlayIcon.isHidden = true
            DispatchQueue.global(qos: .default).async{
                let urlStr = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") as? String)!
                DispatchQueue.main.async {
                    photosCell.photoImageView.sd_setImage(with: URL(string:urlStr), placeholderImage: UIImage(named: "Default Icon")) //, options: .refreshCached
                }
            }
          
            photosCell.photoImageView.contentMode = UIViewContentMode.scaleAspectFill
        }
        else if ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "type") as? String == "2"
        {
            photosCell.videoPlayIcon.isHidden = false
            DispatchQueue.global(qos: .default).async{
                let videoStr = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") as? String)!
                self.videoUrlAtIndex = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") as? String)!
                DispatchQueue.main.async {
                    photosCell.photoImageView.image = self.createThumbnailOfVideoFromFileURL(videoStr)
                }
            }
           
            photosCell.photoImageView.contentMode = UIViewContentMode.scaleAspectFill
        }
        
            return photosCell
    }
    
    
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == self.photosCollectionView
        {
            if ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "type") as? String == "1"
            {
                photoDeleteIndex = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "id") as? String)!
                if ((((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "likedbycurrent") as? String) != nil)
                {
                     self.checkMediaLikedByMe = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "likedbycurrent") as? String)!
                }
                else
                {
                    self.checkMediaLikedByMe = "no"
                }
                
                let likesCountStr = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "total_likes") as? String)!
                if (likesCountStr == "0")
                {
                    self.imageLikeCount_lbl.text = ""
                }
                else
                {
                    if (likesCountStr == "")
                    {
                        self.imageLikeCount_lbl.text = ""
                    }
                    else
                    {
                        self.imageLikeCount_lbl.text = String(likesCountStr)
                    }
                }
                
                if (self.checkMediaLikedByMe == "yes")
                {
                    DispatchQueue.main.async {
                        self.userAlbumLikeImage.image = UIImage(named: "LikeIconBlue")
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.userAlbumLikeImage.image = UIImage(named: "confessionLikeIcon")
                    }
                }
                
                let captionStr =  ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String
                if captionStr == ""
                {
                    self.captionPhoto_txtView.text = ""
                }
                else
                {
                    self.captionPhoto_txtView.text = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String
                }
                
                self.captionPhoto_txtView.isScrollEnabled = false
                
                let fixedWidth = self.captionPhoto_txtView.frame.size.width
                self.captionPhoto_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize = self.captionPhoto_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame = self.captionPhoto_txtView.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                self.captionPhoto_txtView.frame = newFrame;
                
                self.viewParcticularImage.frame.size.width = self.view.frame.size.width
                self.viewParcticularImage.frame.size.height = self.view.frame.size.width
                self.fullImageViewBtn.frame.size.width = self.view.frame.size.width
                self.fullImageViewBtn.frame.size.height = self.view.frame.size.width
                self.viewParcticularImage.contentMode = UIViewContentMode.scaleAspectFit
                
                self.wrapperViewBtns.frame.origin.y = self.viewParcticularImage.frame.origin.y +  self.viewParcticularImage.frame.size.height + 20
                self.captionPhoto_txtView.frame.origin.y = self.wrapperViewBtns.frame.origin.y +  self.wrapperViewBtns.frame.size.height + 10
                
                self.photoCloseBtn.frame.origin.y = self.captionPhoto_txtView.frame.origin.y + self.captionPhoto_txtView.frame.height + 70
                
                self.wrapperScrollPhoto.isScrollEnabled=true
                self.wrapperScrollPhoto.contentSize.height = self.photoCloseBtn.frame.origin.y + self.photoCloseBtn.frame.size.height + 50
                
                let photosCell = collectionView.cellForItem(at: indexPath) as! PhotosCollectionViewCell
                self.viewParcticularImage.image = photosCell.photoImageView.image
//                self.viewParcticularImage.contentMode = UIViewContentMode.scaleToFill
                self.viewParcticularImage.contentMode = UIViewContentMode.scaleAspectFill
                self.viewParcticularImage.clipsToBounds = true
                self.ViewFullSize_ImageView.isHidden = false
              /*  self.viewParcticularImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.viewParcticularImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: { (finished) in

                })*/
                
                let timeStampInt = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "created") as! Int
                let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampInt))
                let dateString = timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
                if dateString == ""
                {
                    self.displayTime_lbl.text = ""
                }
                else{
                    self.displayTime_lbl.text = dateString
                }
            }
            else
            {
                self.deleteVideoURL = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "id") as? String)!
                if ((((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "likedbycurrent") as? String) != nil)
                {
                    self.checkMediaLikedByMe = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "likedbycurrent") as? String)!
                }
                else
                {
                    self.checkMediaLikedByMe = "no"
                }
                
                let likesCountStr = (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "total_likes") as? String)!
                if (likesCountStr == "0")
                {
                    self.imageLikeCount_lbl.text = ""
                }
                else
                {
                    if (likesCountStr == "")
                    {
                        self.imageLikeCount_lbl.text = ""
                    }
                    else
                    {
                        self.imageLikeCount_lbl.text = String(likesCountStr)
                    }
                }
                
                if (self.checkMediaLikedByMe == "yes")
                {
                    DispatchQueue.main.async {
                        self.userVideoLikeImage.image = UIImage(named: "LikeIconBlue")
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.userVideoLikeImage.image = UIImage(named: "confessionLikeIcon")
                    }
                }
                
                self.viewVideoView.isHidden=false
                
                let timeStampInt = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "created") as! Int
                let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampInt))
                let dateString = timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
                if dateString == ""
                {
                    self.displayVideoTime_lbl.text = ""
                }
                else
                {
                    self.displayVideoTime_lbl.text = dateString
                }
                
                let captionStr = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String
                if captionStr == ""
                {
                    self.captionVideo_txtView.text = ""
                }
                else{
                    self.captionVideo_txtView.text = ((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "caption") as? String
                }
                
                
                self.captionVideo_txtView.isScrollEnabled = false
                
                let fixedWidth = self.captionVideo_txtView.frame.size.width
                self.captionPhoto_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize = self.captionVideo_txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame = self.captionVideo_txtView.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                self.captionVideo_txtView.frame = newFrame;
                
                self.videoClosebtn.frame.origin.y = self.captionVideo_txtView.frame.origin.y + self.captionVideo_txtView.frame.height + 70
                
                self.wrapperScrollVideo.isScrollEnabled=true
                self.wrapperScrollVideo.contentSize.height = self.videoClosebtn.frame.origin.y + self.videoClosebtn.frame.size.height + 50
                
                let videoURL1 = URL(string: (((self.albumArray[indexPath.row]) as AnyObject).value(forKey: "media") as? String)!)
                player = AVPlayer(url: videoURL1!)
                playerViewController.player = player
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                    try AVAudioSession.sharedInstance().setActive(true)
                } catch {
//                    print(error)
                }
                
                // using custom view for AVPlayerController()
                if UIScreen.main.bounds.size.height == 568{
                    playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(50), width: CGFloat(self.view.frame.size.width), height: CGFloat(250))
                    self.displayLikeView.frame.origin.y = playerViewController.view.frame.origin.y + playerViewController.view.frame.size.height + 10
                    self.captionVideo_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                }
                else
                {
                    playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(60), width: CGFloat(self.view.frame.size.width), height: CGFloat(350))
                    self.displayLikeView.frame.origin.y = playerViewController.view.frame.origin.y + playerViewController.view.frame.size.height + 10
                    self.captionVideo_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
                }
             
                self.addChildViewController(playerViewController)
                self.wrapperScrollVideo.addSubview(playerViewController.view)
                playerViewController.didMove(toParentViewController: self)
                playerViewController.player!.play()
            }
        }
    }

    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) mon ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 mon ago"
            } else {
                return "Last mon"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) min ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) sec ago"
        } else {
            return "just now"
        }
    }
    
    
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
  
    
    // MARK: - ***** Play Particular Video Button Action. *****
    func playVideoBtn(_ sender: UIButton!) {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WWAN")
            
          /*  self.viewVideoView.isHidden=false
            self.deleteVideoURL = self.getVideoURLArray[sender.tag]
            if self.timestampVideoArray[sender.tag] as String == ""
            {
                self.displayVideoTime_lbl.text = ""
            }
            else{
                let timestampStr = self.timedifference(dateString: self.timestampVideoArray[sender.tag]  as String)
                //  as Int64)
                if timestampStr == ""
                {
                    self.displayVideoTime_lbl.text = ""
                }
                else{
                    self.displayVideoTime_lbl.text = timestampStr
                }
            }
            
            let captionStr = self.captionVideoArray[sender.tag]
            if captionStr == ""
            {

            }
            else{
                self.captionVideo_txtView.text = self.captionVideoArray[sender.tag]
            }
            
            let videoURL1 = URL(string: self.videoArray[sender.tag])
            player = AVPlayer(url: videoURL1!)
            playerViewController.player = player
            
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                try AVAudioSession.sharedInstance().setActive(true)
            } catch {
                print(error)
            }
            
            // using custom view for AVPlayerController()
            if UIScreen.main.bounds.size.height == 568{
                playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(50), width: CGFloat(self.view.frame.size.width), height: CGFloat(250))
                self.displayLikeView.frame.origin.y = playerViewController.view.frame.origin.y + playerViewController.view.frame.size.height + 10
                self.captionVideo_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
            }
            else{
                playerViewController.view.frame = CGRect(x: CGFloat(0), y: CGFloat(60), width: CGFloat(self.view.frame.size.width), height: CGFloat(350))
                self.displayLikeView.frame.origin.y = playerViewController.view.frame.origin.y + playerViewController.view.frame.size.height + 10
                self.captionVideo_txtView.frame.origin.y = self.displayLikeView.frame.origin.y + self.displayLikeView.frame.size.height + 10
            }
            self.addChildViewController(playerViewController)
            self.viewVideoView.addSubview(playerViewController.view)
            playerViewController.didMove(toParentViewController: self)
            playerViewController.player!.play()
          
            */
            
        }
     }

    
    //MARK:- ***** TIME DIFFERENCE FROM TIMESTAMP AND CURRENT TIME *****
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        
        /// GET TIMESTAMP IN UNIX
        let currentTimeStamp = Int64(date.timeIntervalSince1970 * 1000)
        
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        
        // Convert Timestamp to NSDate
        let date22:NSDate = NSDate(timeIntervalSince1970: (Double(dateString)!)/1000)
        
        var dateString = String()
        
        let nYears = timeDiff / (1000*60*60*24*365)
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        var timeMsg = ""
        if nYears > 0
        {
           // var yearWord = "years"
            if nYears == 1
            {
               // yearWord = "year"
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd, MMMM yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
            dateString = dateFormatter.string(from: date22 as Date)
            
            timeMsg = dateString //"\(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            //var monthWord = "months"
            if nMonths == 1
            {
                //monthWord = "month"
            }
            
            //Date formatting
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM, dd"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
            dateString = dateFormatter.string(from: date22 as Date)
            
            timeMsg = dateString  //" \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            //Date formatting
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM, dd"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
            let dateString = dateFormatter.string(from: date22 as Date)
            
            if nDays > 7
            {
                timeMsg = dateString
            }
            else
            {
                timeMsg = " \(nDays) \(dayWord) ago"
            }
            
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = " \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = " \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }
    
    
    // MARK: - ***** Owner Image Like Btn Action. *****
    @IBAction func ownerImageLike_btnAction(_ sender: Any) {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
           
        
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.likeUnlikeUsersMedia(user_id: userID, authToken: userToken, mediaID: self.photoDeleteIndex, completion: { (resBool) in
                        
                        self.view.isUserInteractionEnabled = true
                        self.loadingView.isHidden = true
                        
                        if (resBool == "Liked successfully.")
                        {
                            DispatchQueue.main.async {
                                self.userAlbumLikeImage.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.imageLikeCount_lbl.text!
                                
                                if (likesCountStr == "0")
                                {
                                    self.imageLikeCount_lbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        self.imageLikeCount_lbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalCount = Int(self.imageLikeCount_lbl.text!)! + 1
                                        self.imageLikeCount_lbl.text = String(totalCount)
                                    }
                                }
                            }
                        }
                        else if (resBool == "Unliked successfully.")
                        {
                            DispatchQueue.main.async {
                                self.userAlbumLikeImage.image = UIImage(named: "confessionLikeIcon")
                            }
             
                            var likesCountStr = String()
                            likesCountStr = self.imageLikeCount_lbl.text!
                            
                            if (likesCountStr == "0")
                            {
                                self.imageLikeCount_lbl.text = ""
                            }
                            else
                            {
                                if (likesCountStr == "")
                                {
                                     self.imageLikeCount_lbl.text = ""
                                }
                                else
                                {
                                    let totalCount = Int(self.imageLikeCount_lbl.text!)! - 1
                                    self.imageLikeCount_lbl.text = String(totalCount)
                                }
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self.userAlbumLikeImage.image = UIImage(named: "confessionLikeIcon")
                                self.imageLikeCount_lbl.text = self.imageLikeCount_lbl.text
                            }
                        }
                    })
                }
            }
        }
    }
    
    func likeUnlikeUsersMedia(user_id: String, authToken: String, mediaID: String, completion :@escaping (String)  -> Swift.Void)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    
                    ApiHandler.likeUnlikeMedia(userID: userID, authToken: userToken, media_id: mediaID, completion: { (responseData) in
                        
                        if (responseData.value(forKey: "status") as? String == "200")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            if (message != nil)
                            {
                                completion(message!)
                            }
                        }
                        else if (responseData.value(forKey: "status") as? String == "400")
                        {
                            let message = responseData.value(forKey: "message") as? String
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            self.backBtnLbl.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            completion(message!)
                        }
                        else
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loadingView.isHidden = true
                            let alert = UIAlertController(title: "Alert!", message: "Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            completion("Error")
                        }
                    })
                }
            }
        }
    }
    
    
    
    
    // MARK: - ***** Fetch All Liked Count For Uploaded Images. *****
    func fetchImageLikedByUsersCount(imageIdStr: String)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WiFi")
       
            /*
            if let ownerId: String = usersDetailsDict.value(forKey: "UserID") as? String
            {
                if imageIdStr != ""
                {
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child(imageIdStr).child("LikedByUsers").observe(FIRDataEventType.value, with: { snapshot in
                        self.picturesLikedByArray = []
                        if snapshot.exists()
                        {
                            print("snapshot for my uploaded images liked by users exists")
                            let enumerator = snapshot.children
                            while let rest = enumerator.nextObject() as? FIRDataSnapshot
                            {
                                var userData = NSMutableDictionary()
                                userData = rest.value as! NSMutableDictionary
                                print("userData = \(userData)")
                                
                                if !(self.picturesLikedByArray.contains(rest.key))
                                {
                                    self.picturesLikedByArray.add(rest.key)
                                }
                                print("picturesLikedByArray = \(self.picturesLikedByArray)")
                            }
                            
                            if self.picturesLikedByArray.count > 0
                            {
                                self.imageLikeCount_lbl.text = String(self.picturesLikedByArray.count)
                            }
                            else
                            {
                                self.imageLikeCount_lbl.text = ""
                            }
                            
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            
                            
                            if self.picturesLikedByArray.count == 0
                            {
                                self.picturesLikedByArray = []
                                self.imageLikeCount_lbl.text = ""
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                            }
                            
                            
                            if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                            {
                                if self.picturesLikedByArray.contains(id)
                                {
                                    self.userAlbumLikeImage.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    //confessionLikeIcon
                                    self.userAlbumLikeImage.image = UIImage(named: "confessionLikeIcon")
                                }
                            }

                        }
                        else
                        {
                            print("snapshot for my uploaded images liked by users doesnot exists")
                            self.imageLikeCount_lbl.text = ""
                            self.userAlbumLikeImage.image = UIImage(named: "confessionLikeIcon")
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        }
                    })
                }
            }*/
        }
    }
    
    
    /*
    // MARK: - ***** Fetch All Replies on particular post. *****
    func fetchAllLikesCountForParticularImageUploaded(imageIdStr: String)
    {
        if let ownerId: String = usersDetailsDict.value(forKey: "UserID") as? String
        {
            if imageIdStr != ""
            {
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child(imageIdStr).child("LikesCount").observe(FIRDataEventType.value, with: { (snapshot) in
                    if snapshot.value is NSNull
                    {
                        self.imageLikeCount_lbl.text = ""
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        return
                    }
                    
                    DispatchQueue.main.async {
                        let count = Int((snapshot.value as? String)!)!
                        if count > 0
                        {
                            self.imageLikeCount_lbl.text = snapshot.value as? String
                        }
                        else
                        {
                            self.imageLikeCount_lbl.text = ""
                        }
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                })
            }
        }
    }*/

    
    
    // MARK: - ***** Owner Image Super Like Btn Action. *****
    @IBAction func ownerImageSuperLike_btnAction(_ sender: Any) {
       /* let reference = FIRDatabase.database().reference()
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WWAN")
            print("Connected via WiFi")
            
            let name = UserDefaults.standard.value(forKey: "UserName") as! String
            let currentUserID: String = (UserDefaults.standard.value(forKey: "UserAuthID") as? String)!
            let ownerId: String = usersDetailsDict.value(forKey: "UserID") as! String
            
            if self.picturesSuperLikedByArray.contains(currentUserID)
            {
                var totalLikesCountInt = Int()
                totalLikesCountInt = Int(self.imageSuperLikeCount_Lbl.text!)!
                
                if totalLikesCountInt > 0
                {
                    totalLikesCountInt = totalLikesCountInt - 1
                }
                
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Photos").child(self.photoDeleteIndex).updateChildValues(["SuperLikesCount" : String(totalLikesCountInt)])
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Photos").child(self.photoDeleteIndex).child("SuperLikedByUsers").child(currentUserID).setValue(nil)
            }
            else
            {
                var totalLikesCountInt = Int()
                totalLikesCountInt = Int(self.imageSuperLikeCount_Lbl.text!)!
                totalLikesCountInt = totalLikesCountInt + 1
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Photos").child(self.photoDeleteIndex).child("SuperLikedByUsers").child(currentUserID).updateChildValues(["UserID": currentUserID , "UserName": name])
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Photos").child(self.photoDeleteIndex).updateChildValues(["SuperLikesCount" : String(totalLikesCountInt)])
                
                if self.opponentUserDeviceId != ""
                {
                    if  self.relLikesStatus  == "relLikesNotPurchased"
                    {
                        self.sendNotificationsForAlbumPictures("someone has rel liked your picture.", andToken: self.opponentUserDeviceId)
                    }
                    else
                    {
                        let name = UserDefaults.standard.value(forKey: "UserName") as! String
//                        self.sendNotifications("\(name) viewed your profile.", andToken: self.opponentUserDeviceId)
                        self.sendNotificationsForAlbumPictures("\(name) has rel liked your picture.", andToken: self.opponentUserDeviceId)
                    }
                   
                }
            }
            
            self.fetchImageSuperLikedByUsersCount(imageIdStr: self.photoDeleteIndex)
        }*/
    }
    
    
    
    // MARK: - Fetch All Super Liked Count For Uploaded Images.
   /* func fetchImageSuperLikedByUsersCount(imageIdStr: String)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WWAN")
            print("Connected via WiFi")
            self.loadingView.isHidden = false
            self.view.isUserInteractionEnabled = false
            
            if let userId = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if imageIdStr != ""
                {
                     let ownerId: String = usersDetailsDict.value(forKey: "UserID") as! String
                    //                FIRDatabase.database().reference().child("Users").child(userId).child("Uploads").child("Photos").child(imageIdStr).child("LikedByUsers").keepSynced(true)
                    FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Photos").child(imageIdStr).child("SuperLikedByUsers").observe(FIRDataEventType.value, with: { snapshot in
                        
                        self.picturesSuperLikedByArray = []
                        if snapshot.exists()
                        {
                            print("snapshot for my uploaded images liked by users exists")
                            
                            let enumerator = snapshot.children
                            while let rest = enumerator.nextObject() as? FIRDataSnapshot
                            {
                                var userData = NSMutableDictionary()
                                userData = rest.value as! NSMutableDictionary
                                print("userData = \(userData)")
                                if !(self.picturesSuperLikedByArray.contains(rest.key))
                                {
                                    self.picturesSuperLikedByArray.add(rest.key)
                                }
                                print("picturesSuperLikedByArray = \(self.picturesSuperLikedByArray)")
                            }
                            
                            self.imageSuperLikeCount_Lbl.text = String(self.picturesSuperLikedByArray.count)
                            
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            if self.picturesSuperLikedByArray.count == 0
                            {
                                self.picturesSuperLikedByArray = []
                                self.imageSuperLikeCount_Lbl.text = "0"
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                            }
                        }
                        else
                        {
                            print("snapshot for my uploaded images liked by users doesnot exists")
                            self.imageSuperLikeCount_Lbl.text = "0"
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        }
                    })
                }
            }
        }
    } */
    
    
    
    // MARK: - Fetch All Super like counts.
   /* func fetchAllSuperLikesCountForParticularImageUploaded(imageIdStr: String)
    {
        if let userId = UserDefaults.standard.value(forKey: "UserAuthID") as? String
        {
            if imageIdStr != ""
            {
                let ownerId: String = usersDetailsDict.value(forKey: "UserID") as! String
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Photos").child(imageIdStr).child("SuperLikesCount").observe(FIRDataEventType.value, with: { (snapshot) in
                    print("snapshot value for  images SuperLikesCount  = \(snapshot.value)")
                    if snapshot.value is NSNull
                    {
                        self.imageSuperLikeCount_Lbl.text = "0"
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.imageSuperLikeCount_Lbl.text = snapshot.value as? String
                    }
                })
            }
        }
    } */

    
    // MARK: - ***** Owner Video Like Btn Action. *****
    @IBAction func ownerVideoLike_btnAction(_ sender: Any)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            
            if let userID = UserDefaults.standard.value(forKey: "UserAuthID") as? String
            {
                if let userToken = UserDefaults.standard.value(forKey: "auth_token") as? String
                {
                    self.likeUnlikeUsersMedia(user_id: userID, authToken: userToken, mediaID: self.deleteVideoURL, completion: { (resBool) in
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        
                        if (resBool == "Liked successfully.")
                        {
                            DispatchQueue.main.async {
                                self.userVideoLikeImage.image = UIImage(named: "LikeIconBlue")
                                
                                var likesCountStr = String()
                                likesCountStr = self.videoLikeCount_lbl.text!
                                if (likesCountStr == "0")
                                {
                                    self.videoLikeCount_lbl.text = "1"
                                }
                                else
                                {
                                    if (likesCountStr == "")
                                    {
                                        self.videoLikeCount_lbl.text = "1"
                                    }
                                    else
                                    {
                                        let totalCount = Int(self.videoLikeCount_lbl.text!)! + 1
                                        self.videoLikeCount_lbl.text = String(totalCount)
                                    }
                                }
                            }
                        }
                        else if (resBool == "Unliked successfully.")
                        {
                            DispatchQueue.main.async {
                                self.userVideoLikeImage.image = UIImage(named: "confessionLikeIcon")
                            }
                            
                            var likesCountStr = String()
                            likesCountStr = self.videoLikeCount_lbl.text!
                            if (likesCountStr == "0")
                            {
                                self.videoLikeCount_lbl.text = ""
                            }
                            else
                            {
                                if (likesCountStr == "")
                                {
                                    self.videoLikeCount_lbl.text = ""
                                }
                                else
                                {
                                    let totalCount = Int(self.videoLikeCount_lbl.text!)! - 1
                                    self.videoLikeCount_lbl.text = String(totalCount)
                                }
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self.userVideoLikeImage.image = UIImage(named: "confessionLikeIcon")
                                self.videoLikeCount_lbl.text  = self.videoLikeCount_lbl.text
                            }
                        }
                    })
                }
            }
        }
    }
    
    /*
    
    // MARK: - ***** Fetch All Liked Count For Uploaded Videos. *****
    func fetchVideoLikedByUsersCount(imageIdStr: String)
    {
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WWAN")
            print("Connected via WiFi")
            
            if  let ownerId: String = usersDetailsDict.value(forKey: "UserID") as? String
            {
                if imageIdStr != ""
                {
                    self.loadingView.isHidden = false
                    self.view.isUserInteractionEnabled = false
                    FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child(imageIdStr).child("LikedByUsers").observe(FIRDataEventType.value, with: { snapshot in
                        
                        self.videosLikedByArray = []
                        if snapshot.exists()
                        {
                            print("snapshot for my uploaded Videos liked by users exists")
                            
                            let enumerator = snapshot.children
                            while let rest = enumerator.nextObject() as? FIRDataSnapshot
                            {
                                //var userData = NSMutableDictionary()
                                //userData = rest.value as! NSMutableDictionary
                                
                                if !(self.videosLikedByArray.contains(rest.key))
                                {
                                    self.videosLikedByArray.add(rest.key)
                                }
                            }
                            
                            if self.videosLikedByArray.count > 0
                            {
                                self.videoLikeCount_lbl.text = String(self.videosLikedByArray.count)
                            }
                            else{
                                self.videoLikeCount_lbl.text = ""
                            }
                            
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                            if self.videosLikedByArray.count == 0
                            {
                                self.videosLikedByArray = []
                                self.videoLikeCount_lbl.text = ""
                                self.userVideoLikeImage.image = UIImage(named: "confessionLikeIcon")
                                self.loadingView.isHidden = true
                                self.view.isUserInteractionEnabled = true
                            }
                            
                            if let id = UserDefaults.standard.value(forKey: "UserAuthID") as? String
                            {
                                if self.videosLikedByArray.contains(id)
                                {
                                    self.userVideoLikeImage.image = UIImage(named: "LikeIconBlue")
                                }
                                else
                                {
                                    self.userVideoLikeImage.image = UIImage(named: "confessionLikeIcon")
                                }
                            }
                        }
                        else
                        {
                            self.videosLikedByArray = []
                            print("snapshot for my uploaded Videos liked by users doesnot exists")
                            self.videoLikeCount_lbl.text = ""
                            self.userVideoLikeImage.image = UIImage(named: "confessionLikeIcon")
                            self.loadingView.isHidden = true
                            self.view.isUserInteractionEnabled = true
                        }
                    })
                }
            }
        }
    }
    
    
    
    // MARK: - ***** Fetch All Replies on particular post. *****
    func fetchAllLikesCountForParticularVideoUploaded(imageIdStr: String)
    {
        if  let ownerId: String = usersDetailsDict.value(forKey: "UserID") as? String
        {
            if imageIdStr != ""
            {
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child(imageIdStr).child("LikesCount").observe(FIRDataEventType.value, with: { (snapshot) in
                    if snapshot.value is NSNull
                    {
                        self.videoLikeCount_lbl.text = ""
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                        return
                    }
                    
                    DispatchQueue.main.async {
                        let count = Int((snapshot.value as? String)!)!
                        if count > 0
                        {
                             self.videoLikeCount_lbl.text = snapshot.value as? String
                        }
                        else
                        {
                            self.videoLikeCount_lbl.text = ""
                        }
                        
                        self.loadingView.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                })
            }
        }
    }*/
    
    
    
    // MARK: - ***** Owner Video Super Like Btn Action. *****
    @IBAction func ownerVideoSuperLike_btnAction(_ sender: Any) {
        /*let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            let alert = UIAlertController(title: "", message: "No Internet Connection.Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan) , .online(.wiFi):
            print("Connected via WWAN")
            print("Connected via WiFi")
            
            let name = UserDefaults.standard.value(forKey: "UserName") as! String
            let currentUserID: String = (UserDefaults.standard.value(forKey: "UserAuthID") as? String)!
            let ownerId: String = usersDetailsDict.value(forKey: "UserID") as! String
            
            self.videoLikedBool = true
            
            if self.videosSuperLikedByArray.contains(currentUserID)
            {
                var totalLikesCountInt = Int()
                totalLikesCountInt = Int(self.videoSuperLikeCount_lbl.text!)!
                
                if totalLikesCountInt > 0
                {
                    totalLikesCountInt = totalLikesCountInt - 1
                }
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Videos").child(self.deleteVideoURL).updateChildValues(["SuperLikesCount" : String(totalLikesCountInt)])
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Videos").child(self.deleteVideoURL).child("SuperLikedByUsers").child(currentUserID).setValue(nil)
            }
            else
            {
                var totalLikesCountInt = Int()
                totalLikesCountInt = Int(self.videoSuperLikeCount_lbl.text!)!
                totalLikesCountInt = totalLikesCountInt + 1
            FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Videos").child(self.deleteVideoURL).child("SuperLikedByUsers").child(currentUserID).updateChildValues(["UserID": currentUserID , "UserName": name])
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child("Videos").child(self.deleteVideoURL).updateChildValues(["SuperLikesCount" : String(totalLikesCountInt)])
                
                if self.opponentUserDeviceId != ""
                {
                    
                    if  self.relLikesStatus  == "relLikesNotPurchased"
                    {
                        self.sendNotificationsForAlbumVideo("someone has rel liked your video.", andToken: self.opponentUserDeviceId)
                    }
                    else
                    {
                        let name = UserDefaults.standard.value(forKey: "UserName") as! String
//                        self.sendNotifications("\(name) viewed your profile.", andToken: self.opponentUserDeviceId)
                        self.sendNotificationsForAlbumVideo("\(name) has rel liked your video.", andToken: self.opponentUserDeviceId)
                    }
                }
            }
        } */
    }
   
    
    /*
    
    // MARK: - ***** Post Video Deleted Alert *****
    func postVideoDeletedAlert(imageIdStr: String)
    {
       
        if  let ownerId: String = usersDetailsDict.value(forKey: "UserID") as? String
        {
            if imageIdStr != ""
            {
                 FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child(imageIdStr).observe(FIRDataEventType.value, with: { (snapshot) in
                    if snapshot.exists()
                    {
                        print("current post exists")
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Oops! Current Post is no longer available.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                            
                            self.photosCollectionView.isHidden = false
                            self.aboutMeView.isHidden = true
                            self.aboutMe_scrollView.isHidden = true
                            self.PhotosView.isHidden = true
                            self.videosView.isHidden = true
                            self.tappedBtnStr = "albumBtn"
                            self.viewVideoView.isHidden = true
                            self.aboutMeBtn.setTitleColor(UIColor.gray, for: .normal)
                            self.albumBnt.setTitleColor(UIColor.black, for: .normal)
                            self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                            
                            self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
                            self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                            self.postsBtnATop.setTitleColor(UIColor.gray, for: .normal)
                            
                            self.getVideoURLArray = []
                            self.timestampVideoArray = []
                            self.videoArray = []
                            self.superLikesCountArray = []
                            self.likesCountArray = []
                            self.thumbNailArray = []
                            self.captionVideoArray = []
                            self.albumArray = []
                            self.getUploadedAlbum()
                            self.photosCollectionView.reloadData()
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    
    // MARK: - ***** Post Photo Deleted Alert *****
    func postPhotoDeletedAlert(imageIdStr: String)
    {
        if let ownerId = usersDetailsDict.value(forKey: "UserID") as? String
        {
            if imageIdStr != ""
            {
                FIRDatabase.database().reference().child("Users").child(ownerId).child("Uploads").child(imageIdStr).observe(FIRDataEventType.value, with: { (snapshot) in
                    if snapshot.exists()
                    {
                        print("current post exists")
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Oops! Current Post is no longer available.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (alert) -> Void in
                            
                            self.photosCollectionView.isHidden = false
                            self.aboutMeView.isHidden = true
                            self.aboutMe_scrollView.isHidden = true
                            self.PhotosView.isHidden = true
                            self.videosView.isHidden = true
                            self.ViewFullSize_ImageView.isHidden = true
                            self.tappedBtnStr = "albumBtn"
                            self.aboutMeBtn.setTitleColor(UIColor.gray, for: .normal)
                            self.albumBnt.setTitleColor(UIColor.black, for: .normal)
                            self.postsBtn.setTitleColor(UIColor.gray, for: .normal)
                            
                            self.albumBtnTop.setTitleColor(UIColor.black, for: .normal)
                            self.aboutMeTop.setTitleColor(UIColor.gray, for: .normal)
                            self.postsBtnATop.setTitleColor(UIColor.gray, for: .normal)
                            
                            self.photosCollectionView.reloadData()
                            self.captionPhotoArray = []
                            self.timestampArray = []
                            self.getPhotoKeyArray = []
                            self.profileImagesArray = []
                            self.albumArray = []
                            self.getUploadedAlbum()
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                })
            }
        }
    }
    */


    
    // MARK: - ***** viewImage_btnAction *****
    @IBAction func viewImage_btnAction(_ sender: Any) {
        self.showCurrentPostedImage.image = self.viewParcticularImage.image
        self.showCurrentPostedImage.contentMode = UIViewContentMode.scaleAspectFit
        self.showCurrentPostedImage.clipsToBounds = true
        
        self.viewImage.isHidden=false
       /* self.showCurrentPostedImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.showCurrentPostedImage.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { (finished) in
        })*/
    }
    
    
    // MARK: - ***** closeViewImage_btnAction *****
    @IBAction func closeViewImage_btnAction(_ sender: Any) {
        self.viewImage.isHidden = true
        if self.player != nil
        {
            playerViewController.player!.pause()
            if let play = player {
                print("stopped")
                play.pause()
                player = nil
                print("player deallocated")
            } else {
                print("player was already deallocated")
            }
        }
    }
    
    
    
    // MARK: - ***** DidReceiveMemoryWarning. *****
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
