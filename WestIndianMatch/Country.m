//
//  Customer.m
//  Chanda
//
//  Created by Mohammad Azam on 10/25/11.
//  Copyright (c) 2011 HighOnCoding. All rights reserved.
//

#import "Country.h"

@implementation Country

@synthesize customerId,firstName,name,lastName;

-(NSString *) getName 
{
    return [NSString stringWithFormat:@"%@ %d",self.firstName,self.customerId];
}

@end
