//
//  HeaderCommentsForGif.swift
//  Wi Match
//
//  Created by brst on 01/08/18.
//  Copyright © 2018 brst. All rights reserved.
//

import UIKit
import FLAnimatedImage

class HeaderCommentsForGif: UITableViewHeaderFooterView {

    
    @IBOutlet weak var statusTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var usersLocationLbl: UILabel!
    @IBOutlet weak var usersNameLbl: UILabel!
    @IBOutlet weak var usersProfileIcon: UIImageView!
    
    @IBOutlet weak var viewProfileBtn: UIButton!
    
    @IBOutlet weak var dropDownOptionsBtn: UIButton!
    
    @IBOutlet weak var postedGifImage: FLAnimatedImageView!
    
    @IBOutlet weak var postedGifViewBtn: UIButton!
    
    @IBOutlet weak var confesionLikeIcon: UIImageView!
    @IBOutlet weak var statusPostedTextView: UITextView!
    
    @IBOutlet weak var likesBtn: UIButton!
    @IBOutlet weak var likesCountLbl: UILabel!
    
    @IBOutlet weak var replyBtn: UIButton!
    @IBOutlet weak var repliesCountLbl: UILabel!
    
    @IBOutlet weak var messageBtn: UIButton!
    
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
