//
//  Model.swift
//  Wi Match
//
//  Created by brst on 31/05/18.
//  Copyright © 2018 brst. All rights reserved.
//

import Foundation


 class getUserDetails {
    
    //MARK: Properties
    let userName: String
    let email: String
    let currentLocation: String
    let dob: String
    let gender: String
    let countryRepresenting: String
    let meetUpPreferences: String
    let interestedInMeetingWith: String
    let bodyType: String
    let hairColor: String
    let smoking: String
    let drinking: String
    let education: String
    let profilePic: String
    let userID: String
    let age: String
    let currentLat: String
    let currentLong: String
    let userType: String
    let relationshipStatus: String
    let detailsDict: NSDictionary
    
    init(myDetails:NSDictionary) {
        
        self.detailsDict = myDetails
        self.userName = myDetails.value(forKey: "username") as! String
        self.email = myDetails.value(forKey: "email") as! String
        self.currentLocation = myDetails.value(forKey: "location") as! String
        self.dob = myDetails.value(forKey: "dob") as! String
        self.gender = myDetails.value(forKey: "gender") as! String
        self.countryRepresenting = myDetails.value(forKey: "country") as! String
        self.meetUpPreferences = myDetails.value(forKey: "meet_up_preferences") as! String
        self.interestedInMeetingWith = myDetails.value(forKey: "intrested_in_meeting") as! String
        self.bodyType = myDetails.value(forKey: "body_type") as! String
        self.hairColor = myDetails.value(forKey: "hair_color") as! String
        self.smoking = myDetails.value(forKey: "smoking") as! String
        self.drinking = myDetails.value(forKey: "drinking") as! String
        self.education = myDetails.value(forKey: "education") as! String
        self.profilePic = myDetails.value(forKey: "profile_pic") as! String
        self.userID = myDetails.value(forKey: "id") as! String
        self.age = myDetails.value(forKey: "age") as! String
        self.currentLat = myDetails.value(forKey: "latitude") as! String
        self.currentLong = myDetails.value(forKey: "longitude") as! String
        self.userType = myDetails.value(forKey: "user_type") as! String
        self.relationshipStatus = myDetails.value(forKey: "relationship_status") as! String
    }
    
    func getData(completion: @escaping (NSDictionary) -> Swift.Void)
    {
        completion(self.detailsDict)
    }
    
}


class bodTypeClass
{
    let name: String
    let id: String
    
    init(bodyType: String, id: String)
    {
        self.name = bodyType
        self.id = id
    }
}
