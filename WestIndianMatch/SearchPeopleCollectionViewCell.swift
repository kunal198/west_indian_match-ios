//
//  SearchPeopleCollectionViewCell.swift
//  Wi Match
//
//  Created by brst on 20/03/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class SearchPeopleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var usersStatusOnlineImage: UIImageView!
    @IBOutlet weak var location_lbl: UILabel!
    @IBOutlet weak var usersName_lbl: UILabel!
    @IBOutlet weak var usersProfileImage: UIImageView!
    
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        usersProfileImage.layer.cornerRadius = usersProfileImage.frame.width/2
//        usersProfileImage.layer.backgroundColor = UIColor.whiteColor().CGColor
    }
}
