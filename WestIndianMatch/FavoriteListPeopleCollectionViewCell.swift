//
//  FavoriteListPeopleCollectionViewCell.swift
//  Wi Match
//
//  Created by brst on 20/03/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class FavoriteListPeopleCollectionViewCell: UICollectionViewCell {
 
    
    @IBOutlet weak var onlineUserIcon: UIImageView!
    @IBOutlet weak var usersName: UILabel!
    @IBOutlet weak var usersProfileImage: UIImageView!
    
    
    @IBOutlet weak var hoursLbl: UILabel!
    @IBOutlet weak var radioABtnIcon: UIImageView!
    
    //MARK: - CreatePostViewController
    @IBOutlet weak var selectBackgroundImage: UIImageView!
    @IBOutlet weak var selectedImage: UIImageView!
    
    //MARK: - WiFeedsViewController
    @IBOutlet weak var feedLikes_lbl: UILabel!
    @IBOutlet weak var feedTime_lbl: UILabel!
    @IBOutlet weak var feedBackgroundImage: UIImageView!
    @IBOutlet weak var feedUserAge_lbl: UILabel!
    @IBOutlet weak var feedUserLocation_lbl: UILabel!
    @IBOutlet weak var feedsStatus_lbl: UILabel!
    
    @IBOutlet weak var postOwnerStatus_Icon: UIImageView!
    @IBOutlet weak var totalReply_Lbl: UILabel!
    
    //MARK: - OpenParticularFeedViewController
    @IBOutlet weak var repliedUserAge: UILabel!
    @IBOutlet weak var repliedUserLocation_lbl: UILabel!
    @IBOutlet weak var repliesBackgroundImage: UIImageView!
    @IBOutlet weak var repliesText_lbl: UILabel!
    
    @IBOutlet weak var deleteParticularPostBtn: UIButton!
    
    
}
